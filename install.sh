
#!/bin/bash

# param 1 PROD_DB_TOKEN
# param 2 docker-compose run file
# param 3 DB PASSWORD

# Установить docker
apt install curl
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh

# Установить docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
usermod -a -G docker $USER
systemctl enable docker # Auto-start on boot
systemctl start docker # Start right now
chown $USER:docker /var/run/docker.sock

