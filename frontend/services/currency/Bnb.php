<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\VarDumper;
use yii\helpers\StringHelper;

class Bnb extends Erc20 implements CurrencyInterface
{
    public $tokenAddress = '0XB8C77482E45F1F44DE1745F52C74426C631BDD52';
    public $decimals = 18;

    /**
     * @param \common\models\piramida\Currency  $cInt
     * @param \common\models\avatar\Currency    $cExt
     *
     * @return array
     * [
     *  'comission_options' = массив для выбора комиссии / null
     *  'is_show_comission' = bool показывать строку с комиссией
     * ]
     */
    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $currencyExt = \common\models\avatar\Currency::findOne($cExt->id);
        $eth = \common\models\avatar\Currency::findOne(['code' => 'ETH']);
        $ethPrice = $eth->price_usd;

        $EthereumProposeGasPrice = \common\models\Config::get('EthereumProposeGasPrice');
        $EthereumFastGasPrice = \common\models\Config::get('EthereumFastGasPrice');

        $comission1 = 70000 * $EthereumProposeGasPrice;
        $comission1 = bcdiv($comission1, bcpow(10, 9), 18);
        $comission1 = bcdiv(bcmul($comission1, $ethPrice ,$cInt->decimals) , $currencyExt->price_usd, $cInt->decimals);


        $comission2 = 70000 * $EthereumFastGasPrice;
        $comission2 = bcdiv($comission2, bcpow(10, 9), 18);
        $comission2 = bcdiv(bcmul($comission2, $ethPrice ,$cInt->decimals) , $currencyExt->price_usd, $cInt->decimals);


        $comission3 = bcmul($comission1, bcpow(10, $cInt->decimals));
        $comission4 = bcmul($comission2, bcpow(10, $cInt->decimals));

        $comission1_usd = bcmul($comission1, $cExt->price_usd, 2);
        $comission2_usd = bcmul($comission2, $cExt->price_usd, 2);

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = $(this).val();
    var c1 = {$comission1};
    var c2 = {$comission2};
    var c3 = '';
    var c4 = '';
    $('input[name = "TaskOutput[comission_id]"]').each(function(o, i) {
        if ($(i).is(':checked')) {
            c3 = $(i).attr('value');
        }
    });
    if (c3 == 2) {
        c4 = c1;
        $('#taskoutput-comission_user').val($comission3);
    }
    if (c3 == 3) {
        c4 = c2;
        $('#taskoutput-comission_user').val($comission4);
    }
    if (c3 == "") {
        c4 = 0;
        $('#taskoutput-comission_user').val('');
    }
    $('#comission_blockchain').html(c4 + ' ' + '{$cInt->code}');
    $('#sum_exit').val(val - parseFloat(c4));
});

$('input[name = "TaskOutput[comission_id]"]').on('change', function(e) {
    var val = $('#taskoutput-amount').val();
    var c1 = {$comission1};
    var c2 = {$comission2};
    var c3 = '';
    var c4 = '';
    $('input[name = "TaskOutput[comission_id]"]').each(function(o, i) {
        if ($(i).is(':checked')) {
            c3 = $(i).attr('value');
        }
    });
    if (c3 == 2) {
        c4 = c1;
        $('#taskoutput-comission_user').val($comission3);
    }
    if (c3 == 3) {
        c4 = c2;
        $('#taskoutput-comission_user').val($comission4);
    }
    if (c3 == "") {
        c4 = 0;
        $('#taskoutput-comission_user').val('');
    }
    $('#comission_blockchain').html(c4 + ' ' + '{$cInt->code}');
    $('#sum_exit').val(val - parseFloat(c4));
});

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var c1 = {$comission1};
    var c2 = {$comission2};
    var c3 = '';
    var c4 = '';
    $('input[name = "TaskOutput[comission_id]"]').each(function(o, i) {
        if ($(i).is(':checked')) {
            c3 = $(i).attr('value');
        }
    });
    if (c3 == 2) {
        c4 = c1;
        $('#taskoutput-comission_user').val($comission3);
    }
    if (c3 == 3) {
        c4 = c2;
        $('#taskoutput-comission_user').val($comission4);
    }
    if (c3 == "") {
        c4 = 0;
        $('#taskoutput-comission_user').val('');
    }
    var x = parseFloat(val) + parseFloat(c4);
    $('#comission_blockchain').html(c4 + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
});

JS
        );

        return [
            'comission_options' => [
                2 => \Yii::t('c.TB9ZAPoWIy', 'Средняя') . ' ' . $comission1 . ' ' . $cInt->code . ' (~' . $comission1_usd . ' USDT)',
                3 => \Yii::t('c.TB9ZAPoWIy', 'Высокая') . ' ' . $comission2 . ' ' . $cInt->code . ' (~' . $comission2_usd . ' USDT)',
            ],
            'is_show_comission' => true
        ];
    }
}