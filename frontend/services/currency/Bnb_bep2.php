<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\VarDumper;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class Bnb_bep2 implements CurrencyInterface
{
    public $decimals = 18;

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        return ['status' => 0];
    }

    /**
     * @param $data
     */
    public function qrcode($data)
    {
        $html = [];
        $html[] = '<b style="color: red; font-size: 26px;" class="buttonCopy" data-toggle="tooltip" data-clipboard-text="'.$data['master_wallet'].'" title="Нажмите чтобы скопировать">'.$data['master_wallet'].'</b>';
        $extended_info = Json::decode($data['extended_info']);
        $html[] = '<p>MEMO:<br><b>'.$extended_info['memo'].'</b></p>';

        $wallet_address = $data['master_wallet'];

        $qr = new \Endroid\QrCode\QrCode();
        $qr->setText($wallet_address);
        $qr->setSize(180);
        $qr->setMargin(20);
        $qr->setErrorCorrectionLevel(\Endroid\QrCode\ErrorCorrectionLevel::HIGH());
        $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
        $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
        $qr->setLabelFontSize(16);
        $content = $qr->writeString();
        $src = 'data:image/png;base64,' . base64_encode($content);

        $html[] = '<img src="'.$src.'">';

        return join('', $html);
    }


    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, 'bnb')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с bnb'),
            ];
        }

        return [
            'status' => true,
        ];
    }


    /**
     * @param \common\models\piramida\Currency  $cInt
     * @param \common\models\avatar\Currency    $cExt
     *
     * @return array
     * [
     *  'comission_options' = массив для выбора комиссии / null
     *  'is_show_comission' = bool показывать строку с комиссией
     * ]
     */
    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}