<?php

namespace avatar\services\currency;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\TaskInput;
use common\models\TaskPrizmInput;
use cs\services\VarDumper;
use Yii;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class Prizm extends \yii\base\BaseObject implements CurrencyInterface
{
    public $decimals = 2;

    /**
     * @param $data
     */
    public function qrcode($data)
    {
        $html = [];
        $html[] = '<b style="color: red; font-size: 26px;" class="buttonCopy" data-toggle="tooltip" data-clipboard-text="'.$data['master_wallet'].'" title="Нажмите чтобы скопировать">'.$data['master_wallet'].'</b>';
        $extended_info = Json::decode($data['extended_info']);
        $html[] = '<p>Публичный ключ:<br><b>'.$extended_info['public_key'].'</b></p>';

        $wallet_address = $data['master_wallet'];

        $qr = new \Endroid\QrCode\QrCode();
        $qr->setText($wallet_address);
        $qr->setSize(180);
        $qr->setMargin(20);
        $qr->setErrorCorrectionLevel(\Endroid\QrCode\ErrorCorrectionLevel::HIGH());
        $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
        $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
        $qr->setLabelFontSize(16);
        $content = $qr->writeString();
        $src = 'data:image/png;base64,' . base64_encode($content);

        $html[] = '<img src="'.$src.'">';

        return join('', $html);
    }

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        /** @var \common\services\PrizmApi $PrizmApi */
        $PrizmApi = Yii::$app->PrizmApi;

        // Делаю запрос на PRIZM ноду
        try {
            $response = $PrizmApi->post('', [
                'requestType' => 'getBlockchainTransactions',
                'account'     => $currencyIO->master_wallet,
                'lastIndex'   => 100,
            ]);
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $transactionList = $response['transactions'];
        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id])->select('txid')->column();

        if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
            if ($task->account) {
                // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
                foreach ($transactionList as $t) {

                    // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                    if (in_array($t['transaction'], $transactionListSuccess)) continue;

                    if ($t['senderRS'] == $task->account) {
                        $amountNQT = $t['amountNQT'];
                        $task->amount = $amountNQT;

                        $value = $t['amountNQT'];
                        $c = \common\models\piramida\Currency::findOne($currencyIO->currency_int_id);
                        if ($this->decimals > $c->decimals) {
                            $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                        }
                        if ($this->decimals < $c->decimals) {
                            $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                        }

                        return [
                            'status'          => 1,
                            'value'           => $value,
                            'transactionHash' => $t['transaction'],
                        ];
                    }
                }
            }
        }
        if ($task->type_id == TaskInput::TYPE_TRANSACTION) {
            foreach ($transactionList as $t) {

                // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                if (in_array($t['transaction'], $transactionListSuccess)) continue;

                if ($t['transaction'] == $task->txid) {
                    $amountNQT = $t['amountNQT'];
                    $task->amount = $amountNQT;

                    $value = $t['amountNQT'];
                    $c = \common\models\piramida\Currency::findOne($currencyIO->currency_int_id);
                    if ($this->decimals > $c->decimals) {
                        $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                    }
                    if ($this->decimals < $c->decimals) {
                        $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                    }
                    if ($value == $task->amount_user) {
                        return [
                            'status'          => 1,
                            'value'           => $value,
                            'transactionHash' => $t['transaction'],
                        ];
                    }
                }
            }
        }

        return ['status' => 0];
    }

    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, 'PRIZM-')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с PRIZM-'),
            ];
        }

        return [
            'status' => true,
        ];
    }

    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $per = '0.5% max 10 PZM';
        \Yii::$app->view->registerJs(<<<JS
                        
$('#taskoutput-amount').on('input', function(e) {
    var val = $(this).val();
    var c = val * 0.005;
    var cText;
    if (c >= 10) {
        c = 10;
        cText = 10;
    } else {
        cText = c + ' (0.5%)';
    }
    $('#comission_blockchain').html(cText);
    $('#sum_exit').val(val - c);
    var comission_user = parseInt(Math.pow(10, {$cInt->decimals}) * c);
    $('#taskoutput-comission_user').val(comission_user);
});

$('#sum_exit').on('input', function(e) {
    var val = parseFloat($(this).val());
    var x = val / (1 - 0.005);
    var c = x * 0.005;
    var comission_blockchain;
    if (c > 10) {
        comission_blockchain = 10;
        c = 10;
        x = val + c;
    } else {
        comission_blockchain = c + ' (0.5%)';
    }
    $('#comission_blockchain').html(comission_blockchain);
    $('#taskoutput-amount').val(x);
    var comission_user = parseInt(Math.pow(10, {$cInt->decimals}) * c);
    $('#taskoutput-comission_user').val(comission_user);
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}