<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\Str;
use cs\services\VarDumper;
use frontend\services\BaseConvert;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

class Trc20 extends BaseObject
{
    public $tokenAddress = '';
    public $decimals = 0;

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        $url = 'https://api.trongrid.io/v1';
        if (YII_ENV_DEV) $url = 'https://api.shasta.trongrid.io';
        $client = new Client(['baseUrl' => $url]);

        // Делаю запрос на полученные монеты
        try {
            $response = $client->get('accounts/' . $CurrencyIoModification->master_wallet . '/transactions/trc20', [])->send();
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $data = Json::decode($response->content);

        if ($data['success']) {
            $transactionList = $data['data'];
            /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
            $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id, 'modification_id' => $CurrencyIoModification->id])->select('txid')->column();

            if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
                if ($task->account) {
                    $c = 0;
                    // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
                    foreach ($transactionList as $t) {
                        $c++;

                        // Если это перевод трона?
                        if ($t['token_info']['address'] == $this->tokenAddress) {
                            // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                            $txid = $t['transaction_id'];
                            if (in_array(strtolower($txid), $transactionListSuccess)) continue;

                            if ($t['type'] == 'Transfer') {
                                // Адрес получателя
                                // Адрес мастер кошелька и адрес получателя в транзакции совпадают? (пользователь на наш кошелек кидает деньги?)
                                if ($CurrencyIoModification->master_wallet == $t['to']) {

                                    // Адрес отправителя в транзакции и кошелек который указал пользователь в заявке совпадают?
                                    if ($task->account == $t['from']) {

                                        // привожу сумму в соответствие с нашей системой
                                        $value = $t['value'];
                                        $valueAtom = $t['value'];
                                        $c = Currency::findOne($currencyIO->currency_int_id);
                                        if ($this->decimals > $c->decimals) {
                                            $value = bcdiv($valueAtom, bcpow(10, $this->decimals - $c->decimals));
                                        }
                                        if ($this->decimals < $c->decimals) {
                                            $value = bcmul($valueAtom, bcpow(10, $c->decimals - $this->decimals));
                                        }
                                        return [
                                            'status'          => 1,
                                            'value'           => $value,
                                            'transactionHash' => $txid,
                                        ];

                                    }
                                }
                            }
                        }
                    }
                }
            }
            \Yii::info([$task->type_id], 'avatar\services\currency\Trc20:90');
            if ($task->type_id == TaskInput::TYPE_TRANSACTION) {
                // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
                foreach ($transactionList as $t) {
                    // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                    $txid = $t['transaction_id'];
                    if (in_array(strtolower($txid), $transactionListSuccess)) continue;
                    \Yii::info([strtolower($txid), strtolower($task->txid)], 'avatar\services\currency\Trc20:96');
                    if (strtolower($txid) == strtolower($task->txid)) {
                        \Yii::info([$t['token_info']['address'], $this->tokenAddress], 'avatar\services\currency\Trc20:97');
                        if ($t['token_info']['address'] == $this->tokenAddress) {
                            \Yii::info([$t['type']], 'avatar\services\currency\Trc20:98');
                            if ($t['type'] == 'Transfer') {
                                // Адрес получателя
                                // Адрес мастер кошелька и адрес получателя в транзакции совпадают? (пользователь на наш кошелек кидает деньги?)
                                \Yii::info([$CurrencyIoModification->master_wallet, $t['to']], 'avatar\services\currency\Trc20:101');
                                if ($CurrencyIoModification->master_wallet == $t['to']) {

                                    // привожу сумму в соответствие с нашей системой
                                    $value = $t['value'];
                                    $valueAtom = $t['value'];
                                    $c = Currency::findOne($currencyIO->currency_int_id);
                                    if ($this->decimals > $c->decimals) {
                                        $value = bcdiv($valueAtom, bcpow(10, $this->decimals - $c->decimals));
                                    }
                                    if ($this->decimals < $c->decimals) {
                                        $value = bcmul($valueAtom, bcpow(10, $c->decimals - $this->decimals));
                                    }
                                    \Yii::info([$value, $task->amount_user], 'avatar\services\currency\Trc20:113');
                                    if ($value == $task->amount_user) {
                                        return [
                                            'status'          => 1,
                                            'value'           => $value,
                                            'transactionHash' => $txid,
                                            'account'         => $t['from'],
                                        ];
                                    } else {
                                        echo 'task id='. $task->id. ' $value != $task->amount_user' . "\n";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return ['status' => 0];
    }

    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, 'T')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с T'),
            ];
        }

        return [
            'status' => true,
        ];
    }

}