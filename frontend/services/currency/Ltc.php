<?php

namespace avatar\services\currency;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\MasterTransaction;
use common\models\piramida\Wallet;
use common\models\TaskInput;
use common\models\TaskPrizmInput;
use common\services\BlockCypher;
use cs\services\VarDumper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

class Ltc extends \yii\base\BaseObject implements CurrencyInterface
{
    public $decimals = 8;

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        /** @var BlockCypher $BlockCypher */
        $BlockCypher = Yii::$app->BlockCypher;

        // Делаю запрос на API
        try {
            $data = $BlockCypher->call(BlockCypher::COIN_LTC, 'addrs/'.$currencyIO->master_wallet, [
                'limit'   => 2,
            ]);
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $transactionList = $data['txrefs'];
        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id])->select('txid')->column();
        $MasterTransactionList = MasterTransaction::find()
            ->where([
                'currency_id' => $currencyIO->currency_ext_id,
                'address'     => $currencyIO->master_wallet,
            ])
            ->all();
        $MasterTransactionList2 = ArrayHelper::map($MasterTransactionList,'txid' , function ($data) {return $data;});
        $MasterTransactionList3 = array_keys($MasterTransactionList2);

        if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
            if ($task->account) {
                // Если указан призм кошелек то ищу его среди всех последних транзакций.
                foreach ($transactionList as $t) {

                    // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                    if (in_array($t['tx_hash'], $transactionListSuccess)) continue;

                    // Если транзакция у нас в БД нет?
                    if (!in_array($t['tx_hash'], $MasterTransactionList3)) {
                        // Делаю запрос на API
                        try {
                            $data = $BlockCypher->call(BlockCypher::COIN_LTC, 'txs/'.$t['tx_hash']);
                        } catch (\Exception $e) {
                            return ['status' => 2, 'message' => $e->getMessage()];
                        }
                        $MasterTransaction = MasterTransaction::add([
                            'currency_id' => $currencyIO->currency_ext_id,
                            'txid'        => $t['tx_hash'],
                            'data'        => Json::encode($data),
                            'address'     => $currencyIO->master_wallet,
                        ]);
                        echo 'added $MasterTransaction ' . $t['tx_hash'] . "\n";

                    } else {
                        $MasterTransaction = $MasterTransactionList2[$t['tx_hash']];
                    }

                    // Если транзакция от адреса который указал пользователь
                    $data2 = Json::decode($MasterTransaction['data']);
                    if ($this->isAccount($data2['inputs'], $task->account)) {

                        // Ищу наш мастер кошелек среди outputs (входящих транзакций)
                        $value = null;
                        foreach ($data2['outputs'] as $o) {
                            foreach ($o['addresses'] as $a) {
                                if ($a == $currencyIO->master_wallet) {
                                    $value = $o['value'];
                                }
                            }
                        }
                        // Если нашлась хоть одна
                        if (!is_null($value)) {
                            // то возвращаю найденный результат
                            $task->amount = $value;
                            $c = \common\models\piramida\Currency::findOne($currencyIO->currency_int_id);
                            if ($this->decimals > $c->decimals) {
                                $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                            }
                            if ($this->decimals < $c->decimals) {
                                $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                            }

                            return [
                                'status'          => 1,
                                'value'           => $value,
                                'transactionHash' => $t['tx_hash'],
                            ];
                        }
                    }
                }
            }
        }
        if ($task->type_id == TaskInput::TYPE_TRANSACTION) {
            foreach ($transactionList as $t) {

                // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                if (in_array($t['tx_hash'], $transactionListSuccess)) continue;

                if (strtolower($t['tx_hash']) == strtolower($task->txid)) {
                    // Если транзакция у нас в БД нет?
                    if (!in_array($t['tx_hash'], $MasterTransactionList3)) {
                        // Делаю запрос на API
                        try {
                            $data = $BlockCypher->call(BlockCypher::COIN_LTC, 'txs/'.$t['tx_hash']);
                        } catch (\Exception $e) {
                            return ['status' => 2, 'message' => $e->getMessage()];
                        }
                        $MasterTransaction = MasterTransaction::add([
                            'currency_id' => $currencyIO->currency_ext_id,
                            'txid'        => $t['tx_hash'],
                            'data'        => Json::encode($data),
                            'address'     => $currencyIO->master_wallet,
                        ]);
                        echo 'added $MasterTransaction ' . $t['tx_hash'] . "\n";

                    } else {
                        $MasterTransaction = $MasterTransactionList2[$t['tx_hash']];
                    }

                    // Если транзакция от адреса который указал пользователь
                    $data2 = Json::decode($MasterTransaction['data']);

                    // Ищу наш мастер кошелек среди outputs (входящих транзакций)
                    $value = null;
                    foreach ($data2['outputs'] as $o) {
                        foreach ($o['addresses'] as $a) {
                            if ($a == $currencyIO->master_wallet) {
                                $value = $o['value'];
                            }
                        }
                    }
                    // Если нашлась хоть одна
                    if (!is_null($value)) {
                        // то возвращаю найденный результат
                        $task->amount = $value;
                        $c = \common\models\piramida\Currency::findOne($currencyIO->currency_int_id);
                        if ($this->decimals > $c->decimals) {
                            $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                        }
                        if ($this->decimals < $c->decimals) {
                            $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                        }
                        if ($value == $task->amount_user) {
                            return [
                                'status'          => 1,
                                'value'           => $value,
                                'transactionHash' => $t['tx_hash'],
                            ];
                        }
                    }
                }

            }
        }

        return ['status' => 0];
    }

    private function isAccount($inputs, $address)
    {
        foreach ($inputs as $i) {
            foreach ($i['addresses'] as $a) {
                if ($a == $address) {
                    return true;
                }
            }
        }
        return  false;
    }


    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!(StringHelper::startsWith($address, 'L') || StringHelper::startsWith($address, 'M'))) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с L или M'),
            ];
        }

        return [
            'status' => true,
        ];
    }

    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}