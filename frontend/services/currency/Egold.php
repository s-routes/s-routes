<?php

namespace avatar\services\currency;

use common\models\CurrencyIO;
use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\VarDumper;
use yii\base\BaseObject;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

class Egold extends BaseObject implements CurrencyInterface
{
    public $ip = '91.228.155.55';

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        $this->ip = env('EGOLD_SERVER_IP', '91.228.155.55');

        $url = 'http://' . $this->ip;
        $client = new Client(['baseUrl' => $url]);

        // Делаю запрос на полученные монеты
        try {
            $response = $client->get('egold.php', [
                'type'      => 'history',
                'recipient' => $currencyIO->master_wallet,
                'limit'     => '100',
                'all'       => '0',
            ])->send();
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $transactionList = Json::decode($response->content);

        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id])->select('txid')->column();

        if ($task->account) {

            // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
            foreach ($transactionList as $t) {

                // Если транзакция подтверждена блокчейном?
                if ($t['checkhistory'] == 1) {
                    // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                    $txid = $this->getHash($t);
                    if (in_array($txid, $transactionListSuccess)) continue;

                    if (strtolower($t['wallet']) == $this->getShortAddress($task->account)) {
                        $value = $t['money'];

                        return [
                            'status'          => 1,
                            'value'           => $value,
                            'transactionHash' => $txid,
                        ];
                    }
                }
            }

            return ['status' => 0];
        }
    }

    public function getHash($t)
    {
        $data =
            $t['wallet'].
            $t['recipient'].
            $t['money'].
            $t['height'].
            $t['date']
        ;
        return hash('sha256', $data);
    }

    public function getShortAddress($address)
    {
        $address = substr($address, 2);
        $address = str_replace('-', '', $address);
        $address = str_replace('-', '', $address);
        $address = str_replace('-', '', $address);
        $address = str_replace('-', '', $address);

        return $address;
    }


    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, 'G')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с G'),
            ];
        }

        return [
            'status' => true,
        ];
    }

    /**
     * @param Currency $cInt
     * @param \common\models\avatar\Currency $cExt
     * @param array $options
     * ['comission_out']
     */
    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}