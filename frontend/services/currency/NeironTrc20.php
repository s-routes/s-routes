<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\Str;
use cs\services\VarDumper;
use frontend\services\BaseConvert;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

class NeironTrc20 extends Trc20 implements CurrencyInterface
{
    public $tokenAddress = 'TQBhWj95R8unqXxd47AFyLMkhQu8jDxM8G';
    public $decimals = 4;

    /**
     * @param \common\models\piramida\Currency  $cInt
     * @param \common\models\avatar\Currency    $cExt
     *
     * @return array
     * [
     *  'comission_options' = массив для выбора комиссии / null
     *  'is_show_comission' = bool показывать строку с комиссией
     * ]
     */
    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}