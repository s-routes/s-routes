<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\Str;
use cs\services\VarDumper;
use frontend\services\BaseConvert;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

class UsdtTrc20 extends Trc20 implements CurrencyInterface
{
    public $tokenAddress = 'TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t';
    public $decimals = 6;

    public static function getHash($t)
    {
        $data =
            $t['wallet'].
            $t['recipient'].
            $t['money'].
            $t['height'].
            $t['date']
        ;
        return hash('sha256', $data);
    }

    public static function hexToBase58($address)
    {
        $address = BaseConvert::base58_hex($address);

        return $address;
    }

    public static function clearFirstNull($address)
    {
        $chars = Str::getChars($address);
        $rows = [];
        foreach ($chars as $c) {
            if ($c != '0') {
                $rows[] = $c;
            }
        }

        return join('', $rows);
    }

    /**
     * @param \common\models\piramida\Currency  $cInt
     * @param \common\models\avatar\Currency    $cExt
     *
     * @return array
     * [
     *  'comission_options' = массив для выбора комиссии / null
     *  'is_show_comission' = bool показывать строку с комиссией
     * ]
     */
    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}