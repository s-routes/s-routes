<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\VarDumper;
use yii\base\BaseObject;
use yii\helpers\StringHelper;

class Erc20 extends BaseObject
{
    public $tokenAddress = '';
    public $decimals = 0;

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        // Делаю запрос на полученные монеты
        try {
            $provider = new \avatar\modules\ETH\ServiceEthPlorer();
            $response = $provider->getAddressHistory($CurrencyIoModification->master_wallet, $this->tokenAddress);
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $transactionList = $response;
        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id, 'modification_id' => $CurrencyIoModification->id])->select('txid')->column();

        if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
            if ($task->account) {
                // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
                foreach ($transactionList as $t) {

                    // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                    if (in_array(strtolower($t['transactionHash']), $transactionListSuccess)) continue;

                    if (strtolower($t['tokenInfo']['address']) == strtolower($this->tokenAddress)) {
                        if (strtolower($t['from']) == strtolower($task->account)) {
                            $value = $t['value'];

                            $c = Currency::findOne($currencyIO->currency_int_id);
                            if ($this->decimals > $c->decimals) {
                                $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                            }
                            if ($this->decimals < $c->decimals) {
                                $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                            }

                            return [
                                'status'          => 1,
                                'value'           => $value,
                                'transactionHash' => $t['transactionHash'],
                            ];
                        }
                    }
                }

                return ['status' => 0];
            }
        }

        if ($task->type_id == TaskInput::TYPE_TRANSACTION) {
            foreach ($transactionList as $t) {

                // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                if (in_array(strtolower($t['transactionHash']), $transactionListSuccess)) continue;

                if (strtolower($t['transactionHash']) == strtolower($task->txid)) {
                    if (strtolower($t['tokenInfo']['address']) == strtolower($this->tokenAddress)) {
                        $value = $t['value'];

                        $c = Currency::findOne($currencyIO->currency_int_id);
                        if ($this->decimals > $c->decimals) {
                            $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                        }
                        if ($this->decimals < $c->decimals) {
                            $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                        }
                        if ($value == $task->amount_user) {
                            return [
                                'status'          => 1,
                                'value'           => $value,
                                'transactionHash' => $t['transactionHash'],
                                'account'         => $t['from'],
                            ];
                        }
                    }
                }
            }
        }

        return ['status' => 0];
    }

    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, '0x')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с 0x'),
            ];
        }

        return [
            'status' => true,
        ];
    }
}