<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\VarDumper;
use yii\helpers\StringHelper;

class Eth extends \yii\base\BaseObject implements CurrencyInterface
{
    public $decimals = 18;

    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        // Делаю запрос на полученные монеты
        try {
            $provider = new \avatar\modules\ETH\ServiceEtherScan();
            $response = $provider->transactionList($currencyIO->master_wallet);
        } catch (\Exception $e) {
            return [
                'status'  => 2,
                'message' => $e->getMessage(),
            ];
        }

        $transactionList = $response;
        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id])->select('txid')->column();

        if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
            if ($task->account) {
                // Если указан кошелек то ищу его среди всех последних 20 транзакций.
                foreach ($transactionList as $t) {
                    // Если это перевод эфира, то есть контракт не указан
                    if ($t['contractAddress'] == '') {

                        // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                        if (in_array(strtolower($t['hash']), $transactionListSuccess)) continue;

                        if (strtolower($t['from']) == strtolower($task->account)) {
                            $value = $t['value'];
                            $c = Currency::findOne($currencyIO->currency_int_id);
                            if ($this->decimals > $c->decimals) {
                                $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                            }
                            if ($this->decimals < $c->decimals) {
                                $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                            }

                            return [
                                'status'          => 1,
                                'value'           => $value,
                                'transactionHash' => strtolower($t['hash']),
                            ];
                        }
                    }
                }
            }
        }
        if ($task->type_id == TaskInput::TYPE_TRANSACTION) {
            foreach ($transactionList as $t) {
                // Если это перевод эфира, то есть контракт не указан
                if ($t['contractAddress'] == '') {

                    // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                    if (in_array(strtolower($t['hash']), $transactionListSuccess)) continue;

                    if (strtolower($t['hash']) == strtolower($task->txid)) {
                        $value = $t['value'];
                        $c = Currency::findOne($currencyIO->currency_int_id);
                        if ($this->decimals > $c->decimals) {
                            $value = bcdiv($value, bcpow(10, $this->decimals - $c->decimals));
                        }
                        if ($this->decimals < $c->decimals) {
                            $value = bcmul($value, bcpow(10, $c->decimals - $this->decimals));
                        }
                        if ($value == $task->amount_user) {
                            return [
                                'status'          => 1,
                                'value'           => $value,
                                'transactionHash' => strtolower($t['hash']),
                            ];
                        }
                    }
                }
            }
        }

        return ['status' => 0];
    }


    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, '0x')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с 0x'),
            ];
        }

        return [
            'status' => true,
        ];
    }

    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}