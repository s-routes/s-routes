<?php

namespace avatar\services\currency;

use common\models\piramida\Currency;
use common\models\TaskInput;
use cs\services\VarDumper;
use yii\base\BaseObject;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * https://developers.tron.network/reference#account-info-by-address
 *
 * Class Trx
 * @package avatar\services\currency
 */
class Trx extends BaseObject implements CurrencyInterface
{
    /**
     * @param TaskInput $task
     * @param \common\models\CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     * @return array|int[]
     */
    public function check($task, $currencyIO, $CurrencyIoModification)
    {
        $url = 'https://api.trongrid.io/v1/accounts';
        $client = new Client(['baseUrl' => $url]);

        // Делаю запрос на полученные монеты
        try {
            $response = $client->get($currencyIO->master_wallet . '/transactions', [
            ])->send();
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $data = Json::decode($response->content);

        if ($data['success']) {
            $transactionList = $data['data'];

            /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
            $transactionListSuccess = TaskInput::find()->where(['status' => 1, 'currency_io_id' => $currencyIO->id])->select('txid')->column();

            if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
                if ($task->account) {
                    $c = 0;
                    // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
                    foreach ($transactionList as $t) {
                        $c++;

                        // Если это перевод трона?
                        if ($t['raw_data']['contract'][0]['type'] == 'TransferContract') {
                            // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                            $txid = $t['txID'];
                            if (in_array($txid, $transactionListSuccess)) continue;

                            // Адрес мастер кошелька и адрес получателя в транзакции совпадают? (пользователь на наш кошелек кидает деньги?)
                            if (StringHelper::startsWith(self::hexToBase58($currencyIO->master_wallet), strtoupper($t['raw_data']['contract'][0]['parameter']['value']['to_address']))) {

                                // Адрес отправителя в транзакции и кошелек который указал пользователь в заявке совпадают?
                                if (StringHelper::startsWith(self::hexToBase58($task->account), strtoupper($t['raw_data']['contract'][0]['parameter']['value']['owner_address']))) {
                                    $value = $t['raw_data']['contract'][0]['parameter']['value']['amount'];

                                    return [
                                        'status'          => 1,
                                        'value'           => $value,
                                        'transactionHash' => $txid,
                                    ];
                                }

                            }
                        }
                    }
                }
            }
            if ($task->type_id == TaskInput::TYPE_TRANSACTION) {
                foreach ($transactionList as $t) {
                    // Если это перевод трона?
                    if ($t['raw_data']['contract'][0]['type'] == 'TransferContract') {
                        // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                        $txid = $t['txID'];
                        if (in_array($txid, $transactionListSuccess)) continue;
                        if ($txid == strtolower($task->txid)) {
                            // Адрес мастер кошелька и адрес получателя в транзакции совпадают? (пользователь на наш кошелек кидает деньги?)
                            if (StringHelper::startsWith(self::hexToBase58($currencyIO->master_wallet), strtoupper($t['raw_data']['contract'][0]['parameter']['value']['to_address']))) {
                                $value = $t['raw_data']['contract'][0]['parameter']['value']['amount'];

                                if ($value == $task->amount_user) {
                                    return [
                                        'status'          => 1,
                                        'value'           => $value,
                                        'transactionHash' => $txid,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        return ['status' => 0];
    }

    public static function hexToBase58($address)
    {
        $address = \frontend\services\BaseConvert::base58_hex($address);

        return $address;
    }

    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error'  => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address)
    {
        if (!StringHelper::startsWith($address, 'T')) {
            return [
                'status' => false,
                'error'  => \Yii::t('c.TB9ZAPoWIy', 'Кошелек должен начинаться с T'),
            ];
        }

        return [
            'status' => true,
        ];
    }

    /**
     * @param \common\models\piramida\Currency  $cInt
     * @param \common\models\avatar\Currency    $cExt
     * @param                                   $options
     *
     * @return array
     * [
     *  'comission_options' = массив для выбора комиссии / null
     *  'is_show_comission' = bool показывать строку с комиссией
     * ]
     */
    public function out_algoritm($cInt, $cExt, $options = [])
    {
        $comission = $options['comission_out'];
        $c_user = bcmul($comission, pow(10, $cInt->decimals));

        \Yii::$app->view->registerJs(<<<JS
$('#taskoutput-amount').on('input', function(e) {
    var val = parseFloat($(this).val());
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );
    $('#sum_exit').val(val - parseFloat({$comission}));
    $('#taskoutput-comission_user').val({$c_user});
});

$('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}' );

$('#sum_exit').on('input', function(e) {
    var val = $(this).val();
    var x = parseFloat(val) + parseFloat({$comission});
    $('#comission_blockchain').html({$comission} + ' ' + '{$cInt->code}');
    $('#taskoutput-amount').val(x);
    $('#taskoutput-comission_user').val({$c_user});
});

JS
        );

        return [
            'is_show_comission' => true
        ];
    }
}