<?php
namespace avatar\services\currency;


use common\models\CurrencyIO;
use common\models\TaskInput;

interface CurrencyInterface
{
    /**
     * @param TaskInput $task
     * @param CurrencyIO $currencyIO
     * @param \common\models\CurrencyIoModification $CurrencyIoModification
     *
     * @return array
     *
     * // не найдено зачисление
     * [
     *  'status' => 0
     * ]
     *
     * // найдено зачисление
     * [
     *  'status' => 1
     *  'value' => 1        //кол-во копеек сколько надо зачислить
     *  'transactionHash' => 'sdsdadasd'
     * ]
     *
     * //критическая ошибка
     * [
     *  'status' => 2
     *  'message' => string
     * ]
     */
    public function check($task, $currencyIO, $CurrencyIoModification);

    /**
     * @param string $address
     * @return array [
     *  'status' => bool
     *  'error' => string - не обязательный, присутствует только если `status` = true
     * ]
     */
    public function validateAddress($address);

    /**
     * @param \common\models\piramida\Currency  $cInt
     * @param \common\models\avatar\Currency    $cExt
     * @param array                             $options
     * [
     * 'comission_out' => string/double
     * ]
     *
     * @return array
     * [
     *  'comission_options' = массив для выбора комиссии / null
     *  'is_show_comission' = bool показывать строку с комиссией
     * ]
     */
    public function out_algoritm($cInt, $cExt, $options = []);

}