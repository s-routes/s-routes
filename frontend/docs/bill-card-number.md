Номер Карты
===========


Что меня останавливает от того чтобы номера карт сделать?
+ 1. нужно сделать номер карты в в БД
2. нужно сделать дизайн карты (1 сторона уже есть, 2 нужна!!!)
3. получение номера на уже существующий счет
4. выписывание нового номера при создании карты
5. Методология печати
6. Сделать активацию карты
7. Алгоритм номера карты (с контрольной цифрой и префиксом банка)

Решение
3. это можно сделать в кабинете отдельной кнопкой "Получить номер карты"
алгоритм действий
- нажимаю кнопку "Получить номер" (нажать можно только 10 раз в день)
- на странице сразу генерируется номер карты случайный
- Аватар нажимает кнопку "получить"
- номер привязывается к счету
- Аватар заходит на "счета"
- нажимает карты и там может сделать себе карту с номером

нажимаю сохранить
POST
сохраниение, установка flash, показывается "Сохранено успешно".


4. Выписываение нового номера карты
Что здесь?
здесь может быть два варианта
1. Создавать карту с открытым номером (пользователь сам его генерирует)
2. Создавать карту (тираж) с уже сгенерированным кодом
оба варианта нужны
Значит делаем оба сразу
решение следующее
на странице генерирование QR кодов делаю в модальном окне галочку "Генерировать Номер карты?"
0 - Номер карты не будет сгенерирован, будет сгенерирован только счет
1 - Номер будет сгенерирован и будет привязан к карте

6. Сделать активацию карты
Это можно и легко
нужно добавить условие проверки
и поиск по номеру карты

