Авторизация
===========

# Соц сети

При авторизации через соц сеть используются следющие данные:

Callback

## VK

/auth/auth?authclient?vk

```
<!-- Put this script tag to the <head> of your page -->
<script type="text/javascript" src="//vk.com/js/api/openapi.js?136"></script>

<script type="text/javascript">
  VK.init({apiId: 5816392});
</script>

<!-- Put this div tag to the place, where Auth block will be -->
<div id="vk_auth"></div>
<script type="text/javascript">
VK.Widgets.Auth("vk_auth", {width: "200px", authUrl: '/auth/auth?authclient?vk'});
</script>
```


Документация по виджету
https://vk.com/dev/widget_auth


https://vk.com/dev/widget_auth

## FaceBook


