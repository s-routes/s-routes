Кошелек
=======

https://paper.dropbox.com/doc/9yU6VKD5hpQ2XfRyTSKtj


На Инструменте Вознесения можно подключить внешний адрес кошелька BitCoin. При этом отображается только кол-во денег в кошельке.
На этот кошелек будут послупать бонусы от участия в партнерской программе.

Также можно создать свой внутренний кошелек, который обладает всеми возможностями настоящего кошелька.
Можно положить в него деньги и оплатить с него.
К кошельку можно заказать карту Аватара который привязан к вашему кошельку и расплачиваться в магазинах и принимать на кошелек деньги.

# хост для API

'https://www.galaxysss.com/api'
Ответ JSON:
```
[
    'status' => bool
    'data'   => не обязательный
    если ошибки то 
    [
        'id'   => int,
        'data' => mixed
    ]
]
```

# Биткойн кошелек

в настройках
- изменить пин код
- местная валюта
- множитель биткойна
    BTC/mBTC
- цифр после запятой (в виде ползунка)
- безопасность
    - смс подтверждение
    - email подтверждение
- обратная связь
- о приложении
- выйти (после этого остображается Логин(ПИН)/Регистрация)
 
если человек платит с телефона и включена СМС подтверждение, то на телефон приходит подтверждение 
и ее нужно найти сразу в телефоне и прочитать на нужный код

# Оплата при помощи телефона:
`/pay`

- address - string
- amount - double с точкой

`/api/pay-sms`
- sms - string

# Получить деньги:
`/get`

ответ:

- address - string
генерируется код и показывается

# Регистрация

нажимает зарегистрироваться
выдает 
почта
пароль

отправляет на 
`/register`
- email
- password

Потом следующая страница
Ввод пин кода (два раза)
`/api/register-pin`
- pin - string(4) только цифры

# Логин

При входе в приложение еслиуже был вход, то выводится логин с пином
отправляет на 
/login
- pin - string(4) только цифры

Желательно сделать вход по пальцу (отпечатку на iPhone)

# Забрать деньги с карты Аватара c подтверждением смс
`/get-from-card`
- address
- amount

потом выводится ввод пин кода
`/get-from-card-pin`
- pin - string(4) только цифры

# обратная связь
`/feedback`
- message - string


# Функция "Отправить деньги другу по СМС"
отправляется СМС другу (задействовать записную книжку в телефоне). У клиента резервируются деньги
после этого человеку приходит СМС с ссылкой для регистрации, человек регистрируется и получает на счету деньги.

# Главный экран
`/main`
без параметров

выход
- transactions - array
```
[
    'transactions' => [
        [
            'direction' => int,
            'amount'    => double,
            'id'        => string,
            'fee'       => double,
            'date'      => int,
        ],
        // ...
    ],
    'balance' => [
        'confirmed'     => double
        'unconfirmed' => double
    ]
]
```






# Получение курса

/rate

