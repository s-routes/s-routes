<?php
/**
 * BaseController
 */

namespace avatar\base;

use Yii;
use yii\base\Response;
use yii\db\Query;
use yii\helpers\VarDumper;
use yii\log\Logger;
use yii\web\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

class BaseAction extends \yii\base\Action
{

    /**
     * Возвращает стандартный ответ JSON при положительном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param mixed $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonSuccess($data = null)
    {
        $ret = [
            'success' => true,
        ];
        if (!is_null($data)) {
            $ret['data'] = $data;
        };

        return self::json($ret);
    }
    
    /**
     * Возвращает стандартный ответ JSON при отрицательном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param mixed $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonError($data = null)
    {
        if (is_null($data)) $data = '';

        return self::json([
            'success' => false,
            'data'    => $data,
        ]);
    }

    /**
     * Возвращает стандартный ответ JSON при отрицательном срабатывании
     * https://redmine.suffra.com/projects/suffra/wiki/Стандартный_ответ_JSON
     *
     * @param integer $id   идентификатор ошибки
     * @param mixed   $data [optional] возвращаемые данные
     *
     * @return \yii\web\Response json
     */
    public static function jsonErrorId($id, $data = null)
    {
        $return = [
            'id' => $id,
        ];
        if (!is_null($data)) $return['data'] = $data;

        return self::jsonError($return);
    }

    /**
     * Закодировать в JSON
     *
     * @return \yii\web\Response json
     * */
    public static function json($array)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->data = $array;

        return Yii::$app->response;
    }


    /**
     * Возвращает переменную из REQUEST
     *
     * @param string $name    имя переменной
     * @param mixed  $default значние по умолчанию [optional]
     *
     * @return string|null
     * Если такой переменной нет, то будет возвращено null
     */
    public static function getParam($name, $default = null)
    {
        $vGet = \Yii::$app->request->get($name);
        $vPost = \Yii::$app->request->post($name);
        $value = (is_null($vGet)) ? $vPost : $vGet;

        return (is_null($value)) ? $default : $value;
    }
}