<?php

/** @var  \common\models\TaskOutput $task */

use common\models\CurrencyIO;

$cExt = \common\models\avatar\Currency::findOne($task->currency_id);
$cio = CurrencyIO::findOne($task->currency_io_id);
$value = \common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, $cio->currency_int_id);
$text = 'Вывод ' . $value . ' ' . $cExt->code . ' по заявке #' . $task->id . ' инициализирован';

?>

<p><?= $text ?></p>

