<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 14.03.2017
 * Time: 11:22
 */

/**
 * @var $deal \common\models\exchange\Deal
 */

$url = \yii\helpers\Url::to(['cabinet-exchange/deal-action', 'id' => $deal->id], true);
?>

<p>Примите сделку в течение 20 минут сделка автоматически будет закрыта.</p>
<p>Ссылка: <a href="<?= $url ?>"><?= $url ?></a></p>
