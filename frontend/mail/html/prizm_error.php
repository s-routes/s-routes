<?php

/** @var int $blockInt */
/** @var int $blockExt */
?>

<p>Наша нода рассинхронизировалась.</p>
<p>На нашей ноде высота последнего блока равна <?= $blockInt ?></p>
<p>На внешней (http://blockchain.prizm.space) ноде высота последнего блока равна <?= $blockExt ?></p>
