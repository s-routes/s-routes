// 0.5.8-c8a2
// Enable optimization 200
pragma solidity ^0.5.0;

import "./ERC20.sol";
import "./ERC20Detailed.sol";

/**
 * @title SimpleToken
 * @dev Very simple ERC20 Token example, where all tokens are pre-assigned to the creator.
 * Note they can later distribute these tokens as they wish using `transfer` and other
 * `ERC20` functions.
 */
contract Neiron is ERC20, ERC20Detailed {



    /**
     * @dev Constructor that gives msg.sender all of existing tokens.
     */
    constructor () public ERC20Detailed("Neiron", "NEIRO", 4) {
        owner = msg.sender;
        _mint(msg.sender, 270000000 * (10 ** uint256(decimals())));
    }
}