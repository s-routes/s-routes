
    /* Уничтожает токены на счете владельца контракта */
    function burn(uint256 _value) onlyowner returns (bool success)
    {
        if (balances[msg.sender] < _value) {
            return false;
        }
        totalSupply -= _value;
        balances[msg.sender] -= _value;
        return true;
    }

