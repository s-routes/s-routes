pragma solidity ^0.4.8;

interface token {
    function transfer(address receiver, uint amount);
}

contract USDOracle {
    function WEI() constant returns (uint);
    function USD() constant returns (uint);
}

contract Crowdsale {
    address owner;
    uint deadline;
    uint public amountRaised;
    uint public fundingGoal;
    uint public price;
    uint public ethereumPrice;
    token public tokenReward;
    address public walletOut1;
    address public walletOut2;
    bool crowdsaleClosed = false;

    event GoalReached(address recipient, uint totalAmountRaised);
    event FundTransfer(address backer, uint amount, bool isContribution);

    function Crowdsale() {
        uint durationInMinutes = 20000;
        uint fundingGoalAmount = 1000000000000000000;
        uint usdCentCostOfEachToken = 15;
        address addressOfTokenUsedAsReward = 0x82eFFcf00c56CacfC2AAf3EB99B7019Ef708fD60;
        address walletOut1 = 0x83e93837028d7fe777881e611b378675c92b9f25;
        address walletOut2 = 0x4eb02a723f5f8655b7c855ef4909236f22c6b868;

        deadline = now + durationInMinutes * 1 minutes;
        fundingGoal = fundingGoalAmount;
        price = usdCentCostOfEachToken;
        tokenReward = token(addressOfTokenUsedAsReward);
        ethereumPrice = 900;
    }

    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    modifier onlyowner() {
        if (msg.sender==owner) _;
    }

    modifier afterDeadline() { if (now >= deadline) _; }

    function () payable {
        uint256 amount = msg.value;
        amountRaised += amount;
        uint256 amountOut1 = amount / 2;
        uint256 amountOut2 = amount - amountOut1;

        walletOut1.transfer(amountOut1);
        walletOut2.transfer(amountOut2);

        uint256 amountWei = amount;
        uint priceUsdCentEth = ethereumPrice * 100;
        uint priceUsdCentAvr = price;
        uint256 amountAvrAtom = ((amountWei * priceUsdCentEth) / priceUsdCentAvr) / 10000000000;

        tokenReward.transfer(msg.sender, amountAvrAtom);
        FundTransfer(msg.sender, amount, true);
    }

    function updateETHPrice() payable {
        // USDOracle oracle = USDOracle(0x1c68f4f35ac5239650333d291e6ce7f841149937);
        // ethereumPrice = oracle.USD();
        ethereumPrice = 900;
    }

    function setWalletOut1(address wallet) {
        walletOut1 = wallet;
    }

    function setWalletOut2(address wallet) {
        walletOut2 = wallet;
    }

    // uint usdCentCostOfEachToken - цена в центах
    function setPrice(uint usdCentCostOfEachToken) {
        price = usdCentCostOfEachToken;
    }
}
