pragma solidity ^0.4.8;

interface token {
    function transfer(address receiver, uint amount);
}

contract USDOracle {
    function WEI() constant returns (uint);
    function USD() constant returns (uint);
}

contract Crowdsale {
    address owner;
    uint public amountRaised;
    uint public price;
    uint public ethereumPrice;
    token public tokenReward;
    address public walletOut1;
    address public walletOut2;
    bool crowdsaleClosed = false;

    event FundTransfer(address backer, uint amount, bool isContribution);

    function Crowdsale() {
        walletOut1 = 0x83e93837028d7fe777881e611b378675c92b9f25;
        walletOut2 = 0x4eb02a723f5f8655b7c855ef4909236f22c6b868;

        price = 15;
        tokenReward = token(0x82eFFcf00c56CacfC2AAf3EB99B7019Ef708fD60);
        ethereumPrice = 900;
    }

    function changeOwner(address newOwner) onlyowner {
        owner = newOwner;
    }

    modifier onlyowner() {
        if (msg.sender==owner) _;
    }

    function () payable {
        uint256 amount = msg.value;
        amountRaised += amount;
        uint256 amountOut1 = amount / 2;
        uint256 amountOut2 = amount - amountOut1;

        walletOut1.transfer(amountOut1);
        walletOut2.transfer(amountOut2);

        uint256 amountWei = amount;
        uint priceUsdCentEth = ethereumPrice * 100;
        uint priceUsdCentAvr = price;
        uint256 amountAvrAtom = ((amountWei * priceUsdCentEth) / priceUsdCentAvr) / 10000000000;

        tokenReward.transfer(msg.sender, amountAvrAtom);
        FundTransfer(msg.sender, amount, true);
    }

    function setWalletOut1(address wallet) {
        walletOut1 = wallet;
    }

    function setWalletOut2(address wallet) {
        walletOut2 = wallet;
    }

    function sendAVR(address wallet, uint256 amountAvrAtom) onlyowner
    {
        tokenReward.transfer(wallet, amountAvrAtom);
    }

    // uint usdCentCostOfEachToken - цена в центах
    function setPrice(uint usdCentCostOfEachToken) {
        price = usdCentCostOfEachToken;
    }
}
