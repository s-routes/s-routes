pragma solidity ^0.4.8;

/*
AvatarNetwork Copyright

https://avatarnetwork.io
*/

contract Owned {

    address owner;

    function Owned() {
        owner = msg.sender;
    }

    function changeOwner(address newOwner) onlyOwner {
        owner = newOwner;
    }

    modifier onlyOwner() {
        if (msg.sender==owner) _;
    }
}

contract Token is Owned {
    uint256 public totalSupply;
    function balanceOf(address _owner) constant returns (uint256 balance);
    function transfer(address _to, uint256 _value) returns (bool success);
    function transferFrom(address _from, address _to, uint256 _value) returns (bool success);
    function approve(address _spender, uint256 _value) returns (bool success);
    function allowance(address _owner, address _spender) constant returns (uint256 remaining);
    event Transfer(address indexed _from, address indexed _to, uint256 _value);
    event Approval(address indexed _owner, address indexed _spender, uint256 _value);
    event Err(uint256 _value);
}

contract ERC20Token is Token {

    function transfer(address _to, uint256 _value) returns (bool success) {

        if (balances[msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[msg.sender] -= _value;
            balances[_to] += _value;
            Transfer(msg.sender, _to, _value);
            return true;
        } else { return false; }
    }

    function transferFrom(address _from, address _to, uint256 _value) returns (bool success) {

        if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && balances[_to] + _value > balances[_to]) {
            balances[_to] += _value;
            balances[_from] -= _value;
            allowed[_from][msg.sender] -= _value;
            Transfer(_from, _to, _value);
            return true;
        } else { return false; }
    }

    function balanceOf(address _owner) constant returns (uint256 balance) {
        return balances[_owner];
    }

    function approve(address _spender, uint256 _value) returns (bool success) {
        allowed[msg.sender][_spender] = _value;
        Approval(msg.sender, _spender, _value);
        return true;
    }

    function allowance(address _owner, address _spender) constant returns (uint256 remaining) {
        return allowed[_owner][_spender];
    }

    mapping (address => uint256) balances;
    mapping (address => mapping (address => uint256)) allowed;
}

contract RevvaCoin is ERC20Token {

    bool public isTokenSale = true;
    uint256 public price;
    uint256 public limit;

    address walletOut = 0xCd2d3F664bF3044922110C07967fF40c9971AeE7;

    function getWalletOut() constant returns (address _to) {
        return walletOut;
    }

    function () external payable  {
        if (isTokenSale == false) {
            throw;
        }

        uint256 tokenAmount = (msg.value  * 100000000) / price;

        if (balances[owner] >= tokenAmount && balances[msg.sender] + tokenAmount > balances[msg.sender]) {
            if (balances[owner] - tokenAmount < limit) {
                throw;
            }
            balances[owner] -= tokenAmount;
            balances[msg.sender] += tokenAmount;
            Transfer(owner, msg.sender, tokenAmount);
        } else {
            throw;
        }
    }

    function stopSale() onlyOwner {
        isTokenSale = false;
    }

    function startSale() onlyOwner {
        isTokenSale = true;
    }

    function setPrice(uint256 newPrice) onlyOwner {
        price = newPrice;
    }

    function setLimit(uint256 newLimit) onlyOwner {
        limit = newLimit;
    }

    function setWallet(address _to) onlyOwner {
        walletOut = _to;
    }

    function sendFund() onlyOwner {
        walletOut.send(this.balance);
    }

    string public name;
    uint8 public decimals;
    string public symbol;
    string public version = '1.0';

    function RevvaCoin() {
        totalSupply = 100000000 * 100000000;
        balances[msg.sender] = totalSupply;
        name = 'RevvaCoin';
        decimals = 8;
        symbol = 'REVVA';
        price = 714285714285714;
        limit = totalSupply - 10000000000000;
    }


    /* Добавляет на счет токенов */
    function add(uint256 _value) onlyOwner returns (bool success)
    {
        if (balances[msg.sender] + _value <= balances[msg.sender]) {
            return false;
        }
        totalSupply += _value;
        balances[msg.sender] += _value;

        return true;
    }

    /* Уничтожает токены на счете владельца контракта */
    function burn(uint256 _value) onlyOwner  returns (bool success)
    {
        if (balances[msg.sender] < _value) {
            return false;
        }
        totalSupply -= _value;
        balances[msg.sender] -= _value;
        return true;
    }
}

MainNet

slide nothing glove unfold buddy depend offer coast among meat bid eyebrow
0xd33D102C06Cd1bCbC8d156c5432b5b56426643eb


606060409081526004805460ff1916600117905560078054600160a060020a03191673cd2d3f664bf3044922110c07967ff40c9971aee71790558051908101604052600381527f312e3000000000000000000000000000000000000000000000000000000000006020820152600b90805161007e92916020019061017d565b50341561008a57600080fd5b60008054600160a060020a03191633600160a060020a03169081178255662386f26fc1000060018190559082526002602052604091829020558051908101604052600981527f5265767661436f696e00000000000000000000000000000000000000000000006020820152600890805161010892916020019061017d565b506009805460ff1916600817905560408051908101604052600581527f52455656410000000000000000000000000000000000000000000000000000006020820152600a90805161015d92916020019061017d565b50660289a39a44a4926005556001546509184e729fff1901600655610218565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106101be57805160ff19168380011785556101eb565b828001600101855582156101eb579182015b828111156101eb5782518255916020019190600101906101d0565b506101f79291506101fb565b5090565b61021591905b808211156101f75760008155600101610201565b90565b610bc6806102276000396000f3006060604052600436106101325763ffffffff7c010000000000000000000000000000000000000000000000000000000060003504166306fdde038114610246578063095ea7b3146102d05780631003e2d21461030657806318160ddd1461031c57806323b872dd1461034157806327ea6f2b14610369578063313ce5671461038157806342966c68146103aa57806345d58a4e146103c057806354fd4d50146103ef57806370a08231146104025780637768dec01461042157806391b7f5ed1461043457806395d89b411461044a578063a035b1fe1461045d578063a4d66daf14610470578063a6f9dae114610483578063a9059cbb146104a2578063b66a0e5d146104c4578063dd0cf15d146104d7578063dd62ed3e146104ea578063deaa59df1461050f578063e36b0b371461052e575b60045460009060ff16151561014657600080fd5b600554346305f5e1000281151561015957fe5b60008054600160a060020a031681526002602052604090205491900491508190108015906101a05750600160a060020a033316600090815260026020526040902054818101115b1561023e5760065460008054600160a060020a031681526002602052604090205482900310156101cf57600080fd5b60008054600160a060020a03908116825260026020526040808320805485900390553382168084528184208054860190559254909116907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9084905190815260200160405180910390a3610243565b600080fd5b50005b341561025157600080fd5b610259610541565b60405160208082528190810183818151815260200191508051906020019080838360005b8381101561029557808201518382015260200161027d565b50505050905090810190601f1680156102c25780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34156102db57600080fd5b6102f2600160a060020a03600435166024356105df565b604051901515815260200160405180910390f35b341561031157600080fd5b6102f260043561064c565b341561032757600080fd5b61032f6106ba565b60405190815260200160405180910390f35b341561034c57600080fd5b6102f2600160a060020a03600435811690602435166044356106c0565b341561037457600080fd5b61037f6004356107d1565b005b341561038c57600080fd5b6103946107f1565b60405160ff909116815260200160405180910390f35b34156103b557600080fd5b6102f26004356107fa565b34156103cb57600080fd5b6103d3610869565b604051600160a060020a03909116815260200160405180910390f35b34156103fa57600080fd5b610259610878565b341561040d57600080fd5b61032f600160a060020a03600435166108e3565b341561042c57600080fd5b6102f26108fe565b341561043f57600080fd5b61037f600435610907565b341561045557600080fd5b610259610923565b341561046857600080fd5b61032f61098e565b341561047b57600080fd5b61032f610994565b341561048e57600080fd5b61037f600160a060020a036004351661099a565b34156104ad57600080fd5b6102f2600160a060020a03600435166024356109de565b34156104cf57600080fd5b61037f610a9a565b34156104e257600080fd5b61037f610ac1565b34156104f557600080fd5b61032f600160a060020a0360043581169060243516610b08565b341561051a57600080fd5b61037f600160a060020a0360043516610b33565b341561053957600080fd5b61037f610b77565b60088054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105d75780601f106105ac576101008083540402835291602001916105d7565b820191906000526020600020905b8154815290600101906020018083116105ba57829003601f168201915b505050505081565b600160a060020a03338116600081815260036020908152604080832094871680845294909152808220859055909291907f8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b9259085905190815260200160405180910390a35060015b92915050565b6000805433600160a060020a03908116911614156106b557600160a060020a0333166000908152600260205260409020548281011161068d575060006106b5565b506001805482018155600160a060020a03331660009081526002602052604090208054830190555b919050565b60015481565b600160a060020a0383166000908152600260205260408120548290108015906107105750600160a060020a0380851660009081526003602090815260408083203390941683529290522054829010155b80156107355750600160a060020a038316600090815260026020526040902054828101115b156107c657600160a060020a03808416600081815260026020908152604080832080548801905588851680845281842080548990039055600383528184203390961684529490915290819020805486900390559091907fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a35060016107ca565b5060005b9392505050565b60005433600160a060020a03908116911614156107ee5760068190555b50565b60095460ff1681565b6000805433600160a060020a03908116911614156106b557600160a060020a0333166000908152600260205260409020548290101561083b575060006106b5565b50600180548290038155600160a060020a033316600090815260026020526040902080548390039055919050565b600754600160a060020a031690565b600b8054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105d75780601f106105ac576101008083540402835291602001916105d7565b600160a060020a031660009081526002602052604090205490565b60045460ff1681565b60005433600160a060020a03908116911614156107ee57600555565b600a8054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105d75780601f106105ac576101008083540402835291602001916105d7565b60055481565b60065481565b60005433600160a060020a03908116911614156107ee5760008054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff1990911617905550565b600160a060020a033316600090815260026020526040812054829010801590610a205750600160a060020a038316600090815260026020526040902054828101115b15610a9257600160a060020a033381166000818152600260205260408082208054879003905592861680825290839020805486019055917fddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef9085905190815260200160405180910390a3506001610646565b506000610646565b60005433600160a060020a0390811691161415610abf576004805460ff191660011790555b565b60005433600160a060020a0390811691161415610abf57600754600160a060020a039081169030163180156108fc0290604051600060405180830381858888f15050505050565b600160a060020a03918216600090815260036020908152604080832093909416825291909152205490565b60005433600160a060020a03908116911614156107ee5760078054600160a060020a03831673ffffffffffffffffffffffffffffffffffffffff1990911617905550565b60005433600160a060020a0390811691161415610abf576004805460ff191690555600a165627a7a72305820feb67256ed3f17a47a095c75d00a4400de6def6cacf531e39047606b8f99a09d0029
[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"add","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"newLimit","type":"uint256"}],"name":"setLimit","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_value","type":"uint256"}],"name":"burn","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"getWalletOut","outputs":[{"name":"_to","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"}],"name":"balanceOf","outputs":[{"name":"balance","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"isTokenSale","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newPrice","type":"uint256"}],"name":"setPrice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"price","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"limit","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"success","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"startSale","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"sendFund","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"}],"name":"setWallet","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[],"name":"stopSale","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_from","type":"address"},{"indexed":true,"name":"_to","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Transfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_owner","type":"address"},{"indexed":true,"name":"_spender","type":"address"},{"indexed":false,"name":"_value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"_value","type":"uint256"}],"name":"Err","type":"event"}]

TestNet
5ccfe8d30757c63151f3cd7f46e110b0b7ecd6d4c1f50fe96aeea4284a00d108 pk
0xc4bC58CF3D31E9764bC297028F54D2FB8948354F address owner

0x92a7f303ae56ee36424e94ba9d0a50abd4c2bdf6bceda1c9a1e2aa6b3a18c9d6


