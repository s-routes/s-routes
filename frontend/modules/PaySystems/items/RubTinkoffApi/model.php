<?php

namespace avatar\modules\PaySystems\items\RubSberApi;

use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  string */
    public $operator;

    public $api;

    /** @var  string  */
    public $password;

    public $secret;

    public function rules()
    {
        return [
            ['operator', 'string'],

            ['api', 'string'],

            ['password', 'string'],

            ['secret', 'string'],
        ];
    }

    /**
     * @param string $config JSON
     */
    public function setData($config)
    {
        if (Application::isEmpty($config)) {
            $data = Json::decode('{"operator":"","api":"","password":"","secret":""}');
        } else {
            $data = Json::decode($config);
        }
        $this->operator = $data['operator'];
        $this->api = $data['api'];
        $this->password = $data['password'];
        $this->secret = ArrayHelper::getValue($data, 'secret', '');
    }

    /**
     * @return string JSON
     */
    public function save()
    {
        $data = [
            'operator' => $this->operator,
            'api'      => $this->api,
            'password' => $this->password,
            'secret'   => $this->secret,
        ];

        return Json::encode($data);
    }

    public function attributeLabels()
    {
        return [
            'operator' => 'operator',
            'api'      => 'api',
            'password' => 'password',
            'secret'   => 'secret для симметричного шифрования',
        ];
    }

}