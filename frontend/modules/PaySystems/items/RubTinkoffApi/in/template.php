<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View                      $this */
/** @var \common\models\BillingMain         $billing */
/** @var string                             $destinationAddress */
/** @var \common\models\PaymentSberKassa    $payment1 */


use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$config = \yii\helpers\Json::decode($destinationAddress);

$href = 'https://3dsec.sberbank.ru/payment/merchants/sbersafe/payment_ru.html?mdOrder=' . $payment1->payment_id;
$this->registerJs(<<<JS

window.location = '{$href}';

JS
)
?>