<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace avatar\modules\PaySystems\items\RubTinkoffApi\in;

use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\PaySystem;
use common\models\PaySystemConfig;
use common\services\SberKassa;
use cs\Application;
use Yii;
use cs\web\Exception;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;

class Base extends \common\models\piramida\WalletSource\Base implements \common\models\piramida\WalletSourceInterface
{
    /** @var  mixed транзакция от платежной системы */
    public $transaction;

    /** @var  string тип операци от яндекса 'PC' 'AC' */
    public $paymentType;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getTypeId() {}

    /**
     * Выдает цену с учетом оплаты комиссии
     *
     * @param float $price цена которую хотим получить от клиента через прием денег через эту платежную систему
     *
     * @return float цена для оплаты в платежной соистеме
     */
    public function getPriceWithTax($price)
    {
        $d = $price / (1 - ($this->getTax() / 100));
        $d = explode('.', $d);
        if (count($d) > 1) {
            return (float)$d[0] . '.' . substr($d[1], 0, 2);
        }
        return (float)$d[0];
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param BillingMain $billing
     * @param string $description
     * @param string $destinationAddress - счет поучателя
     *
     * @param null | array $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     * - action - string - поле label в форме
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $options = null)
    {
        /** @var \common\services\SberKassa $client */
        $client = Yii::$app->SberKassa;

        /** @var \common\models\PaymentSberKassa $payment */
        $payment1 = \common\models\PaymentSberKassa::find()->where(['billing_id' => $billing->id])->one();
        if (is_null($payment1)) {
            $Payment = $client->createPayment($billing, $description, $destinationName, ['successUrl' => $billing->success_url]);

            Yii::info($Payment, 'avatar\modules\PaySystems\items\RubSberApi\in\Base::getForm1');

            $payment1 = \common\models\PaymentSberKassa::add([
                'amount'      => $billing->sum_after,
                'billing_id'  => $billing->id,
                'payment_id'  => $Payment['orderId'],
                'currency_id' => 6,
            ]);
            Yii::info($payment1, 'avatar\modules\PaySystems\items\RubSberApi\in\Base::getForm2');
        } else {
//            $Payment = $client->getPaymentInfo($payment1->payment_id);
        }

        return \Yii::$app->view->renderFile('@avatar/modules/PaySystems/items/RubSberApi/in/template.php', [
            'billing'            => $billing,
            'destinationAddress' => $this->config,
            'payment1'           => $payment1,
        ]);
    }

    /**
     * POST
     * [
     * 'TerminalKey' => '1619680110399DEMO'
     * 'OrderId' => '22'
     * 'Success' => true
     * 'Status' => 'CONFIRMED'
     * 'PaymentId' => 548510414
     * 'ErrorCode' => '0'
     * 'Amount' => 10000
     * 'CardId' => 78052629
     * 'Pan' => '430000******0777'
     * 'ExpDate' => '1122'
     * 'Token' => 'f92999a286783a01c6da587d961b86b7b8ad9b978d342432fc57a7b20b2d1d48'
     * ]
     */
    public function success()
    {
        $rawBody = Yii::$app->request->rawBody;
        $rawBody = str_replace('\\"', '"', $rawBody);
        $rawBody = json_decode($rawBody);
        $rawBody = ArrayHelper::toArray($rawBody);

        Yii::info(Yii::$app->request->post(), 'avatar\modules\PaySystems\items\RubTinkoffApi\in\Base::success');
        $billing_id = $rawBody['OrderId'];
        $Status = $rawBody['Status'];

        if ($Status == 'CONFIRMED') {

            $client = $this->getProvider($billing_id);
            $params = $rawBody;
            $Token = $rawBody['Token'];
//            if (!$client->isCheckSum($params, $checksum)) {
//                throw new \Exception('Не верная контрольная сумма'.VarDumper::dumpAsString([$params, $checksum]));
//                return;
//            }

            // обобряю счет
            $bill = BillingMain::findOne($billing_id);
            $bill->successClient();
            Yii::info($bill, 'avatar\modules\PaySystems\items\RubSberApi\in\Base::success2');

            $bill->successShop();
            Yii::info('finish', 'avatar\modules\PaySystems\items\RubSberApi\in\Base::success3');

            return 'OK';
        }
    }

    /**
     * @param $billing_id
     * @return \common\services\TinkoffKassa
     */
    public function getProvider($billing_id)
    {
        /** @var \common\services\TinkoffKassa $client */
        $client = Yii::$app->TinkoffKassa;

        return $client;
    }

}