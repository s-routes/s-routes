<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */
/** @var \yii\web\View                  $this */
/** @var \common\models\BillingMain     $billing */
/** @var string                         $destinationAddress */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\services\Security;

$config = \yii\helpers\Json::decode($destinationAddress);


/** @var \common\models\PaymentSber $payment */
$payment = \common\models\PaymentSber::find()->where(['billing_id' => $billing->id])->one();
if (is_null($payment)) {
    $payment = new \common\models\PaymentSber([
        'value'      => $billing->sum_after,
        'billing_id' => $billing->id,
        'card'       => $config['card'],
        'key'        => Security::generateRandomString(3, Security::ALGORITM_NUMS) . '-' . Security::generateRandomString(3, Security::ALGORITM_NUMS),
    ]);
    $payment->save();
}

?>
<p>Шаг 1: Переведите <?= \Yii::$app->formatter->asDecimal($billing->sum_after/100, 2) ?> рублей на карту <b><?= $config['card'] ?></b></p>
<p>Шаг 2: В комментарии к платежу укажите ключ: <b><?= $payment['key'] ?></b></p>
<p>Шаг 3: Нажмите кнопку "Подтвердить".</p>
<p>Шаг 4: Средства поступят на ваш счет после верификации платежа.</p>
<p class="alert alert-danger">Обратите внимание, при зачислении средств банк возьмет комиссию в размере 3%.
    Сервис TopMate никаких комиссий не берет.</p>

<p>
    <button class="btn btn-success btn-lg" id="buttonSuccess">Подтвердить</button>
</p>
