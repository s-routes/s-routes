<?php

namespace avatar\modules\PaySystems\items\RubOzon\success;

use common\models\BillingMain;
use common\models\PaymentEthereum;
use common\models\PaymentSber;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;


/**
 *
 */
class model extends \yii\base\Model
{
    /** @var  BillingMain он должен передаваться при инициализации */
    public $billing;

    public $num;
    public $date;

    public function rules()
    {
        return [
            ['num', 'required'],
            ['num', 'string'],
            ['date', 'required'],
            ['date', 'string'],
        ];
    }

    /**
     * @return string возвращает данные для вывода в заказ что заказ оплачен и какие параменты клиент указал
     */
    public function action()
    {
        $payment = PaymentSber::findOne(['billing_id' => $this->billing->id]);
        $payment->num = $this->num;
        $payment->date = $this->date;
        $payment->save();

        return 'номер квитанции: ' . $this->num . ' Дата оплаты: ' . $this->date;
    }

    public function attributeLabels()
    {
        return [
            'num'  => 'Номер квитанции',
            'date' => 'Дата отправки',
        ];
    }

}