<?php

/** $this \yii\web\View  */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\modules\PaySystems\items\RubModulBank\model */


use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

?>

<?= $form->field($model, 'login') ?>
<?= $form->field($model, 'password')->widget('\avatar\widgets\SecretKey') ?>
