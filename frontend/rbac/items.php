<?php

/**
 * Правило хорошего тона создания ролей и правил (разрешений)
 * Разрешение создается с символом '2' в конце
 * а название роли повторяет его и содержит только одно разрешение, это простая роль
 *
 * составные роли отдельно пишутся в конце и состоят из других ролей
 */
return [

    // разрешения (permission)
    'permission_admin'         => [
        'type'        => 2,
        'description' => 'admin',
    ],
    'permission_content'         => [
        'type'        => 2,
        'description' => 'Редактирование контента сайта',
    ],
    'permission_developer' => [
        'type'        => 2,
        'description' => 'Архитектор матрицы',
    ],
    'permission_languages-admin' => [
        'type'        => 2,
        'description' => 'Администратор языков',
    ],
    'permission_languages' => [
        'type'        => 2,
        'description' => 'Редактор языков',
    ],
    'permission_arbitrator' => [
        'type'        => 2,
        'description' => 'Арбитр',
    ],
    'permission_buh' => [
        'type'        => 2,
        'description' => 'Бухгалтер',
    ],
    'permission_admin_chat' => [
        'type'        => 2,
        'description' => 'Админ чата',
    ],
    'permission_shop_buh' => [
        'type'        => 2,
        'description' => 'Бухгалтер магазина',
    ],
    'permission_shop_manager' => [
        'type'        => 2,
        'description' => 'Менеджер магазина',
    ],
    'permission_lot_admin' => [
        'type'        => 2,
        'description' => 'Админ лотереи',
    ],


    // простые роли
    'role_admin'               => [
        'type'        => 1,
        'description' => 'admin',
        'children'    => [
            'permission_admin',
        ],
    ],
    'role_buh'               => [
        'type'        => 1,
        'description' => 'Бухгалтер neiro-n.com',
        'children'    => [
            'permission_buh',
        ],
    ],
    'role_content'               => [
        'type'        => 1,
        'description' => 'Редактирование контента сайта',
        'children'    => [
            'permission_content',
        ],
    ],
    'role_languages-admin'       => [
        'type'        => 1,
        'description' => 'Администратор языков',
        'children'    => [
            'permission_languages-admin',
        ],
    ],
    'role_languages'       => [
        'type'        => 1,
        'description' => 'Редактор языков',
        'children'    => [
            'permission_languages',
        ],
    ],
    'role_developer'       => [
        'type'        => 1,
        'description' => 'Архитектор матрицы',
        'children'    => [
            'permission_developer',
        ],
    ],
    'role_arbitrator'       => [
        'type'        => 1,
        'description' => 'Арбитр',
        'children'    => [
            'permission_arbitrator',
        ],
    ],
    'role_admin_chat'       => [
        'type'        => 1,
        'description' => 'Админ чата',
        'children'    => [
            'permission_admin_chat',
        ],
    ],
    'role_shop_buh'       => [
        'type'        => 1,
        'description' => 'Бухгалтер topmate.one',
        'children'    => [
            'permission_shop_buh',
        ],
    ],
    'role_shop_manager'       => [
        'type'        => 1,
        'description' => 'Менеджер topmate.one',
        'children'    => [
            'permission_shop_manager',
        ],
    ],
    'role_lot_admin'       => [
        'type'        => 1,
        'description' => 'Админ лотереи ',
        'children'    => [
            'permission_lot_admin',
        ],
    ],
];
