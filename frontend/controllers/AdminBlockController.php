<?php

namespace avatar\controllers;

use avatar\controllers\actions\DefaultAjax;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminBlockController extends AdminBaseController
{
    public function actions()
    {
        return [
            'set' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\forms\AdminBlockAjax',
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new \avatar\models\forms\AdminBlock();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }
}
