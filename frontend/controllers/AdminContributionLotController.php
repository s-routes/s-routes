<?php

namespace avatar\controllers;

use avatar\models\forms\AdminContributionLot;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminContributionLotController extends AdminBaseController
{
    public function actionIndex()
    {
        $model = new AdminContributionLot();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render(['model' => $model]);
    }
}
