<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\ProjectGift;
use common\models\investment\Gift;
use common\models\investment\Project;
use common\models\MapBankomat;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class CabinetProjectsGiftsController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new ProjectGift();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = ProjectGift::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        ProjectGift::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
