<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\School;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class CabinetPrizmController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionOut()
    {
        $model = new \avatar\models\forms\TaskPrizmOutput();
        Event::on('\avatar\models\forms\TaskPrizmOutput', ActiveRecord::EVENT_BEFORE_INSERT, function ($e) {
            /** @var \avatar\models\forms\TaskPrizmOutput $sender */
            $sender = $e->sender;
            $a = $sender->amount * 100;
            $sender->amount = (int)$a;
        });

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->user_id = Yii::$app->user->id;
            $model->created_at = time();
            $ret = $model->save();
            if (!$ret) VarDumper::dump($model->errors);
            $model->id = $model::getDb()->lastInsertID;

            // Отправляю уведомление бухгалтеру
            $usersIds = Yii::$app->authManager->getUserIdsByRole('role_buh');
            $usersEmail = [];
            foreach ($usersIds as $id) {
                $usersEmail[] = UserAvatar::findOne($id)->email;
            }
            $task = TaskPrizmOutput::findOne($model->id);
            \common\services\Subscribe::sendArray($usersEmail, 'Новая заявка #' . $task->id,'task_out_new', [
                'task' => $task
            ]);

            // Устанавливаю переменную для отображения страницы в режиме "выполнено"
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }
    /**
     */
    public function actionOutIndex()
    {
        return $this->render([]);
    }

}
