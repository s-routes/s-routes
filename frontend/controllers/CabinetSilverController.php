<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\controllers\actions\DefaultAjax;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\CurrencyIO;
use common\models\HD;
use common\models\HDtown;
use common\models\school\School;
use common\models\shop\Product;
use common\models\TaskOutput;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetSilverController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'pay-rub' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\forms\CabinetSilverPayRub',
            ]
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     */
    public function actionStep1($id)
    {
        $model = new \avatar\models\forms\CabinetSilverStep1(['id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionStep2()
    {
        $model = new \avatar\models\forms\CabinetSilverStep2();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionStep3()
    {
        $model = new \avatar\models\forms\CabinetSilverStep3();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionStep4()
    {
        $model = new \avatar\models\forms\CabinetSilverStep4();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionStep5($id)
    {
        return $this->render(['id' => $id]);
    }

    /**
     */
    public function actionStep6($id)
    {
        $model = new \avatar\models\forms\CabinetSilverStep6(['id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'id'    => $id,
            'model' => $model,
        ]);
    }

    /**
     */
    public function actionItem($id)
    {
        $Product = Product::findOne($id);
        if (is_null($Product)) {
            throw new Exception('Не найден продукт');
        }

        return $this->render([
            'item' => $Product,
        ]);
    }

}
