<?php

namespace avatar\controllers;

use common\models\Anketa;
use common\models\task\Task;
use common\services\Security;
use common\services\Subscribe;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class AdminTelegramController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionSubscribe()
    {
        $model = new \avatar\models\forms\school\AdminTelegram();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionSetCallBack()
    {
        $path = self::getParam('path');
        $callback = self::getParam('callback');
        $component = self::getParam('component');

        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;

        $i = new Url($path);
        $file = Yii::getAlias('@webroot'.$i->path);

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['url' => $callback])
            ->addFile('certificate', $file)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionGetWebhookInfo()
    {
        $component = self::getParam('component');

        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/getWebhookInfo";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionRemoveCallBack()
    {
        $component = self::getParam('component');

        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }
}
