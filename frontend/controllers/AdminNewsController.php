<?php

namespace avatar\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\NewsItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminNewsController extends AdminBaseController
{


    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new NewsItem();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
            $i = \common\models\NewsItem::findOne($model->id);
            $i->created_at = time();
            $i->save();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = NewsItem::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = \common\models\NewsItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена новость');
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $html = $mailer->render('html/' . 'subscribe/news-item', ['newsItem' => $item], 'layouts/html/subscribe');

        $provider = new UniSender();
        try {
            $result = $provider->_call('createEmailMessage', [
                'sender_name'   => 'AvatarNetwork',
                'sender_email'  => 'god@avatar-bank.com',
                'subject'       => $item->name,
                'body'          => $html,
                'list_id'       => \common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG,
                'generate_text' => 1,
            ]);
        } catch (\Exception $e) {
            return self::jsonErrorId(101, $e->getMessage());
        }

        $message_id = $result['message_id'];
        $result = $provider->_call('createCampaign', [
            'message_id' => $message_id,
            'track_read' => 1
        ]);

        $item->is_send = 1;
        $item->save();

        return self::jsonSuccess($result);
    }

    /**
     * AJAX
     * Ставит флаг что рассылка выполнена успешно
     * Добавляет site_update
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribeSuccess($id)
    {
        $item = \common\models\NewsItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена новость');
        }
        $item->subscribe_is_send = 1;

        return self::jsonSuccess();
    }

    public function actionDelete($id)
    {
        NewsItem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
