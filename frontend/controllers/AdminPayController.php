<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\exchange\OfferPayLink;
use common\models\exchange\PayMethod;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminPayController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_admin'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\exchange\PayMethod();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\exchange\PayMethod::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEditLang($id, $lang)
    {
        $model = \avatar\models\forms\PayMethodLang::findOne(['parent_id' => $id, 'language' => $lang]);
        if (is_null($model)) {
            $model = new \avatar\models\forms\PayMethodLang(['parent_id' => $id, 'language' => $lang]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        \avatar\models\forms\exchange\PayMethod::findOne($id)->delete();

        return self::jsonSuccess();
    }


    public function actionSortAjax()
    {
        $ids = self::getParam('ids');

        $c = 0;
        foreach ($ids as $id) {
            $category = \common\models\exchange\PayMethod::findOne($id);
            $category->sort_index = $c;
            $category->save();
            $c++;
        }

        return self::jsonSuccess();
    }

    public function actionSort()
    {

        return $this->render();
    }
}
