<?php

namespace avatar\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminCloudflareController extends AdminBaseController
{
    public function actions()
    {
        return [
            'prod' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\AdminCloudflareProd',
            ],
            'reserv' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\AdminCloudflareReserv',
            ],
            'get-status' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\AdminCloudflareGetStatus',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }
}
