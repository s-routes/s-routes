<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\School;
use common\models\ShopRequest;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 */
class CabinetShopRequestsController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetShopRequestsSend',
            ],
        ];
    }

    /**
     * @param int $id school.id
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionView($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        if ($request->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш заказ');
        }

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     */
    public function actionPaid($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        if ($request->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш заказ');
        }
        $request->successClient();

        return self::jsonSuccess();
    }

    public function actionMessage()
    {
        $model = new \avatar\models\validate\CabinetSchoolShopMessage();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    public function actionAdd($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        $model = new \avatar\models\validate\CabinetRequestAdd(['request' => $request]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {

                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'request' => $request,
        ]);
    }

    public function actionAddSuccess($id)
    {
        $request = ShopRequest::findOne($id);

        return $this->render([
            'request' => $request,
        ]);
    }
}
