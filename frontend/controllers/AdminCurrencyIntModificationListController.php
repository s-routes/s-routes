<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use common\models\CurrencyIO;
use common\models\CurrencyIoModification;
use common\models\piramida\Currency;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminCurrencyIntModificationListController extends AdminBaseController
{

    /**
     * @param $id
     * @return string
     */
    public function actionIndex($id)
    {
        $cio = CurrencyIO::findOne($id);

        return $this->render(['cio' => $cio]);
    }

    public function actionEdit($id)
    {
        $model = \frontend\models\forms\CurrencyIoModification::findOne($id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', $model->currency_io_id);
            $model->save();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
