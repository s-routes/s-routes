<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\CurrencyIO;
use common\models\HD;
use common\models\HDtown;
use common\models\school\School;
use common\models\TaskOutput;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetActiveController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        $a = parent::actions();
        $s = [
            'add' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\ActiveInput',
                'formName' => 'ActiveInput',
            ],
            'add-mod' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\ActiveInputMod',
                'formName' => 'ActiveInputMod',
            ],
            'add2' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\forms\ActiveInputTransaction',
                'formName' => 'ActiveInputTransaction',
            ],
            'add3' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\forms\ActiveInputTransactionStep2',
                'formName' => 'ActiveInputTransactionStep2',
            ],
            'check' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ActiveInputCheck',
                'formName' => '',
            ],
            'close' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ActiveInputClose',
                'formName' => '',
            ],
        ];

        return ArrayHelper::merge($a, $s);
    }

    /**
     */
    public function actionLegat()
    {
        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view);
    }

    /**
     */
    public function actionPrizm()
    {
        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view);
    }

    /**
     */
    public function actionItem($id)
    {
        $CurrencyIO = CurrencyIO::findOne($id);
        if (is_null($CurrencyIO)) {
            throw new NotFoundHttpException();
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view,  ['CurrencyIO' => $CurrencyIO]);
    }

    /**
     */
    public function actionOutItem($id)
    {
        $task = TaskOutput::findOne($id);
        if (is_null($task)) {
            throw new NotFoundHttpException();
        }
        if ($task->user_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException();
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';

        return $this->render($view, ['item' => $task]);
    }

    /**
     */
    public function actionOut($id)
    {
        $CurrencyIO = CurrencyIO::findOne($id);
        if (is_null($CurrencyIO)) {
            throw new Exception('Не найдена монета для вывода');
        }

        $model = new \avatar\models\forms\TaskOutput(['CurrencyIO' => $CurrencyIO]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action($CurrencyIO);

            // Устанавливаю переменную для отображения страницы в режиме "выполнено"
            Yii::$app->session->setFlash('form', 1);
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'      => $model,
            'CurrencyIO' => $CurrencyIO,
        ]);
    }
    /**
     */
    public function actionOutIndex()
    {
        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view);
    }
}
