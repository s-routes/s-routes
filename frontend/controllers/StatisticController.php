<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\base\Application;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class StatisticController extends \avatar\base\BaseController
{

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionItem($id)
    {
        if (!Application::isInteger($id)) {
            throw new \Exception('Указано не целое число');
        }
        $currency = Currency::findOne($id);
        if (is_null($currency)) {
            throw new \Exception('Не найдена валюта');
        }

        return $this->render(['currency' => $currency]);
    }

}
