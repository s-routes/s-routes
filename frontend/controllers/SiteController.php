<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class SiteController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@avatar/views/site/error.php',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => !YII_ENV_PROD ? '123' : null,
            ],
        ];
    }

    /**
     */
    public function actionMerchant()
    {
        return $this->render();
    }

    /**
     */
    public function actionToken()
    {
        return $this->render();
    }

    /**
     */
    public function actionContracts()
    {
        return $this->render();
    }


    /**
     */
    public function actionFreePrivacyPolicy()
    {
        return $this->render('free-privacy-policy');
    }

    /**
     */
    public function actionMap()
    {
        return $this->render();
    }

    /**
     */
    public function actionService()
    {
        return $this->render();
    }

    /**
     */
    public function actionIndex()
    {
        $layout = 'landing';
        $view = '/landing/index';

        $this->layout = $layout;

        return $this->render($view);
    }

    /**
     */
    public function actionMain()
    {
        return $this->render();
    }



    /**
     */
    public function actionHelp()
    {
        return $this->render();
    }


    /**
     */
    public function actionAbout()
    {
        return $this->render();
    }

    /**
     */
    public function actionProtection()
    {
        return $this->render();
    }

}
