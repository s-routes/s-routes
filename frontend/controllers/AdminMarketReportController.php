<?php

namespace avatar\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminMarketReportController    extends AdminBaseController
{
    public function actionIndex()
    {
        $model = new \avatar\models\forms\AdminMarketReport();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render(['model' => $model]);
    }
}
