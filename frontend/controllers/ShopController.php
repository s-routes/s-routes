<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\shop\Basket;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ShopController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'cart-update' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ShopCartUpdate',
            ],
            'cart-add' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\CartAdd',
            ],
            'cart-delete' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\CartDelete',
            ],
            'first' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ShopFirst',
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionCatalog($id)
    {
        $catalog = CatalogItem::findOne($id);

        return $this->render([
            'catalog' => $catalog,
        ]);
    }


    /**
     */
    public function actionItem($id)
    {
        $Product = Product::findOne($id);
        if (is_null($Product)) {
            throw new Exception('Не найден продукт');
        }

        return $this->render([
            'item' => $Product,
        ]);
    }

    /**
     */
    public function actionOrder()
    {
        $id = Yii::$app->request->get('id');
        $request2 = Yii::$app->session->get('request');

        $isProduct = false;
        $data = null;
        if (!is_null($id)) {
            $product = \common\models\shop\Product::findOne($id);
            $isProduct = true;
            $data = [
                'product' => $product,
            ];
            $request = [
                'price'       => $product->price,
                'currency_id' => $product->currency_id,
                'productList' => [
                    $product->id => 1,
                ],
            ];
        } else {
            $basket = Basket::get();
            if (Basket::getCount() == 0) throw new Exception('Корзина пустая, перейдите в магазин и добавьте хотябы один товар');

            $product = \common\models\shop\Product::findOne($basket[0]['id']);
            $request = [
                'price'       => Basket::getPrice(),
                'currency_id' => $product->currency_id,
                'productList' => ArrayHelper::map($basket, 'id', 'count'),
            ];
            $data = [
                'price'       => Basket::getPrice(),
                'basket'      => $basket,
                'currency_id' => $product->currency_id,
            ];
        }
        $request['address']['comment'] = $request2['address']['comment'];

        Yii::$app->session->set('request', $request);

        return $this->render([
            'isProduct' => $isProduct,
            'data'      => $data,
        ]);
    }

    /**
     */
    public function actionOrder2()
    {
        $model = new \avatar\models\forms\ShopOrderAddress();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionLogin()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\ShopLogin();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionRegistration()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\ShopRegistration();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }


    /**
     * Отправляет код
     */
    public function actionSendCode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $code = Yii::$app->session->get('dogovor_code');
        if (is_null($code)) {
            $code = Security::generateRandomString();
            Yii::$app->session->set('dogovor_code', $code);
        }
        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        $walletEscrow = \Yii::$app->params['walletEscrow'];

        \common\services\Subscribe::sendArray(
            [$user->email],
            'Код для подписания договора оферты',
            'oferta',
            [
                'code' => $code,
            ]
        );

        return self::jsonSuccess();
    }



    /**
     * Показывает корзину
     */
    public function actionCart()
    {
        return $this->render();
    }

    /**
     * проверяет код
     */
    public function actionValidateCode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new \avatar\models\forms\shop\ValidateCode();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess();
    }

    /**
     */
    public function actionDelivery()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopDelivery();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionAddress()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopAddress();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionAnketa($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $anketa = \common\models\school\AnketaShop::findOne($id);

        $model = new \avatar\models\forms\ShopAnketa(['anketa' => $anketa]);

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

    /**
     */
    public function actionPaySystem()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $model = new \avatar\models\forms\ShopPaySystem();

        if (!$model->load(Yii::$app->request->post())) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }


    /**
     * Оплата заявки
     *
     * @param int $id идентификатор заявки gs_users_shop_requests.id
     *
     * @return string Response
     * @throws
     */
    public function actionPay($id)
    {
        if (Yii::$app->user->isGuest) {
            return self::jsonErrorId(10, 'Пользователь должен быть авторизован');
        }

        $request = Request::findOne($id);
        if (is_null($request)) {
            throw new \Exception('Не найдена заявка');
        }

        return $this->render('@avatar/views/shop/pay', [
            'request' => $request,
        ]);
    }

}
