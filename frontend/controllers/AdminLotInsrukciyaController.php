<?php

namespace avatar\controllers;

use avatar\controllers\actions\DefaultAjax;
use common\models\Instrukciya;
use common\models\LotInstrukciya;
use Yii;

class AdminLotInsrukciyaController extends AdminBaseController
{
    public function actions()
    {
        return [
            'delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model' => '\avatar\models\validate\AdminInstrukciya',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionSort()
    {
        return $this->render();
    }

    public function actionSortAjax()
    {
        $ids = self::getParam('ids');
        $c = 1;
        foreach ($ids as $i) {
            $foto = LotInstrukciya::findOne($i);
            $foto->sort_index = $c;
            $foto->save();
            $c++;
        }

        return self::jsonSuccess();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\validate\AdminLotInstrukciya();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
