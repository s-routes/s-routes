<?php

namespace avatar\controllers;

use avatar\models\forms\UserAvatar;
use avatar\models\validate\CardNumberAccept;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceEthPlorer;
use common\components\Card;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\UserSeed;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use console\controllers\MoneyRateController;
use cs\services\VarDumper;
use cs\web\Exception;
use cs\Widget\FileUpload4\ModelFields;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\imagine\Image;
use yii\web\Response;

class CabinetBillsController extends \avatar\controllers\CabinetBaseController
{

    public function actions()
    {
        return [
            'balance2' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetBillsBalance2',
            ],
        ];
    }


    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }


    /**
     * Показывает настройки счета
     *
     * REQUEST:
     * + id - int - идентификатор счета
     */
    public function actionSettings()
    {
        $billing = self::getBill();

        if ($billing->load(Yii::$app->request->post()) && $billing->validate()) {
            $billing->save();
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $billing,
            ]);
        }
    }

    /**
     */
    public function actionConvert($id)
    {
        return $this->render(['id' => $id]);
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    private function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * AJAX
     * Создает кошелек
     *
     * REQUEST:
     * + name            string - название счета
     * - currency        int - валюта Currency::BTC,ETH, по умолчанию BTC
     * - password        string - пароль
     * - password_type   int - 2 - пароль от кабинета, 1 - открытый пароль
     *
     * @return string
     * ошибки
     * 101, Не переданы данные
     * 102, ошибки формы
     * 103, ошибка ETH сервера Invalid JSON RPC response
     * 104, ошибка
     */
    public function actionNew()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new \avatar\models\validate\CabinetBillsControllerNew();
        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'Не переданы данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $this->convert($model->errors));
        }
        try {
            $billing = $model->action();
        } catch (\Exception $e) {
            if (StringHelper::startsWith($e->getMessage(), 'Invalid JSON RPC response')) {
                return self::jsonErrorId(103, $e->getMessage());
            } else {
                return self::jsonErrorId(104, $e->getMessage());
            }
        }

        return self::jsonSuccess([
            'id'      => $billing->id,
            'address' => $billing->address,
        ]);
    }

    public function ajaxActions()
    {
        return [
            'close',
            'password-type2',
        ];
    }

    /**
     * Получает счет ID
     * Делает проверку на хозяина
     *
     * @return \common\models\avatar\UserBill
     * @throws \yii\base\Exception
     */
    public function getBill($id = null)
    {
        if (is_null($id)) {
            $id = self::getParam('id');
        }
        $bill = UserBill::findOne($id);
        if ($bill->user_id != Yii::$app->user->id) {
            throw new \yii\base\Exception('Не ваш счет');
        }

        return $bill;
    }

}
