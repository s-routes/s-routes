<?php

namespace avatar\controllers;

use common\models\avatar\Currency;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminMarketConvertController extends AdminBaseController
{
    public static $currencyList = [
        Currency::TRX,
        Currency::PZM,
        Currency::ETH,
        Currency::USDT,

        Currency::BTC,
        Currency::LTC,
        Currency::BNB,
        Currency::DASH,
        Currency::NEIRO,
        Currency::EGOLD,
        Currency::DAI,
        Currency::DOGE,
        Currency::LOT,
    ];


        public function actionIndex()
    {
        $model = new \avatar\models\forms\AdminMarketConvert();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }
}
