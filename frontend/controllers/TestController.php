<?php

namespace avatar\controllers;

use avatar\models\forms\Contact;
use avatar\models\WalletETH;
use avatar\models\WalletVVB;
use avatar\modules\ETH\ServiceEtherScan;
use avatar\services\LogReader;
use common\components\providers\BTC;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillConfig;
use common\models\BillingMain;
use common\models\BinanceOrder;
use common\models\Card;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\PaymentBitCoin;
use common\models\piramida\Operation;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\ReferalTransaction;
use common\models\RequestTokenCreate;
use common\models\statistic\UserBinanceStatisticItem;
use common\models\Token;
use common\models\UserAvatar;
use common\models\UserBill2;
use common\models\UserBill3;
use common\models\UserDocument;
use common\models\UserRegistration;
use common\models\UserSeed;
use common\services\Security;
use common\services\Security\AES;
use common\services\Subscribe;
use cs\Application;
use cs\base\BaseController;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use frontend\services\BaseConvert;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Response;

class TestController extends \avatar\base\BaseController
{
    public function actionTest1()
    {
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        /**
         * @var array
         *
         * [
         * 0 => [
         * 'amount' => '15'
         * 'coin' => 'USDT'
         * 'network' => 'TRX'
         * 'status' => 1
         * 'address' => 'TUtQbDUjgTrgofhna4tyFFHMxA2J3RJWV1'
         * 'addressTag' => ''
         * 'txId' => 'Internal transfer 60276181415'
         * 'insertTime' => 1620846069000
         * 'transferType' => 1
         * 'confirmTimes' => '1/1'
         * ]
         */
        $data = $provider->_callApiKeyAndSigned('/sapi/v1/capital/deposit/hisrec', [
            'coin' => 'USDT',
        ]);

        VarDumper::dump($data);

    }

    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'  => $id, 'in'  => 0,
            'out' => 0,
        ];
    }

    public function actionTest2()
    {
        VarDumper::dump(date('d.m.Y H:i:s', 1614442844));

    }

    public function actionForm()
    {
        return $this->render();
    }


    public function actionTest3()
    {
        /** @var \frontend\modules\Binance\Binance $b */
        $provider = Yii::$app->Binance;
        $data = $provider->_callApiKeyAndSigned('api/v3/account', []);

        VarDumper::dump($data);
    }
    public function actionTest4()
    {
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        $data = $provider->_callApiKeyAndSigned('wapi/v3/withdrawHistory.html', []);

        VarDumper::dump($data);
    }
    public function actionTest5()
    {
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        $data = $provider->_callApiKeyAndSigned('api/v3/openOrders', [
        ]);

        VarDumper::dump($data);
    }

    public function actionTest6()
    {
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        $data = $provider->_callApiKeyAndSigned('/api/v3/order', [
            'symbol'  =>    'TRXUSDT',
            'orderId' =>    591932839,
        ]);

        VarDumper::dump($data);
    }


    /**
     * Обрезает лишние знаки $this->getCurrency()->decimals_view
     * Убирает лишние нули сзади
     *
     * @param $attribute
     * @param $params
     */
    public function normalizePrice($v1)
    {
        $c = Currency::findOne(Currency::RUB);
        $link = CurrencyLink::findOne(['currency_ext_id' => $c->id]);
        $c2 = \common\models\piramida\Currency::findOne($link->currency_int_id);
        $a = explode('.', $v1);
        $v = '0';
        if (count($a) == 1) {
            $v =  $this->price;
        }
        if (count($a) == 2) {
            $v = $a[0] . '.' . substr($a[1], 0, $c2->decimals_view_shop);
        }

        return $v;
    }

    /**
     * Убирает лишние нули сзади
     * @param $v
     * @return mixed
     */
    public function normalize($v)
    {
        $a = explode('.', $v);
        if (count($a) == 1) {
            return $v;
        }
        $a = Str::getChars($v);
        $a = array_reverse($a);
        $c1 = 0;
        foreach ($a as $c) {
            if ($c == '0') {
                $c1++;
                continue;
            } else {
                break;
            }
        }
        if ($a[$c1] == '.') $c1++;

        $ret = [];
        for($i = $c1; $i < count($a); $i++) {
            $ret[] = $a[$i];
        }
        $ret = array_reverse($ret);

        return join('', $ret);
    }

    function convBase($numberInput, $fromBaseInput, $toBaseInput)
    {
        if ($fromBaseInput==$toBaseInput) return $numberInput;
        $fromBase = str_split($fromBaseInput,1);
        $toBase = str_split($toBaseInput,1);
        $number = str_split($numberInput,1);
        $fromLen=strlen($fromBaseInput);
        $toLen=strlen($toBaseInput);
        $numberLen=strlen($numberInput);
        $retval='';
        if ($toBaseInput == '0123456789')
        {
            $retval=0;
            for ($i = 1;$i <= $numberLen; $i++)
                $retval = bcadd($retval, bcmul(array_search($number[$i-1], $fromBase),bcpow($fromLen,$numberLen-$i)));
            return $retval;
        }
        if ($fromBaseInput != '0123456789')
            $base10=$this->convBase($numberInput, $fromBaseInput, '0123456789');
        else
            $base10 = $numberInput;
        if ($base10<strlen($toBaseInput))
            return $toBase[$base10];
        while($base10 != '0')
        {
            $retval = $toBase[bcmod($base10,$toLen)].$retval;
            $base10 = bcdiv($base10,$toLen,0);
        }

        return $retval;
    }

    public static function getHash($t)
    {
        $data =
            $t['wallet'].
            $t['recipient'].
            $t['money'].
            $t['height'].
            $t['date']
        ;
        return hash('sha256', $data);
    }


    /**
     */
    public function actionBinance()
    {
        $v = '/api/v3/time';
        /** @var \frontend\modules\Binance\Binance $b */
        $b = Yii::$app->Binance;
        $data = $b->_callApiKey($v, []);

        VarDumper::dump($data);
    }

    /**
     */
    public function actionApp()
    {
        VarDumper::dump(Yii::$app);
    }

    /**
     */
    public function actionConfig()
    {
        $main = require (Yii::getAlias('@common/config/main.php'));
        $main_frontend = require (Yii::getAlias('@frontend/config/main.php'));

        VarDumper::dump(ArrayHelper::merge($main, $main_frontend));
    }

    /**
     */
    public function actionBinance2()
    {
        $v = '/api/v3/account';
        /** @var \frontend\modules\Binance\Binance $b */
        $b = Yii::$app->Binance;
        $data = $b->_callApiKeyAndSigned($v, []);

        VarDumper::dump($data);
    }


    /**
     */
    public function actionMail1()
    {
        Subscribe::sendArray(['dram1008@yandex.ru'], '1', 'test', ['text' => '12']);
    }

    /**
     */
    public function actionMail2()
    {
        $mail = Yii::$app->request->get('mail', 'dram1008@yandex.ru');

        VarDumper::dump(Application::mail($mail, '1', 'test', ['text' => '12']));
    }

    /**
     */
    public function actionMail3()
    {
        \Yii::$app->mailer->transport->setAuthMode('login');
        $o = \Yii::$app->mailer
            ->compose()
            ->setFrom('info@neuro-n.com')
            ->setTo('alex331911@yandex.ru')
            ->setTextBody('Текст тестового письма для отправки')
            ->setSubject('Тестовое письмо')
            ->send();
        VarDumper::dump($o);
    }

    public function actionMail31()
    {
        $mail = Yii::$app->request->get('mail', 'dram1008@yandex.ru');
//        \Yii::$app->mailer->transport->setAuthMode('login');
        $o = \Yii::$app->mailer
            ->compose()
            ->setFrom('info@neuro-n.com')
            ->setTo($mail)
            ->setTextBody('Текст тестового письма для отправки')
            ->setSubject('Тестовое письмо2')
            ->send();
        VarDumper::dump([$o,\Yii::$app->mailer]);
    }


    /**
     */
    public function actionMail4()
    {
        VarDumper::dump(\Yii::$app->mailer);
    }

    /**
     */
    public function actionMail5()
    {
        $mail = Yii::$app->request->get('mail', 'dram1008@yandex.ru');
        $o = \Yii::$app->mailer
            ->compose()
            ->setFrom(['info@neuro-n.com' => 'Info Neuro-N'])
            ->setTo($mail)
            ->setTextBody('test11')
            ->setSubject('test')
            ->send();
        VarDumper::dump($o);
//
//        // Create the Transport
//    $transport = (new Swift_SmtpTransport('smtp-pulse.com', 465))
//        ->setUsername('smk202007@gmail.com')
//        ->setPassword('2________H')
//    ;
//
//    // Create the Mailer using your created Transport
//    $mailer = new Swift_Mailer($transport);
//
//    // Create a message
//    $message = (new Swift_Message('Wonderful Subject'))
//        ->setFrom(['info@neuro-n.com' => 'Info Neuro-N'])
//        ->setTo(['alex331911@yandex.ru', 'alex3319@mail.ru' => 'Alex Pustoutov'])
//        ->setBody('Here is the message itself')
//    ;
//
//    // Send the message
//    $result = $mailer->send($message);
    }


}
