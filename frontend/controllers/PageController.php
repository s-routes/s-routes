<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\models\forms\exchange\Page;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\school\School;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PageController extends \avatar\base\BaseController
{
    /**
     */
    public function actionDogovorOferty()
    {
        return $this->render();
    }


    /**
     */
    public function actionOpisanieTovarovUslug()
    {
        return $this->render();
    }

    /**
     */
    public function actionYuridicheskayaInformatsiya()
    {
        return $this->render();
    }

    /**
     */
    public function actionCard()
    {
        return $this->render();
    }

    /**
     */
    public function actionHelp()
    {
        return $this->render();
    }

    /**
     */
    public function actionRules()
    {
        return $this->render();
    }

    /**
     */
    public function actionPrivacyPolicy()
    {
        return $this->render();
    }

    /**
     */
    public function actionUserAgreement()
    {
        return $this->render();
    }

    /**
     */
    public function actionNews()
    {
        return $this->render();
    }

    /**
     */
    public function actionBlog()
    {
        return $this->render();
    }

    /**
     */
    public function actionContact()
    {
        return $this->render();
    }

    /**
     */
    public function actionAbout()
    {
        return $this->render();
    }

    /**
     */
    public function actionStatistic()
    {
        return $this->render();
    }

    /**
     */
    public function actionActivate()
    {
        return $this->render();
    }

    /**
     */
    public function actionItem($id)
    {
        $page = \common\models\exchange\Page::findOne($id);
        if (is_null($page)) {
            throw new NotFoundHttpException('Не найдена страница');
        }

        return $this->render(['page' => $page]);
    }

}
