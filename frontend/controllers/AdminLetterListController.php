<?php

namespace avatar\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\NewsItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\SendLetter;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminLetterListController extends AdminBaseController
{

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionView($id)
    {
        $item = SendLetter::findOne($id);
        $model = new \avatar\models\forms\Answer(['id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                return self::jsonSuccess($model->save());
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
            'item'  => $item,
        ]);
    }

}
