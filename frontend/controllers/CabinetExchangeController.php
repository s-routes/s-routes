<?php

namespace avatar\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\exchange\OfferPayLink;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\NeironTransaction;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetExchangeController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->isGuest) return false;

                            $deal = [
                                'invite-arbitrator',
                                'set-assessment',
                                'cancel',
                                'deal',

                                'buy-money-received',
                                'buy-money-sended',
                                'buy-open-accept',

                                'sell-money-received',
                                'sell-money-sended',
                                'sell-open-accept',
                            ];
                            if (in_array($action->id, $deal)) {
                                $deal = Deal::findOne(Yii::$app->request->get('id'));
                                if (is_null($deal)) {
                                    throw new NotFoundHttpException();
                                }
                                if (!in_array(Yii::$app->user->id, [$deal->user_id, $deal->getOffer()->user_id])) {
                                    return false;
                                }
                            }
                            $deal = [
                                'deal-open-send',
                            ];
                            if (in_array($action->id, $deal)) {
                                $deal = Deal::findOne(Yii::$app->request->post('id'));
                                if (is_null($deal)) {
                                    throw new NotFoundHttpException();
                                }
                                if (!in_array(Yii::$app->user->id, [$deal->user_id, $deal->getOffer()->user_id])) {
                                    return false;
                                }
                            }
                            $deal = [
                                'deal-action',
                            ];
                            if (in_array($action->id, $deal)) {
                                $deal = Deal::findOne(Yii::$app->request->get('id'));
                                if (is_null($deal)) {
                                    throw new NotFoundHttpException();
                                }
                                if (Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_arbitrator')) {
                                    return true;
                                }
                                if (!in_array(Yii::$app->user->id, [$deal->user_id, $deal->getOffer()->user_id])) {
                                    return false;
                                }
                            }

                            return true;
                        },
                    ],
                ],
            ],
        ];
    }



    public function actions()
    {
        return [
            'set-assessment' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetExchangeSetAssessment',
                'formName' => ''
            ],
            'deal-open-ajax' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetExchangeDealOpenAjax',
                'formName' => ''
            ],
            'deal-open-send' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetExchangeDealOpenSend',
                'formName' => ''
            ],
            'offer-close' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetExchangeOfferClose',
                'formName' => ''
            ],
            'offer-delete' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\CabinetExchangeOfferDelete',
                'formName' => ''
            ],
        ];
    }

    /**
     * @param string $value
     * @param Currency | \common\models\piramida\Currency $cur
     */
    public static function format($value, $cur)
    {
        return bcdiv($value, bcpow(10, $cur->decimals), $cur->decimals);
    }

    /**
     * @param string $value
     * @param Currency | \common\models\piramida\Currency $cur
     */
    public static function format2($value, $cur)
    {
        return Yii::$app->formatter->asDecimal(bcdiv($value, bcpow(10, $cur->decimals), $cur->decimals_view), $cur->decimals_view);
    }

    public function actionIndex()
    {
        return $this->render([]);
    }

    public function actionDeal($id)
    {
        $deal = Deal::findOne($id);
        if ($deal->status == Deal::STATUS_CREATE) {
            return $this->redirect(['deal-open', 'id' => $deal->id]);
        }

        return $this->redirect(['deal-action', 'id' => $deal->id]);
    }

    public function actionChat()
    {
        $user_id = Yii::$app->user->id;
        $chat_id = 2000000001;

        /** @var \common\models\UserBan $UserBan */
        $UserBan = \common\models\UserBan::find()->where(['user_id' => $user_id, 'chat_id' => $chat_id])->one();

        if (!is_null($UserBan)) {
            if ($UserBan->type == \common\models\UserBan::TYPE_DELETE) {
                throw new ForbiddenHttpException('Вам запрещен доступ в этот чат');
            }
        }

        return $this->render(['chat_id' => $chat_id]);
    }

    public function actionChatExchange()
    {
        $user_id = Yii::$app->user->id;
        $chat_id = 2000000002;

        /** @var \common\models\UserBan $UserBan */
        $UserBan = \common\models\UserBan::find()->where(['user_id' => $user_id, 'chat_id' => $chat_id])->one();

        if (!is_null($UserBan)) {
            if ($UserBan->type == \common\models\UserBan::TYPE_DELETE) {
                throw new ForbiddenHttpException('Вам запрещен доступ в этот чат');
            }
        }

        return $this->render('chat', ['chat_id' => $chat_id]);
    }

    public function actionOffer()
    {
        return $this->render([]);
    }

    public function actionMyDeals()
    {
        return $this->render([]);
    }

    public function actionOfferAdd()
    {
        $id = Yii::$app->request->get('id');
        $offer = null;
        if (!Application::isEmpty($id)) {
            if (Application::isInteger($id)) {
                $offer = Offer::findOne($id);
                if (is_null($offer)) {
                    throw new \Exception('Предложение не найдено');
                }
                if ($offer->user_id != Yii::$app->user->id) {
                    throw new \Exception('Это не ваше предложение');
                }
            }
        }

        $model = new \avatar\models\forms\exchange\OfferAdd(['offer' => $offer]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $offer = $model->action();
            Yii::$app->session->setFlash('form', $offer->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionOfferEdit($id)
    {
        if (Deal::find()->where(['offer_id' => $id])->count() > 0) {
            throw new Exception('Изменение цены на этом этапе сделки уже невозможно. Создайте новый оффер с новой ценой.');
        }
        $model = \avatar\models\forms\exchange\OfferEdit::findOne($id);
        $model->convertDown();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->convertUp();
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * @param int $id deal.id
     * @return string|Response
     * @throws \Exception
     */
    public function actionDealOpen($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);
        $Offer = $Deal->getOffer();

        $model = new \avatar\models\forms\exchange\DealOpen([
            'offer' => $Offer,
            'deal'  => $Deal,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $deal = $model->action();

            return $this->redirect(['cabinet-exchange/deal-action', 'id' => $deal->id]);
        }

        return $this->render([
            'model' => $model,
            'offer' => $Offer,
            'deal'  => $Deal,
        ]);
    }

    public function actionDealAction($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if (
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin')
            ||
            Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_arbitrator')
        ) {
            // пускаю permission_admin permission_arbitrator
        } else {
            if (!(Yii::$app->user->id == $Deal->user_id || Yii::$app->user->id == $Deal->getOffer()->user_id)) {
                throw new ForbiddenHttpException('Вам нельзя заходить в чужие сделки');
            }
        }

        return $this->render([
            'Deal' => $Deal,
        ]);
    }

    public function actionDealAction2($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        return $this->render([
            'Deal' => $Deal,
        ]);
    }

    public function actionSellOpenAccept($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $CurrencyIO = CurrencyIO::findOne($Deal->currency_io);

        // Ищу кошелек
        $offer = $Deal->getOffer();
        $data = UserBill::getInternalCurrencyWallet($CurrencyIO->currency_int_id, $offer->user_id);
        /** @var \common\models\piramida\Wallet $wallet */
        $wallet = $data['wallet'];
        if ($wallet->amount < $Deal->volume_vvb) {
            return self::jsonErrorId(104, 'Не достаточно монет');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_BLOCK);

            $Deal->status = Deal::STATUS_BLOCK;
            $Deal->time_accepted = time();
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_BLOCK, 'deal_id' => $Deal->id]);

            // вычисляю тип операции
            $type = NeironTransaction::TYPE_BUY_OUT_NOT_TRUSTED;
            $ids = \common\models\Config::get('trusted-users');
            if (\cs\Application::isEmpty($ids)) $ids = [];
            else $ids = \yii\helpers\Json::decode($ids);
            if (in_array($offer->user_id, $ids)) {
                $type = NeironTransaction::TYPE_BUY_OUT_TRUSTED;
            }

            // Блокирую деньги (делаю перевод для блокировки)
            $transactionBlock = $wallet->move2(
                $CurrencyIO->block_wallet,
                $Deal->volume_vvb,
                Json::encode([
                    'Списание средств от сделки {deal} от пользователя {user} с кошелька {wallet}',
                    [
                        'deal'   => Html::a($Deal->id, ['cabinet-exchange/deal-action', 'id' => $Deal->id], ['data-pjax' => 0]),
                        'user'   => $offer->user_id,
                        'wallet' => $wallet->id,
                    ],
                ]),
                $type
            );

            // Делаю запись о типе транзакции если это NERON
            if ($wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
                NeironTransaction::add([
                    'transaction_id' => $transactionBlock['transaction']['id'],
                    'operation_id'   => $transactionBlock['operation_sub']['id'],
                    'type_id'        => $type,
                ]);
            }

            $Deal->trigger(Deal::EVENT_AFTER_BLOCK);

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        // Отправка письма к maker (offer)
        $dealUser = $Deal->getUser();
        Subscribe::sendArray([$dealUser->email], 'Сделка принята #' . $Deal->id, 'sell_open_accept', [
            'deal' => $Deal
        ]);

        // отправка уведомления для $dealUser
        if (!Application::isEmpty($dealUser->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($dealUser->language)) ? 'ru' : $dealUser->language;
            $telegram->sendMessage(['chat_id' => $dealUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сделка принята', [], $language).' #' . $Deal->id]);
        }

        return self::jsonSuccess();
    }

    public function actionSellMoneySended($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        // Если сделка на арбитре ?
        if (in_array($Deal->status, [
            Deal::STATUS_AUDIT_WAIT,
            Deal::STATUS_AUDIT_ACCEPTED,
            Deal::STATUS_AUDIT_FINISH,
        ])) {
            return self::jsonErrorId(403, 'Сделка разбирается арбитром');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_SENDED);

            $Deal->status = Deal::STATUS_MONEY_SENDED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_MONEY_SENDED, 'deal_id' => $Deal->id]);

            // отправка уведомления для $offerUser
            $offerUser = $Deal->getOffer()->getUser();
            if (!Application::isEmpty($offerUser->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($offerUser->language)) ? 'ru' : $offerUser->language;
                $telegram->sendMessage(['chat_id' => $offerUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Средства отправлены по сделке', [], $language).' #' . $Deal->id]);
            }

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_SENDED);

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        return self::jsonSuccess();
    }

    public function actionSellMoneyReceived($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        // Если сделка на арбитре ?
        if (in_array($Deal->status, [
            Deal::STATUS_AUDIT_WAIT,
            Deal::STATUS_AUDIT_ACCEPTED,
            Deal::STATUS_AUDIT_FINISH,
        ])) {
            return self::jsonErrorId(403, 'Сделка разбирается арбитром');
        }

        // Если сделка уже в статусе "Деньги получены" или "Закрыта"
        if (in_array($Deal->status, [
            Deal::STATUS_MONEY_RECEIVED,
            Deal::STATUS_CLOSE,
        ])) {
            return self::jsonErrorId(403, 'Сделка закрыта');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_RECEIVED);

            $Deal->status = Deal::STATUS_MONEY_RECEIVED;
            $Deal->time_finish = time();
            $Deal->save();

            // обновляю среднее время сделки в предложениях
            $this->recalculateAvgDeal($Deal->getOffer()->user_id);

            // Добавляю статус в историю
            DealStatus::add(['status' => Deal::STATUS_MONEY_RECEIVED, 'deal_id' => $Deal->id]);

            // Разблокирую деньги
            {
                $CurrencyIO = CurrencyIO::findOne($Deal->currency_io);

                // Ищу кошелек
                $data = UserBill::getInternalCurrencyWallet($CurrencyIO->currency_int_id, $Deal->user_id);

                /** @var \common\models\piramida\Wallet $wallet */
                $wallet = $data['wallet'];
                $walletFrom = Wallet::findOne($CurrencyIO->block_wallet);

                // вычисляю тип операции
                $type = NeironTransaction::TYPE_BUY_NOT_TRUSTED;
                $ids = \common\models\Config::get('trusted-users');
                if (\cs\Application::isEmpty($ids)) $ids = [];
                else $ids = \yii\helpers\Json::decode($ids);
                if (in_array($Deal->getOffer()->user_id, $ids)) {
                    $type = NeironTransaction::TYPE_BUY_TRUSTED;
                }

                // делаю перевод для разблокировки
                $transactionUnBlock = $walletFrom->move2(
                    $wallet,
                    $Deal->volume_vvb,
                    Json::encode([
                        'Зачисление средств от сделки {deal} в пользу пользователя {user} в кошелек {wallet}',
                        [
                            'deal'   => Html::a($Deal->id, ['cabinet-exchange/deal-action', 'id' => $Deal->id], ['data-pjax' => 0]),
                            'user'   => $Deal->user_id,
                            'wallet' => $wallet->id,
                        ],
                    ]),
                    $type
                );

                // Делаю запись о типе транзакции если это NERON
                if ($wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
                    NeironTransaction::add([
                        'transaction_id' => $transactionUnBlock['transaction']['id'],
                        'operation_id'   => $transactionUnBlock['operation_add']['id'],
                        'type_id'        => $type,
                    ]);
                }
            }

            // отправка уведомления для $dealUser
            $dealUser = $Deal->getUser();
            if (!Application::isEmpty($dealUser->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($dealUser->language)) ? 'ru' : $dealUser->language;
                $telegram->sendMessage(['chat_id' => $dealUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Средства получены по сделке', [], $language).' #' . $Deal->id]);
            }

            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_RECEIVED);

            $t->commit();

        } catch ( \Exception $e) {

            $t->rollBack();
            throw new \Exception($e);

        }


        return self::jsonSuccess();
    }

    public function actionBuyOpenAccept($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $CurrencyIO = CurrencyIO::findOne($Deal->currency_io);
        // Ищу кошелек
        $data = UserBill::getInternalCurrencyWallet($CurrencyIO->currency_int_id, $Deal->user_id);
        /** @var \common\models\piramida\Wallet $wallet */
        $wallet = $data['wallet'];

        // проверка
        if ($wallet->amount < $Deal->volume_vvb) {
            return self::jsonErrorId(103, 'Недостаточно монет у продавца');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_BLOCK);

            $Deal->status = Deal::STATUS_BLOCK;
            $Deal->time_accepted = time();
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_BLOCK, 'deal_id' => $Deal->id]);

            // вычисляю тип операции
            $type = NeironTransaction::TYPE_BUY_OUT_NOT_TRUSTED;
            $ids = \common\models\Config::get('trusted-users');
            if (\cs\Application::isEmpty($ids)) $ids = [];
            else $ids = \yii\helpers\Json::decode($ids);
            if (in_array($Deal->user_id, $ids)) {
                $type = NeironTransaction::TYPE_BUY_OUT_TRUSTED;
            }

            // Блокирую деньги (делаю перевод для блокировки)
            $transactionBlock = $wallet->move2(
                $CurrencyIO->block_wallet,
                $Deal->volume_vvb,
                Json::encode([
                    'Списание средств от сделки {deal} от пользователя {user} с кошелька {wallet}',
                    [
                        'deal'   => Html::a($Deal->id, ['cabinet-exchange/deal-action', 'id' => $Deal->id], ['data-pjax' => 0]),
                        'user'   => $Deal->user_id,
                        'wallet' => $wallet->id,
                    ],
                ]),
                $type
            );

            // Делаю запись о типе транзакции если это NERON
            if ($wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
                NeironTransaction::add([
                    'transaction_id' => $transactionBlock['transaction']['id'],
                    'operation_id'   => $transactionBlock['operation_sub']['id'],
                    'type_id'        => $type,
                ]);
            }

            $Deal->trigger(Deal::EVENT_AFTER_BLOCK);

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        // Отправка письма к maker (offer)
        $dealUser = $Deal->getUser();
        Subscribe::sendArray([$dealUser->email], 'Сделка принята #' . $Deal->id, 'buy_open_accept', [
            'deal' => $Deal
        ]);

        // отправка уведомления для $dealUser
        if (!Application::isEmpty($dealUser->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($dealUser->language)) ? 'ru' : $dealUser->language;
            $telegram->sendMessage(['chat_id' => $dealUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сделка принята', [], $language).' #' . $Deal->id]);
        }

        return self::jsonSuccess();
    }

    public function actionBuyMoneySended($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        // Если сделка на арбитре ?
        if (in_array($Deal->status, [
            Deal::STATUS_AUDIT_WAIT,
            Deal::STATUS_AUDIT_ACCEPTED,
            Deal::STATUS_AUDIT_FINISH,
        ])) {
            return self::jsonErrorId(403, 'Сделка разбирается арбитром');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_SENDED);

            $Deal->status = Deal::STATUS_MONEY_SENDED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_MONEY_SENDED, 'deal_id' => $Deal->id]);

            // отправка уведомления для $dealUser
            $dealUser = $Deal->getUser();
            if (!Application::isEmpty($dealUser->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($dealUser->language)) ? 'ru' : $dealUser->language;
                $telegram->sendMessage(['chat_id' => $dealUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Средства отправлены по сделке', [], $language).' #' . $Deal->id]);
            }

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_SENDED);

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        return self::jsonSuccess();
    }

    public function actionBuyMoneyReceived($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        // Если сделка на арбитре ?
        if (in_array($Deal->status, [
            Deal::STATUS_AUDIT_WAIT,
            Deal::STATUS_AUDIT_ACCEPTED,
            Deal::STATUS_AUDIT_FINISH,
        ])) {
            return self::jsonErrorId(403, 'Сделка разбирается арбитром');
        }

        // Если сделка уже в статусе "Деньги получены" или "Закрыта"
        if (in_array($Deal->status, [
            Deal::STATUS_MONEY_RECEIVED,
            Deal::STATUS_CLOSE,
        ])) {
            return self::jsonErrorId(403, 'Сделка закрыта');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_MONEY_RECEIVED);

            $Deal->status = Deal::STATUS_MONEY_RECEIVED;
            $Deal->time_finish = time();
            $Deal->save();

            // обновляю среднее время сделки в предложениях
            $this->recalculateAvgDeal($Deal->getOffer()->user_id);

            // Добавляю статус в историю
            DealStatus::add(['status' => Deal::STATUS_MONEY_RECEIVED, 'deal_id' => $Deal->id]);

            // Разблокирую деньги
            {
                $CurrencyIO = CurrencyIO::findOne($Deal->currency_io);
                // Ищу кошелек
                $offer = $Deal->getOffer();
                $userBill = UserBill::getInternalCurrencyWallet($CurrencyIO->currency_int_id, $offer->user_id);
                /** @var \common\models\piramida\Wallet $wallet */
                $wallet = $userBill['wallet'];
                $walletFrom = Wallet::findOne($CurrencyIO->block_wallet);

                // вычисляю тип операции
                $type = NeironTransaction::TYPE_BUY_NOT_TRUSTED;
                $ids = \common\models\Config::get('trusted-users');
                if (\cs\Application::isEmpty($ids)) $ids = [];
                else $ids = \yii\helpers\Json::decode($ids);
                if (in_array($offer->user_id, $ids)) {
                    $type = NeironTransaction::TYPE_BUY_TRUSTED;
                }

                // делаю перевод для разблокировки
                $transactionUnBlock = $walletFrom->move2(
                    $wallet,
                    $Deal->volume_vvb,
                    Json::encode([
                        'Зачисление средств от сделки {deal} в пользу пользователя {user} в кошелек {wallet}',
                        [
                            'deal'   => Html::a($Deal->id, ['cabinet-exchange/deal-action', 'id' => $Deal->id], ['data-pjax' => 0]),
                            'user'   => $offer->user_id,
                            'wallet' => $wallet->id,
                        ],
                    ]),
                    $type
                );

                // Делаю запись о типе транзакции если это NERON
                if ($wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
                    NeironTransaction::add([
                        'transaction_id' => $transactionUnBlock['transaction']['id'],
                        'operation_id'   => $transactionUnBlock['operation_add']['id'],
                        'type_id'        => $type,
                    ]);
                }
            }

            // отправка уведомления для $offerUser
            $offerUser = $Deal->getOffer()->getUser();
            if (!Application::isEmpty($offerUser->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($offerUser->language)) ? 'ru' : $offerUser->language;
                $telegram->sendMessage(['chat_id' => $offerUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Средства получены по сделке', [], $language).' #' . $Deal->id]);
            }

            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $Deal->trigger(Deal::EVENT_AFTER_MONEY_RECEIVED);

            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        return self::jsonSuccess();
    }

    /**
     * Обновляет среднее время сделки в предложениях которые создал $u
     * @param int $uid идентификатор Пользователя предложения
     * @return int кол-во обновленных записей offer
     */
    private function recalculateAvgDeal($uid)
    {
        $c1 = \common\models\exchange\Deal::find()
            ->innerJoin('offer', 'offer.id = deal.offer_id')
            ->where(['offer.user_id' => $uid])
            ->andWhere([
                'deal.status' => [
                    \common\models\exchange\Deal::STATUS_CLOSE,
                    \common\models\exchange\Deal::STATUS_MONEY_RECEIVED,
                ],
            ])
            ->andWhere(['not', ['deal.time_finish' => null]])
            ->select('avg(deal.time_finish - deal.time_accepted)')
            ->scalar();
        $c2 = \common\models\exchange\Offer::updateAll(['avg_deal' => $c1], ['user_id' => $uid]);

        return $c2;
    }

    /**
     * Обрабатывает кнопку "Пригласить арбитра"
     * - отправляет уведомление на телеграм партнеру
     *
     * @param $id
     *
     * @return Response
     * @throws \Exception
     */
    public function actionInviteArbitrator($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_WAIT);

            $Deal->status = Deal::STATUS_AUDIT_WAIT;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_AUDIT_WAIT, 'deal_id' => $Deal->id]);

            // отправка уведомления для партнера через телеграм
            {
                if (Yii::$app->user->id == $Deal->user_id) {
                    // отправляю тому кто сделал предложение
                    $user = $Deal->getOffer()->getUser();
                } else {
                    $user = $Deal->getUser();
                }
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Вызван арбитр по сделке', [], $language).' #' . $Deal->id]);
                }
            }

            // Уведомляю админа по телеграму
            $userList = Yii::$app->authManager->getUserIdsByRole('role_admin');
            foreach ($userList as $uid) {
                $user = UserAvatar::findOne($uid);
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Вызван арбитр в сделку', [], $language).' #' . $Deal->id]);
                }
            }

            $Deal->trigger(Deal::EVENT_AFTER_AUDIT_WAIT);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        return self::jsonSuccess();
    }

    /**
     * Обрабатывает кнопку "Отменить сделку"
     *
     * @param $id
     *
     * @return Response
     * @throws \Exception
     */
    public function actionCancel($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if ($Deal->status != Deal::STATUS_OPEN) {
            return self::jsonErrorId(403, 'Запрещено отменять сделку после подтверждения');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->status = Deal::STATUS_CANCEL;
            $Deal->save();
            DealStatus::add([
                'status'  => Deal::STATUS_CANCEL,
                'deal_id' => $Deal->id,
                'comment' => Json::encode(['Сделка отменена']),
            ]);

            // Добавляю в соответствие с ситуацией 20200708
            $offer = $Deal->getOffer();
            $offer->volume_io_finish_deal = bcadd($offer->volume_io_finish_deal, $Deal->volume_vvb);
            $offer->save();

            // отправка уведомления для $dealUser через телеграм
            {
                // выясняю кто отменил
                // Если отменил инициатор сделки, то
                if (Yii::$app->user->id == $Deal->user_id) {
                    // отправляю тому кто сделал предложение
                    $user = $Deal->getOffer()->getUser();
                } else {
                    $user = $Deal->getUser();
                }
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сделка отменена', [], $language). ' #' . $Deal->id]);
                }
            }

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
            throw new \Exception($e);
        }

        return self::jsonSuccess();
    }



}
