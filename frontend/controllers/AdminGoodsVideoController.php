<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\shop\ProductImage;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormDelete;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminGoodsVideoController extends AdminBaseController
{
    public function actions()
    {
        return [
            'delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model' => '\avatar\models\forms\ProductVideo',
            ],
        ];
    }

    public function actionIndex($id)
    {
        return $this->render(['id' => $id]);
    }

    public function actionAdd($id)
    {
        $model = new \avatar\models\forms\ProductVideo(['product_id' => $id]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\ProductVideo::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
