<?php

namespace avatar\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\TaskOutput;
use common\models\TaskPrizmOutput;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminBuhController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['role_buh'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'hide' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\AdminBuhHide',
            ],
            'start' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\AdminBuhStart',
            ],
            'done-ajax2' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\AdminBuhDoneAjax2',
            ],
            'done-pzm-ajax' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\AdminBuhDonePzmAjax',
            ],
            'cancel' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\AdminBuhInputCancel',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionInput()
    {
        return $this->render();
    }

    public function actionIndexAll()
    {
        return $this->render();
    }

    /**
     */
    public function actionOutItem($id)
    {
        $task = TaskOutput::findOne($id);
        if (is_null($task)) {
            throw new NotFoundHttpException();
        }

        return $this->render(['item' => $task]);
    }

    /**
     * @throws
     */
    public function actionDone($id)
    {
        $task = TaskOutput::findOne($id);
        if (is_null($task)) {
            throw new Exception('Заявка не найдена');
        }
        $model = new \avatar\models\forms\AdminBuhDone(['task' => $task]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', $task->id);
            $model->action();
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
