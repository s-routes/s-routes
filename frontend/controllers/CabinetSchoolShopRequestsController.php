<?php

namespace avatar\controllers;

use app\common\components\Piramida;
use app\models\Article;
use app\models\Form\Shop\Order;
use app\models\Piramida\WalletSource\Custom;
use app\models\Piramida\WalletSource\Pochta;
use app\models\Piramida\WalletSource\Yandex;
use app\models\Shop;
use app\models\Shop\Product;
use app\models\Shop\Request;
use app\models\Shop\RequestMessage;
use app\models\Shop\RequestProduct;
use app\models\Union;
use app\models\User;
use app\modules\Shop\services\Basket;
use common\models\school\School;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\Html;
use yii\db\Query;


/**
 */
class CabinetSchoolShopRequestsController extends AdminBaseController
{
    public function actions()
    {
        return [
            'send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSchoolShopRequestsSend',
            ],
        ];
    }

    /**
     * @param int $id school.id
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionView($id)
    {
        $request = \common\models\shop\Request::findOne($id);

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     */
    public function actionPaid($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        if ($request->is_paid_client != 1) $request->successClient();
        $request->successShop();

        return self::jsonSuccess();
    }

    /**
     */
    public function actionDone()
    {
        $id = Yii::$app->request->post('id');
        $request = \common\models\shop\Request::findOne($id);
        $request->addStatusToClient(\common\models\shop\Request::STATUS_DONE);
        $user = UserAvatar::findOne($request->user_id);
        $d = Subscribe::sendArray([$user->email], 'Заказ был исполнен', 'shop_done', [
            'request' => $request
        ]);

        return self::jsonSuccess();
    }

    public function actionMessage()
    {

        $model = new \avatar\models\validate\CabinetSchoolShopMessage();

        if ($model->load(Yii::$app->request->post(), '') && $model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }

        return self::jsonSuccess($model->action());
    }

}
