<?php

namespace avatar\controllers;

use avatar\base\Application;
use avatar\models\search\UserAvatar;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillSystem;
use console\controllers\MoneyRateController;
use cs\web\Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\Response;

class CabinetUsersController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->isGuest) return false;
                            if (Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin')) return true;
                            if (Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_buh')) return true;

                            return false;
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionView($id)
    {
        $user = \common\models\UserAvatar::findOne($id);

        return $this->render(['user' => $user]);
    }
}
