<?php

namespace avatar\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminOfertaController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\shop\Oferta();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEdit($id)
    {
        $model = \avatar\models\forms\shop\Oferta::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        \avatar\models\forms\shop\Oferta::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
