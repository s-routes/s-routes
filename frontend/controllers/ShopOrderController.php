<?php

namespace avatar\controllers;

use common\models\MerchantRequest;
use common\models\PaySystem;
use common\models\piramida\InRequest;
use common\models\piramida\WalletSource\BitCoin;
use common\models\piramida\WalletSource\RubRubYandex;
use common\models\piramida\WalletSourceInterface;
use common\models\school\RequestInput;
use common\models\shop\Request;
use common\models\UserEnter;
use cs\Application;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * Контроллер который обрабатывает callback от платежных систем
 * CRRF валидация отключена, так как запрос идет извне
 *
 * Class ShopOrderController
 * @package avatar\controllers
 */
class ShopOrderController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    /**
     * Получает уведомление об оплате
     *
     * REQUEST:
     * - id
     *
     * @return string
     * @throws
     */
    public function actionSuccess()
    {
        $rawBody = Yii::$app->request->rawBody;
        $rawBody = str_replace('\\"', '"', $rawBody);
        $rawBody = json_decode($rawBody);
        $rawBody = ArrayHelper::toArray($rawBody);
        \Yii::info(VarDumper::dumpAsString([$rawBody, $_POST]), 'avatar\\payments\\beforeRequest2');

        $type = self::getParam('type');
        Yii::info(VarDumper::dumpAsString(Yii::$app->request->get()), 'avatar\\payments');
        if (is_null($type)) {
            return self::jsonErrorId(101, 'Отсутствует обязательный параметр type');
        }
        $actions = [
            'shopRequest' => function($id, WalletSourceInterface $transaction){
                Yii::info('avatarPartner action begin', 'avatar\\payments\\order\\success::actionBegin');
                $requestId = $id;
                $request = Request::findOne($requestId);
                $request->successShop();

                return true;
            },
        ];

        switch ($type) {
            case 'sberkassa':
                Yii::info('sberkassa', 'avatar\\payments\\order\\success::actionBegin');
                $ps = PaySystem::findOne(['code' => 'rub-sber-api']);
                if (is_null($ps)) throw  new InvalidConfigException('Не найдена платежная система rub-sber-api');

                $class = $ps->getClass();
                $ret = $class->success($actions);
                break;

        }

        return self::jsonSuccess();
    }

    /**
     * Получает уведомление об оплате
     *
     * REQUEST:
     * - id
     *
     * @return string
     */
    public function actionSuccess2()
    {
        \Yii::info([$_POST, $_GET, Yii::$app->request->post()], 'avatar\\beforeRequest2');

        return self::jsonSuccess();
    }
}
