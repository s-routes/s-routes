<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\sms\IqSms;
use common\models\avatar\UserBill;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Response;

class UserController extends \avatar\base\BaseController
{
    /**
     * Выводит профиль пользователя
     *
     * @param int $id идентификатор пользователя
     *
     * @return string Response
     */
    public function actionIndex($id)
    {
        $user = UserAvatar::findOne($id);

        return $this->render([
            'user' => $user,
        ]);
    }
    /**
     * Выводит профиль пользователя
     *
     * @param int $id идентификатор пользователя
     *
     * @return string Response
     */
    public function actionItem($id)
    {
        $user = UserAvatar::findOne($id);

        return $this->render([
            'user' => $user,
        ]);
    }

    /**
     * Показывает счет
     */
    public function actionBilling($id)
    {
        $billing = UserBill::findOne($id);


        return $this->render([
            'billing' => $billing,
        ]);
    }


}
