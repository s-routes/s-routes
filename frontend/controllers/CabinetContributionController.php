<?php

namespace avatar\controllers;

use avatar\controllers\actions\DefaultAjax;
use common\models\Contribution;
use common\models\UserContribution;
use yii\web\Response;

class CabinetContributionController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    public function actions()
    {
        return [
            'add-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetContributionAddAjax'
            ],
        ];
    }

    /**
     * @param id int contribution.id
     * @return string|Response
     */
    public function actionAdd()
    {
        $model = new \avatar\models\forms\CabinetContributionAdd();

        if (\Yii::$app->request->isPost) {
            if ($model->load(\Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     * @param id int contribution.id
     *
     * @return string|Response
     */
    public function actionView($id)
    {
        $user_contribution = UserContribution::findOne($id);
        $contribution = Contribution::findOne($user_contribution->contribution_id);

        return $this->render([
            'contribution'      => $contribution,
            'user_contribution' => $user_contribution,
        ]);
    }

}
