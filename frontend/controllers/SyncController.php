<?php
/**
 * Created by PhpStorm.
 * User: vlad
 * Date: 11/12/14
 * Time: 10:25 AM
 */

namespace avatar\controllers;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\Response;


/**
 *
 *
 * Class SyncController
 * @package cabinet\controllers
 */
class SyncController extends \avatar\base\BaseController
{
    /**
     * @var string Ключ
     */
    public $key = 'YIslvjZCMicGwFiN7ClX762lusQ0451M';

    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (Yii::$app->request->get('key') != $this->key) {
//            throw new HttpException(401, 'Не верный код', 101);
        }
    }

    /**
     * REQUEST
     * - name - string символьное обозначение БД
     *
     * @return mixed
     */
    public function actionTables()
    {
        $dbAlias = self::getParam('name', '');
        if ($dbAlias == '') $dbAlias = 'db';
        /** @var \yii\db\Connection $dbConnection */
        $dbConnection = Yii::$app->$dbAlias;

        return self::jsonSuccess($dbConnection->schema->getTableNames());
    }

    /**
     * REQUEST:
     * - db - string -  символьное обозначение БД
     * + name - string
     * - itemsPerPage - int
     * - page - int
     *
     * @return string JSON array
     * [
     *      'columns' => ['', ...],
     *      'rows' => [['',...], ...],
     * ]
     */
    public function actionTable()
    {
        $dbAlias = self::getParam('db', 'db');
        $name = self::getParam('name');
        $itemsPerPage = self::getParam('itemsPerPage', 100);
        $page = self::getParam('page', 1);

        /** @var \yii\db\Connection $dbConnection */
        $dbConnection = Yii::$app->$dbAlias;
        $columns = $dbConnection->schema->getTableSchema($name);
        $columns = ArrayHelper::toArray($columns->columns);

        $rows = (new Query())
            ->select('*')
            ->from($name)
            ->limit($itemsPerPage)
            ->offset(($page - 1) * $itemsPerPage)
            ->all($dbConnection);
        $rows2 = [];
        foreach ($rows as $row) {
            $r = [];
            foreach ($row as $k => $v) {
                $r[] = $v;
            }
            $rows2[] = $r;
        }

        return self::jsonSuccess([
            'columns' => $columns,
            'rows'    => $rows2,
        ]);
    }

    /**
     * Выдает список файлов
     * Поддерживает только один уровень вложенности
     *
     * REQUEST:
     * + path - string - путь от корня проекта, например /cabinet/web/uploads/banners
     */
    public function actionFolder()
    {
        $path = self::getParam('path');
        if (is_null($path)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра');
        }
        $pathFull = Yii::getAlias('@cabinet/..' . $path);
        if (!file_exists($pathFull)) {
            return self::jsonErrorId(103, 'Пути не сушествует');
        }
        $fileList = scandir($pathFull);
        $rows = [];
        foreach ($fileList as $i) {
            if (!in_array($i, ['.', '..'])) {
                $rows[] = $i;
            }
        }

        return self::jsonSuccess($rows);
    }

    /**
     * Выдает файл с контентом
     * Поддерживает только один уровень вложенности
     *
     * REQUEST:
     * + path - string - путь от корня проекта, например /cabinet/web/uploads/banners
     * + offset - int - индекс на то какой файл нужен, подразумевая что сортировака по возрастанию по названию файла. начинается от 0.
     *
     * @return string JSON array
     * {
     *      'name': string - только имя
     *      'content': array (base64) - закодированный в Base64 данные файла разбитые на строки по 1000 символов
     * }
     *
     */
    public function actionFile()
    {
        $path = self::getParam('path');
        if (is_null($path)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра');
        }
        $offset = self::getParam('offset');
        if (is_null($offset)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра');
        }
        $pathFull = Yii::getAlias('@cabinet/..' . $path);
        if (!file_exists($pathFull)) {
            return self::jsonErrorId(103, 'Пути не сушествует');
        }
        $fileList = scandir($pathFull);
        if (!isset($fileList[$offset + 2])) {
            return self::jsonErrorId(104, 'Нет файла по указанному индексу');
        }
        $fileName = $fileList[$offset + 2];
        $filePath = $pathFull . '/' . $fileName;
        $content = base64_encode(file_get_contents($filePath));
        $chars = 100;
        $c = (int)((strlen($content) + ($chars - 1)) / $chars);
        $rows = [];
        for ($page = 0; $page < $c; $page++) {
            $offset = $page * $chars;
            $rows[] = substr($content, $offset, $chars);
        }

        return self::jsonSuccess([
            'name'    => $fileName,
            'content' => $rows
        ]);
    }

    /**
     * Выдает файл с контентом
     *
     * REQUEST:
     * + path - string - путь до файла от корня проекта, например /cabinet/web/uploads/banners/file.jpg
     *
     * @return string JSON array
     * {
     *      'content': array (base64) - закодированный в Base64 данные файла разбитые на строки по 1000 символов
     * }
     *
     */
    public function actionFile2()
    {
        $path = self::getParam('path');
        if (is_null($path)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра');
        }
        $pathFull = Yii::getAlias('@frontend/..' . $path);
        if (!file_exists($pathFull)) {
            return self::jsonErrorId(103, 'Пути не сушествует');
        }
        $content = base64_encode(file_get_contents($pathFull));
        $chars = 100;
        $c = (int)((strlen($content) + ($chars - 1)) / $chars);
        $rows = [];
        for ($page = 0; $page < $c; $page++) {
            $offset = $page * $chars;
            $rows[] = substr($content, $offset, $chars);
        }

        return self::jsonSuccess([
            'content' => $rows
        ]);
    }

    /**
     * Выдает список файлов
     *
     * REQUEST:
     * + path - string - путь до файлов от корня проекта, например /cabinet/web/uploads/banners
     *
     * @return string JSON array
     * {
     *      '<file_path>': string - путь к файлу от корня проекта
     * }
     *
     */
    public function actionFiles()
    {
        $path = self::getParam('path');
        if (is_null($path)) {
            return self::jsonErrorId(101, 'Нет обязательного параметра');
        }
        $pathFull = Yii::getAlias('@frontend/..' . $path);
        if (!file_exists($pathFull)) {
            return self::jsonErrorId(103, 'Пути не сушествует');
        }
        $content = base64_encode(file_get_contents($pathFull));
        $chars = 100;
        $c = (int)((strlen($content) + ($chars - 1)) / $chars);
        $rows = [];
        for ($page = 0; $page < $c; $page++) {
            $offset = $page * $chars;
            $rows[] = substr($content, $offset, $chars);
        }

        return self::jsonSuccess([
            'content' => $rows
        ]);
    }
}