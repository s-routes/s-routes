<?php

namespace avatar\controllers;

use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\BinanceOrder;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\HD;
use common\models\HDtown;
use common\models\TaskOutput;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetConvert2Controller extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render([]);
    }
    /**
     */
    public function actionItem($id)
    {
        $item = ConvertOperation::findOne($id);

        return $this->render(['item' => $item]);
    }
}
