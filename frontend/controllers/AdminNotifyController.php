<?php

namespace avatar\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminNotifyController extends AdminBaseController
{
    public function actionIndex()
    {
        $model = new \avatar\models\forms\AdminNotify();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }
}
