<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\avatar\Currency;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminKorobkaController extends AdminBaseController
{
    public function actions()
    {
        return [
            'delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model' => '\avatar\models\forms\AdminKorobka',
            ],
        ];
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\AdminKorobka();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->currency_id = Currency::RUB;
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Редактирование товара
     *
     * @param int $id идентификатор товара
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\AdminKorobka::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
