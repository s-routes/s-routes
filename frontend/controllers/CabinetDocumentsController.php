<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class CabinetDocumentsController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Добавляет документ
     *
     * @return string|Response
     */
    public function actionAdd()
    {
        $model = new \avatar\models\forms\UserDocument();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted', $model->id);

            $document = UserDocument::findOne($model->id);
            $document->created_at = time();
            $document->hash = hash('sha256', file_get_contents(Yii::getAlias('@webroot' . $document->file)) . $document->data);
            $document->user_id = Yii::$app->user->id;
            $document->save();

            return $this->refresh();
        } else {
            return $this->render('add', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Добавляет документ. Шаг2
     *
     * @return string|Response
     */
    public function actionAddStep2()
    {
        $model = new \avatar\models\forms\UserDocumentStep2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            $model->action();

            return $this->refresh();
        } else {
            return $this->render('add-step2', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDownload($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        if ($doc->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш документ');
        }
        $info = pathinfo($doc->file);

        return Yii::$app->response->sendContentAsFile(file_get_contents(Yii::getAlias('@webroot' . $doc->file)), $doc->id . '.'  . $info['extension']);
    }

    /**
     * Выдает документ на скачивание
     *
     * @return string
     * @throws \cs\web\Exception
     */
    public function actionDownloadPdf($id)
    {
        $doc = UserDocument::findOne($id);
        if (is_null($doc)) {
            throw new Exception('Не найден документ');
        }
        if ($doc->user_id != Yii::$app->user->id) {
            throw new Exception('Это не ваш документ');
        }
        $info = pathinfo($doc->file);

        require \Yii::getAlias('@common/components/mpdf60/mpdf.php');
        $pdfDriver = new \mPDF();
        $filePath = \Yii::getAlias('@avatar/views/cabinet-documents/pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, ['document' => $doc]);
        $pdfDriver->WriteHTML($contentHtml);
        $contentPdf = $pdfDriver->Output('', 'S');

        return Yii::$app->response->sendContentAsFile($contentPdf, 'document' . '_' . $id . '.pdf', [
            'mimeType' => 'application/pdf',
        ]);
    }

    /**
     * Показывает документ
     *
     * @param $id
     * @return string
     * @throws Exception
     */
    public function actionView($id)
    {
        $document = UserDocument::findOne($id);
        if (is_null($document)) {
            throw new Exception('Нет такого документа');
        }

        return $this->render('view', [
            'document' => $document,
        ]);
    }
}
