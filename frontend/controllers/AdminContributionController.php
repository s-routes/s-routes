<?php

namespace avatar\controllers;

use avatar\models\forms\AdminContribution;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminContributionController extends AdminBaseController
{
    public function actionIndex()
    {
        $model = new AdminContribution();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render(['model' => $model]);
    }
}
