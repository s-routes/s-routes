<?php

namespace avatar\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminCountryController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\Country();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEditLang($id, $lang)
    {
        $model = \avatar\models\forms\CountryLang::findOne(['parent_id' => $id, 'language' => $lang]);
        if (is_null($model)) {
            $model = new \avatar\models\forms\CountryLang(['parent_id' => $id, 'language' => $lang]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Country::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        \avatar\models\forms\Country::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
