<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\TaskPrizmPullInput;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class CabinetTaskInputController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        $a = parent::actions();
        $t = [
            'add' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\PrizmInput',
                'formName' => 'PrizmInput',
            ],
            'check' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\PrizmInputCheck',
                'formName' => '',
            ],
            'close' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\PrizmInputClose',
                'formName' => '',
            ],
            'legat-add' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\LegatInput',
                'formName' => 'LegatInput',
            ],
            'legat-check' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\LegatCheck',
                'formName' => '',
            ],
            'legat-close' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\LegatClose',
                'formName' => '',
            ],
        ];

        return ArrayHelper::merge($a, $t);
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionPullInput()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $task = TaskPrizmPullInput::add([
            'user_id' => Yii::$app->user->id,
        ]);

        // Забираю SB
        $dataSBout = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::SB);
        /** @var \common\models\piramida\Wallet $walletSBout */
        $walletSBout = $dataSBout['wallet'];
        $walletSBin = Wallet::findOne(Yii::$app->params['walletSBall']);
        $amount = $walletSBout->amount;
        $txid_sb = $walletSBout->move($walletSBin, $amount, [
            'transaction' => 'Списание в связи с внеснием в пулл по задаче id=' . $task->id,
            'from'        => 'Списание в связи с внеснием в пулл по задаче id=' . $task->id,
            'to'          => 'Списание в связи с внеснием в пулл по задаче id=' . $task->id . ' для пользователя uid=' . Yii::$app->user->id,
        ]);

        // Отдаю SBP
        $walletSBPout = Wallet::findOne(Yii::$app->params['walletSBPall']);
        $dataSBPin = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::SBP);
        /** @var \common\models\piramida\Wallet $walletSBPin */
        $walletSBPin = $dataSBPin['wallet'];
        $txid_sbp = $walletSBPout->move($walletSBPin, $amount, [
            'from'        => 'Начисление в связи с внеснием в пулл по задаче id=' . $task->id . ' для пользователя uid=' . Yii::$app->user->id,
            'to'          => 'Начисление в связи с внеснием в пулл по задаче id=' . $task->id,
            'transaction' => 'Начисление в связи с внеснием в пулл по задаче id=' . $task->id,
        ]);

        $task->amount = $amount;
        $task->txid_sb = $txid_sb->id;
        $task->txid_sbp = $txid_sbp->id;
        $task->save();

        return self::jsonSuccess();
    }


}
