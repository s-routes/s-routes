<?php

namespace avatar\controllers;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use school\modules\sroutes\models\Arbitrator;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CabinetWalletController extends \avatar\controllers\CabinetBaseController
{
    public $layout = 'cabinet';

    public function actions()
    {
        return [
            'convert-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetWalletConvertAjax',
            ],
            'send2' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\forms\SendAtom2',
                'formName' => 'SendAtom2',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionItem($id)
    {
        try {
            $bill = UserBill::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        // Если не (бухгалтер или арбитр)
        if (!(Yii::$app->authManager->checkAccess($user->id, 'permission_buh') || Yii::$app->authManager->checkAccess($user->id, 'permission_arbitrator'))) {
            // Проверяю доступ
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, ['bill' => $bill]);
    }

    public function actionSend($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Счет не найден']);
        }
        // проверка на возможность отправлять средства
        $isShowSend = true;
        if ($bill->currency == \common\models\avatar\Currency::MARKET) {
            $cInt = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $UserBill2 = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cInt->id]);
            $w = \common\models\piramida\Wallet::findOne($UserBill2->wallet_id);
            if ((Yii::$app->user->identity->market_till < time()) || (Yii::$app->user->identity->market_till == 0) || ($w->amount == 0)) {
                $isShowSend = false;
            }
        }
        if (!$isShowSend) {
            return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Переводы и зачисление средств на баланс недоступны без активации аккаунта. Активируйте его покупкой любого "Пакета возможностей" ' . \yii\helpers\Html::a('здесь', 'https://topmate.one/cabinet-market/index')]);
        }

        $model = new \avatar\models\forms\SendAtom2();

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'bill'   => $bill,
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertNeiro($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\Convert(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertNeiroBinance($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertNeiroBinance(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view,[
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertMarketBinance($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertMarketBinance(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertLotBinance($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertLotBinance(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertMarket($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\Convert3(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     *
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertLot($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertLot(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     *
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvert($from, $to)
    {
        $currencyFrom = \common\models\avatar\Currency::findOne($from);
        $currencyTo = \common\models\avatar\Currency::findOne($to);
        $model = new \avatar\models\forms\ConvertAny([
            'currencyFrom' => $currencyFrom,
            'currencyTo'   => $currencyTo,
        ]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';

        return $this->render($view, [
            'model'        => $model,
            'currencyFrom' => $currencyFrom,
            'currencyTo'   => $currencyTo,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertMarketCoin($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertMarketCoin(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertLotCoin($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertLotCoin(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    /**
     * @param  int $id db.currency.id
     * @return string
     * @throws Exception
     */
    public function actionConvertNeiroCoin($id)
    {
        $c = \common\models\avatar\Currency::findOne($id);
        $model = new \avatar\models\forms\ConvertNeiroCoin(['currency' => $c]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'model'  => $model,
        ]);
    }

    public function actionConvert2($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        $view = '@avatar/views/' . $this->id . '/' . $this->action->id . '.php';
        return $this->render($view, [
            'bill'   => $bill,
        ]);
    }

    public function actionDelete()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDelete();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $result = $model->action();
        if (!$result) return self::jsonErrorId(2, 'Кошелек не пустой');

        return self::jsonSuccess();
    }

    public function actionDeleteAgree()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDeleteAgree();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }
}