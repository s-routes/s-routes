<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 18.01.2017
 * Time: 13:17
 */

namespace avatar\controllers\actions;

use common\models\UserAvatar;
use common\models\UserTelegramConnect;
use common\models\UserTelegramTemp;
use cs\Application;
use cs\services\Security;
use Yii;
use yii\base\Action;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\Response;

class TelegramCallBack extends Action
{
    public $messages = [
        'textFirst' => 'Какой язык вы предпочитаете русский или английский?
Напишите "RU" или "EN"

Which language do you prefer Russian or English?
Write "RU" or "EN"',
        'commands' => [
            'user' => [
                '/reset'      => 'Перейти в начало диалога. Сброс.',
                '/help'       => 'Вывести помощь.',
                '/disconnect' => 'Отсоединить телеграм бота от профиля Neiro-n',
                '/me'         => 'Посмотртеть информацию о себе',
            ],
            'guest' => [
                '/reset' => 'Перейти в начало диалога. Сброс.',
                '/help'  => 'Вывести помощь.',
            ],
        ],
        'disconnect' => 'Я отсоединяюсь! До скорых встреч!',
        't' => [
            'case2yes'    => 'Напишите пожалуйста ваш e-mail, который вы указывали при регистрации в Neiro-N',
            'case2no'     => 'Не страшно, давайте я вас зарегистрирую. Это просто, напишите пожалуйста ваш e-mail',
            'case2error'  => 'Можно ответить только словами «да» или «нет»',
            'case3error'  => 'Вы указываете неверный e-mail, введите пожалуйста корректный адрес почты',
            'case3str1'   => 'Отлично! Все получается!',
            'case3str2'   => 'Подтверждение почты от Telegram @Neiro_n_Nommy_bot',
            'case3error2' => 'Сожалею, но такого пользователя на платформе нет. Давайте попробуем еще раз, - напишите ваш e-mail снова',
            'case5str1'   => 'Подтверждение почты от Telegram @Neiro_n_Nommy_bot',
            'case5str2'   => 'Дополнительное письмо выслано. Получите почту и подтвердите e-mail.',
            'case5str3'   => 'Отлично! Все получается!',
            'case6error'  => 'Не верный email, введите пожалуйста корректный email',
            'case6error2' => 'Такой email уже зарегистрирован.',
            'case6error3' => 'Введите e-mail, который не зарегистрирован, или сбросьте разговор к его началу командой /reset',
            'case6str1'   => 'Зайдите на почту и подтвердите почту или напишите "выслать" и я вышлю еще раз',
            'case6str2'   => 'Подтверждение почты от Telegram @Neiro_n_Nommy_bot',
            'case8str1'   => 'Выслала. Зайдите на почту и подтвердите почту.',
            'case8str2'   => 'Зайдите на почту и подтвердите почту или напишите "выслать" и я вышлю еще раз',
            'case10str1'  => 'Доброго времени суток, уважаемый пользователь экосистемы NEIRO-N',
            'case10str2'  => 'Можно ответить только словами «ru» или «en» / You can only respond with the words «ru» or «en»',
        ],
    ];
    
    /** @var string Категория для переводов на английский */
    public $languageCategory = 'c.pucsfXhpLK';
    
    public $botCodeName = '@Neiro_n_Nommy_bot';

    public $component = 'telegram';

    /** @var string Название поля в модели UserAvatar */
    public $userField = 'telegram_chat_id';

    public $userFieldAccount = 'telegram_chat_username';

    /** @var ActiveRecord */
    public $modelTemp = '\common\models\UserTelegramTemp';

    /** @var ActiveRecord */
    public $modelConnect = '\common\models\UserTelegramConnect';

    /** @var string Основа для генерации ссылок  */
    public $scheme = 'https://neiro-n.com';

    /** @var string опции для `\common\services\Subscribe::sendArray()` */
    public $mailSendOptions = [];


    public static $languageStrategy = [
        'ru' => ['ru'],
        'en' => ['en', 'hi', 'id', 'zh'],
    ];
    
    public function run()
    {
        $userField = $this->userField;
        $userFieldAccount = $this->userFieldAccount;
        Yii::info(\yii\helpers\VarDumper::dumpAsString(Json::decode(Yii::$app->request->rawBody)), 'avatar\controllers\TelegramController::actionCallBack');

        $data = Json::decode(Yii::$app->request->rawBody);

        $component = $this->component;

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;

        if (isset($data['channel_post'])) {
            return 'ok';
        }
        if (!isset($data['message'])) {
            return 'ok';
        }
        $chat_id = $data['message']['chat']['id'];

        $language = 'ru';
        $languageStrategy = self::$languageStrategy;

        // определить а кто вообще пишет? Свой или гость?
        {
            $username = null;
            if (isset($data['message']['chat']['username'])) {
                $username = $data['message']['chat']['username'];
            }
            try {
                $user = UserAvatar::findOne([$userField => $chat_id]);

                // Пишет свой $user
                // Если язык установлен у пользователя?
                if (!Application::isEmpty($user->language)) {
                    foreach ($languageStrategy as $e => $l) {
                        if (in_array($user->language, $l)) {
                            $language = $e;
                        }
                    }
                }

            } catch (\Exception $e) {
                // Пишет гость
                $user = null;
            }
        }

        Yii::info(\yii\helpers\VarDumper::dumpAsString($user), 'avatar\TelegramCallBack::actionCallBack2');

        $textFirst = $this->messages['textFirst'];

        Yii::info([isset($data['message']['text'])], 'avatar\telegram1');
        if (isset($data['message']['text'])) {

            $text = trim($data['message']['text']);

            Yii::info([$text, $text == '/help'], 'avatar\telegram2');
            if ($text == '/help') {
                if ($user) {
                    // Пишет свой $user
                    $rows = $this->messages['commands']['user'];
                } else {
                    // Пишет гость
                    $rows =  $this->messages['commands']['guest'];
                }
                $lines = [];
                foreach ($rows as $command => $description) {
                    $lines[] = $command . ' - ' . $description;
                }
                $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => join("\n", $lines)]);
                
            } else {
                Yii::info([0], 'avatar/telegram3');
                if ($user) {

                    // Пишет свой $user
                    if ($text == '/disconnect') {
                        // удаляю прошлый статус разговора
                        $connect = $this->modelTemp::findOne(['chat_id' => $user->$userField]);
                        if (!is_null($connect)) $connect->delete();

                        // сообщаю
                        $telegram->sendMessage(['chat_id' => $user->$userField, 'text' => $this->messages['disconnect']]);

                        // сбрасываю привязку у пользователя
                        $user->$userField = null;
                        $user->$userFieldAccount = null;
                        $user->save();

                    } elseif ($text == '/me') {
                        $lines = [];
                        $lines[] = 'email: ' . $user->email;
                        $lines[] = 'id: ' . $user->id;
                        $lines[] = 'Имя: ' . $user->name_first;
                        $lines[] = 'Фамилия: ' . $user->name_last;
                        $telegram->sendMessage(['chat_id' => $user->$userField, 'text' => join("\n", $lines)]);
                    } else {
                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => 'Привет ' . $user->getName2()]);
                    }
                } else {
                    // Пишет гость
                    $userTelegram = $this->modelTemp::findOne(['chat_id' => $chat_id]);

                    if ($userTelegram) {
                        if (!Application::isEmpty($userTelegram->language)) {
                            $language = $userTelegram->language;
                        }
                        // уже общались
                        if ($text == '/reset') {
                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $textFirst]);
                            $userTelegram->status = 10;
                            $userTelegram->save();
                        } else {
                            // Это может быть и не сообщение, а я обрабатываю пока только сообщения
                            if (isset($data['message']['text'])) {
                                switch ($userTelegram->status) {
                                    case 2:
                                        if ($language == 'ru') {
                                            $yes = ['да', 'Да', 'ДА'];
                                            $no = ['нет', 'Нет'];
                                        } else {
                                            $yes = ['yes', 'Yes', 'YES'];
                                            $no = ['no', 'No', 'NO'];
                                        }

                                        if (in_array($text, $yes)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case2yes'], [], $language)]);
                                            $userTelegram->status = 3;
                                            $userTelegram->save();
                                            break;
                                        }
                                        if (in_array($text, $no)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case2no'], [], $language)]);
                                            $userTelegram->status = 6;
                                            $userTelegram->save();
                                            break;
                                        }
                                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case2error'], [], $language)]);
                                        break;

                                    case 3:
                                        if (!filter_var($text, FILTER_VALIDATE_EMAIL)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case3error'], [], $language)]);
                                            break;
                                        }
                                        try {
                                            $email = strtolower($text);
                                            $user = UserAvatar::findOne(['email' => $email]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case3str1'], [], $language)]);
                                            $userTelegram->email = strtolower($text);
                                            $userTelegram->status = 5;
                                            $userTelegram->save();

                                            $hash = Security::generateRandomString(32);
                                            $this->modelConnect::add([
                                                'username' => $username,
                                                'hash'     => $hash,
                                                'user_id'  => $user->id,
                                                'chat_id'  => $data['message']['chat']['id'],
                                            ]);

                                            // отправить почту
                                            \common\services\Subscribe::sendArray(
                                                [$email],
                                                Yii::t($this->languageCategory, $this->messages['t']['case3str2'], [], $language),
                                                'telegram_email',
                                                [
                                                    'hash'     => $hash,
                                                    'username' => $username,
                                                    'email'    => $email,
                                                    'url'      => $this->scheme . Url::to(['auth/confirm-telegram', 'hash' => $hash]),
                                                    'language' => $language,
                                                ],
                                                $this->mailSendOptions
                                            );
                                        } catch (\Exception $e) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case3error2'], [], $language)]);
                                        }
                                        break;

                                    case 5:
                                        if ($language == 'ru') {
                                            $send = ['выслать', 'Выслать'];
                                        } else {
                                            $send = ['send', 'Send'];
                                        }
                                        if (in_array($text, $send)) {
                                            // отправить почту
                                            /** @var \common\components\telegram\TelegramConnectInterface $UserTelegramConnect */
                                            $UserTelegramConnect = $this->modelConnect::findOne(['chat_id' => $chat_id]);
                                            $user = UserAvatar::findOne($UserTelegramConnect->user_id);

                                            \common\services\Subscribe::sendArray(
                                                [$user->email],
                                                Yii::t($this->languageCategory, $this->messages['t']['case5str1'], [], $language),
                                                'telegram_email',
                                                [
                                                    'hash'     => $UserTelegramConnect->hash,
                                                    'username' => $username,
                                                    'email'    => $user->email,
                                                    'url'      => $this->scheme . Url::to(['auth/confirm-telegram', 'hash' => $UserTelegramConnect->hash]),
                                                    'language' => $language,
                                                ],
                                                $this->mailSendOptions
                                            );
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case5str2'], [], $language)]);
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case5str3'], [], $language)]);
                                        }
                                        break;

                                    case 6:
                                        if (!filter_var($data['message']['text'], FILTER_VALIDATE_EMAIL)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case6error'], [], $language)]);
                                            break;
                                        }
                                        try {
                                            $user = UserAvatar::findOne(['email' => $data['message']['text']]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case6error2'], [], $language)]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case6error3'], [], $language)]);
                                        } catch (\Exception $e) {
                                            // регистрация
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case6str1'], [], $language)]);

                                            $email = strtolower($text);

                                            $hash = Security::generateRandomString(32);
                                            $this->modelConnect::add([
                                                'username' => $username,
                                                'hash'     => $hash,
                                                'chat_id'  => $data['message']['chat']['id'],
                                                'email'    => $email,
                                                'language' => $userTelegram->language,
                                            ]);

                                            // отправить почту
                                            \common\services\Subscribe::sendArray(
                                                [$email],
                                                Yii::t($this->languageCategory, $this->messages['t']['case6str2'], [], $language),
                                                'telegram_email',
                                                [
                                                    'hash'     => $hash,
                                                    'username' => $username,
                                                    'email'    => $email,
                                                    'url'      => $this->scheme . Url::to(['auth/registration-telegram', 'hash' => $hash]),
                                                    'language' => $language,
                                                ],
                                                $this->mailSendOptions
                                            );
                                            $userTelegram->status = 8;
                                            $userTelegram->save();
                                        }
                                        break;
                                    case 8:
                                        if (in_array($text, ['выслать', 'Выслать'])) {
                                            // отправить почту
                                            $UserTelegramConnect = $this->modelConnect::findOne(['chat_id' => $chat_id]);

                                            Yii::info($this->mailSendOptions, 'avatar\telegram');
                                            \common\services\Subscribe::sendArray(
                                                [$UserTelegramConnect->email],
                                                Yii::t($this->languageCategory, $this->messages['t']['case6str2'], [], $language),
                                                'telegram_email',
                                                [
                                                    'hash'     => $UserTelegramConnect->hash,
                                                    'username' => $username,
                                                    'email'    => $UserTelegramConnect->email,
                                                    'url'      => $this->scheme . Url::to(['auth/registration-telegram', 'hash' => $UserTelegramConnect->hash]),
                                                    'language' => $language,
                                                ],
                                                $this->mailSendOptions
                                            );
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case8str1'], [], $language)]);
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t($this->languageCategory, $this->messages['t']['case8str2'], [], $language)]);
                                        }
                                        break;

                                    case 10:
                                        if (in_array(strtolower($text), ['ru', 'en'])) {
                                            // установить язык
                                            if (strtolower($text) == 'ru') {
                                                $userTelegram->language = 'ru';
                                            }
                                            if (strtolower($text) == 'en') {
                                                $userTelegram->language = 'en';
                                            }
                                            $language = $userTelegram->language;
                                            $text10 = Yii::t($this->languageCategory, $this->messages['t']['case10str1'], [], $language);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $text10]);

                                            $userTelegram->status = 2;
                                            $userTelegram->save();
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $this->messages['t']['case10str2']]);
                                        }
                                        break;
                                }

                            }

                        }
                    } else {
                        // первый раз пишет
                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $textFirst]);
                        $this->modelTemp::add([
                            'username' => $username,
                            'chat_id'  => $data['message']['chat']['id'],
                            'status'   => 10,
                        ]);
                    }
                }
            }
        }

        return 'ok';
    }
}