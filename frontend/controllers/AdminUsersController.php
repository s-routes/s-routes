<?php

namespace avatar\controllers;

use avatar\base\Application;
use avatar\models\search\UserAvatar;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillSystem;
use console\controllers\MoneyRateController;
use cs\web\Exception;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\Response;

class AdminUsersController extends \avatar\controllers\AdminBaseController
{
    public static $isStatistic = false;

    public function actions()
    {
        return [
            'permissions-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\AdminUsersPermissionsAjax'
            ]
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionPermissions($id)
    {
        $item = \common\models\UserAvatar::findOne($id);

        return $this->render(['item' => $item]);
    }

    /**
     */
    public function actionTree($id)
    {
        $item = \common\models\UserAvatar::findOne($id);

        return $this->render(['user' => $item]);
    }

    /**
     */
    public function actionTreeShop($id)
    {
        $item = \common\models\UserAvatar::findOne($id);

        return $this->render(['user' => $item]);
    }

    /**
     * @param int $id идентификатор пользователя
     * @return string
     * @throws
     */
    public function actionWalletList($id)
    {
        if (!Application::isInteger($id)) {
            throw new Exception('id должен быть INT');
        }
        try {
            $user = \common\models\UserAvatar::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Не найден пользователь');
        }

        return $this->render(['user' => $user]);
    }

    /**
     */
    public function actionBinanceView($id)
    {
        return $this->render(['user' => \common\models\UserAvatar::findOne($id)]);
    }

    /**
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\UserAvatar::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form',1 );
            $model->update();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model
            ]);
        }
    }
}
