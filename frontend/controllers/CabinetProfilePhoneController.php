<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use common\models\UserAvatar;
use cs\Application;
use cs\base\BaseController;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class CabinetProfilePhoneController extends \avatar\controllers\CabinetBaseController
{
    public function actionIndex()
    {
        /** @var \avatar\models\forms\ProfilePhone $model */
        $model = \avatar\models\forms\ProfilePhone::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form');

            return $this->refresh();
        }
        else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Отправляет СМС первый раз
     *
     * REQUEST:
     * - phone: string
     *
     * @return string|Response
     */
    public function actionSend()
    {
        $sms = substr(str_shuffle('0123456789'), 0, 4);
        Yii::$app->session->set('sms', $sms);
        /** @var \common\components\sms\IqSms $smsProvider */
        $smsProvider = Yii::$app->sms;
        $phone = self::getParam('phone');
        $Str = Str::getChars($phone);
        $rows = [];
        $dig = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
        foreach ($Str as $char) {
            if (in_array($char, $dig)) {
                $rows[] = $char;
            }
        }
        $phone = join('', $rows);
        $text = $this->getText($sms);
        $ret = $smsProvider->send($phone, $text);
        Yii::info($ret, 'avatar\controllers\CabinetProfilePhoneController::actionSend');

        if (YII_ENV_PROD) return self::jsonSuccess();
        else return self::jsonSuccess(['sms' => $sms]);
    }

    /**
     * Устанавливает флаг
     *
     * REQUEST:
     * - value: string
     *
     * @return string|Response
     */
    public function actionSetFlag()
    {
        $value = Yii::$app->request->post('value');
        if (!Application::isInteger($value)) {
            throw new \Exception('Это не целое');
        }
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        if ($value == 1) {
            $user->is_show_phone = 1;
        } else {
            $user->is_show_phone = 0;
        }
        $user->save();

        return self::jsonSuccess();
    }

    /**
     * Отправляет СМС повторно
     *
     * REQUEST:
     * - sms: string
     *
     * @return string|Response
     */
    public function actionSendMore()
    {
        $sms = Yii::$app->session->get('sms');

        /** @var \common\components\sms\IqSms $smsProvider */
        $smsProvider = Yii::$app->sms;
        $phone = self::getParam('phone');
        $phone = str_replace('+', '', $phone);
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);
        $text = $this->getText($sms);
        $ret = $smsProvider->send($phone, $text);
        Yii::info($ret, 'avatar\controllers\CabinetProfilePhoneController::actionSend');

        return self::jsonSuccess();
    }

    private function getText($sms)
    {
        return Yii::t('c.0f7xruhCnE', 'Ваш код для подтверждения телефона', ['sms' => $sms]);
    }

    /**
     * AJAX
     * Проверяет СМС
     *
     * REQUEST:
     * - sms: string
     * - phone: string
     *
     * @return string|Response
     */
    public function actionCheck()
    {
        $smsForm = self::getParam('sms');
        $smsCache = Yii::$app->session->get('sms');

        if ($smsForm != $smsCache) {
            return self::jsonErrorId(101, 'Не верный код');
        }
        $phone = self::getParam('phone');
        $phone = str_replace('+', '', $phone);
        $phone = str_replace('(', '', $phone);
        $phone = str_replace(')', '', $phone);
        $phone = str_replace('-', '', $phone);

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->phone = $phone;
        $user->phone_is_confirm = 1;
        $user->save();

        return self::jsonSuccess();
    }

    /**
     * AJAX
     * Сбрасывает телефон `phone` в БД и снимает флаг проверенного телефона `phone_is_confirm`
     *
     * @return string|Response
     * @throws \cs\web\Exception
     */
    public function actionReset()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->phone = '';
        $user->phone_is_confirm = 0;
        $ret = $user->save();
        if (!$ret) throw new Exception('Не удалось сохранить');

        return self::jsonSuccess();
    }

}
