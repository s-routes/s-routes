<?php

namespace avatar\controllers;


use common\models\exchange\Deal;

class AdminDealListController extends AdminBaseController
{
    public function actionIndex()
    {
        return $this->render();
    }

    public function actionView($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        return $this->render([
            'Deal' => $Deal,
        ]);
    }

    public function actionViewCreated($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        return $this->render([
            'Deal' => $Deal,
        ]);
    }
}
