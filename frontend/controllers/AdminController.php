<?php

namespace avatar\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use common\models\avatar\UserBill;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class AdminController extends AdminBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionCron()
    {
        $password = Yii::$app->request->get('password');
        $db = [
            's_routes_prod_main',
            's_routes_prod_wallet',
        ];
        $path = '/root/s-routes/db/dumps/day';
        $rows = [];
        $p = Yii::getAlias('@frontend/../db/dumps/day');
        if (!file_exists($p)) mkdir($p);
        for ($h = 0; $h < 24; $h++) {
            for ($m = 0; $m < 60; $m=$m+5) {
                if ($h < 10) $hnull = '0'.$h;
                else $hnull = $h;
                if ($m < 10) $mnull = '0'.$m;
                else $mnull = $m;
                foreach ($db as $dbitem) {
                    $rows[] = sprintf(
                        '%s %s * * * docker exec s-routes-mysql mysqldump -uroot -p%s %s | gzip > %s/%s/%s.sql.gz',
                        $m,
                        $h,
                        $password,
                        $dbitem,
                        $path,
                        $hnull.$mnull,
                        $dbitem
                    );
                }
                if (!file_exists($p.'/'.$hnull.$mnull)) mkdir($p.'/'.$hnull.$mnull);
            }
        }

        return join($rows, "\n");
    }

    /**
     */
    public function actionTechnicalPause()
    {
        return $this->render();
    }

    /**
     */
    public function actionSetEthereumWalletRead()
    {
        $model = new \avatar\models\forms\EthereumWalletRead();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->session->setFlash('form', 1);
            $model->action();

            return $this->refresh();
        } else {
            return $this->render('set-ethereum-wallet-read', [
                'model' => $model,
            ]);
        }
    }

    /**
     */
    public function actionMailTest()
    {
        $model = new \avatar\models\forms\MailTest();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
