<?php

namespace avatar\controllers;

use avatar\controllers\actions\DefaultAjax;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\chat\Message;
use common\models\exchange\Arbitrator;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetExchangeChatController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->isGuest) return false;

                            $deal = [
                                'delete',
                            ];
                            if (in_array($action->id, $deal)) {
                                if (Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin_chat')) {
                                    return true;
                                }
                                return false;
                            }
                            $deal = [
                                'delete2',
                            ];
                            if (in_array($action->id, $deal)) {
                                $m = \common\models\exchange\ChatMessage::findOne(Yii::$app->request->post('id'));
                                if (is_null($m)) {
                                    throw new NotFoundHttpException();
                                }
                                if ($m->user_id != \Yii::$app->user->id) {
                                    return false;
                                }
                            }

                            return true;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {

        return [
            'delete' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetExchangeChatDelete',
            ],
            'delete2' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetExchangeChatDelete',
            ],
            'ban' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetExchangeChatBan',
            ],
            'delete-user' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetExchangeChatDeleteUser',
            ],
            'send2' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetExchangeChatSend',
            ],
            'send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\frontend\models\validate\ChatSend',
            ],
        ];
    }

    public function actionSend3($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if (!Yii::$app->user->can('permission_arbitrator')) {
            if ($Deal->user_id != Yii::$app->user->id and $Deal->getOffer()->user_id != Yii::$app->user->id) {
                throw new ForbiddenHttpException('Нельзя писать сюда');
            }
        }

        $Deal->trigger(Deal::EVENT_BEFORE_MESSAGE);

        // Добавляю сообщение
        $message = ChatMessage::add([
            'deal_id' => $Deal->id,
            'user_id' => Yii::$app->user->id,
            'message' => Yii::$app->request->post('message'),
        ]);

        // Отправляю уведомление maker, taker и арбитру если сделка в статусе арбитра
        {
            try {
                $users = [
                    [
                        'user_id' => $Deal->user_id,
                        'link'    => Url::to(['cabinet-exchange/deal', 'id' => $Deal->id], true),
                    ],
                    [
                        'user_id' => $Deal->getOffer()->user_id,
                        'link'    => Url::to(['cabinet-exchange/deal', 'id' => $Deal->id], true),
                    ],
                ];
                if (in_array($Deal->status, [Deal::STATUS_AUDIT_ACCEPTED])) {
                    $arbitrDeal = Arbitrator::findOne(['deal_id' => $Deal->id]);
                    $aid = $arbitrDeal->arbitrator_id;
                    $users[] = [
                        'user_id' => $aid,
                        'link'    => Url::to(['cabinet-arbitrator/item', 'id' => $Deal->id], true),
                    ];
                }
                /** @var \common\models\UserAvatar $uCurrent */
                $uCurrent = Yii::$app->user->identity;
                foreach ($users as $u1) {
                    $u = UserAvatar::findOne($u1['user_id']);
                    if (!Application::isEmpty($u->telegram_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegram;
                        $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                        $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение от пользователя', [], $language).': ' . $uCurrent->getName2() . "\n" .  Yii::$app->request->post('message')  . "\n" .  "\n" . Yii::t('c.LSZBX1ce2W', 'Ссылка на чат', [], $language).': '.$u1['link']]);
                    }
                }
            } catch (\Exception $e) {

            }
        }

        $Deal->trigger(Deal::EVENT_AFTER_MESSAGE);

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s');
        $data['user'] = [
            'id'     => Yii::$app->user->id,
            'avatar' => Yii::$app->user->identity->getAvatar(),
            'name2'  => Yii::$app->user->identity->getName2(),
        ];

        return self::jsonSuccess([
            'message' => $data,
        ]);
    }

    /**
     * REQUEST:
     * - list: string набор идентификаторов сообщений которые разделены запятой и которые не нужно включать в список
     * выборки, потому что они уже есть на странице
     *
     * @param int $id deal.id
     *
     * @return Response
     * @throws ForbiddenHttpException
     * @throws \Exception
     */
    public function actionGet($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if ($Deal->user_id != Yii::$app->user->id and $Deal->getOffer()->user_id != Yii::$app->user->id) {
            throw new ForbiddenHttpException('Нельзя писать сюда');
        }
        $list = Yii::$app->request->post('list');
        $ids = [];
        if ($list) {
            $ids = explode(',', $list);
        }
        $listMessage = ChatMessage::find()
            ->where(['deal_id' => $Deal->id])
            ->andWhere(['not', ['in', 'id', $ids]])
            ->all();
        $arr = [];
        /** @var \common\models\exchange\ChatMessage $message */
        foreach ($listMessage as $message) {
            $i = ArrayHelper::toArray($message);
            $u = UserAvatar::findOne($message->user_id);
            $i['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:d.m.Y H:i:s');
            $i['user'] = [
                'id'     => $u->id,
                'avatar' => $u->getAvatar(),
                'name2'  => $u->getName2(),
            ];
            $arr[] = $i;
        }

        return self::jsonSuccess([
            'list' => $arr,
        ]);
    }


}
