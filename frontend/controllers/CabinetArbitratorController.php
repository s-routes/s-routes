<?php

namespace avatar\controllers;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class CabinetArbitratorController extends \avatar\controllers\CabinetBaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'         => true,
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->isGuest) return false;
                            if (!Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_arbitrator')) return false;

//                            $deal = [
//                                'finish1',
//                                'finish2',
//                                'item',
//                            ];
//                            if (in_array($action->id, $deal)) {
//                                $deal = Deal::findOne(Yii::$app->request->get('id'));
//                                if (is_null($deal)) {
//                                    throw new NotFoundHttpException();
//                                }
//                                $a = Arbitrator::findOne(['deal_id' => $deal->id]);
//                                if (is_null($a)) {
//                                    throw new ForbiddenHttpException('Не найдена запись для арбитра');
//                                }
//                                if ($a->arbitrator_id != Yii::$app->user->id) {
//                                    throw new ForbiddenHttpException('Вы не являетесь арбитром данной сделки');
//                                }
//                            }
//                            $deal = [
//                                'index',
//                            ];

                            return true;
                        },
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {

        return [
            'finish1' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\forms\CabinetArbitratorFinish1',
                'formName' => 'CabinetArbitratorFinish1',
            ],
            'finish2' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\forms\CabinetArbitratorFinish2',
                'formName' => 'CabinetArbitratorFinish2',
            ],
            'rating-down1' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\CabinetArbitratorRatingDown1',
            ],
            'rating-down2' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\CabinetArbitratorRatingDown2',
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render([]);
    }

    /**
     * Принимает сделку арбитром
     *
     * @param int $id Идентификатор сделки
     *
     * @return Response
     * @throws \Exception
     */
    public function actionAccept($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        if (Arbitrator::find()->where(['deal_id' => $Deal->id])->exists()) {
            return self::jsonErrorId(403, 'Эта сделка уже разбирается другим арбитром');
        }

        $t = Yii::$app->db->beginTransaction();
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_ACCEPTED);

            $Deal->status = Deal::STATUS_AUDIT_ACCEPTED;
            $Deal->save();
            DealStatus::add(['status' => Deal::STATUS_AUDIT_ACCEPTED, 'deal_id' => $Deal->id]);

            $arbitrator = Arbitrator::add([
                'deal_id'       => $Deal->id,
                'arbitrator_id' => Yii::$app->user->id,
            ]);

            $Deal->trigger(Deal::EVENT_AFTER_AUDIT_ACCEPTED);

            $t->commit();
        }catch ( \Exception $e) {
            $t->rollBack();
        }


        return self::jsonSuccess();
    }

    public function actionItem($id)
    {
        $Deal = Deal::findOne($id);
        if (is_null($Deal)) throw new \Exception('Не найден Deal ' . $id);

        return $this->render([
            'Deal' => $Deal,
        ]);
    }
}