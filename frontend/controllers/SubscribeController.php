<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.04.2017
 * Time: 0:55
 */

namespace avatar\controllers;


use avatar\models\forms\UserAvatar;
use avatar\models\validate\SubscribeAdd;
use common\models\subscribe\Subscribe;
use common\models\subscribe\SubscribeMail;
use cs\web\Exception;
use yii\db\Query;
use yii\helpers\VarDumper;

class SubscribeController extends \avatar\base\BaseController
{



    /**
     * Отписаться от рассылки
     *
     * REQUEST:
     * + mail - string - почта
     * + type - int - тип рассылки
     * + hash - проверочный хеш
     *
     */
    public function actionUnsubscribe()
    {
        $model = new \avatar\models\validate\UnSubscribe();

        if (!$model->load(\Yii::$app->request->get(), '')) {
            throw new Exception('Не переданы данные');
        }
        if (!$model->validate()) {
            throw new Exception('Ошибки валидации ' . VarDumper::dumpAsString($model->errors));
        }
        $model->action();

        return $this->render();
    }


    /**
     * Делает рассылку писем из списка рассылки
     *
     * POST REQUEST:
     * - data
     * - key
     */
    public function actionAdd()
    {
        $model = new SubscribeAdd();
        $model->load(\Yii::$app->request->post(), '');
        if (!$model->validate()) {
            return self::jsonErrorId(101, $model->errors);
        }

        $fields = [
            'date_insert' => time(),
            'subject'     => $model->dataArray['subject'],
            'type'        => $model->dataArray['type'],
            'from_name'   => $model->dataArray['from']['name'],
            'from_mail'   => $model->dataArray['from']['mail'],
        ];
        if (isset($model->dataArray['mail']['html'])) {
            $fields['html'] = $model->dataArray['mail']['html'];
        }
        if (isset($model->dataArray['unSubscribeUrl'])) {
            $fields['un_subscribe_url'] = $model->dataArray['unSubscribeUrl'];
        }
        if (isset($model->dataArray['mail']['text'])) {
            $fields['text'] = $model->dataArray['mail']['text'];
        }

        $subscribe = Subscribe::add($fields);
        $rows = [];
        foreach ($model->dataArray['mailList'] as $mail) {
            $rows[] = [
                $subscribe->id, $mail
            ];
        }
        (new Query())->createCommand()->batchInsert(SubscribeMail::tableName(), [
            'subscribe_id',
            'mail',
        ], $rows)->execute();

        return self::jsonSuccess();
    }

    /**
     * Делает рассылку писем из списка рассылки
     *
     * POST REQUEST:
     * - data
     * - key
     */
    public function actionAdd2()
    {
        \Yii::$app->mailer
            ->compose()
            ->setFrom('god@galaxysss.ru')
            ->setTo('dram1008@yandex.ru')
            ->setSubject('asd')
            ->setTextBody('afdsdf')
            ->send();
    }

}