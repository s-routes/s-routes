<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use common\models\avatar\Currency;
use cs\Application;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminLotController extends AdminBaseController
{

    public function actions()
    {
        return [
            'delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model' => '\avatar\models\forms\Lot',
            ],
        ];
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\Lot();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $this->onAfterLoad($model) && $model->validate()) {
                $model->currency_id = Currency::RUB;
                $this->onBeforeUpdate($model);
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function onAfterLoad($model)
    {
        $model->time_start = \DateTime::createFromFormat('Y-m-d H:i', $model->time_start);
        $model->time_finish = \DateTime::createFromFormat('Y-m-d H:i', $model->time_finish);
        $model->time_raffle = \DateTime::createFromFormat('Y-m-d H:i', $model->time_raffle);

        return true;
    }

    public function onAfterLoadDb($model)
    {
        if (!Application::isEmpty($model->time_start)) {
            $model->time_start = (new \DateTime('@' . $model->time_start))->format('Y-m-d H:i');
        }
        if (!Application::isEmpty($model->time_finish)) {
            $model->time_finish = (new \DateTime('@' . $model->time_finish))->format('Y-m-d H:i');
        }
        if (!Application::isEmpty($model->time_raffle)) {
            $model->time_raffle = (new \DateTime('@' . $model->time_raffle))->format('Y-m-d H:i');
        }

        return true;
    }

    public function onBeforeUpdate($model)
    {
        $model->time_start = $model->time_start->format('U');
        $model->time_finish = $model->time_finish->format('U');
        $model->time_raffle = $model->time_raffle->format('U');
        return true;
    }

    /**
     *
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Редактирование товара
     *
     * @param int $id идентификатор товара
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Lot::findOne($id);
        $this->onAfterLoadDb($model);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $this->onAfterLoad($model) && $model->validate()) {
                $this->onBeforeUpdate($model);
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }
}
