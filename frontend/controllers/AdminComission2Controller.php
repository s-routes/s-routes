<?php

namespace avatar\controllers;

use common\models\avatar\Currency;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminComission2Controller extends AdminBaseController
{
    public static $currencyList = [
        Currency::TRX,
        Currency::ETH,
        Currency::BTC,
        Currency::LTC,
        Currency::BNB,
        Currency::DASH,
        Currency::DAI,
        Currency::DOGE,

        Currency::USDT,

        Currency::LOT,
        Currency::NEIRO,
        Currency::PZM,
        Currency::MARKET,
    ];

    public function actionIndex()
    {
        $model = new \avatar\models\forms\AdminComission2();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {

                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }
}
