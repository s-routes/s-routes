<?php

namespace avatar\controllers;

use app\services\Subscribe;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use avatar\modules\UniSender\UniSender;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\NewsItem;
use app\models\Chenneling;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\SiteUpdate;

class AdminBlogController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_content'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new BlogItem();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);

            $i = \common\models\BlogItem::findOne($model->id);

            /** @var \avatar\services\CustomizeService $CustomizeService */
            $CustomizeService = Yii::$app->CustomizeService;
            $i->company_id = $CustomizeService->getParam1('id');

            $i->created_at = time();
            $i->save();

            return $this->refresh();
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }


    public function actionEdit($id)
    {
        $model = BlogItem::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * AJAX
     * Формирует данные для рассылки
     *
     * @param integer $id - идентификатор послания
     *
     * @return string
     */
    public function actionSubscribe($id)
    {
        $item = \common\models\BlogItem::findOne($id);
        if (is_null($item)) {
            return self::jsonErrorId(101, 'Не найдена новость');
        }

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = Yii::$app->mailer;
        $html = $mailer->render('html/' . 'subscribe/blog-item', ['newsItem' => $item], 'layouts/html/subscribe');

        $provider = new UniSender();
        try {
            $result = $provider->_call('createEmailMessage', [
                'sender_name'   => 'AvatarNetwork',
                'sender_email'  => 'god@avatar-bank.com',
                'subject'       => $item->name,
                'body'          => $html,
                'list_id'       => \common\models\UserAvatar::SUBSCRIBE_UNISERDER_BLOG,
                'generate_text' => 1,
            ]);
        } catch (\Exception $e) {
            return self::jsonErrorId(101, $e->getMessage());
        }

        $message_id = $result['message_id'];
        $result = $provider->_call('createCampaign', [
            'message_id' => $message_id,
            'track_read' => 1
        ]);

        $item->is_send = 1;
        $item->save();

        return self::jsonSuccess($result);
    }

    public function actionDelete($id)
    {
        BlogItem::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
