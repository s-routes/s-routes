<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$config = [
    'id'                                              => 'sroutes-frontend',
    'basePath'                                        => dirname(__DIR__),
    'bootstrap'                                       => ['log'],
    'controllerNamespace'                             => 'avatar\controllers',
    'vendorPath'                                      => dirname(dirname(__DIR__)) . '/vendor',
    'language'                                        => 'ru',
    'sourceLanguage'                                  => 'ru',
    'timeZone'                                        => 'Etc/GMT-3',
    'catchAll'                                        => require('catchAll.php'),
    'components'                                      => [
        'session'         => [
            'timeout' => 60*60*24*365,
            'class'   => 'yii\web\Session',
        ],
        'urlManager'         => [
            'rules' => [
                'user/<id:\\d+>' => 'user/item',
            ],
        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'default'  => 'ru',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'basePath'        => '/application/public_html/assets',
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            'class'                  => '\yii\web\Request',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],

    ],
    'params'                                          => $params,
    'controllerMap'                                   => [
        'upload'  => '\common\widgets\HtmlContent\Controller',
        'upload3' => '\common\widgets\FileUploadMany2\UploadController',
        'upload4' => '\iAvatar777\assets\JqueryUpload1\Upload2Controller',
    ],
    'on beforeRequest'                                => function ($event) {
        Yii::$app->languageManager->onBeforeRequest();
        Yii::$app->onlineManager->onBeforeRequest();
        Yii::$app->monitoring->onBeforeRequest();
//        if (!YII_ENV_DEV) \common\models\statistic\ServerRequest::add();
        if (!Yii::$app->user->isGuest) {
            if (\cs\Application::isEmpty(Yii::$app->user->identity->language) or Yii::$app->user->identity->language != Yii::$app->language) {
                Yii::$app->user->identity->language = Yii::$app->language;
            }
            Yii::$app->user->identity->last_action = time();
            Yii::$app->user->identity->save();
            if (!\cs\Application::isEmpty(Yii::$app->user->identity->time_zone)) {
                Yii::$app->setTimeZone(Yii::$app->user->identity->time_zone);
            }
        }

        // Сохраняю реферальный параметр в сессию
        $value = \Yii::$app->request->get('partner_id');
        if (!is_null($value)) {
            try {
                $user = \common\models\UserAvatar::findOne($value);
                if ($user->is_referal_program) {
                    \Yii::$app->session->set('ReferalProgram', ['partner_id' => $value]);
                }
            } catch (\Exception $e) {

            }
        }
    },
    'on ' . \yii\web\Application::EVENT_AFTER_REQUEST => function ($event) {
        Yii::$app->monitoring->onAfterRequest();
    }
];

if (env('MEMCACHE_ENABLED', false)) {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_HOST', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
} else {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\FileCache',
    ];
}

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
