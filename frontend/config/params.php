<?php

return [
    'timeOnline'         => 60*5,
    'mailList'           => [
        'contact' => 'god@avatar-bank.com',
    ],

    'mailer'                        => [
        // адрес который будет указан как адрес отправителя для писем и рассылок
        'from' => [
            env('MAILER_FROM_EMAIL') => env('MAILER_FROM_NAME'),
        ],
    ],

    'widgetFileUpload7' => [
        'uploadDirectory'               => '@webroot/u',
        'inputName'                     => 'uploadfile',
        'Origin'                        => '*',
        'Access-Control-Allow-Origin'   => '*',
    ],
];
