<?php
//true
if (!isset($_SERVER['HTTP_HOST'])) {
    return null;
}
if (in_array($_SERVER['HTTP_HOST'], ['control.neiro-n.com', 'localhost:3080'])) {
    return null;
} else {
    return ['offline/index'];
}
