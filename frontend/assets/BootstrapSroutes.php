<?php
/**
 */

namespace avatar\assets;

use yii\web\AssetBundle;

/**
 */
class BootstrapSroutes extends AssetBundle
{
    public $css = [
        '/css/bootstrap-sroutes.css'
    ];

    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
