<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace avatar\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Clipboard extends AssetBundle
{
    public $css = [
    ];
    public $js = [
        '/files/clipboard.js-master/dist/clipboard.min.js'
    ];
    public $depends = [
        '\yii\web\JqueryAsset',
    ];
}
