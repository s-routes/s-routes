<?php
namespace avatar\assets\SocketIO;

use yii\web\AssetBundle;


/**

 */
class Asset extends AssetBundle
{
    public static function getHost()
    {
        return env('SOCKETIO_SERVER');
    }

    public function init()
    {
        $serverName = self::getHost();

        \Yii::$app->view->registerJsFile($serverName . '/socket.io/socket.io.js', ['depends' => ['yii\web\JqueryAsset']]);
    }

}