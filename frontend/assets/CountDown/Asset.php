<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace avatar\assets\CountDown;

use cs\services\VarDumper;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath  = '@avatar/assets/CountDown/src';
    public $css     = [
        'index.css',
    ];
    public $js      = [
        'jquery.downCount.js',
    ];
    public $depends = [
        'yii\web\JQueryAsset',
    ];
}
