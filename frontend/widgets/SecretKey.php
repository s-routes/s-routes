<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets;

use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class SecretKey extends \yii\widgets\InputWidget
{
    public function run()
    {
        $attribute = $this->attribute;
        $strLen = strlen($this->model->$attribute);

        $rows = [];

        $rows[] = Html::input('text', Html::getInputName($this->model, $this->attribute), str_repeat('-', $strLen), [
            'class'     => 'form-control',
            'id'        => Html::getInputId($this->model, $this->attribute),
            'disabled'  => 'disabled',
            'data'  => [
                'value'  => $this->model->$attribute,
                'hidden' => str_repeat('-', $strLen),
            ],
        ]);
        $rows[] = Html::tag(
            'span',
            Html::button(Html::tag('i', null, ['class' => 'glyphicon glyphicon-eye-close']), ['class' => 'btn btn-default', 'id' => Html::getInputId($this->model, $this->attribute) . '-button']),
            ['class' => 'input-group-btn']
        );
        $id1 = Html::getInputId($this->model, $this->attribute);
        $id2 = Html::getInputId($this->model, $this->attribute) . '-button';
        \Yii::$app->view->registerJs(<<<JS
$('#{$id2}').click(function(e) {
    var i1 = $('#{$id1}');
    var i2 = $(this).find('i');
    if (i2.attr('class') == 'glyphicon glyphicon-eye-close') {
        i2.attr('class', 'glyphicon glyphicon-eye-open');
        i1.val(i1.data('value'));
    } else {
        i2.attr('class', 'glyphicon glyphicon-eye-close');
        i1.val(i1.data('hidden'));
    }
});
JS
        );

        return Html::tag('div', join('', $rows), ['class' => 'input-group']);
    }
}