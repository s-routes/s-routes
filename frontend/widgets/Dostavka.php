<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets;

use common\models\shop\DeliveryItem;
use common\models\shop\ProductKorobka;
use common\widgets\FileUpload7\FileUpload;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Dostavka extends \yii\widgets\InputWidget
{
    /** @var  \common\models\shop\ProductKorobka[] */
    public $rows;

    public function run()
    {
        $attribute = $this->attribute;
        \Yii::$app->session->set('\avatar\widgets\Dostavka', Html::getInputName($this->model, $attribute));


        \Yii::$app->view->registerJS(<<<JS
$('.rowTable').click(function() {
    var id =  $(this).data('id');
    $(this).parent().find('input').removeProp('checked');
    $(this).find('input').prop('checked', 'checked');
});
JS
        );

        return \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => DeliveryItem::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => '',
                    'content' => function ($item) {
                        return Html::radio(\Yii::$app->session->get('\avatar\widgets\Dostavka'), false, ['value' => $item->id]);
                    }
                ],
                'name',
            ],
        ]);
    }
}