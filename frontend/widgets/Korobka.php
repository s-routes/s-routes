<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 27.09.2016
 * Time: 17:24
 */

namespace avatar\widgets;

use common\models\avatar\Currency;
use common\models\CurrencyIO;
use common\models\shop\ProductKorobka;
use common\widgets\FileUpload7\FileUpload;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Korobka extends \yii\widgets\InputWidget
{
    /** @var  \common\models\shop\ProductKorobka[] */
    public $rows;

    public function run()
    {
        $attribute = $this->attribute;
        \Yii::$app->session->set('\avatar\widgets\Korobka', Html::getInputName($this->model, $attribute));


        \Yii::$app->view->registerJS(<<<JS
$('.rowTable').click(function() {
    var id =  $(this).data('id');
    $(this).parent().find('input').removeProp('checked');
    $(this).find('input').prop('checked', 'checked');
});
JS
        );

        $h = '<p>'.Html::radio(\Yii::$app->session->get('\avatar\widgets\Korobka'), true, ['value' => '', 'id' => 'Korobka']).
        Html::tag('label', 'Ничего не выбрано', ['for' => 'Korobka', 'style' => 'margin-left: 20px;']).'</p>'
        ;

        return $h.\yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => ProductKorobka::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'showHeader' => false,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => '',
                    'content' => function ($item) {
                        return Html::radio(\Yii::$app->session->get('\avatar\widgets\Korobka'), false, ['value' => $item->id]);
                    }
                ],
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {

                        return Html::img(FileUpload::getFile($item['image'], 'crop'), [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'description',
                [
                    'header'  => 'Цена',
                    'content' => function ($item) {
                        $currencyExt = Currency::findOne($item['currency_id']);
                        $CIO = CurrencyIO::findFromExt($currencyExt->id);

                        return \common\models\piramida\Currency::getValueFromAtom($item['price'], $CIO->currency_int_id);
                    }
                ],
                [
                    'header'  => 'Цена',
                    'content' => function ($item) {
                        $currencyExt = Currency::findOne($item['currency_id']);

                        return '<span class="label label-info">'.$currencyExt->code.'</span>';
                    }
                ],
            ],
        ]);
    }
}