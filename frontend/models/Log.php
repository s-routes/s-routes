<?php

namespace avatar\models;

use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

class Log extends ActiveRecord
{
    public static  function tableName()
    {
        return 'log';
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

}