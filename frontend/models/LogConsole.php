<?php

namespace avatar\models;

use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

class LogConsole extends ActiveRecord
{
    public static  function tableName()
    {
        return 'log_console';
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

}