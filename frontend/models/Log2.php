<?php

namespace avatar\models;

use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;

class Log2 extends ActiveRecord
{
    public static  function tableName()
    {
        return 'log2';
    }

    public static function getDb()
    {
        return \Yii::$app->dbStatistic;
    }

}