<?php

namespace avatar\models;

use cs\services\Security;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string code
 */
class UserEnter extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_enter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'id'], 'required'],
            [['code'], 'string', 'max' => 60],
            [['id'], 'integer', 'min' => 1],
        ];
    }
}
