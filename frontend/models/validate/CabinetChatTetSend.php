<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ChatList;
use common\models\ChatRoom;
use common\models\ChatTet;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\base\Theme;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetChatTetSend extends Model
{
    public $message;

    /** @var  int */
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],

            ['message', 'required'],
            ['message', 'string'],
        ];
    }

    /**
     */
    public function action()
    {
        // Генерирую сообщение HTML если там есть сссылки
        $htmlMessage = $this->getHtml($this->message);

        // Добавляю сообщение
        $message = \common\models\ChatMessage2::add([
            'room_id' => $this->id,
            'user_id' => Yii::$app->user->id,
            'message' => $htmlMessage,
        ]);
        $html = Yii::$app->view->renderFile('@frontend/views/cabinet-exchange/message.php', ['message' => $message]);

        // Обглвдяю время последнего сообщения в комнате
        $room = ChatRoom::findOne($this->id);
        $room->last_message = time();
        $room->save();

        $chat = ChatTet::findOne(['room_id' => $this->id]);

        // Уведомляю по телеграму
        try {
            $this->notificateTelegram($chat);
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'avatar\models\validate\CabinetChatTetSend::action');
        }

        return [
            'message' => [
                'object'        => $message,
                'html'          => $html,
                'timeFormatted' => Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s'),
            ],
            'user'    => [
                'id'       => Yii::$app->user->id,
                'avatar'   => Yii::$app->user->identity->getAvatar(),
                'name2'    => Yii::$app->user->identity->getName2(),
            ],
        ];
    }

    /**
     * @param  \common\models\ChatTet $chat
     * @throws \yii\base\Exception
     */
    private function notificateTelegram($chat)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        /** @var \common\models\UserAvatar $uCurrent */
        $uCurrent = Yii::$app->user->identity;
        $u1 = UserAvatar::findOne($chat->user1_id);
        $u2 = UserAvatar::findOne($chat->user2_id);

        if (!Application::isEmpty($u1->telegram_chat_id)) {
            $telegram->sendMessage(['chat_id' => $u1->telegram_chat_id, 'text' => 'Сообщение от пользователя ' . $uCurrent->getName2() . ': ' . "\n". $this->message]);
        }
        if (!Application::isEmpty($u2->telegram_chat_id)) {
            $telegram->sendMessage(['chat_id' => $u2->telegram_chat_id, 'text' => 'Сообщение от пользователя ' . $uCurrent->getName2() . ': ' . "\n". $this->message]);
        }
    }

    /**
     * Преобразовывает text в HTML формат
     * перенос строки в <br>
     * и ссылки начинающиеся на https://neuro-n.com в ссылки
     *
     * @return string
     */
    private function getHtml($text)
    {
        $rows = explode("\n", $text);
        $rows2 = [];
        foreach ($rows as $row) {
            $arr = explode(' ', $row);
            $arr2 = [];
            foreach ($arr as $word) {
                $w = trim($word);
                if (StringHelper::startsWith($w, 'https://neiro-n.com')) {
                    $w = Html::a($w, $w, ['target' => '_blank']);
                    $arr2[] = $w;
                } else {
                    $arr2[] = Html::encode($w);
                }
            }
            $rows2[] = join(' ', $arr2);
        }

        return join('<br>', $rows2);
    }
}
