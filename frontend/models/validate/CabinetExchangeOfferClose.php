<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetExchangeOfferClose extends Model
{
    /** @var  int offer.id */
    public $id;

    /** @var \common\models\exchange\Offer */
    public $offer;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyOffer'],
        ];
    }

    /**
     *
     */
    public function verifyOffer($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $o = Offer::findOne($this->id);
            if (is_null($o)) {
                $this->addError($attribute, 'Не найдено предложение');
                return;
            }
            if ($o->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваше предложение');
                return;
            }
            $this->offer = $o;
        }
    }

    /**
     */
    public function action()
    {
        $this->offer->is_hide = 1;
        $this->offer->save();
    }
}
