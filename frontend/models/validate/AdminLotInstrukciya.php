<?php

namespace avatar\models\validate;

use common\models\Config;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use cs\services\Url;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminLotInstrukciya extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'lot_instrukciya';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['page_id', 'required'],
            ['page_id', 'integer'],
        ];
    }

}
