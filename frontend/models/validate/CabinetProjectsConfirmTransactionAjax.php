<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetProjectsConfirmTransactionAjax extends Model
{
    /** @var string */
    public $txid;

    /** @var int */
    public $pay_system_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['txid', 'required'],
            ['txid', 'string'],
            ['txid', 'validateTX'],

            ['pay_system_id', 'required'],
            ['pay_system_id', 'integer'],
            ['pay_system_id', 'exist', 'targetClass' => '\common\models\PaySystem', 'targetAttribute' => 'id'],
        ];
    }

    /**
     *
     */
    public function validateTX($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $request = RequestTokenCreate::findOne($this->id);
            if (is_null($request)) {
                $this->addError($attribute, 'Не найдена заявка');
                return;
            }
            if ($request->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваша заявка');
                return;
            }
            $this->request = $request;
        }
    }

    /**
     */
    public function action()
    {
        // Подтверждает транзацию
        $paySystem = PaySystem::findOne($this->pay_system_id);

        $class = $paySystem->getClass();
    }
}
