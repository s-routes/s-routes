<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\PassportLink;
use common\models\piramida\Wallet;
use common\models\RequestTokenCreate;
use common\models\shop\Request;
use common\models\TaskOutput;
use common\models\TaskOutputHistory;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetlLastSeeKolokol extends Model
{
    /** @var int идентификатор заявки \common\models\TaskPrizmOutput */
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->last_see_kolokol = time();
        $user->save();

        return 1;
    }
}
