<?php

namespace avatar\models\validate;

use common\models\Config;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use cs\services\Url;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminPageFileDeleteAjax extends \iAvatar777\services\FormAjax\Model
{
    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['url', 'required'],
            ['url', 'string'],
//            ['url', 'url'],
            ['url', 'fileExist'],
        ];
    }

    public function fileExist($a, $p)
    {
        if (!$this->hasErrors()) {
            $url = new Url($this->url);
            $path = Yii::getAlias('@frontend/../public_html' . $url->path);
            if (!file_exists($path)) {
                $this->addError($a, 'Нет такого файла');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $url = new Url($this->url);
        $path = Yii::getAlias('@frontend/../public_html' . $url->path);
        unlink($path);

        return 1;
    }

}
