<?php

namespace avatar\models\validate;

use common\models\Config;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminTrustDelete extends Model
{
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
        ];
    }

    public function action()
    {
        $ids = \common\models\Config::get('trusted-users');
        if (\cs\Application::isEmpty($ids)) $ids = [];
        else $ids = Json::decode($ids);
        $new = [];
        foreach ($ids as $id) {
            if ($id == $this->id) {

            } else {
                $new[] = $id;
            }
        }

        Config::set('trusted-users', Json::encode($new));

        return 1;
    }

}
