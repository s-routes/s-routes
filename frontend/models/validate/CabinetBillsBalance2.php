<?php

namespace avatar\models\validate;

use avatar\models\forms\Security;
use avatar\models\Wallet;
use avatar\models\WalletETC;
use avatar\models\WalletETH;
use avatar\modules\ETH\ServiceEtherScan;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillCrypto;
use common\models\CurrencyCrypto;
use common\models\UserAvatar;
use common\models\UserSeed;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;

/**
 * password_type здесь нельзя задать в открытом виде так как только зашифрованный вид, потому что введен новый стандарт повышающий шифрование
 */
class CabinetBillsBalance2 extends Model
{
    public $currency_id;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['currency_id', 'required'],
            ['currency_id', 'integer'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль от кабинета не верный');
                return;
            }
        }
    }

    /**
     * Создает кошелек
     *
     * @return int
     *
     * @throws
     */
    public function action()
    {
        switch ($this->currency_id) {

            case CurrencyCrypto::ETH:
                $bill = UserBillCrypto::findOne([
                    'user_id'      => Yii::$app->user->id,
                    'currency'     => $this->currency_id,
                    'mark_deleted' => 0,
                ]);
                $d1 = new ServiceEtherScan();
                $d = $d1->getBalance($bill->address);
                $ret = [
                    'code'              => 'ETH',
                    'amountWei'         => $d[0]['balance'],
                    'amountFormatted'   => Yii::$app->formatter->asDecimal($d[0]['balance']/ pow(10, 18), 8 ),
                    'bcpow'             => bcpow(10, 18),
                ];
                break;
            case CurrencyCrypto::LEGAT:
                $bill = UserBillCrypto::findOne([
                    'user_id'      => Yii::$app->user->id,
                    'currency'     => $this->currency_id,
                    'mark_deleted' => 0,
                ]);
                $d1 = new ServiceEtherScan();
                $d = $d1->tokenGetBalance('0xa5f3cec958d038150dc313b8846fec04339d099b', $bill->address);
                $ret = [
                    'code'            => 'LEGAT',
                    'amountWei'       => $d,
                    'amountFormatted' => Yii::$app->formatter->asDecimal($d / pow(10, 18), 2 ),
                ];
                break;

        }

        return $ret;
    }
}
