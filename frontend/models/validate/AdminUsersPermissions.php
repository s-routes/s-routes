<?php

namespace avatar\models\validate;

use common\models\SendLetter;
use common\models\TaskInput;
use common\models\TaskPrizmInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminUsersPermissions extends Model
{
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        $this->task->status = 2;
        $ret = $this->task->save();

        return $ret;
    }
}
