<?php

namespace avatar\models\validate;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\TaskInput;
use common\models\TaskLegatInput;
use common\models\TaskPrizmInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ActiveInputCheck extends Model
{
    public $id;

    /** @var TaskInput */
    public $task;

    /** @var CurrencyIO */
    public $c;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateAccount'],
        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            $task = TaskInput::findOne($this->id);

            if (is_null($task)) {
                $this->addError($a, 'не найдена задача');
                return;
            }
            if ($task->status != 0) {
                $this->addError($a, 'Задача закрыта');
                return;
            }
            if ($task->user_id != Yii::$app->user->id) {
                $this->addError($a, 'Это не ваша задача');
                return;
            }

            $this->task = $task;
        }
    }

    /**
     * @return array
     * [
     *              'status' => int
     *              0 - Ничего не найдено
     *              1 - Найдено и зачислено
     * 'amount'                => $task->amount,
     * 'amount_formatted'      => Yii::$app->formatter->asDecimal($task->amount / 100, 2),
     * 'code'                  => $cur->code,
     *
     *              2 - Пользователь указал в задаче транзакцию а она уже была обработана как положительная
     * @throws
     */
    public function action()
    {
        $c = CurrencyIO::findOne($this->task->currency_io_id);
        $this->c = $c;

        /** @var \avatar\services\currency\CurrencyInterface $function_check */
        $function_check = $c->function_check;
        $ret = $function_check::check($this->task, $c);
        if ($ret['status'] == 1) {
            return $this->action2($ret['value'], $ret['transactionHash']);
        }

        return $ret;
    }

    /**
     * @param int $amountNQT кол-во копеек сколько надо зачислить
     * @param string $txid
     * @return array
     * @throws \Exception
     */
    private function action2($amountNQT, $txid)
    {
        $task = $this->task;
        $task->amount = $amountNQT;

        // Ищу кошелек пользователя
        $d = UserBill::getInternalCurrencyWallet($this->c->currency_int_id);
        /** @var \common\models\piramida\Wallet $walletIn */
        $walletIn = $d['wallet'];

        // Ищу кошелек откуда зачислять
        $walletOut = Wallet::findOne($this->c->main_wallet);
        $txid_int = $walletOut->move($walletIn, $task->amount,
            Json::encode([
                'Зачисление на основании задачи id={task}',
                [
                    'task' => $task->id
                ]
            ])
        );

        // Сохраняю данные в задачу о положительном завершении
        $task->txid = $txid;
        $task->txid_internal = $txid_int->id;
        $task->status = 1;
        $task->finished_at = time();
        $task->save();

        $cur = \common\models\piramida\Currency::findOne($this->c->currency_int_id);

        return [
            'status'                => 1,
            'amount'                => $task->amount,
            'amount_formatted'      => Yii::$app->formatter->asDecimal(bcdiv($task->amount, bcpow(10, $cur->decimals), $cur->decimals), $cur->decimals),
            'code'                  => $cur->code,
        ];
    }
}
