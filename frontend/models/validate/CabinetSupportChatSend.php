<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\NeironSupport;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class CabinetSupportChatSend extends Model
{
    public $message;

    /** @var  int */
    public $room_id;

    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['room_id', 'required'],
            ['room_id', 'integer'],

            ['message', 'string'],

            ['file', 'string'],
        ];
    }

    /**
     */
    public function action()
    {
        $message = ChatMessage2::add([
            'room_id' => $this->room_id,
            'user_id' => Yii::$app->user->id,
            'message' => Json::encode([
                'text' => $this->message,
                'file' => $this->file,
            ]),
        ]);

        $html = Yii::$app->view->renderFile('@shop/views/cabinet-exchange/message.php', ['message' => $message]);

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s');

        $room = \common\models\ChatRoom::findOne($this->room_id);
        $room->last_message = time();
        $room->save();

        $supportShop = NeironSupport::findOne(['room_id' => $this->room_id]);
        $supportShop->last_message = time();
        $supportShop->save();

        // Уведомляю по телеграму
        try {
            $this->notificateTelegram();
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'shop\models\validate\CabinetSupportChatSend::action');
        }

        return [
            'message' => [
                'object'        => $data,
                'html'          => $html,
                'timeFormatted' => Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s'),
            ],
            'user'    => [
                'id'       => Yii::$app->user->id,
                'avatar'   => Yii::$app->user->identity->getAvatar(),
                'name2'    => Yii::$app->user->identity->getName2(),
            ],
        ];
    }

    private function notificateTelegram()
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        // Если пишет поддержка
        if (Yii::$app->requestedRoute == 'admin-support/send') {
            $SupportShop = NeironSupport::findOne(['room_id' => $this->room_id]);
            $uid = $SupportShop->user_id;

            /** @var \common\models\UserAvatar $uCurrent */
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                $link = Url::to(['cabinet-support/chat'], true);
                $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение от поддержки', [], $language). ': ' . "\n". $this->message  . "\n" .  "\n" . 'Ссылка на чат: '.$link]);
            }
        }

        // Если пишет клиент
        if (Yii::$app->requestedRoute == 'cabinet-support/send') {
            $adminList = Yii::$app->authManager->getUserIdsByRole('role_admin');
            $buhList = Yii::$app->authManager->getUserIdsByRole('role_buh');
            $all = ArrayHelper::merge($adminList, $buhList);
            $all = array_unique($all);

            Yii::info($all, 'avatar\shop\models\validate\CabinetSupportSend::notificateTelegram');

            foreach ($all as $uid) {
                $u = UserAvatar::findOne($uid);
                if (!Application::isEmpty($u->telegram_chat_id)) {
                    /** @var \common\models\UserAvatar $uCurrent */
                    $uCurrent = Yii::$app->user->identity;
                    $uid = $uCurrent->id;
                    $link = Url::to(['admin-support/chat', 'id' => $uid], true);
                    $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                    $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение в поддержку от пользователя', [], $language).' ' . $uCurrent->getName2() . ': ' . "\n". $this->message  . "\n" .  "\n" . 'Ссылка на чат: ' . $link]);
                }
            }
        }


    }
}
