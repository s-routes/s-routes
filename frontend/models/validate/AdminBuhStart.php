<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\TaskOutput;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class AdminBuhStart extends Model
{
    /** @var int идентификатор заявки \common\models\TaskPrizmOutput */
    public $id;

    /** @var  TaskOutput */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],
        ];
    }

    public function validateRequest($attr, $params)
    {
        if (!$this->hasErrors()) {
            $request = TaskOutput::findOne($this->id);
            if (is_null($request)) {
                $this->addError($attr, 'Заявка не найдена');
                return;
            }
            if (!Application::isEmpty($request->buh_user_id)) {
                $this->addError($attr, 'Заявка уже обрабатывается другим бухгалтером');
                return;
            }

            $this->task = $request;
        }
    }

    /**
     */
    public function action()
    {
        // Прикрепляю пользователя-бухгалтера кто обрабатывает заявку
        $this->task->buh_user_id = Yii::$app->user->id;
        $this->task->status = TaskOutput::STATUS_ACCEPTED;
        $this->task->save();

        return 1;
    }
}
