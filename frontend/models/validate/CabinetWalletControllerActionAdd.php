<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\piramida\Wallet;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetWalletControllerActionAdd extends Model
{
    /** @var int */
    public $name;

    /** @var int */
    public $currency_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {

        return [
            ['name', 'required'],
            ['name', 'string', 'max' => 100],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'exist', 'targetAttribute' => 'id', 'targetClass' => '\common\models\avatar\Currency'],
            ['currency_id', 'exist', 'targetAttribute' => 'currency_ext_id', 'targetClass' => '\common\models\avatar\CurrencyLink'],
        ];
    }

    /**
     * Добавляет счет
     *
     * @return UserBill
     */
    public function action()
    {
        $currency = Currency::findOne($this->currency_id);
        $currencyLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency_id]);
        $wallet = Wallet::addNew([
            'currency_id' => $currencyLink->currency_int_id,
        ]);
        $userBill = UserBill::addWithDefault([
            'address'  => (string)$wallet->id,
            'user_id'  => Yii::$app->user->id,
            'name'     => $this->name,
            'currency' => $currency->id,
        ]);

        return $userBill;
    }


    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }
}
