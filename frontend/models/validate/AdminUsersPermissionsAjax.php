<?php

namespace avatar\models\validate;

use common\models\SendLetter;
use common\models\TaskInput;
use common\models\TaskPrizmInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\rbac\Role;
use yii\web\UploadedFile;

/**
 */
class AdminUsersPermissionsAjax extends Model
{
    public $user_id;
    public $value;
    public $role;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['user_id', 'integer'],
            ['value', 'safe'],
            ['role', 'string'],
        ];
    }

    /**
     */
    public function action()
    {
        $role = Yii::$app->authManager->getRole($this->role);
        if ($this->value == 'true') {
            Yii::$app->authManager->assign($role, $this->user_id);
        }
        if ($this->value == 'false') {
            Yii::$app->authManager->revoke($role, $this->user_id);
        }

        return 1;
    }
}
