<?php

namespace avatar\models\validate;

use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ChatRoom;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CabinetExchangeDealOpenAjax extends Model
{
    /** @var  int offer.id */
    public $id;

    /** @var \common\models\exchange\Offer */
    public $offer;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyID'],
        ];
    }

    /**
     *
     */
    public function verifyID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $Offer = Offer::findOne($this->id);
            if (is_null($Offer)) {
                $this->addError($attribute, 'Не найдена сделка');
                return;
            }
            $this->offer = $Offer;
        }
    }

    /**
     */
    public function action()
    {
        $price = $this->offer->price;

        // проверяю "Доверенный продает NEIRON?"
        if ($this->offer->type_id == Offer::TYPE_ID_SELL) {
            if ($this->offer->currency_io == 12) { // NEIRON
                $ids = \common\models\Config::get('trusted-users');
                if (\cs\Application::isEmpty($ids)) $ids = [];
                else $ids = \yii\helpers\Json::decode($ids);
                if (in_array($this->offer['user_id'], $ids)) {
                    if ($this->offer->currency_id == Currency::RUB) {
                        $c = Currency::findOne(Currency::NEIRO);
                        $c2 = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::RUB);
                        $price = bcmul($c->price_rub, pow(10, $c2->decimals));
                    }
                    if ($this->offer->currency_id == Currency::USDT) {
                        $c = Currency::findOne(Currency::NEIRO);
                        $c2 = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::USDT);
                        $price = bcmul($c->price_usd, pow(10, $c2->decimals));
                    }
                }
            }
        }

        // Создаю комнату для чата
        $chat_room = ChatRoom::add([]);

        $deal = Deal::add([
            'offer_id'     => $this->id,
            'user_id'      => Yii::$app->user->id,
            'status'       => Deal::STATUS_CREATE,
            'type_id'      => ($this->offer->type_id == Offer::TYPE_ID_SELL) ? Deal::TYPE_ID_BUY : Deal::TYPE_ID_SELL,
            'currency_io'  => $this->offer->currency_io,
            'price'        => $price,
            'currency_id'  => $this->offer->currency_id,
            'chat_room_id' => $chat_room->id,
        ]);
        DealStatus::add(['status' => Deal::STATUS_CREATE, 'deal_id' => $deal->id, 'comment' => Json::encode(['Сделка создана'])]);

        return ['id' => $deal->id];
    }

}
