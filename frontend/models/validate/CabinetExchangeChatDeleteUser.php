<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetExchangeChatDeleteUser extends Model
{
    /** @var  int */
    public $id;
    public $chat_id;

    /** @var \common\models\exchange\ChatMessage */
    public $message;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['chat_id', 'required'],
            ['chat_id', 'integer'],

            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyID'],
        ];
    }

    /**
     *
     */
    public function verifyID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $message = ChatMessage::findOne($this->id);
            if (is_null($message)) {
                $this->addError($attribute, 'Не найдено сообщение');
                return;
            }
            $this->message = $message;
        }
    }

    /**
     */
    public function action()
    {
        $user_id = $this->message->user_id;
        UserBan::add([
            'user_id' => $user_id,
            'chat_id' => $this->chat_id,
            'type'    => UserBan::TYPE_DELETE,
        ]);

        return [
            'user_id' => $user_id,
        ];
    }

}
