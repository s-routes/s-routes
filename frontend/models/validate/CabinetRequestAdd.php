<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class CabinetRequestAdd extends \iAvatar777\services\FormAjax\Model
{
    public $price;

    public $comment;

    /** @var \common\models\shop\Request */
    public $request;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'validateAmount'], // Проверка средств на кошельке

            ['comment', 'required'],
            ['comment', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price'       => 'Сумма',
            'comment'     => 'Коментарий',
        ];
    }

    public function validateAmount($a, $p)
    {
        if (!$this->hasErrors()) {
            $CIO = CurrencyIO::findFromExt(\common\models\avatar\Currency::MARKET);
            $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id);
            /** @var \common\models\piramida\Wallet $wallet */
            $wallet = $data['wallet'];
            if ($wallet->amount < Currency::getAtomFromValue($this->price, $CIO->currency_int_id)) {
                $this->addError($a, 'Недостаточно средств на кошельке');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // списываю деньги
        $CIO = CurrencyIO::findFromExt(\common\models\avatar\Currency::MARKET);
        $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id);
        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $data['wallet'];
        $t = $walletFrom->move2($CIO->main_wallet, Currency::getAtomFromValue($this->price, $CIO->currency_int_id), 'Доплата по заказу №' . $this->request->id . ': ' . $this->comment);

        // Добавляю доплату
        $m = \common\models\shop\RequestAdd::add([
            'amount'      => Currency::getAtomFromValue($this->price, $CIO->currency_int_id),
            'currency_id' => \common\models\avatar\Currency::MARKET,
            'user_id'     => Yii::$app->user->id,
            'request_id'  => $this->request->id,
            'comment'     => $this->comment,
        ]);

        return [];
    }

}
