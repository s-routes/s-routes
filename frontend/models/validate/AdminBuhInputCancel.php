<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\CurrencyIO;
use common\models\PassportLink;
use common\models\piramida\Wallet;
use common\models\RequestTokenCreate;
use common\models\shop\Request;
use common\models\TaskInput;
use common\models\TaskOutput;
use common\models\TaskOutputHistory;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class AdminBuhInputCancel extends Model
{
    /** @var int идентификатор заявки \common\models\TaskPrizmOutput */
    public $id;

    /** @var  TaskInput */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],
        ];
    }

    public function validateRequest($attr, $params)
    {
        if (!$this->hasErrors()) {
            $request = TaskInput::findOne($this->id);
            if (is_null($request)) {
                $this->addError($attr, 'Заявка не найдена');
                return;
            }

            $this->task = $request;
        }
    }

    /**
     */
    public function action()
    {
        // Статус меняю
        $this->task->status = 2;
        $this->task->save();

        return true;
    }
}
