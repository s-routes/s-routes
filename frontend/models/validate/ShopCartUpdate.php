<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.06.2016
 * Time: 11:48
 */

namespace avatar\models\validate;


use common\models\avatar\Currency;
use common\models\shop\Basket;
use common\models\shop\Product;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ShopCartUpdate extends Model
{
    /** @var  int */
    public $id;

    /** @var  int */
    public $count;
    
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'trim'],
            ['id', 'integer'],

            ['count', 'required'],
            ['count', 'trim'],
            ['count', 'integer'],
        ];
    }

    public function action()
    {
        $p = Product::findOne($this->id);
        Basket::setCount($this->id, $this->count);
        $sum = $p->price * $this->count;
        $sumAll = Basket::getPrice();
        $currency = Currency::findOne($p->currency_id);
        $cHtml = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

        return [
            'itemSum'          => $sum,
            'itemSumFormatted' => \Yii::$app->formatter->asDecimal($sum / 100, 2) . $cHtml,
            'allSum'           => $sumAll,
            'allSumFormatted'  => \Yii::$app->formatter->asDecimal($sumAll / 100, 2) . $cHtml,
        ];
    }
}