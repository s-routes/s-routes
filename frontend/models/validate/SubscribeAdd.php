<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.06.2016
 * Time: 11:48
 */

namespace avatar\models\validate;


use yii\base\Model;
use yii\helpers\ArrayHelper;

class SubscribeAdd extends Model
{
    /** @var  string */
    public $key;

    /** @var  string */
    public $data;
    
    /** @var  array */
    public $dataArray;
    
    public function rules()
    {
        return [
            ['key', 'string'],

            ['data', 'required'],
            ['data', 'validateData'],
        ];
    }
    
    public function validateKey($attribute, $params)
    {
        if (!$this->hasErrors()) {
            return $this->key == \Yii::$app->params['key'];  
        }
    }
    
    public function validateData($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $data = json_decode($this->data);
            $this->dataArray = ArrayHelper::toArray($data);

            // проверка на почту
            if (!isset($this->dataArray['mailList'])) {
                $this->addError($attribute, 'Нет обязательного параметра: mailList');
                return;
            }
            foreach ($this->dataArray['mailList'] as $mail) {
                if (filter_var($mail, FILTER_VALIDATE_EMAIL) === false) {
                    $this->addError($attribute, 'Поле mailList, \''.$mail.'\' не верный');
                    return;
                }
            }
            // проверка на почту
            if (!isset($this->dataArray['mail'])) {
                $this->addError($attribute, 'Нет обязательного параметра: mail');
                return;
            }
            if (!(isset($this->dataArray['mail']['html']) || isset($this->dataArray['mail']['text']))) {
                $this->addError($attribute, 'Нет обязательного параметра: mail.html или mail.text');
                return;
            }
            // проверка на subject
            if (!isset($this->dataArray['subject'])) {
                $this->addError($attribute, 'Нет обязательного параметра: subject');
                return;
            }
            // проверка на type
            if (!isset($this->dataArray['type'])) {
                $this->addError($attribute, 'Нет обязательного параметра: type');
                return;
            }
            if (!\cs\Application::isInteger($this->dataArray['type'])) {
                $this->addError($attribute, 'Поле: type должно быть целым числом');
                return;
            }
        }
    }
}