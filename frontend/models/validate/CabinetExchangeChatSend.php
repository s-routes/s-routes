<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetExchangeChatSend extends Model
{
    public $message;

    /** @var  int */
    public $chat_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['chat_id', 'required'],
            ['chat_id', 'integer'],

            ['message', 'required'],
            ['message', 'string'],
        ];
    }


    /**
     */
    public function action()
    {
        $message = ChatMessage::add([
            'deal_id' => $this->chat_id,
            'user_id' => Yii::$app->user->id,
            'message' => $this->message,
        ]);

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s');

        return [
            'message' => $data,
            'user'    => [
                'id'       => Yii::$app->user->id,
                'avatar'   => Yii::$app->user->identity->getAvatar(),
                'name2'    => Yii::$app->user->identity->getName2(),
                'is_admin' => Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin_chat') ? 1 : 0,
            ],
        ];
    }

}
