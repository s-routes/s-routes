<?php

namespace frontend\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\ChatRoom;
use common\models\exchange\Arbitrator;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class ChatSend extends Model
{
    public $message;

    /** @var  int */
    public $room_id;

    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['room_id', 'required'],
            ['room_id', 'integer'],

            ['message', 'string'],

            ['file', 'string'],
        ];
    }

    /**
     */
    public function action()
    {
        $Deal = Deal::findOne(['chat_room_id' => $this->room_id]);

        $Deal->trigger(Deal::EVENT_BEFORE_MESSAGE);

        $message = ChatMessage2::add([
            'room_id' => $this->room_id,
            'user_id' => Yii::$app->user->id,
            'message' => Json::encode([
                'text' => $this->message,
                'file' => $this->file,
            ]),
        ]);

        $html = Yii::$app->view->renderFile('@shop/views/cabinet-exchange/message.php', ['message' => $message]);

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s');

        $room = \common\models\ChatRoom::findOne($this->room_id);
        $room->last_message = time();
        $room->save();

        $Deal->trigger(Deal::EVENT_AFTER_MESSAGE);

        // Уведомляю по телеграму
        $this->notificateTelegram($Deal);

        return [
            'message' => [
                'object'        => $data,
                'html'          => $html,
                'timeFormatted' => Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s'),
            ],
            'user'    => [
                'id'       => Yii::$app->user->id,
                'avatar'   => Yii::$app->user->identity->getAvatar(),
                'name2'    => Yii::$app->user->identity->getName2(),
            ],
        ];
    }


    /**
     * @param \common\models\exchange\Deal $Deal
     */
    private function notificateTelegram($Deal)
    {
        try {
            $users = [
                [
                    'user_id' => $Deal->user_id,
                    'link'    => Url::to(['cabinet-exchange/deal', 'id' => $Deal->id], true),
                ],
                [
                    'user_id' => $Deal->getOffer()->user_id,
                    'link'    => Url::to(['cabinet-exchange/deal', 'id' => $Deal->id], true),
                ],
            ];
            if (in_array($Deal->status, [Deal::STATUS_AUDIT_ACCEPTED])) {
                $arbitrDeal = Arbitrator::findOne(['deal_id' => $Deal->id]);
                $aid = $arbitrDeal->arbitrator_id;
                $users[] = [
                    'user_id' => $aid,
                    'link'    => Url::to(['cabinet-arbitrator/item', 'id' => $Deal->id], true),
                ];
            }
            /** @var \common\models\UserAvatar $uCurrent */
            $uCurrent = Yii::$app->user->identity;
            foreach ($users as $u1) {
                $u = UserAvatar::findOne($u1['user_id']);
                if (!Application::isEmpty($u->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                    $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение от пользователя', [], $language).': ' . $uCurrent->getName2() . "\n" .  Yii::$app->request->post('message')  . "\n" .  "\n" . Yii::t('c.LSZBX1ce2W', 'Ссылка на чат', [], $language).': '.$u1['link']]);
                }
            }
        } catch (\Exception $e) {

        }
    }


}
