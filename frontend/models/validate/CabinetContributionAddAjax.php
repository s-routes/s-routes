<?php

namespace avatar\models\validate;

use avatar\models\forms\Security;
use avatar\models\Wallet;
use avatar\models\WalletETC;
use avatar\models\WalletETH;
use avatar\modules\ETH\ServiceEtherScan;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillCrypto;
use common\models\CurrencyCrypto;
use common\models\NeironTransaction;
use common\models\piramida\Operation;
use common\models\UserAvatar;
use common\models\UserContribution;
use common\models\UserSeed;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;

/**
 * password_type здесь нельзя задать в открытом виде так как только зашифрованный вид, потому что введен новый стандарт повышающий шифрование
 */
class CabinetContributionAddAjax extends Model
{
    public $id;
    /** @var  \common\models\Contribution */
    public $Contribution;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateId'],
            ['id', 'validateAmount'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateId($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $Contribution = \common\models\Contribution::findOne($this->id);
            if (is_null($Contribution)) {
                $this->addError($attribute, 'Вклад не найден');
                return;
            }
            $this->Contribution = $Contribution;
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAmount($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userNeiron = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::NEIRO);
            /** @var \common\models\piramida\Wallet $userWallet */
            $userWallet = $userNeiron['wallet'];

            // Вычисление баланса для стейкинга
            $rows = \common\models\piramida\Operation::find()
                ->innerJoin('neiron_transaction', 'neiron_transaction.operation_id = operations.id')
                ->select([
                    'operations.*',
                    'neiron_transaction.type_id',
                ])
                ->where(['operations.wallet_id' => $userWallet->id])
                ->orderBy(['operations.datetime' => SORT_DESC])
                ->asArray()
                ->all();

            $sum = 0;
            foreach ($rows as $o) {
                switch ($o['type_id']) {
                    case \common\models\NeironTransaction::TYPE_REFERAL:
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                            $sum += $o['amount'];
                        }
                        break;
                    case \common\models\NeironTransaction::TYPE_BUY:
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                            $sum += $o['amount'];
                        }
                        break;
                    case \common\models\NeironTransaction::TYPE_MOVE:
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_OUT) {
                            $sum -= $o['amount'];
                        }
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                            // Ничего не делаю
                        }
                        break;
                    case \common\models\NeironTransaction::TYPE_CONVERT:
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_OUT) {
                            $sum -= $o['amount'];
                        }
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                            // Ничего не делаю
                        }
                        break;
                    case \common\models\NeironTransaction::TYPE_HOLD:
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_OUT) {
                            $sum -= $o['amount'];
                        }
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                            // Ничего не делаю
                        }
                        break;
                }
            }

            if ($sum < $this->Contribution->amount) {
                $this->addError($attribute, 'Недостаточно монет чтобы сделать вклад.');
                return;
            }
        }
    }

    /**
     * @return \common\models\UserContribution
     *
     * @throws
     */
    public function action()
    {
        $w = \common\models\piramida\Wallet::addNew(['currency_id' => \common\models\piramida\Currency::NEIRO]);
        $userNeiron = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::NEIRO);
        /** @var \common\models\piramida\Wallet $userWallet */
        $userWallet = $userNeiron['wallet'];

        $userC = UserContribution::add([
            'contribution_id' => $this->id,
            'user_id'         => Yii::$app->user->id,
            'created_at'      => time(),
            'finish_at'       => time() + $this->Contribution->time,
            'wallet_id'       => $w->id,
        ]);

        $t = $userWallet->move($w, $this->Contribution->amount, 'Вклад #' . $userC->id);

        // Делаю запись о типе транзакции если это NERON
        if ($userWallet->currency_id == \common\models\piramida\Currency::NEIRO) {
            $operations = Operation::find()->where(['transaction_id' => $t->id])->all();
            if (count($operations) == 2) {
                NeironTransaction::add([
                    'transaction_id' => $t->id,
                    'operation_id'   => $operations[0]['id'],
                    'type_id'        => NeironTransaction::TYPE_HOLD,
                ]);
                NeironTransaction::add([
                    'transaction_id' => $t->id,
                    'operation_id'   => $operations[1]['id'],
                    'type_id'        => NeironTransaction::TYPE_HOLD,
                ]);
            }
        }

        return $userC;
    }
}
