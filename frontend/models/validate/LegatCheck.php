<?php

namespace avatar\models\validate;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\TaskLegatInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class LegatCheck extends Model
{
    public $id;

    /** @var TaskLegatInput */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateAccount'],
        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            $task = TaskLegatInput::findOne($this->id);

            if (is_null($task)) {
                $this->addError($a, 'не найдена задача');
                return;
            }
            if ($task->status != 0) {
                $this->addError($a, 'Задача закрыта');
                return;
            }
            if ($task->user_id != Yii::$app->user->id) {
                $this->addError($a, 'Это не ваша задача');
                return;
            }

            $this->task = $task;
        }
    }

    /**
     * @return array
     * [
     *              'status' => int
     *              0 - Ничего не найдено
     *              1 - Найдено и зачислено
     *              2 - Пользователь указал в задаче транзакцию а она уже была обработана как положительная
     *              'amount' => int - не обязательное, кол-во атомов, которое было зачислено
     * ]
     * @throws
     */
    public function action()
    {
        // Делаю запрос на полученные монеты
        try {
            $provider = new \avatar\modules\ETH\ServiceEthPlorer();
            $response = $provider->getAddressHistory($this->task->account, Yii::$app->params['legatToken']);
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $transactionList = $response;
        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskLegatInput::find()->where(['status' => 1])->select('txid')->column();

        $task = $this->task;
        if ($task->account) {
            // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
            foreach ($transactionList as $t) {

                // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                if (in_array(strtolower($t['transactionHash']), $transactionListSuccess)) continue;

                if (strtolower($t['tokenInfo']['address']) == strtolower(Yii::$app->params['legatToken'])) {
                    if (strtolower($t['from']) == strtolower($task->account)) {

                        return $this->action2($t);

                    }
                }
            }

            return ['status' => 0];
        }

    }

    private function action2($t)
    {
        $task = $this->task;
        $amountNQT = $t['value'];
        $task->amount = bcdiv($amountNQT, '10000000000000000');

        // Ищу кошелек LEGAT пользователя
        $d = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LEGAT);
        $walletIn = $d['wallet'];

        // Ищу кошелек LEGAT откуда зачислять
        $walletOut = Wallet::findOne(Yii::$app->params['walletLEGAT_all']);
        $txid_int = $walletOut->move($walletIn, $task->amount,
            Json::encode([
                'Зачисление на основании задачи id={task}',
                [
                    'task' => $task->id
                ]
            ])
        );

        // Сохраняю данные в задачу о положительном завершении
        $task->txid = $t['transactionHash'];
        $task->txid_internal = $txid_int->id;
        $task->status = 1;
        $task->finished_at = time();
        $task->save();

        return [
            'status'                => 1,
            'amount'                => $task->amount,
            'amount_formatted'      => Yii::$app->formatter->asDecimal($task->amount / 100, 2),
        ];
    }
}
