<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\PassportLink;
use common\models\piramida\Wallet;
use common\models\RequestTokenCreate;
use common\models\shop\Request;
use common\models\TaskOutput;
use common\models\TaskOutputHistory;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class AdminBuhHide extends Model
{
    /** @var int идентификатор заявки \common\models\TaskPrizmOutput */
    public $id;

    /** @var  TaskOutput */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],
        ];
    }

    public function validateRequest($attr, $params)
    {
        if (!$this->hasErrors()) {
            $request = TaskOutput::findOne($this->id);
            if (is_null($request)) {
                $this->addError($attr, 'Заявка не найдена');
                return;
            }

            $this->task = $request;
        }
    }

    /**
     */
    public function action()
    {
        // Статус меняю
        $this->task->status = TaskPrizmOutput::STATUS_HIDE;
        $this->task->save();

        // Возвращаю монеты
        $Bill = UserBill::findOne($this->task->billing_id);
        $currency = Currency::findOne($Bill->currency);
        $CIO = CurrencyIO::findOne(['currency_ext_id' => $currency->id]);
        $wallet = Wallet::findOne($CIO->block_wallet);
        $walletClient = Wallet::findOne($Bill->address);
        $t = $wallet->move2(
            $walletClient,
            $this->task->amount,
            Yii::t('c.TB9ZAPoWIy', 'Возврат средств по заявке на вывод') . ' #' . $this->task->id,
            NeironTransaction::TYPE_OUT_RETURN
        );

        // Делаю запись о типе транзакции если это NERON
        if ($wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
            NeironTransaction::add([
                'transaction_id' => $t['transaction']['id'],
                'operation_id'   => $t['operation_add']['id'],
                'type_id'        => NeironTransaction::TYPE_OUT_RETURN,
            ]);
        }

        // Добавляю историю заявки
        $i = TaskOutputHistory::add([
            'task_id' => $this->task->id,
            'comment' => Yii::t('c.TB9ZAPoWIy', 'Возврат средств по заявке на вывод') . ' ' . Html::a('#' . $this->task->id, ['cabinet-active/out-item', 'id' => $this->task->id], ['data-pjax' => 0]),
        ]);

        // Уведомляю пользователя

        return true;
    }
}
