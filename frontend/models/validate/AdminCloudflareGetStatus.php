<?php

namespace avatar\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\shop\Request;
use common\models\TaskOutput;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class AdminCloudflareGetStatus extends Model
{
    /** @var int идентификатор заявки \common\models\TaskPrizmOutput */
    public $id;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        /** @var \common\services\CloudFlare $p */
        $p = Yii::$app->CloudFlare;

        $dns = Yii::$app->params['cloudflare']['dns'];

        $rows = [];

        foreach ($dns as $domain => $data) {
            $res = $p->_get('zones/' . $data['id']. '/dns_records');
            $data = Json::decode($res->content);
            foreach ($data['result'] as $i) {
                if (isset($i['type'])) {
                    if ($i['type'] == 'A' && $i['name'] == $domain) {
                        $rows[] = [
                            'name' => $domain,
                            'ip' => $i['content'],
                        ];
                    }
                }
            }
        }

        return $rows;
    }
}
