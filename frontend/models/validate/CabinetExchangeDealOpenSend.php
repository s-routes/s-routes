<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CabinetExchangeDealOpenSend extends Model
{
    /** @var  int */
    public $id;

    /** @var \common\models\exchange\Deal */
    public $deal;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyID'],
        ];
    }

    /**
     *
     */
    public function verifyID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $Deal = Deal::findOne($this->id);
            if (is_null($Deal)) {
                $this->addError($attribute, 'Не найдена сделка');
                return;
            }
            $this->deal = $Deal;
        }
    }

    /**
     */
    public function action()
    {
        $Deal = $this->deal;

        $offerUser = $Deal->getOffer()->getUser();

        // отправка уведомления для $offerUser по почте
        Subscribe::sendArray([$offerUser->email], 'Открыта сделка', 'deal_open', [
            'deal' => $Deal,
        ]);
    }

}
