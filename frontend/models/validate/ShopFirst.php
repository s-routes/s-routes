<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.06.2016
 * Time: 11:48
 */

namespace avatar\models\validate;


use common\models\avatar\Currency;
use common\models\shop\Basket;
use common\models\shop\Product;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ShopFirst extends Model
{
    /** @var  string */
    public $comment;
    
    public function rules()
    {
        return [
            ['comment', 'string'],
        ];
    }

    public function action()
    {
        $request = \Yii::$app->session->get('shop-request2');
        if (is_null($request)) {
            $request = [];
        }
        $request['comment'] = $this->comment;
        \Yii::$app->session->set('shop-request2', $request);

        return 1;
    }
}