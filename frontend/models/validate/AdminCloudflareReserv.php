<?php

namespace avatar\models\validate;

use common\models\SendLetter;
use common\models\TaskInput;
use common\models\TaskPrizmInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminCloudflareReserv extends Model
{
    public $id;
    /**
     *
     */
    public function action()
    {
        /** @var \common\services\CloudFlare $p */
        $p = Yii::$app->CloudFlare;

        $dns = Yii::$app->params['cloudflare']['dns'];
        $ip = Yii::$app->params['cloudflare']['ip']['reserv'];

        foreach ($dns as $domain => $data) {
            foreach ($data['records'] as $record_id) {
                $res = $p->_put('zones/' . $data['id'] . '/dns_records/' . $record_id, [
                    'type' => 'A',
                    'content' => $ip,
                ]);
                Yii::info(VarDumper::dumpAsString($res), 'avatar\models\validate\AdminCloudflareReserv::action()');
            }
        }

        return 1;
    }
}
