<?php

namespace avatar\models\validate;

use common\models\SendLetter;
use common\models\TaskPrizmInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class PrizmInputClose extends Model
{
    public $id;

    /** @var TaskPrizmInput */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateAccount'],
        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            $task = TaskPrizmInput::findOne($this->id);

            if (is_null($task)) {
                $this->addError($a, 'не найдена задача');
                return;
            }

            $this->task = $task;
        }
    }

    /**
     */
    public function action()
    {
        $this->task->status = 2;
        $ret = $this->task->save();

        return $ret;
    }
}
