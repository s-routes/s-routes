<?php

namespace avatar\models\validate;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\TaskPrizmInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class PrizmInputCheck extends Model
{
    public $id;

    /** @var TaskPrizmInput */
    public $task;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateAccount'],
        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            $task = TaskPrizmInput::findOne($this->id);

            if (is_null($task)) {
                $this->addError($a, 'не найдена задача');
                return;
            }
            if ($task->status != 0) {
                $this->addError($a, 'Задача закрыта');
                return;
            }
            if ($task->user_id != Yii::$app->user->id) {
                $this->addError($a, 'Это не ваша задача');
                return;
            }

            $this->task = $task;
        }
    }

    /**
     * @return array
     * [
     *              'status' => int
     *              0 - Ничего не найдено
     *              1 - Найдено и зачислено
     *              2 - Пользователь указал в задаче транзакцию а она уже была обработана как положительная
     *              'amount' => int - не обязательное, кол-во атомов, которое было зачислено
     * ]
     * @throws
     */
    public function action()
    {
        /** @var \common\services\PrizmApi $PrizmApi */
        $PrizmApi = Yii::$app->PrizmApi;

        // Делаю запрос на PRIZM ноду
        try {
            $response = $PrizmApi->post('request', [
                'requestType' => 'getBlockchainTransactions',
                'account'     => Yii::$app->params['prizmMasterAccount'],
                'lastIndex'   => 20,
            ]);
        } catch (\Exception $e) {
            return ['status' => 2, 'message' => $e->getMessage()];
        }

        $transactionList = $response['transactions'];
        /** @var array $transactionListSuccess  Список транзакций, для которых уже успешно зачислены монеты */
        $transactionListSuccess = TaskPrizmInput::find()->where(['status' => 1])->select('txid')->column();

        $task = $this->task;
        if ($task->account) {
            // Если указан призм кошелек то ищу его среди всех последних 20 транзакций.
            foreach ($transactionList as $t) {

                // Если транзакция из блокчейна находится в списке уже подтвержденных то пропускаю ее
                if (in_array($t['transaction'], $transactionListSuccess)) continue;

                if ($t['senderRS'] == $task->account) {
                    $amountNQT = $t['amountNQT'];
                    $task->amount = $amountNQT;

                    // Вычисляю сколько LETM и LETN
                    $c = Currency::findOne(['code' => 'PZM']);
                    $mnoj = Yii::$app->params['input_prizm_mnoj'];
                    $amount_LETN = (int)($amountNQT * $mnoj);
                    $amount_LETM = (int)(($amountNQT - $amount_LETN) * $c->kurs);
                    $task->amount_letm = $amount_LETM;
                    $task->amount_letn = $amount_LETN;

                    // Ищу кошелек LETN пользователя
                    $d = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETN);
                    $walletIn = $d['wallet'];

                    // Ищу кошелек LETN откуда зачислять
                    $walletOut = Wallet::findOne(Yii::$app->params['walletLETN_all']);
                    $txid_int = $walletOut->move($walletIn, $amount_LETN,
                        Json::encode([
                            'Зачисление на основании задачи id={task}',
                            [
                                'task' => $task->id
                            ]
                        ])
                    );

                    // Ищу кошелек LETM пользователя
                    $dM = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETM);
                    $walletInM = $dM['wallet'];

                    // Ищу кошелек LETM откуда зачислять
                    $walletOut = Wallet::findOne(Yii::$app->params['walletLETM_All']);
                    $txid_intM = $walletOut->move($walletInM, $amount_LETM,
                        Json::encode([
                            'Зачисление на основании задачи id={task}',
                            [
                                'task' => $task->id
                            ]
                        ])
                    );

                    // Сохраняю данные в задачу о положительном завершении
                    $task->txid = $t['transaction'];
                    $task->txid_internal = $txid_int->id;
                    $task->txid_internal_letm = $txid_intM->id;
                    $task->status = 1;
                    $task->finished_at = time();
                    $task->save();

                    return [
                        'status'                => 1,
                        'amount'                => $amountNQT,
                        'amount_formatted'      => Yii::$app->formatter->asDecimal($amountNQT / 100, 2),
                        'amount_letm'           => $amount_LETM,
                        'amount_letm_formatted' => Yii::$app->formatter->asDecimal($amount_LETM / 100, 2),
                        'amount_letn'           => $amount_LETN,
                        'amount_letn_formatted' => Yii::$app->formatter->asDecimal($amount_LETN / 100, 2),
                    ];
                }
            }

            return ['status' => 0];
        }

    }
}
