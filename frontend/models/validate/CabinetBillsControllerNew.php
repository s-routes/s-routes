<?php

namespace avatar\models\validate;

use avatar\models\forms\Security;
use avatar\models\Wallet;
use avatar\models\WalletETC;
use avatar\models\WalletETH;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\UserAvatar;
use common\models\UserSeed;
use Yii;
use yii\base\Model;

/**
 * password_type здесь нельзя задать в открытом виде так как только зашифрованный вид, потому что введен новый стандарт повышающий шифрование
 */
class CabinetBillsControllerNew extends Model
{
    public $name;
    public $currency = Currency::BTC;
    public $password;
    public $password_type;

    public function init()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $this->password_type = $user->password_save_type;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['currency', 'integer'],
            ['currency', 'default', 'value' => Currency::BTC],
            ['currency', 'in', 'range' => [
                Currency::BTC,
                Currency::ETH,
            ]],

            ['password', 'string'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль от кабинета не верный');
                return;
            }
        }
    }

    /**
     * Создает кошелек
     *
     * @return \common\models\avatar\UserBill
     *
     * @throws
     */
    public function action()
    {
//        switch ($this->currency) {
//
//            case Currency::BTC:
//                $wallet = Wallet::create($this->name, $this->password, Yii::$app->user->id, $this->password_type);
//                $billing = $wallet->billing;
//                break;
//
//            case Currency::ETH:
//                $billing = WalletETH::create($this->name, $this->password, Yii::$app->user->id, $this->password_type)->billing;
//                break;
//
//        }

        return WalletETH::create($this->name, $this->password, Yii::$app->user->id, $this->password_type)->billing;
    }
}
