<?php

namespace avatar\models\validate;

use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CabinetWalletConvertAjax extends Model
{
    /** @var int идентификатор счета пополнения */
    public $id;

    /** @var double кол-во монет для получения */
    public $value;

    /** @var int Идентификатор валюты списания \common\models\avatar\Currency */
    public $wallet;

    /** @var  \common\models\piramida\Wallet кошелек пополнения */
    public $walletTo;

    /** @var  \common\models\avatar\UserBill счет пополнения */
    public $billTo;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'setItem'],

            ['value', 'required'],
            ['value', 'double'],

            ['wallet', 'required'],
            ['wallet', 'integer'],
        ];
    }

    public function setItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billTo = UserBill::findOne($this->id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Счет не найден');
                return;
            }
            if ($billTo->mark_deleted) {
                $this->addError($attribute, 'Счет удален');
                return;
            }
            if ($billTo->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваш счет');
                return;
            }

            $walletTo = Wallet::findOne($billTo->address);

            $this->walletTo = $walletTo;
            $this->billTo = $billTo;
        }
    }

    /**
     */
    public function action()
    {
        // получаю курс валюты
        if ($this->billTo->currency == Currency::LETM) {
            $currency = \common\models\avatar\Currency::findOne($this->wallet);
            if (in_array($currency->id, [
                Currency::PZM,
                Currency::LETN,
                Currency::LEGAT,
            ])) {
                $pzm = \common\models\avatar\Currency::findOne(Currency::PZM);
                $kurs = $pzm->kurs;
            } else {
                $kurs = $currency->kurs;
            }
        } else {
            $currency = \common\models\avatar\Currency::findOne($this->wallet);
            if (in_array($currency->id, [
                Currency::PZM,
            ])) {
                $kurs = 1;
            } else {
                $pzm = \common\models\avatar\Currency::findOne(Currency::PZM);
                $kurs = 1 / $pzm->kurs;
            }
        }

        // монет к списанию
        $from_amount = $this->value / $kurs;
        // атомов к списанию
        $from_currency = Currency::findOne($this->wallet);
        $from_currency_IO = CurrencyIO::findOne(['currency_ext_id' => $from_currency->id]);
        $from_currencyP = \common\models\piramida\Currency::findOne($from_currency_IO->currency_int_id);
        $from_amount_atom = bcmul($from_amount, bcpow(10, $from_currencyP->decimals), 0);
        $from_wallet_all = Wallet::findOne($from_currency_IO->main_wallet);
        $from_wallet_user = UserBill::getInternalCurrencyWallet($from_currency_IO->currency_int_id)['wallet'];;

        $to_amount = $this->value;
        $to_currency = Currency::findOne($this->billTo->currency);
        $to_currency_IO = CurrencyIO::findOne(['currency_ext_id' => $to_currency->id]);
        $to_currencyP = \common\models\piramida\Currency::findOne($to_currency_IO->currency_int_id);
        $to_amount_atom = bcmul($to_amount, bcpow(10, $to_currencyP->decimals), 0);

        $to_wallet_all = Wallet::findOne($to_currency_IO->main_wallet);
        $to_wallet_user = UserBill::getInternalCurrencyWallet($to_currency_IO->currency_int_id)['wallet'];

        // Завожу операцию
        $operation = ConvertOperation::add([
            'kurs' => (int)($kurs * 1000000),
        ]);

        // Забираю монеты
        $t_from = $from_wallet_user->move(
            $from_wallet_all,
            $from_amount_atom,
            'Конвертация монет, операция #' . $operation->id . ' курс=' . $kurs
        );

        // Начисляю монеты
        $t_to = $to_wallet_all->move(
            $to_wallet_user,
            $to_amount_atom,
            'Конвертация монет, операция #' . $operation->id . ' курс=' . $kurs
        );

        // Обновляю операцию
        $operation->wallet_from_id = $from_wallet_user->id;
        $operation->wallet_to_id = $to_wallet_user->id;
        $operation->tx_from_id = $t_from->id;
        $operation->tx_to_id = $t_to->id;
        $operation->amount = $from_amount_atom;
        $operation->amount_to = $to_amount_atom;
        $operation->from_currency_id = $from_currency_IO->currency_int_id;
        $operation->to_currency_id = $to_currency_IO->currency_int_id;
        $operation->save();

        return [$t_from, $t_to];
    }

}
