<?php

namespace avatar\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * ContactForm is the model behind the contact form.
 */
class CabinetExchangeSetAssessment extends Model
{
    /** @var  int */
    public $id;

    /** @var \common\models\exchange\Deal */
    public $deal;

    /** @var  int идентификатор экрана с которого производится оценка */
    public $screen;

    /** @var int оценка 1 или -1 */
    public $value;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'verifyID'],
            ['id', 'verifyRatingExist'],

            ['screen', 'required'],
            ['screen', 'integer'],

            ['value', 'required'],
            ['value', 'integer'],
            ['value', 'in', 'range' => [-1,0,1]],
        ];
    }

    /**
     *
     */
    public function verifyID($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $Deal = Deal::findOne($this->id);
            if (is_null($Deal)) {
                $this->addError($attribute, 'Не найдена сделка');
                return;
            }
            $this->deal = $Deal;
        }
    }

    /**
     *
     */
    public function verifyRatingExist($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $Deal = $this->deal;
            $offer = $Deal->getOffer();
            if ($this->screen == Assessment::SCREEN_DEAL) {
                $user_id = $offer->user_id;
            } else {
                $user_id = $Deal->user_id;
            }

            $exist = Assessment::find()
                ->where([
                    'deal_id'        => $this->deal->id,
                    'user_id'        => $user_id,
                ])
                ->exists();

            if ($exist) {
                $this->addError($attribute, 'Рейтинг уже поставлен');
                return;
            }
        }
    }

    /**
     */
    public function action()
    {
        $Deal = $this->deal;

        $t = Yii::$app->db->beginTransaction();
        $assessment_positive = 0;
        $assessment_negative = 0;
        try {
            $Deal->trigger(Deal::EVENT_BEFORE_CLOSE);

            $offer = $Deal->getOffer();
            if ($this->screen == Assessment::SCREEN_DEAL) {
                $user_id = $offer->user_id;
                $user_id_author = $Deal->user_id;
                $role = $offer->type_id;
            } else {
                $user_id = $Deal->user_id;
                $user_id_author = $offer->user_id;
                $role = ($offer->type_id == Offer::TYPE_ID_SELL)? Offer::TYPE_ID_BUY : Offer::TYPE_ID_SELL ;
            }

            // прибалвляю значение во всех предложениях
            if ($this->value > 0) {

                // Вычисляю рейтинги
                $v = Assessment::find()->where(['user_id' => $user_id])->andWhere(['>', 'value', 0])->select('sum(value)')->scalar();
                if (is_null($v)) $v=0;
                $p1real = $v;

                // обновляю показатели
                Offer::updateAll(['assessment_positive' => $p1real], ['user_id' => $user_id]);
                $assessment_positive = $p1real;

            } else {

                // Вычисляю рейтинги
                $v2 = Assessment::find()->where(['user_id' => $user_id])->andWhere(['<', 'value', 0])->select('sum(value)')->scalar();
                if (is_null($v2)) $v2=0;
                if ($v2 < 0) $v2 = -$v2;
                $n1real = $v2;

                // обновляю показатели
                Offer::updateAll(['assessment_negative' => $n1real], ['user_id' => $user_id]);
                $assessment_negative = $n1real;

            }

            // Добавляю оценку
            Assessment::add([
                'deal_id'        => $Deal->id,
                'value'          => $this->value,
                'user_id'        => $user_id,
                'user_id_author' => $user_id_author,
                'offer_id'       => $offer->id,
                'offer_type'     => $offer->type_id,
                'role'           => $role,
            ]);

            // отправка уведомления в Телеграм
            $userTelegram = UserAvatar::findOne($user_id);
            $userAuthor = UserAvatar::findOne($user_id_author);
            if (!Application::isEmpty($userTelegram->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($userTelegram->language)) ? 'ru' : $userTelegram->language;
                if ($this->value > 0) {
                    $telegram->sendMessage(['chat_id' => $userTelegram->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Пользователь {user} вас оценил положительно. Сделка #{deal}', [
                        'user' => $userAuthor->getAddress(),
                        'deal' => $Deal->id,
                    ], $language)]);
                } else {
                    $telegram->sendMessage(['chat_id' => $userTelegram->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Пользователь {user} вас оценил отрицательно. Сделка #{deal}', [
                        'user' => $userAuthor->getAddress(),
                        'deal' => $Deal->id,
                    ], $language)]);
                }
            }

            // Устанавливаю статус
            $Deal->status = Deal::STATUS_CLOSE;
            $Deal->save();
            if ($this->screen == Assessment::SCREEN_DEAL) {
                $u = $Deal->getUser();
            } else {
                $u = $offer->getUser();
            }
            DealStatus::add([
                'status'  => Deal::STATUS_CLOSE,
                'deal_id' => $Deal->id,
                'comment' => Json::encode([
                    'Сделка оценена и завершена пользователем: {user}',
                    [
                        'user' => $u->getAddress(),
                    ]
                ]),
            ]);

            // Сохраняю данные у пользователя
            $User = $userTelegram;
            $User->assessment_positive = $assessment_positive;
            $User->assessment_negative = $assessment_negative;
            $User->save();

            $Deal->trigger(Deal::EVENT_AFTER_CLOSE);

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
        }
    }

}
