<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\Json;

/**
 * name{id}_in
 * name{id}_out
 */
class AdminLotConvert extends Model
{
    /**
     * @var array
     * [
     *  [
     *      id
     *      in
     *      out
     *  ]
     * ]
     */
    public $params;

    public function init()
    {
        $params = Config::get('LotConvertParams');
        if ($params === false) $params = '[]';

        $this->params = Json::decode($params);
    }

    public function rules()
    {
        $currencyList = \common\models\avatar\Currency::find()->select('id')->column();
        $rules = [];
        $fields = [];

        foreach ($currencyList as $cid) {
            $fields[] = 'name' . $cid . '_in';
            $fields[] = 'name' . $cid . '_out';
        }
        $rules[] = [$fields, 'double', 'min' => 0];
        return $rules;
    }


    public function __get($name)
    {
        if ($name == 'attributes') {
            $attributes = [];
            foreach ($this->params as $p) {
                $attributes['name' . $p['id'] . '_in'] = $p['in'];
                $attributes['name' . $p['id'] . '_out'] = $p['out'];
            }
            return $attributes;
        }
        if ($name == 'errors') {
            return parent::__get($name);
        }
        $arr = explode('_', $name);
        try {
            $param = $arr[1];
        } catch (\Exception $e) {
            return parent::__get($name);
        }

        $id = substr($arr[0], 4);
        foreach ($this->params as $p) {
            if ($id == $p['id']) {
                return $p[$param];
            }
        }

        return 0;
    }

    public function __set($name, $value)
    {
        $arr = explode('_', $name);
        $param = $arr[1];
        $id = substr($arr[0], 4);
        $c = count ($this->params);
        for ($i = 0; $i < $c; $i++) {
            if ($id == $this->params[$i]['id']) {
                $this->params[$i][$param] = $value;
                return true;
            }
        }
        if ($param == 'in') {
            $param2 = 'out';
        } else {
            $param2 = 'in';
        }
        $this->params[] = [
            'id'    => $id,
            $param  => $value,
            $param2 => 0,
        ];
        return true;
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        Config::set('LotConvertParams', Json::encode($this->params));

        return parent::save($runValidation, $attributeNames); // TODO: Change the autogenerated stub
    }
}
