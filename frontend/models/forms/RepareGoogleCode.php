<?php

namespace avatar\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\UserAvatar;
use Yii;
use yii\base\Model;


/**
 */
class RepareGoogleCode extends Model
{
    public $email;
    /** @var  \common\models\UserAvatar */
    private $_user;
    public $code;
    public $verificationCode;

    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['email', 'trim'],
            ['email', 'email', 'checkDNS' => true, 'message' => 'Email должен быть с корректным доменным именем'],
            ['email', 'validateEmail'],
            ['verificationCode', 'captcha'],
            ['code', 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['code', 'string'],
            ['code', 'validateCode'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verificationCode' => 'Проверочный код',
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->email = strtolower($this->email);
            try {
                $user = UserAvatar::findOne(['email' => $this->email]);
                $this->_user = $user;
            } catch (\Exception $e) {
                $this->addError($attribute, 'Такого пользователя нет');
            }

        }
    }

    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->_user->google_auth_code != $this->code) {
                $this->addError($attribute, 'Код не верный');
            }
        }
    }

    /**
     * Устанавливает $this->url
     *
     */
    public function action()
    {
        $secret = $this->code;
        $file = Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
        require_once($file);
        $url = sprintf("otpauth://totp/%s?secret=%s", $this->email . '@' . Yii::$app->params['google-authorisation-code']['server'], $secret);
        $this->url = $url;
    }
}
