<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopAll extends ActiveRecord
{
    public function rules()
    {
        return [
            [[
                 'link',
                 'description',
                 'about',
                 'account_login',
                 'account_password',
                 'rekvisit_karti',
             ], 'string'],
            ['link', 'url'],
            ['type', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'link'             => 'Ссылка',
            'description'      => 'Описание',
            'type'             => 'Тип',
            'about'            => 'Описание полное',
            'account_login'    => 'Логин',
            'account_password' => 'Пароль',
            'rekvisit_karti'   => 'Реквизиты карты',
        ];
    }
}
