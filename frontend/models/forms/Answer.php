<?php
namespace avatar\models\forms;

use common\models\SendLetter;
use cs\base\FormActiveRecord;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 *
 * @property string  text
 */
class Answer extends Model
{
    public $id;
    public $text;

    public function rules()
    {
        return [
            ['text', 'required'],
            ['text', 'string'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $letter = SendLetter::findOne($this->id);
        \common\services\Subscribe::sendArray([$letter->email], 'Ответ из техподдержки', 'support_mail', [
            'item' => $letter,
            'text' => $this->text,
        ]);

        \common\models\SendLetterItem::add([
            'text'      => $this->text,
            'letter_id' => $letter->id,
        ]);

        return 1;
    }
}
