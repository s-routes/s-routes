<?php

namespace avatar\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ActiveInputTransaction extends \iAvatar777\services\FormAjax\Model
{
    /** @var string */
    public $cid;
    public $mod_id;
    public $amount_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['cid', 'required'],
            ['cid', 'integer'],

            ['amount_user', 'required'],
            ['amount_user', 'trim'],
            ['amount_user', 'double', 'message' => 'Дробное число пишется через точку'],

            ['mod_id', 'integer'],
            ['mod_id', 'trim'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'amount_user' => 'Укажите точное количество монет, которое поступит на ваш кошелек, включая дробную часть. Разрядность важна.'
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // Закрываю все старые задачи пользователя
        \common\models\TaskInput::updateAll(['status' => 2], [
            'user_id'        => Yii::$app->user->id,
            'status'         => 0,
            'currency_io_id' => $this->cid,
            'type_id'        => \common\models\TaskInput::TYPE_TRANSACTION,
        ]);

        $CIO = CurrencyIO::findOne($this->cid);

        // Создаю новую задачу
        $input = \common\models\TaskInput::add([
            'user_id'         => Yii::$app->user->id,
            'status'          => 0,
            'currency_io_id'  => $this->cid,
            'type_id'         => \common\models\TaskInput::TYPE_TRANSACTION,
            'modification_id' => $this->mod_id,
            'amount_user'     => \common\models\piramida\Currency::getAtomFromValue($this->amount_user, $CIO->currency_int_id),
        ]);

        return $input;
    }
}
