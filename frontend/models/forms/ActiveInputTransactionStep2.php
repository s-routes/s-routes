<?php

namespace avatar\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\TaskInput;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ActiveInputTransactionStep2 extends \iAvatar777\services\FormAjax\Model
{
    public $task_id;

    public $txid;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['task_id', 'required'],
            ['task_id', 'integer'],

            ['txid', 'required'],
            ['txid', 'trim'],
            ['txid', 'string'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'txid' => 'Укажите TxID (hash) транзакции'
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $task = TaskInput::findOne($this->task_id);
        $task->txid = $this->txid;
        $task->save();

        return 1;
    }
}
