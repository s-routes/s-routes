<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\ActiveRecord;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class AdminCurrencyExtInfo extends ActiveRecord
{
    public static function tableName()
    {
        return 'currency_io';
    }

    public function rules()
    {
        return [
            ['extended_info', 'required'],
            ['extended_info', 'string'],
            ['extended_info', 'JsonValidate'],
        ];
    }

    public function JsonValidate($a, $v)
    {
        if (!$this->hasErrors()) {
            try {
                $data = Json::decode($this->$a);
            } catch (\Exception $e) {
                $this->addError($a, 'Не верный JSON');
                return;
            }
        }
    }
}