<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Currency model
 *
 */
class Currency extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    public function formAttributes()
    {
        return [
            [
                'code',
                'Код валюты',
                1,
                'string',
                ['max' => 6],
            ],
            [
                'title',
                'Наименование валюты',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'code_coinmarketcap',
                'code_coinmarketcap',
                0,
                'string',
                ['max' => 6],
            ],
            [
                'id_coinmarketcap',
                'id_coinmarketcap',
                0,
                'string',
                ['max' => 32],
            ],
            [
                'decimals',
                'Кол-во символов после запятой',
                0,
                'integer',
                ['min' => 0],
            ],
            [
                'kurs',
                'Курс относительно рубля',
                0,
                'double',
            ],
            [
                'is_crypto',
                'is_crypto',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_view',
                'is_view',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_merchant',
                'is_merchant',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'is_avr_wallet',
                'is_avr_wallet',
                0,
                '\common\widgets\CheckBox2\Validator',
                'widget' => [
                    '\common\widgets\CheckBox2\CheckBox'
                ],
            ],
            [
                'image',
                'Картинка',
                0,
                'string',
                'widget' => [
                    '\iAvatar777\widgets\FileUpload7\FileUpload',
                    [
                        'settings' => [
                            'controller'   => 'upload4',
                            'maxSize'      => 2000,
                            'server'       => '',
                            'button_label' => Yii::t('c.FMg0mLqFR9', 'Выберите файл'),
                        ],
                        'update'     => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => '300',
                                    'height' => '300',
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }
}
