<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Cookie;

/**
 */
class MailTest extends Model
{
    public $mail;

    public function init()
    {
        $e = Yii::$app->request->cookies->get('mailTest');
        if (!is_null($e)) {
            if ($e->value != '') {
                $this->mail = $e->value;
            }
        }
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['mail'], 'required'],
            [['mail'], 'trim'],
            [['mail'], 'email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'mail' => 'Почта',
        ];
    }


    /**
     * @return boolean whether the model passes validation
     */
    public function action()
    {
        return true;
    }
}
