<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class ChatFile extends Model
{
    public $upload1;

    public function rules()
    {
        return [
            ['upload1', 'string'],
        ];
    }
}
