<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\Contribution;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\NeironTransaction;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\models\UserContribution;
use cs\Application;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class CabinetContributionAdd extends Model
{
    public $amount;
    public $amount_atom;
    public $Contribution;


    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'choosePacket'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAmount($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userNeiron = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::NEIRO);
            /** @var \common\models\piramida\Wallet $userWallet */
            $userWallet = $userNeiron['wallet'];

            // Вычисление баланса для стейкинга
            $rows = \common\models\piramida\Operation::find()
                ->innerJoin('neiron_transaction', 'neiron_transaction.operation_id = operations.id')
                ->select([
                    'operations.*',
                    'neiron_transaction.type_id',
                ])
                ->where(['operations.wallet_id' => $userWallet->id])
                ->orderBy(['operations.datetime' => SORT_DESC])
                ->asArray()
                ->all();

            $sum = 0;
            foreach ($rows as $o) {
                switch ($o['type_id']) {
                    case \common\models\NeironTransaction::TYPE_REFERAL:
                    case \common\models\NeironTransaction::TYPE_REFERAL_CONVERT:
                    case \common\models\NeironTransaction::TYPE_REFERAL_CONVERT_BINANCE:
                    case \common\models\NeironTransaction::TYPE_BUY_TRUSTED:
                    case \common\models\NeironTransaction::TYPE_HOLD_PERCENT:
                    case \common\models\NeironTransaction::TYPE_CONVERT:
                    case \common\models\NeironTransaction::TYPE_CONVERT_GOOD:
                        if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                            $sum += $o['amount'];
                        }
                        break;
                }
            }

            $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
            $amount_atom = bcmul($this->amount, pow(10, $c->decimals ));
            $this->amount_atom = $amount_atom;

            // минимум который можно заложить
            if ($sum < $userWallet->amount) $min = $sum;
            else $min = $userWallet->amount;

            // если "минимум который можно заложить" меньше того что укащзывает пользователь?
            if ($min < $amount_atom) {
                $this->addError($attribute, 'Недостаточно средств чтобы сделать стейкинг.');
                return;
            }
        }
    }

    public function choosePacket($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\Contribution $c */
            foreach (Contribution::find()->all() as $c) {
                if ($this->amount_atom >= $c->amount && $this->amount_atom < $c->amount_max) {
                    $this->Contribution = $c;
                    return;
                }
            }
            $this->addError($attribute, 'Нет такого пакета соответствующего указанной вами сумме');
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $w = \common\models\piramida\Wallet::addNew(['currency_id' => \common\models\piramida\Currency::NEIRO]);
        $userNeiron = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::NEIRO);
        /** @var \common\models\piramida\Wallet $userWallet */
        $userWallet = $userNeiron['wallet'];

        $UserContribution = UserContribution::add([
            'contribution_id' => $this->Contribution->id,
            'user_id'         => Yii::$app->user->id,
            'created_at'      => time(),
            'finish_at'       => time() + $this->Contribution->time,
            'wallet_id'       => $w->id,
            'amount'          => $this->amount_atom,
        ]);

        $data = $userWallet->move2(
            $w,
            $this->amount_atom,
            Json::encode([
                'Вклад #{id}',
                [
                    'id' => $UserContribution->id
                ]
            ]),
            NeironTransaction::TYPE_HOLD
        );

        // Делаю запись о типе транзакции если это NERON
        NeironTransaction::add([
            'transaction_id' => $data['transaction']['id'],
            'operation_id'   => $data['operation_sub']['id'],
            'type_id'        => NeironTransaction::TYPE_HOLD,
        ]);

        return $UserContribution;
    }

}