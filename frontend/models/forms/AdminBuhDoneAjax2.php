<?php

namespace avatar\models\forms;

use common\models\CurrencyIO;
use common\models\CurrencyIoModification;
use common\models\NeironTransaction;
use common\models\SendLetter;
use common\models\TaskInput;
use common\models\TaskOutputHistory;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminBuhDoneAjax2 extends Model
{
    public $id;

    /** @var \common\models\TaskOutput */
    public $task;

    public $data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['id', 'required'],
            ['id', 'integer'],

            ['id', 'setRequest'],
            ['id', 'validateAmount'],
            ['id', 'tryBinance'],
        ];

        return $rules;
    }

    /**
     * @param $a
     * @param $p
     */
    public function setRequest($a, $p)
    {
        if (!$this->hasErrors()) {
            $order = \common\models\TaskOutput::findOne($this->id);
            if (is_null($order)) {
                $this->addError($a, 'Заявка не найдена');
                return;
            }
            $this->task = $order;
        }
    }

    /**
     * @param $a
     * @param $p
     */
    public function validateAmount($a, $p)
    {
        if (!$this->hasErrors()) {
            $task = $this->task;
            $cExt = \common\models\avatar\Currency::findOne($task->currency_id);
            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($cExt);

            // Проверяю баланс на бинансе
            /** @var \frontend\modules\Binance\Binance $provider */
            $provider = Yii::$app->Binance;

            if (!Application::isEmpty($task->modification_id)) {
                $mod = CurrencyIoModification::findOne($task->modification_id);
                $binanceFee = $mod->benance_fee;
            } else {
                $cio = CurrencyIO::findOne(['currency_ext_id' => $task->currency_id]);
                $binanceFee = $cio->benance_fee;
            }

            // проверяю балансы
            $data = $provider->_callApiKeyAndSigned('/api/v3/account', []);
            $data['balances'];
            $rows = ArrayHelper::map($data['balances'], 'asset', 'free');
            $balanceBinance = (float)$rows[$cExt->code];
            $taskAmount = ((float)\common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, $cInt)) + ((float)$binanceFee);
            if ($balanceBinance < $taskAmount) {
                $this->addError($a, 'На бирже недостаточно денег для вывода. Сейчас там: ' . $balanceBinance . $cExt->code );
                return;
            }
        }
    }

    /**
     * @param $a
     * @param $p
     */
    public function tryBinance($a, $p)
    {
        if (!$this->hasErrors()) {
            $task = $this->task;
            $cExt = \common\models\avatar\Currency::findOne($task->currency_id);
            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($cExt);

            $network = null;
            if (!Application::isEmpty($task->modification_id)) {
                $mod = CurrencyIoModification::findOne($task->modification_id);
                $binanceFee = $mod->benance_fee;
                $network = $mod->code;
                $decimals = $mod->decimals;
            } else {
                $cio = CurrencyIO::findOne(['currency_ext_id' => $task->currency_id]);
                $binanceFee = $cio->benance_fee;
                $decimals = $cio->decimals;
            }

            /** @var \frontend\modules\Binance\Binance $provider */
            $provider = Yii::$app->Binance;
            $amount = ((float)\common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, $cInt)) + ((float)$binanceFee);
            $amount = \common\models\piramida\Currency::getAtomFromValue($amount, $cInt);
            $amount = bcdiv($amount, pow(10, $cInt->decimals), $decimals);
            $params = [
                'coin'      => $cExt->code,
                'address'   => $task->account,
                'amount'    => $amount,
                'timestamp' => time() * 1000,
            ];
            if (!is_null($network)) {
                $params['network'] = $network;
            }
            try {
                $data = $provider->_callApiKeyPostAndSigned('sapi/v1/capital/withdraw/apply', $params);
                $this->data = $data;
            } catch (\Exception $e) {
                $this->addError($a, 'Ошибка Binance: '.$e->getMessage());
                return;
            }
        }
    }

    public function action()
    {
        $task = $this->task;
        $cExt = \common\models\avatar\Currency::findOne($task->currency_id);
        $data = $this->data;

        // забираю монеты
        $cio = CurrencyIO::findOne($task->currency_io_id);
        $wallet = \common\models\piramida\Wallet::findOne($cio->block_wallet);

        $walletToId = $cio->main_wallet;
        $walletTo = \common\models\piramida\Wallet::findOne($walletToId);
        $t = $wallet->move2(
            $walletTo,
            $task->amount,
            Json::encode([
                Yii::t('c.GGWWLBoXB4', 'Вывод средств по заявке id={id}'),
                ['id' => $task->id],
                ['type_id' => 1],
            ])
        );
        $task->status = \common\models\TaskOutput::STATUS_INIT;
        $task->binance_id = $data['id'];
        $task->txid_internal = $t['transaction']['id'];
        $task->buh_user_id = Yii::$app->user->id;
        $task->save();

        $value = \common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, $cio->currency_int_id);
        $text = 'Вывод ' . $value . ' ' . $cExt->code . ' по заявке #' . $this->task->id . ' инициализирован';

        // Отправляю письмо уведомление клиенту
        /** @var UserAvatar $user */
        $user = UserAvatar::findOne($task->user_id);
        \common\services\Subscribe::sendArray([$user->email], $text,'task_out_init', [
            'task' => $task,
        ]);

        // Добавляю историю заявки
        $i = TaskOutputHistory::add([
            'task_id' => $task->id,
            'comment' => $text,
        ]);

        // Уведомляю пользователю по телеграму
        $user = UserAvatar::findOne($task->user_id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
        }

        // Уведомляю админа и бухгалтера по телеграму
        $userListBuh = Yii::$app->authManager->getUserIdsByRole('role_buh');
        $userListAdmin = Yii::$app->authManager->getUserIdsByRole('role_admin');
        $userList = ArrayHelper::merge($userListBuh, $userListAdmin);
        $userList = array_unique($userList);
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
            }
        }

        return ['id' => $task->binance_id];
    }
}
