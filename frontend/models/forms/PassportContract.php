<?php

namespace avatar\models\forms;

use avatar\models\UserLogin;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\UserAvatar;
use common\services\Security\AES;
use common\widgets\FileUpload4\FileUploadMany;
use cs\base\BaseForm;
use cs\base\FormActiveRecord;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;

/**
 */
class PassportContract extends Model
{
    public $name_first;
    public $name_last;
    public $name_middle;
    public $born_time;
    public $born_date;
    public $born_place;
    public $born_point;
    public $data;
    public $image;

    /** @var  string  */
    public $password;

    public function rules()
    {
        return [
            [[
                'name_first',
                'name_last',
                'name_middle',
                'born_time',
                'born_date',
                'born_place',
                'born_point',
                'data',
                'image',
            ], 'safe'],
            ['password', 'required'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            $passportWalletEth = $user->getWalletEthPassport();

            if ($passportWalletEth->billing->validatePassword($this->password) == false) {
                $this->addError($attribute, 'Не верный пароль');
            }
        }
    }

    public function action()
    {
        $txid = $this->registerNet();

        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $passportWalletEth = $user->getWalletEthPassport();

        $f = UserBillOperation::add([
            'bill_id'     => $passportWalletEth->billing->id,
            'type'        => UserBillOperation::TYPE_OUT,
            'transaction' => $txid,
            'message'     => 'Регистрация пользователя',
            'user_id'     => \Yii::$app->user->id,
        ]);

        return $txid;
    }

    /**
     * Регистрирует пользователя в сети
     *
     * @return string txid
     */
    private function registerNet()
    {
        $field = new FileUploadMany([
            'model'     => $this,
            'attribute' => 'image',
            'value'     => $this->image,
        ]);
        $field->onLoad();
        $file = $this->image[0][2];
        Yii::trace($file, 'avatar\models\forms\PassportContract::action');
        $filePath = Yii::getAlias('@webroot' . $file);
        $content = file_get_contents($filePath);
        $sha256 = hash('sha256', $content);

        // Контракт регистрации
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $passportWalletEth = $user->getWalletEthPassport();

        $contractAddress = '0xf180E0F0b8ae56F01427817C2627F1fc75202929';
        $abi = '[{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"memberData","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"memberId","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"members","outputs":[{"name":"member","type":"address"},{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"approved","type":"bool"},{"name":"memberSince","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getMemberCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getMember","outputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"pks","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getPK","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"name":"addMember","outputs":[],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberChanged","type":"event"}]';
        $func = 'addMember';
        if (!YII_ENV_PROD) {
            $contractAddress = '0x7540224488976689302ce187b402cfc069daf25b';
            $abi = '[{"constant":true,"inputs":[],"name":"getDocumentsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"hash","type":"string"},{"name":"namef","type":"string"},{"name":"namel","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"}],"name":"registerDocument","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"id","type":"uint256"}],"name":"addSignature","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocument","outputs":[{"name":"hash","type":"string"},{"name":"namef","type":"string"},{"name":"namel","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"count","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"documentsIds","outputs":[{"name":"hash","type":"string"},{"name":"namef","type":"string"},{"name":"namel","type":"string"},{"name":"link","type":"string"},{"name":"data","type":"string"},{"name":"creator","type":"address"},{"name":"date","type":"uint256"},{"name":"signsCount","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getDocumentSignsCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"},{"name":"index","type":"uint256"}],"name":"getDocumentSign","outputs":[{"name":"member","type":"address"},{"name":"date","type":"uint256"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"member","type":"address"}],"name":"DocumentSigned","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"id","type":"uint256"},{"indexed":false,"name":"hash","type":"string"}],"name":"DocumentRegistered","type":"event"}]';
            $func = 'registerDocument';
        }

        /**
         * string name,
         * string surname,
         * string patronymic,
         * uint birthDate,
         * string birthPlace,
         * string avatarHash,
         * uint avatarID,
         * string data
         */
        $txid = $passportWalletEth->contract(
            $this->password,
            $contractAddress,
            $abi,
            $func,
            [
                $sha256,
                $this->name_first,
                $this->name_last,
                Url::to($file, true),
                Json::encode([
                    $this->name_middle,
                    $this->born_time,
                    $this->born_date,
                    $this->born_place,
                    $this->born_point,
                    $this->data,
                    AES::encrypt256CBC(Json::encode([
                        $sha256,
                        $this->name_first,
                        $this->name_last,
                        Url::to($file, true),
                        $this->name_middle,
                        $this->born_time,
                        $this->born_date,
                        $this->born_place,
                        $this->born_point,
                        $this->data,
                    ]), UserBill::passwordToKey32($this->password))
                ])
            ],
            'true'
        );
        \Yii::trace($txid, 'avatar\models\forms\PassportContract::action()');

        return $txid;
    }
}
