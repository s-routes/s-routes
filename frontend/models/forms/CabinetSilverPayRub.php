<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatRoom;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\shop\Basket;
use common\models\shop\Product;
use common\models\shop\ProductKorobka;
use common\models\shop\Request;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;

class CabinetSilverPayRub extends \yii\base\Model
{
    /** @var integer  */
    public $id;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    public function action()
    {
        $request1 = \Yii::$app->session->get('shop-request2');

        $request = Request::findOne($this->id);
        // Создаю счет

        $k = ProductKorobka::findOne($request1['korobka']);
        $bm = BillingMainClass::findOne(['name' => '\common\models\shop\Request']);
        $b = BillingMain::add([
            'sum_before'  => $k->price,
            'sum_after'   => $k->price,
            'source_id'   => null,
            'currency_id' => \common\models\avatar\Currency::RUB,
            'class_id'    => $bm->id,
            'config_id'   => null,
            'success_url' => null,
        ]);

        $b->success_url = Url::to('/cabinet-shop-requests/view?id=' . $request->id, true);
        $b->save();

        $request->billing_id = $b->id;
        $request->save();

        return ['id' => $b->id];

    }

}