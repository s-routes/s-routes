<?php
namespace avatar\models\forms;

use avatar\base\Application;
use common\models\exchange\ChatMessage;
use common\models\SendLetter;
use common\models\UserAvatar;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class LandingSend extends Model
{
    public $name;
    public $email;
    public $text;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
            ['name', 'trim'],

            ['email', 'required'],
            ['email', 'string'],
            ['email', 'trim'],
            ['email', 'email'],

            ['text', 'required'],
            ['text', 'string'],
        ];
    }

    public function action()
    {
        $letter = SendLetter::add([
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->text,
        ]);

        Application::mail(Yii::$app->params['landingEmail'], 'Письмо с лендинга', 'landing', [
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->text,
        ]);

        $admins = Yii::$app->authManager->getUserIdsByRole('role_admin');

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        foreach ($admins as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!\cs\Application::isEmpty($u->telegram_chat_id)) {
                /** @var \common\models\UserAvatar $uCurrent */
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => 'Сообщение с лендинга от пользователя ' . $letter->email . ': ' . "\n". $this->text]);
            }
        }

        return true;
    }


}
