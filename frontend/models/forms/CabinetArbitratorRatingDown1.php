<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;

class CabinetArbitratorRatingDown1 extends Model
{
    /** @var int deal.id */
    public $id;

    /** @var \common\models\exchange\Deal */
    public $deal;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateDeal'],
        ];
    }

    public function validateDeal($a, $p)
    {
        if (!$this->hasErrors()) {
            $Deal = Deal::findOne($this->id);
            if (is_null($Deal)) {
                $this->addError($a, 'Не найден Deal ' . $this->id);
                return true;
            }
            $this->deal = $Deal;
        }
        return true;
    }


    public function action()
    {
        $Deal = $this->deal;

        $t = Yii::$app->db->beginTransaction();
        try {
            $offer = $Deal->getOffer();
            $user_id = $Deal->user_id;
            $user_id_author = Yii::$app->user->id;
            $role = ($offer->type_id == Offer::TYPE_ID_SELL)? Offer::TYPE_ID_BUY : Offer::TYPE_ID_SELL ;

            // Добавляю оценку
            Assessment::add([
                'deal_id'        => $Deal->id,
                'value'          => -100,
                'user_id'        => $user_id,
                'user_id_author' => $user_id_author,
                'offer_id'       => $offer->id,
                'offer_type'     => $offer->type_id,
                'role'           => $role,
            ]);

            // прибалвляю значение во всех предложениях
            $r1 = \common\models\exchange\Assessment::find()
                ->where(['user_id' => $user_id])
                ->andWhere(['<', 'value', 0])
                ->select('sum(value)')
                ->scalar();
            Offer::updateAll(['assessment_negative' => $r1], ['user_id' => $user_id]);

            $t->commit();
        } catch ( \Exception $e) {
            $t->rollBack();
        }

        return 1;
    }
}