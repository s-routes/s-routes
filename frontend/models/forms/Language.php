<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use iAvatar777\widgets\FileUpload7\FileUpload;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer id
 * @property string  code
 * @property string  name
 * @property string  image
 */
class Language extends \iAvatar777\services\FormAjax\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'languages';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['title', 'string'],
            ['image', 'string'],
            ['code', 'string'],
            ['status', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Название внутр (рус)',
            'title' => 'Название на языке',
            'image' => 'Картинка',
            'code' => 'КОД 2 символа',
            'status' => 'Статус',
        ];
    }

    public function attributeWidgets()
    {
        return [
            'image' => [
                'class' => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'settings' => [
                    'controller'   => 'upload4',
                    'maxSize'      => 2000,
                    'server'       => '',
                    'button_label' => 'Выберите файл',
                ],
                'update'     => [
                    [
                        'function' => 'crop',
                        'index'    => 'crop',
                        'options'  => [
                            'width'  => '300',
                            'height' => '300',
                            'mode'   => 'MODE_THUMBNAIL_CUT',
                        ],
                    ],
                ],
            ]
        ];
    }

}
