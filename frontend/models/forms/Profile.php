<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 22.11.2016
 * Time: 20:20
 */

namespace avatar\models\forms;


use avatar\services\Html;
use common\widgets\FileUpload3\FileUpload;
use cs\base\FormActiveRecord;
use yii\base\Exception;
use yii\widgets\ActiveForm;
use Yii;

class Profile extends FormActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    /**
     * Ищт профиль в БД
     * @param mixed $condition
     * @return \avatar\models\forms\Profile
     * @throws
     */
    public static function findOne($condition)
    {
        $i = parent::findOne($condition);
        if (is_null($i)) {
            throw new Exception(Yii::t('c.FMg0mLqFR9', 'Не найден профиль'));
        }

        return $i;
    }


    function formAttributes()
    {
        return [
            ['name_first', Yii::t('c.FMg0mLqFR9', 'Ник'), 1, 'string'],
            ['name_last', Yii::t('c.FMg0mLqFR9', 'Имя'), 0, 'string'],
            ['country_id', Yii::t('c.FMg0mLqFR9', 'Страна'), 0, 'integer'],
            ['town', Yii::t('c.FMg0mLqFR9', 'Город'), 0, 'string'],
            ['time_zone', Yii::t('c.FMg0mLqFR9', 'Временная зона'), 0, 'string'],
            ['address', Yii::t('c.FMg0mLqFR9', 'Адрес'), 0, 'string'],
            ['public_telegram', Yii::t('c.FMg0mLqFR9', 'Адрес телеграмм'), 0, 'string'],
            ['is_show_telegram', Yii::t('c.FMg0mLqFR9', 'Показывать адрес телеграмм в профиле'), 0, 'integer'],
            [
                'avatar', Yii::t('c.FMg0mLqFR9', 'Аватарка'), 0, 'default',
                'widget' => [
                    '\iAvatar777\widgets\FileUpload7\FileUpload',
                    [
                        'settings' => [
                            'controller'   => 'upload4',
                            'maxSize'      => 2000,
                            'server'       => '',
                            'button_label' => Yii::t('c.FMg0mLqFR9', 'Выберите файл'),
                        ],
                        'update'     => [
                            [
                                'function' => 'crop',
                                'index'    => 'crop',
                                'options'  => [
                                    'width'  => '300',
                                    'height' => '300',
                                    'mode'   => 'MODE_THUMBNAIL_CUT',
                                ],
                            ],
                        ],
                    ],
                ]
            ],
        ];
    }
} 