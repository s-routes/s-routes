<?php
namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 */
class ConvertMarketCoin extends Model
{
    /** @var  \common\models\avatar\Currency */
    public $currency;

    public $amount;

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'validate12Usd'],
        ];
    }

    public function validate12Usd($a, $v)
    {
        if (!$this->hasErrors()) {
            $priceUsd = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::USDT)->kurs;
            $p = $this->amount / $priceUsd;
            if ($p < 100) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Минимальная сумма конвертации не может быть меньше эквивалентной 100 USDT. Сейчас у вас заказ ={sum} USDТ', ['sum' => Yii::$app->formatter->asDecimal($p, 2)]));
                return;
            }
        }
    }

    public function validateAmount($a, $v)
    {
        if (!$this->hasErrors()) {
            $cLink = CurrencyLink::findOne(['currency_ext_id' => \common\models\avatar\Currency::MARKET]);

            // Забираю монеты
            $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletFrom = $dataTo['wallet'];
            $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $cLink->currency_ext_id]);
            $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
            $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));

            if ($amountFrom > $walletFrom->amount) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $fromCurrencyLink = CurrencyLink::findOne(['currency_ext_id' => \common\models\avatar\Currency::MARKET]);
        $fromCurrencyExt = \common\models\avatar\Currency::findOne($fromCurrencyLink->currency_ext_id);

        $operetion = ConvertOperation::add([
            'wallet_from_id'   => 1,
        ]);

        // Забираю монеты
        $fromData = UserBill::getInternalCurrencyWallet($fromCurrencyLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $fromWalletOut */
        $fromWalletOut = $fromData['wallet'];
        $fromCIO = CurrencyIO::findOne(['currency_ext_id' => $fromCurrencyLink->currency_ext_id]);
        $fromWalletIn = Wallet::findOne($fromCIO->main_wallet);
        $fromCurrencyInt = \common\models\piramida\Currency::findOne($fromCIO->currency_int_id);
        $fromAmount = bcmul($this->amount, bcpow(10, $fromCurrencyInt->decimals));
        $fromTx = $fromWalletOut->move($fromWalletIn, $fromAmount, Json::encode([
            'Конвертация MARKET в {to} id={id}',
            [
                'id' => $operetion->id,
                'to' => $this->currency->code,
            ],
        ]));

        // Отдаю монеты
        $toCurrencyLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);
        $toCurrencyInt = \common\models\piramida\Currency::findOne($toCurrencyLink->currency_int_id);
        $toCIO = CurrencyIO::findOne(['currency_int_id' => $toCurrencyInt->id]);
        $toCurrencyExt = \common\models\avatar\Currency::findOne($toCIO->currency_ext_id);
        $toWalletOut = Wallet::findOne($toCIO->main_wallet);
        $toData = UserBill::getInternalCurrencyWallet($toCurrencyInt->id, Yii::$app->user->id);

        // получаю коммию системы
        $dataJson = Config::get('MarketConvertParams');
        $data = Json::decode($dataJson);
        $o = $this->getId($data, $fromCIO->currency_ext_id);

        // Определяю кол-во монет
        $kurs = $fromCurrencyExt->price_usd * (1 - ($o['out'] / 100)) / $toCurrencyExt->price_usd;
        $toAmount_m = $this->amount * $kurs;
        $toAmount = bcmul($toAmount_m, bcpow(10, $toCurrencyInt->decimals));

        /** @var \common\models\piramida\Wallet $toWalletIn */
        $toWalletIn = $toData['wallet'];
        $toTx = $toWalletOut->move($toWalletIn, $toAmount, Json::encode([
            'Конвертация MARKET в {to} id={id}',
            [
                'id' => $operetion->id,
                'to' => $this->currency->code,
            ],
        ]));

        $operetion->user_id = Yii::$app->user->id;
        $operetion->from_currency_id = $fromCIO->currency_int_id;
        $operetion->wallet_from_id = $fromWalletOut->id;
        $operetion->tx_from_id = $fromTx->id;
        $operetion->amount = $fromAmount;
        $operetion->from_price_usd = $fromCurrencyExt->price_usd;

        $operetion->kurs = (int) ($kurs * 1000000);

        $operetion->to_currency_id = $toCIO->currency_int_id;
        $operetion->wallet_to_id = $toWalletIn->id;
        $operetion->tx_to_id = $toTx->id;
        $operetion->amount_to = $toAmount;
        $operetion->to_price_usd = $toCurrencyExt->price_usd;

        $operetion->save();


        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

                try {
                    $userBill = \common\models\avatar\UserBill::findOne(['address' => $operetion->wallet_from_id]);
                } catch (\Exception $e) {
                    return '';
                }
                $taskUser = \common\models\UserAvatar::findOne($userBill->user_id);

                $from = Yii::$app->formatter->asDecimal($this->amount, $fromCurrencyInt->decimals) . ' ' . $fromCurrencyInt->code;
                $currencyToInt = \common\models\piramida\Currency::findOne($toCIO->currency_int_id);
                $to = Yii::$app->formatter->asDecimal($toAmount_m, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

                $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'User-ID {user}. Проведена конвертация {from} в {to}', ['user' => $taskUser->getAddress(), 'from' => $from, 'to' => $to], $language)]);
            }
        }

        // Уведомляю клиента по телеграму
        $user = UserAvatar::findOne(Yii::$app->user->id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

            $from = Yii::$app->formatter->asDecimal($this->amount, $fromCurrencyInt->decimals) . ' ' . $fromCurrencyInt->code;
            $currencyToInt = Currency::findOne($toCIO->currency_int_id);
            $to = Yii::$app->formatter->asDecimal($toAmount_m, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Проведена конвертация {from} в {to}', ['from' => $from, 'to' => $to], $language)]);
        }

        return $operetion->id;
    }

    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id' => $id,
            'in' => 0,
            'out' => 0,
        ];
    }
}
