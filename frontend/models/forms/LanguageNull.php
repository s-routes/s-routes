<?php

namespace avatar\models\forms;

use app\models\ActionsRange;
use cabinet\base\Str;
use cabinet\models\NewsText;
use common\components\EmailVerifier;
use common\models\Config;
use common\models\Language;
use common\models\language\Message;
use common\services\Debugger;
use yii\base\Model;
use Yii;

class LanguageNull extends Model
{
    public $language;

    /** @var  \common\models\language\Message[] */
    public $messages;

    public function rules()
    {
        return [
            ['language', 'string'],
        ];
    }

    /**
     * Сохраняет все языки в БД
     *
     * @return bool
     */
    public function search()
    {

    }

    public function attributeLabels()
    {
        return [
            'language' => 'Язык',
        ];
    }

}