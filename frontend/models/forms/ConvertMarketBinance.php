<?php
namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\ShopReferalTransaction;
use common\models\ShopUserPartner;
use common\models\UserAvatar;
use common\models\UserBill2;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 * COIN > MARKET
 */
class ConvertMarketBinance extends Model
{
    /** @var  \common\models\avatar\Currency */
    public $currency;

    public $amount;

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'validate12Usd'],
        ];
    }

    public function validate12Usd($a, $v)
    {
        if (!$this->hasErrors()) {
            $priceUsd = $this->currency->price_usd;
            $p = $this->amount * $priceUsd;
            if ($p < 12) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT. Сейчас у вас заказ ={sum} USDТ', ['sum' => Yii::$app->formatter->asDecimal($p, 2)]));
                return;
            }
        }
    }

    public function validateAmount($a, $v)
    {
        if (!$this->hasErrors()) {
            $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

            // Забираю монеты
            $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletFrom = $dataTo['wallet'];
            $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
            $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
            $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));

            if ($amountFrom > $walletFrom->amount) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // Проверяю баланс на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        // проверяю балансы
        $data = $provider->_callApiKeyAndSigned('/api/v3/account', []);
        $rows = ArrayHelper::map($data['balances'], 'asset', 'free');

        // Если баланс на бирже меньше чем указал пользователь?
        if ($rows[$this->currency->code] < $this->amount) {
            // создаю заявку на конвертацию и забираю монеты

            $id = $this->binanceNo();
        } else {
            $id = $this->binanceYes();
        }

        return $id;
    }

    /**
     * Если денег на бинансе достаточно то создает ордер на продажу
     *
     * @return mixed|null
     * @throws \Exception
     */
    private function binanceYes()
    {
        $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

        // Создаю ордер на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        /**
        'symbol' => 'BTCUSDT'
        'orderId' => 78364
        'orderListId' => -1
        'clientOrderId' => 'ikAmswABSyH35iAmNG3ama'
        'transactTime' => 1611765998657
        'price' => '0.00000000'
        'origQty' => '0.01000000'
        'executedQty' => '0.00841200'
        'cummulativeQuoteQty' => '192.10515000'
        'status' => 'EXPIRED'
        'timeInForce' => 'GTC'
        'type' => 'MARKET'
        'side' => 'SELL'
        'fills' => [
        0 => [
        'price' => '32250.00000000'
        'qty' => '0.00012900'
        'commission' => '0.00000000'
        'commissionAsset' => 'USDT'
        'tradeId' => 4980
        ]
        ]
        ]
         */

        try {
            $data = $provider->_callApiKeyPostAndSigned('/api/v3/order', [
                'symbol'           => $this->currency->code . 'USDT',
                'side'             => 'sell', // продаю
                'type'             => 'MARKET',
                'quantity'         => $this->amount,
                'newOrderRespType' => 'FULL',
            ]);
        } catch (\Exception $e) {
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Ошибка в Binance convert: {text}', ['text'   => $e->getMessage()]
            ));
            throw $e;
        }

        $cIO = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $cInt = \common\models\piramida\Currency::findOne($cIO->currency_int_id);
        $data2 = UserBill::getInternalCurrencyWallet($cInt->id);
        $BinanceOrder = \common\models\BinanceOrder::add([
            'binance_id'       => $data['clientOrderId'],
            'binance_order_id' => $data['orderId'],
            'user_id'          => Yii::$app->user->id,
            'billing_id'       => $data2['billing']['id'],
            'amount'           => (int)($this->amount * pow(10, $cInt->decimals)),
            'binance_return'   => Json::encode($data),
            'currency_to'      => \common\models\avatar\Currency::MARKET,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move($walletTo, $amountFrom, Json::encode([
            'Конвертация в MARKET bid={id}',
            ['id' => $BinanceOrder->id],
        ]));

        $BinanceOrder->tx_from_id = $t_from->id;
        $BinanceOrder->save();

        /** Если заявка выполнена? */
        if ($data['status'] == 'FILLED') {

            // возвращаю деньги
            $amount1 = $data['cummulativeQuoteQty'];

            // Отдаю монеты
            $cInt_to = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::MARKET);
            $CIO_to = CurrencyIO::findOne(['currency_int_id' => $cInt_to->id]);
            $toCurrencyExt = \common\models\avatar\Currency::findOne($CIO_to->currency_ext_id);
            $walletOut = Wallet::findOne($CIO_to->main_wallet);
            $dataTo = UserBill::getInternalCurrencyWallet($cInt_to->id, Yii::$app->user->id);

            // получаю комиссию системы
            $dataJson = Config::get('MarketConvertParams');
            $data = Json::decode($dataJson);
            $o = $this->getId($data, $CIOfrom->currency_ext_id);
            $amount_m_to = $amount1 * (1 - ($o['in'] / 100)) / $toCurrencyExt->price_usd;
            $amountTo = bcmul($amount_m_to, bcpow(10, $cInt_to->decimals));

            // колво маркетов 100% без учета процентов вычета
            $amount100 = $amount1 / $toCurrencyExt->price_usd;

            // Вычисляю тип транзакции
            $type = null;

            $t_to = $walletOut->move2(
                $dataTo['wallet'],
                $amountTo,
                Json::encode([
                    'Конвертация в MARKET bid={id}',
                    ['id' => $BinanceOrder->id],
                ]),
                $type
            );

            // Устанавливаю статус
            $BinanceOrder->tx_to_id = $t_to['transaction']['id'];
            $BinanceOrder->amount_to = $amountTo;
            $BinanceOrder->status = $BinanceOrder::STATUS_FILLED;
            $BinanceOrder->save();

            // раскидываю рефералку
            self::referal($BinanceOrder->id, $walletOut, Yii::$app->user->id, Currency::getAtomFromValue($amount100, $cInt_to->id));

            // Уведомляю пользователя по телеграму
            $user = UserAvatar::findOne($BinanceOrder->user_id);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        'Конвертация #{id} выполнена. Получено {sum} {code}',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => $amount_m_to,
                            'code' => $cInt_to->code,
                        ],
                        $language
                    ),
                ]);
            }

            // Уведомляю бухгалтера по телеграму
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Конвертация #{id} выполнена. Получено {sum} {code}',
                [
                    'id'   => $BinanceOrder->id,
                    'sum'  => $amount_m_to,
                    'code' => $cInt_to->code,
                ]
            ));
        }

        return $BinanceOrder->id;
    }

    /**
     * раскидывает рефералку
     *
     * @param int $oid binance_order.id
     * @param \common\models\piramida\Wallet $walletOut откуда брать деньги на рефералку
     * @param int  $uid пользователь кто вызвал событие рефералки, совершил конвертацию
     * @param int  $amount кол-во атомов цены от которой считаются проценты на рефералку
     *
     */
    public static function referal($oid, $walletOut, $uid, $amount)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $cMar = Currency::findOne(['code' => 'MAR']);
        $levels = Yii::$app->params['shop-referal-program']['level-list1'];
        $uCurrent = $uid;
        $u1 = UserAvatar::findOne($uid);
        for ($i = 0; $i < 3; $i++) {
            $s = ShopUserPartner::findOne(['user_id' => $uCurrent]);
            if (is_null($s)) break;
            $parent_id = $s->parent_id;
            $u = \common\models\UserAvatar::findOne($parent_id);

            // Если куплен пакет то user.market_packet > 0 and user.market_till < time() and wallet.amount > 0
            $data2 = UserBill2::findOne(['user_id' => $u->id, 'currency_id' => $cMar->id]);
            $w1 = Wallet::findOne($data2->wallet_id);
            if ($u['market_till'] > 0) {
                if (($u['market_packet'] > 0) and ($u['market_till'] > time()) and ($w1->amount > 0)) {
                    $dataTo = UserBill::getInternalCurrencyWallet($walletOut->currency_id, $u->id);
                    $w = $dataTo['wallet'];
                    $a = (int) ($amount * ($levels[$i]/100));
                    $t_to = $walletOut->move2($w, $a, Json::encode([
                        'Реферальное начисление от {from} к {to}',
                        [
                            'from' => $uid,
                            'to'   => $u->id,
                        ],
                    ]));

                    $ShopReferalTransaction = ShopReferalTransaction::add([
                        'request_id'     => $oid,
                        'type_id'        => \common\models\ShopReferalTransaction::TYPE_BINANCE,
                        'currency_id'    => $walletOut->currency_id,
                        'level'          => $i + 1,
                        'from_uid'       => $uid,
                        'to_uid'         => $u->id,
                        'amount'         => $a,
                        'transaction_id' => $t_to['transaction']['id'],
                    ]);

                    // уведомляю по телеграму
                    $userTelegram = $u;
                    $uC = UserAvatar::findOne($u1);
                    if (!Application::isEmpty($userTelegram->telegram_shop_chat_id)) {
                        try {
                            $telegram->sendMessage([
                                'chat_id' => $userTelegram->telegram_chat_id,
                                'text'    => sprintf('Вам начислено реферальное вознаграждение %s МБ. Ваш реферал: USER ID-%s, линия %s',
                                    Currency::getValueFromAtom($a, $walletOut->currency_id),
                                    $uC->getAddress(),
                                    $i + 1
                                ),
                            ]);
                        } catch (\Exception $e) {
                            Yii::warning($e->getMessage(), 'avatar\shop\models\forms\CabinetMarkeyBuy::referal()');
                        }
                    }
                }
            }

            // текущим становится $parent_id
            $uCurrent = $parent_id;
        }
    }

    private function telegram($message)
    {
        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => $message,
                ]);
            }
        }
    }

    /**
     * Если на бинансе денег не хваьает
     * - создаю ордер
     * - забираю монеты
     * - уведомляю бухгалтера
     *
     * @return int
     */
    private function binanceNo()
    {
        $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

        $cIO = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $cInt = \common\models\piramida\Currency::findOne($cIO->currency_int_id);
        $data2 = UserBill::getInternalCurrencyWallet($cInt->id);
        $BinanceOrder = \common\models\BinanceOrder::add([
            'user_id'          => Yii::$app->user->id,
            'billing_id'       => $data2['billing']['id'],
            'amount'           => (int)($this->amount * pow(10, $cInt->decimals)),
            'currency_to'      => \common\models\avatar\Currency::MARKET,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move(
            $walletTo,
            $amountFrom,
            Json::encode([
                'Конвертация в MARKET bid={id}',
                ['id' => $BinanceOrder->id],
            ])
        );

        $BinanceOrder->tx_from_id = $t_from->id;
        $BinanceOrder->save();

        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        '!!! Конвертация #{id} не выполнена, денег на бирже меньше чем указал пользователь. Получено {sum} {code}',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => $this->amount,
                            'code' => $cInt->code,
                        ],
                        $language
                    ),
                ]);
            }
        }

        return $BinanceOrder->id;
    }

    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'  => $id,
            'in'  => 0,
            'out' => 0,
        ];
    }
}
