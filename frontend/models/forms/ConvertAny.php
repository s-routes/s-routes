<?php
namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\ShopReferalTransaction;
use common\models\ShopUserPartner;
use common\models\UserAvatar;
use common\models\UserBill2;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 * COIN > COIN
 */
class ConvertAny extends Model
{
    /** @var  \common\models\avatar\Currency */
    public $currencyFrom;

    /** @var  \common\models\avatar\Currency */
    public $currencyTo;

    public $amount;

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'validate12Usd'],
//            ['amount', 'validatePrizm'],
        ];
    }

    public function validateAmount($a, $v)
    {
        if (!$this->hasErrors()) {
            $cio = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);

            $dataTo = UserBill::getInternalCurrencyWallet($cio->currency_int_id, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletFrom = $dataTo['wallet'];
            $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);
            $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
            $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));

            if ($amountFrom > $walletFrom->amount) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }


    public function validate12Usd($a, $v)
    {
        if (!$this->hasErrors()) {
            $priceUsd = $this->currencyFrom->price_usd;
            $p = $this->amount * $priceUsd;
            if ($p < 12) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT. Сейчас у вас заказ ={sum} USDТ', ['sum' => Yii::$app->formatter->asDecimal($p, 2)]));
                return;
            }
        }
    }

    public function validatePrizm($a, $v)
    {
        if (!$this->hasErrors()) {
            if ($this->currencyFrom->id == \common\models\avatar\Currency::PZM) {
                $c = \common\models\Config::get('pzm_exchange_limit');
                $v = \common\models\piramida\Currency::getAtomFromValue($this->amount, \common\models\piramida\Currency::PZM);
                if ($v > $c) {
                    $this->addError($a, 'Введенное Вами количество монет превышает общий доступный для конвертации объем актива.');
                    return;
                }
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $binanceFrom = [
            \common\models\avatar\Currency::BTC,
            \common\models\avatar\Currency::ETH,
            \common\models\avatar\Currency::LTC,
            \common\models\avatar\Currency::TRX,
            \common\models\avatar\Currency::DASH,
            \common\models\avatar\Currency::BNB,
            \common\models\avatar\Currency::DAI,
            \common\models\avatar\Currency::DOGE
        ];

        if (in_array($this->currencyFrom->id, $binanceFrom)) {
            return $this->actionExternal();
        } else {
            return $this->actionInternal();
        }
    }


    public function actionInternal()
    {
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);

        $operetion = ConvertOperation::add([
            'wallet_from_id'   => 1,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($CIOfrom->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move($walletTo, $amountFrom, Json::encode([
            'Конвертация {from} в {to} id={id}',
            [
                'from' => $this->currencyFrom->code,
                'to'   => $this->currencyTo->code,
                'id'   => $operetion->id,
            ],
        ]));
        $fromCurrencyExt = $this->currencyFrom;

        // Отдаю монеты
        $toCurrencyExt = $this->currencyTo;
        $CIO_to = CurrencyIO::findOne(['currency_ext_id' => $this->currencyTo->id]);
        $cInt = \common\models\piramida\Currency::findOne($CIO_to->currency_int_id);
        $walletOut = Wallet::findOne($CIO_to->main_wallet);
        $dataTo = UserBill::getInternalCurrencyWallet($cInt->id, Yii::$app->user->id);

        // получаю коммию системы
        $dataJson = Config::get('ConvertParams');
        $data = Json::decode($dataJson);
        $o = $this->getId($data);

        // Определяю кол-во монет
        $kurs = $fromCurrencyExt->price_usd * (1-(($o['percent']) / 100)) / $toCurrencyExt->price_usd;
        $amount_m = $this->amount * $kurs;
        $amount_m_to = $amount_m;
        $amount = bcmul($amount_m, bcpow(10, $cInt->decimals));

        $kurs2 = $fromCurrencyExt->price_usd / $toCurrencyExt->price_usd;
        $amount100 = $this->amount * $kurs2;

        /** @var \common\models\piramida\Wallet $walletTo */
        $walletTo = $dataTo['wallet'];
        $t_to = $walletOut->move($walletTo, $amount, Json::encode([
            'Конвертация {from} в {to} id={id}',
            [
                'from' => $this->currencyFrom->code,
                'to'   => $this->currencyTo->code,
                'id'   => $operetion->id,
            ],
        ]));

        $operetion->user_id = Yii::$app->user->id;
        $operetion->wallet_from_id = $walletFrom->id;
        $operetion->wallet_to_id = $walletTo->id;
        $operetion->tx_from_id = $t_from->id;
        $operetion->tx_to_id = $t_to->id;
        $operetion->kurs = (int) ($kurs * 1000000);
        $operetion->amount = $amountFrom;
        $operetion->amount_to = $amount;
        $operetion->from_currency_id = $CIOfrom->currency_int_id;
        $operetion->to_currency_id = $CIO_to->currency_int_id;

        $operetion->from_price_usd = $fromCurrencyExt->price_usd;
        $operetion->to_price_usd = $toCurrencyExt->price_usd;

        $operetion->save();

        // вычитаю лимит PZM
        if ($this->currencyFrom->id == \common\models\avatar\Currency::PZM) {
            $c = \common\models\Config::get('pzm_exchange_limit');
            $v = \common\models\piramida\Currency::getAtomFromValue($this->amount, \common\models\piramida\Currency::PZM);
            $c = $c - $v;
            \common\models\Config::set('pzm_exchange_limit', $c);
        }

        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            try {
                $user = \common\models\UserAvatar::findOne($uid);
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

                    try {
                        $userBill = \common\models\avatar\UserBill::findOne(['address' => $operetion->wallet_from_id]);
                    } catch (\Exception $e) {
                        return '';
                    }
                    $taskUser = \common\models\UserAvatar::findOne($userBill->user_id);

                    $from = Yii::$app->formatter->asDecimal($this->amount, $currencyFromInt->decimals) . ' ' . $currencyFromInt->code;
                    $currencyToInt = \common\models\piramida\Currency::findOne($CIO_to->currency_int_id);
                    $to = Yii::$app->formatter->asDecimal($amount_m_to, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'User-ID {user}. Проведена конвертация {from} в {to}', ['user' => $taskUser->getAddress(), 'from' => $from, 'to' => $to], $language)]);
                }
            }  catch (\Exception $e) {

            }
        }

        // Уведомляю клиента по телеграму
        $user = UserAvatar::findOne(Yii::$app->user->id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

            $from = Yii::$app->formatter->asDecimal($this->amount, $currencyFromInt->decimals) . ' ' . $currencyFromInt->code;
            $currencyToInt = Currency::findOne($CIO_to->currency_int_id);
            $to = Yii::$app->formatter->asDecimal($amount_m_to, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Проведена конвертация {from} в {to}', ['from' => $from, 'to' => $to], $language)]);
        }


        return $operetion->id;
    }

    public function getId($data)
    {
        $id = $this->currencyFrom->id . '_' . $this->currencyTo->id;
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'      => $id,
            'percent' => 0,
            'apply'   => 0,
        ];
    }


    public function actionExternal()
    {
        // Проверяю баланс на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        // проверяю балансы
        $data = $provider->_callApiKeyAndSigned('/api/v3/account', []);
        $rows = ArrayHelper::map($data['balances'], 'asset', 'free');

        // Если баланс на бирже меньше чем указал пользователь?
        if ($rows[$this->currencyFrom->code] < $this->amount) {
            // создаю заявку на конвертацию и забираю монеты

            $id = $this->binanceNo();
        } else {
            $id = $this->binanceYes();
        }

        return $id;
    }


    /**
     * Если денег на бинансе достаточно то создает ордер на продажу
     *
     * @return mixed|null
     * @throws \Exception
     */
    private function binanceYes()
    {
        $cLink = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);

        // Создаю ордер на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        /**
        'symbol' => 'BTCUSDT'
        'orderId' => 78364
        'orderListId' => -1
        'clientOrderId' => 'ikAmswABSyH35iAmNG3ama'
        'transactTime' => 1611765998657
        'price' => '0.00000000'
        'origQty' => '0.01000000'
        'executedQty' => '0.00841200'
        'cummulativeQuoteQty' => '192.10515000'
        'status' => 'EXPIRED'
        'timeInForce' => 'GTC'
        'type' => 'MARKET'
        'side' => 'SELL'
        'fills' => [
        0 => [
        'price' => '32250.00000000'
        'qty' => '0.00012900'
        'commission' => '0.00000000'
        'commissionAsset' => 'USDT'
        'tradeId' => 4980
        ]
        ]
        ]
         */

        try {
            $data = $provider->_callApiKeyPostAndSigned('/api/v3/order', [
                'symbol'           => $this->currencyFrom->code . 'USDT',
                'side'             => 'sell', // продаю
                'type'             => 'MARKET',
                'quantity'         => $this->amount,
                'newOrderRespType' => 'FULL',
            ]);
        } catch (\Exception $e) {
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Ошибка в Binance convert: {text}', ['text'   => $e->getMessage()]
            ));
            throw $e;
        }

        $cIO = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);
        $cInt = \common\models\piramida\Currency::findOne($cIO->currency_int_id);
        $data2 = UserBill::getInternalCurrencyWallet($cInt->id);
        $BinanceOrder = \common\models\BinanceOrder::add([
            'binance_id'       => $data['clientOrderId'],
            'binance_order_id' => $data['orderId'],
            'user_id'          => Yii::$app->user->id,
            'billing_id'       => $data2['billing']['id'],
            'amount'           => (int)($this->amount * pow(10, $cInt->decimals)),
            'binance_return'   => Json::encode($data),
            'currency_to'      => $this->currencyTo->id,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move($walletTo, $amountFrom, Json::encode([
            'Конвертация {from} в {to} bid={id}',
            [
                'from' => $this->currencyFrom->code,
                'to'   => $this->currencyTo->code,
                'id'   => $BinanceOrder->id,
            ],
        ]));

        $BinanceOrder->tx_from_id = $t_from->id;
        $BinanceOrder->save();

        /** Если заявка выполнена? */
        if ($data['status'] == 'FILLED') {

            // возвращаю деньги
            $amount1 = $data['cummulativeQuoteQty'];

            // Отдаю монеты
            $toCurrencyExt = $this->currencyTo;
            $CIO_to = CurrencyIO::findOne(['currency_ext_id' => $toCurrencyExt->id]);
            $cInt_to = \common\models\piramida\Currency::findOne($CIO_to->currency_int_id);
            $walletOut = Wallet::findOne($CIO_to->main_wallet);
            $dataTo = UserBill::getInternalCurrencyWallet($cInt_to->id, Yii::$app->user->id);

            // получаю комиссию системы
            $dataJson = Config::get('ConvertParams');
            $data = Json::decode($dataJson);
            $o = $this->getId($data);
            $amount_m_to = $amount1 * (1 - ($o['percent'] / 100)) / $toCurrencyExt->price_usd;
            $amountTo = bcmul($amount_m_to, bcpow(10, $cInt_to->decimals));

            // колво маркетов 100% без учета процентов вычета
            $amount100 = $amount1 / $toCurrencyExt->price_usd;

            // Вычисляю тип транзакции
            $type = null;

            $t_to = $walletOut->move2(
                $dataTo['wallet'],
                $amountTo,
                Json::encode([
                    'Конвертация {from} в {to} bid={id}',
                    [
                        'from' => $this->currencyFrom->code,
                        'to'   => $this->currencyTo->code,
                        'id'   => $BinanceOrder->id,
                    ],
                ]),
                $type
            );

            // Устанавливаю статус
            $BinanceOrder->tx_to_id = $t_to['transaction']['id'];
            $BinanceOrder->amount_to = $amountTo;
            $BinanceOrder->status = $BinanceOrder::STATUS_FILLED;
            $BinanceOrder->save();

            // раскидываю рефералку
            self::referal($BinanceOrder->id, $walletOut, Yii::$app->user->id, Currency::getAtomFromValue($amount100, $cInt_to->id));

            // Уведомляю пользователя по телеграму
            $user = UserAvatar::findOne($BinanceOrder->user_id);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        'Конвертация #{id} выполнена. Получено {sum} {code}',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => $amount_m_to,
                            'code' => $cInt_to->code,
                        ],
                        $language
                    ),
                ]);
            }

            // Уведомляю бухгалтера по телеграму
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Конвертация #{id} выполнена. Получено {sum} {code}',
                [
                    'id'   => $BinanceOrder->id,
                    'sum'  => $amount_m_to,
                    'code' => $cInt_to->code,
                ]
            ));
        }

        return $BinanceOrder->id;
    }

    /**
     * раскидывает рефералку
     *
     * @param int $oid binance_order.id
     * @param \common\models\piramida\Wallet $walletOut откуда брать деньги на рефералку
     * @param int  $uid пользователь кто вызвал событие рефералки, совершил конвертацию
     * @param int  $amount кол-во атомов цены от которой считаются проценты на рефералку
     *
     */
    public static function referal($oid, $walletOut, $uid, $amount)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $cMar = Currency::findOne(['code' => 'MAR']);
        $levels = Yii::$app->params['shop-referal-program']['level-list1'];
        $uCurrent = $uid;
        $u1 = UserAvatar::findOne($uid);
        for ($i = 0; $i < 3; $i++) {
            $s = ShopUserPartner::findOne(['user_id' => $uCurrent]);
            if (is_null($s)) break;
            $parent_id = $s->parent_id;
            $u = \common\models\UserAvatar::findOne($parent_id);

            // Если куплен пакет то user.market_packet > 0 and user.market_till < time() and wallet.amount > 0
            $data2 = UserBill2::findOne(['user_id' => $u->id, 'currency_id' => $cMar->id]);
            $w1 = Wallet::findOne($data2->wallet_id);
            if ($u['market_till'] > 0) {
                if (($u['market_packet'] > 0) and ($u['market_till'] > time()) and ($w1->amount > 0)) {
                    $dataTo = UserBill::getInternalCurrencyWallet($walletOut->currency_id, $u->id);
                    $w = $dataTo['wallet'];
                    $a = (int) ($amount * ($levels[$i]/100));
                    $t_to = $walletOut->move2($w, $a, Json::encode([
                        'Реферальное начисление от {from} к {to}',
                        [
                            'from' => $uid,
                            'to'   => $u->id,
                        ],
                    ]));

                    $ShopReferalTransaction = ShopReferalTransaction::add([
                        'request_id'     => $oid,
                        'type_id'        => \common\models\ShopReferalTransaction::TYPE_BINANCE,
                        'currency_id'    => $walletOut->currency_id,
                        'level'          => $i + 1,
                        'from_uid'       => $uid,
                        'to_uid'         => $u->id,
                        'amount'         => $a,
                        'transaction_id' => $t_to['transaction']['id'],
                    ]);

                    // уведомляю по телеграму
                    $userTelegram = $u;
                    $uC = UserAvatar::findOne($u1);
                    if (!Application::isEmpty($userTelegram->telegram_shop_chat_id)) {
                        try {
                            $telegram->sendMessage([
                                'chat_id' => $userTelegram->telegram_chat_id,
                                'text'    => sprintf('Вам начислено реферальное вознаграждение %s МБ. Ваш реферал: USER ID-%s, линия %s',
                                    Currency::getValueFromAtom($a, $walletOut->currency_id),
                                    $uC->getAddress(),
                                    $i + 1
                                ),
                            ]);
                        } catch (\Exception $e) {
                            Yii::warning($e->getMessage(), 'avatar\shop\models\forms\CabinetMarkeyBuy::referal()');
                        }
                    }
                }
            }

            // текущим становится $parent_id
            $uCurrent = $parent_id;
        }
    }

    private function telegram($message)
    {
        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => $message,
                ]);
            }
        }
    }

    /**
     * Если на бинансе денег не хваьает
     * - создаю ордер
     * - забираю монеты
     * - уведомляю бухгалтера
     *
     * @return int
     */
    private function binanceNo()
    {
        $cLink = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);

        $cIO = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);
        $cInt = \common\models\piramida\Currency::findOne($cIO->currency_int_id);
        $data2 = UserBill::getInternalCurrencyWallet($cInt->id);
        $BinanceOrder = \common\models\BinanceOrder::add([
            'user_id'          => Yii::$app->user->id,
            'billing_id'       => $data2['billing']['id'],
            'amount'           => (int)($this->amount * pow(10, $cInt->decimals)),
            'currency_to'      => $this->currencyTo->id,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currencyFrom->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move(
            $walletTo,
            $amountFrom,
            Json::encode([
                'Конвертация {from} в {to} bid={id}',
                [
                    'from' => $this->currencyFrom->code,
                    'to'   => $this->currencyTo->code,
                    'id'   => $BinanceOrder->id,
                ],
            ])
        );

        $BinanceOrder->tx_from_id = $t_from->id;
        $BinanceOrder->save();

        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        '!!! Конвертация #{id} не выполнена, денег на бирже меньше чем указал пользователь. Получено {sum} {code}',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => $this->amount,
                            'code' => $cInt->code,
                        ],
                        $language
                    ),
                ]);
            }
        }

        return $BinanceOrder->id;
    }

}
