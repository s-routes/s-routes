<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\SendLetter;
use common\models\UserSeed;
use common\models\UserZalog;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use function Sodium\add;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;

/**
 */
class CabinetBillsTokenZalog extends Model
{
    /** @var  string */
    public $password;

    /** @var  int */
    public $billing_id;

    /** @var \common\models\avatar\UserBill  */
    private $billing;

    /** @var  int */
    public $days;

    /** @var string */
    public $email;

    /** @var double */
    public $amount;

    /** @var  \common\models\UserAvatar */
    private $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['days', 'required', 'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению')],
            ['days', 'trim'],
            ['days', 'integer', 'min' => 1, 'max' => 100],

            ['email', 'required', 'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению')],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'validateEmail'],

            ['billing_id', 'required', 'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению')],
            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],

            ['password', 'required', 'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению')],
            ['password', 'string'],
            ['password', 'validatePassword'],

            ['amount', 'required', 'message' => Yii::t('c.NRHKEjgSii', 'Это поле обязательно к заполнению')],
            ['amount', 'trim'],
            ['amount', 'double', 'min' => 0],
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findByUsername($this->email);
            } catch (\Exception $e) {
                $this->addError($attribute, Yii::t('c.oifM0YkVx7', 'Пользователь не найден'));
                return;
            }
            if ($user->mark_deleted) {
                $this->addError($attribute, Yii::t('c.oifM0YkVx7', 'Пользователь удален'));
                return;
            }
            if ($user->id == Yii::$app->user->id) {
                $this->addError($attribute, Yii::t('c.oifM0YkVx7', 'Нельзя использовать свой email'));
                return;
            }
            $this->user = $user;
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {

            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError($attribute, Yii::t('c.oifM0YkVx7', 'Не найден счет'));
                return;
            }
            if ($billing->mark_deleted) {
                $this->addError($attribute, Yii::t('c.oifM0YkVx7', 'Счет удален'));
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError($attribute, Yii::t('c.oifM0YkVx7', 'Это не ваш счет'));
                return;
            }
            $this->billing = $billing;
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]
     */
    public function convert($params)
    {
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => strtolower($name),
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     * Создает залог
     *
     * @return \common\models\UserZalog
     */
    public function action()
    {
        $currency = $this->billing->getCurrencyObject();
        $billingPassword = $this->billing->getPassword($this->password);
        $key = Security::generateRandomString();
        $hash = UserBill::passwordEnСript($billingPassword, $key);
        $zalog = UserZalog::add([
            'owner_user_id'      => $this->billing->user_id,
            'owner_billing_id'   => $this->billing_id,
            'partner_billing_id' => null,
            'partner_user_id'    => $this->user->id,
            'finish'             => time() + (60 * 60 * 24 * $this->days),
            'amount'             => pow(10, $currency->decimals) * $this->amount,
            'hash'               => $hash,
            'status'             => 0,
        ]);
        $ret = \avatar\base\Application::mail($this->email, 'Открыта сделка залога #' . $zalog->id,'zalog',[
            'zalog' => $zalog,
            'key'   => $key,
        ]);
        Yii::info(VarDumper::dumpAsString([$ret, $zalog, $key]), 'avatar\models\forms\CabinetBillsTokenZalog::action');

        return $zalog;
    }

}
