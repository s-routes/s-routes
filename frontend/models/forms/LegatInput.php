<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class LegatInput extends Model
{
    /** @var string */
    public $account;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['account', 'string'],
            ['account', 'trim'],
            ['account', 'validateAccount'],

        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->account, '0x')) {
                $this->addError($a, 'Кошелек должен начинаться с 0x');
                return;
            }
        }
    }


    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'account' => 'Адрес кошелька призм с которого производилась транзакция',
        ];
    }

    /**
     * @return \common\models\TaskLegatInput
     */
    public function action()
    {
        // Закрываю все старые задачи
        \common\models\TaskLegatInput::updateAll(['status' => 2], ['user_id' => Yii::$app->user->id, 'status' => 0]);

        // Создаю новую задачу
        $input = \common\models\TaskLegatInput::add([
            'account' => strtolower($this->account),
            'user_id' => Yii::$app->user->id,
        ]);

        return $input;
    }
}
