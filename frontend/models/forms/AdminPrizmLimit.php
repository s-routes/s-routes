<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 */
class AdminPrizmLimit extends Model
{
    /**
     * @var int
     */
    public $param;

    public function init()
    {
        $params = Config::get('pzm_exchange_limit');
        if ($params === false) $params = 0;
        $params = \common\models\piramida\Currency::getValueFromAtom($params, \common\models\piramida\Currency::PZM);
        $this->param = $params;
    }

    public function rules()
    {
        return [
            ['param', 'required'],
            ['param', 'integer'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $p = \common\models\piramida\Currency::getAtomFromValue($this->param, \common\models\piramida\Currency::PZM);
        Config::set('pzm_exchange_limit', $p);

        return 1;
    }
}
