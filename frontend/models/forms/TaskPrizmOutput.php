<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

/**
 * @property integer id
 * @property string account
 * @property string txid
 * @property integer status         Статус задачи 0 - задача создана. 1 - задача выполнена, транзакция указана
 * @property integer user_id
 * @property integer created_at
 * @property integer amount
 * @property integer finished_at
 * @property integer txid_internal
 * @property integer billing_id    Идентификатор валюты для вывода, db.currency.id, может быть только SB или SBP
 *
 * @package common\models
 */
class TaskPrizmOutput extends ActiveRecord
{
    public static function tableName()
    {
        return 'task_prizm_output';
    }

    public function rules()
    {
        return [
            ['account', 'required'],
            ['account', 'string'],
            ['account', 'validateAccount'],

            ['billing_id', 'required'],
            ['billing_id', 'integer'],

            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'account'     => 'Кошелек',
            'billing_id'  => 'Счет',
            'amount'      => 'Кол-во монет',
        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->account, 'PRIZM-')) {
                $this->addError($a, 'Кошелек должен начинаться с PRIZM-');
                return;
            }
        }
    }

    public function validateAmount($a,$p)
    {
        if (!$this->hasErrors()) {
            $billing = UserBill::findOne($this->billing_id);
            $wallet = Wallet::findOne($billing->address);

            if ($wallet->amount < $this->amount * 100) {
                $this->addError($a, 'На выбранном кошельке недостаточно монет');
                return;
            }
        }
    }


}