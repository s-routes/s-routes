<?php

namespace avatar\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ActiveInput extends Model
{
    /** @var string */
    public $account;
    public $cid;

    public $currencyID;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['cid', 'required'],
            ['cid', 'integer'],

            ['account', 'string'],
            ['account', 'trim'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'account' => Yii::t('c.TTaRzGgiYr', 'Адрес кошелька с которого будет производиться транзакция'),
        ];
    }

    /**
     * @return \common\models\TaskInput
     */
    public function action()
    {
        // Закрываю все старые задачи
        \common\models\TaskInput::updateAll(['status' => 2], [
            'user_id'        => Yii::$app->user->id,
            'status'         => 0,
            'currency_io_id' => $this->cid,
            'type_id'        => \common\models\TaskInput::TYPE_ACCOUNT,
        ]);

        $CurrencyIO = CurrencyIO::findOne($this->cid);
        $UserWallet = \common\models\UserWallet::findOne(['user_id' => Yii::$app->user->id]);
        $attribute = $CurrencyIO->field_name_wallet;

        $this->account = $UserWallet->$attribute;

        // Создаю новую задачу
        $input = \common\models\TaskInput::add([
            'account'        => $this->account,
            'user_id'        => Yii::$app->user->id,
            'status'         => 0,
            'currency_io_id' => $this->cid,
            'type_id'        => \common\models\TaskInput::TYPE_ACCOUNT,
        ]);

        return $input;
    }
}
