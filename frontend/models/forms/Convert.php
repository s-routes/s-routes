<?php
namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\ReferalTransaction;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 * COIN > NEIRO
 */
class Convert extends Model
{
    /** @var  \common\models\avatar\Currency */
    public $currency;

    public $amount;

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'validate12Usd'],
            ['amount', 'validatePrizm'],
        ];
    }

    public function validate12Usd($a, $v)
    {
        if (!$this->hasErrors()) {
            $priceUsd = $this->currency->price_usd;
            $p = $this->amount * $priceUsd;
            if ($p < 12) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT. Сейчас у вас заказ ={sum} USDТ', ['sum' => Yii::$app->formatter->asDecimal($p, 2)]));
                return;
            }
        }
    }

    public function validateAmount($a, $v)
    {
        if (!$this->hasErrors()) {
            $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

            // Забираю монеты
            $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletFrom = $dataTo['wallet'];
            $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
            $currencyFromInt = Currency::findOne($CIOfrom->currency_int_id);
            $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));

            if ($amountFrom > $walletFrom->amount) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }

    public function validatePrizm($a, $v)
    {
        if (!$this->hasErrors()) {
            if ($this->currency->id == \common\models\avatar\Currency::PZM) {
                $c = \common\models\Config::get('pzm_exchange_limit');
                $v = \common\models\piramida\Currency::getAtomFromValue($this->amount, \common\models\piramida\Currency::PZM);
                if ($v > $c) {
                    $this->addError($a, 'Введенное Вами количество монет превышает общий доступный для конвертации объем актива.');
                    return;
                }
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

        $operetion = ConvertOperation::add([
            'wallet_from_id'   => 1,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move($walletTo, $amountFrom, Json::encode([
            'Конвертация {from} в NEIRO id={id}',
            [
                'from' => $this->currency->code,
                'id'   => $operetion->id,
            ],
        ]));
        $fromCurrencyExt = $this->currency;

        // Отдаю монеты
        $cInt_to = Currency::findOne(Currency::NEIRO);
        $CIO = CurrencyIO::findOne(['currency_int_id' => $cInt_to->id]);
        $CIO_to = $CIO;
        $toCurrencyExt = \common\models\avatar\Currency::findOne($CIO->currency_ext_id);
        $walletOut = Wallet::findOne($CIO->main_wallet);
        $dataTo = UserBill::getInternalCurrencyWallet($cInt_to->id, Yii::$app->user->id);

        // получаю коммию системы
        $dataJson = Config::get('NeironConvertParams');
        $data = Json::decode($dataJson);
        $o = $this->getId($data, $CIOfrom->currency_ext_id);

        // получаю комиссию системы
        $kurs = $fromCurrencyExt->price_usd * (1-($o['in'] / 100)) / $toCurrencyExt->price_usd;

        $amount_m = $this->amount * $kurs;
        $amount_m_to = $amount_m;
        $amount = bcmul($amount_m_to, bcpow(10, $cInt_to->decimals));
        $amountTo = $amount;

        // тип транзакции
        $type = \common\models\NeironTransaction::TYPE_CONVERT_GOOD;

        /** @var \common\models\piramida\Wallet $walletTo */
        $walletTo = $dataTo['wallet'];
        $t_to = $walletOut->move2(
            $walletTo,
            $amountTo,
            Json::encode([
                'Конвертация {from} в NEIRO id={id}',
                [
                    'from' => $this->currency->code,
                    'id'   => $operetion->id,
                ],
            ]),
            $type
        );

        // Делаю запись о типе транзакции если это NERON
        NeironTransaction::add([
            'transaction_id' => $t_to['transaction']['id'],
            'operation_id'   => $t_to['operation_add']['id'],
            'type_id'        => $type,
        ]);

        // Сохраняю данные в БД
        $operetion->user_id = Yii::$app->user->id;
        $operetion->wallet_from_id = $walletFrom->id;
        $operetion->wallet_to_id = $walletTo->id;
        $operetion->tx_from_id = $t_from->id;
        $operetion->tx_to_id = $t_to['transaction']['id'];
        $operetion->kurs = (int) ($kurs * 1000000);
        $operetion->amount = $amountFrom;
        $operetion->amount_to = $amountTo;
        $operetion->from_currency_id = $CIOfrom->currency_int_id;
        $operetion->to_currency_id = $CIO->currency_int_id;

        $operetion->from_price_usd = $fromCurrencyExt->price_usd;
        $operetion->to_price_usd = $toCurrencyExt->price_usd;

        $operetion->save();

        // вычитаю лимит
        if ($this->currency->id == \common\models\avatar\Currency::PZM) {
            $c = \common\models\Config::get('pzm_exchange_limit');
            $v = \common\models\piramida\Currency::getAtomFromValue($this->amount, \common\models\piramida\Currency::PZM);
            $c = $c - $v;
            \common\models\Config::set('pzm_exchange_limit', $c);
        }

        // Делаю реферальные начисления
        $this->referalProgram(Yii::$app->user->id, $operetion);

        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            try {
                $user = \common\models\UserAvatar::findOne($uid);
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

                    try {
                        $userBill = \common\models\avatar\UserBill::findOne(['address' => $operetion->wallet_from_id]);
                    } catch (\Exception $e) {
                        return '';
                    }
                    $taskUser = \common\models\UserAvatar::findOne($userBill->user_id);

                    $from = Yii::$app->formatter->asDecimal($this->amount, $currencyFromInt->decimals) . ' ' . $currencyFromInt->code;
                    $currencyToInt = Currency::findOne($CIO_to->currency_int_id);
                    $to = Yii::$app->formatter->asDecimal($amount_m_to, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'User-ID {user}. Проведена конвертация {from} в {to}', ['user' => $taskUser->getAddress(), 'from' => $from, 'to' => $to], $language)]);
                }
            } catch (\Exception $e) {

            }
        }

        // Уведомляю клиента по телеграму
        $user = UserAvatar::findOne(Yii::$app->user->id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

            $from = Yii::$app->formatter->asDecimal($this->amount, $currencyFromInt->decimals) . ' ' . $currencyFromInt->code;
            $currencyToInt = Currency::findOne($CIO_to->currency_int_id);
            $to = Yii::$app->formatter->asDecimal($amount_m_to, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Проведена конвертация {from} в {to}', ['from' => $from, 'to' => $to], $language)]);
        }

        return $operetion->id;
    }

    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'  => $id,
            'in'  => 0,
            'out' => 0,
        ];
    }


    /**
     * @param int $user_id
     * @param \common\models\ConvertOperation $convert
     */
    function referalProgram($user_id, $convert)
    {
        $cio = CurrencyIO::findOne(['currency_int_id' => Currency::NEIRO]);
        $walletOut = Wallet::findOne($cio->main_wallet);

        /** @var \common\models\UserPartner $partner */
        $partner = \common\models\UserPartner::find()->where(['school_id' => 1, 'user_id' => $user_id])->one();
        $c = 0;
        $level = [
            0 => 5,
            1 => 3,
            2 => 1,
            3 => 3,
            4 => 5,
        ];
        while ($c < 5) {
            if (is_null($partner)) return;
            $uid = $partner->parent_id;
            $data = UserBill::getInternalCurrencyWallet(Currency::NEIRO, $uid);

            // Если у пользователя больше 1000 монет на счету то делаю рефералку
            if ($data['wallet']['amount'] > Currency::getAtomFromValue(1000, Currency::NEIRO)) {
                $amountRef = (int) ($convert->amount_to * ($level[$c] / 100));

                // Делаю транзакцию пользователю
                $t = $walletOut->move2(
                    $data['wallet'],
                    $amountRef,
                    Json::encode([
                        'Реферальные начисления за конвертацию = {cid} от uid={from} к uid={to}',
                        [
                            'cid'  => $convert->id,
                            'from' => $user_id,
                            'to'   => $uid,
                        ]
                    ]),
                    NeironTransaction::TYPE_REFERAL_CONVERT
                );

                // Делаю запись о типе транзакции если это NERON
                NeironTransaction::add([
                    'transaction_id' => $t['transaction']['id'],
                    'operation_id'   => $t['operation_add']['id'],
                    'type_id'        => NeironTransaction::TYPE_REFERAL_CONVERT,
                ]);

                // Сделать запись о реферальном начислении
                ReferalTransaction::add([
                    'request_id'     => $convert->id,
                    'type_id'        => ReferalTransaction::TYPE_CONVERT,
                    'currency_id'    => Currency::NEIRO,
                    'level'          => $c + 1,
                    'from_uid'       => $user_id,
                    'to_uid'         => $uid,
                    'to_wid'         => $data['wallet']['id'],
                    'amount'         => $amountRef,
                    'transaction_id' => $t['transaction']['id'],
                ]);
            }

            $partner = \common\models\UserPartner::find()->where(['school_id' => 1, 'user_id' => $partner['parent_id']])->one();

            $c++;
        }
    }
}
