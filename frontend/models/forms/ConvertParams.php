<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class ConvertParams extends Model
{
    public $params;

    public function rules()
    {
        return [
            ['params', 'required'],
            ['params', 'string'],
        ];
    }

    public function action()
    {

    }
}
