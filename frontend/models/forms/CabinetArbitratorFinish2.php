<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class CabinetArbitratorFinish2 extends Model
{
    /** @var int deal.id */
    public $id;

    /** @var string */
    public $code;

    /** @var \common\models\exchange\Deal */
    public $deal;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateDeal'],

            ['code', 'required'],
            ['code', 'string', 'length' => 6],
            ['code', 'validateDeal2'],
            ['code', 'validateCode'],
        ];
    }

    public function validateDeal($a, $p)
    {
        if (!$this->hasErrors()) {
            $Deal = Deal::findOne($this->id);
            if (is_null($Deal)) {
                $this->addError($a, 'Не найден Deal ' . $this->id);
                return true;
            }
            $this->deal = $Deal;
        }
        return true;
    }

    public function validateDeal2($a, $p)
    {
        if (!$this->hasErrors()) {
            $Deal = $this->deal;

            if ($Deal->status == Deal::STATUS_AUDIT_FINISH) {
                $this->addError($a, 'Статус уже установлен');
                return true;
            }
        }
        return true;
    }

    /**
     * @param $a
     * @param $p
     * @return bool
     */
    public function validateCode($a, $p)
    {
        if (!$this->hasErrors()) {
            $user = UserAvatar::findOne(Yii::$app->user->id);

            if (Application::isEmpty($user->google_auth_code)) {
                $this->addError('code', 'Код 2FA не установлен');
                return true;
            }
            if (!$user->validateGoogleAuthCode($this->code)) {
                $this->addError('code', 'Код не верный');
                return true;
            }
        }
        return true;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $Deal = $this->deal;

        $Deal->trigger(Deal::EVENT_BEFORE_AUDIT_FINISH);

        $t = Yii::$app->db->beginTransaction();

        try {
            $Deal->status = Deal::STATUS_AUDIT_FINISH;
            $ret = $Deal->save();
            if (!$ret) throw new \Exception('Не сохранился статус');
            DealStatus::add(['status' => Deal::STATUS_AUDIT_FINISH, 'deal_id' => $Deal->id]);

            // Разблокирую монеты в пользу пользователя 2
            {
                // Ищу кошелек
                $offer = $Deal->getOffer();
                $IO = CurrencyIO::findOne($offer->currency_io);
                $data = UserBill::getInternalCurrencyWallet($IO->currency_int_id, $offer->user_id);
                $wallet = $data['wallet'];
                $walletEscrow = Wallet::findOne($IO->block_wallet);

                // делаю перевод для разблокировки
                $text = 'Арбитр ' . Yii::$app->user->id . ' разблокировал средства от сделки ' . $Deal->id . ' в пользу пользователя ' . $offer->user_id . ' в кошелек ' . $wallet->id;
                $transactionUnBlock = $walletEscrow->move($wallet, $Deal->volume_vvb,
                    Json::encode([
                        'Арбитр {arbitr} разблокировал средства от сделки {deal} в пользу пользователя {user} в кошелек {wallet}',
                        [
                            'arbitr' => Yii::$app->user->id,
                            'deal'   => $Deal->id,
                            'user'   => $offer->user_id,
                            'wallet' => $wallet->id,
                        ],
                    ])
                );

                // Записываю информацию о принятии решения арбитром
                $arbitration = Arbitrator::findOne(['deal_id' => $Deal->id]);
                $arbitration->decision_user_id = $offer->user_id;
                $arbitration->finish_at = time();
                $arbitration->unblock_transaction_id = $transactionUnBlock->id;
                $arbitration->save();

                // Отправляю письма обоим участникам сделки
                $u1 = UserAvatar::findOne($Deal->user_id);
                $u2 = UserAvatar::findOne($offer->user_id);
                \common\services\Subscribe::sendArray(
                    [
                        $u1->email,
                        $u2->email,
                    ],
                    'Арбитр принял решение о сделке',
                    'arbitr_accept',
                    [
                        'text' => $text
                    ]
                );

                // отправка уведомления для партнеров через телеграм
                {
                    $t1 = 'Арбитр принял решение о сделке #' . $Deal->id . ' решение принято в пользу пользователя ' . $u2->getAddress();
                    if (!Application::isEmpty($u1->telegram_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegram;
                        $telegram->sendMessage(['chat_id' => $u1->telegram_chat_id, 'text' => $t1]);
                    }
                    if (!Application::isEmpty($u2->telegram_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegram;
                        $telegram->sendMessage(['chat_id' => $u2->telegram_chat_id, 'text' => $t1]);
                    }
                }
            }

            // Завершаю сделку (вычитает из предложения объем сделки)
            $Deal->finish();

            $t->commit();

        } catch (\Exception $e) {

            $t->rollBack();
            throw $e;

        }

        $Deal->trigger(Deal::EVENT_AFTER_AUDIT_FINISH);

        return true;
    }
}