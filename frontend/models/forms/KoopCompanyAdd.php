<?php
namespace avatar\models\forms;

use common\widgets\FileUpload4\ModelFields;
use cs\base\FormActiveRecord;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * @property integer id
 */
class KoopCompanyAdd extends Model
{
    public $id;
    public $koop_id;

    public function rules()
    {
        return [['id', 'integer']];
    }

    /**
     * @param int $id идентификатор кооператива
     * @return int
     */
    public function action($id)
    {
        $m = new \common\models\koop\LinkCompanyKooperative([
            'koop_id'    => $id,
            'company_id' => $this->id,
        ]);
        $m->save();
    }
}
