<?php

namespace avatar\models\forms;

use app\models\User;
use common\components\Card;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\UserAvatar;
use cs\Application;
use cs\web\Exception;
use Endroid\QrCode\QrCode;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\Url;

/**
 *
 */
class QrEnter extends Model
{
    /** @var string email */
    public $email;

    /** @var string пароль1 */
    public $password1;

    /** @var string пароль2 */
    public $password2;

    /** @var string номер карты */
    public $code;

    /** @var  \common\models\Card */
    public $card;

    /** @var  \common\models\avatar\QrCode */
    private $codeObject;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['code', 'email'], 'required', 'message' => Yii::t('c.RGQ3VcUnI0', 'Это поле должно быть заполнено обязательно')],
            ['email', 'trim'],
            ['email', 'email', 'checkDNS' => true, 'message' => Yii::t('c.RGQ3VcUnI0', 'Email должен быть верным')],
            ['email', 'unique', 'targetAttribute' => 'email', 'targetClass' => '\common\models\UserAvatar', 'message' => Yii::t('c.RGQ3VcUnI0', 'Такая почта уже есть')],

            ['code', 'required', 'message' => Yii::t('c.RGQ3VcUnI0', 'Это поле должно быть заполнено обязательно')],
            ['code', 'string', 'max' => 20, 'min' => 16],
            ['code', 'normalize'],
            ['code', 'validateCardNumber'],
            ['code', 'joinBilling'],
        ];
    }

    /**
     * Проверяет номер карты на валидность
     * Как входное значене уже приходит номер без пробелов
     *
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $card_number = $this->code;
            if (strlen($card_number) == 16) {
                if (preg_match('/\D/',$card_number) != 0) {
                    $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'В номере карты есть запрещенные символы'));
                    return;
                }
                if (!Card::checkSum($card_number)) {
                    $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Контрольная сумма в номере карты не совпадает'));
                    return;
                }
            } else {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'В номере карты должно быть 16 цифр'));
                return;
            }
        }
    }

    public function joinBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $card = \common\models\Card::findOne(['number' => $this->code]);
            } catch (\Exception $e) {
                $this->addError($attribute, Yii::t('c.RGQ3VcUnI0', 'Карта не найдена'));
                return;
            }
            $this->card = $card;

            // проверка на активированность карты
            $codeObject = \common\models\avatar\QrCode::findOne(['card_id' => $card->id]);
            if (is_null($codeObject)) {
                $this->addError($attribute, 'Не найден объект кода');
                return;
            }
            if ($codeObject->is_used) {
                $this->addError($attribute, 'Карта уже активирована');
                return;
            }
            $this->codeObject = $codeObject;
        }
    }

    public function normalize($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->code = str_replace(' ', '', $this->code);
        }
    }

    /**
     * Активирует карту
     *
     */
    public function activate()
    {
        // Создаю пользователя
        $data = UserAvatar::registration($this->email, $this->password1);

        /** @var \common\models\UserAvatar $user */
        $user = $data['user'];

        $codeObject = $this->codeObject;
        $codeObject->is_used = 1;
        $codeObject->save();

        // устанавливаю счета по умолчанию
        try {
            $btc = UserBill::findOne(['card_id' => $this->card->id, 'currency' => \common\models\avatar\Currency::BTC]);
            $d = new UserBillDefault([
                'id'          => $btc->id,
                'user_id'     => $user->id,
                'currency_id' => \common\models\avatar\Currency::BTC,
            ]);
            $d->save();
        } catch (\Exception $e) {

        }
        try {
            $eth = UserBill::findOne(['card_id' => $this->card->id, 'currency' => \common\models\avatar\Currency::ETH]);
            $d = new UserBillDefault([
                'id'          => $eth->id,
                'user_id'     => $user->id,
                'currency_id' => \common\models\avatar\Currency::BTC,
            ]);
            $d->save();
        } catch (\Exception $e) {

        }

        $billingList = UserBill::find()->where(['card_id' => $this->card->id])->all();
        /** @var \common\models\avatar\UserBill $billing */
        foreach ($billingList as $billing) {
            $billing->user_id = $user->id;
            $billing->save();
        }

        return true;
    }
}
