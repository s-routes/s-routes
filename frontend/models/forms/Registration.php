<?php

namespace avatar\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;


/**
 * Registration is the model behind the contact form.
 */
class Registration extends Model
{
    public $name_first;
    public $email;
    public $password1;
    public $password2;
    public $is_rules;
    public $verificationCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name_first', 'required'],
            ['name_first', 'string'],

            ['email', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['email', 'trim'],
            ['email', 'strtolower'],
//            ['email', 'email', 'checkDNS' => true, 'message' => Yii::t('c.0uMdJb0e0n', 'Email должен быть с корректным доменным именем')],
            ['email', 'validateEmail'],

            ['password1', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],

            ['password2', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],

            ['is_rules', 'integer'],
            ['is_rules', 'validateRules', 'skipOnEmpty' => false],

            ['verificationCode', '\cs\Widget\Google\reCaptcha\Validator', 'skipOnEmpty' => false],
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => ['email', 'password1', 'password2', 'name_first', 'is_rules', 'verificationCode'],
            'ajax'   => ['email', 'password1', 'password2', 'name_first', 'is_rules'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verificationCode' => Yii::t('c.0uMdJb0e0n', 'Проверочный код'),
        ];
    }

    public function validateEmail($attribute, $params)
    {
        if (UserAvatar::find()->where(['email' => $this->email])->exists()) {
            $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Такой пользователь уже есть'));
            return;
        }
    }

    public function strtolower($attribute, $params)
    {
        $this->email = strtolower($this->email);
    }

    public function validateRules($attribute, $params)
    {
        if (Application::isEmpty($this->is_rules)) {
            $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Условия должны быть приняты обязательно'));
            return;
        } else {
            if ($this->is_rules != 1) {
                $this->addError($attribute, Yii::t('c.0uMdJb0e0n', 'Условия должны быть приняты обязательно'));
                return;
            }
        }
    }

    /**
     * @return boolean whether the model passes validation
     */
    public function register()
    {
        $data = \common\models\UserAvatar::registration($this->email, $this->password1, ['name_first' => $this->name_first]);

        return true;
    }
}
