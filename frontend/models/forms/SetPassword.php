<?php

namespace avatar\models\forms;

use app\models\Piramida\Piramida;
use app\models\Piramida\Wallet;
use app\models\User;
use common\models\avatar\UserBill;
use common\models\UserAvatar;
use common\models\UserTelegramTemp;
use cs\Application;
use Yii;
use yii\base\Model;
use yii\db\Query;


/**
 * Registration is the model behind the contact form.
 */
class SetPassword extends Model
{
    public $password1;
    public $password2;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password1', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],
        ];
    }

    /**
     * @param \common\models\UserTelegramConnect $registration
     * @param int                                $timeOut
     *
     * @return UserAvatar
     * @throws
     */
    public function action($registration, $timeOut)
    {
        $user = UserAvatar::add([
            'email'              => $registration->email,
            'password'           => UserAvatar::hashPassword($this->password1),
            'registered_ad'      => time(),
            'email_is_confirm'   => 1,
            'auth_key'           => \Yii::$app->security->generateRandomString(60),
            'telegram_username'  => $registration->username,
            'telegram_chat_id'   => $registration->chat_id,
            'language'           => $registration->language,
        ]);

        // удалаю \common\models\UserTelegramConnect
        $registration->delete();

        // удалаю \common\models\UserTelegramTemp
        UserTelegramTemp::deleteAll(['username' => $user->telegram_username]);

        // Устанавливаю язык для бота
        $language = \avatar\controllers\TelegramController::getOptimalLanguage($user);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;
        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.pucsfXhpLK', 'Ура! Получилось, мы соединились с тобой!', [], $language)]);

        // авторизую пользователя
        Yii::$app->user->login($user, $timeOut);

        return $user;
    }
}
