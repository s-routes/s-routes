<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class CountryLang extends ActiveRecord
{

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['parent_id', 'required'],
            ['parent_id', 'integer'],

            ['language', 'required'],
            ['language', 'string'],
        ];
    }

}
