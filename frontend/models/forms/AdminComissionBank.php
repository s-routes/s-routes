<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 */
class AdminComissionBank extends Model
{
    /**
     * @var int
     */
    public $param;

    public function init()
    {
        $params = Config::get('ComissionBank');
        if ($params === false) $params = 0;

        $this->param = $params;
    }

    public function rules()
    {
        return [
            ['param', 'required'],
            ['param', 'integer'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Config::set('ComissionBank', $this->param);

        return 1;
    }
}
