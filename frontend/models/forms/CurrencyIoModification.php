<?php

namespace frontend\models\forms;

use common\models\piramida\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    currency_io_id
 * @property integer    output_min
 * @property double     comission_out
 * @property string     master_wallet
 * @property string     name
 * @property string     wallet_field_name
 * @property string     function_check
 */
class CurrencyIoModification extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_io_modification';
    }

    public function rules()
    {
        return [
            ['output_min', 'integer'],
            ['comission_out', 'double'],
            ['master_wallet', 'string'],
            ['name', 'string'],
            ['extended_info', 'string'],
            ['master_wallet_is_binance', 'integer'],
        ];
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }
}
