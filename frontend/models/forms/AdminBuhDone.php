<?php

namespace avatar\models\forms;

use avatar\models\Wallet;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\SendLetter;
use common\models\TaskOutputHistory;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminBuhDone extends Model
{
    /** @var  string */
    public $txid;

    /** @var  string */
    public $code;

    /** @var  double */
    public $comission;

    /** @var \common\models\TaskOutput */
    public $task;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['txid', 'required'],
            ['txid', 'string'],

            ['comission', 'double'],

            ['code', 'string', 'length' => 6],
            ['code', 'validateCode'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'txid'      => 'Идентификатор транзакции',
            'code'      => 'Код 2FA',
            'comission' => 'Комиссия',
        ];
    }

    /**
     * @param $a
     * @param $p
     * @return bool
     */
    public function validateCode($a, $p)
    {
        if (!$this->hasErrors()) {
            $user = UserAvatar::findOne(Yii::$app->user->id);

            if (Application::isEmpty($user->google_auth_code)) {
                $this->addError('code', 'Код 2FA не установлен');
                return true;
            }
            if (!$user->validateGoogleAuthCode($this->code)) {
                $this->addError('code', 'Код не верный');
                return true;
            }
        }
        return true;
    }

    /**
     */
    public function action()
    {
        if (!Application::isEmpty($this->comission)) {
            if (!Application::isEmpty($this->task->comission_user)) {
                $cio = \common\models\CurrencyIO::findOne($this->task->currency_io_id);
                $c = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                if ($c->decimals > 0) {
                    $comission_buh = bcmul($this->comission, bcpow(10, $c->decimals));
                } else {
                    $comission_buh = $this->comission;
                }

                // Если бухгалтер указал комиссию больше чем обычно
                if ($comission_buh > $this->task->comission_user) {
                    // то сохраняю
                    $this->task->comission = $comission_buh;
                    $this->task->save();
                }
            }
        }

        // забираю монеты
        $cio = CurrencyIO::findOne($this->task->currency_io_id);
        $wallet = \common\models\piramida\Wallet::findOne($cio->block_wallet);

        $walletToId = $cio->main_wallet;
        $walletTo = \common\models\piramida\Wallet::findOne($walletToId);
        $t = $wallet->move2(
            $walletTo,
            $this->task->amount, Json::encode([
            Yii::t('c.GGWWLBoXB4', 'Вывод средств по заявке id={id}'),
                ['id' => $this->task->id],
                ['type_id' => 1],
            ]),
            NeironTransaction::TYPE_OUT
        );

        // Устанавливаю статус заявки
        $this->task->txid = $this->txid;
        $this->task->txid_internal = $t['transaction']['id'];
        $this->task->finished_at = time();
        $this->task->status = \common\models\TaskPrizmOutput::STATUS_DONE;
        $this->task->save();

        // Делаю запись о типе транзакции если это NEIRON
        if ($wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
            NeironTransaction::add([
                'transaction_id' => $t['transaction']['id'],
                'operation_id'   => $t['operation_sub']['id'],
                'type_id'        => NeironTransaction::TYPE_OUT,
            ]);
        }

        // Отправляю письмо уведомление клиенту
        /** @var UserAvatar $user */
        $user = UserAvatar::findOne($this->task->user_id);
        \common\services\Subscribe::sendArray([$user->email], 'Заявка на вывод #' . $this->task->id . ' выполнена','task_out_done', [
            'task' => $this->task,
        ]);

        // Добавляю историю заявки
        $i = TaskOutputHistory::add([
            'task_id' => $this->task->id,
            'comment' => 'Заявка на вывод #' . $this->task->id . ' выполнена txid=' . $this->txid,
        ]);

        // Уведомляю пользователя по телеграму
        $user = UserAvatar::findOne($this->task->user_id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => 'Заявка на вывод #' . $this->task->id . ' выполнена']);
        }
    }
}
