<?php

namespace avatar\models\forms\exchange;

use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 *
 */
class DealOpen extends Model
{
    public $volume_io;

    /** @var  Offer */
    public $offer;

    /** @var Deal */
    public $deal;

    public function rules()
    {
        return [
            ['volume_io', 'required'],
            ['volume_io', 'validateVolume'], // При открытии сделки нужно проверить что покупаемый объем больше чем указано в предложении
            ['volume_io', 'validateBuyExist'], // При открытии сделки нужно проверить что если предложение == Offer::TYPE_BUY то у открывающего сделку должны быть на счету монеты
        ];
    }

    /**
     * При открытии сделки нужно проверить что покупаемый объем больше чем указано в предложении
     *
     * @param $attribute
     * @param $params
     */
    public function validateVolume($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $offer = $this->offer;
            $cio = CurrencyIO::findOne($offer->currency_io);
            $currency = \common\models\piramida\Currency::findOne($cio->currency_int_id);

            $v = bcmul(bcpow(10, $currency->decimals), $this->volume_io);
            if ($offer->volume_io_start != $offer->volume_io_finish) {
                if ($v < $offer->volume_io_start) {
                    $this->addError('volume_io', Yii::t('c.hD8QKgnMrG', 'объем сделки должен превышать Минимального объем предложения'));
                    return;
                }
                if ($v > $offer->volume_io_finish) {
                    $this->addError('volume_io', Yii::t('c.hD8QKgnMrG', 'объем сделки должен быть меньше Максимального объема предложения'));
                    return;
                }
                if ($v > $offer->volume_io_finish_deal) {
                    $this->addError('volume_io', Yii::t('c.hD8QKgnMrG', 'объем сделки должен быть меньше Максимального объема предложения доступного на данный момент'));
                    return;
                }
            } else {
                if ($offer->volume_io_finish != $v) {
                    $this->addError('volume_io', Yii::t('c.hD8QKgnMrG', 'объем сделки должен быть равен объему предложения'));
                    return;
                }
            }
        }
    }

    /**
     * При открытии сделки нужно проверить что если предложение == Offer::TYPE_BUY то у открывающего сделку должны быть на счету монеты
     *
     * @param $attribute
     * @param $params
     */
    public function validateBuyExist($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $offer = $this->offer;
            if ($offer->type_id == Offer::TYPE_ID_BUY) {

                $cid = CurrencyIO::findOne($offer->currency_io);
                $data = UserBill::getInternalCurrencyWallet($cid->currency_int_id, Yii::$app->user->id);

                $currency = \common\models\piramida\Currency::findOne($cid->currency_int_id);
                $v = bcmul(bcpow(10, $currency->decimals), $this->volume_io);

                /** @var \common\models\piramida\Wallet $w */
                $w = $data['wallet'];
                // $w->amount < $v
                if (bccomp($w->amount, $v) == -1) {
                    $this->addError('volume_io', Yii::t('c.hD8QKgnMrG', 'Вы не можете начать сделку потому что у вас недостаточно монет на счету'));
                    return;
                }
                $Deal = $this->deal;
                if (bccomp($this->offer->volume_io_finish_deal, $Deal->volume_vvb) == -1) {
                    $this->addError('volume_io', Yii::t('c.hD8QKgnMrG', 'В предложении недостаточно монет'));
                    return;
                }
            }
        }
    }

    public function attributeHints()
    {
        return [];
    }

    /**
     * @return Deal
     */
    public function action()
    {
        $currencyIO = CurrencyIO::findOne($this->offer->currency_io);
        $currencyVVB = \common\models\piramida\Currency::findOne($currencyIO->currency_int_id);

        $price_atom = $this->deal->price;
        $link = CurrencyLink::findOne(['currency_ext_id' => $this->offer->currency_id]);
        $c = \common\models\piramida\Currency::findOne($link->currency_int_id);
        $price = bcdiv($price_atom, bcpow(10, $c->decimals), $c->decimals);
        $volume = bcmul($price, $this->volume_io, $c->decimals); /** цена * объем */

        $Deal = $this->deal;
        $Deal->volume = bcmul($volume, pow(10, $c->decimals));
        $Deal->volume_vvb = bcmul($this->volume_io, pow(10, $currencyVVB->decimals));
        $Deal->status = Deal::STATUS_OPEN;

        // Если есть реферал то записываю
        if (Yii::$app->session->has('ReferalProgram')) {
            $Deal->partner_id = Yii::$app->session->get('ReferalProgram')['partner_id'];
        }

        $Deal->save();

        DealStatus::add(['status' => Deal::STATUS_OPEN, 'deal_id' => $Deal->id, 'comment' => Json::encode(['Сделка открыта'])]);

        // вычитаю монеты в связи с ситуацией 20200708
        $this->offer->volume_io_finish_deal = bcsub($this->offer->volume_io_finish_deal, $Deal->volume_vvb);
        $this->offer->save();

        // отправка уведомления для $offerUser по телеграм
        $offerUser = $this->offer->getUser();
        if (!Application::isEmpty($offerUser->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $url = Url::to(['cabinet-exchange/deal-action', 'id' => $Deal->id], true);
            $language = (Application::isEmpty($offerUser->language)) ? 'ru' : $offerUser->language;
            $telegram->sendMessage(['chat_id' => $offerUser->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Открыта сделка', [], $language) . ' #' . $Deal->id . "\n" . 'Ссылка: ' . $url]);
        }

        $Deal->trigger(Deal::EVENT_AFTER_OPEN);

        return $Deal;
    }

}
