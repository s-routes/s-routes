<?php
namespace avatar\models\forms\exchange;

use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class PageLang extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_lang';
    }

    public function formAttributes()
    {
        return [
            [
                'parent_id',
                'parent_id',
                1,
                'integer',
            ],
            [
                'language',
                'language',
                1,
                'string',
                ['max' => 5],
            ],
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
        ];
    }
}
