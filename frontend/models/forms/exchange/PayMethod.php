<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace avatar\models\forms\exchange;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int        id                      идентификатор заявки
 * @property string     name
 *
 *
 */
class PayMethod extends ActiveRecord
{
    public static function tableName()
    {
        return 'pay_method';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],
        ];
    }
}