<?php

namespace avatar\models\forms\exchange;

use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Deal;
use common\models\exchange\Offer;
use common\models\exchange\OfferPay;
use common\models\exchange\OfferPayLink;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class OfferAdd extends Model
{
    /** @var Offer предложение которое взято за основу формы */
    public $offer;

    public $name;
    public $type_id;
    public $pay_id;

    /** @var  array */
    public $pay_list;

    public $currency_id;

    /** @var  Currency */
    public $currency;

    /** @var  \common\models\piramida\Currency */
    public $currency_p;

    /** @var double цена в монетах */
    public $price;
    public $price_is_free;

    public $condition;

    public $volume_start;  // атомы вычисляются из $volume_io_start
    public $volume_finish; // атомы вычисляются из $volume_io_finish

    public $volume_io_start;
    public $volume_io_finish;


    public $currency_io;

    public $offer_pay_id;

    public function init()
    {
        if (!Application::isEmpty($this->offer)) {
            $offer = $this->offer;
            $this->type_id = $offer->type_id;
            $this->currency_id = $offer->currency_id;


            $cExt = Currency::findOne($offer->currency_id);
            $cio = CurrencyIO::findOne($offer->currency_io);
            $volume_io_cInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);

            $this->price = \common\models\piramida\Currency::getValueFromAtom($offer->price, \common\models\piramida\Currency::initFromCurrencyExt($cExt));

            $this->condition = $offer->condition;

            $this->offer_pay_id = $offer->offer_pay_id;
            $this->currency_io = $offer->currency_io;

            $this->volume_start = $offer->volume_start;
            $this->volume_finish = $offer->volume_finish;
            $this->volume_io_start =  \common\models\piramida\Currency::getValueFromAtom($offer->volume_io_start, $volume_io_cInt);
            $this->volume_io_finish = \common\models\piramida\Currency::getValueFromAtom($offer->volume_io_finish, $volume_io_cInt);

            $this->pay_list = \common\models\exchange\OfferPayLink::find()->where(['offer_id' => $offer->id])->select('pay_id')->column();
        }
    }

    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],

            ['type_id', 'required'],
            ['type_id', 'integer'],
            ['type_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле обязательно для заполнения')],

            ['offer_pay_id', 'required'],
            ['offer_pay_id', 'integer'],
            ['offer_pay_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле обязательно для заполнения')],
            ['offer_pay_id', 'validateCurrency'],   // проверить что currency_id != currency_io

            ['price_is_free', 'integer'],

            ['price', 'trim'],
            ['price', 'double'],
            ['price', 'eGold', 'skipOnEmpty' => false],
            ['price', 'normalizePrice'],
            ['price', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле обязательно для заполнения')],

            ['condition', 'string'],

            ['volume_io_start', 'required'],
            ['volume_io_start', 'trim'],
            ['volume_io_start', 'double'],
            ['volume_io_start', 'compare', 'operator' => '>', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле должно быть более 0')],

            ['currency_io', 'required'],
            ['currency_io', 'integer'],

            ['pay_list', 'safe'],

            ['volume_io_finish', 'required'],
            ['volume_io_finish', 'trim'],
            ['volume_io_finish', 'double'],
            ['volume_io_finish', 'compare', 'operator' => '>', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле должно быть положительным')],
            ['volume_io_finish', 'validateEnd'],    // Объем конечный должен быть больше объема начального
            ['volume_io_finish', 'validateWallet'], // Только для продажи. Проверяет достаточно ли монет у меня на счету
        ];
    }

    /**
     * Обрезает лишние знаки $this->getCurrency()->decimals_view
     * Убирает лишние нули сзади
     *
     * @param $attribute
     * @param $params
     */
    public function normalizePrice($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $c = $this->getCurrency();
            $link = CurrencyLink::findOne(['currency_ext_id' => $c->id]);
            $c2 = \common\models\piramida\Currency::findOne($link->currency_int_id);
            $a = explode('.', $this->price);
            $v = '0';
            if (count($a) == 1) {
                $v =  $this->price;
            }
            if (count($a) == 2) {
                $v = $a[0] . '.' . substr($a[1], 0, $c2->decimals_view);
            }
            $this->price = $this->normalize($v);
        }
    }

    /**
     * Обрезает лишние знаки $this->getCurrency()->decimals_view
     * Убирает лишние нули сзади
     *
     * @param $attribute
     * @param $params
     */
    public function eGold($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->currency_io == 13) {
                $cEGOLD = Currency::findOne(['code' => 'EGOLD']);
                $this->price = $cEGOLD->kurs;
            } else {
                if (Application::isEmpty($this->price)) {
                    $this->addError($attribute, 'Это значение обязательно');
                    return;
                }
            }
        }
    }

    /**
     * Убирает лишние нули сзади
     * @param $v
     * @return mixed
     */
    public function normalize($v)
    {
        $a = explode('.', $v);
        if (count($a) == 1) {
            return $v;
        }
        $a = Str::getChars($v);
        $a = array_reverse($a);
        $c1 = 0;
        foreach ($a as $c) {
            if ($c == '0') {
                $c1++;
                continue;
            } else {
                break;
            }
        }
        if ($a[$c1] == '.') $c1++;

        $ret = [];
        for($i = $c1; $i < count($a); $i++) {
            $ret[] = $a[$i];
        }
        $ret = array_reverse($ret);

        return join('', $ret);
    }

    public function attributeLabels()
    {
        return [
            'name'             => 'Заголовок',
            'type_id'          => Yii::t('c.HWRG4KACAF', 'Тип сделки (Я хочу ...)'),
            'pay_id'           => 'Платежная система',
            'pay_list'         => Yii::t('c.HWRG4KACAF', 'Выберите условия сделки и платежные системы'),
            'price_is_free'    => 'Тип цены',
            'price'            => Yii::t('c.HWRG4KACAF', 'Цена'),
            'currency_id'      => Yii::t('c.HWRG4KACAF', 'Валюта'),
            'offer_pay_id'     => Yii::t('c.HWRG4KACAF', 'Валюта'),
            'user_id'          => 'Пользователь',
            'condition'        => Yii::t('c.HWRG4KACAF', 'Условия сделки'),
            'volume_start'     => Yii::t('c.HWRG4KACAF', 'Объем от'),
            'volume_end'       => Yii::t('c.HWRG4KACAF', 'до'),
            'volume_io_start'  => Yii::t('c.HWRG4KACAF', 'Объем от'),
            'volume_io_finish' => Yii::t('c.HWRG4KACAF', 'до'),
            'currency_io'      => Yii::t('c.HWRG4KACAF', 'Монета для обмена'),
        ];
    }

    public function validateEnd($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->volume_io_finish < $this->volume_io_start) {
                $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'Объем конечный должен быть больше объема начального'));
                return;
            }
        }
    }

    public function getCurrency()
    {
        if (is_null($this->currency)) {
            $this->currency = $this->_getCurrency();
        }

        return $this->currency;
    }

    public function _getCurrency()
    {
        return Currency::findOne($this->currency_id);
    }

    public function validateCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $cio = CurrencyIO::findOne($this->currency_io);
            $offerPay = OfferPay::findOne($this->offer_pay_id);
            $this->currency_id = $offerPay->currency_id;

            // если меняемая монета USDT
            if ($this->currency_io == 3) {
                if ($offerPay->id == 3) {
                    $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'Монета обмена не должна соответствовать валюте обмена'));
                    return;
                }
            } else {
                if ($this->currency_id == $cio->currency_ext_id) {
                    $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'Монета обмена не должна соответствовать валюте обмена'));
                    return;
                }
            }
        }
    }

    public function validateWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->currency = Currency::findOne($this->currency_id);
            $link = CurrencyLink::findOne(['currency_ext_id' => $this->currency_id]);
            $this->currency_p = \common\models\piramida\Currency::findOne($link->currency_int_id);

            if ($this->type_id == Offer::TYPE_ID_SELL) {
                $currency = Currency::findOne($this->currency_id);

                // Валюта для монеты за которую производится обмен
                $currency_io = CurrencyIO::findOne($this->currency_io);

                // получаю кошелек VVB
                $data = UserBill::getInternalCurrencyWallet($currency_io->currency_int_id);

                /** @var Wallet $wallet */
                $wallet = $data['wallet'];
                $currencyInt = \common\models\piramida\Currency::findOne($currency_io->currency_int_id);

                $volume_io_finish_atom = bcmul($this->volume_io_finish, bcpow(10, $currencyInt->decimals));
                if ($wallet->amount < $volume_io_finish_atom) {
                    $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'У вас недостаточно монет на счету'));
                    return;
                }
            }
        }
    }


    public function attributeHints()
    {
        return [
            'volume_io_start' => Yii::t('c.HWRG4KACAF', 'Объемы указываются в монетах продажи или покупки'),
            'pay_list'        => Yii::t('c.HWRG4KACAF', 'Дополнительная информация'),
        ];
    }

    /**
     * @return Offer
     */
    public function action()
    {

        // надо сконвертировать $this->volume_io_finish $this->volume_io_start в $this->volume_finish $this->volume_start
        $link_pay = CurrencyLink::findOne(['currency_ext_id' => $this->currency_id]);
        $currencyInt_pay = \common\models\piramida\Currency::findOne($link_pay->currency_int_id);

        $volume_io_start_m = $this->volume_io_start;
        $volume_start_m = $volume_io_start_m * $this->price;
        $this->volume_start = bcmul($volume_start_m, pow(10, $currencyInt_pay->decimals));

        $volume_io_finish_m = $this->volume_io_finish;
        $volume_finish_m = $volume_io_finish_m * $this->price;
        $this->volume_finish = bcmul($volume_finish_m, pow(10, $currencyInt_pay->decimals));

        // Считаю данные для avg_deal, assessment_positive, assessment_negative
        $o = Offer::findOne(['user_id' => Yii::$app->user->id]);
        if (!is_null($o)) {
            $avg_deal = $o->avg_deal;
            $assessment_positive = $o->assessment_positive;
            $assessment_negative = $o->assessment_negative;
            $deals_count = $o->deals_count;
        } else {
            $avg_deal = null;
            $assessment_positive = 0;
            $assessment_negative = 0;
            $deals_count = 0;
        }

        $currency_io = CurrencyIO::findOne($this->currency_io);
        $currencyInt = \common\models\piramida\Currency::findOne($currency_io->currency_int_id);

        $offer = new Offer();
        $offer->name = $this->name;
        $offer->avg_deal = $avg_deal;
        $offer->assessment_positive = $assessment_positive;
        $offer->assessment_negative = $assessment_negative;
        $offer->deals_count = $deals_count;
        $offer->user_id = Yii::$app->user->id;
        $offer->type_id = $this->type_id;
        $offer->price_is_free = $this->price_is_free;
        $offer->price = bcmul($this->price, bcpow(10, $this->currency_p->decimals));
        $offer->price_m = $this->price;
        $offer->currency_id = $this->currency_id;
        $offer->condition = $this->condition;
        $offer->offer_pay_id = $this->offer_pay_id;
        $offer->currency_io = $this->currency_io;
        $offer->volume_start = $this->volume_start;
        $offer->volume_finish = $this->volume_finish;
        $offer->volume_io_start = bcmul($this->volume_io_start, bcpow(10, $currencyInt->decimals));
        $offer->volume_io_finish = bcmul($this->volume_io_finish, bcpow(10, $currencyInt->decimals));
        $offer->volume_io_finish_deal = bcmul($this->volume_io_finish, bcpow(10, $currencyInt->decimals));

        $offer->volume_io_start_m = $this->volume_io_start;
        $offer->volume_io_finish_m = $this->volume_io_finish;

        $ret = $offer->save();
        $offer->trigger(Deal::EVENT_AFTER_OPEN);

        if (!Application::isEmpty($this->pay_list)) {
            foreach ($this->pay_list as $item) {
                OfferPayLink::add(['offer_id' => $offer->id, 'pay_id' => $item]);
            }
        }

        return $offer;
    }

}
