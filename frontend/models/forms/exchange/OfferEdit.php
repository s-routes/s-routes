<?php

namespace avatar\models\forms\exchange;

use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Deal;
use common\models\exchange\Offer;
use common\models\exchange\OfferPay;
use common\models\exchange\OfferPayLink;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\Json;

/**
 * @property int    id                         идентификатор заявки
 * @property int    user_id                    Идентификатор пользователя кто сделал предложение
 * @property int    price                      Цена по которой предлагается купить продать VVB
 * @property int    currency_id                Валюта, указывает на db.currency
 * @property int    type_id                    Вид сделки 1 - купить, 2 - продать
 * @property string volume                     Объем предложения от и до, формат JSON "[100, 10000]"
 * @property string condition                  Условия сделки, простой текст
 * @property string name                       Заголовок
 * @property int    created_at                 Момент создания предложения
 * @property int    avg_deal                   Среднее время сделки в сек для создателя предложения user_id
 * @property int    assessment_positive        Кол-во положительных оценок для создателя предложения user_id
 * @property int    assessment_negative        Кол-во положительных оценок для создателя предложения user_id
 * @property int    deals_count
 * @property int    currency_io
 * @property int    volume_start
 * @property int    volume_finish
 * @property int    volume_io_start
 * @property int    volume_io_finish
 * @property int    volume_io_finish_deal
 * @property int    offer_pay_id
 */
class OfferEdit extends ActiveRecord
{
    private $currency;

    public $pay_list;

    public static function tableName()
    {
        return 'offer';
    }

    public function convertDown()
    {
        $link = CurrencyLink::findOne(['currency_ext_id' => $this->currency_id]);
        $cur = \common\models\piramida\Currency::findOne($link->currency_int_id);
        $io = CurrencyIO::findOne($this->currency_io);
        $c = \common\models\piramida\Currency::findOne($io->currency_int_id);
        $d = $c->decimals;
        $this->price = $this->normalize(bcdiv($this->price, bcpow(10, $cur->decimals), $cur->decimals_view));
        $this->volume_io_start = $this->normalize(bcdiv($this->volume_io_start, bcpow(10, $d), $d));
        $this->volume_io_finish = $this->normalize(bcdiv($this->volume_io_finish, bcpow(10, $d), $d));

        $this->pay_list = OfferPayLink::find()->where(['offer_id' => $this->id])->select('pay_id')->column();
    }

    /**
     * Обрезает лишние знаки $this->getCurrency()->decimals_view
     * Убирает лишние нули сзади
     *
     * @param $attribute
     * @param $params
     */
    public function eGold($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->currency_io == 13) {
                $cEGOLD = Currency::findOne(['code' => 'EGOLD']);
                $this->price = $cEGOLD->kurs;
            } else {
                if (Application::isEmpty($this->price)) {
                    $this->addError($attribute, 'Это значение обязательно');
                    return;
                }
            }
        }
    }

    /**
     * Убирает лишние нули сзади
     * @param $v
     * @return mixed
     */
    public function normalize($v)
    {
        $a = explode('.', $v);
        if (count($a) == 1) {
            return $v;
        }
        $a = Str::getChars($v);
        $a = array_reverse($a);
        $c1 = 0;
        foreach ($a as $c) {
            if ($c == '0') {
                $c1++;
                continue;
            } else {
                break;
            }
        }
        if ($a[$c1] == '.') $c1++;

        $ret = [];
        for($i = $c1; $i < count($a); $i++) {
            $ret[] = $a[$i];
        }
        $ret = array_reverse($ret);

        return join('', $ret);
    }

    public function convertUp()
    {
        // надо сконвертировать $this->volume_io_finish $this->volume_io_start в $this->volume_finish $this->volume_start
        $link = CurrencyLink::findOne(['currency_ext_id' => $this->currency_id]);
        $cur = \common\models\piramida\Currency::findOne($link->currency_int_id);

        $volume_io_start_m = $this->volume_io_start;
        $volume_start_m = $volume_io_start_m * $this->price;
        $this->volume_start = bcmul($volume_start_m, pow(10, $cur->decimals));

        $volume_io_finish_m = $this->volume_io_finish;
        $volume_finish_m = $volume_io_finish_m * $this->price;
        $this->volume_finish = bcmul($volume_finish_m, pow(10, $cur->decimals));

        // volume_io_start, volume_io_finish

        $io = CurrencyIO::findOne($this->currency_io);
        $c = \common\models\piramida\Currency::findOne($io->currency_int_id);
        $d = $c->decimals;
        $this->price = bcmul($this->price, bcpow(10, $cur->decimals));

        $this->volume_io_start = bcmul($this->volume_io_start, bcpow(10, $d));
        $this->volume_io_finish = bcmul($this->volume_io_finish, bcpow(10, $d));
    }

    public function getCurrency()
    {
        if (is_null($this->currency)) {
            $this->currency = $this->_getCurrency();
        }

        return $this->currency;
    }

    public function _getCurrency()
    {
        return Currency::findOne($this->currency_id);
    }

    public function rules()
    {
        return [
            ['name', 'string', 'max' => 255],

            ['type_id', 'required'],
            ['type_id', 'validateDeals'],
            ['type_id', 'integer'],
            ['type_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле обязательно для заполнения')],

            ['offer_pay_id', 'required'],
            ['offer_pay_id', 'integer'],
            ['offer_pay_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле обязательно для заполнения')],
            ['offer_pay_id', 'validateCurrency'], // проверить что currency_id != currency_io

            ['price', 'double'],
            ['price', 'eGold', 'skipOnEmpty' => false],
            ['price', 'normalizePrice'],
            ['price', 'compare', 'operator' => '>', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле должно быть более 0')],

            ['condition', 'string'],

            ['volume_io_start', 'required'],
            ['volume_io_start', 'double'],
            ['volume_io_start', 'compare', 'operator' => '>', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле должно быть более 0')],

            ['pay_list', 'safe'],

            ['currency_io', 'required'],
            ['currency_io', 'integer'],

            ['volume_io_finish', 'required'],
            ['volume_io_finish', 'double'],
            ['volume_io_finish', 'compare', 'operator' => '>', 'compareValue' => 0, 'message' => Yii::t('c.HWRG4KACAF', 'Это поле должно быть более 0')],
            ['volume_io_finish', 'validateEnd'],
            ['volume_io_finish', 'validateWallet'], // Только для продажи. Проверяет достаточно ли монет у меня на счету

            ['volume_io_finish_deal', 'integer'],
        ];
    }

    /**
     * Обрезает лишние знаки $this->getCurrency()->decimals_view
     * Убирает лишние нули сзади
     *
     * @param $attribute
     * @param $params
     */
    public function normalizePrice($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $c = $this->getCurrency();
            $link = CurrencyLink::findOne(['currency_ext_id' => $c->id]);
            $c2 = \common\models\piramida\Currency::findOne($link->currency_int_id);
            $a = explode('.', $this->price);
            $v = '0';
            if (count($a) == 1) {
                $v =  $this->price;
            }
            if (count($a) == 2) {
                $v = $a[0] . '.' . substr($a[1], 0, $c2->decimals_view);
            }
            $this->price = $this->normalize($v);
        }
    }

    /**
     * Проверяет на предмет открытых сделок
     *
     * @param $attribute
     * @param $params
     */
    public function validateDeals($attribute, $params)
    {
        if (!$this->hasErrors()) {
        
            if (Deal::find()
            ->where(['offer_id' => $this->id])
            ->andWhere(['not', ['status' => Deal::STATUS_CREATE]])
            ->count() > 0) {
                $this->addError($attribute, Yii::t('c.svE9ShjLps', 'По предложению уже открыта сделка, сохранение не возможно'));
                return;
            }
        }
    }

    public function validateEnd($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->volume_io_finish < $this->volume_io_start) {
                $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'Объем конечный должен быть больше объема начального'));
                return;
            }
        }
    }

    public function validateCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $cio = CurrencyIO::findOne($this->currency_io);
            $offerPay = OfferPay::findOne($this->offer_pay_id);
            $this->currency_id = $offerPay->currency_id;

            // если меняемая монета USDT
            if ($this->currency_io == 3) {
                if ($offerPay->id == 3) {
                    $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'Монета обмена не должна соответствовать валюте обмена'));
                    return;
                }
            } else {
                if ($this->currency_id == $cio->currency_ext_id) {
                    $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'Монета обмена не должна соответствовать валюте обмена'));
                    return;
                }
            }
        }
    }

    public function validateWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->currency = Currency::findOne($this->currency_id);
            if ($this->type_id == Offer::TYPE_ID_SELL) {
                $currency = Currency::findOne($this->currency_id);
                // Валюта для монеты за которую производится обмен
                $currency_io = CurrencyIO::findOne($this->currency_io);

                // получаю кошелек VVB
                $data = UserBill::getInternalCurrencyWallet($currency_io->currency_int_id);

                /** @var Wallet $wallet */
                $wallet = $data['wallet'];
                $currencyInt = \common\models\piramida\Currency::findOne($currency_io->currency_int_id);

                $volume_io_finish_atom = bcmul($this->volume_io_finish, bcpow(10, $currencyInt->decimals));
                if ($wallet->amount < $volume_io_finish_atom) {
                    $this->addError($attribute, Yii::t('c.HWRG4KACAF', 'У вас недостаточно монет на счету'));
                    return;
                }
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'name'             => 'Заголовок',
            'type_id'          => Yii::t('c.HWRG4KACAF', 'Тип сделки (Я хочу ...)'),
            'pay_id'           => 'Платежная система',
            'pay_list'         => Yii::t('c.HWRG4KACAF', 'Выберите условия сделки и платежные системы'),
            'price'            => Yii::t('c.HWRG4KACAF', 'Цена'),
            'currency_id'      => Yii::t('c.HWRG4KACAF', 'Валюта'),
            'offer_pay_id'     => Yii::t('c.HWRG4KACAF', 'Валюта'),
            'user_id'          => 'Пользователь',
            'condition'        => Yii::t('c.HWRG4KACAF', 'Условия сделки'),
            'volume_start'     => Yii::t('c.HWRG4KACAF', 'Объем от'),
            'volume_end'       => Yii::t('c.HWRG4KACAF', 'до'),
            'volume_io_start'  => Yii::t('c.HWRG4KACAF', 'Объем от'),
            'volume_io_finish' => Yii::t('c.HWRG4KACAF', 'до'),
            'currency_io'      => Yii::t('c.HWRG4KACAF', 'Монета для обмена'),
        ];
    }

    public function attributeHints()
    {
        return [
            'volume_start' => Yii::t('c.HWRG4KACAF', 'Объемы указываются в монетах продажи или покупки'),
            'pay_list'     => Yii::t('c.HWRG4KACAF', 'Дополнительная информация'),
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->volume_io_finish_deal = $this->volume_io_finish;
        parent::save(false, $attributeNames); // TODO: Change the autogenerated stub

        // Обновляю ПС
        {
            $old = OfferPayLink::find()->where(['offer_id' => $this->id])->select('pay_id')->column();
            $new = $this->pay_list;
            if (Application::isEmpty($new)) $new = [];
            $delete = [];
            $add = [];

            // выясняю что удалить
            foreach ($old as $olditem) {
                if (!in_array($olditem, $new)) $delete[] = $olditem;
            }

            // выясняю что добавить
            foreach ($new as $newitem) {
                if (!in_array($newitem, $old)) $add[] = $newitem;
            }
            OfferPayLink::deleteAll(['offer_id' => $this->id, 'pay_id' => $delete]);
            foreach ($add as $item) {
                OfferPayLink::add(['offer_id' => $this->id, 'pay_id' => $item]);
            }
        }

        return true;
    }

}
