<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class CabinetSilverStep1 extends Model
{
    /** @var int  */
    public $id;

    /** @var int  */
    public $count;

    /** @var int  */
    public $korobka;

    public function init()
    {
        $this->count = 1;
    }

    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'integer'],

            ['korobka', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'count'   => 'Кол-во',
            'korobka' => 'Коробка',
        ];
    }

    public function attributeWidgets()
    {
        return [
            'korobka' => '\avatar\widgets\Korobka',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $request1 = \Yii::$app->session->get('shop-request2');
        $request1 = [];
        $request1['id'] = $this->id;
        $request1['count'] = $this->count;
        $request1['korobka'] = $this->korobka;
        \Yii::$app->session->set('shop-request2', $request1);

        return 1;
    }
}