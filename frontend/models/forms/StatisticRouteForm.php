<?php

namespace avatar\models\forms;

use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\SendLetter;
use common\models\shop\Basket;
use common\models\shop\DeliveryItem;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Request;
use yii\web\UploadedFile;

/**
 */
class StatisticRouteForm extends ActiveRecord
{
    public $action;
    public $controller;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timer';
    }

    public function rules()
    {
        return [
            [['time', 'controller_id', 'action_id', 'timer', ], 'required'],
            [['time', 'timer', ], 'double'],
            [['controller_id', 'action_id', ], 'string', 'max' => 64],
            [['controller', 'action', ], 'string', 'max' => 64],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->dbStatistic;
    }

    /**
     * @param array $params
     * @param array|null $where
     * @param Sort | array $sort
     *
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $query = \common\models\statistic\StatisticRoute::find()
            ->select([
                'action_id as action',
                'controller_id as controller',
                'min(timer) as min',
                'avg(timer) as avg',
                'max(timer) as max',
                'count(*) as counter',
            ])
            ->groupBy([
                'controller_id', 'action_id'
            ])
            ->asArray()
        ;

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            if (!is_null($where)) $query->andWhere($where);
            return $dataProvider;
        }
        if ($this->action) $query->andFilterWhere(['like', 'action_id', $this->action]);
        if ($this->controller) $query->andFilterWhere(['like', 'controller_id', $this->controller]);

        return $dataProvider;
    }
}
