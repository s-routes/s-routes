<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\ChatRoom;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\shop\Basket;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\shop\RequestProduct;
use common\models\ShopRequestProduct;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

class CabinetSilverStep4 extends Model
{
    /** @var string  */
    public $count;

    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'string'],
            ['count', 'validateWallet'],
        ];
    }

    public function attributeLabels()
    {
        return ['count' => 'Цена'];
    }

    public function init()
    {
        $this->count = Currency::getValueFromAtom(Basket::getPrice(), Currency::NEIRO);
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            $request1 = \Yii::$app->session->get('shop-request2');
            $price = Basket::getPrice();

            $cio = CurrencyIO::findFromExt(\common\models\avatar\Currency::NEIRO);
            $main_wallet = Wallet::findOne($cio->main_wallet);
            $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id);
            /** @var \common\models\piramida\Wallet $wFrom */
            $wFrom = $data['wallet'];
            if ($wFrom->amount < $price) {
                $this->addError($a, 'Недостаточно средств на вашем счету');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $request1 = \Yii::$app->session->get('shop-request2');
        $price = Basket::getPrice();

        $room = ChatRoom::add(['last_message' => time()]);
        // Добавляю заказ
        $request = Request::add([
            'created_at'  => time(),
            'billing_id'  => null,
            'address'     => $request1['address'],
            'user_id'     => Yii::$app->user->id,
            'price'       => $price,
            'sum'         => $price,
            'currency_id' => \common\models\avatar\Currency::NEIRO,
            'chat_id'     => $room->id,
            'name'     => $request1['name'],
            'phone'     => $request1['phone'],
        ]);

        foreach (Basket::get() as $row) {
            // Добавляю товары
            RequestProduct::add([
                'request_id' => $request->id,
                'product_id' => $row['id'],
                'count'      => $row['count'],
            ]);
        }

        // делаю оплату
        $cio = CurrencyIO::findFromExt(\common\models\avatar\Currency::NEIRO);
        $main_wallet = Wallet::findOne($cio->main_wallet);
        $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id);

        /** @var \common\models\piramida\Wallet $wFrom */
        $wFrom = $data['wallet'];
        $t = $wFrom->move2($main_wallet, $price, 'Оплата заказа #' . $request->id);
        $request->txid_coin = $t['transaction']['id'];
        $request->save();

        $this->notificateTelegram($request);

        Basket::clear();

        return ['id' => $request->id];
    }

    /**
     * Уведомляет Админа и бух
     * @param \common\models\shop\Request $request
     */
    private function notificateTelegram($request)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $buhList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        $adminList = Yii::$app->authManager->getUserIdsByRole('role_admin');
        $all = ArrayHelper::merge($buhList, $adminList);
        $all = array_unique($all);

        foreach ($all as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                $link = Url::to(['cabinet-school-shop-requests/view', 'id' => $request->id], true);
                $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => 'Сформирован заказ на монету №' . $request->id . "\n" . "\n" . 'Ссылка на чат: ' . $link]);
            }
        }
    }
}