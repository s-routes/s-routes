<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security\AES;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class PasswordType4 extends Model
{
    /** @var  string пароль от кабинета */
    public $password;

    /** @var  string пароль от кабинета */
    public $seeds;

    /** @var  string пароль от кабинета */
    public $isAgree;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string'],
            ['password', 'validatePassword'],

            ['seeds', 'required'],
            ['seeds', 'string'],
            ['seeds', 'normalizeSeeds'],
            ['seeds', 'validateSeeds', 'message' => 'Вы ввели не верный ключ'],

            ['isAgree', 'required'],
            ['isAgree', 'integer'],
            ['isAgree', 'compare', 'compareValue' => 1, 'message' => 'Нужно принять условия соглашения'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
            }
        }
    }

    public function normalizeSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $ret = [];
            $arr = explode("\n", $this->seeds);
            foreach ($arr as $row) {
                $arr2 = explode(" ", $row);
                foreach ($arr2 as $word) {
                    if (trim($word) != '') {
                        $ret[] = trim($word);
                    }
                }
            }
            $this->seeds = join(' ', $ret);
        }
    }

    public function validateSeeds($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (Yii::$app->session->get('seeds-start') != $this->seeds) {
                $this->addError($attribute, 'Не верный SEEDS');
            }
        }
    }


    /**
     */
    public function action()
    {
        // Устанавливаю шифрование на кошельки
        $this->setPassword($this->password);

        // Устанавливаю настройки в пользователе
        $this->setUserSettings();

        // Сохраняю SEEDS
        $this->saveSeeds();

        return true;
    }

    /**
     * Генерирует ключи и сохраняет в БД и кеше
     *
     * @return string
     */
    private function saveSeeds()
    {
        $seeds = $this->seeds;
        $sha256 = hash('sha256', $seeds);
        $key32 = substr($sha256, 0, 32);
        $hash = \common\services\Security\AES::encrypt256CBC($this->password, $key32);
        Yii::info(\yii\helpers\VarDumper::dumpAsString([$seeds, $this->password, $key32]), 'info\avatar\models\forms\PasswordType4::saveSeeds');
        $seedsHashObject = UserSeed::add($hash);
        $seedsHashObject->seeds = $hash;
        $seedsHashObject->save();

        Yii::$app->session->remove('seeds-start');
        Yii::$app->session->remove('seeds');

        return $seeds;
    }

    /**
     * Устанавливает настройки пользователя
     *
     * @return bool
     */
    private function setUserSettings()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        $user->password_save_type = UserBill::PASSWORD_TYPE_HIDE_CABINET;
        $user->wallets_is_locked = 0;

        return $user->save();
    }

    /**
     * Устанавливает новый пароль на кошельки
     *
     * @param string $to пароль от кабинета
     *
     * @throws \Exception
     */
    private function setPassword($to)
    {
        /**
         * Все счета кроме мерчанта
         */
        $billingList = UserBill::find()
            ->where([
                'user_id'      => Yii::$app->user->id,
                'mark_deleted' => 0,
                'is_merchant'  => 0,
            ])
            ->all();

        $t = \Yii::$app->db->beginTransaction();
        try {
            /** @var \common\models\avatar\UserBill $billing */
            foreach ($billingList as $billing) {
                $billing->setPasswordHideCabinet($to);
            }
            $t->commit();
        } catch (\Exception $e) {
            $t->rollBack();
            throw $e;
        }
    }
}
