<?php

namespace avatar\models\forms\shop;

use common\models\school\UserLink;
use common\models\SendLetter;
use common\models\UserRoot;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopRegistration extends Model
{
    public $login;
    public $password;

    /** @var  int */
    public $school_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['school_id', 'required'],
            ['school_id', 'integer'],

            ['login', 'required'],
            ['login', 'string'],
            ['login', 'trim'],
            ['login', 'toLower'],
            ['login', 'email'],
            ['login', 'validateUser'],
            ['login', 'validateUserRoot'],

            ['password', 'required'],
            ['password', 'string'],

        ];
    }

    /**
     */
    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $user = \common\models\UserAvatar::findOne(['email' => $this->login]);
                $this->addError($attribute, 'Такой пользователь уже есть, придумайте другой или если это ваш логин то войдите');
                return;
            } catch (\Exception $e) {

            }
        }
    }

    /**
     */
    public function toLower($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->login = strtolower($this->login);
        }
    }

    /**
     */
    public function validateUserRoot($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $userRoot = UserRoot::findOne(['email' => $this->login]);
            if (is_null($userRoot)) {
                $userRoot = UserRoot::add(['email' => $this->login]);
                $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
            } else {
                $link = UserLink::findOne(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
                if (is_null($link)) {
                    $link = UserLink::add(['user_root_id' => $userRoot->id, 'school_id' => $this->school_id]);
                }
            }
        }
    }

    /**
     */
    public function action()
    {
        // отосылаю письмо
        // Добавляю регистрационную запись
        // Добавляю запись с паролем
        \common\models\UserAvatar::registration($this->login, $this->password);

        // Здесь как раз нужно сделать прозрачную регистриацию
        // то есть создать пользователя и залогинить его
    }
}
