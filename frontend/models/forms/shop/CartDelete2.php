<?php

namespace avatar\models\forms\shop;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\PaySystemConfig;
use common\models\piramida\Wallet;
use common\models\shop\Basket;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\ShopTempProduct;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use school\modules\sroutes\models\Offer;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 */
class CartDelete2 extends Model
{
    public $id;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    public function action()
    {

        // удаляю товар
        ShopTempProduct::deleteAll(['id' => $this->id]);

        // считаю сумму
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        if ($data['is_guest'] == false) {
            $user_id = Yii::$app->user->id;
        } else {
            $user_id = $data['user_id'];
        }
        $sum = 0;
        $rows = ShopTempProduct::find()->where(['user_id' => $user_id])->all();
        foreach ($rows as $row) {
            $sum += $row['price'] * $row['count'];
        }

        return [
            'sum'          => $sum,
            'sumFormatted' => Yii::$app->formatter->asDecimal($sum, 2),
        ];
    }
}
