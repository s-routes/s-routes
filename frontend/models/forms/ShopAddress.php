<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopAddress extends Model
{
    public $index;
    public $address;
    public $comment;

    public $phone;
    public $name_first;
    public $name_last;
    public $email;
    public $country_id;
    public $town;

    public $dov_name_first;
    public $dov_name_last;
    public $dov_email;
    public $dov_phone;
    public $dov_country_id;
    public $dov_town;
    public $dov_address;

    public function init()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        $this->email = $user->email;
        $this->name_first = $user->name_first;
        $this->name_last = $user->name_last;
        $this->country_id = $user->country_id;
        $this->town = $user->town;
        $this->address = $user->address;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['comment', 'string', 'max' => 255],
            ['name_first', 'string', 'max' => 32],
            ['name_last', 'string', 'max' => 32],
            ['phone', 'string', 'max' => 20],
            ['email', 'string', 'max' => 32],
            ['country_id', 'integer'],
            ['town', 'string', 'max' => 64],

            ['dov_name_first', 'string', 'max' => 32],
            ['dov_name_last', 'string', 'max' => 32],
            ['dov_phone', 'string', 'max' => 20],
            ['dov_email', 'string', 'max' => 32],
            ['dov_country_id', 'integer'],
            ['dov_town', 'string', 'max' => 64],

        ];
    }

    public function attributeLabels()
    {
        return [
            'comment' => 'Комментарий к заказу',

            'address'    => 'Адрес',
            'phone'      => 'Телефон',
            'name_first' => 'Имя',
            'name_last'  => 'Фамилия',
            'email'      => 'Email',
            'country_id' => 'Страна',
            'town'       => 'Город',

            'dov_address'    => 'Адрес',
            'dov_phone'      => 'Телефон',
            'dov_name_first' => 'Имя',
            'dov_name_last'  => 'Фамилия',
            'dov_email'      => 'Email',
            'dov_country_id' => 'Страна',
            'dov_town'       => 'Город',
        ];
    }

    /**
     */
    public function action()
    {
        $request = Yii::$app->session->get('request', []);
        $request['address']['address'] = $this->address;
        $request['address']['phone'] = $this->phone;
        $request['address']['name_first'] = $this->name_first;
        $request['address']['name_last'] = $this->name_last;
        $request['address']['town'] = $this->town;
        $request['address']['country_id'] = $this->country_id;
        $request['address']['email'] = $this->email;

        $request['address']['dov'] = [
            'name_first' => $this->dov_name_first,
            'name_last'  => $this->dov_name_last,
            'country_id' => $this->dov_country_id,
            'email'      => $this->dov_email,
            'town'       => $this->dov_town,
            'address'    => $this->dov_address,
            'phone'      => $this->dov_phone,
        ];

        Yii::$app->session->set('request', $request);

        return true;
    }
}
