<?php

namespace avatar\models\forms;

use avatar\models\Wallet;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\SendLetter;
use common\models\TaskOutputHistory;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminContributionLot extends Model
{
    /** @var  string */
    public $date_start;

    /** @var  string */
    public $date_end;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['date_start', 'string'],

            ['date_end', 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'date_start' => 'Дата начала',
            'date_end'   => 'Дата окончания',
        ];
    }

    /**
     */
    public function action()
    {
//        \cs\services\VarDumper::dump(ArrayHelper::toArray($this));
    }
}
