<?php

namespace avatar\models\forms;

use app\models\Union;
use app\services\Subscribe;
use common\models\school\School;
use common\models\shop\DostavkaItem;
use cs\Application;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * @property int    id
 * @property int    product_id
 * @property string url
 * @property string image
 *
 */
class ProductVideo extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_product_video';
    }

    public function rules()
    {
        return [
            ['product_id', 'integer'],
            ['image', 'string'],
            ['url', 'string'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'image' => [
                'class' => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'settings' => [
                    'controller'   => 'upload4',
                    'maxSize'      => 2000,
                    'server'       => '',
                    'button_label' => 'Выберите файл',
                ],
                'update'     => [
                    [
                        'function' => 'crop',
                        'index'    => 'crop',
                        'options'  => [
                            'width'  => '300',
                            'height' => '300',
                            'mode'   => 'MODE_THUMBNAIL_CUT',
                        ],
                    ],
                ],
            ]
        ];
    }
}