<?php
namespace avatar\models\forms;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer $id
 * @property string  $code
 * @property string  $title
 * @property integer $comission_percent
 * @property string  $comission_fix
 * @property string  $class_name
 * @property string  $image
 */
class PaySystem extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paysystems';
    }

    public function formAttributes()
    {
        return [
            [
                'code',
                'Код',
                1,
                'string',
                ['max' => 20],
                'Малые английские буквы или тире'
            ],
            [
                'title',
                'Название',
                1,
                'string',
                ['max' => 60],
            ],
            [
                'currency',
                'Валюта',
                1,
                'string',
                ['max' => 6],
                'Код валюты, большие буквы'
            ],
            [
                'comission_percent',
                'Комиссия в процентах',
                0,
                'double',
            ],
            [
                'comission_fix',
                'Комиссия фиксированная',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'class_name',
                'Название класса',
                0,
                'string',
                ['max' => 20],
                'app\models\Piramida\WalletSource'
            ],
            [
                'image',
                'Картинка',
                0,
                '\common\widgets\FileUpload3\Validator',
                'widget' => [
                    '\common\widgets\FileUpload3\FileUpload',
                    [
                        'options' => [
                            'small' => \avatar\services\Html::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }
}
