<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;

class CabinetPhoneCheckBox extends Model
{
    public $flag;

    public function init()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;
        if ($user->is_show_phone == 1) {
            $this->flag = 1;
        } else {
            $this->flag = 0;
        }
    }

    public function rules()
    {
        return [
            ['flag', 'integer'],
        ];
    }
}