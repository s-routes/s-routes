<?php

namespace avatar\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class PrizmInput extends Model
{
    public $account;
    public $txid;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['account', 'string'],
            ['account', 'trim'],
            ['account', 'validateAccount'],
            ['txid', 'string'],
            ['txid', 'trim'],
            ['txid', 'validateTwo', 'skipOnEmpty' => false],
        ];
    }

    public function validateAccount($a,$p)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->account, 'PRIZM-')) {
                $this->addError($a, 'Кошелек должен начинаться с PRIZM-');
                return;
            }
        }
    }

    /**
     *
     * @param $a
     * @param $p
     */
    public function validateTwo($a,$p)
    {
        if (!$this->hasErrors()) {
            if (Application::isEmpty($this->account) && Application::isEmpty($this->txid)) {
                $this->addError('account', 'Нужно заполнить хотябы одно поле');
                $this->addError('txid', 'Нужно заполнить хотябы одно поле');
                return;
            }
        }
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'account' => 'Адрес кошелька призм с которого производилась транзакция',
            'txid'    => 'Идентификатор транзакции',
        ];
    }

    /**
     */
    public function action()
    {
        // Закрываю все старые задачи
        \common\models\TaskPrizmInput::updateAll(['status' => 2], ['user_id' => Yii::$app->user->id, 'status' => 0]);

        // Создаю новую задачу
        $input = \common\models\TaskPrizmInput::add([
            'account' => $this->account,
            'user_id' => Yii::$app->user->id,
            'txid'    => $this->txid,
        ]);

        return $input;
    }
}
