<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class CabinetSilverStep3 extends Model
{
    /** @var string  */
    public $name;

    /** @var string  */
    public $phone;

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string'],

            ['phone', 'required'],
            ['phone', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'   => 'Имя',
            'phone'  => 'Телефон',
        ];
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        $request1 = \Yii::$app->session->get('shop-request2');
        $request1['name'] = $this->name;
        $request1['phone'] = $this->phone;
        \Yii::$app->session->set('shop-request2', $request1);


        return 1;
    }
}