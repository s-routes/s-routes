<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use kartik\datetime\DateTimePicker;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 *
 */
class Lot extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public function rules()
    {
        return [
            ['price', 'required'],
            ['price', 'integer'],

            ['currency_id', 'integer'],

            ['type', 'required'],
            ['type', 'integer'],

            ['prize_type', 'required'],
            ['prize_type', 'integer'],

            ['prize_name', 'string'],

            ['prize_amount', 'integer'],

            ['bilet_count', 'required'],
            ['bilet_count', 'integer'],

            ['cash_back_percent', 'integer'],

            ['time_type', 'integer'],

            ['cash_back_price', 'integer'],

            ['time_start', 'required'],
            ['time_start', 'default'],
            ['time_start', 'validateLate'],

            ['time_finish', 'default'],
            ['time_finish', 'validateLate2'],
            ['time_finish_add', 'integer'],

            ['time_raffle', 'default'],
            ['time_raffle', 'validateLate2'],
            ['time_raffle_add', 'integer'],

            ['name', 'required'],
            ['name', 'string'],

            ['image', 'required'],
            ['image', 'string'],

            ['description', 'required'],
            ['description', 'string'],

            ['link_youtube', 'string'],
            ['link_youtube', 'url'],

            ['link_instagram', 'string'],
            ['link_instagram', 'url'],

            ['link_telegram', 'string'],
            ['link_telegram', 'url'],

            ['content', 'string'],
        ];
    }

    public function validateLate($a, $p)
    {
        if (!$this->hasErrors()) {
            $t = $this->$a->format('U');
            if ((time() - $t) > 0) {
                $this->addError($a, 'Нужно указываь время больше настоящего');
                return;
            }
        }
    }

    public function validateLate2($a, $p)
    {
        if (!$this->hasErrors()) {
            if ($this->time_type) {

            } else {
                $t = $this->$a->format('U');
                if ((time() - $t) > 0) {
                    $this->addError($a, 'Нужно указываь время больше настоящего');
                    return;
                }
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'time_raffle'       => 'Время розырыша',
            'time_start'        => 'Время старта продаж',
            'time_finish'       => 'Время окончания продаж',
            'description'       => 'Описание',
            'name'              => 'Наименование',
            'price'             => 'Цена билета',
            'bilet_count'       => 'Кол-во билетов',
            'cash_back_price'   => 'Цена пакета компенсации',
            'cash_back_percent' => 'Процент кешбека',
            'type'              => 'Тип',
            'prize_name'        => 'Приз. Наименование Главного Приза',
            'prize_type'        => 'Приз. Тип',
            'prize_amount'      => 'Приз. Стоимость Главного Приза',
            'link_youtube'      => 'ссылка на youtube для ручной лотереи',
            'link_instagram'    => 'ссылка на instagram для ручной лотереи',
            'link_telegram'     => 'ссылка на telegram для ручной лотереи',
            'time_type'         => 'Задавать дельту от старта?',
            'time_finish_add'   => 'Дельта от старта до окончания продаж',
            'time_raffle_add'   => 'Дельта от старта до розыгрыша',
        ];
    }

    public function attributeHints()
    {
        return [
            'price'           => 'LOT (RUB)',
            'time_raffle'     => 'Время МСК',
            'time_start'      => 'Время МСК',
            'time_finish'     => 'Время МСК',
            'time_finish_add' => 'мин',
            'time_raffle_add' => 'мин',
        ];
    }

    public function attributeWidgets()
    {
        return [
            'content'         => '\common\widgets\HtmlContent\HtmlContent',
            'time_type'       => '\common\widgets\CheckBox2\CheckBox',
            'price'           => '\avatar\widgets\Price',
            'prize_amount'    => '\avatar\widgets\Price',
            'cash_back_price' => '\avatar\widgets\Price',
            'image' => [
                'class' => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'settings' => [
                    'controller'   => 'upload4',
                    'maxSize'      => 2000,
                    'server'       => '',
                    'button_label' => 'Выберите файл',
                ],
                'update'     => [
                    [
                        'function' => 'crop',
                        'index'    => 'crop',
                        'options'  => [
                            'width'  => '300',
                            'height' => '300',
                            'mode'   => 'MODE_THUMBNAIL_CUT',
                        ],
                    ],
                ],
            ]
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->time_type == 1) {
            // Если дельта указана
            $this->time_finish = $this->time_start + ($this->time_finish_add * 60);
            $this->time_raffle = $this->time_start + ($this->time_raffle_add * 60);
        } else {
            // Если указаны точные даты и время
            $this->time_finish_add = intval(($this->time_finish - $this->time_start) / 60);
            $this->time_raffle_add = intval(($this->time_raffle - $this->time_start) / 60);
        }

        return parent::save(false, $attributeNames); // TODO: Change the autogenerated stub
    }
}
