<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\ChatRoom;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class CabinetSilverStep6 extends Model
{
    /** @var integer  */
    public $id;

    /** @var integer  */
    public $count;

    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'integer'],
            ['count', 'validateWallet'],
        ];
    }

    public function attributeLabels()
    {
        return ['count' => 'Кол-во'];
    }

    public function init()
    {
        $this->count = 1;
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            $request1 = \Yii::$app->session->get('shop-request2');

            // вычисляю стоимость коробок в NEIRO
            $DeliveryItem = \common\models\shop\DeliveryItem::findOne($request1['dostavka']);
            $cio = CurrencyIO::findFromExt($DeliveryItem['currency_id']);
            $cInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);
            $price = \common\models\piramida\Currency::getValueFromAtom($DeliveryItem['price'], $cInt->id);
            $cNeiro = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::NEIRO);
            $priceNeiro = $price / $cNeiro->kurs;

            $cio = CurrencyIO::findFromExt(\common\models\avatar\Currency::NEIRO);
            $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id);
            /** @var \common\models\piramida\Wallet $wFrom */
            $wFrom = $data['wallet'];
            if ($wFrom->amount < Currency::getAtomFromValue($priceNeiro, Currency::NEIRO)) {
                $this->addError($a, 'Недостаточно средств на вашем счету');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $request = Request::findOne($this->id);
        $request1 = \Yii::$app->session->get('shop-request2');

        // вычисляю стоимость коробок в NEIRO
        $DeliveryItem = \common\models\shop\DeliveryItem::findOne($request1['dostavka']);
        $cio = CurrencyIO::findFromExt($DeliveryItem['currency_id']);
        $cInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);
        $price = \common\models\piramida\Currency::getValueFromAtom($DeliveryItem['price'], $cInt->id);
        $cNeiro = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::NEIRO);
        $priceNeiro = $price / $cNeiro->kurs;
        $priceAtom = Currency::getAtomFromValue($priceNeiro, Currency::NEIRO);

        // делаю оплату
        $cio = CurrencyIO::findFromExt(\common\models\avatar\Currency::NEIRO);
        $main_wallet = Wallet::findOne($cio->main_wallet);
        $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id);

        /** @var \common\models\piramida\Wallet $wFrom */
        $wFrom = $data['wallet'];
        $t = $wFrom->move2($main_wallet, $priceAtom, 'Оплата коробочки в заказе #' . $request->id);
        $request->txid_korobka = $t['transaction']['id'];
        $request->save();

        return ['id' => $request->id];
    }
}