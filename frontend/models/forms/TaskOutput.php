<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\CurrencyIoModification;
use common\models\NeironTransaction;
use common\models\piramida\Wallet;
use common\models\TaskOutputHistory;
use common\models\TaskPrizmOutput;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use Yii;

/**
 * @property integer id
 * @property integer currency_io_id
 * @property integer status         Статус задачи 0 - задача создана. 1 - задача выполнена, транзакция указана
 * @property integer created_at
 * @property integer finished_at
 * @property integer amount
 * @property integer txid_internal
 * @property integer billing_id    Идентификатор валюты для вывода, db.currency.id
 * @property integer currency_id
 * @property string  account
 * @property string  txid
 * @property string  comment
 * @property string  prizm_key
 * @property integer user_id
 * @property integer comission
 * @property integer comission_id
 * @property integer comission_user
 * @property integer modification_id
 *
 * @package common\models
 */
class TaskOutput extends ActiveRecord
{
    /** @var \common\models\avatar\UserBill */
    public $_billing;

    /** @var \common\models\CurrencyIO */
    public $CurrencyIO;

    /** @var  CurrencyIoModification | null */
    public $modification;

    /** @var \common\models\piramida\Wallet */
    public $_wallet;

    public static function tableName()
    {
        return 'task_output';
    }

    public function rules()
    {
        $params = [
            ['modification_id', 'integer'],
            ['modification_id', 'setModification'],

            ['account', 'required'],
            ['account', 'string'],
            ['account', 'validateAccount'],

            ['billing_id', 'required'],
            ['billing_id', 'integer'],

            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'compare', 'operator' => '>', 'compareValue' => 0, 'type' => 'number'],
            ['amount', 'validateAmount'],

            ['comission_user', 'required'],
            ['comission_user', 'integer'],
        ];

        if ($this->CurrencyIO->currency_int_id == \common\models\piramida\Currency::EGOLD) {
            $params = ArrayHelper::merge($params, [
                ['comment', 'string', 'max' => 18],
                ['comment', 'validateCommentEgold'],
            ]);
        } else  {
            $params = ArrayHelper::merge($params, [
                ['comment', 'string', 'max' => 1000],
            ]);
        }

        if ($this->CurrencyIO->currency_int_id == \common\models\piramida\Currency::PZM) {
            $params = ArrayHelper::merge($params, [
                ['prizm_key', 'required'],
                ['prizm_key', 'string', 'max' => 160],
            ]);
        }

        if ($this->CurrencyIO->currency_int_id == \common\models\piramida\Currency::NEIRO) {
            $params = ArrayHelper::merge($params, [
                ['comission_id', 'required'],
                ['comission_id', 'integer'],
            ]);
        }

        return $params;
    }

    public function attributeLabels()
    {
        $params = [
            'account'      => Yii::t('c.TB9ZAPoWIy', 'Вывести на кошелек'),
            'billing_id'   => Yii::t('c.TB9ZAPoWIy', 'Счет'),
            'amount'       => Yii::t('c.TB9ZAPoWIy', 'Кол-во монет, которое я хочу вывести (без учета комиссии)'),
            'comission_id' => Yii::t('c.TB9ZAPoWIy', 'Выберите скорость транзакции'),
        ];
        if ($this->CurrencyIO->currency_int_id == \common\models\piramida\Currency::EGOLD) {
            $params['comment'] = Yii::t('c.TB9ZAPoWIy', 'Метка (пин-комментарий)');
        } else {
            $params['comment'] = Yii::t('c.TB9ZAPoWIy', 'Комментарий');
        }

        return $params;
    }

    public function attributeHints()
    {
        if ($this->CurrencyIO->currency_int_id == \common\models\piramida\Currency::EGOLD) {
            return ['comment' => Yii::t('c.TB9ZAPoWIy', 'Только цифры. Ввод максимум до 18 цифр')];
        } else {
            return [];
        }
    }

    public function validateAccount($a, $p)
    {
        if (!$this->hasErrors()) {
            $billing = UserBill::findOne($this->billing_id);
            $wallet = Wallet::findOne($billing->address);

            $this->_billing = $billing;
            $this->_wallet = $wallet;

            if ($this->modification) {
                $function_class = $this->modification->function_check;
            } else {
                $function_class = $this->CurrencyIO->function_check;
            }

            /** @var \avatar\services\currency\CurrencyInterface $function_check */
            $function_check = new $function_class();
            $data = $function_check->validateAddress($this->account);
            if (!$data['status']) {
                $this->addError($a, $data['error']);
                return;
            }
        }
    }

    public function setModification($a, $p)
    {
        if (!$this->hasErrors()) {
            $m = CurrencyIoModification::findOne($this->modification_id);
            $this->modification = $m;
        }
    }

    public function validateCommentEgold($a, $p)
    {
        if (!$this->hasErrors()) {
            $value = $this->$a;
            if (preg_match('/^\d+$/', $value) != 1) {
                $this->addError($a, Yii::t('c.TB9ZAPoWIy', 'Это поле может содержать только цифры и максимум до 18 цифр'));
                return;
            }
        }
    }

    public function validateAmount($a, $p)
    {
        if (!$this->hasErrors()) {
            $c = \common\models\piramida\Currency::findOne($this->_wallet->currency_id);
            $a_atom = bcmul($this->amount, bcpow(10, $c->decimals));
            if ($this->_wallet->amount < $a_atom) {
                $this->addError($a, Yii::t('c.TB9ZAPoWIy', 'На выбранном кошельке недостаточно монет'));
                return;
            }

            if ($this->modification) {
                $output_min = $this->modification->output_min;
                $comission_out = $this->modification->comission_out;
            } else {
                $output_min = $this->CurrencyIO->output_min;
                $comission_out = $this->CurrencyIO->comission_out;
            }

            // минимальная сумма на вывод
            if (!Application::isEmpty($output_min)) {
                $min = bcdiv($output_min, pow(10, $c->decimals), $c->decimals);
                if ($this->amount < $min) {
                    $this->addError($a, Yii::t('c.TB9ZAPoWIy', 'Минимальная суммы вывода = {min} {code}', ['min' => $min, 'code' => $c->code]));
                    return;
                }
            }

            if ($this->comission_user > $this->_wallet->amount) {
                $this->addError($a, Yii::t('c.TB9ZAPoWIy', 'У вас недостаточно средств для оплаты комиссии'));
                return;
            }
        }
    }

    /**
     * @param \common\models\CurrencyIO $CurrencyIO
     * @throws \yii\base\Exception
     */
    public function action($CurrencyIO)
    {
        $c = \common\models\piramida\Currency::findOne($this->_wallet->currency_id);
        $a_atom = bcmul($this->amount, bcpow(10, $c->decimals));

        $this->amount = $a_atom;
        $this->user_id = Yii::$app->user->id;
        $this->currency_id = $this->_billing->currency;
        $this->currency_io_id = $CurrencyIO->id;
        $this->created_at = time();
        $ret = $this->save(false);
        if (!$ret) VarDumper::dump($this->errors);
        $this->id = $this::getDb()->lastInsertID;

        $amountM = \common\models\piramida\Currency::getValueFromAtom($this->amount - $this->comission_user, $c->id);

        // Отправляю уведомление бухгалтеру
        $usersIds = Yii::$app->authManager->getUserIdsByRole('role_buh');
        $usersEmail = [];
        foreach ($usersIds as $id) {
            $usersEmail[] = UserAvatar::findOne($id)->email;
        }

        $task = \common\models\TaskOutput::findOne($this->id);
        \common\services\Subscribe::sendArray($usersEmail, Yii::t('c.TB9ZAPoWIy', 'Новая заявка').' #' . $task->id,'task_out_new', [
            'task' => $task
        ]);

        // Блокирую средства
        $t = $this->_wallet->move2(
            $CurrencyIO->block_wallet,
            $a_atom,
            Json::encode([
                'Средства списаны со счета по заявке на вывод {request}',
                [
                    'request' => Html::a('#' . $task->id, ['cabinet-active/out-index'], ['data-pjax' => 0])
                ]
            ]),
            NeironTransaction::TYPE_OUT
        );

        // Делаю запись о типе транзакции если это NERON
        if ($this->_wallet->currency_id == \common\models\piramida\Currency::NEIRO) {
            NeironTransaction::add([
                'transaction_id' => $t['transaction']['id'],
                'operation_id'   => $t['operation_sub']['id'],
                'type_id'        => NeironTransaction::TYPE_OUT,
            ]);
        }

        // Добавляю историю заявки
        $i = TaskOutputHistory::add([
            'task_id' => $task->id,
            'comment' => Yii::t('c.TB9ZAPoWIy', 'Средства списаны со счета по заявке на вывод {request}') . ' ' . Html::a('#' . $task->id, ['cabinet-active/out-item', 'id' => $task->id], ['data-pjax' => 0]),
        ]);

        // Уведомляю админа по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $sum = Yii::$app->formatter->asDecimal($amountM, $c->decimals) . ' ' . $c->code;
                $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Создана заявка #{task}  на вывод {sum}', ['task' => $task->id, 'sum' => $sum], $language)]);
            }
        }

        // Уведомляю клиента по телеграму
        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
            $sum = Yii::$app->formatter->asDecimal($amountM, $c->decimals) . ' ' . $c->code;
            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Создана заявка #{task}  на вывод {sum}', ['task' => $task->id, 'sum' => $sum], $language)]);
        }
    }
}