<?php

namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ConvertOperation;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\httpclient\Client;
use yii\web\IdentityInterface;

/**
 */
class ConvertSbp extends Model
{
    /** @var  int Идентификатор счета user_bill.id */
    public $id;

    /** @var  int Кол-во монет для обмена */
    public $amount;

    /** @var  int Кол-во атомов для отправки */
    public $amountToSend;

    /** @var  \common\models\piramida\Wallet */
    public $walletOut;

    /** @var  \common\models\avatar\CurrencyLink */
    public $link;

    /** @var  \common\models\avatar\UserBill */
    public $billOut;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\common\models\avatar\UserBill'],
            ['id', 'setItem'],

            ['amount', 'required'],
            ['amount', 'integer'],
            ['amount', 'convertToAtom'],
            ['amount', 'validateOutWallet'],
        ];
    }

    public function setItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billOut = UserBill::findOne($this->id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Счет не найден');
                return;
            }
            if ($billOut->mark_deleted) {
                $this->addError($attribute, 'Счет удален');
                return;
            }
            if ($billOut->user_id != Yii::$app->user->id) {
                $this->addError($attribute, 'Это не ваш счет');
                return;
            }
            $link = CurrencyLink::findOne(['currency_ext_id' => $billOut->currency]);
            if (is_null($link)) {
                $this->addError($attribute, 'Эта монета не предназначена для перевода');
                return;
            }
            $walletOut = Wallet::findOne($billOut->address);
            if (!in_array($walletOut->currency_id, [\common\models\piramida\Currency::SBP, \common\models\piramida\Currency::VVB])) {
                $this->addError($attribute, 'Не верная валюта');
                return;
            }
            $this->link = $link;
            $this->walletOut = $walletOut;
            $this->billOut = $billOut;
        }
    }

    public function convertToAtom($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $currency = \common\models\piramida\Currency::findOne($this->link->currency_int_id);
            $this->amountToSend = $this->amount * pow(10, $currency->decimals);
        }
    }

    public function validateOutWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->walletOut->amount < $this->amountToSend) {
                $this->addError($attribute, 'Недостаточно денег в кошельке');
                return;
            }
        }
    }

    /**
     * @return array
     * @throws
     */
    public function action()
    {
        // получаю курс валюты
        $currencyPZM = \common\models\avatar\Currency::findOne(172);
        $kursPZM = $currencyPZM->price_rub;
        if ($this->walletOut->currency_id == \common\models\piramida\Currency::VVB) {
            $kursPZM = 1/$kursPZM;
        }

        // Завожу операцию
        $operation = ConvertOperation::add([
            'kurs' => (int)($kursPZM * 1000000)
        ]);

        // Забираю монеты SBP
        $walletIn = Yii::$app->params['walletSBPall'];
        if ($this->walletOut->currency_id == \common\models\piramida\Currency::VVB) {
            $walletIn = Yii::$app->params['walletVvbAll'];
        }

        $tOut = $this->walletOut->move(
            $walletIn,
            $this->amountToSend,
            'Конвертация монет, операция #' . $operation->id . ' курс=' . $kursPZM
        );

        // Начисляю монеты VVB
        $walletOut = Yii::$app->params['walletVvbAll'];
        if ($this->walletOut->currency_id == \common\models\piramida\Currency::VVB) {
            $walletOut = Yii::$app->params['walletSBPall'];
        }
        $walletOutObject = Wallet::findOne($walletOut);

        $currencyTo = \common\models\piramida\Currency::VVB;
        if ($this->walletOut->currency_id == \common\models\piramida\Currency::VVB) {
            $currencyTo = \common\models\piramida\Currency::SBP;
        }
        $dataTo = UserBill::getInternalCurrencyWallet($currencyTo, Yii::$app->user->id);
        $walletTo = $dataTo['wallet'];
        $amountFromConvert = (int) ($kursPZM * $this->amountToSend);

        $tIn = $walletOutObject->move(
            $walletTo,
            $amountFromConvert,
            'Конвертация монет, операция #' . $operation->id . ' курс=' . $kursPZM
        );

        // Обновляю операцию
        $operation->wallet_from_id = $this->walletOut->id;
        $operation->wallet_to_id = $walletTo->id;
        $operation->tx_from_id = $tOut->id;
        $operation->tx_to_id = $tIn->id;
        $operation->amount = $this->amountToSend;
        $operation->save();

        return [$tOut, $tIn];
    }
}
