<?php

namespace avatar\models\forms;

use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatRoom;
use common\models\Config;
use common\models\piramida\Currency;
use common\models\shop\Basket;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\shop\RequestProduct;
use common\models\ShopRequestProduct;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 *
 */
class ShopOrderAddress extends Model
{
    /**
     * @var string
     */
    public $address;

    public function attributeWidgets()
    {
        return [];
    }

    public function rules()
    {
        return [
            ['address', 'required'],
            ['address', 'string'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $request1 = \Yii::$app->session->get('request');

        $price = Basket::getPrice() / 100;
        $bm = BillingMainClass::findOne(['name' => '\common\models\shop\Request']);
        $b = BillingMain::add([
            'sum_before'  => $price * 100,
            'sum_after'   => $price * 100,
            'source_id'   => null,
            'currency_id' => \common\models\avatar\Currency::RUB,
            'class_id'    => $bm->id,
            'config_id'   => null,
            'success_url' => 'https://topmate.one/cabinet-wallet/input-rub?id=',
        ]);
        $room = ChatRoom::add(['last_message' => time()]);

        // Добавляю заказ
        $request = Request::add([
            'created_at'  => time(),
            'billing_id'  => $b->id,
            'address'     => $this->address,
            'user_id'     => Yii::$app->user->id,
            'price'       => Currency::getAtomFromValue($price, Currency::RUB),
            'sum'         => Currency::getAtomFromValue($price, Currency::RUB),
            'currency_id' => \common\models\avatar\Currency::RUB,
            'chat_id'     => $room->id,
            'comment'     => $request1['comment'],
        ]);
        $b->success_url = Url::to('/cabinet-shop-requests/view?id=' . $request->id, true);
        $b->save();

        // Добавляю товары
        foreach (Basket::get() as $p) {
            RequestProduct::add([
                'request_id' => $request->id,
                'product_id' => $p['product']['id'],
                'count'      => $p['count'],
            ]);
        }

        // Удаляю товары из корзины
        Basket::clear();

        return ['id' => $b->id];
    }
}
