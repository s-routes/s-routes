<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\models\koop\Kooperative;
use common\models\RequestTokenCreate;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class CabinetTelegramNotification extends \iAvatar777\services\FormAjax\Model
{
    /** @var  int */
    public $open;
    public $confirm;
    public $arbitr;
    public $cancel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['open', 'integer'],
            ['confirm', 'integer'],
            ['arbitr', 'integer'],
            ['cancel', 'integer'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'open' => [
                'class'    => '\common\widgets\CheckBox2\CheckBox',
            ],
            'confirm' => [
                'class'    => '\common\widgets\CheckBox2\CheckBox',
            ],
            'arbitr' => [
                'class'    => '\common\widgets\CheckBox2\CheckBox',
            ],
            'cancel' => [
                'class'    => '\common\widgets\CheckBox2\CheckBox',
            ],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'open'    => 'Сделка открыта',
            'confirm' => 'Сделка подтверждена',
            'arbitr'  => 'Вызван арбитр',
            'cancel'  => 'Сделка отменена',
        ];
    }

    /**
     * @return string идентификатор транзакции
     */
    public function action()
    {
        return 1;
    }
}
