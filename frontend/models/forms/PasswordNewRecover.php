<?php

namespace avatar\models\forms;

use app\models\User;
use avatar\models\UserRecover;
use common\models\avatar\UserBill;
use common\models\UserAvatar;
use common\models\UserSeed;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;

/**
 *
 */
class PasswordNewRecover extends Model
{
    public $password1;
    public $password2;
    public $code;

    public $seeds;

    /** @var  \common\models\UserAvatar */
    public $_user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                [
                    'password1',
                    'password2'
                ],
                'required',
                'message' => 'Это поле должно быть заполнено обязательно'
            ],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => 'Пароли должны совпадать'],
            ['code', 'string'],
            ['code', 'validateCode'], // устанавливает $_user
        ];
    }

    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $row = UserRecover::findOne(['code' => $this->code]);
            if (is_null($row)) {
                $this->addError($attribute, 'Не верный code');
                return;
            }
            try {
                $user = UserAvatar::findOne($row->parent_id);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден пользователь');
                return;
            }
            $this->_user = $user;
        }
    }

    /**
     * Устанавливает
     *
     * @param  \common\models\UserAvatar
     *
     * @return boolean
     *
     * @throws \cs\web\Exception
     */
    public function action(\common\models\UserAvatar $user)
    {
        $user->setPassword($this->password1);
        $user->email_is_confirm = 1;

        $user->save();

        return true;
    }
}
