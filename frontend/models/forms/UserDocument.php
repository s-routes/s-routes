<?php

namespace avatar\models\forms;

use common\models\avatar\UserBill;
use common\payment\BitCoinBlockTrailPayment;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * @property int     id
 * @property int     user_id
 * @property string  name
 * @property string  txid
 * @property string  file
 * @property string  hash
 * @property string  data
 * @property string  link
 * @property int     created_at
 *
 */
class UserDocument extends FormActiveRecord
{
    public static function tableName()
    {
        return 'user_documents';
    }

    public function formAttributes()
    {
        return [
            [
                'file',
                'Файл',
                0,
                '\common\widgets\DocumentUpload\Validator',
                'widget' => [
                    '\common\widgets\DocumentUpload\DocumentUpload',
                ],
            ],
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'link',
                'Ссылка',
                0,
                'string',
                ['max' => 255],
            ],
            [
                'data',
                'Данные',
                0,
                'string',
                ['max' => 255*255],
            ],
        ];
    }
}