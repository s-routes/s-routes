<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 *
 */
class AdminBlockAjax extends \yii\base\Model
{
    /**
     * @var integer
     */
    public $value;


    public function rules()
    {
        return [
            ['value', 'integer']
        ];
    }
    public function action()
    {
        if ($this->value == 1) {
            $file = '@frontend/config/catchAll.true.php';
        } else {
            $file = '@frontend/config/catchAll.false.php';
        }
        $dest = Yii::getAlias('@frontend/config/catchAll.php');
        $dest2 = Yii::getAlias('@shop/config/catchAll.php');
        $content = file_get_contents(Yii::getAlias($file));
        file_put_contents($dest, $content);
        file_put_contents($dest2, $content);

        return 1;
    }
}
