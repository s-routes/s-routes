<?php

namespace avatar\models\forms;

use app\models\User;
use avatar\models\UserRecover;
use common\models\UserAvatar;
use cs\Application;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class PasswordRecoverGoogle extends Model
{
    public $code;
    /** @var  \common\models\UserAvatar */
    public $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['code', 'required', 'message' => 'Это поле должно быть заполнено обязательно'],
            ['code', 'string'],
            ['code', 'validateCode']
        ];
    }

    public function validateCode($a, $p)
    {
        if (!$this->hasErrors()) {
            $email = \Yii::$app->session->get('PasswordRecover');
            $user = UserAvatar::findOne(['email' => $email]);
            $this->user = $user;

            if (!$user->validateGoogleAuthCode($this->code)) {
                $this->addError('code', Yii::t('c.HS07bGa1P0', 'Код не верный'));
                return;
            }
        }
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string $email the target email address
     *
     * @return boolean whether the model passes validation
     *
     * @throws \cs\web\Exception
     */
    public function send()
    {
        $user = $this->user;
        $userRecover = UserRecover::add($user->id);

        $ret = \cs\Application::mail($user->email, 'Восстановление пароля', 'password_recover', [
            'user'     => $user,
            'url'      => Url::to([
                'auth/password-recover-activate',
                'code' => $userRecover->code
            ], true),
            'datetime' => Yii::$app->formatter->asDatetime($userRecover->date_finish),
        ]);

        // Уведомляю клиента по телеграму
        /** @var UserAvatar $user */
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Создана заявка на смену пароля для E-mail {email}', ['email' => $user->email], $language)]);
        }

        // Уведомляю админа по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_admin');
        foreach ($userList as $uid) {
            $user2 = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user2->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user2->language)) ? 'ru' : $user2->language;
                $telegram->sendMessage(['chat_id' => $user2->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Создана заявка на смену пароля для E-mail {email}', ['email' => $user->email], $language)]);
            }
        }

        return $ret;
    }
}
