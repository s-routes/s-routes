<?php
namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\piramida\Wallet;
use common\models\ReferalTransaction;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 * COIN > NEIRO
 */
class ConvertNeiroBinance extends Model
{
    /** @var  \common\models\avatar\Currency */
    public $currency;

    public $amount;

    private $id;

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'validate12Usd'],
            ['amount', 'tryBinance'],
        ];
    }

    public function validate12Usd($a, $v)
    {
        if (!$this->hasErrors()) {
            $priceUsd = $this->currency->price_usd;
            $p = $this->amount * $priceUsd;
            if ($p < 12) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT. Сейчас у вас заказ ={sum} USDТ', ['sum' => Yii::$app->formatter->asDecimal($p, 2)]));
                return;
            }
        }
    }

    public function validateAmount($a, $v)
    {
        if (!$this->hasErrors()) {
            $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

            // Забираю монеты
            $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletFrom = $dataTo['wallet'];
            $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
            $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
            $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));

            if ($amountFrom > $walletFrom->amount) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }

    public function tryBinance($a, $v)
    {
        // Проверяю баланс на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        // проверяю балансы
        $data = $provider->_callApiKeyAndSigned('/api/v3/account', []);
        $rows = ArrayHelper::map($data['balances'], 'asset', 'free');

        try {
            // Если баланс на бирже меньше чем указал пользователь?
            if ($rows[$this->currency->code] < $this->amount) {
                // создаю заявку на конвертацию и забираю монеты

                $id = $this->binanceNo($this->amount, $rows[$this->currency->code]);
            } else {
                $id = $this->binanceYes();
            }
        } catch (\Exception $e) {
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Ошибка в Binance convert: {text}', ['text' => $e->getMessage()]
            ));
            $id = null;
        }

        $this->id = $id;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        return $this->id;
    }

    private function binanceYes()
    {
        $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

        // Создаю ордер на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        /**
        'symbol' => 'BTCUSDT'
        'orderId' => 78364
        'orderListId' => -1
        'clientOrderId' => 'ikAmswABSyH35iAmNG3ama'
        'transactTime' => 1611765998657
        'price' => '0.00000000'
        'origQty' => '0.01000000'
        'executedQty' => '0.00841200'
        'cummulativeQuoteQty' => '192.10515000'
        'status' => 'EXPIRED'
        'timeInForce' => 'GTC'
        'type' => 'MARKET'
        'side' => 'SELL'
        'fills' => [
        0 => [
        'price' => '32250.00000000'
        'qty' => '0.00012900'
        'commission' => '0.00000000'
        'commissionAsset' => 'USDT'
        'tradeId' => 4980
        ]
        ]
        ]
         */

        try {
            $data = $provider->_callApiKeyPostAndSigned('/api/v3/order', [
                'symbol'           => $this->currency->code . 'USDT',
                'side'             => 'sell', // продаю
                'type'             => 'MARKET',
                'quantity'         => $this->amount,
                'newOrderRespType' => 'FULL',
            ]);
        } catch (\Exception $e) {
            $this->addError('amount', 'Конвертация не выполнена. Задержка в получении данных от третьей стороны. Ожидайте завершения процесса.');
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Ошибка в Binance convert: {text}', ['text' => $e->getMessage()]
            ));
            throw $e;
        }

        $cIO = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $cInt = \common\models\piramida\Currency::findOne($cIO->currency_int_id);
        $data2 = UserBill::getInternalCurrencyWallet($cInt->id);
        $BinanceOrder = \common\models\BinanceOrder::add([
            'binance_id'       => $data['clientOrderId'],
            'binance_order_id' => $data['orderId'],
            'user_id'          => Yii::$app->user->id,
            'billing_id'       => $data2['billing']['id'],
            'amount'           => (int)($this->amount * pow(10, $cInt->decimals)),
            'binance_return'   => Json::encode($data),
            'currency_to'      => \common\models\avatar\Currency::NEIRO,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move($walletTo, $amountFrom, Json::encode([
            'Конвертация в NEIRO bid={id}',
            ['id' => $BinanceOrder->id],
        ]));

        $BinanceOrder->tx_from_id = $t_from->id;
        $BinanceOrder->save();

        /** Если заявка выполнена? */
        if ($data['status'] == 'FILLED') {

            // возвращаю деньги
            $amount1 = $data['cummulativeQuoteQty'];

            // Отдаю монеты
            $cInt_to = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
            $CIO_to = CurrencyIO::findOne(['currency_int_id' => $cInt_to->id]);
            $toCurrencyExt = \common\models\avatar\Currency::findOne($CIO_to->currency_ext_id);
            $walletOut = Wallet::findOne($CIO_to->main_wallet);
            $dataTo = UserBill::getInternalCurrencyWallet($cInt_to->id, Yii::$app->user->id);

            // получаю комиссию системы
            $dataJson = Config::get('NeironConvertParams');
            $data = Json::decode($dataJson);
            $o = $this->getId($data, $CIOfrom->currency_ext_id);
            $amount_m_to = $amount1 * (1-($o['in'] / 100)) / $toCurrencyExt->price_usd;
            $amountTo = bcmul($amount_m_to, bcpow(10, $cInt_to->decimals));

            // Вычисляю тип транзакции
            $type = \common\models\NeironTransaction::TYPE_CONVERT;
            if (in_array($CIOfrom->currency_ext_id, [
                \common\models\avatar\Currency::BTC,
                \common\models\avatar\Currency::LTC,
                \common\models\avatar\Currency::DASH,
                \common\models\avatar\Currency::ETH,
                \common\models\avatar\Currency::TRX,
                \common\models\avatar\Currency::BNB,
            ])) {
                $type = \common\models\NeironTransaction::TYPE_CONVERT_GOOD;
            }

            $t_to = $walletOut->move2(
                $dataTo['wallet'],
                $amountTo,
                Json::encode([
                    'Конвертация в NEIRO bid={id}',
                    ['id' => $BinanceOrder->id],
                ]),
                $type
            );

            // Устанавливаю статус
            $BinanceOrder->tx_to_id = $t_to['transaction']['id'];
            $BinanceOrder->amount_to = $amountTo;
            $BinanceOrder->status = $BinanceOrder::STATUS_FILLED;
            $BinanceOrder->save();

            // Делаю запись о типе транзакции если это NERON
            NeironTransaction::add([
                'transaction_id' => $t_to['transaction']['id'],
                'operation_id'   => $t_to['operation_add']['id'],
                'type_id'        => $type,
            ]);

            // Делаю реферальные начисления
            $this->referalProgram($BinanceOrder->user_id, $BinanceOrder);

            // Уведомляю пользователя по телеграму
            $user = UserAvatar::findOne($BinanceOrder->user_id);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        'Конвертация #{id} выполнена. Получено {sum} {code}',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => $amount_m_to,
                            'code' => $cInt_to->code,
                        ],
                        $language
                    ),
                ]);
            }

            // Уведомляю бухгалтера по телеграму
            $this->telegram(Yii::t(
                'c.LSZBX1ce2W',
                'Конвертация #{id} выполнена. Получено {sum} {code}',
                [
                    'id'   => $BinanceOrder->id,
                    'sum'  => $amount_m_to,
                    'code' => $cInt_to->code,
                ]
            ));
        }

        return $BinanceOrder->id;
    }

    private function telegram($message)
    {
        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => $message,
                ]);
            }
        }
    }

    /**
     * @param string $amountUserForm - указал пользователь в форме для конвертации
     * @param string $amountBinance - на бирже сейчас
     * @return int
     */
    private function binanceNo($amountUserForm, $amountBinance)
    {
        $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

        $cIO = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $cInt = \common\models\piramida\Currency::findOne($cIO->currency_int_id);
        $data2 = UserBill::getInternalCurrencyWallet($cInt->id);
        $BinanceOrder = \common\models\BinanceOrder::add([
            'user_id'          => Yii::$app->user->id,
            'billing_id'       => $data2['billing']['id'],
            'amount'           => (int)($this->amount * pow(10, $cInt->decimals)),
            'currency_to'      => \common\models\avatar\Currency::NEIRO,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move(
            $walletTo,
            $amountFrom,
            Json::encode([
                'Конвертация в NEIRO bid={id}',
                ['id' => $BinanceOrder->id],
            ])
        );

        $BinanceOrder->tx_from_id = $t_from->id;
        $BinanceOrder->save();


        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        'Конвертация #{id} не выполнена. Админ! На Бинансе не хватает средств в размере {sum} {code}. Требуется пополнение.',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => bcsub($amountUserForm, $amountBinance),
                            'code' => $cInt->code,
                        ],
                        $language
                    ),
                ]);
            }
        }

        return $BinanceOrder->id;
    }

    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'  => $id,
            'in'  => 0,
            'out' => 0,
        ];
    }

    /**
     * @param int $user_id
     * @param \common\models\BinanceOrder $convert
     */
    function referalProgram($user_id, $convert)
    {
        $cio = CurrencyIO::findOne(['currency_int_id' => \common\models\piramida\Currency::NEIRO]);
        $walletOut = Wallet::findOne($cio->main_wallet);

        /** @var \common\models\UserPartner $partner */
        $partner = \common\models\UserPartner::find()->where(['school_id' => 1, 'user_id' => $user_id])->one();
        $c = 0;
        $level = [
            0 => 5,
            1 => 3,
            2 => 1,
            3 => 3,
            4 => 5,
        ];
        while ($c < 5) {
            if (is_null($partner)) return;
            $uid = $partner->parent_id;
            $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::NEIRO, $uid);

            // Если у пользователя больше 1000 монет на счету то делаю рефералку
            if ($data['wallet']['amount'] > \common\models\piramida\Currency::getAtomFromValue(1000, \common\models\piramida\Currency::NEIRO)) {
                $amountRef = (int) ($convert->amount_to * ($level[$c] / 100));

                // Делаю транзакцию пользователю
                $t = $walletOut->move2(
                    $data['wallet'],
                    $amountRef,
                    Json::encode([
                        'Реферальные начисления за конвертацию bid={cid} от uid={from} к uid={to}',
                        [
                            'cid'  => $convert->id,
                            'from' => $user_id,
                            'to'   => $uid,
                        ]
                    ]),
                    NeironTransaction::TYPE_REFERAL_CONVERT_BINANCE
                );

                // Делаю запись о типе транзакции если это NERON
                NeironTransaction::add([
                    'transaction_id' => $t['transaction']['id'],
                    'operation_id'   => $t['operation_add']['id'],
                    'type_id'        => NeironTransaction::TYPE_REFERAL_CONVERT_BINANCE,
                ]);

                // Сделать запись о реферальном начислении
                ReferalTransaction::add([
                    'request_id'     => $convert->id,
                    'type_id'        => ReferalTransaction::TYPE_CONVERT_BINANCE,
                    'currency_id'    => \common\models\piramida\Currency::NEIRO,
                    'level'          => $c + 1,
                    'from_uid'       => $user_id,
                    'to_uid'         => $uid,
                    'to_wid'         => $data['wallet']['id'],
                    'amount'         => $amountRef,
                    'transaction_id' => $t['transaction']['id'],
                ]);
            }

            $partner = \common\models\UserPartner::find()->where(['school_id' => 1, 'user_id' => $partner['parent_id']])->one();

            $c++;
        }
    }
}
