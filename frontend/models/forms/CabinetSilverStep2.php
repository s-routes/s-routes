<?php
namespace avatar\models\forms;


use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Arbitrator;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\helpers\Json;

class CabinetSilverStep2 extends Model
{
    /** @var string  */
    public $address;

    /** @var int  */
    public $dostavka;

    public function rules()
    {
        return [
            ['address', 'required'],
            ['address', 'string'],

            ['dostavka', 'required'],
            ['dostavka', 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'address'   => 'Адрес',
            'dostavka'  => 'Доставка',
        ];
    }

    public function attributeWidgets()
    {
        return [
            'dostavka' => '\avatar\widgets\Dostavka',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $request1 = \Yii::$app->session->get('shop-request2');
        $request1['address'] = $this->address;
        $request1['dostavka'] = $this->dostavka;
        \Yii::$app->session->set('shop-request2', $request1);

        return 1;
    }
}