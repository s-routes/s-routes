<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * name{id}_in
 * name{id}_out
 */
class AdminComission extends Model
{
    public static $currencyList = [
        [
            'id' => 'BTC/BTC',
        ],
        [
            'id' => 'ETH/ETH',
        ],
        [
            'id' => 'USDT/ETH',
        ],
        [
            'id' => 'USDT/TRX',
        ],
        [
            'id' => 'LTC/LTC',
        ],
        [
            'id' => 'TRX/TRX',
        ],
        [
            'id' => 'DASH/DASH',
        ],
        [
            'id' => 'BNB/ETH',
        ],
        [
            'id' => 'BNB/BSC',
        ],
        [
            'id' => 'DAI/ETH',
        ],
        [
            'id' => 'DAI/BSC',
        ],
        [
            'id' => 'DOGE/DOGE',
        ],
        [
            'id' => 'DOGE/BSC',
        ],
    ];

    /**
     * @var array
     * [
     *  [
     *      id
     *      min
     *      out
     *  ]
     * ]
     */
    public $params;

    public function init()
    {
        $params = Config::get('WithdrawFeePersent');
        if ($params === false) {
            $params = [];
        } else {
            // проверяю на наличие новых параметров, валют и добавляю
            $params = Json::decode($params);
            $currencyList = self::$currencyList;
            $currencyList = ArrayHelper::getColumn($currencyList,'id');
            foreach ($currencyList as $c) {
                // есть ли $c в $params
                if (!$this->isExist($c, $params)) {
                    $id = str_replace('/', '_', $c);
                    $params[] = [
                        'id' => $id,
                        'out' => 0,
                        'min' => 0,
                    ];
                }
            }
        }

        $this->params = $params;
    }

    public function isExist($c, $params)
    {
        $id = str_replace('/', '_', $c);
        foreach ($params as $p) {
            if ($p['id'] == $id) return true;
        }

        return false;
    }

    public function rules()
    {
        $currencyList = self::$currencyList;

        $currencyList = ArrayHelper::getColumn($currencyList,'id');
        $rules = [];
        $fields = [];

        foreach ($currencyList as $cid) {
            $fields[] = 'name' . str_replace('/', '_', $cid) . '_out';
            $fields[] = 'name' . str_replace('/', '_', $cid) . '_min';
        }
        $rules[] = [$fields, 'double', 'min' => 0];
        return $rules;
    }


    public function __get($name)
    {
        if ($name == 'attributes') {
            $attributes = [];
            foreach ($this->params as $p) {
                $attributes['name'.$p['id'].'_out'] = $p['out'];
                $attributes['name'.$p['id'].'_min'] = $p['min'];
            }
            return $attributes;
        }
        if ($name == 'errors') {
            return parent::__get($name);
        }
        $arr = explode('_', $name);
        try {
            $param = $arr[2];
        } catch (\Exception $e) {
            return parent::__get($name);
        }

        $id = substr($arr[0], 4) . '_' .$arr[1];
        foreach ($this->params as $p) {
            if ($id == $p['id']) {
                if (!isset($p[$param])) {
                    return 0;
                }
                return $p[$param];
            }
        }

        return 0;
    }

    public function __set($name, $value)
    {
        $arr = explode('_', $name);
        $param = $arr[2];
        $id = substr($arr[0], 4) . '_' .$arr[1];
        $c = count ($this->params);
        for ($i = 0; $i < $c; $i++) {
            if ($id == $this->params[$i]['id']) {
                $this->params[$i][$param] = $value;
                return true;
            }
        }
        if ($param == 'out') {
            $param2 = 'min';
        } else {
            $param2 = 'out';
        }

        $this->params[] = [
            'id'    => $id,
            $param  => $value,
            $param2 => 0,
        ];

        return true;
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        Config::set('WithdrawFeePersent', Json::encode($this->params));

        return 1;
    }
}
