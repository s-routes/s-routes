<?php

namespace avatar\models\forms;

use common\models\Config;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminTrustAdd extends \iAvatar777\services\FormAjax\Model
{
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $ids = \common\models\Config::get('trusted-users');
        if (\cs\Application::isEmpty($ids)) $ids = [];
        else $ids = Json::decode($ids);
        $ids[] = $this->id;

        Config::set('trusted-users', Json::encode($ids));

        return 1;
    }

}
