<?php
namespace avatar\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\ShopReferalTransaction;
use common\models\ShopUserPartner;
use common\models\UserAvatar;
use common\models\UserBill2;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\IdentityInterface;

/**
 *
 * COIN > MARKET
 */
class Convert3 extends Model
{
    /** @var  \common\models\avatar\Currency */
    public $currency;

    public $amount;

    public function rules()
    {
        return [
            ['amount', 'required'],
            ['amount', 'double'],
            ['amount', 'validateAmount'],
            ['amount', 'validate12Usd'],
            ['amount', 'validatePrizm'],
        ];
    }

    public function validateAmount($a, $v)
    {
        if (!$this->hasErrors()) {
            $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

            // Забираю монеты
            $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletFrom = $dataTo['wallet'];
            $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
            $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
            $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));

            if ($amountFrom > $walletFrom->amount) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }


    public function validate12Usd($a, $v)
    {
        if (!$this->hasErrors()) {
            $priceUsd = $this->currency->price_usd;
            $p = $this->amount * $priceUsd;
            if ($p < 12) {
                $this->addError($a, Yii::t('c.WxnFTlXT5m', 'Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT. Сейчас у вас заказ ={sum} USDТ', ['sum' => Yii::$app->formatter->asDecimal($p, 2)]));
                return;
            }
        }
    }

    public function validatePrizm($a, $v)
    {
        if (!$this->hasErrors()) {
            if ($this->currency->id == \common\models\avatar\Currency::PZM) {
                $c = \common\models\Config::get('pzm_exchange_limit');
                $v = \common\models\piramida\Currency::getAtomFromValue($this->amount, \common\models\piramida\Currency::PZM);
                if ($v > $c) {
                    $this->addError($a, 'Введенное Вами количество монет превышает общий доступный для конвертации объем актива.');
                    return;
                }
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $cLink = CurrencyLink::findOne(['currency_ext_id' => $this->currency->id]);

        $operetion = ConvertOperation::add([
            'wallet_from_id'   => 1,
        ]);

        // Забираю монеты
        $dataTo = UserBill::getInternalCurrencyWallet($cLink->currency_int_id, Yii::$app->user->id);

        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $dataTo['wallet'];
        $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $this->currency->id]);
        $walletTo = Wallet::findOne($CIOfrom->main_wallet);
        $currencyFromInt = \common\models\piramida\Currency::findOne($CIOfrom->currency_int_id);
        $amountFrom = bcmul($this->amount, bcpow(10, $currencyFromInt->decimals));
        $t_from = $walletFrom->move($walletTo, $amountFrom, Json::encode([
            'Конвертация {from} в MARKET id={id}',
            [
                'from' => $this->currency->code,
                'id'   => $operetion->id,
            ],
        ]));
        $fromCurrencyExt = $this->currency;

        // Отдаю монеты
        $cInt = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::MARKET);
        $CIO = CurrencyIO::findOne(['currency_int_id' => $cInt->id]);
        $CIO_to = $CIO;
        $toCurrencyExt = \common\models\avatar\Currency::findOne($CIO->currency_ext_id);
        $walletOut = Wallet::findOne($CIO->main_wallet);
        $dataTo = UserBill::getInternalCurrencyWallet($cInt->id, Yii::$app->user->id);

        // получаю коммию системы
        $dataJson = Config::get('MarketConvertParams');
        $data = Json::decode($dataJson);
        $o = $this->getId($data, $CIOfrom->currency_ext_id);

        // Определяю кол-во монет
        $referal = 2.5;
        $kurs = $fromCurrencyExt->price_usd * (1-(($o['in']) / 100)) / $toCurrencyExt->price_usd;
        $amount_m = $this->amount * $kurs;
        $amount_m_to = $amount_m;
        $amount = bcmul($amount_m, bcpow(10, $cInt->decimals));

        $kurs2 = $fromCurrencyExt->price_usd / $toCurrencyExt->price_usd;
        $amount100 = $this->amount * $kurs2;

        /** @var \common\models\piramida\Wallet $walletTo */
        $walletTo = $dataTo['wallet'];
        $t_to = $walletOut->move($walletTo, $amount, Json::encode([
            'Конвертация {from} в MARKET id={id}',
            [
                'from' => $this->currency->code,
                'id'   => $operetion->id,
            ],
        ]));

        // раскидываю рефералку
        $this->referal($operetion->id, $walletOut, Yii::$app->user->id, Currency::getAtomFromValue($amount100, $cInt->id));

        $operetion->user_id = Yii::$app->user->id;
        $operetion->wallet_from_id = $walletFrom->id;
        $operetion->wallet_to_id = $walletTo->id;
        $operetion->tx_from_id = $t_from->id;
        $operetion->tx_to_id = $t_to->id;
        $operetion->kurs = (int) ($kurs * 1000000);
        $operetion->amount = $amountFrom;
        $operetion->amount_to = $amount;
        $operetion->from_currency_id = $CIOfrom->currency_int_id;
        $operetion->to_currency_id = $CIO->currency_int_id;

        $operetion->from_price_usd = $fromCurrencyExt->price_usd;
        $operetion->to_price_usd = $toCurrencyExt->price_usd;

        $operetion->save();

        // вычитаю лимит
        if ($this->currency->id == \common\models\avatar\Currency::PZM) {
            $c = \common\models\Config::get('pzm_exchange_limit');
            $v = \common\models\piramida\Currency::getAtomFromValue($this->amount, \common\models\piramida\Currency::PZM);
            $c = $c - $v;
            \common\models\Config::set('pzm_exchange_limit', $c);
        }

        // Уведомляю бухгалтера по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
        foreach ($userList as $uid) {
            try {
                $user = \common\models\UserAvatar::findOne($uid);
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

                    try {
                        $userBill = \common\models\avatar\UserBill::findOne(['address' => $operetion->wallet_from_id]);
                    } catch (\Exception $e) {
                        return '';
                    }
                    $taskUser = \common\models\UserAvatar::findOne($userBill->user_id);

                    $from = Yii::$app->formatter->asDecimal($this->amount, $currencyFromInt->decimals) . ' ' . $currencyFromInt->code;
                    $currencyToInt = \common\models\piramida\Currency::findOne($CIO_to->currency_int_id);
                    $to = Yii::$app->formatter->asDecimal($amount_m_to, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'User-ID {user}. Проведена конвертация {from} в {to}', ['user' => $taskUser->getAddress(), 'from' => $from, 'to' => $to], $language)]);
                }
            }  catch (\Exception $e) {

            }
        }

        // Уведомляю клиента по телеграму
        $user = UserAvatar::findOne(Yii::$app->user->id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

            $from = Yii::$app->formatter->asDecimal($this->amount, $currencyFromInt->decimals) . ' ' . $currencyFromInt->code;
            $currencyToInt = Currency::findOne($CIO_to->currency_int_id);
            $to = Yii::$app->formatter->asDecimal($amount_m_to, $currencyToInt->decimals) . ' ' . $currencyToInt->code;

            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Проведена конвертация {from} в {to}', ['from' => $from, 'to' => $to], $language)]);
        }


        return $operetion->id;
    }

    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id' => $id,
            'in' => 0,
            'out' => 0,
        ];
    }

    /**
     * раскидывает рефералку
     * @param \common\models\piramida\Wallet $walletOut
     */
    public function referal($oid, $walletOut, $uid, $amount)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $cMar = Currency::findOne(['code' => 'MAR']);
        $levels = Yii::$app->params['shop-referal-program']['level-list1'];
        $uCurrent = $uid;
        $u1 = UserAvatar::findOne($uid);
        for ($i = 0; $i < 3; $i++) {
            $s = ShopUserPartner::findOne(['user_id' => $uCurrent]);
            if (is_null($s)) break;
            $parent_id = $s->parent_id;
            $u = \common\models\UserAvatar::findOne($parent_id);

            // Если куплен пакет то user.market_packet > 0 and user.market_till < time() and wallet.amount > 0
            $data2 = UserBill2::findOne(['user_id' => $u->id, 'currency_id' => $cMar->id]);
            $w1 = Wallet::findOne($data2->wallet_id);
            if ($u['market_till'] > 0) {
                if (($u['market_packet'] > 0) and ($u['market_till'] > time()) and ($w1->amount > 0)) {
                    $dataTo = UserBill::getInternalCurrencyWallet($walletOut->currency_id, $u->id);
                    $w = $dataTo['wallet'];
                    $a = (int) ($amount * ($levels[$i]/100));
                    $t_to = $walletOut->move2($w, $a, Json::encode([
                        'Реферальное начисление от {from} к {to}',
                        [
                            'from' => $uid,
                            'to'   => $u->id,
                        ],
                    ]));

                    $ShopReferalTransaction = ShopReferalTransaction::add([
                        'request_id'     => $oid,
                        'type_id'        => 1,
                        'currency_id'    => $walletOut->currency_id,
                        'level'          => $i + 1,
                        'from_uid'       => $uid,
                        'to_uid'         => $u->id,
                        'amount'         => $a,
                        'transaction_id' => $t_to['transaction']['id'],
                    ]);

                    // уведомляю по телеграму
                    $userTelegram = $u;
                    $uC = UserAvatar::findOne($u1);
                    if (!Application::isEmpty($userTelegram->telegram_shop_chat_id)) {
                        try {
                            $telegram->sendMessage([
                                'chat_id' => $userTelegram->telegram_chat_id,
                                'text'    => sprintf('Вам начислено реферальное вознаграждение %s МБ. Ваш реферал: USER ID-%s, линия %s',
                                    Currency::getValueFromAtom($a, $walletOut->currency_id),
                                    $uC->getAddress(),
                                    $i + 1
                                ),
                            ]);
                        } catch (\Exception $e) {
                            Yii::warning($e->getMessage(), 'avatar\shop\models\forms\CabinetMarkeyBuy::referal()');
                        }
                    }
                }
            }

            // текущим становится $parent_id
            $uCurrent = $parent_id;
        }
    }
}
