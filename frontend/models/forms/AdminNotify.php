<?php

namespace avatar\models\forms;

use common\models\Config;
use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminNotify extends \iAvatar777\services\FormAjax\Model
{
    /** @var string */
    public $start_date;
    public $start_time;
    public $finish_date;
    public $finish_time;
    public $text;

    public function init()
    {
        $adminNotifyData = Config::get('admin-notify');
        $isData = false;
        if ($adminNotifyData !== false) {
            if (!is_null($adminNotifyData)) {
                if ($adminNotifyData != '') {
                    $isData = true;
                }
            }
        }

        if ($isData) {
            $data = Json::decode($adminNotifyData);
        } else {
            $data = [
                'start_date'  => '',
                'start_time'  => '',
                'finish_date' => '',
                'finish_time' => '',
                'text'        => '',
            ];
        }

        $this->start_date = $data['start_date'];
        $this->start_time = $data['start_time'];
        $this->finish_date = $data['finish_date'];
        $this->finish_time = $data['finish_time'];
        $this->text = $data['text'];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['start_date', 'required'],
            ['start_date', 'string'],

            ['start_time', 'required'],
            ['start_time', 'string'],

            ['finish_date', 'required'],
            ['finish_date', 'string'],

            ['finish_time', 'required'],
            ['finish_time', 'string'],

            ['text', 'required'],
            ['text', 'string'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'start_date'  => 'Старт/ дата',
            'start_time'  => 'Старт/ время',
            'finish_date' => 'Финиш/ дата',
            'finish_time' => 'Финиш/ время',
            'text'        => 'Текст',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Config::set('admin-notify', Json::encode([
            'start_date'  => $this->start_date,
            'start_time'  => $this->start_time,
            'finish_date' => $this->finish_date,
            'finish_time' => $this->finish_time,
            'text'   => $this->text,
        ]));

        return 1;
    }

}
