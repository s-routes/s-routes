<?php

namespace avatar\models\forms;

use common\models\CurrencyIO;
use common\models\CurrencyIoModification;
use common\models\NeironTransaction;
use common\models\SendLetter;
use common\models\TaskInput;
use common\models\TaskOutputHistory;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class AdminBuhDonePzmAjax extends Model
{
    public $id;

    /** @var \common\models\TaskOutput */
    public $task;

    public $data;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['id', 'required'],
            ['id', 'integer'],

            ['id', 'setRequest'],
            ['id', 'tryPrizmApi'],
        ];

        return $rules;
    }

    /**
     * @param $a
     * @param $p
     */
    public function setRequest($a, $p)
    {
        if (!$this->hasErrors()) {
            $order = \common\models\TaskOutput::findOne($this->id);
            if (is_null($order)) {
                $this->addError($a, 'Заявка не найдена');
                return;
            }
            $this->task = $order;
        }
    }


    /**
     * @param $a
     * @param $p
     */
    public function tryPrizmApi($a, $p)
    {
        if (!$this->hasErrors()) {
            $task = $this->task;

            /** @var \common\services\PrizmApi $PrizmApi */
            $PrizmApi = Yii::$app->PrizmApi;

            $amountNQT = (int)(\common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, \common\models\piramida\Currency::PZM) * 100);

            try {
                $data = $PrizmApi->post('withdraw', [
                    'amountNQT'             => $amountNQT ,
                    'recipient'             => $task->account,
                    'recipientPublicKey'    => $task->prizm_key,
                ]);
                if (!isset($data['transaction'])) {
                    Yii::error(VarDumper::dumpAsString($data), 'avatar\models\forms\AdminBuhDonePzmAjax');
                    \Neiron::$app->sendTelegramAdminSms('Ошибка PrizmApi: ' . VarDumper::dumpAsString($data));
                    $this->addError($a, 'Ошибка PrizmApi: ' . VarDumper::dumpAsString($data));
                    return;
                }
                $this->data = $data;

            } catch (\Exception $e) {
                $this->addError($a, 'Ошибка PrizmApi: ' . $e->getMessage());
                return;
            }
        }
    }

    public function action()
    {
        $task = $this->task;
        $cExt = \common\models\avatar\Currency::findOne($task->currency_id);
        $data = $this->data;

        // забираю монеты
        $cio = CurrencyIO::findOne($task->currency_io_id);
        $wallet = \common\models\piramida\Wallet::findOne($cio->block_wallet);

        $walletToId = $cio->main_wallet;
        $walletTo = \common\models\piramida\Wallet::findOne($walletToId);
        $t = $wallet->move2(
            $walletTo,
            $task->amount,
            Json::encode([
                Yii::t('c.GGWWLBoXB4', 'Вывод средств по заявке id={id}'),
                ['id' => $task->id],
                ['type_id' => 1],
            ])
        );

        $task->status = \common\models\TaskOutput::STATUS_DONE;
        $task->txid = $data['transaction'];
        $task->txid_internal = $t['transaction']['id'];
        $task->buh_user_id = Yii::$app->user->id;
        $task->save();

        $value = \common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, $cio->currency_int_id);
        $text = 'Заявка #' . $this->task->id . ' на вывод ' . $value . ' ' . $cExt->code . ' выполнена' . "\n" . 'Txid=' . $task->txid;

        // Добавляю историю заявки
        $i = TaskOutputHistory::add([
            'task_id' => $task->id,
            'comment' => $text,
        ]);

        // Уведомляю пользователю по телеграму
        $user = UserAvatar::findOne($task->user_id);
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            try {
                $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
            } catch (\Exception $e) {

            }
        }

        // Уведомляю админа и бухгалтера по телеграму
        $userListBuh = Yii::$app->authManager->getUserIdsByRole('role_buh');
        $userListAdmin = Yii::$app->authManager->getUserIdsByRole('role_admin');
        $userList = ArrayHelper::merge($userListBuh, $userListAdmin);
        $userList = array_unique($userList);
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                try {
                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
                } catch (\Exception $e) {

                }
            }
        }

        return ['id' => $task->binance_id];
    }
}
