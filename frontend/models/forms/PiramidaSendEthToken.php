<?php

namespace avatar\models\forms;

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\Card;
use common\models\Token;
use common\models\UserAvatar;
use common\services\Security;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use cs\Widget\FileUpload3\FileUpload;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * ContactForm is the model behind the contact form.
 */
class PiramidaSendEthToken extends Model
{
    /** @var string можно задать адрес, телефон, почта, номер карты с пробелами */
    public $address;

    /** @var \common\models\avatar\UserBill счет отправителя  */
    public $billingFrom;

    /** @var int идентификатор валюты, валюта в которой пользователь указывал переводимые средства */
    public $currency;

    /** @var  \common\models\avatar\Currency валюта в которой пользователь указывал переводимые средства устанавливается в normalizeCurrency */
    public $_currencyObject;

    /** @var string пароль откабинета */
    public $password;

    /** @var string кол-во монет для отправки */
    public $amount;

    /** @var string комментарий к транзакции */
    public $comment;

    /** @var string $gasPrice */
    public $gasPrice;

    /** @var int Идентификатор счета откуда отправляются деньги */
    public $billing_id;

    private $possibleChars = [
        'a',
        'b',
        'c',
        'd',
        'e',
        'f',
        '0',
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8',
        '9',
    ];

    public function rules()
    {
        return [
            [['address', 'password', 'amount', 'billing_id'], 'required'],

            ['currency', 'normalizeCurrency'],

            ['billing_id', 'integer'],
            ['billing_id', 'validateBilling'],

            ['address', 'string'],
            ['address', 'trim'],
            ['address', 'normalizeAddress'],

            ['amount', 'normalizeAmount'],

            ['password', 'string'],
            ['password', 'validatePassword'],

            ['comment', 'string'],

            ['gasPrice', 'string'],
            ['gasPrice', 'integer', 'min' => 2, 'max' => 50],
        ];
    }

    /**
     * Преобразовывает запятую в точку если есть
     * Если есть запрещенные символы то выводит сообщение об ошибке
     * Если валюта отлична от BTC то производит конвертацию
     *
     * @param $attribute
     * @param $params
     *
     * @throws
     */
    public function normalizeAmount($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->amount = str_replace(',', '.', $this->amount);
            if (!$this->validateAmount($this->amount)) {
                $this->addError($attribute, 'Не верные символы');
                return;
            }
            // для токенов. Если указана валюта отличная от валюты токена то вызывается ошибка
            if ($this->billingFrom->currency != $this->currency) {
                $this->addError($attribute, 'Должна быть установлена валюта токена');
                return;
            }
        }
    }

    /**
     * Преобразовывает ВТС в \common\models\avatar\Currency
     *
     * @param $attribute
     * @param $params
     *
     * @throws
     */
    public function normalizeCurrency($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $c = Currency::findOne($this->currency);
            if (is_null($c)) {
                $this->addError($attribute, 'Не найдена валюта');
            }
            $this->_currencyObject = $c;
        }
    }

    private function validateAmount($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'])) return false;
        }
        return true;
    }

    /**
     * Если адрес задан как номер карты, номер телефона, или логин то приводит его к адресу по умолчанию
     *
     * @param $attribute
     * @param $params
     */
    public function normalizeAddress($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $address = $this->address;
            if (StringHelper::startsWith($address, 'bitcoin')) {
                $this->addError($attribute, 'Здесь может быть указан только Эфир кошелек');
                return;
            } else if (StringHelper::startsWith($address, 'ethereum')) {
                $address = substr($address, 9);
                $arr = explode('?', $address);
                if (count($arr) > 1) {
                    $address = $arr[0];
                }
            } else if (filter_var($address, FILTER_VALIDATE_EMAIL)) {
                // email
                $email = $address;
                try {
                    $user = UserAvatar::findByUsername($email);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найдена почта');
                    return;
                }

                // Ищу кошелек токена
                try {
                    $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => $this->billingFrom->currency, 'mark_deleted' => 0]);
                    $this->address = $billing->address;
                    return;
                } catch (\Exception $e) {
                    Yii::info('Не найден кошелек токена', 'avatar\normalizeAddress');
                }

                // Получаю кошелек эфира
                try {
                    $billing = $user->getPiramidaBilling($this->billingFrom->currency);
                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                    return;
                }
                $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $user->id);
                $billing = $tokenBilling->billing;

                $address = $billing->address;
            } else if ($this->isPhone($address)) {

                // phone
                $phone = $address;
                $phone = str_replace('+', '', $phone);
                $phone = str_replace('-', '', $phone);
                $phone = str_replace('(', '', $phone);
                $phone = str_replace(')', '', $phone);
                $phone = str_replace(' ', '', $phone);
                try {
                    $user = UserAvatar::findOne(['phone' => $phone]);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найден телефон');
                    return;
                }

                // Ищу кошелек токена
                try {
                    $billing = UserBill::findOne(['user_id' => $user->id, 'currency' => $this->billingFrom->currency, 'mark_deleted' => 0]);
                    $this->address = $billing->address;
                    return;
                } catch (\Exception $e) {
                    Yii::info('Не найден кошелек токена', 'avatar\normalizeAddress');
                }

                // Получаю кошелек эфира
                try {
                    $billing = $user->getPiramidaBilling($this->billingFrom->currency);
                } catch (\Exception $e) {
                    $this->addError($attribute, $e->getMessage());
                    return;
                }
                $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $user->id);
                $billing = $tokenBilling->billing;

                $address = $billing->address;

            } else if ($this->isCardNumber($address)) {
                $address = str_replace(' ', '', $address);
                $number = $address;

                $card = Card::findOne(['number' => $number]);
                if (is_null($card)) {
                    $this->addError($attribute, 'Не найдена такая карта');
                    return;
                }

                // пробую найти кошелек с такой же валютой на карте
                try {
                    $billing = UserBill::findOne(['card_id' => $card->id, 'currency' => $this->billingFrom->currency]);
                    $this->address = $billing->address;
                    return;
                } catch (\Exception $e) {
                }

                // пробую найти эфировский кошелек на карте
                try {
                    $billing = UserBill::findOne(['card_id' => $card->id, 'currency' => Currency::ETH]);
                    $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                    $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $billing->user_id);
                    Yii::info('$tokenBilling->id'.$tokenBilling->billing->id, 'avatar\models\forms\PiramidaSendEthToken::normalizeAddress');
                    $tokenBilling->billing->card_id = $card->id;
                    $tokenBilling->billing->save();
                    $billing = $tokenBilling->billing;
                    $this->address = $billing->address;
                    return;
                } catch (\Exception $e) {
                    // cчитаю что ошибка связана с не найденым счетом
                }

                // выясняю есть ли на карте биткойн кошелек
                try {
                    $billingBTC = UserBill::findOne(['card_id' => $card->id, 'currency' => Currency::BTC]);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'На карте нет ни ETH ни BTC счета');
                    return;
                }

                // создаю эфировский кошелек
                $walletETH = WalletETH::create('Эфир', Security::generateRandomString(), $billingBTC->user_id, UserBill::PASSWORD_TYPE_OPEN_CRYPT);
                $walletETH->billing->card_id = $card->id;
                $walletETH->billing->save();
                $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                $tokenBilling = WalletToken::createFromEth($walletETH->billing, $tokenObject, $tokenObject->getCurrency()->title, $billingBTC->user_id);
                Yii::info('$tokenBilling->id'.$tokenBilling->billing->id, 'avatar\models\forms\PiramidaSendEthToken::normalizeAddress2');
                $tokenBilling->billing->card_id = $card->id;
                $tokenBilling->billing->save();
                $billing = $tokenBilling->billing;

                $address = $billing->address;
            } else {

                // предполагаю что здесь простой адрес

                // проверяю на валидность
                // Ethereum адрес должен начинаться только с 0x
                if (!StringHelper::startsWith($this->address,'0x')) {
                    $this->addError($attribute, 'Адрес должен начинаться на 0x');
                    return;
                }

                $charList = Str::getChars(substr(strtolower($this->address), 2));
                foreach ($charList as $char) {
                    if (!in_array($char, $this->possibleChars)) {
                        $this->addError($attribute, 'Не верный символ в адресе ' . $char);
                        return;
                    }
                }

                // проверяю "не равен ли он адресу токена"
                if (strtolower($this->address) == strtolower($this->billingFrom->getWalletToken()->token->address)) {
                    $this->addError($attribute, 'Нельзя отправить монеты на адрес контракта монеты');
                    return;
                }

                try {
                    // ищу есть ли счет токена у клиента
                    $billingTo = UserBill::findOne(['address' => $address, 'currency' => $this->billingFrom->currency, 'mark_deleted' => 0]);
                } catch (\Exception $e) {
                    // Пробую найти кошелек эфира у получателя

                    try {
                        $billing = UserBill::findOne(['address' => $address, 'currency' => Currency::ETH, 'mark_deleted' => 0]);
                        $tokenObject = Token::findOne(['currency_id' => $this->billingFrom->currency]);
                        $tokenBilling = WalletToken::createFromEth($billing, $tokenObject, $tokenObject->getCurrency()->title, $billing->user_id);

                    } catch (\Exception $e) {
                        // внешний адрес, ничего не делаю
                    }
                }
            }

            $this->address = $address;
        }
    }

    private function isPhone($address)
    {
        return StringHelper::startsWith($address, '+');
    }

    private function isCardNumber($address)
    {
        $address = str_replace(' ', '', $address);
        if (strlen($address) == 16) {
            return preg_match('/\D/', $address) == 0;
        } else {
            return false;
        }
    }

    /**
     * Проверяет пароль для счета
     * Сравнивает с паролем кабинета
     *
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->billingFrom->validatePassword($this->password)) {
                $this->addError('password', 'Не верный пароль');
                return;
            }
        }
    }

    /**
     * Проверяет что счет существует и принадлежит пользователю
     * и присоединяет объект billing
     *
     * @param $attribute
     * @param $params
     */
    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            /** @var \common\models\UserAvatar $user */
            try {
                $billing = UserBill::findOne($this->billing_id);
            } catch (\Exception $e) {
                $this->addError('address', 'Не найден счет');
                return;
            }
            if ($billing->user_id != Yii::$app->user->id) {
                $this->addError('address', 'Это не ваш счет. Не красиво так делать');
                return;
            }
            if ($billing->address == $this->address) {
                $this->addError('address', 'Нельзя отправить деньги на счет отправителя');
                return;
            }
            $this->billingFrom = $billing;
        }
    }

    /**
     * Отправляет деньги на счет
     *
     * @return bool|string
     * Если возвращено false то появилась ошибка
     * Иначе идентификатор транзакции
     *
     * @throws Exception
     */
    public function send()
    {
        $wallet = $this->billingFrom->getWalletToken();
        try {
            if (!empty($this->gasLimit) || !empty($this->gasPrice)) {
                $amount = [];
                $amount['amount'] = $this->amount;
                if (!empty($this->gasPrice)) $amount['gasPrice'] = $this->gasPrice * pow(10,9);
            } else {
                $amount = $this->amount;
            }
            $transaction = $wallet->pay([$this->address => [
                'amount'  => $amount,
                'comment' => $this->comment,
            ]], $this->password);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 400:
                    if (StringHelper::startsWith($e->getMessage(), 'Error: insufficient funds for gas * price + value')) {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                        break;
                    }
                    if (StringHelper::startsWith($e->getMessage(), 'insufficient funds for gas * price + value')) {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                        break;
                    }
                    if (StringHelper::startsWith(strtolower($e->getMessage()), 'insufficient')) {
                        $this->addError('amount', 'Не хватило денег для оплаты комиссии');
                        break;
                    }
                    $this->addError('amount', 'Другая ошибка от API: '. $e->getMessage());
                    break;
                case 401:
                    $this->addError('amount', 'Не верная авторизация API на сервере');
                    break;
                case 502:
                    $this->addError('amount', 'Сервер не отвечает');
                    break;
                case 402:
                    $this->addError('amount', 'Не верный формат JSON от сервера');
                    break;
                default:
                    $this->addError('amount', $e->getMessage());
                    break;
            }
            return false;
        }

        return $transaction;
    }

    /**
     * Подготовка для отправки, ищет счет пользователя, user billing не обязательно
     *
     * @return bool | array
     * [
     *  'address' => '<string>',
     *  'user' => [
     *      'id' =>
     *      'name2' =>
     *      'avatar' =>
     *  ],
     *  'billing' => [
     *      'id' =>
     *      'currency' => 'BTC'
     *      'image' => '/images/btc.png' (квадратная)
     *  ],
     * ]
     *
     * @throws Exception
     */
    public function prepare()
    {
        try {
            $address = UserBillAddress::findOne(['address' => $this->address]);
            $billing = $address->getBilling();
        } catch (\Exception $e) {
            try {
                $billing = UserBill::findOne(['address' => $this->address, 'currency' => $this->billingFrom->currency]);
            } catch (\Exception $e) {
                return [
                    'address'  => $this->address,
                    'amount'   => $this->amount,
                    'currency' => $this->currency,
                    'hasUser'  => false,
                ];
            }
        }
        try {
            $user = $billing->getUser();
        } catch (\Exception $e) {
            return [
                'address'  => $this->address,
                'amount'   => $this->amount,
                'currency' => $this->currency,
                'hasUser'  => false,
            ];
        }

        try {
            $data = $this->calculateEstimateCost();
        } catch (\Exception $e) {

            switch ($e->getCode()) {
                case 502:
                    $this->addError('amount', 'Сервер не отвечает');
                    break;
                case 100:
                    $this->addError('amount', 'Сервер выдает ошибку:' . $e->getMessage());
                    break;
                case 401:
                    $this->addError('amount', 'Не авторизовано API');
                    break;
                case 402:
                    $this->addError('amount', 'Не верный формат JSON от сервера');
                    break;
                case 101:
                    $this->addError('amount', 'API выдает указанную ошибку:' . $e->getMessage());
                    break;
                default:
                    $this->addError('amount', $e->getMessage());
                    break;
            }
            return false;
        }

        return [
            'address'  => $this->address,
            'amount'   => $this->amount,
            'currency' => $this->currency,
            'hasUser'  => true,
            'calculate' => $data,
            'user'     => [
                'id'     => $user->id,
                'name2'  => $user->getName2(),
                'avatar' => $user->getAvatar(),
            ],
            'billing'  => [
                'id'       => $billing->id,
                'address'  => $billing->address,
                'name'     => $billing->name,
                'currency' => $billing->getCurrencyObject()->code,
                'image'    => '/images/controller/cabinet-bills/index/bitcoin2.jpg',
            ],
        ];
    }

    private function calculateEstimateCost()
    {
        $wallet = $this->billingFrom->getWalletToken();

        return $wallet->calculate([$this->address => [
            'amount'  => $this->amount,
            'comment' => $this->comment,
        ]], $this->password);
    }

}
