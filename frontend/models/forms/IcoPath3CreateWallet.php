<?php

namespace avatar\models\forms;

use avatar\models\WalletETH;
use avatar\models\WalletToken;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\investment\IcoRequest;
use common\models\PaySystemConfig;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class IcoPath3CreateWallet extends Model
{
    /** @var  string */
    public $password1;

    /** @var  float */
    public $tokens;

    /** @var  int */
    public $currency;

    /** @var  int */
    public $billing_id;

    /** @var  \common\models\avatar\UserBill кошелек эфира */
    public $billing;

    /** @var  int тип состояния формы
     // 1. выбран счет
     // 2. создание нового счета
     * устанавливается в defineType
     */
    private $type;

    const TYPE_BILLING = 1;
    const TYPE_NEW = 2;

    /** @var  int */
    private $icoId;

    /** @var  \common\models\investment\Project */
    private $ico;

    /** @var  \common\models\Token токен который продается на ICO */
    private $tokenObject;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password1', 'string'],

            ['tokens', 'double'],

            ['currency', 'required'],
            ['currency', 'integer'],

            ['billing_id', 'integer'],

            ['billing_id', 'defineType', 'skipOnEmpty' => false],
            ['billing_id', 'validateBilling', 'skipOnEmpty' => false],
            ['billing_id', 'validateNew', 'skipOnEmpty' => false],
            ['billing_id', 'setICO', 'skipOnEmpty' => false],
        ];
    }

    public function defineType($attribute, $params)
    {
        if (!$this->hasErrors()) {
            // проверка на два состояния
            // 1. выбран счет
            // 2. создание нового счета

            if (empty($this->billing_id)) {
                $this->type = self::TYPE_NEW;
            } else {
                $this->type = self::TYPE_BILLING;
            }
        }
    }

    public function validateBilling($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->type == self::TYPE_BILLING) {
                try {
                    $billing = UserBill::findOne($this->billing_id);
                } catch (\Exception $e) {
                    $this->addError($attribute, 'Не найден счет');
                    return;
                }
                if ($billing->user_id != Yii::$app->user->id) {
                    $this->addError($attribute, 'Это не ваш счет');
                    return;
                }

                $this->billing = $billing;
            }
        }
    }

    public function validateNew($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->type == self::TYPE_NEW) {
                /** @var \common\models\UserAvatar $user */
                $user = Yii::$app->user->identity;
                if (!$user->validatePassword($this->password1)) {
                    $this->addError($attribute, 'Пароль не верный');
                    return;
                }
            }
        }
    }

    public function setICO($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->icoId = \Yii::$app->session->get('ico')['id'];
            $this->ico = \common\models\investment\Project::findOne($this->icoId);
            $this->tokenObject = $this->ico->getIco()->getCurrency()->getToken();
        }
    }

    /**
     * Конвертирует массив ошибок от формы чтобы их можно было выдать через JSON
     *
     * @param array $params
     *      [
     *      'amount' => ['Не верное число',...],
     *      ]
     *
     * @return array
     * [[
     *      'name'  => 'amount',
     *      'value' => ['Не верное число',...],
     * ]]
     */
    public function convert($params = null)
    {
        if (is_null($params)) {
            $params = $this->errors;
        }
        if (count($params) == 0) {
            return [];
        }
        $ret = [];
        foreach ($params as $name => $arr) {
            $ret[] = [
                'name'  => $name,
                'value' => $arr,
            ];
        }
        return $ret;
    }

    /**
     *
     *
     * @return \common\models\investment\IcoRequest
     */
    public function action()
    {
        $this->createTokenWallet();
        $request = $this->createRequest();
        $this->setNewStatus($request);

        return $request;
    }

    /**
     * @return WalletToken
     */
    private function createTokenWallet()
    {
        // Если нужно создать кошелек эфира то создает его
        if ($this->type == self::TYPE_NEW) {
            /** @var \common\models\UserAvatar $user */
            $user = Yii::$app->user->identity;
            $walletETH = WalletETH::create('ETH', $this->password1, Yii::$app->user->id, $user->password_save_type);
            $this->billing = $walletETH->billing;
        }

        // Проверяет наличие кошелька токена
        try {
            $billing = UserBill::findOne([
                'user_id'      => Yii::$app->user->id,
                'currency'     => $this->tokenObject->currency_id,
                'address'      => $this->billing->address,
                'mark_deleted' => 0,
            ]);
            $billingToken = $billing->getWalletToken();
        } catch (\Exception $e) {
            // нет токена, создаю
            $billingToken = WalletToken::createFromEth($this->billing, $this->tokenObject, $this->tokenObject->getCurrency()->title, Yii::$app->user->id);
        }

        return $billingToken;
    }


    /**
     * Создает счет и заказ
     *
     * @return IcoRequest
     */
    private function createRequest()
    {
        $c = \common\models\avatar\Currency::findOne($this->currency);

        $id = PaySystemConfig::find()
            ->select(['paysystems_config.id'])
            ->innerJoin('paysystems', 'paysystems.id = paysystems_config.paysystem_id')
            ->where(['paysystems_config.parent_id' => $this->ico->payments_list_id])
            ->andWhere(['paysystems.currency' => $c->code])
            ->scalar();

        $config = PaySystemConfig::findOne($id);
        $paySystem = $config->getPaySystem();

        $tokens = $this->tokens;
        $address = $this->billing->address;
        $currency = $paySystem->getCurrencyObject();
        $sumAfter = $this->convertTokens($tokens, $currency);
        $sumBefore = $sumAfter;

        $billing = BillingMain::add([
            'sum_before'  => $sumBefore,
            'sum_after'   => $sumAfter,
            'source_id'   => $config->paysystem_id,
            'currency_id' => $currency->id,
        ]);

        $request = IcoRequest::add([
            'paysystem_config_id' => $config->id,
            'billing_id'          => $billing->id,
            'tokens'              => $tokens,
            'address'             => $address,
            'user_id'             => Yii::$app->user->id,
            'ico_id'              => $this->icoId,
        ]);

        return $request;
    }

    /**
     * Устанавливает статус "Заказ создан"
     *
     * @param \common\models\investment\IcoRequest $IcoRequest
     */
    public function setNewStatus($IcoRequest)
    {
        $IcoRequest->addStatusToShop([
            'status'  => IcoRequest::STATUS_CREATED,
            'message' => 'Создан заказ',
        ]);
    }

    /**
     * Переводит кол-во токенов в валюту
     *
     * @param float $tokens
     * @param \common\models\avatar\Currency $currency
     *
     * @return float кол-во единиц
     * @throws
     */
    private function convertTokens($tokens, $currency)
    {
        $ico = $this->ico->getIco();

        /** @var \avatar\models\forms\Currency $currencyIco */
        $currencyIco = Currency::findOne($ico->kurs_currency_id);
        if (is_null($currencyIco)) {
            throw new \Exception('Не найдена валюта');
        }

        return \common\models\avatar\Currency::convert($tokens * $ico->kurs, $currencyIco->code, $currency->code);
    }

}

