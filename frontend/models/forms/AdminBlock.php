<?php

namespace avatar\models\forms;

use common\models\Config;
use cs\Application;
use cs\services\Security;
use cs\services\VarDumper;
use iAvatar777\services\FormAjax\Model;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 *
 */
class AdminBlock extends Model
{
    /**
     * @var bool
     */
    public $param;

    public function init()
    {
        $file = Yii::getAlias('@frontend/config/catchAll.php');
        $content = file_get_contents($file);
        $rows = explode("\n", $content);
        $v = $rows[1];
        if ($v == '//false') $this->param = 0;
        if ($v == '//true') $this->param = 1;
    }

    public function attributeWidgets()
    {
        return ['param' => '\common\widgets\CheckBox2\CheckBox'];
    }

    public function rules()
    {
        return [
            ['param', 'integer']
        ];
    }
}
