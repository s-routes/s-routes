<?php

namespace avatar\models\api\forms;

use common\models\avatar\UserBill;
use common\models\SendLetter;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class Registration2 extends Model
{

    public $email;

    public $password;

    /**
     */
    public function rules()
    {
        return [
            [['password', 'email'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'existEmail', 'message' => 'Такой пользователь уже существует'],
            ['password', 'string', 'min' => 3],
        ];
    }

    public function existEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if  (UserAvatar::find()->where(['email' => $this->email])->exists()) {
                $this->addError($attribute, 'Такой пользователь уже существует');
                return;
            }
        }
    }

    /**
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function action()
    {
        $email = strtolower($this->email);
        $fields = [
            'email'              => $email,
            'password'           => $this->password,
            'mark_deleted'       => 0,
            'email_is_confirm'   => 1,
            'wallets_is_locked'  => 0,
            'subscribe_is_news'  => 1,
            'subscribe_is_blog'  => 1,
            'created_at'         => time(),
            'auth_key'           => \Yii::$app->security->generateRandomString(60),
            'password_save_type' => UserBill::PASSWORD_TYPE_OPEN_CRYPT,
        ];

        $user = new UserAvatar($fields);
        $ret = $user->save();
        $user->id = UserAvatar::getDb()->lastInsertID;

        return $user;
    }
}
