<?php

namespace avatar\models\api\forms;

use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class RegistrationEmail extends Model
{

    public $email;

    public $password;

    /**
     */
    public function rules()
    {
        return [
            [['password', 'email'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'existEmail', 'message' => 'Такой пользователь уже существует'],
            ['password', 'string', 'min' => 3],
        ];
    }

    public function existEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if  (UserAvatar::find()->where(['email' => $this->email])->exists()) {
                $this->addError($attribute, 'Такой пользователь уже существует');
                return;
            }
        }
    }

    /**
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return array
     * [
     *  'key'  =>
     * ]
     */
    public function action()
    {
        $user = UserAvatar::registrationCreateUser($this->email, $this->password);
        $pin = substr(str_shuffle('012345678901234567890123456789012345678901234567890123456789'),0,8);
        $key = Security::generateRandomString();
        Yii::$app->cache->set('registration-mail-' . $key, [
            'pin'     => $pin,
            'user_id' => $user->id,
        ]);
        $ret = \avatar\base\Application::mail($this->email, 'Подтверждение регистрации', 'registrationPin', [
            'pin'      => $pin,
            'user'     => $user,
        ]);

        \Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\\registrationApi\\mail\\result');

        return [
            'user' => $user,
            'key'  => $key,
        ];
    }
}
