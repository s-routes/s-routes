<?php

namespace avatar\models\api\forms;

use common\models\SendLetter;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 */
class Registration extends Model
{

    public $email;

    public $password;

    /**
     */
    public function rules()
    {
        return [
            [['password', 'email'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'existEmail', 'message' => 'Такой пользователь уже существует'],
            ['password', 'string', 'min' => 3],
        ];
    }

    public function existEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if  (UserAvatar::find()->where(['email' => $this->email])->exists()) {
                $this->addError($attribute, 'Такой пользователь уже существует');
                return;
            }
        }
    }

    /**
     */
    public function attributeLabels()
    {
        return [
            'email'    => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function action()
    {
        return UserAvatar::registration($this->email, $this->password);
    }
}
