<?php

namespace avatar\models\search;

use common\models\language\Message;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * @property int    id                         идентификатор заявки
 * @property int    user_id                    Идентификатор пользователя кто сделал предложение
 * @property int    price                      Цена по которой предлагается купить продать VVB
 * @property int    currency_id                Валюта, указывает на db.currency
 * @property int    currency_io
 * @property int    type_id                    Вид сделки 1 - купить, 2 - продать
 * @property string volume_start
 * @property string condition                  Условия сделки, простой текст
 * @property string name                       Заголовок
 * @property int    created_at                 Момент создания предложения
 * @property int    assessment_positive
 * @property int    offer_pay_id
 * @property double volume_io_start
 */
class Offer2 extends ActiveRecord
{
    public $pay_id;
    public $last_action3;


    public static function tableName()
    {
        return 'offer';
    }

    public function rules()
    {
        return [
            ['name', 'string'],
            ['user_id', 'integer'],
            ['price', 'integer'],
            ['volume_io_start', 'double'],
            ['type_id', 'integer'],
            ['pay_id', 'integer'],
            ['currency_io', 'integer'],
            ['assessment_positive', 'string'],
            ['offer_pay_id', 'integer'],
        ];
    }

    /**
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $t = time() - Yii::$app->params['timeOnline'];

        $query = \common\models\exchange\Offer::find()
            ->innerJoin('user', 'user.id = offer.user_id')
            ->select([
                'offer.*',
                'user.last_action',
                'user.assessment_positive',
                'user.assessment_negative',
                'user.deals_count_all',
                'user.deals_count_offer',
                'if(user.last_action > '.$t.',1,0) as last_action2',
            ])
            ->asArray()
        ;
        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        if (!is_null($sort)) $dataProvider->sort = $sort;

        // загружаю параметры и валидирую
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        if ($this->type_id) $query->andWhere(['type_id' => $this->type_id]);
        if ($this->price) {
            $query->andWhere([
                'and',
                ['>=', 'price_m', $this->price],
                ['<', 'price_m', $this->price + 1],
            ]);
        }
        if ($this->volume_io_start) {
            $query->andWhere([
                'and',
                ['<=', 'volume_io_start_m', $this->volume_io_start],
                ['>=', 'volume_io_finish_m', $this->volume_io_start],
            ]);
        }
        if ($this->currency_io) $query->andWhere(['currency_io' => $this->currency_io]);
        if ($this->name) $query->andWhere(['like', 'name', $this->name]);
        if ($this->offer_pay_id) {
            $query->andWhere(['offer.offer_pay_id' => $this->offer_pay_id]);
        }
        if ($this->pay_id) {
            $query
                ->innerJoin('offer_pay_link', 'offer_pay_link.offer_id = offer.id')
                ->andWhere(['offer_pay_link.pay_id' => $this->pay_id]);
        }
        if ($this->assessment_positive) {
            $query
                ->where(['like', 'user.name_first', $this->assessment_positive]);
        }

        return $dataProvider;
    }
}
