<?php

namespace avatar\models\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 */
class Wallet extends Message
{
    public $id;
    public $amount;

    public function rules()
    {
        return [
            ['id', 'integer'],
        ];
    }

    /**
     * @param array $params
     * @param null $where
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $query = \common\models\piramida\Wallet::find();

        if (!is_null($where)) $query->andWhere($where);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // adjust the query by adding the filters
        if ($this->id) {
            $query
                ->andWhere(['id' => $this->id])
            ;
        }

        return $dataProvider;
    }
}
