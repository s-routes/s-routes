<?php

namespace avatar\models\search;

use common\models\language\Message;
use common\services\Debugger;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\StringHelper;

/**
 * @property int id                      Идентификатор заявки
 * @property int user_id                 Идентификатор пользователя кто сделал предложение
 * @property int price                   Цена по которой предлагается купить продать 1 VVB, в атомах
 * @property int currency_id             Валюта для price, указывает на db.currency
 * @property int offer_id                ссылка на предложение
 * @property int created_at              Момент создания предложения
 * @property int volume                  Объем сделки в атомах price и currency_id = (price/100) * volume_vvb
 * @property int volume_vvb              Объем сделки в атомах VVB
 * @property int type_id                 Тип сделки для user_id
 * @property int status                  Статус сделки
 * @property int time_finish             Время окончания сделки
 * @property int time_accepted           Время подтверждения сделки
 * @property int currency_io
 */
class Deal extends ActiveRecord
{
    public $status2;

    public function rules()
    {
        return [
            ['status2', 'integer'],
        ];
    }

    /**
     * @param array $params
     * @param array|null $where
     * @param Sort | array $sort
     *
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $query = \common\models\exchange\Deal::find()
            ->leftJoin('arbitrator', 'arbitrator.deal_id = deal.id')
            ->select([
                'deal.*',
                'arbitrator.arbitrator_id',
            ])
            ->andWhere([
                'deal.status' => [
                  \common\models\exchange\Deal::STATUS_AUDIT_WAIT,
                  \common\models\exchange\Deal::STATUS_AUDIT_ACCEPTED,
                  \common\models\exchange\Deal::STATUS_AUDIT_FINISH,
                ]
            ])
            ->asArray()
        ;

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        if (!is_null($sort)) $dataProvider->sort = $sort;

        // load the search form data and validate
        if (!($this->load($params) && $this->validate())) {
            if (!is_null($where)) $query->andWhere($where);
            return $dataProvider;
        }

        // adjust the query by adding the filters
        if ($this->status2) {
            if ($this->status2 == 1) {
                $dataProvider->query->andWhere(['deal.status' => [
                    \common\models\exchange\Deal::STATUS_AUDIT_WAIT,
                ]]);
            }
        } else {
            if (!is_null($where)) $query->andWhere($where);
        }


        return $dataProvider;
    }
}
