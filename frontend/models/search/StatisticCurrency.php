<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.04.2016
 * Time: 17:20
 */

namespace avatar\models\search;

use common\models\Countries;
use common\models\User;
use common\services\Debugger;use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class StatisticCurrency extends Model
{
    public $code;
    public $title;

    public function rules()
    {
        return [
            ['code', 'string'],
            ['title', 'string'],
        ];
    }

    /**
     * @param $sort
     * @param array | null $where
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\avatar\Currency::find()
                ->where(['not', ['id_coingecko2' => null]])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 100
            ]
        ]);
        if ($sort) {
            $dataProvider->sort = $sort;
        }
        if ($where) {
            $dataProvider->query->where($where);
        }

        // Загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // применяю фильтры
        if ($this->code) {
            $dataProvider->query->andWhere(['like', 'code', $this->code]);
        }
        if ($this->title) {
            $dataProvider->query->andWhere(['like', 'title', $this->title]);
        }


        return $dataProvider;
    }
}