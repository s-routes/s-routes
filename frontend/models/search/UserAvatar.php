<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 26.04.2016
 * Time: 17:20
 */

namespace avatar\models\search;

use common\models\Countries;
use common\models\User;
use common\services\Debugger;use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class UserAvatar extends Model
{
    public $id;
    public $email;
    public $name_last;
    public $name_first;

    public function rules()
    {
        return [
            [
               [
                   'id',
                   'email',
                   'name_last',
                   'name_first',
               ], 'safe'
            ]
        ];
    }

    /**
     * @param $sort
     * @param array | null $where
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params, $where = null, $sort = null)
    {
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => \common\models\UserAvatar::find()
        ]);
        if ($sort) {
            $dataProvider->sort = $sort;
        }
        if ($where) {
            $dataProvider->query->where($where);
        }

        // Загружаю данные
        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        // применяю фильтры
        if ($this->id) {
            $dataProvider->query->andWhere(['id' => $this->id]);
        }
        if ($this->email) {
            $dataProvider->query->andWhere(['like', 'email', $this->email]);
        }
        if ($this->name_last) {
            $dataProvider->query->andWhere(['like', 'name_last', $this->name_last]);
        }
        if ($this->name_first) {
            $dataProvider->query->andWhere(['like', 'name_first', $this->name_first]);
        }


        return $dataProvider;
    }
}