<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.11.2016
 * Time: 3:55
 */

namespace avatar\models;


use avatar\modules\ETH\ServiceEtherScan;
use avatar\modules\ETH\ServiceInfura;
use avatar\modules\ETH\ServiceSiriusB;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBillConfig;
use common\services\Security;
use common\services\UsersInCache;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use Blocktrail\SDK\WalletInterface;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use common\models\piramida\BitCoinUserAddress;
use common\payment\BitCoinBlockTrailPayment;
use yii\base\BaseObject;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * Класс для кошелька VVB
 *
 * Class WalletVVB
 *
 * @package avatar\models
 */
class WalletVVB extends BaseObject
{
    /** @var  \iAvatar777\avatarPay\AvatarPay */
    public $provider;

    /** @var  \common\models\avatar\UserBill */
    public $billing;

    /** @var  \common\models\piramida\Wallet */
    public $wallet;

    /**
     * @return \common\models\piramida\Wallet
     */
    public function getWallet()
    {
        if (is_null($this->wallet)) {
            $this->wallet = \common\models\piramida\Wallet::findOne($this->billing->address);
        }

        return $this->wallet;
    }

    /**
     * Выдает баланс в атомах
     *
     * @return int as string
     */
    public function getBalance()
    {
        $wallet = $this->getWallet();
        if (is_null($wallet)) new \Exception('Не найден кошелек');

        return $wallet->amount;
    }

    /**
     * Пересылает монеты на другой кошелек
     *
     * @param int | \common\models\piramida\Wallet | \common\models\avatar\UserBill | \avatar\models\WalletVVB $to кошелек назначения
     * @param int $amount кол-во атомов
     * @param string $comment коментарий
     * @param string $password пароль от кабинета, только для кошельков типа PASSWORD_TYPE_HIDE_CABINET
     *
     * @return \common\models\Piramida\Transaction
     * @throws
     */
    public function send($to, $amount, $comment = '', $password = '')
    {
        if (in_array($this->billing->password_type, [UserBill::PASSWORD_TYPE_HIDE_CABINET])) {
            if (!$this->billing->validatePassword($password)) {
                throw new \Exception('Пароль не верный');
            }
            $passwordWallet = $this->billing->getPassword($password);
            if (password_verify($passwordWallet, $this->getWallet()->password)) {
                throw new \Exception('Пароль не верный');
            }
        }

        $wallet = null;
        if ($to instanceof \common\models\avatar\UserBill) {
            /** @var \common\models\avatar\UserBill $bill */
            $bill = $to;
            $walletVVB = $bill->getWalletVVB();
            $wallet = $walletVVB->getWallet();
        } else if ($to instanceof \avatar\models\WalletVVB) {
            /** @var \avatar\models\WalletVVB $WalletVVB */
            $WalletVVB = $to;
            $wallet = $WalletVVB->getWallet();
        } else if ($to instanceof \common\models\piramida\Wallet) {
            $wallet = $to;
        } else {
            if (!Application::isInteger($to)) {
                throw new \Exception('Идентификатор кошелька не верный');
            }
            $wallet = \common\models\piramida\Wallet::findOne($to);
            if (is_null($wallet)) {
                throw new \Exception('Кошелек не найден');
            }
        }
        $from = $this->getWallet();
        $t = $from->move($wallet, $amount, $comment);

        return $t;
    }

    /**
     * Создает кошелек
     *
     * Создает счет \common\models\avatar\UserBill
     * Создает адресн новый \common\models\avatar\UserBillAddress
     *
     * @param string $name
     * @param string $password пароль от кабинета
     * @param int | null $userId
     * @param int $passwordType
     *
     * @return \avatar\models\WalletVVB
     *
     * @throws
     */
    public static function create($name, $password, $userId = null, $passwordType = \common\models\avatar\UserBill::PASSWORD_TYPE_OPEN_CRYPT)
    {
        $passwordData = UserBill::createPassword($password, $passwordType);
        $passwordWallet = $passwordData['passwordWallet'];
        $passwordHash = $passwordData['passwordHash'];

        $master_key = \cs\services\Security::generateRandomString(64);

        $wallet = \common\models\piramida\Wallet::addNew([
            'currency_id' => 10,
            'password'    => password_hash($passwordWallet, PASSWORD_BCRYPT),
            'master_key'  => Security\AES::encrypt256CBC($master_key, UserBill::passwordToKey32(\Yii::$app->params['unlockKey'])),
        ]);

        $t = Yii::$app->db->beginTransaction();
        try {
            // Сохраняю счет
            $billing = \common\models\avatar\UserBill::add([
                'user_id'       => $userId,
                'address'       => $wallet->id,
                'name'          => $name,
                'password'      => $passwordHash,
                'password_type' => $passwordType,
                'currency'      => Currency::VVB,
            ]);

            $c = new UserBillConfig([
                'id'      => $billing->id,
                'bill_id' => $billing->id,
                'config'  => Security\AES::encrypt256CBC($master_key, UserBill::passwordToKey32(\Yii::$app->params['unlockKey'])),
            ]);
            $c->save();

            $walletAvatar = new self([
                'billing'  => $billing,
                'provider' => null,
                'wallet'   => $wallet,
            ]);

            $t->commit();

            return $walletAvatar;

        } catch (\Exception $e) {
            $t->rollBack();

            throw $e;
        }
    }
}