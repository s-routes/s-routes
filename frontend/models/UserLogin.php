<?php

namespace avatar\models;

use cs\services\Security;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    user_id
 * @property string     ip
 * @property int        ip_int
 * @property integer    time
 * @property integer    is_success
 * @property string     browser
 */
class UserLogin extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_login';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'ip', 'browser', 'time'], 'required'],
            [['ip'], 'string', 'max' => 60],
            [['browser'], 'string', 'max' => 255],
            [['user_id', 'time'], 'integer'],
        ];
    }

    /**
     * @param bool $is_success
     *
     * @return self
     */
    public static function add($is_success = true)
    {
        $fields = [
            'user_id'    => Yii::$app->user->id,
            'ip'         => Yii::$app->request->getUserIP(),
//            'ip_int'     => self::ip2int(Yii::$app->request->getUserIP()),
            'browser'    => Yii::$app->request->getUserAgent(),
            'time'       => time(),
            'is_success' => $is_success ? 1 : 0,
        ];
        

        $item = new static($fields);
        $item->save();
        if ($item->hasErrors()) VarDumper::dump($item->errors);
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

    /**
     * Преобразовывает строку IP в INT
     *
     * @param $ip
     *
     * @return int
     * @throws \yii\base\Exception
     */
    private static function ip2int($ip)
    {
        $ip_int = 0;
        $arr = explode('.', $ip);
        if (count($arr) != 4) throw new Exception('IP должен содержать 4 значения через точку');
        $mnojitel = 1;
        for ($i = 4; $i > 0; $i--) {
            if ($arr[$i - 1] < 0 or $arr[$i - 1] > 255) throw new Exception('Значение IP выходит за пределы 0-255');
            $ip_int += $arr[$i - 1] * $mnojitel;
            $mnojitel = $mnojitel * 256;
        }

        return $ip_int;
    }
}
