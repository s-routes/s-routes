<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\validate\AuthSecondFaResetStep2 */

$this->title = 'Сброс 2FA';
?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet/index';
}
JS

        ]) ?>

        <p>После входа 2FA будет сброшена и вы войдете в кабинет.</p>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>

        <hr>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Сбросить и войти снова']) ?>
    </div>
</div>

