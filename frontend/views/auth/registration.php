<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Registration */

$this->title = Yii::t('c.0uMdJb0e0n', 'Регистрация');
?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="alert alert-success">
                <?= \Yii::t('c.0uMdJb0e0n', 'Успешно! Письмо о регистрации отправлено и придет') ?>
            </div>
        <?php else: ?>
            <p class="alert alert-warning">Внимание уважаемый пользователь! Выберите один из способов регистрации - как простой пользователь или как участник реферальных программ. Примите решение, и нажмите соответствующую кнопку ниже: планируете ли вы участвовать в реферальных программах?</p>

            <p>
                <a href="/auth/registration-yes" class="btn btn-default">Да, я буду участвовать в реферальных программах</a>
            </p>
            <p>
                <a href="/auth/registration-no" class="btn btn-default">Нет, я не буду участвовать в реферальных программах</a>
            </p>

        <?php endif; ?>
    </div>
</div>