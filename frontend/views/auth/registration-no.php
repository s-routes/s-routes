<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Registration */

$this->title = Yii::t('c.0uMdJb0e0n', 'Регистрация');
?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
            <div class="alert alert-success">
                <?= \Yii::t('c.0uMdJb0e0n', 'Успешно! Письмо о регистрации отправлено и придет') ?>
            </div>
            <?php
        $this->registerJs(<<<JS
var count = 60 ;
setInterval(function () {
    if (count == 0) {
        $('#collapse-text').collapse('hide');
        $('#collapse-button').collapse('show');
    } else {
        $('#time').html(count + ' сек');
        count--;
    }
}, 1000);

$('.buttonSend').click(function (e){
    ajaxJson({
        url: '/auth/registration-send',
        data: {id: 1},
        success: function (ret) {
            count = 60;
            $('#collapse-text').collapse('show');
            $('#collapse-button').collapse('hide');
        }
    }); 
});
JS
        )
            ?>
            <div class="collapse in" id="collapse-text">
                <p class="alert alert-info">Через <span id="time"></span> можно будет отправить письмо снова</p>
            </div>
            <div class="collapse" id="collapse-button">
                <p class="text-center"><button class="btn btn-primary buttonSend">Отправить письмо еще раз</button></p>
            </div>
        <?php else: ?>
            <?php $form = ActiveForm::begin([
                'id'                   => 'contact-form',
                'enableAjaxValidation' => true,
            ]); ?>

            <?= $form->field($model, 'name_first', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', Yii::t('c.0uMdJb0e0n', 'Ник/имя'))]])->label(Yii::t('c.0uMdJb0e0n', 'Ник/имя'), ['class' => 'hide']) ?>

            <?php $field = $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label(Yii::t('c.0uMdJb0e0n', 'Почта'), ['class' => 'hide']);
            $field->validateOnBlur = true;
            $field->validateOnChange = true;
            echo $field;
            ?>
            <?= $form->field($model, 'password1', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Пароль'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'password2', ['inputOptions' => ['placeholder' => Yii::t('c.0uMdJb0e0n', 'Повторите пароль')]])->passwordInput()->label(Yii::t('c.0uMdJb0e0n', 'Повторите пароль'), ['class' => 'hide']) ?>
            <?= $form->field($model, 'is_rules')->label(Yii::t('c.0uMdJb0e0n', 'Условия использования сервиса'))->widget('\common\widgets\CheckBox2\CheckBox') ?>
            <ol>
                <li><a href="/page/rules"><?= \Yii::t('c.0uMdJb0e0n', 'Условия использования сервиса') ?></a></li>
                <li><a href="/page/privacy-policy"><?= \Yii::t('c.0uMdJb0e0n', 'Политика конфиденциальности') ?></a></li>
                <li><a href="/page/user-agreement"><?= \Yii::t('c.0uMdJb0e0n', 'Пользовательское соглашение') ?></a></li>
            </ol>

            <?= $form->field($model, 'verificationCode')->widget('\cs\Widget\Google\reCaptcha\reCaptcha') ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.0uMdJb0e0n', 'Зарегистрироваться'), [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php endif; ?>
    </div>
</div>