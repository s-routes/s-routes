<?php
use common\models\avatar\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\AdminComission */

$this->title = 'Проценты на вывод';

Yii::$app->session->set('frontend/views/admin-comission/index.php', $model);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model' => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin/index';
    }).modal();
}
JS
            ,
        ]);
        Yii::$app->session->set('frontend/views/admin-comission/index.php_form', $form);

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => \avatar\models\forms\AdminComission::$currencyList
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $arr = explode('/', $item['id']);
                        $coin = Currency::findOne(['code' => $arr[0]]);

                        return Html::img($coin->image, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                [
                    'header'  => 'Комиссия binance',
                    'content' => function ($item) {
                        $arr = explode('/', $item['id']);
                        if ($arr[0] == $arr[1]) {
                            $c = \common\models\piramida\Currency::findOne(['code' => $arr[0]]);
                            $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $c->id]);

                            return Yii::$app->formatter->asDecimal($cio->benance_fee, $c->decimals);
                        } else {
                            $c = \common\models\piramida\Currency::findOne(['code' => $arr[0]]);
                            $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $c->id]);
                            $mod = \common\models\CurrencyIoModification::findOne(['code' => $arr[1], 'currency_io_id' => $cio->id]);

                            return Yii::$app->formatter->asDecimal($mod->benance_fee, $c->decimals);
                        }
                    }
                ],
                [
                    'header'  => 'Комиссия на вывод, %',
                    'content' => function ($item) {
                        $model = Yii::$app->session->get('frontend/views/admin-comission/index.php');

                        /** @var \iAvatar777\services\FormAjax\ActiveForm $form */
                        $form = Yii::$app->session->get('frontend/views/admin-comission/index.php_form');

                        return $form->field($model, 'name' . str_replace('/', '_', $item['id']) . '_out')->label('', ['class' => 'hide']);
                    }
                ],
                [
                    'header'  => 'min вывод binance',
                    'content' => function ($item) {
                        $arr = explode('/', $item['id']);
                        if ($arr[0] == $arr[1]) {
                            $c = \common\models\piramida\Currency::findOne(['code' => $arr[0]]);
                            $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $c->id]);

                            return Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($cio->benance_min, $c), $c->decimals);
                        } else {
                            $c = \common\models\piramida\Currency::findOne(['code' => $arr[0]]);
                            $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $c->id]);
                            $mod = \common\models\CurrencyIoModification::findOne(['code' => $arr[1], 'currency_io_id' => $cio->id]);

                            return Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($mod->benance_min, $c), $c->decimals);
                        }
                    }
                ],
                [
                    'header'  => 'min вывод, %',
                    'content' => function ($item) {
                        $model = Yii::$app->session->get('frontend/views/admin-comission/index.php');

                        /** @var \iAvatar777\services\FormAjax\ActiveForm $form */
                        $form = Yii::$app->session->get('frontend/views/admin-comission/index.php_form');

                        return $form->field($model, 'name' . str_replace('/', '_', $item['id']) . '_min')->label('', ['class' => 'hide']);
                    }
                ],
            ],
        ]) ?>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end([]); ?>


    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>