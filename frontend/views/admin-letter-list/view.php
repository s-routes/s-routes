<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $item \common\models\SendLetter */
/* @var $model \avatar\models\forms\Answer */

$this->title = $item->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p style="font-weight: bold;">Письмо</p>
        <pre><?= $item->text ?></pre>
        <p><?= Yii::$app->formatter->asDatetime($item->time) ?></p>

        <?php $rows = \common\models\SendLetterItem::find()->where(['letter_id' => $item->id])->all(); ?>
        <?php foreach ($rows as $row) { ?>
            <p style="font-weight: bold;">Ответ</p>
            <pre><?= $row->text ?></pre>
            <p><?= Yii::$app->formatter->asDatetime($row->created_at) ?></p>
        <?php } ?>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-letter-list/index';
    }).modal();
}
JS
,
        ]); ?>

        <?= $form->field($model, 'text')->textarea(['rows' => 20])->label('Ответ') ?>

        <hr>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>
    </div>
</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>