<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все монеты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php \yii\widgets\Pjax::begin(); ?>

        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-currency-int/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});


$('.rowTable').click(function() {
    // window.location = '/admin-currency-int/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Currency::find()
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'decimals',
                [
                    'header'  => 'Монет',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right', 'nowrap' => 'nowrap'],
                    'content' => function ($item) {
                        $m = bcdiv($item['amount'], bcpow(10, $item['decimals']));

                        return Yii::$app->formatter->asDecimal($m, 0);
                    }
                ],
                'name',
                'code',
                [
                    'header'  => 'master_wallet',
                    'content' => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $item['id']]);
                        if (is_null($cio)) {
                            return '[нет значения]';
                        }

                        if (\cs\Application::isEmpty($cio['master_wallet'])) {
                            $v = '[нет значения]';
                        } else {
                            $v = $cio['master_wallet'];
                        }

                        return Html::a($v, ['master-wallet', 'id' => $cio['id']]);
                    }
                ],
                [
                    'header'  => 'extended_info',
                    'content' => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $item['id']]);
                        if (is_null($cio)) {
                            return '[нет значения]';
                        }
                        if (\cs\Application::isEmpty($cio['master_wallet'])) {
                            $v = '[нет значения]';
                        } else {
                            $v = substr($cio['extended_info'], 0, 4) . ' ...';
                        }

                        return Html::a($v, ['ext-info', 'id' => $cio['id']]);
                    }
                ],
                [
                    'header'  => 'main',
                    'content' => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $item['id']]);
                        if (is_null($cio)) return  '';

                        return Html::a($cio['main_wallet'], ['admin-wallets/view', 'id' => $cio['main_wallet']]);
                    }
                ],
                [
                    'header'  => 'block',
                    'content' => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $item['id']]);
                        if (is_null($cio)) return  '';

                        return Html::a($cio['block_wallet'], ['admin-wallets/view', 'id' => $cio['block_wallet']]);
                    }
                ],
                [
                    'header'  => 'lost',
                    'content' => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $item['id']]);
                        if (is_null($cio)) return  '';

                        return Html::a($cio['lost_wallet'], ['admin-wallets/view', 'id' => $cio['lost_wallet']]);
                    }
                ],
                [
                    'header'  => 'Модификации',
                    'content' => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $item['id']]);
                        if (is_null($cio)) return '';
                        $mList = \common\models\CurrencyIoModification::find()->where(['currency_io_id' => $cio->id])->all();

                        if (count($mList) == 0) return  '';

                        return Html::a('Модификации', ['admin-currency-int-modification-list/index', 'id' => $cio['id']]);
                    }
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>