<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Документы';

?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
        <?php \yii\widgets\Pjax::begin() ?>
        <?php
        $this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.rowTable').click(function() {
    window.location = '/documents' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonSignature').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var button = $(this);
    var id = $(this).data('id');
    window.location = '/documents/add-signature' + '?' + 'id' + '=' + id;
});
JS
        );
        ?>
        <?php
        $sort = new \yii\data\Sort([
            'defaultOrder' => [
                'created_at' => SORT_DESC,
            ],
        ]);
        $model = new \avatar\models\search\Documents();
        $provider = $model->search($sort, Yii::$app->request->get());
        ?>
        <?= \yii\grid\GridView::widget([
            'filterModel'  => $model,
            'dataProvider' => $provider,
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                return [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Файл',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'file', '');
                        if ($v == '') return '';
                        $file = pathinfo($v);

                        return Html::img('/images/controller/cabinet/documents/file.png', ['width' => 50]) . '.' . strtoupper($file['extension']);
                    },
                ],
                'name:text:Наименование',
                [
                    'header'  => 'Автор',
                    'content' => function ($item) {
                        if (is_null($item['user_id'])) return '';
                        $u = UsersInCache::find($item['user_id']);
                        if (is_null($u)) return '';
                        $arr = [];
                        $arr[] = Html::img($u['avatar'], [
                            'width' => 50,
                            'class' => 'img-circle',
                            'style' => Html::cssStyleFromArray([
                                'margin-bottom' => '0px',
                                'margin-right'  => '10px',
                                'float'         => 'left',
                            ]),
                        ]);
                        $arr[] = Html::a($u['name'], '/user/' . $u['id']);

                        return join('', $arr);
                    }
                ],
                [
                    'header'    => 'TXID',
                    'attribute' => 'txid',
                    'content'   => function ($item) {
                        return
                            Html::tag(
                                'span',
                                substr(\yii\helpers\ArrayHelper::getValue($item, 'txid'), 0, 6) . '...',
                                [
                                    'class' => 'js-buttonTransactionInfo textDecorated',
                                    'style' => 'font-family: "Courier New", Courier, monospace;',
                                    'role'  => 'button',

                                    'title' => 'Подробнее',
                                    'data'  => [
                                        'toggle'    => 'tooltip',
                                        'placement' => 'bottom',
                                    ],
                                ]
                            );
                    },
                ],
                [
                    'header'    => 'hash',
                    'attribute' => 'hash',
                    'content'   => function ($item) {
                        return
                            Html::tag(
                                'span',
                                substr(\yii\helpers\ArrayHelper::getValue($item, 'hash'), 0, 6) . '...',
                                [
                                    'class' => 'js-buttonTransactionInfo textDecorated',
                                    'style' => 'font-family: "Courier New", Courier, monospace;',
                                    'role'  => 'button',

                                    'title' => 'Подробнее',
                                    'data'  => [
                                        'toggle'    => 'tooltip',
                                        'placement' => 'bottom',
                                    ],
                                ]
                            );
                    },
                ],
                [
                    'header'    => 'Создано',
                    'attribute' => 'created_at',
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => [
                                'toggle'    => 'tooltip',
                                'placement' => 'bottom',
                            ],
                            'class' => 'textDecorated',
                            'title' => Yii::$app->formatter->asDatetime($v),
                        ]);
                    },
                ],
                [
                    'header'  => 'Подписей',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'counter', 0);
                        if ($v == 0) return Html::tag('span', $v, ['class' => 'label label-warning']);

                        return Html::tag('span', $v, ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'  => 'Подписать',
                    'content' => function ($item) {
                        return Html::button('Подписать', [
                            'class' => 'btn btn-success btn-xs buttonSignature',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end() ?>
    </div>
</div>



