<?php

/** @var $this \yii\web\View  */
/** @var $model \avatar\models\forms\AdminContribution  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Отчет по NEIRON';

function groupBy($rows, $attrName)
{
    $offer_user_id_list = ArrayHelper::map($rows, $attrName, function ($i) { return $i;});
    $rows2 = [];
    foreach ($offer_user_id_list as $offer_user_id => $data) {
        $item = [
            $attrName => $offer_user_id,
        ];
        $items = [];
        foreach ($rows as $row) {
            if ($row[$attrName] == $offer_user_id) $items[] = $row;
        }
        $item['rows'] = $items;
        $rows2[] = $item;
    }

    return $rows2;
}

?>

<div style="margin: 0px 50px 0px 50px;">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-6">
                <?php $form = \yii\bootstrap\ActiveForm::begin([

                ]) ?>
                <?= $form->field($model, 'date_start')->widget('\yii\jui\DatePicker', ['options' => ['class' => 'form-control']]) ?>
                <?= $form->field($model, 'date_end')->widget('\yii\jui\DatePicker', ['options' => ['class' => 'form-control']]) ?>
                <hr>
                <?= Html::submitButton('Показать', [
                        'class' => 'btn btn-success',
                        'style' => 'width: 100%',
                ]) ?>

                <?php \yii\bootstrap\ActiveForm::end() ?>
                <p>
                    первая дата считается от 00:00:00 включительно<br>
                    вторая дата учитывается до начала следующего дня (00:00:00)<br>
                    фильтруется дата начала сделки
                </p>

            </div>
            <div class="col-lg-6">
                <?php
                $cio1 = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::NEIRO]);
                ?>
                <?php
                $date_start = null;
                $date_end = null;
                if (\yii\helpers\StringHelper::startsWith(Yii::$app->formatter->dateFormat, 'php')) {
                    $dateFormat = substr(Yii::$app->formatter->dateFormat, 3);
                } else {
                    $dateFormat = \yii\helpers\FormatConverter::convertDateIcuToPhp(Yii::$app->formatter->dateFormat);
                }
                if (!\cs\Application::isEmpty($model->date_start)) {

                    $d = DateTime::createFromFormat($dateFormat . ' H:i:s', $model->date_start . ' 00:00:00');
                    $date_start = $d->format('U');
                }
                if (!\cs\Application::isEmpty($model->date_end)) {
                    $d = DateTime::createFromFormat($dateFormat . ' H:i:s', $model->date_end . ' 00:00:00');
                    $d->add(new DateInterval('P1D'));
                    $date_end = $d->format('U');
                }


                $rowsQuery = \common\models\exchange\Deal::find()
                    ->where(['deal.currency_io' => $cio1->id])
                    ->andWhere(['deal.status' => \common\models\exchange\Deal::STATUS_CLOSE])
                    ->innerJoin('offer', 'offer.id = deal.offer_id')
                    ->select([
                        'deal.*',
                        'offer.user_id as offer_user_id',
                    ])
                    ->asArray()
                ;
                if (!is_null($date_start)) {
                    $rowsQuery->andWhere(['>=', 'deal.created_at', $date_start]);
                }
                if (!is_null($date_end)) {
                    $rowsQuery->andWhere(['<', 'deal.created_at', $date_end]);
                }
                $rows = $rowsQuery->all();

                $rows2 = groupBy($rows, 'offer_user_id');
                $rows3 = [];
                foreach ($rows2 as $r) {
                    $i = [
                        'offer_user_id' => $r['offer_user_id'],
                        'rows'          => groupBy($r['rows'], 'currency_id'),
                    ];
                    $u = \common\models\UserAvatar::findOne($r['offer_user_id']);
                    foreach ($i['rows'] as $clist) {
                        $cext = $clist['currency_id'];
                        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $cext]);
                        $cint = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                    }
                    $rows3[] = $i;
                }

                echo '<table class="table table-striped table-hover" style="width: auto">';

                foreach ($rows3 as $rowUser) {
                    echo '<tr>';
                    echo '<td colspan="2">';
                    $u = \common\models\UserAvatar::findOne($rowUser['offer_user_id']);
                    echo Html::img($u->getAvatar(), ['width' => 20, 'style' => 'margin-right: 10px;']) . $u->getName2();
                    echo '</td>';
                    echo '</tr>';
                    foreach ($rowUser['rows'] as $rowCurrency) {
                        echo '<tr>';
                        echo '<td>';
                        echo '</td>';
                        echo '<td>';
                        $cInt = \common\models\piramida\Currency::initFromCurrencyExt($rowCurrency['currency_id']);
                        echo \common\models\piramida\Currency::getValueFromAtom(
                                array_sum(ArrayHelper::getColumn($rowCurrency['rows'], 'volume')),
                                $cInt
                            ) . ' ' . $cInt->code;
                        echo '</td>';
                        echo '</tr>';
                    }
                }
                echo '</table>';
                ?>
            </div>
        </div>





    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS

$('.rowTable').click(function() {
    //window.location = '/cabinet-exchange/deal-action' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    $rowsQuery = \common\models\exchange\Deal::find()
        ->where(['deal.currency_io' => $cio1->id])
        ->andWhere(['deal.status' => \common\models\exchange\Deal::STATUS_CLOSE])
        ->innerJoin('offer', 'offer.id = deal.offer_id')
        ->select([
            'deal.*',
            'offer.user_id as offer_user_id',
        ])
        ->asArray();
    if (!is_null($date_start)) {
        $rowsQuery->andWhere(['>=', 'deal.created_at', $date_start]);
    }
    if (!is_null($date_end)) {
        $rowsQuery->andWhere(['<', 'deal.created_at', $date_end]);
    }
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => $rowsQuery
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'offer_user_id',
            [
                'header'  => 'Открыл сделку (taker)',
                'content' => function ($item) {
                    $i = ArrayHelper::getValue($item, 'user_id', '');
                    if ($i == '') return '';
                    $user = \common\models\UserAvatar::findOne($i);

                    return Html::img($user->getAvatar(), [
                        'class'  => "img-circle",
                        'width'  => 60,
                        'height' => 60,
                        'style'  => 'margin-bottom: 0px;',
                        'title' => $user->getName2(),
                        'data' => [
                            'toggle' => 'tooltip',
                        ],
                    ]);
                },
            ],
            [
                'header' => 'Тип сделки',

                'content' => function ($item) {
                    $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                    $type_id = $offer->type_id;
                    $text = '';
                    if ($type_id == \common\models\exchange\Offer::TYPE_ID_BUY) {
                        $text = Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right','data' => ['toggle' => 'tooltip'], 'title' => 'Maker покупает у Taker']);
                    }
                    if ($type_id == \common\models\exchange\Offer::TYPE_ID_SELL) {
                        $text = Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left','data' => ['toggle' => 'tooltip'], 'title' => 'Maker продает для Taker']);
                    }

                    return $text;
                },
            ],

            [
                'header'         => 'Заказ',
                'headerOptions'  => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'content'        => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'volume_vvb', 0);
                    $cio = \common\models\CurrencyIO::findOne($item['currency_io']);
                    $cint = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                    $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                    return Yii::$app->formatter->asDecimal($a, $cint->decimals_view);
                },
            ],
            [
                'header'         => '',
                'content'        => function ($item) {
                    $cio = \common\models\CurrencyIO::findOne($item['currency_io']);
                    $c = \common\models\avatar\Currency::findOne($cio->currency_ext_id);
                    $code = $c->code;

                    $cHtml = Html::tag('span', $code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                    return $cHtml;
                },
            ],
            [
                'header'         => 'Цена',
                'headerOptions'  => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'content'        => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                    $ca = \common\models\avatar\Currency::findOne($item['currency_id']);
                    $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $ca->id]);
                    $cint = \common\models\piramida\Currency::findOne($link->currency_int_id);
                    $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                    return Yii::$app->formatter->asDecimal($a, $cint->decimals_view);
                },
            ],
            [
                'header'         => '',
                'content'        => function ($item) {
                    $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                    $offerPay = \common\models\exchange\OfferPay::findOne($offer->offer_pay_id);
                    $code = $offerPay->code;

                    $cHtml = Html::tag('span', $code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                    return $cHtml;
                },
            ],
            [
                'header'  => 'Сделал предложение (maker)',
                'content' => function ($item) {
                    $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                    $i = $offer->user_id;
                    if ($i == '') return '';
                    $user = \common\models\UserAvatar::findOne($i);

                    return Html::img($user->getAvatar(), [
                        'class'  => "img-circle",
                        'width'  => 60,
                        'height' => 60,
                        'style'  => 'margin-bottom: 0px;',
                        'title' => $user->getName2(),
                        'data' => [
                            'toggle' => 'tooltip',
                        ],
                    ]);
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'         => 'Статус',
                'contentOptions' => ['aria-label' => 'Статус'],
                'content'        => function ($item) {
                    if (!isset(\common\models\exchange\Deal::$statusList[$item['status']])) {
                        return '';
                    }
                    $statusClass = [
                        \common\models\exchange\Deal::STATUS_CREATE         => 'default',
                        \common\models\exchange\Deal::STATUS_OPEN           => 'success',
                        \common\models\exchange\Deal::STATUS_BLOCK          => 'info',
                        \common\models\exchange\Deal::STATUS_MONEY_SENDED   => 'success',
                        \common\models\exchange\Deal::STATUS_MONEY_RECEIVED => 'info',
                        \common\models\exchange\Deal::STATUS_CLOSE          => 'success',
                        \common\models\exchange\Deal::STATUS_AUDIT_WAIT     => 'warning',
                        \common\models\exchange\Deal::STATUS_AUDIT_ACCEPTED => 'warning',
                        \common\models\exchange\Deal::STATUS_AUDIT_FINISH   => 'warning',
                        \common\models\exchange\Deal::STATUS_HIDE           => 'danger',
                        \common\models\exchange\Deal::STATUS_CANCEL         => 'danger',
                    ];

                    return Html::tag('span', \common\models\exchange\Deal::$statusList[$item['status']], ['class' => 'label label-' . $statusClass[$item['status']]]);
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    return Html::a('Просмотр', ['admin-deal-list/view', 'id' => $item['id']], [
                        'class' => 'btn btn-info',
                        'data'  => [
                            'id' => $item['id'],
                        ],
                    ]);
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    </div>
