<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\ProfilePhone */

$this->title = 'Мои стейкинг-пакеты';
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">
        <p><a href="<?= Url::to(['cabinet-contribution/add']) ?>" class="btn btn-default"><?= \Yii::t('c.xaREK0rYPp', 'Добавить') ?></a></p>

        <?php
        $time = time();
        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
        $contributionList = \common\models\Contribution::find()->all();
        $contributionListJson = \yii\helpers\Json::encode($contributionList);

        $this->registerJs(<<<JS

function getContribution(id) {
    for(var i=0;i<contributionList.length;i++) {
        if (contributionList[i].id == id) {
            return contributionList[i];
        }
    }
    
    return null;
}

function getRow(id) {
    for(var i=0;i<rows.length;i++) {
        if (rows[i].id == id) {
            return rows[i];
        }
    }
    
    return null;
}

var decimals_neiron = {$c->decimals};
var rows = [];
var contributionList = {$contributionListJson};
$('tr.rowTable').each(function(i,e) {
    var user_contribution = $(e).data('row');
    var contribution = getContribution(user_contribution.contribution_id);
    var v = user_contribution.amount / Math.pow(10, decimals_neiron); // сумма при открытии вклада
    var percent_all = ((contribution.percent / 1000) / 30) * (contribution.time  / (60*60*24)); // процент за весь периуд
    var all_add = v * (percent_all / 100); // сумма прибавления за весь срок
    var time = contribution.time; // кол-во сек во вкладе
    var add_sec = all_add / time; // кол-во добавки в сек
    var diff = {$time} - user_contribution.created_at;
    var v_start = v + (add_sec * diff); // накопленная сумма во вкладе сейчас 
    var add_dsec = add_sec / 10; // кол-во добавки в 100 мсек
   
    rows.push({
       id: user_contribution.id,
       v_start: v_start,
       add_dsec: add_dsec
    });
});

setInterval(function() {
    $('tr.rowTable').each(function(i,e) {
        var user_contribution = $(e).data('row');
        var row = getRow(user_contribution.id);
        $(e).find('.js-counter').html(row.v_start.toFixed(10));
        for(var i2=0; i2 < rows.length; i2++) {
            if (rows[i2].id == user_contribution.id) {
                rows[i2].v_start += row.add_dsec;
            }
        }
    });
}, 100);

JS
        )
        ?>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/cabinet-contribution/view' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\UserContribution::find()
                    ->where([
                        'user_id' => Yii::$app->user->id,
                        'status'  => 0,
                    ])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => [
                        'id'  => $item['id'],
                        'row' => \yii\helpers\Json::encode($item),
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns' => [
                [
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'attribute'     => 'id',

                ],
                [
                    'header'        => 'Наименование',
                    'headerOptions' => ['style' => 'width: 15%;'],
                    'content'       => function ($i) {
                        $c = \common\models\Contribution::findOne($i->contribution_id);
                        return $c->name;
                    },
                ],
                [
                    'header'        => 'Сумма',
                    'headerOptions' => ['style' => 'width: 15%;'],
                    'content'       => function ($i) {
                        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
                        $m = bcdiv($i->amount, pow(10, $c->decimals), $c->decimals);

                        return Yii::$app->formatter->asDecimal($m, $c->decimals);
                    },
                ],
                [
                    'header'        => 'Создано',
                    'headerOptions' => ['style' => 'width: 10%;'],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'        => 'Окончание',
                    'headerOptions' => ['style' => 'width: 15%;'],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'finish_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::forward($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'         => 'Счетчик',
                    'headerOptions'  => ['style' => 'width: 20%;'],
                    'contentOptions' => ['class' => 'js-counter'],
                    'content'        => function ($item) {
                        return '';
                    },
                ],
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>

</div>