<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $contribution      \common\models\Contribution */
/** @var $user_contribution \common\models\UserContribution */

$this->title = 'Просмотр стейкинг-пакета';

\avatar\assets\Notify::register($this);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">
        <?php

        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
        $amount_m = bcdiv($user_contribution->amount, pow(10, $c->decimals), $c->decimals);
        ?>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $user_contribution,
            'attributes' => [
                    'id',
                    [
                        'label' => 'Наименование',
                        'value' => $contribution->name,
                    ],
                    [
                        'label' => 'Начало',
                        'format' => 'datetime',
                        'value' => $user_contribution->created_at,
                    ],
                    [
                        'label' => 'Окончание',
                        'format' => 'datetime',
                        'value' => $user_contribution->finish_at,
                    ],
                    [
                        'label' => 'Сумма',
                        'value' => Yii::$app->formatter->asDecimal($amount_m, $c->decimals),
                    ],
            ]
        ]) ?>


        <?php
        $diff = time() - $user_contribution->created_at; // кол-во сек после открытия вклада
        $this->registerJs(<<<JS
var v = {$amount_m}; // сумма при открытии вклада
var percent_all = (({$contribution->percent} / 1000) / 30) * ($contribution->time  / (60*60*24)); // процент за весь периуд
var all_add = v * (percent_all / 100); // сумма прибавления за весь срок
var time = $contribution->time; // кол-во сек во вкладе
var add_sec = all_add / time; // кол-во добавки в сек
var v_start = v + (add_sec * {$diff} ); // накопленная сумма во вкладе сейчас 
var add_dsec = add_sec / 10; // кол-во добавки в 100 мсек

setInterval(function() {
    $('.js-counter').html(v_start);
    v_start += add_dsec;
}, 100);

JS
)
        ?>
        <p>Сейчас: <span class="js-counter"></span></p>

        <hr>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>