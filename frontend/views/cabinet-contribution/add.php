<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this  yii\web\View */
/* @var $form  yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\CabinetContributionAdd */

$this->title = 'Добавить стейкинг-пакет';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <style>
        table.table20210122 th {
            font-size: 80%;
        }
        table.table20210122 td {
            font-size: 80%;
        }
    </style>
    <div class="col-lg-8">

        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();


JS
        );
        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Contribution::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['amount' => SORT_ASC]],
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover table20210122',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns' => [
                'name:text:Наименование',
                [
                    'header'  => 'Время, дней',
                    'content' => function ($item) {
                        return $item['time'] / (60 * 60 * 24);
                    },
                ],
                [
                    'header'  => 'Время , мес',
                    'content' => function ($item) {
                        return  Yii::$app->formatter->asDecimal($item['time'] / (60 * 60 * 24 * 30), 2);
                    },
                ],
                [
                    'header'         => 'Мин сумма, от, включительно',
                    'contentOptions' => ['class' => 'text-right', 'nowrap' => 'nowrap'],
                    'headerOptions'  => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
                        $cExt = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::NEIRO);
                        return Yii::$app->formatter->asDecimal($item['amount'] / pow(10, $c->decimals), 0) . ' NEIRO' . '<br>' .
                            Html::tag('span',
                                '~ ' . Yii::$app->formatter->asDecimal(($item['amount'] / pow(10, $c->decimals)) * $cExt->price_usd, 2) . ' USDT',
                                ['style' => 'font-size:70%; color: #888;']
                                )
                            ;
                    },
                ],
                [
                    'header'         => 'Макс сумма, до',
                    'contentOptions' => ['class' => 'text-right', 'nowrap' => 'nowrap'],
                    'headerOptions'  => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
                        $cExt = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::NEIRO);
                        return Yii::$app->formatter->asDecimal($item['amount_max'] / pow(10, $c->decimals), 0) . ' NEIRO' . '<br>' .
                            Html::tag('span',
                                '~ ' . Yii::$app->formatter->asDecimal(($item['amount_max'] / pow(10, $c->decimals)) * $cExt->price_usd, 2) . ' USDT',
                                ['style' => 'font-size:70%; color: #888;']
                            )

                            ;
                    },
                ],
                [
                    'header'         => 'Процентов за 30 дней',
                    'contentOptions' => ['class' => 'text-right'],
                    'headerOptions'  => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['percent'] / 1000, 1);
                    },
                ],
                [
                    'header'         => 'Процентов в день',
                    'contentOptions' => ['class' => 'text-right'],
                    'headerOptions'  => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        return Yii::$app->formatter->asDecimal($item['percent'] / (1000 * 30), 3);
                    },
                ],
                [
                    'header'         => 'Процентов за весь срок',
                    'contentOptions' => ['class' => 'text-right'],
                    'headerOptions'  => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        return Yii::$app->formatter->asDecimal(($item['time'] / (60 * 60 * 24)) * ($item['percent'] / (1000 * 30)), 2);
                    },
                ],
            ]
        ]) ?>

        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-contribution/index';
    }).modal();
}
JS

        ]); ?>
        <?= $form->field($model, 'amount')->label('Сумма') ?>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']); ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>