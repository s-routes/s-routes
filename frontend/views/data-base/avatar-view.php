<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\AvatarHash */

$this->title = $model->id . '.' . $model->hash;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                'id',
                'image:image:Аватар',
                [
                    'attribute' => 'created_at',
                    'format'    => ['datetime', 'php:c'],
                    'label' => 'Создано',
                ],
                'data:text:Данные',
                'hash:text:SHA256',
            ],
        ]) ?>
    </div>
</div>
