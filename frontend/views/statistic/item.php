<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 17.11.2016
 * Time: 2:44
 */

/* @var $this yii\web\View */
/* @var $currency \common\models\avatar\Currency */

$this->title = $currency->title;

$options = [
    'src'   => $currency->image,
    'class' => 'img-circle',
    'title' => $currency->title,
    'style' => \yii\helpers\Html::cssStyleFromArray([
        'width'     => '100%',
        'max-width' => '308px',
    ]),
];


?>

<div class="container">
    <h2 class="page-header text-center"><?= $this->title ?></h2>

    <p class="text-center" style="font-weight: bold;">
        <?= Yii::$app->formatter->asDecimal($currency->price_usd, 4) ?> <span style="color: #ccc;">$</span>
    </p>
    <p class="text-center">
        <?= \yii\helpers\Html::tag('img', null, $options) ?>
    </p>

    <div class="row" style="margin: 20px 0px 20px 0px;">
        <?php

        $rows = \common\models\ChartPoint::find()
            ->select([
                'usd',
                'time',
            ])
            ->where(['>', 'time', time() - (60 * 60 * 24 * 30)])
            ->andWhere(['currency_id' => $currency->id])
            ->all();
        $rowsJson = \yii\helpers\Json::encode($rows);
        $this->registerJs(<<<JS
        Highcharts.setOptions({
            global: {
                timezoneOffset: -3 * 60
            }
        });
        var rows2 = {$rowsJson};
        var newRowsUsd = [];
        for(i = 0; i < rows2.length; i++)
        {
            var item = rows2[i];
            newRowsUsd.push({
                x: new Date(item.time * 1000),
                y: item.usd
            });
        }
JS
        );
        ?>
        <?= \common\widgets\HighCharts\HighCharts::widget([
            'chartOptions' => [
                'chart'       => [
                    'zoomType' => 'x',
                    'type'     => 'spline',
                ],
                'title'       => [
                    'text' => 'График',
                ],
                'subtitle'    => [
                    'text' => 'Выделите область для изменения масштаба',
                ],
                'xAxis'       => [
                    'type' => 'datetime',
                ],
                'yAxis'       => [
                    [
                        'title' => [
                            'text' => 'Количество',
                        ],
                    ],
                ],
                'legend'      => [
                    'enabled' => true,
                ],
                'tooltip'     => [
                    'crosshairs' => true,
                    'shared'     => true,
                ],
                'plotOptions' => [
                    'series' => [
                        'turboThreshold' => 0,
                    ],
                ],
                'series'      => [
                    [
                        'type' => 'spline',
                        'name' => 'USD',
                        'data' => new \yii\web\JsExpression('newRowsUsd'),
                    ],
                ],
            ],
        ]);
        ?>
    </div>


</div>