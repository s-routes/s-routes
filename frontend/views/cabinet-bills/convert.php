<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $id int db.currency.id */

$this->title = 'Конвертация';




$id1 = $id;
?>





<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <?php
            $params = \common\models\Config::get('ConvertParams');
            $params = \yii\helpers\Json::decode($params);
            $rows = [];
            foreach ($params as $p) {
                $id = $p['id'];
                $i = explode('_', $id);
                if ($i[0] == $id1) {
                    if ($p['apply'] == 1) {
                        $cFrom = Currency::findOne($i[0]);
                        $cExt = Currency::findOne($i[1]);
                        $rows[] = Html::a($cFrom->code . ' > ' . $cExt->code, ['cabinet-wallet/convert', 'from' => $i[0], 'to' => $i[1]], ['class' => 'btn btn-default']);
                    }
                }
            }
            ?>
            <?= join('<br>', $rows) ?>

        </div>
    </div>
</div>