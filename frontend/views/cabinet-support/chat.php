<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */


$this->title = Yii::t('c.zpiXUyz0nd', 'Чат поддержки');

$user_id = Yii::$app->user->id;
$supportChat = \common\models\NeironSupport::findOne(['user_id' => $user_id]);
if (is_null($supportChat)) {
    $chatRoom = \common\models\ChatRoom::add(['last_message' => null]);
    $supportChat = \common\models\NeironSupport::add([
        'room_id' => $chatRoom->id,
        'user_id' => $user_id,
    ]);
}
$room_id = $supportChat->room_id;

\avatar\assets\Notify::register($this);

?>

<?= $this->render('@frontend/views/blocks/modalHelp.php', ['id' => 20]) ?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
    </div>

    <div class="col-lg-8">
        <?= $this->render('@shop/views/cabinet-exchange/chat', [
            'room_id'  => $room_id,
            'user_id'  => $user_id,
            'send_url' => '/cabinet-support/send',
        ]); ?>

    </div>

    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>
