<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Управление новыми QR кодами';

$sort = new \yii\data\Sort([
    'attributes'   => [
        'id',
        'is_used',
        'created_at',
    ],
    'defaultOrder' => [
        'created_at' => SORT_DESC,
    ],
])
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\avatar\QrCode::find(),
                'sort'       => $sort,
                'pagination' => [
                    'pageSize' => 1000,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'style' => 'width: auto;',
                'id'    => 'tableTransaction',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => [
                        'id'       => $item['id'],
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];

                return $data;
            },
            'columns'      => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    // you may configure additional properties here
                ],
                'id',
                [
                    'header'  => 'code',
                    'content' => function ($item) {
                        return Html::tag('code', $item['code']);
                    },
                ],
                [
                    'header'  => 'Создан',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'  => 'Использован?',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'is_used', 0);
                        if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                ],
                [
                    'header'  => 'Счет',
                    'content' => function ($item) {
                        $card = \common\models\Card::findOne($item['card_id']);
                        try {
                            $billing = \common\models\avatar\UserBill::findOne(['card_id' => $card->id]);
                        } catch (Exception $e) {
                            return Html::tag('code', '- Ошибка -');

                        }
                        return Html::tag('code', $billing->address);
                    },
                ],
                [
                    'header'  => 'Номер карты',
                    'content' => function ($item) {
                        $card = \common\models\Card::findOne($item['card_id']);
                        if (is_null($card)) {
                            return Html::tag('code', '- Ошибка -');
                        }

                        return Html::tag('code', $card->number);
                    },
                ],
                [
                    'header'  => 'Карты',
                    'content' => function ($item) {
                        return
                            Html::a(
                                'Карты',
                                ['admin-qr-code/cards', 'id' => $item['id']],
                                [
                                    'class' => 'btn btn-info btn-xs',
                                    'data'  => [
                                        'id' => $item['id'],
                                    ],
                                ]
                            );
                    },
                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $card = \common\models\Card::findOne($item['card_id']);

                        try {
                            $billing = \common\models\avatar\UserBill::findOne(['card_id' => $card->id]);
                        } catch (Exception $e) {
                            return '';
                        }

                        try {
                            $user = \common\models\UserAvatar::findOne($billing->user_id);
                        } catch (Exception $e) {
                            return '';
                        }
                        $rows = [];
                        $rows [] = Html::tag($user->getName2());
                        $rows [] = Html::button('Отсоединить', ['class' => 'btn btn-default buttonDisconnect', 'data' => ['id' => $item['id']]]);

                        return join('', $rows);
                    },
                ],
            ],

        ]) ?>
        <?php
        $this->registerJs(<<<JS
$('.buttonNew').click(function(e) {
    $('#myModal').modal();
});

$('.buttonNewEthereum').click(function(e) {
    $('#NewEthereum').modal();
});


$('.rowTable').click(function(e) {
    var find = $(this).find('input');
    var input = find[0];
    if ($(input).is(':checked')) {
        $(input).prop('checked', false);
    } else {
        $(input).prop('checked', true);
    }
});
$('.buttonDisconnect').click(function(e) {
    ajaxJson({
        url: '/admin-qr-code/disconnect',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            window.location.reload();
        },
        errorScript: function(ret) {
            alert('Ошибка');
        }
    });
});
$('.buttonMark').click(function(e) {
    var b = $(this);
    ajaxJson({
        url: '/admin-qr-code/mark',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            b.removeClass('btn-default');
            b.addClass('btn-success');
        },
        errorScript: function(ret) {
            alert('Ошибка');
        }
    });
});
$('.buttonDownloadList').click(function(e) {
    var list = $('input[name="selection[]"]:checked');
    if (list.length == 0) {
        alert('Нужно выбрать хотябы один элемент');
        return;
    }
    var ids = [];
    list.each(function(i,v) {
      ids.push($(v).val());
    });
    window.location = '/admin-qr-code/download' + '?' + 'ids' + '=' + ids.join(',')
});
$('.buttonDownloadArchive').click(function(e) {
    var list = $('input[name="selection[]"]:checked');
    if (list.length == 0) {
        alert('Нужно выбрать хотябы один элемент');
        return;
    }
    var ids = [];
    list.each(function(i,v) {
      ids.push($(v).val());
    });
    window.location = '/admin-qr-code/download-archive' + '?' + 'ids' + '=' + ids.join(',')
});



JS
        );
        ?>
        <hr>
        <p>
<!--            <button class="btn btn-default buttonNew">Создать BTC</button>-->
            <button class="btn btn-default buttonNewEthereum">Создать ETH</button>
            <button class="btn btn-default buttonDownloadList">Скачать список</button>
            <a class="btn btn-default" href="/admin-qr-code/cards3">Печать QR code</a>
        </p>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Введите кол-во кошельков для создания</h4>
            </div>
            <div class="modal-body">
                <div class="form-group required">
                    <input  type="text" class="form-control number">
                </div>
                <div class="form-group">
                    <div class="progress hide" id="counter-progress">
                        <div class="progress-bar counter" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width: 0%;">
                            0%
                        </div>
                    </div>
                    <p class="hide progressText"><code class="progress-text"></code></p>
                </div>
            </div>
            <div class="modal-footer">
<?php
$this->registerJs(<<<JS
$('.buttonCreate').click(function(e) {
    var m = $('#myModal');
    m.find('.counter-progress').removeClass('hide');
    m.find('.progressText').removeClass('hide');
    var n = parseInt(m.find('.number').val());
    m.find('.progress-text').html(0 + '/' + n);
    $(this).off('click').attr('disabled', 'disabled');
    functionCalc(0, n, m);
});


var functionCalc = function(i, end, modal) {
    
    ajaxJson({
        url: '/admin-qr-code/new',
        success: function(ret) {
            i++;
            var percent = parseInt((i/end)*100);
            modal.find('.counter').html(percent + '%').css('width', percent + '%').data('valuenow', percent);
            modal.find('.progress-text').html(i + '/' + end);
            if (i < end) {
                functionCalc(i, end, modal);
            } else {
                m.on('hidden.bs.modal', function(e) {
                    window.location.reload();
                }).modal('hide');
            }
        },
        errorScript: function(ret) {
            var percent = parseInt((i/end)*100);
            modal.find('.counter').html(percent + '%').css('width', percent + '%').data('valuenow', percent);
            modal.find('.progress-text').html(i + '/' + end);
            if (i < end) {
                functionCalc(i, end, modal);
            } else {
                m.on('hidden.bs.modal', function(e) {
                    window.location.reload();
                }).modal('hide');
            }
        }
    });
};

JS
);
?>
                <button type="button" class="btn btn-primary buttonCreate" style="width: 100%;">Создать</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="NewEthereum" tabindex="-1" role="dialog" aria-labelledby="NewEthereumLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Введите кол-во кошельков для создания</h4>
            </div>
            <div class="modal-body">
                <div class="form-group required">
                    <input type="text" class="form-control number">
                </div>
                <div class="form-group">
                    <div class="progress hide counter-progress">
                        <div class="progress-bar counter" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                             aria-valuemax="100" style="width: 0%;">
                            0%
                        </div>
                    </div>
                    <p class="hide progressText"><code class="progress-text"></code></p>
                </div>
            </div>
            <div class="modal-footer">
<?php
$this->registerJs(<<<JS
$('.buttonCreateEthereum').click(function(e) {
    var m = $('#NewEthereum');
    m.find('.counter-progress').removeClass('hide');
    m.find('.progressText').removeClass('hide');
    var n = parseInt(m.find('.number').val());
    console.log(n);
    m.find('.progress-text').html(0 + '/' + n);
    $(this).off('click').attr('disabled', 'disabled');
    functionCalc2(0, n, m);
});
var BillingList2 = [];

var functionCalc2 = function(i, end, modal) {
    console.log([i,end]);
    
    ajaxJson({
        url: '/admin-qr-code/new-ethereum',
        data: {
            design_id: 10,
            secret_num: i
        },
        success: function(ret) {
            i++;
            var percent = parseInt((i/end)*100);
            
            console.log(i);
            console.log(percent);
            modal.find('.counter').html(percent + '%').css('width', percent + '%').data('valuenow', percent);
            
            modal.find('.progress-text').html(i + '/' + end);
            BillingList2.push(ret.billing.id);
            if (i < end) {
                functionCalc2(i, end, modal);
            } else {
                modal.on('hidden.bs.modal', function(e) {
                    $('#ModalInfo').on('hidden.bs.modal', function(e) {
                        window.location.reload();
                    }).modal();
                }).modal('hide');
            }
        },
        errorScript: function(ret) {
            var percent = parseInt((i/end)*100);
            modal.find('.counter').html(percent + '%').css('width', percent + '%').data('valuenow', percent);
            modal.find('.progress-text').html(i + '/' + end);
            if (i < end) {
                functionCalc2(i, end, modal);
            } else {
                modal.on('hidden.bs.modal', function(e) {
                    $('#ModalInfo').on('hidden.bs.modal', function(e) {
                        window.location.reload();
                    }).modal();
                }).modal('hide');
            }
        }
    });
};

JS
);
?>
                <button type="button" class="btn btn-primary buttonCreateEthereum" style="width: 100%;">Создать</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ModalInfo" tabindex="-1" role="dialog" aria-labelledby="ModalInfo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-center" id="myModalLabel">Уведомление</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <p class="text-center">Кошельки созданы успешно</p>
                    <?php
                    $this->registerJs(<<<JS
$('.buttonDownload').click(function() {
    window.location = '/admin-qr-code/download?ids=' + BillingList2.join(',');
});
JS
);

                    ?>
                    <p class="text-center"><button class="btn btn-primary buttonDownload">Скачать CSV</button></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default buttonDone" style="width: 100%;" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>