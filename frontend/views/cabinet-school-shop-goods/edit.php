<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\shop\Product */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <div class="row">
            <div class="col-lg-8">

                <?php
                $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-school-shop-goods/index';
    }).modal();
}
JS
                    ,
                ]); ?>

                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'price')->hint('NEIRO') ?>
                <?= $form->field($model, 'image') ?>
                <?= $form->field($model, 'content')->textarea(['rows' => 5]) ?>
                <hr>
                <a href="/admin-goods-image/index?id=<?= $model->id ?>" class="btn btn-default">Изображения (<?= \common\models\shop\ProductImage::find()->where(['product_id' => $model->id])->count() ?>)</a>
                <a href="/admin-goods-video/index?id=<?= $model->id ?>" class="btn btn-default">Видео (<?= \common\models\shop\ProductVideo::find()->where(['product_id' => $model->id])->count() ?>)</a>
                <hr>

                <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Указать']); ?>


            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>