<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Админка';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <h2 class="page-header">Магазин</h2>

        <p><a class="btn btn-default" href="/admin-market-report/index">Отчет по MARKET</a></p>
        <p><a class="btn btn-default" href="/cabinet-school-pay-systems/index">Управление платежными системами</a></p>
        <p><a class="btn btn-default" href="/admin-oferta/index">Оферта</a></p>
        <p><a class="btn btn-default" href="/admin-letter-list/index">Письма с лендинга</a></p>
        <p><a class="btn btn-default" href="/admin-shop/index">Магазины</a></p>
        <p><a class="btn btn-default" href="/manager-request-list/index">Все заказы</a></p>
        <p><a class="btn btn-default" href="/admin-comission-bank/index">Комиссия на эквайринг</a></p>

        <h2 class="page-header">Заказ серебра</h2>

        <p><a class="btn btn-default" href="/cabinet-school-shop-requests/index">Заявки в магазин</a></p>
        <p><a class="btn btn-default" href="/cabinet-school-shop-goods/index">Товары</a></p>
        <p><a class="btn btn-default" href="/cabinet-school-shop-catalog/index">Каталог</a></p>
        <p><a class="btn btn-default" href="/admin-korobka/index">Коробочки</a></p>

        <h2 class="page-header">P2P обменник</h2>

        <p><a class="btn btn-default" href="/admin-prizm-limit/index">PRIZM лимит</a></p>
        <p><a class="btn btn-default" href="/admin-contribution/index">Отчет по NEIRON</a></p>
        <p><a class="btn btn-default" href="/admin-contribution-lot/index">Отчет по LOT</a></p>
        <p><a class="btn btn-default" href="/admin-comission/index">% для комиссий на вывод</a></p>
        <p><a class="btn btn-default" href="/admin-comission2/index">Таблица конвертации</a></p>
        <p><a class="btn btn-default" href="/admin-country/index">Страны</a></p>
        <p><a class="btn btn-default" href="/admin-deal-list/index">Все сделки</a></p>
        <p><a class="btn btn-default" href="/admin-trust/index">Все доверенные представители</a></p>
        <p>Конвертация:<br>
            <a class="btn btn-default btn-xs" href="/admin-convert/index">Alternative Coins</a>
            <br>
            <a class="btn btn-default btn-xs" href="/admin-convert2/index">Stable Coins & Service Coins</a>
        </p>
        <p><a class="btn btn-default" href="/admin-support/index">ТехПоддержка</a></p>
        <p><a class="btn btn-default" href="/admin-users/index">Пользователи</a></p>

        <h2 class="page-header">Другое</h2>
        <p><a class="btn btn-default" href="/admin-page/index">Страницы</a> <a class="btn btn-default" href="/admin-page/file-delete">Удалить файл</a></p>
        <p><a class="btn btn-default" href="/admin-insrukciya/index">Инструкции</a></p>

        <p><a class="btn btn-default" href="/admin-pay/index">Все платежные системы</a></p>
        <p><a class="btn btn-default" href="/admin-news/index">Новости</a></p>
        <p><a class="btn btn-default" href="/admin-developer/index">Документация</a></p>
        <p><a class="btn btn-default" href="/admin/technical-pause">Техническая пауза</a></p>
        <p><a class="btn btn-default" href="/admin-cloudflare/index">Управление кластером</a></p>
        <p><a class="btn btn-default" href="/admin-notify/index">Возможность админу вывешивать и убирать транспарант предупреждения для клиентов</a></p>

        <h2 class="page-header">Переводы</h2>
        <p><a href="https://ru.wikipedia.org/wiki/%D0%9A%D0%BE%D0%B4%D1%8B_%D1%8F%D0%B7%D1%8B%D0%BA%D0%BE%D0%B2" >Коды языков</a></p>
        <p><a href="/admin-languages/index" class="btn btn-default">Языки</a></p>
        <p><a href="/languages/index" class="btn btn-default">Переводы</a></p>
        <p><a href="/languages-admin/group" class="btn btn-default">Группа</a></p>
        <p><a href="/languages-admin/config" class="btn btn-default">Настройки</a></p>

        <h2 class="page-header">Процессинг</h2>
        <p><a href="/admin-currency/index" class="btn btn-default">Валюты</a></p>
        <p><a href="/admin-currency-int/index" class="btn btn-default">Монеты</a></p>
        <p><a href="/admin-wallets/index" class="btn btn-default">Кошельки</a></p>
        <p><a href="/admin-wallets/report" class="btn btn-default">Отчет</a></p>

        <h2 class="page-header">Системное</h2>
        <p><a href="/log/index" class="btn btn-default">лог index</a></p>
        <p><a href="/log/log-db" class="btn btn-default">лог DB</a></p>
        <p><a href="/admin-monitoring/index" class="btn btn-default">Мониторинг</a></p>
        <p><a href="/admin-monitoring/timer" class="btn btn-default">Мониторинг времени исполнения</a></p>
        <p><a href="/admin-block/index" class="btn btn-default">Ручная блокировка</a></p>
    </div>
</div>