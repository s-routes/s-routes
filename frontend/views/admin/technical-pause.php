<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Админка';


\avatar\assets\SocketIO\Asset::register($this);
$serverName = \avatar\assets\SocketIO\Asset::getHost();

$t = Yii::$app->params['technical-pause']['start'] * 1000;
$this->registerJs(<<<JS

if (typeof socket == 'undefined') {
    var socket = io.connect('{$serverName}');
}

$('.buttonStart').click(function (ret) {
    socket.emit('technical-pause-alert', {$t});
    var noty = new Noty({
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Успешно'
    });
    noty.show();
});
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <p><button class="btn btn-primary buttonStart">Запустить счетчик</button>
    </div>
</div>