<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\NewsItem */

$this->title = 'Добавить новость';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <div class="alert alert-success">
                Успешно добавлено.
            </div>

            <?php $item = \common\models\NewsItem::findOne($id); ?>
            <?php if ($item->is_send == 0) { ?>
                <?php
                $this->registerJs(<<<JS
$('.buttonAddSiteUpdate').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите рассылку')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-news/subscribe' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.remove();
                }).modal();
            }
        });
    }
});
JS
                );
                ?>
                <button class="btn btn-info buttonAddSiteUpdate" data-id="<?= $id ?>">Сделать рассылку</button>
                <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Информация</h4>
                            </div>
                            <div class="modal-body">
                                Успешно!
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <a class="btn btn-info" href="/admin-news/index">Все новости</a>
        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name') ?>
                    <?= $model->field($form, 'description')->textarea(['rows' => 5]) ?>
                    <?= $model->field($form, 'content') ?>
                    <?= $model->field($form, 'image') ?>


                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Добавить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
