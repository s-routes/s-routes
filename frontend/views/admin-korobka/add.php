<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\AdminKorobka */

$this->title = 'Добавить коробку';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/admin-korobka/index';
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'image') ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Добавить']); ?>
    </div>
</div>
