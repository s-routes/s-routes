<?php


/** @var \yii\web\View $this */

use common\models\Language;
use common\models\language\Category;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = 'Переводы';

function getLink($params = [])
{
    $options = Yii::$app->request->get();
    $options[] = 'languages/index';
    ArrayHelper::merge($options, $params);

    return \yii\helpers\Url::to($options);
}

?>

<div  style="margin: 0px 50px 0px 50px;">

<h2 class="page-header">Переводы</h2>

<div class="row">
<div class="col-lg-2">
    <ul>
        <?= $this->render('_tree', ['rows' => Category::getRows(['selectFields' => 'id,name,code'])]); ?>
    </ul>

</div>

</div>
</div>
