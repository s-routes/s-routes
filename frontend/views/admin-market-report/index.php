<?php

/** @var $this \yii\web\View  */
/** @var $model \avatar\models\forms\AdminContribution  */

use common\models\ConvertOperation;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Отчет по MARKET';

function groupBy($rows, $attrName)
{
    $offer_user_id_list = ArrayHelper::map($rows, $attrName, function ($i) { return $i;});
    $rows2 = [];
    foreach ($offer_user_id_list as $offer_user_id => $data) {
        $item = [
            $attrName => $offer_user_id,
        ];
        $items = [];
        foreach ($rows as $row) {
            if ($row[$attrName] == $offer_user_id) $items[] = $row;
        }
        $item['rows'] = $items;
        $rows2[] = $item;
    }

    return $rows2;
}

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


$str838 = Yii::t('c.BBXvjwPu8f', 'Скопировано');

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str838}'
    }).show();

});

JS
);

?>

<div style="margin: 0px 50px 0px 50px;">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-6">
                <?php $form = \yii\bootstrap\ActiveForm::begin([

                ]) ?>
                <?= $form->field($model, 'date_start')->widget('\yii\jui\DatePicker', ['options' => ['class' => 'form-control']]) ?>
                <?= $form->field($model, 'date_end')->widget('\yii\jui\DatePicker', ['options' => ['class' => 'form-control']]) ?>
                <hr>
                <?= Html::submitButton('Показать', [
                        'class' => 'btn btn-success',
                        'style' => 'width: 100%',
                ]) ?>

                <?php \yii\bootstrap\ActiveForm::end() ?>
                <p>
                    первая дата считается от 00:00:00 включительно<br>
                    вторая дата учитывается до начала следующего дня (00:00:00)<br>
                    фильтруется дата начала сделки
                </p>

            </div>
            <div class="col-lg-6">
                <?php
                $cio1 = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::NEIRO]);
                ?>
                <?php
                $date_start = null;
                $date_end = null;
                if (\yii\helpers\StringHelper::startsWith(Yii::$app->formatter->dateFormat, 'php')) {
                    $dateFormat = substr(Yii::$app->formatter->dateFormat, 3);
                } else {
                    $dateFormat = \yii\helpers\FormatConverter::convertDateIcuToPhp(Yii::$app->formatter->dateFormat);
                }
                if (!\cs\Application::isEmpty($model->date_start)) {

                    $d = DateTime::createFromFormat($dateFormat . ' H:i:s', $model->date_start . ' 00:00:00');
                    $date_start = $d->format('U');
                }
                if (!\cs\Application::isEmpty($model->date_end)) {
                    $d = DateTime::createFromFormat($dateFormat . ' H:i:s', $model->date_end . ' 00:00:00');
                    $d->add(new DateInterval('P1D'));
                    $date_end = $d->format('U');
                }


                $rowsQuery = \common\models\PurchasePacket::find()
                    ->asArray();
                if (!is_null($date_start)) {
                    $rowsQuery->andWhere(['>=', 'created_at', $date_start]);
                }
                if (!is_null($date_end)) {
                    $rowsQuery->andWhere(['<', 'created_at', $date_end]);
                }

                $c1 = $rowsQuery->select('sum(amount)')->scalar();
                ?>
                <p>Сумма пакетов: <?= Yii::$app->formatter->asDecimal($c1 / 100) ?>, 30% от этого <?= Yii::$app->formatter->asDecimal($c1 * 0.3 / 100) ?>, нам от этого <?= Yii::$app->formatter->asDecimal($c1 * 0.7 / 100) ?></p>
                <?php

                $rowsQuery = \common\models\ConvertOperation::find()
                    ->where(['to_currency_id' => \common\models\piramida\Currency::MARKET])
                    ->asArray();
                if (!is_null($date_start)) {
                    $rowsQuery->andWhere(['>=', 'created_at', $date_start]);
                }
                if (!is_null($date_end)) {
                    $rowsQuery->andWhere(['<', 'created_at', $date_end]);
                }
                $c1 = $rowsQuery->select('sum(amount_to)')->scalar();
                ?>
                <p>Сумма конвертаций: <?= Yii::$app->formatter->asDecimal($c1 / 100) ?>, 2,5% от этого <?= Yii::$app->formatter->asDecimal($c1 * 0.025 / 100) ?>, нам от этого <?= Yii::$app->formatter->asDecimal($c1 * 0.975 / 100) ?></p>

            </div>
        </div>




    <h2 class="page-header">Покупка пакетов</h2>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS

$('.rowTable').click(function() {
    //window.location = '/cabinet-exchange/deal-action' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    $rowsQuery = \common\models\PurchasePacket::find()
        ->asArray();
    if (!is_null($date_start)) {
        $rowsQuery->andWhere(['>=', 'created_at', $date_start]);
    }
    if (!is_null($date_end)) {
        $rowsQuery->andWhere(['<', 'created_at', $date_end]);
    }
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => $rowsQuery
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC],
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns' => [
            'id',
            \common\services\FrontEnd::ColumnTxId('TxID', 'tx_id'),
            [
                'header'  => 'Сумма',
                'content' => function ($item) {
                    return Yii::$app->formatter->asDecimal(
                        \common\models\piramida\Currency::getValueFromAtom($item['amount'], \common\models\piramida\Currency::MARKET)
                    );
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    return Html::tag('span',
                        'MARKET',
                        [
                            'class' => "label label-info",
                        ]
                    );
                },
            ],
            \common\services\FrontEnd::ColumnUserAvatar('Пользователь', 'user_id'),
            \common\services\FrontEnd::ColumnDateTime('Создано', 'created_at'),
        ]

    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    <h2 class="page-header">Конвертации</h2>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $rowsQuery = \common\models\ConvertOperation::find()
        ->where(['to_currency_id' => \common\models\piramida\Currency::MARKET])
        ->asArray();
    if (!is_null($date_start)) {
        $rowsQuery->andWhere(['>=', 'created_at', $date_start]);
    }
    if (!is_null($date_end)) {
        $rowsQuery->andWhere(['<', 'created_at', $date_end]);
    }
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => $rowsQuery
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort'       => ['defaultOrder' => ['id' => SORT_DESC]],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Пользователь',
                'content' => function ($item) {
                    try {
                        $userBill = \common\models\avatar\UserBill::findOne(['address' => $item['wallet_from_id']]);
                    } catch (Exception $e) {
                        return '';
                    }
                    $user = \common\models\UserAvatar::findOne($userBill->user_id);
                    $i = $user->getAvatar();

                    return Html::img($i, [
                        'class'  => "img-circle",
                        'data'   => ['toggle' => 'tooltip'],
                        'title'  => $user->getName2(),
                        'width'  => 40,
                        'height' => 40,
                        'style'  => 'margin-bottom: 0px;',
                    ]);
                },
            ],
            \common\services\FrontEnd::ColumnDateTime('Создано', 'created_at'),
            [
                'header'         => 'amount',
                'headerOptions'  => ['class' => 'text-right',],
                'contentOptions' => ['class' => 'text-right',],
                'content'        => function ($item) {
                    $cInt = \common\models\piramida\Currency::findOne($item['from_currency_id']);
                    if (is_null($cInt)) return '';
                    $v = bcdiv($item['amount'], bcpow(10, $cInt->decimals), $cInt->decimals_view);

                    return $v;
                },
            ],
            [
                'header'  => 'from',
                'content' => function ($item) {
                    $cInt = \common\models\piramida\Currency::findOne($item['from_currency_id']);
                    if (is_null($cInt)) return '';
                    $bill = \common\models\avatar\UserBill::findOne(['address' => $item['wallet_from_id']]);

                    return Html::tag('span',
                        Html::a($cInt->code, ['cabinet-wallet/item', 'id' => $bill->id], ['data'  => ['pjax' => 0]]),
                        [
                            'class' => "label label-info",
                            'data'  => [
                                'toggle' => 'tooltip',
                                'pjax' => 0
                            ],
                            'title' => $item['wallet_from_id'],
                        ]
                    );
                },
            ],
            [
                'header'         => 'amount_to',
                'headerOptions'  => ['class' => 'text-right',],
                'contentOptions' => ['class' => 'text-right',],
                'content'        => function ($item) {
                    $cInt = \common\models\piramida\Currency::findOne($item['to_currency_id']);
                    if (is_null($cInt)) return '';
                    $v = bcdiv($item['amount_to'], bcpow(10, $cInt->decimals), $cInt->decimals_view);

                    return $v;
                },
            ],
            [
                'header'  => 'to',
                'content' => function ($item) {
                    $cInt = \common\models\piramida\Currency::findOne($item['to_currency_id']);
                    if (is_null($cInt)) return '';
                    $bill = \common\models\avatar\UserBill::findOne(['address' => $item['wallet_to_id']]);

                    return Html::tag('span',
                        Html::a($cInt->code, ['cabinet-wallet/item', 'id' => $bill->id], ['data'  => ['pjax' => 0]]),
                        [
                            'class' => "label label-info",
                            'data'  => [
                                'toggle' => 'tooltip',
                                'pjax' => 0
                            ],
                            'title' => $item['wallet_to_id'],
                        ]
                    );
                },
            ],
            \common\services\FrontEnd::ColumnTxId('TxID from', 'tx_from_id'),
            \common\services\FrontEnd::ColumnTxId('TxID to', 'tx_to_id'),
            [
                'header'         => 'kurs',
                'headerOptions'  => ['class' => 'text-right',],
                'contentOptions' => ['class' => 'text-right',],
                'content'        => function ($item) {
                    if (\cs\Application::isEmpty($item['kurs'])) return '';
                    return $item['kurs'] / 1000000;
                },
            ],
            'to_price_usd',
            'from_price_usd',
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>
    </div>
