<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $page int */

$this->title = 'Блог';


/** @var \avatar\services\CustomizeService $CustomizeService */
$CustomizeService = Yii::$app->CustomizeService;
$id = $CustomizeService->getParam1('id');

if (in_array($id, [4,5])) $id = 2;

$query = \common\models\BlogItem::find()->where(['company_id' => $id]);
$recordsCount = $query->count();
$itemsPerPage = 12;

?>
<style>
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <?php /** @var \common\models\BlogItem $item */ ?>
    <?php foreach ($query->limit(12)->offset(($page - 1) * 12)->orderBy(['id' => SORT_DESC])->all() as $item) { ?>
        <div class="col-lg-3">
            <p style="height: 100px;"><b><?= $item->name ?></b></p>
            <p>
                <a href="<?= Url::to(['blog/item', 'id' => $item->id]) ?>">
                    <img src="<?= $item->image ?>" width="100%" class="thumbnail"/>
                </a>
            </p>
            <p style="color: #ccc;height: 50px;"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y') ?></p>
            <?= $item->description ?>
        </div>
    <?php } ?>

    <div class="col-lg-12" style="margin-bottom: 100px;">
        <hr>
        <?php
        $pagesInLine = 10;
        $pagesCount = (int)(($recordsCount + ($itemsPerPage - 1)) / $itemsPerPage);
        if ($pagesCount >= $pagesInLine) { // 11 страниц >= 10
            if ($page > 5 && ($page + 4 >= $pagesInLine)) {
                if ($page + 4 <= $pagesCount) {
                    $first = $page - 5;
                    $end = $page + 4;
                } else {
                    $end = $pagesCount;
                    $first = $pagesCount - 9;
                }
            } else {
                if ($page <= 5) {
                    $first = 1;
                    $end = 10;
                } else {
                    $end = $pagesCount;
                    $first = $pagesCount - 9;
                }
            }
        } else {
            $first = 1;
            $end = 1;
        }
        ?>

        <?php if ($pagesCount > 1) { ?>
            <ul class="pagination">
                <?php if ($page == 1) { ?>
                    <li class="prev disabled"><span>«</span></li>
                <?php } else { ?>
                    <li class="prev"><a href="?page=<?= $page - 1 ?>">«</a></li>
                <?php } ?>

                <?php for($i = $first; $i <= $end; $i++) { ?>
                    <?php if ($i == $page) { ?>
                        <li class="active"><a href="?page=<?= $i ?>"><?= $i ?></a></li>
                    <?php } else { ?>
                        <li><a href="?page=<?= $i ?>"><?= $i ?></a></li>
                    <?php } ?>
                <?php } ?>

                <?php if ($page == $pagesCount) { ?>
                    <li class="next disabled"><span>»</span></li>
                <?php } else { ?>
                    <li class="next"><a href="?page=<?= $page + 1 ?>">»</a></li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>


</div>


