<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\BlogItem */

$this->title = $item->name;

?>
<style>
    .contentBlock p, .contentBlock li {
        font-size: 130%;
    }
</style>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
        <p class="text-center" style="color: #888;"><?= Yii::$app->formatter->asDate($item->created_at, 'php:d.m.Y') ?></p>
    </div>
    <div class="col-lg-8 col-lg-offset-2 contentBlock">
        <p>
            <img src="<?= FileUpload::getOriginal($item->image) ?>" width="100%" class="thumbnail"/>
        </p>

        <?= $item->content ?>
        <hr>
        <?= $this->render('../blocks/fbLike') ?>

        <p class="text-center"><?php if (isset($item->link)): ?>
                <?php if ($item->link != ''): ?>
                    <?= Html::a('Ссылка на источник »', $item->link, [
                        'class'  => 'btn btn-primary',
                        'target' => '_blank',

                    ]) ?>
                <?php endif; ?>
            <?php endif; ?><a href="<?= Url::to(['blog/index']) ?>" class="btn btn-default">Назад к блогу</a></p>
        <hr>
        <center>
            <?php
            $description = $item->description;
            if (is_null($description)) {
                $description = \cs\services\Str::sub(strip_tags($item->content), 0, 100);
            } else {
                if ($description == '') {
                    $description = \cs\services\Str::sub(strip_tags($item->content), 0, 100);
                }
            }
            ?>
            <?= $this->render('../blocks/share', [
                'image'       => Url::to(FileUpload::getOriginal($item->image), true),
                'title'       => $this->title,
                'url'         => Url::current([], true),
                'description' => $description,
            ]) ?>
        </center>
        <hr>
        <p>
            <a href="https://www.binance.com/?ref=12151139" target="_blank">
                <img src="/images/controller/blog/item/binance.gif" width="100%" data-toggle="tooltip" title="Биржа Binance - быстрая, минимальная комиссия, много монет, входит в тройку мировых, удобный вход и выход, зарегистрирована в Японии.">
            </a>
        </p>
        <p class="text-center">
            <a href="https://cryptorg.net/?ref=84072" target="_blank">
                <img src="/images/controller/blog/item/robotic.png" width="200" data-toggle="tooltip" title="Торговый бот на криптовалютах - четыре биржи, алгорим усреднения, торгующий в плюс даже на падении, сам страхует сделки.">
            </a>
        </p>
    </div>
</div>


