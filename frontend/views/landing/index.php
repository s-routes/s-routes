<?php


/* @var $this yii\web\View */

$this->title = 'Neiro-n';

$this->registerJs('$(".lang-switcher").css("width", "160px")');
?>



<section class="top-layer">
    <header>
        <div class="container">
            <div class="row align-items-center">

                <div class="col header-contain">
                    <div class="lang-switcher-container">
                        <?php $l = \common\models\Language::findOne(['code' => Yii::$app->language]) ?>
                        <div class="avatar-btn--header-white lang-switcher" style="width: 160px;"><?= $l->title ?></div>
                        <div class="lang-choose-layer">
                            <ul>
                                <?php foreach (\common\models\Language::find()->where(['status' => \common\models\Language::STATUS_DONE])->all() as $l) { ?>
                                    <li><a href="javascript:void(0);" class="buttonLanguageSet" data-code="<?= $l->code ?>"><?= $l->title ?></a></li>
                                <?php } ?>
                                <li role="separator" class="divider"></li>
                                <li><a href="/page/item?id=27">ImTranslator universal</a></li>
                            </ul>
                        </div>
                    </div>

                    <?php if (!Yii::$app->deviceDetect->isMobile()) { ?>
                        <p style="font-size: 100%; color: #888;">Beta ver.20.11.03.1.3.17</p>
                    <?php } ?>

                    <a href="#contacts2" class="avatar-btn--header-login"><?= \Yii::t('c.YqmF2Dqma4', 'Поддержка') ?></a>
                </div>
            </div>
        </div>
    </header>
    <div class="container full-height">
        <div class="row top-layer__row full-height justify-content-center align-items-center">
            <div class="col text-center top-layer-col">
                <?php if (Yii::$app->deviceDetect->isMobile()) { ?>
                    <div class="top-layer__logo reveal">
                        <img src="/images/controller/landing/index/N3-w.png" alt="logo2" style="margin-bottom: 0px;" width="150">
                    </div>

                    <p><a href="/auth/login" class="avatar-btn avatar-btn--white reveal"><?= \Yii::t('c.YqmF2Dqma4', 'Войти') ?> →</a></p>
                    <p style="color: white; font-size: 100%;">
                        <?= \Yii::t('c.YqmF2Dqma4', 'УНИВЕРСАЛЬНЫЕ СЕРВИСЫ НЕЙРО') ?>:</p>
                    <p>• Р2Р-ОБМЕН КРИПТО БЕЗ КОМИССИЙ - АВТОМАТИЗИРОВАННЫЙ EXCHANGE-СЕРВИС ЦИФРОВЫМИ АКТИВАМИ. ПРОСТОЙ,
                        НАДЕЖНЫЙ, ПРИВАТНЫЙ, БЫСТРЫЙ.<br>

                        • КРИПТОРЕГЕНЕРАТИВНАЯ ПЛАТЕЖНАЯ СИСТЕМА - СЕРВИС ОБЕСПЕЧЕНИЯ ONLINE-ПЛАТЕЖЕЙ. ШОПИНГ-КОНСЬЕРЖ.
                        ОПЛАТА ЦИФРОВЫМИ АКТИВАМИ ЛЮБЫХ ТОВАРОВ, УСЛУГ, ПОПОЛНЕНИЕ БАЛАНСОВ СРЕДСТВ КОММУНИКАЦИИ,
                        ПЛАТЕЖЕЙ ЖИЛИЩНО-КОММУНАЛЬНОЙ СФЕРЫ, РЕГИОНАЛЬНЫХ, ФЕДЕРАЛЬНЫХ И ПЕРИОДИЧЕСКИХ ОБЯЗАТЕЛЬНЫХ
                        ПЛАТЕЖЕЙ.<br>

                        • ОБЕСПЕЧЕННЫЙ СЕРЕБРОМ ЦИФРОВОЙ STABLE COIN NEIRON (ТОКЕН НА ДЕЦЕНТРАЛИЗОВАННЫХ БЛОКЧЕЙНАХ
                        ETHEREUM ERC20 И TRON TRС20) - АКТИВНЫЙ И ПАССИВНЫЙ ДОХОД, СТЕЙКИНГ, РОСТ ВАШИХ АКТИВОВ.<br>

                        • СЕРЕБРЯНЫЕ МОНЕТЫ NEIRON (вес 1ozt; чистота AG FINE SILVER 999+) - ИНВЕСТИЦИОННЫЙ ИНСТРУМЕНТ И
                        ОБЕСПЕЧИТЕЛЬНЫЙ АКТИВ ЦИФРОВЫХ ТОКЕНОВ NEIRON - ЗАКАЗ И ДОСТАВКА МОНЕТ ИЗ ЧИСТОГО СЕРЕБРА.
                    </p>

                    <p style="color: white; font-size: 100%;">В БЛИЖАЙШЕЕ ВРЕМЯ ПОРТАЛ БУДЕТ ПОПОЛНЯТЬСЯ:</p>
                    <p>• ДРУГИМИ НУЖНЫМИ И ИНТЕРЕСНЫМИ СЕРВИСАМИ:
                        ИНВЕСТИЦИОННЫМИ, РАЗВЛЕКАТЕЛЬНЫМИ, СОЦИАЛЬНЫМИ, БИЗНЕС-КЕЙСАМИ...</p>
                <?php } else { ?>
                    <div class="top-layer__logo reveal">
                        <img src="/images/controller/landing/index/N3-w.png" alt="logo1" style="margin-bottom: 0px;" width="300">
                    </div>

                    <p><a href="/auth/login" class="avatar-btn avatar-btn--white reveal"><?= \Yii::t('c.YqmF2Dqma4', 'Войти') ?> →</a></p>
                    <p style="color: white; font-size: 150%;">
                        <?= \Yii::t('c.YqmF2Dqma4', 'УНИВЕРСАЛЬНЫЕ СЕРВИСЫ НЕЙРО') ?>:</p>
                    <p>• Р2Р-ОБМЕН КРИПТО БЕЗ КОМИССИЙ - АВТОМАТИЗИРОВАННЫЙ EXCHANGE-СЕРВИС ЦИФРОВЫМИ АКТИВАМИ. ПРОСТОЙ,
                        НАДЕЖНЫЙ, ПРИВАТНЫЙ, БЫСТРЫЙ.<br>

                        • КРИПТОРЕГЕНЕРАТИВНАЯ ПЛАТЕЖНАЯ СИСТЕМА - СЕРВИС ОБЕСПЕЧЕНИЯ ONLINE-ПЛАТЕЖЕЙ. ШОПИНГ-КОНСЬЕРЖ.
                        ОПЛАТА ЦИФРОВЫМИ АКТИВАМИ ЛЮБЫХ ТОВАРОВ, УСЛУГ, ПОПОЛНЕНИЕ БАЛАНСОВ СРЕДСТВ КОММУНИКАЦИИ,
                        ПЛАТЕЖЕЙ ЖИЛИЩНО-КОММУНАЛЬНОЙ СФЕРЫ, РЕГИОНАЛЬНЫХ, ФЕДЕРАЛЬНЫХ И ПЕРИОДИЧЕСКИХ ОБЯЗАТЕЛЬНЫХ
                        ПЛАТЕЖЕЙ.<br>

                        • ОБЕСПЕЧЕННЫЙ СЕРЕБРОМ ЦИФРОВОЙ STABLE COIN NEIRON (ТОКЕН НА ДЕЦЕНТРАЛИЗОВАННЫХ БЛОКЧЕЙНАХ
                        ETHEREUM ERC20 И TRON TRС20) - АКТИВНЫЙ И ПАССИВНЫЙ ДОХОД, СТЕЙКИНГ, РОСТ ВАШИХ АКТИВОВ.<br>

                        • СЕРЕБРЯНЫЕ МОНЕТЫ NEIRON (вес 1ozt; чистота AG FINE SILVER 999+) - ИНВЕСТИЦИОННЫЙ ИНСТРУМЕНТ И
                        ОБЕСПЕЧИТЕЛЬНЫЙ АКТИВ ЦИФРОВЫХ ТОКЕНОВ NEIRON - ЗАКАЗ И ДОСТАВКА МОНЕТ ИЗ ЧИСТОГО СЕРЕБРА.
                    </p>

                    <p style="color: white; font-size: 150%; margin-top: 50px;">В БЛИЖАЙШЕЕ ВРЕМЯ ПОРТАЛ БУДЕТ ПОПОЛНЯТЬСЯ:</p>
                    <p>• ДРУГИМИ НУЖНЫМИ И ИНТЕРЕСНЫМИ СЕРВИСАМИ:
                        ИНВЕСТИЦИОННЫМИ, РАЗВЛЕКАТЕЛЬНЫМИ, СОЦИАЛЬНЫМИ, БИЗНЕС-КЕЙСАМИ...</p>
                <?php } ?>


            </div>
        </div>
    </div>
</section>








<section id="contacts" class="main-gradient contacts section-padding-sm color-white">
    <div class="container reveal">
        <div class="row">
            <div class="col-12">
                <a name="contacts2"></a>
            </div>
        </div>
        <form class="contacts__form" action="">
            <?= \yii\helpers\Html::hiddenInput('_csrf', Yii::$app->request->csrfToken) ?>
            <div class="row">
                <div class="col-12 col-md-5 offset-md-1">
                    <input type="text" placeholder="<?= \Yii::t('c.YqmF2Dqma4', 'Ваше имя') ?>" name="name">
                </div>
                <div class="col-12 col-md-5">
                    <input type="text" placeholder="<?= \Yii::t('c.YqmF2Dqma4', 'Email') ?>" name="email">
                </div>
                <div class="col-12 col-md-10 offset-md-1">
                    <textarea name="text" placeholder="<?= \Yii::t('c.YqmF2Dqma4', 'Сообщение') ?>" cols="30" rows="5"></textarea>

                    <div class="text-right">
                        <input type="button" value="<?= \Yii::t('c.YqmF2Dqma4', 'Отправить сообщение') ?>" class="avatar-btn avatar-btn--white popup-open-mail" data-form="success" style="border: 0">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
    <style>
        .white {color: #000;}
    </style>
<footer>
    <div class="sections">
        <div class="section">
            <table style="margin-top: 20px;">
                <tr>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="https://neironcoin.com" class="white" target="_blank">neironcoin.com</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="https://www.youtube.com/channel/UCH7MEtOt3niudlhQjPJ3_0A" class="white" target="_blank">YouTube</a></p>
                    </td>

                </tr>
            </table>

            <div class="messegers">
            </div>

            <p class="title" style="margin-bottom: 10px;">Котировки: <a href="https://www.coingecko.com/">coingecko.com</a> | <a href="http://goldpricez.com/uk/gram">Gold Price</a></p>
            <div class="title"><?= \Yii::t('c.YqmF2Dqma4', '© Все права защищены. neiro-n.com 2019-2020') ?></div>

        </div>
    </div>
</footer>

<div class="avatar__overlay"></div>
<div class="currency__popup">
    <div class="currency__popup-inner" style="width: 500px;">

    </div>
</div>

<script>
    var path = document.getElementsByClassName('desicion-svg-path');

    for (i=0;i < path.length;i++){
        path[i].style.strokeDasharray = path[i].getTotalLength();
        path[i].style.strokeDashoffset = path[i].getTotalLength();
    }
</script>
<script src="/network/assets/js/jquery-3.2.1.min.js"></script>
<script src="/network/assets/js/countdown.js"></script>
<script src="/network/assets/js/scrollreveal.min.js"></script>
<script src="/network/assets/js/wow.min.js"></script>
<script src="/network/assets/js/slick/slick.min.js"></script>
<script src="/network/assets/js/main.js?ver=1.0.3311"></script>

<?= $this->render('../blocks/shareMeta', [
    'image'       => \yii\helpers\Url::to('/images/layouts/main/N3-w-128.png', true),
    'title'       => $this->title,
    'url'         => \yii\helpers\Url::current([], true),
    'description' => Yii::t('c.YqmF2Dqma4', 'УНИВЕРСАЛЬНЫЕ СЕРВИСЫ НЕЙРО: Р2Р обмен активами без комиссий, конфиденциальные чаты пользователей, рост ваших активов'),
]) ?>