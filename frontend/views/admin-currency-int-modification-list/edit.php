<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\forms\CurrencyIoModification */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <a href="<?= \yii\helpers\Url::to(['admin-currency-int/index']) ?>" class="btn btn-success">Валюты</a>
        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'master_wallet') ?>
                    <?= $form->field($model, 'extended_info')->textarea(['rows' => 3]) ?>
                    <?= $form->field($model, 'output_min')
                        ->label('Минимальный вывод в атомах')
                    ?>
                    <?= $form->field($model, 'comission_out')
                        ->label('Комиссия при выводе')
                    ?>
                    <?= $form->field($model, 'master_wallet_is_binance')
                        ->label('Мастер кошелек на Бинансе?')
                        ->widget('\common\widgets\CheckBox2\CheckBox') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
