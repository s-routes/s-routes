<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Криптоматы';


?>

<div class="container" style="padding-bottom: 10px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
</div>

<?php
$rows = \common\models\MapBankomat::find()
    ->select([
        'id',
        'image',
        'point',
        'name',
        'address',
        'content',
    ])
    ->asArray()
    ->all();
$jsonArray = \yii\helpers\Json::encode($rows);

\common\assets\YandexMaps::register($this);
$this->registerJs(<<<JS
ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [55.76, 37.64],
            zoom: 10
        }, {
            searchControlProvider: 'yandex#search'
        });
    var rows = {$jsonArray};
    var i;
    var item;
    var point;
    var html;
    
    for(i = 0; i < rows.length; i++) {
        item = rows[i];
        point = item.point.split(', ');
        html = $('<b>').html(item.name)[0].outerHTML;
        html += '<br>';
        html += $('<img>', {src: item.image, class: 'thumbnail', style: 'margin-bottom:0px;', width: '200'})[0].outerHTML;
        html += '<br>';
        html += item.address;
        html += '<br>';
        html += item.content;
        
        myMap.geoObjects
        .add(new ymaps.Placemark([point[0], point[1]], {
            balloonContent: html
        }, {
            preset: 'islands#icon',
            iconColor: '#0095b6'
        }))
        ;
    }
}

JS
);
?>
<div id="map" style="width: 100%; height: 600px;"></div>
<div class="container" style="margin-bottom: 70px;">
    <div class="col-lg-12">
        <hr>
        <center>
            <?= $this->render('../blocks/share', [
                'image'       => Url::to('/images/controller/site/about/logo.jpg', true),
                'title'       => 'Карта криптоматов',
                'url'         => Url::current([], true),
                'description' => 'Карта криптоматов.',
            ]) ?>
        </center>
    </div>
</div>


