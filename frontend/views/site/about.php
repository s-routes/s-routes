<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::t('c.ADjs4sdRbp', 'О нас');


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">
        <p class="text-center">
            <img src="/images/controller/site/index/logo2.png" width="50%">
        </p>
        <style>
            .article p {
                font-size: 150%;
            }
        </style>
        <div class="article">


        </div>
    </div>

    <hr>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to('/images/share/image.jpg', true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => \Yii::t('c.ADjs4sdRbp', 'Avatar Bank – платформа'),
        ]) ?>
    </center>

</div>


