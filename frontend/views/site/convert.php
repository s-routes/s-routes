<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $table array */

$this->title = 'Конвертация валют';

$rub = array_keys(\common\models\avatar\Currency::getBaseRateMatrix());
$keys = $rub;

$this->registerJs(<<<JS
$('.buttonConvert').click(function(e) {
    ajaxJson({
        url: '/site/convert-ajax',
        data: {
            value: $('#text-value').val(),
            from: $('.buttonFrom').attr('data-code'),
            to: $('.buttonTo').attr('data-code')
        },
        success: function(ret) {
            $('#result').html(ret.formated + ' ' + ret.currencyTo.code);
        }
    });
});

$('.buttonCurrencyItemFrom').click(function(e) {
    var code = $(this).data('code');
    $('.buttonFrom').attr('data-code', code).html(code + ' ' + '<span class="caret"></span>');
});
$('.buttonCurrencyItemTo').click(function(e) {
    var code = $(this).data('code');
    $('.buttonTo').attr('data-code', code).html(code + ' ' + '<span class="caret"></span>');
});
JS
);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <div class="row">
                <div class="col-lg-12" style="margin-bottom: 50px;">
                    <div class="input-group">
                        <span class="input-group-addon" id="sizing-addon1">Перевести</span>
                        <input type="text" class="form-control" aria-label="..." width="100px" id="text-value">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle buttonFrom" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Выберите <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php foreach ($rub as $td) { ?>
                                    <li class="buttonCurrencyItemFrom" data-code="<?= $td ?>"><a href="#"><?= $td ?></a></li>
                                <?php } ?>
                            </ul>
                        </div><!-- /btn-group -->
                        <span class="input-group-addon" id="sizing-addon1">В</span>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle buttonTo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Выберите <span class="caret"></span></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php foreach ($rub as $td) { ?>
                                    <li class="buttonCurrencyItemTo" data-code="<?= $td ?>"><a href="#"><?= $td ?></a></li>
                                <?php } ?>
                            </ul>
                        </div><!-- /btn-group -->
                        <span class="input-group-btn">
                            <button class="btn btn-success buttonConvert" type="button">Конвертировать!</button>
                        </span>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
            </div><!-- /.row -->
            <p id="result"></p>

            <table class="table table-hover table-striped">
                <tr>
                    <td>
                    </td>
                    <?php foreach ($rub as $td) { ?>
                        <td style="text-align: right;">
                            <?= $td ?>
                        </td>
                    <?php } ?>
                </tr>
                <?php $c = 0; ?>
                <?php for ($y = 0; $y < count($rub); $y++) { ?>
                    <tr>
                        <td>
                            <?= $keys[$y] ?>
                        </td>
                        <?php for ($x = 0; $x < count($rub); $x++) { ?>
                            <?php
                            $options = ['text-align' => 'right'];
                            if ($y > $x) $options['background-color'] = '#28a4c9';
                            if ($y < $x) $options['background-color'] = '#b2dba1';
                            ?>
                            <?php  ?>
                            <?php  ?>
                            <td style="<?= Html::cssStyleFromArray($options) ?>">
                                <?= isset($table[$x][$y])? Yii::$app->formatter->asDecimal($table[$x][$y], 8) : 'нет' ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            </table>
            <p>Пример</p>
            <p>2.2 ETH = <b><?= Yii::$app->formatter->asDecimal(\common\models\avatar\Currency::convert(2.2, 'ETH', 'BTC'), 8) ?></b> BTC</p>
        </div>
    </div>
</div>