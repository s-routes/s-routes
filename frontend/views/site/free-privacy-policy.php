<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Free Privacy Policy';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <h1 class="page-header text-center"><?= $this->title ?></h1>

            <p><b>What information do we collect?</b></p>
            <p>We collect information from you when you register on our site or fill out a form.</p>
            <p>When ordering or registering on our site, as appropriate, you may be asked to enteryour: name or e-mail address. You may, however, visit our site anonymously.</p>

            <p><b>What do we use your information for?</b></p>
            <p>Any of the information we collect from you may be used in one of the following ways:</p>
            <p>■	To personalize your experience (your information helps us to better respond to your individual needs)</p>
            <p>■	To improve our website (we continually strive to improve our website offerings based on the information and feedback we receive from you)</p>
            <p>■	To improve customer service (your information helps us to more effectively respond to your customer service requests and support needs)</p>
            <p>■	To process transactions.
                Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested by the customer.
            <p>■	To send periodic emails</p>
            <p>The email address you provide for order processing, may be used to send you information and updates pertaining to your order, in addition to receiving occasional company news, updates, related product or service information, etc.</p>
            <p>Note: If at anytime you would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</p>
            <p>■	To administer a contest, promotion, survey or other site feature</p>

            <p><b>How do we protect your information?</b></p>
            <p>We implement a variety of security measures to maintain the safety of your personal information when you access your personal information.</p>

            <p><b>Do we use cookies?</b></p>
            <p>Yes, we use cookies.</p>

            <p><b>Do we disclose any information to outside parties?</b></p>
            <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non- personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

            <p><b>Third party links</b></p>
            <p>Occasionally, at our discretion, we may include or offer third part/products or services on our website. These third party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>

            <p><b>Online Privacy Policy Only</b></p>
            <p>This online privacy policy applies only to information collected through ourwebsite and not to information collected offline.</p>

            <p><b>Terms and Conditions</b></p>
            <p>Please also visit ourTerms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of ourwebsite at http://www.freeprivacypolicy.com/terms.html.</p>

            <p><b>Your Consent</b></p>
            <p>By using our site, you consent to our <b>privacy policy</b></p>

            <p><b>Changes to our Privacy Policy</b></p>
            <p>If we decide to change our privacy policy, we will post those changes on this page.</p>
            <p>This policy was last modified on 21/05/2017</p>

            <p><b>Contacting Us</b></p>
            <p>If there are any questions regarding this privacy policy you may contact us using the information below.</p>
            <p><a href="https://www.avatar-bank.com">https://www.avatar-bank.com</a></p>
            <p><a href="mailto:god@avatar-bank.com">god@avatar-bank.com</a></p>
            <p>+7-925-237-45-01</p>
        </div>
    </div>
</div>


