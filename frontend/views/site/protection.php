<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 22.01.2017
 * Time: 11:58
 */

/* @var $this yii\web\View */
$this->title = 'Принципы защиты нашей Державы Света';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center"><?= $this->title ?></h1>
        </div>
        <div class="col-lg-8 col-lg-offset-2" style="padding: 0px 0px 100px 0px;">

            <p>
                <a href="https://www.galaxysss.com/newEarth/service/vozdayanie" target="_blank">
                    <img src="/images/layouts/main/protection.jpg" width="100%" title="Агентство Воздаяния Творца" data-toggle="tooltip">
                </a>
            </p>

            <p>
                Наш принцип: Мы свободные Аватары, кто к нам с чем придет тот с тем же и уйдет.
            </p>

            <p>
                Мы ценим наших клиентов как своих братьев и сестер и будем защищать их.
            </p>

            <p>
                Мы защищаем границы нашей Державы Света и будет воздавать сполна всем кто к нам придет в гости.
            </p>

            <p>
                Любой человек ограничивающий и сдерживающий нашу экспансию, препятствующий нашей деятельности будет
                наказан
                равноценным образом.
            </p>



            <hr>

        </div>
    </div>
</div>
        