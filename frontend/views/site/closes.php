<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Фирменная одежда AvatarNetwork';


$this->registerJs(<<<JS
window.location = 'http://avatarnetwork.printdirect.ru/';
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

    </div>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <p class="text-center">
                Если вы не перешли то перейдите по <a href="http://printdirect.ru/index.php?mode=account&action=product_info&product=7887493">ссылке</a>.
            </p>
        </div>
    </div>
    <hr>
    <center>
        <?= $this->render('../blocks/share', [
            'image'       => Url::to('/images/controller/site/closes/share.png', true),
            'title'       => $this->title,
            'url'         => Url::current([], true),
            'description' => 'Фирменная одежда AvatarNetwork',
        ]) ?>
    </center>

</div>



