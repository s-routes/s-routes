<?php

/** @var \yii\web\View $this */
$this->title = 'Языки';

$sort = new \yii\data\Sort([
    'attributes' => [
        'code' => [
            'label' => 'Код'
        ],
        'id',
        'name',
    ]
]);

?>
<style>

    a.asc:after {
        content: ' ↓';
        display: inline;
    }

    a.desc:after {
        content: ' ↑';
        display: inline;
    }

</style>

<h2>Языки</h2>
<?php \yii\widgets\Pjax::begin(); ?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ActiveDataProvider([
        'query'      => \common\models\Language::find(),
        'pagination' => [
            'pageSize' => 20,
        ],
        'sort'       => [
            'attributes' => [
                'code',
                'id',
                'name',
            ]
        ],
    ]),
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'summary'      => '',
    'columns'      => [
        'id',
        'name:text:Название',
        [
            'attribute' => 'code',
            'content'   => function ($item) {
                return \yii\helpers\Html::tag('code', $item->code);
            },
        ],
        [
            'header'  => 'Режим',
            'content' => function ($item) {
                $status = \yii\helpers\ArrayHelper::getValue($item, 'status', 1);
                if ($status == 2) return \yii\helpers\Html::tag('span', 'Для всех', ['class' => 'label label-success']);
                return \yii\helpers\Html::tag('span', 'Для переводчиков', ['class' => 'label label-default']);
            },
        ],
        [
            'header'  => 'Редактировать',
            'content' => function ($item) {
                return \yii\helpers\Html::a('Редактировать', ['languages-admin/languages-edit', 'id' => $item->id], ['class' => 'btn btn-info btn-xs']);
            },
        ],
    ],
]) ?>

<?php \yii\widgets\Pjax::end(); ?>

<a href="/all/languages/add" class="btn btn-success">Добавить</a>