<?php

/** @var $this \yii\web\View  */
/** @var $id  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все интрукции';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <p><a href="sort" class="btn btn-default">Сортировка</a> </p>
    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-insrukciya/delete',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});



JS
    );
    ?>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\Instrukciya::find()
                ->innerJoin('page', 'page.id = instrukciya.page_id')
                ->select([
                    'page.*'
                ])
                ->orderBy(['sort_index' => SORT_ASC])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'name',
            [
                'header'  => 'Удалить',
                'content' => function ($item) {
                    return Html::button('Удалить', [
                        'class' => 'btn btn-danger btn-xs buttonDelete',
                        'data'  => [
                            'id' => $item['id'],
                        ]
                    ]);
                }
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>


        <h1 class="page-header">Добавить</h1>
        <?php
        $model = new \avatar\models\validate\AdminInstrukciya();
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'formUrl' => '/admin-insrukciya/add',
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-insrukciya/index';
    }).modal();
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'page_id')->dropDownList(
            ArrayHelper::map(
                \common\models\exchange\Page::find()->all(),
                'id',
                'name'
            )
        ) ?>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Указать']); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>