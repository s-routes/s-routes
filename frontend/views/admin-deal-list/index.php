<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Сделки';


?>

<div style="margin: 0px 50px 0px 50px;">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable').click(function() {
    //window.location = '/cabinet-exchange/deal-action' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\exchange\Deal::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => ['defaultOrder' => ['created_at' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Открыл сделку (taker)',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'user_id', '');
                        if ($i == '') return '';
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                            'title' => $user->getName2(),
                            'data' => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header' => 'Тип сделки',

                    'content' => function ($item) {
                        $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                        $type_id = $offer->type_id;
                        $text = '';
                        if ($type_id == \common\models\exchange\Offer::TYPE_ID_BUY) {
                            $text = Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right','data' => ['toggle' => 'tooltip'], 'title' => 'Maker покупает у Taker']);
                        }
                        if ($type_id == \common\models\exchange\Offer::TYPE_ID_SELL) {
                            $text = Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left','data' => ['toggle' => 'tooltip'], 'title' => 'Maker продает для Taker']);
                        }

                        return $text;
                    },
                ],

                [
                    'header'         => 'Заказ',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'volume_vvb', 0);
                        $cio = \common\models\CurrencyIO::findOne($item['currency_io']);
                        $cint = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                        $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                        return Yii::$app->formatter->asDecimal($a, $cint->decimals_view);
                    },
                ],
                [
                    'header'         => '',
                    'content'        => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne($item['currency_io']);
                        $c = \common\models\avatar\Currency::findOne($cio->currency_ext_id);
                        $code = $c->code;

                        $cHtml = Html::tag('span', $code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                        return $cHtml;
                    },
                ],
                [
                    'header'         => 'Цена',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                        $ca = \common\models\avatar\Currency::findOne($item['currency_id']);
                        $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $ca->id]);
                        $cint = \common\models\piramida\Currency::findOne($link->currency_int_id);
                        $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                        return Yii::$app->formatter->asDecimal($a, $cint->decimals_view);
                    },
                ],
                [
                    'header'         => '',
                    'content'        => function (\common\models\exchange\Deal $item) {
                        $offer = $item->getOffer();
                        $offerPay = \common\models\exchange\OfferPay::findOne($offer->offer_pay_id);
                        $code = $offerPay->code;

                        $cHtml = Html::tag('span', $code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                        return $cHtml;
                    },
                ],
                [
                    'header'  => 'Сделал предложение (maker)',
                    'content' => function ($item) {
                        $i = $item->getOffer()->user_id;
                        if ($i == '') return '';
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                            'title' => $user->getName2(),
                            'data' => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'         => 'Статус',
                    'contentOptions' => ['aria-label' => 'Статус'],
                    'content'        => function ($item) {
                        if (!isset(\common\models\exchange\Deal::$statusList[$item['status']])) {
                            return '';
                        }
                        $statusClass = [
                            \common\models\exchange\Deal::STATUS_CREATE         => 'default',
                            \common\models\exchange\Deal::STATUS_OPEN           => 'success',
                            \common\models\exchange\Deal::STATUS_BLOCK          => 'info',
                            \common\models\exchange\Deal::STATUS_MONEY_SENDED   => 'success',
                            \common\models\exchange\Deal::STATUS_MONEY_RECEIVED => 'info',
                            \common\models\exchange\Deal::STATUS_CLOSE          => 'success',
                            \common\models\exchange\Deal::STATUS_AUDIT_WAIT     => 'warning',
                            \common\models\exchange\Deal::STATUS_AUDIT_ACCEPTED => 'warning',
                            \common\models\exchange\Deal::STATUS_AUDIT_FINISH   => 'warning',
                            \common\models\exchange\Deal::STATUS_HIDE           => 'danger',
                            \common\models\exchange\Deal::STATUS_CANCEL         => 'danger',
                        ];

                        return Html::tag('span', \common\models\exchange\Deal::$statusList[$item['status']], ['class' => 'label label-' . $statusClass[$item['status']]]);
                    },
                ],
                [
                    'header'  => '',
                    'content' => function ($item) {
                        return Html::a('Просмотр', ['admin-deal-list/view', 'id' => $item['id']], [
                            'class' => 'btn btn-info',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
