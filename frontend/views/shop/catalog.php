<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */
/** @var $catalog \common\models\shop\CatalogItem */

$this->title = 'Каталог. '. $catalog->name;


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <ul class="nav nav-tabs">
        <!--<li><a href="/shop/index">Магазин</a></li>-->
        <li><a href="/page/dogovor-oferty">Договор Оферты</a></li>
        <li><a href="/page/opisanie-tovarov-uslug">Описание товаров и услуг</a></li>
        <li><a href="/page/yuridicheskaya-informatsiya">Контакты и реквизиты</a></li>
    </ul>

    <?php foreach (\common\models\shop\CatalogItem::find()->all() as $c) { ?>
        <button type="button" class="btn c-button" style="margin-top: 30px;margin-left: 15px;"><a href="/shop/catalog?id=<?= $c->id ?>" class="c-catalog-link"><?= $c->name ?></a></button>
    <?php } ?>

    <div class="col-lg-8">
        <h2 class="page-header">
            Все товары
        </h2>
        <div class="row">
            <?php /** @var \common\models\shop\Product $product */  ?>
            <?php foreach (\common\models\shop\Product::find()->where(['tree_node_id' => $catalog->id])->all() as $product) {  ?>
                <div class="col-lg-4">
                    <p>
                        <a href="<?= Url::to(['shop/item','id' => $product->id]) ?>">
                            <img class="thumbnail" src="<?= \common\widgets\FileUpload7\FileUpload::getFile($product->image, 'crop') ?>" width="100%">
                        </a>
                    </p>
                    <p class="text-center"><?= $product->name ?></p>
                    <p class="text-center">
                        <?php
                        $currency = Currency::findOne($product->currency_id);
                        $price = $product->price / pow(10, $currency->decimals);
                        ?>

                        <?= Yii::$app->formatter->asDecimal($price, $currency->decimals) ?> <span class="label label-info"><?= $currency->code ?></span>
                    </p>
                    <p><a href="<?= Url::to(['shop/order','id' => $product->id]) ?>" class="btn btn-success" style="width: 100%">Купить</a></p>
                </div>
            <?php } ?>
        </div>

    </div>
</div>


