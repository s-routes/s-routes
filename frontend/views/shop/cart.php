<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\touchspin\TouchSpin;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */


$this->title = 'Корзина';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">

        <div class="row">
            <?php
            $items = \common\models\shop\Basket::get();
            $rows = [];
            foreach ($items as $i) {
                $rows[] = [
                    'id'          => $i['id'],
                    'count'       => $i['count'],
                    'name'        => $i['product']['name'],
                    'image'       => $i['product']['image'],
                    'price'       => $i['product']['price'],
                    'currency_id' => $i['product']['currency_id'],
                ];
            }
            ?>
            <?php
            $this->registerJS(<<<JS
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/shop/cart-delete',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.inputI').on('change', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var t = $(this);
    var id = t.data('id');
    
    var v1 = ajaxJson({
        url: '/shop/cart-update',
        data: {
            id: id,
            count: t.val()
        },
        success: function (ret) {
            $('.sumAll').html(ret.allSumFormatted);
        }
    });
});
$('.inputI').on('input', function (e) {
    e.preventDefault();
    e.stopPropagation();
    var t = $(this);
    var id = t.data('id');
    
    ajaxJson({
        url: '/shop/cart-update',
        data: {
            id: id,
            count: t.val()
        },
        success: function (ret) {
            $('.sumAll').html(ret.allSumFormatted);
        }
    });
});


JS
            );
            ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ArrayDataProvider([
                    'allModels' => $rows,
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover table_exchange',
                ],
                'summary' => '',
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['aria-label' => 'ID'],
                    ],
                    [
                        'header'  => 'Картинка',
                        'contentOptions' => ['class' => 'text-right','aria-label' => 'Картинка'],
                        'content' => function ($item) {
                            $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                            if ($i == '') return '';

                            return Html::img(\common\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                                'class'  => "thumbnail",
                                'width'  => 80,
                                'height' => 80,
                                'style'  => 'margin-bottom: 0px;',
                            ]);
                        },
                    ],
                    [
                        'header'         => 'Наименование',
                        'attribute' => 'name',
                        'contentOptions' => ['aria-label' => 'Наименование'],
                    ],
                    [
                        'header'         => 'Цена',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right','aria-label' => 'Цена'],
                        'content'        => function ($item) {
                            $c = Currency::findOne(Currency::NEIRO);
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                            if ($v == 0) return '';
                            $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                            return Yii::$app->formatter->asDecimal($v / pow(10, $c->decimals), $c->decimals) . $cHtml;
                        },
                    ],
                    [
                        'header'         => 'Стоимость',
                        'attribute'      => 'count',
                        'contentOptions' => ['class' => 'text-right', 'style' => 'width: 150px;','aria-label' => 'Кол-во'],
                        'content'        => function ($item) {
                            return TouchSpin::widget([
                                'name'          => 'volume',
                                'options'       => ['placeholder' => 'Adjust...', 'class' => 'form-control inputI', 'data-id' => $item['id']],
                                'pluginOptions' => [
                                    'initval' => $item['count'],
                                    'step'    => 1,
                                    'max'     => 1000000000,
                                ],
                            ]);
                        },
                    ],

                    [
                        'header'  => 'Удалить',
                        'content' => function ($item) {
                            return Html::button('Удалить', [
                                'class' => 'btn btn-danger btn-xs buttonDelete',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);
                        }
                    ],
                ]
            ]) ?>

            <p>Комментарий:</p>
            <textarea id="comment" class="form-control" style="margin-bottom: 50px;" rows="5"></textarea>

            <?php
            $p = \common\models\shop\Basket::getPrice();
            $c = Currency::findOne(Currency::NEIRO);
            $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

            $this->registerJs(<<<JS
$('.buttonFirst').click(function(e) {
    ajaxJson({
        url: '/shop/first',
        data: {comment: $('#comment').val()},
        success: function(ret) {
            window.location = '/cabinet-silver/step2';
        }
    });
});
JS
);
            ?>
            <p>Итого: <span class="sumAll"><?= Yii::$app->formatter->asDecimal($p / pow(10, $c->decimals), $c->decimals) . $cHtml ?></span></p>

            <p><button class="btn c-order buttonFirst">Оформить</button></p>
        </div>

    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>