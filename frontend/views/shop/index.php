<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */


$this->title = 'Магазин';
$this->registerJs(<<<JS
$('.buttonAddToCart').click(function(e) {
    ajaxJson({
        url: '/shop/cart-add',
        data: {id: $(this).data('id')},
        success: function(ret) {
            $('.js-basketCounter').html(ret.counter);
            new Noty({
                timeout: 3000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Товар успешно добавлен в корзину.'
            }).show();
        }
    })
});
JS
);
?>
?>


<div class="container" style="padding-bottom: 70px;">


    <?php foreach (\common\models\shop\CatalogItem::find()->all() as $c) { ?>
        <button type="button" class="btn c-button" style="margin-top: 30px;margin-left: 15px;"><a href="/shop/catalog?id=<?= $c->id ?>" class="c-catalog-link"><?= $c->name ?></a></button>
    <?php } ?>

    <div class="col-lg-12">
        <h2 class="page-header">
            Все товары
        </h2>
        <div class="row">
            <?php /** @var \common\models\shop\Product $product */  ?>
            <?php foreach (\common\models\shop\Product::find()->all() as $product) {  ?>
                <div class="col-lg-3">
                    <p>
                        <a href="<?= Url::to(['shop/item','id' => $product->id]) ?>">
                            <img class="thumbnail" src="<?= \common\widgets\FileUpload7\FileUpload::getFile($product->image, 'crop') ?>" width="100%">
                        </a>
                    </p>
                    <p class="text-center"><?= $product->name ?></p>
                    <p class="text-center">
                        <?php
                        $currency = Currency::findOne($product->currency_id);
                        $price = $product->price / pow(10, $currency->decimals);
                        ?>

                        <?= Yii::$app->formatter->asDecimal($price, $currency->decimals) ?> <span class="label label-info"><?= $currency->code ?></span>
                    </p>
                    <p><button class="btn btn-success buttonAddToCart" data-id="<?= $product->id ?>" style="width: 100%">В корзину</button></p>
                </div>
            <?php } ?>
        </div>

    </div>
</div>


