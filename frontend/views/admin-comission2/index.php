<?php
use common\models\avatar\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\AdminComission2 */

$this->title = 'Таблица конвертации';

Yii::$app->session->set('frontend/views/admin-market-convert/index.php', $model);
?>

<div style="margin: 20px 20px 20px 20px;">
    <h1 class="page-header"><?= $this->title ?></h1>


    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
        'model' => $model,
        'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin/index';
    }).modal();
}
JS
        ,
    ]);
    Yii::$app->session->set('frontend/views/admin-market-convert/index.php_form', $form);

    $cList = Currency::find()
        ->where(['id' => \avatar\controllers\AdminComission2Controller::$currencyList])
        ->select('id')
        ->orderBy(['sort_index' => SORT_ASC])
        ->column();
    $columns = [
        'id',
        [
            'header'  => 'Картинка',
            'content' => function ($item) {
                $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                if ($i == '') return '';

                return Html::img($i, [
                    'class'  => "thumbnail",
                    'width'  => 80,
                    'height' => 80,
                    'style'  => 'margin-bottom: 0px;',
                ]);
            }
        ],

    ];

    foreach ($cList as $cid) {

        $c = Currency::findOne($cid);
        $columns[] = [
            'header'        => $c->code,
            'headerOptions' => [
                'data' => [
                    'cid' => $c->id,
                ]
            ],
            'content'       => function ($item, $p2, $p3, $p4) {
                if ($p4->headerOptions['data']['cid'] == $item['id']) return '';

                $model = Yii::$app->session->get('frontend/views/admin-market-convert/index.php');

                /** @var \iAvatar777\services\FormAjax\ActiveForm $form */
                $form = Yii::$app->session->get('frontend/views/admin-market-convert/index.php_form');
                $n = 'percent';
                $name = 'v_'.$item['id'].'_'.$p4->headerOptions['data']['cid'].'_'.$n;
                $field1 = $form->field($model, $name)->label('', ['class' => 'hide']);
                $n = 'apply';
                $name = 'v_'.$item['id'].'_'.$p4->headerOptions['data']['cid'].'_'.$n;
                $field2 = $form->field($model, $name)->label('', ['class' => 'hide'])->widget('\common\widgets\CheckBox2\CheckBox');

                return $field1 . $field2;
            }
        ];
    }
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => Currency::find()
                ->where(['id' => \avatar\controllers\AdminComission2Controller::$currencyList]),
            'sort' => ['defaultOrder' => ['sort_index' => SORT_ASC]],
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => $columns,
    ]) ?>

    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end([
    ]); ?>


</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>