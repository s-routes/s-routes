<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Lot */

$this->title = $model->id;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model' => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-lot/index';
    }).modal();
}
JS
            ,
        ]);

        ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'bilet_count') ?>
        <?= $form->field($model, 'time_start')->widget('\kartik\datetime\DateTimePicker') ?>
        <?= $form->field($model, 'time_finish')->widget('\kartik\datetime\DateTimePicker') ?>
        <?= $form->field($model, 'time_raffle')->widget('\kartik\datetime\DateTimePicker') ?>
        <?= $form->field($model, 'image') ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end([
        ]); ?>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>