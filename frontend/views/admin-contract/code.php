<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Конвертация контракта в код';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>
            <?php $data = Yii::$app->session->getFlash('form') ?>
            <p>Код контракта</p>
            <pre><?= $data[0]['bin'] ?></pre>
            <p>ABI</p>
            <pre><?= \yii\helpers\Json::encode($data[0]['abi']) ?></pre>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($model, 'code')->textarea(['rows' => 10]) ?>

            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton('Компилировать', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
    </div>
</div>



