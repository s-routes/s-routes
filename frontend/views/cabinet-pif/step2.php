<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\PifParams */

$this->title = 'Инвестиционные стратегии';


$this->registerCssFile('/images/controller/cabinet-pif/step2/style.css');
$this->registerCssFile('https://fonts.googleapis.com/css?family=Fira+Sans+Condensed&display=swap');
$this->registerCssFile('https://fonts.googleapis.com/css?family=Exo+2&display=swap');

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.6/Chart.bundle.min.js', ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJsFile('/images/controller/cabinet-pif/step2/script.js', ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJsFile('https://kit.fontawesome.com/b99e675b6e.js');

?>
<style>
    .sum {
        font-size: 18pt;
    }
</style>
<div class="container">
    <div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div id="plans" class="col-md-8 plans">
                <div class="row">
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_3year" class="plans__item plans__item_3year plans__item_size">
                            <h1 class="plans__item-h1">20%</h1>
                            <h2 class="plans__item-h2">3 года</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_2year" class="plans__item plans__item_2year plans__item_size">
                            <h1 class="plans__item-h1">18%</h1>
                            <h2 class="plans__item-h2">2 года</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_1year" class="plans__item plans__item_1year plans__item_size">
                            <h1 class="plans__item-h1">16%</h1>
                            <h2 class="plans__item-h2">1 год</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_6months" class="plans__item plans__item_6months plans__item_size">
                            <h1 class="plans__item-h1">15%</h1>
                            <h2 class="plans__item-h2">6 месяцев</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_3months" class="plans__item plans__item_3months plans__item_size">
                            <h1 class="plans__item-h1">13%</h1>
                            <h2 class="plans__item-h2">3 месяца</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_1months" class="plans__item plans__item_1months plans__item_size">
                            <h1 class="plans__item-h1">12%</h1>
                            <h2 class="plans__item-h2">1 месяц</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_20days" class="plans__item plans__item_20days plans__item_size">
                            <h1 class="plans__item-h1">11%</h1>
                            <h2 class="plans__item-h2">20 дней</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <div id="plans__item_10days" class="plans__item plans__item_10days plans__item_size">
                            <h1 class="plans__item-h1">10%</h1>
                            <h2 class="plans__item-h2">10 дней</h2>
                            <hr class="plans__item-hr">
                        </div>
                    </div>
                </div>
        </div>
        <div class="col-md-4">
            <div class="calculator">
                <div class="container-fluid calculator_bg">
                    <div class="row">
                        <div class="range_block">
                            <div class="col-xs-2">
                                <a href="#" class="btn btn-default">
                                    <i class="fas fa-wallet" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-xs-10 input_range text-center">
                                <div class="btn-group" role="group" aria-label="...">
                                    <button type="button" class="btn btn-default calculator-btn-item calculator-btn-item_clicked">ETH</button>
                                    <button type="button" class="btn btn-default calculator-btn-item">USDT</button>
                                    <button type="button" class="btn btn-default calculator-btn-item">VIM</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                            <div class="col-xs-2">
                                <!--<i class="fas fa-dollar-sign fa-square fa-2x"></i>-->
                                <a class="btn btn-default" href="#" aria-label="Настройки">
                                    <i class="fa fa-dollar-sign" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-xs-10">
                                <input type="range" class="range1 calculator-range_summ mt-1" min="0" max="1000000" step="1" value="0">
                            </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-2">
                            <a href="#" class="btn btn-default">
                                <i class="fas fa-percent"></i>
                            </a>
                        </div>
                        <div class="col-xs-10 input_range">
                            <input type="range" class="range2 calculator_range_percent mt-1" min="0" max="8" value="0" step="1">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xs-2">
                            <a href="#" class="btn btn-default">
                                <i class="fas fa-clock"></i>
                            </a>
                        </div>
                        <div class="col-xs-10 input_range">
                            <input type="range" class="range3 calculator_range_time_scale mt-1" min="0" max="8" value="0" step="1">
                        </div>
                    </div>
                </div>
                <h2 class="calculator__h2">Параметры</h2>
                <hr class="calculator__hr">
            </div>
        </div>
    </div>
</div>




    <div class="container">
        <div class="row">

                <div class="col-md-4 block3 news">
                    <div class="news-left">
                    <div class="news_block">
                        <div class="news_one" data-accordion="open">
                            <div class="news_title">«Рома» увеличила предложение по Джеко до € 13 млн</div>
                            <div class="news_date">30 июля 2019, 01:36</div>
                            <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                            <div class="news_text">
                                «Интер» сделал новое предложение «Роме» по боснийскому нападающему Эдину Джеко, сообщает Football Italia.

                                По данным источника, римский клуб увеличил предложение до € 13 млн плюс € 2 млн в виде бонусов. Отмечается, что «Рома» отказывается отдавать боснийца за эту сумму и хочет получить за форварда € 20 млн.

                                33-летний Джеко выступает за «Рому» с 2015 года. В минувшем сезоне босниец принял участие в 33 матчах итальянского национального чемпионата, забил девять голов и отдал шесть результативных передач. Контракт игрока с клубом действует до лета 2020 года.

                                Ранее сообщалось, что «Интер» предлагал «Роме» € 12 млн. Джеко согласен на переход. В трансфере боснийца лично заинтересован главный тренер миланского клуба Антонио Конте.
                            </div>
                        </div>

                        <div class="news_one" data-accordion="open">
                            <div class="news_title">«Зенит» предложил за Малкома € 35 млн плюс € 5 млн в виде бонусов</div>
                            <div class="news_date">30 июля 2019, 00:18</div>
                            <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                            <div class="news_text">
                                Петербургский «Зенит» продолжает переговоры по трансферу бразильского полузащитника «Барселоны» Малкома и предложил за него € 35 млн, утверждает Sport.es.

                                По данным источника, ещё € 5 млн российский клуб предлагает в виде бонусов. При этом «Барселона» оценивает бразильца € 40 млн плюс € 5 в виде бонусов. Сам игрок готов перейти в «Зенит», так как клуб сделал ему выгодное предложение в финансовом плане, которое составляет € 6,5 млн за сезон.

                                22-летний Малком является воспитанником «Коринтианса». Футболист перешёл в «Барселону» из «Бордо» летом 2018 года. Сумма трансфера составила € 41 млн. В минувшем сезоне бразилец провёл 15 матчей в испанской Примере, забил 1 гол и отдал 2 результативные передачи.
                            </div>
                        </div>

                        <div class="news_one" data-accordion="open">
                            <div class="news_title">«Кристал Пэлас» намерен предложить ЦСКА за Чалова € 15 млн</div>
                            <div class="news_date">30 июля 2019, 14:38</div>
                            <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                            <div class="news_text">
                                Как стало известно «Чемпионату», «Кристал Пэлас» намерен в ближайшее время сделать предложение ЦСКА по нападающему Фёдору Чалову в размере € 15 млн.

                                Ранее в СМИ была информация, что интерес к футболисту также проявляют «Ливерпуль», «Манчестер Юнайтед», «Арсенал», «Манчестер Сити», «Тоттенхэм Хотспур», «Монако», «Лацио» и «Валенсия».

                                Чалов является воспитанником «армейского» клуба. За первую команду столичной команды он выступает с 2017 года. В прошлом сезоне на клубном уровне форвард принял участие в 37 играх. На его счету 17 забитых мячей и 7 результативных передач. С 15 голами в активе он стал лучшим бомбардиром Российской Премьер-Лиги. Контракт игрока с ЦСКА рассчитан до лета 2022 года.
                            </div>
                        </div>

                        <div class="news_one" data-accordion="open">
                            <div class="news_title">Егоров: пенальти в ворота «Зенита» был, в ворота «Оренбурга» — нет. Судья ошибся</div>
                            <div class="news_date">30 июля 2019, 14:32</div>
                            <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                            <div class="news_text">
                                Глава департамента судейства и инспектирования Российского футбольного союза Александр Егоров высказался о работе судьи Сергея Карасёва в матче 3-го тура Российской Премьер-Лиги «Оренбург» – «Зенит» (0:2), а также рассказал об итогах заседания экспертно-судейской комиссии по итогам встречи 2-го тура «Ростов» – «Спартак» (2:2).

                                «Момент с назначением пенальти в ворота «Оренбурга»: Карасёв ошибся, это не игра рукой, надо было продолжить игру. На Барриосе надо было назначить пенальти. Сергею снижена оценка за эти две ключевые ошибки. Если «Оренбург» напишет письмо, то передадим документы в экспертную комиссию.

                                Состоялась экспертно-судейская комиссия по обращению «Ростова» по игре со «Спартаком». Решение комиссии: было обращение насчёт моментов на 13-й минуте (падение Норманна), на 25-й минуте (пенальти «Ростову», фолы Фернандо и Бакаева). На 13-й минуте арбитр поступил правильно, не назначив пенальти. На 25-й минуте арбитр правильно назначил пенальти «Ростову». На 60-й минуте арбитр должен был назначить пенальти «Спартаку» за фол Фернандо, и на 86-й минуте Бакаеву должны были показать вторую жёлтую карточку. Будет ли отстранение? Это внутреннее дело департамента, «Ростов» не просил отстранить», — передаёт слова Егорова корреспондент «Чемпионата» Максим Пахомов.
                            </div>
                        </div>

                        <div class="news_one" data-accordion="open">
                            <div class="news_title">РФС оценил работу VAR на матче ЦСКА — «Локомотив»</div>
                            <div class="news_date">30 июля 2019, 14:26</div>
                            <div class="btn_n">Читать<i class="fas fa-angle-down"></i></div>
                            <div class="news_text">
                                Глава департамента судейства и инспектирования Российского футбольного союза Александр Егоров прокомментировал работу судей, а также систему VAR в матче 3-го тура чемпионата России ЦСКА — «Локомотив» (1:0).

                                «VAR сделал то, что и должен был сделать. Наши иностранные коллеги в плане работы VAR, у нас ноль ошибок, оценивают положительно нашу работу. Сёмин жаловался на момент со Смоловым? Есть протокол VAR, где смотрят очевидные моменты – как на седьмой минуте и так далее. А на Смолове очевидно было для всех, что это не пенальти. Снизили оценку за то, что Смолову не показали жёлтую карточку за симуляцию. Смысл останавливать игру, если всё очевидно? Я сидел на стадионе рядом с представителями «Локомотива» — они готовы были к пенальти на Живоглядове, а когда был спорный мяч, то они сильно удивились. VAR в этой игре оправдал себя на сто процентов. Почему не было карточки Эдеру за удар Васину? Арбитр, вне всяких сомнений и экспертов, должен был показать Эдеру жёлтую карточку.

                                Что касается момента, когда Магнуссону показали жёлтую карточку, а потом назначили пенальти, то почему карточки не было? Она показана. Если бы арбитр показал за срыв перспективной атаки, то да, а тут технический фол и грубая игра, мяч был в игре, и карточка никуда не девается. Что касается времени, пока смотрят моменты. Если мяч вышел, то идёт остановка игры. VAR ввели для того, чтобы были правильные решения. Пока у нас бывают паузы, была три минуты. Надеюсь, дальше она будет поменьше. Мы тоже тренируемся, наша задача – делать всё правильно.

                                Гилерме нельзя было играть в чёрной форме? Почему нельзя, покажите пункт правил, где это написано. Зрителям неудобно было? Напишите про это, может, это картинка такая была», — передаёт слова Егорова корреспондент «Чемпионата» Максим Пахомов.
                            </div>
                        </div>

                    </div>
                    </div>
                </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="oilChart">
                            <canvas id="oilChart" height="180"></canvas>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="case">
                            <div class="container-fluid calculator_bg">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h1 class="case__h1">Ваш портфель</h1>
                                    </div>

                                    <div class="col-xs-12 text-left case__item">
                                        <div class="case__title-active case__item-left">
                                            Актив
                                        </div>
                                        <div class="case__title-active-val case__item-right">
                                            ETH
                                        </div>
                                    </div>

                                    <div class="col-xs-12 text-left case__item">
                                        <div class="case__title-summ case__item-left">
                                            Сумма
                                        </div>
                                        <div class="case__title-summ-val case__item-right">
                                            0
                                        </div>
                                    </div>

                                    <div class="col-xs-12 text-left case__item">
                                        <div class="case__title-profitableness case__item-left">
                                            Доходность
                                        </div>
                                        <div class="case__title-profitableness-val case__item-right">
                                           0
                                        </div>
                                    </div>

                                    <div class="col-xs-12 text-left case__item">
                                        <div class="case__title-period case__item-left">
                                            Срок
                                        </div>
                                        <div class="case__title-period-val case__item-right">
                                            0
                                        </div>
                                    </div>

                                    <div class="col-xs-12 text-left case__item-btn">
                                        <button type="button" class="btn btn-success case__btn">Разместить</button>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="speedChart">
                            <canvas id="speedChart" width="300" height="100"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="container">
    <div class="row">
        <div class="col-lg-12 step1">
            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($model, 'currency')->dropDownList([
                '- Ничего не выбрано -',
                1 => 'ETH',
                2 => 'USDT',
                3 => 'VIM',
            ]) ?>

            <hr class="featurette-divider">
            <div class="form-group">

                <?= Html::button('Выбрать', [
                    'class' => 'btn btn-success buttonSelect',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;margin-bottom: 30px;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

</div>



