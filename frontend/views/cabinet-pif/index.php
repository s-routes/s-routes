<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\PifParams */

$this->title = 'ПИФ';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">


        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>

        <?php else: ?>


            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?= $form->field($model, 'currency')->dropDownList([
                1 => 'ETH',
                2 => 'USDT',
                3 => 'VIM',
            ]) ?>
            <?= $form->field($model, 'amount') ?>
            <?= $form->field($model, 'percent') ?>
            <?= $form->field($model, 'period') ?>
            <?= $form->field($model, 'sostav')->dropDownList([
                1 => 'Тотализатор',
                2 => 'Производство',
                3 => 'Энергия',
            ]) ?>



            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton('Инвестировать', [
                    'class' => 'btn btn-default',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



