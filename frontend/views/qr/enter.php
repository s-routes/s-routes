<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\QrEnter */

$this->title = Yii::t('c.RGQ3VcUnI0', 'Активировать Карту Аватара');

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <p class="text-center">
            <img src="/images/controller/site/index/credit-card.png" style="width: 100%; max-width: 500px;"
                >
        </p>
    </div>

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4">
            <?php if (Yii::$app->session->hasFlash('form')) { ?>
                <p class="alert alert-success"><?= \Yii::t('c.RGQ3VcUnI0', 'Успешно. Вам не') ?></p>
            <?php } else { ?>
                <?php $form = \yii\bootstrap\ActiveForm::begin([]) ?>
                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => 'email']])->label('Почта', ['class' => 'hide']) ?>
                <?= $form->field($model, 'password1', ['inputOptions' => ['placeholder' => Yii::t('c.RGQ3VcUnI0', 'Пароль')]])->label('Почта', ['class' => 'hide'])->passwordInput() ?>
                <?= $form->field($model, 'password2', ['inputOptions' => ['placeholder' => Yii::t('c.RGQ3VcUnI0', 'Пароль еще раз')]])->label('Почта', ['class' => 'hide'])->passwordInput() ?>
                <?= $form->field($model, 'code', ['inputOptions' => ['placeholder' => Yii::t('c.RGQ3VcUnI0', 'Номер карты')]])->label('Почта', ['class' => 'hide']) ?>
                <hr>
                <?= Html::submitButton(Yii::t('c.RGQ3VcUnI0', 'Активировать'), ['class' => 'btn btn-success btn-lg', 'style' => 'width: 100%']) ?>
                <?php \yii\bootstrap\ActiveForm::end() ?>
            <?php } ?>
        </div>
    </div>



</div>


