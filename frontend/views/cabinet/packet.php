<?php

use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


$this->title = 'Активы';

$model = new \avatar\models\forms\PrizmInput();

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a></h1>
        <!-- Large modal -->

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Справка по обменнику</h4>
                    </div>
                    <div class="modal-body">
                        <h2><?= Html::encode($this->title) ?></h2>
                        <p>Справочная система в разработке. Скоро обновим</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
        );
        ?>
    </div>

    <?= $this->render('../cabinet-active/_menu4') ?>

    <br>
    <div class="col-lg-8">

        <?php
        $this->registerJs(<<<JS

$('.buttonIN').click(function(e) {
    $('#modalAdd').modal();
});
$('.buttonSEND').click(function(e) {
    $('#sendToPull').modal();
});
$('.buttonOUT').click(function(e) {
    $('#returnToOwner').modal();
});
var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});
$('.buttonCheck').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-input/check',
        data: {id: $(this).data('id')},
        success: function(ret) {
            if (ret.status == 1) {
                $('#pzm_letm').html(ret.amount_letm_formatted);
                $('#pzm_letn').html(ret.amount_letn_formatted);
                $('#modalInfo3').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            } else if (ret.status == 0) {
                $('#modalInfo0').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            } else if (ret.status == 2) {
                $('#modalInfo2_text').html(ret.message);
                $('#modalInfo2').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            }
        }
    });
});
$('.buttonClose').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-input/close',
        data: {id: $(this).data('id')},
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
        );
        ?>

        <?php if (\common\models\TaskPrizmInput::find()
                ->where([
                    'user_id' => Yii::$app->user->id,
                    'status'  => 0,
                ])->count() > 0
        ) { ?>
            <p style="font-size: 20px;">Задачи на ввод:</p>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\TaskPrizmInput::find()
                        ->where([
                            'user_id' => Yii::$app->user->id,
                            'status'  => 0,
                        ])
                        ->orderBy(['created_at' => SORT_DESC])
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'summary'      => '',
                'columns'      => [
                    'id',
                    'account',
                    [
                        'header'  => 'Создано',
                        'content' => function ($item) {
                            $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                            if ($v == 0) return '';

                            return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                        },
                    ],
                    [
                        'header'  => 'Проверить',
                        'content' => function ($item) {
                            return Html::button('Проверить', [
                                'class' => 'btn btn-info buttonCheck',
                                'data'  => [
                                    'id' => $item['id'],
                                ],
                            ]);
                        },
                    ],
                    [
                        'header'  => 'Закрыть',
                        'content' => function ($item) {
                            return Html::button('Закрыть', [
                                'class' => 'btn btn-info buttonClose',
                                'data'  => [
                                    'id' => $item['id'],
                                ],
                            ]);
                        },
                    ],
                ],
            ]) ?>
        <?php } ?>





        <?php
        $sb = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::SB);
        $sbp = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::SBP);
        $walletSB = $sb['wallet'];
        $walletSBP = $sbp['wallet'];
        ?>

        <h4 class="modal-title text-center">Отправить свои PZM в фонд</h4>
        <hr>
        <p>Для того чтобы отравить свои PZM в фонд:<br>
            1. Переведите желаемую сумму со своего кошелька на Мастер-кошелёк указанный ниже.</p>
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <b style="color: red; font-size: 26px;" class="buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= Yii::$app->params['prizmMasterAccount'] ?>" title="Нажмите чтобы скопировать"><?= Yii::$app->params['prizmMasterAccount'] ?></b>
                <p class="buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= Yii::$app->params['prizmMasterAccountKey'] ?>" title="Нажмите чтобы скопировать"><?= Yii::$app->params['prizmMasterAccountKey'] ?></p>
            </div>
        </div>
        <p>
            2. Укажите ниже с какого кошелька вы отправили свои монеты или идентификатор транзакции по которой
            вы отправили свои монеты.<br>
            3. Нажмите "Создать задачу на отправление в фонд".<br>
            4. После закрытия окна вы увидете сформированную задачу на ввод монет, в ней надо нажать
            "Проверить". Если транзакция уже попала в блок блокчейна то она подтвердится системой и вам будут
            зачислены монеты SB в количестве равном отправленным вашим монетам.
        </p>
        <p class="alert alert-danger">
            <i class="glyphicon glyphicon-alert"></i> Внимание! Для удачной проверки делайте только одну транзакцию.
        </p>
        <?php $form = ActiveForm::begin([
            'id'                 => 'formAdd',
            'enableClientScript' => false,
            'options'            => ['enctype' => 'multipart/form-data'],
        ]); ?>
        <?= $form->field($model, 'account') ?>
        <hr>
        <div class="form-group">
            <?php
            $this->registerJs(<<<JS
var functionAdd = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-task-input/add',
        data: $('#formAdd').serializeArray(),
        success: function(ret) {
            b.on('click', functionAdd);
            b.html(bText);
            b.removeAttr('disabled');
            window.location.reload();  
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formAdd');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-prizminput-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
            var b = $('.buttonAdd');
            b.off('click');
            b.on('click', functionAdd);
            b.html('Добавить');
            b.removeAttr('disabled');
        }
    });
};

$('.buttonAdd').click(functionAdd);

$('#formAdd .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent().parent();
    
    p.find('.form-group').each(function(i, e) {
        $(e).removeClass('has-error');
        $(e).find('p.help-block-error').hide();
    });
});
JS
            );
            ?>
            <?= Html::button('Создать задачу на отправление в фонд', [
                'class' => 'btn btn-primary buttonAdd',
                'name'  => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
        </div>
        <?php ActiveForm::end(); ?>

<?php
$bill = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETN, Yii::$app->user->id);
/** @var \common\models\piramida\Wallet $wallet */
$wallet = $bill['wallet'];
?>
        <?php if ($wallet->amount > 0) { ?>
            <p style="margin-top: 30px;"><a href="/cabinet-prizm/out-index" style="font-size: 20px;">Задачи на вывод</a>:</p>
            <p>Возврат PZM на кошелек владельца</p>
            <p>
                <a class="btn btn-danger" href="/cabinet-prizm/out">Вернуть PZM</a>
            </p>
        <?php } ?>

        <!-- Large modal -->

        <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Пополнение кошелька в системе баллов</h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-success">Успешно</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalInfo3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Пополнение кошелька в системе баллов</h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-success">Успешно. Зачислено <span id="pzm_letn" style="font-weight: bold;"></span> LETN и <span id="pzm_letm" style="font-weight: bold;"></span> LETM</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalInfo0" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Пополнение кошелька в системе баллов</h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-danger">Пока еще ничего не найдено.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalInfo2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Пополнение кошелька в системе баллов</h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-danger" id="modalInfo2_text">Пользователь указал в задаче транзакцию а она уже была обработана как
                            положительная</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
