<?php

use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */


$this->title = 'Пакеты';

$model = new \avatar\models\forms\PrizmInput();

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a></h1>
        <!-- Large modal -->

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Справка по обменнику</h4>
                    </div>
                    <div class="modal-body">
                        <h2><?= Html::encode($this->title) ?></h2>
                        <p>Справочная система в разработке. Скоро обновим</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->registerJs(<<<JS
        $(document).ready(function(){
          //при нажатию на любую кнопку, имеющую класс .btn
          $("#modalHelp").click(function() {
            //открыть модальное окно с id="myModal"
            $("#myModal").modal('show');
          });
        });
JS
        );
        ?>
    </div>

    <?= $this->render('_menu4') ?>

    <br>
    <div class="col-lg-8">

        <?php
        $this->registerJs(<<<JS

$('.buttonIN').click(function(e) {
    $('#modalAdd').modal();
});
$('.buttonSEND').click(function(e) {
    $('#sendToPull').modal();
});
$('.buttonOUT').click(function(e) {
    $('#returnToOwner').modal();
});
var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});
$('.buttonCheck').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-input/check',
        data: {id: $(this).data('id')},
        success: function(ret) {
            if (ret.status == 1) {
                $('#pzm').html(ret.amount_formatted);
                $('#modalInfo3').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            } else if (ret.status == 0) {
                $('#modalInfo0').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            } else if (ret.status == 2) {
                $('#modalInfo2').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();    
                });
            }
        }
    });
});
$('.buttonClose').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-input/close',
        data: {id: $(this).data('id')},
        success: function(ret) {
            window.location.reload();
        }
    });
});
$('.buttonSENDconfirm').click(function(e) {
    ajaxJson({
        url: '/cabinet-task-input/pull-input',
        success: function(ret) {
            $('#sendToPull').on('hidden.bs.modal', function(e) {
                $('#modalInfoPull').modal().on('hidden.bs.modal', function(e) {
                    window.location.reload();
                });
            }).modal('hide');
        }
    });
});
JS
        );
        ?>





        <?php
        $sb = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::SB);
        $sbp = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::SBP);
        $walletSB = $sb['wallet'];
        $walletSBP = $sbp['wallet'];
        ?>
        <?php if ($walletSB->amount > 0 || $walletSBP->amount > 0) { ?>
            <p style="margin-top: 30px;">Отправление PZM в пул</p>
            <p>
                <button class="btn btn-primary buttonSEND">Отправить в пул</button>
            </p>

            <p style="font-size: 16px;">Баланс LEGAT: <?= Yii::$app->formatter->asDecimal($walletSB->amount / 100, 2) ?></p>
            <p style="font-size: 16px;">Баланс LETN: <?= Yii::$app->formatter->asDecimal($walletSBP->amount / 100, 2) ?></p>
            <p style="font-size: 16px;">Баланс LETN c процентами 0,7 в день: <span id="percentSBP" data-amount="<?= $walletSBP->amount ?>"></span></p>
            <?php
            // вычисляю кол-во секунд на момент сегодня 00:00 МСК
            $now = new \DateTime();
            $date00 = new \DateTime($now->format('Y-m-d') . ' 00:00:00', new DateTimeZone('Etc/GMT-3'));
            $seconds = $now->format('U') - $date00->format('U');

            $this->registerJs(<<<JS

// кол-во секунд которое прошло от 00:00 МСК до момента рисования страницы
var delta0 = {$seconds};

// кол-во миллисекунд которое прошло после рисования страницы
var delta1 = 0;

// кол-во миллисекунд через которые обновлять счетчик
var timeDelta = 100;

setInterval(function(e) {
    var amount = $('#percentSBP').data('amount');
    
    amount = amount / 100;
    
    // кол-во монет которое надо начислить за секунду (1 сек)
    var amount1sec = (amount * 1.007 - amount) / 86400;

    // кол-во монет которое надо начислить за это мгновение (100 мс)
    var amount100mc = amount1sec / (1000 / timeDelta);
    
    amount = amount + delta0 * amount1sec + (delta1 / timeDelta) * amount100mc;
    
    // округляю до 7 знака
    var round = 7;
    var amountInt = parseInt(amount);
    var amountOstatok = amount - amountInt;
    amountOstatok = amountOstatok * Math.pow(10, round);
    amountOstatok = parseInt(amountOstatok);
    
    $('#percentSBP').html(amountInt + ',' + amountOstatok);
    delta1 += timeDelta;
}, timeDelta);
JS
            );
            ?>
            <p style="margin-top: 30px;"><a href="/cabinet-prizm/out-index" style="font-size: 20px;">Задачи на вывод</a>:</p>
            <p>Возврат PZM на кошелек владельца</p>
            <p>
                <a class="btn btn-danger" href="/cabinet-prizm/out">Вернуть PZM</a>
            </p>
        <?php } ?>

    </div>

</div>

