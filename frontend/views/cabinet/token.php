<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Токены';


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a></h1>
        <!-- Large modal -->

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Справка по обменнику</h4>
                    </div>
                    <div class="modal-body">
                        <h2><?= Html::encode($this->title) ?></h2>
                        <p>Справочная система в разработке. Скоро обновим</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->registerJs(<<<JS
        $(document).ready(function(){
          //при нажатию на любую кнопку, имеющую класс .btn
          $("#modalHelp").click(function() {
            //открыть модальное окно с id="myModal"
            $("#myModal").modal('show');
          });
        });
JS
        );
        ?>
    </div>
    <?= $this->render('_menu4') ?>

    <br>
    <div class="col-lg-8">
        <p class="alert alert-danger">Функциональный модуль в разработке</p>
    </div>
</div>



