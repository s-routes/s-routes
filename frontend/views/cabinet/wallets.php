<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\UserWallet */

$this->title = Yii::t('c.j8zN5pFpQT', 'Кошельки');

?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <p class="alert alert-success">
                <?= \Yii::t('c.FMg0mLqFR9', 'Успешно обновлено') ?>.
            </p>

            <p>
                <a href="/cabinet/wallets" class="btn btn-primary">
                    <?= \Yii::t('c.j8zN5pFpQT', 'Кошельки') ?>
                </a>
            </p>
        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>

            <?php
            $curr = [
                \common\models\avatar\Currency::NEIRO,
                \common\models\avatar\Currency::BTC,
                \common\models\avatar\Currency::ETH,
                \common\models\avatar\Currency::USDT,
                \common\models\avatar\Currency::LTC,
                \common\models\avatar\Currency::PZM,
                \common\models\avatar\Currency::TRX,
                \common\models\avatar\Currency::DASH,
                \common\models\avatar\Currency::BNB,
                \common\models\avatar\Currency::DAI,
                83, // DOGE
            ];
            ?>
            <?php foreach ($curr as $c) { ?>
                <?php
                $c2 = \common\models\avatar\Currency::findOne($c);
                $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $c]);
                $mod = \common\models\CurrencyIoModification::find()->where(['currency_io_id' => $cio->id])->all();
                ?>
                <?php if (count($mod) > 0) { ?>
                    <?php /** @var \common\models\CurrencyIoModification $m */ ?>
                    <?php foreach ($mod as $m) { ?>
                        <?= $form->field($model, $m->wallet_field_name)->label($c2->title . ' ' . $m->name) ?>
                    <?php } ?>
                <?php } else { ?>
                    <?= $form->field($model, $cio->field_name_wallet)->label($c2->title) ?>
                <?php } ?>
            <?php } ?>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.FMg0mLqFR9', 'Обновить'), [
                    'class' => 'btn btn-success',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



