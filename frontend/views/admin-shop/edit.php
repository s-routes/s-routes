<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\ShopAll */

$this->title = $model->id;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'      => $model,
            'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-shop/index';
    }).modal();
}
JS

        ]); ?>
        <?= $form->field($model, 'link') ?>
        <?= $form->field($model, 'type')->dropDownList([
            \common\models\ShopAll::TYPE_FEDERAL => 'Федеральный',
            \common\models\ShopAll::TYPE_REGION  => 'Региональный',
        ]) ?>
        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'about')->textarea(['rows' => 10]) ?>
        <?= $form->field($model, 'rekvisit_karti')->textarea(['rows' => 10]) ?>
        <h3>Аккаунт на внешнем магазине</h3>
        <?= $form->field($model, 'account_login') ?>
        <?= $form->field($model, 'account_password') ?>
        <hr>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>
    </div>
</div>
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>