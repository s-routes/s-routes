<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все страницы';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="<?= Url::to(['admin-page/add']) ?>" class="btn btn-default">Добавить</a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-page/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});


$('.rowTable').click(function() {
    window.location = '/admin-page/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );

        $cols = [
            'id',
            'name',
        ];
        foreach (\common\models\Language::find()->all() as $l) {
            $cols[] = [
                'header'  => $l->code,
                'content' => function ($item, $p2, $p3, $p4) {
                    if (is_null(\common\models\PageLang::findOne(['parent_id' => $item['id'], 'language' => $p4->header]))) {
                        return Html::a($p4->header, ['admin-page/edit-lang', 'id' => $item['id'], 'lang' => $p4->header], ['data' => ['pjax' => 0]]);
                    } else {
                        return Html::a($p4->header, ['admin-page/edit-lang', 'id' => $item['id'], 'lang' => $p4->header], ['data' => ['pjax' => 0], 'class' => 'label label-success']);
                    }
                }
            ];
        }
        $cols[] = [
            'header'  => 'Удалить',
            'content' => function ($item) {
                return Html::button('Удалить', [
                    'class' => 'btn btn-danger btn-xs buttonDelete',
                    'data'  => [
                        'id' => $item['id'],
                    ]
                ]);
            }
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\exchange\Page::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => $cols,
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>