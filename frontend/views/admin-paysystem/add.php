<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\PaySystem */

$this->title = 'Добавить платежную систему';

?>
<div class="container">
    <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


    <?php if (!is_null($id = Yii::$app->session->getFlash('contactFormSubmitted'))) : ?>

        <div class="alert alert-success">
            Успешно добавлено.
        </div>

    <?php else: ?>


        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php $form = ActiveForm::begin([
                    'id' => 'contact-form',
                    'options' => ['enctype' => 'multipart/form-data']
                ]); ?>
                <?= $model->field($form, 'code') ?>
                <?= $model->field($form, 'title') ?>
                <?= $model->field($form, 'comission_percent') ?>
                <?= $model->field($form, 'comission_fix') ?>
                <?= $model->field($form, 'class_name') ?>
                <?= $model->field($form, 'currency') ?>
                <?= $model->field($form, 'image') ?>

                <hr>
                <div class="form-group">
                    <?= Html::submitButton('Добавить', [
                        'class' => 'btn btn-default',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>

    <?php endif; ?>
</div>
