<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = Yii::t('c.06gARzoaQE', 'Статистика рынка');


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>

    <div class="col-lg-12">
        <h2 class="page-header text-center"><?= \Yii::t('c.06gARzoaQE', 'Сервисные валюты') ?></h2>
        <?php
        $sort = new \yii\data\Sort([
            'attributes'   => [
                'code',
                'title'            => ['label' => Yii::t('c.06gARzoaQE', 'Наименование')],
                'price_usd'        => ['label' => Yii::t('c.06gARzoaQE', 'Цена').', $', 'default' => SORT_DESC],
                'chanche_1d'       => ['label' => '1 '.Yii::t('c.06gARzoaQE', 'день').', %', 'default' => SORT_DESC],
                'chanche_1w'       => ['label' => '1'.Yii::t('c.06gARzoaQE', 'нед').', %', 'default' => SORT_DESC],
                'day_volume_usd'   => ['label' => Yii::t('c.06gARzoaQE', 'Объем за 24ч.').', USD', 'default' => SORT_DESC],
                'market_cap_usd'   => ['label' => Yii::t('c.06gARzoaQE', 'Капитализация').', USD', 'default' => SORT_DESC],
                'available_supply' => ['label' => Yii::t('c.06gARzoaQE', 'Монет в обороте'), 'default' => SORT_DESC],
                'total_supply',
                'max_supply',
                'rank'             => ['label' => Html::tag('span', '#', ['title' => Yii::t('c.06gARzoaQE', 'Рейтинг'), 'data' => ['toggle' => 'tooltip']])],
            ],
            'defaultOrder' => [
                'market_cap_usd' => SORT_DESC
            ]
        ]);

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\avatar\Currency::find()
                    ->where(['id' => \common\base\Application::$walletListFirst])
                    ->asArray()
                ,
                'sort'       => ['defaultOrder' => ['sort_index' => SORT_ASC]],
                'pagination' => [
                    'pageSize' => 100
                ]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover table_exchange',
                'align' => 'center',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label'   => Yii::t('c.06gARzoaQE', 'Картинка'),
                    'header'  => Yii::t('c.06gARzoaQE', 'Картинка'),
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "img-circle",
                            'width'  => 50,
                            'height' => 50,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'label'          => 'code',
                    'attribute'      => 'code',
                    'contentOptions' => ['aria-label' => 'Code'],
                ],
                [
                    'label'          => 'title',
                    'attribute'      => 'title',
                    'contentOptions' => ['aria-label' => 'Title'],
                ],
                [
                    'attribute'      => 'price_usd',
                    'contentOptions' => [
                        'aria-label' => 'Price',
                        'style'      => 'text-align:right;font-weight: bold;',
                        'nowrap'     => 'nowrap',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'content'        => function ($item) {
                        $i = $item['price_usd'];
                        if ($item['id'] == \common\models\avatar\Currency::USDT) {
                            $i = 1;
                        }
                        if (is_null($i)) {
                            return '';
                        }
                        if ($i < 0) {
                            $style[] = 'color: #F45F5F';
                        } else {
                            $style[] = 'color: #6ED098';
                        }

                        $v = Yii::$app->formatter->asDecimal($i, 4);
                        $arr = explode('.', $v);
                        $style[] = 'color: #888';
                        $nullsHtml = [];
                        $arr2 = \cs\services\Str::getChars($arr[1]);
                        $colors = [
                            '#888',
                            '#999',
                            '#aaa',
                            '#bbb',
                        ];
                        for ($i = 0; $i < 4; $i++) {
                            $nullsHtml[] = Html::tag(
                                'span',
                                $arr2[$i],
                                ['style' => 'color: ' . $colors[$i]]
                            );
                        }

                        return $arr[0] . '.' . join('', $nullsHtml);
                    },
                ],
            ],
        ]) ?>

        <h2 class="page-header text-center"><?= \Yii::t('c.06gARzoaQE', 'Валюты') ?></h2>
        <?php \yii\widgets\Pjax::begin(); ?>
        <?php

        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();


$('.rowTable').click(function() {
});
JS
        );

        $model = new \avatar\models\search\StatisticCurrency();
        $provider = $model->search(Yii::$app->request->get(), null, $sort);

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'  => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover table_exchange',
                'align' => 'center',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'label'   => Yii::t('c.06gARzoaQE', 'Картинка'),
                    'header'  => Yii::t('c.06gARzoaQE', 'Картинка'),
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "img-circle",
                            'width'  => 50,
                            'height' => 50,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'label'          => 'code',
                    'attribute'      => 'code',
                    'contentOptions' => ['aria-label' => 'Code'],
                ],
                [
                    'label'          => 'title',
                    'attribute'      => 'title',
                    'contentOptions' => ['aria-label' => 'Title'],
                ],
                [
                    'header'         => $sort->link('market_cap_usd'),
                    'attribute'      => 'market_cap_usd',
                    'contentOptions' => [
                        'aria-label' => 'Капитализация',
                        'style'      => 'text-align:right;',
                        'nowrap'     => 'nowrap',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'format'         => ['decimal', 0],
                ],
                [
                    'attribute'      => 'price_usd',
                    'contentOptions' => [
                        'aria-label' => 'Price',
                        'style'      => 'text-align:right;font-weight: bold;',
                        'nowrap'     => 'nowrap',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'content'        => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'price_usd', '');
                        if ($i == '') return '';

                        if ($i < 0) {
                            $style[] = 'color: #F45F5F';
                        } else {
                            $style[] = 'color: #6ED098';
                        }
                        $v = Yii::$app->formatter->asDecimal($i, 2);
                        $arr = explode('.', $v);
                        $style[] = 'color: #888';
                        $nullsHtml = [];
                        $arr2 = \cs\services\Str::getChars($arr[1]);
                        $colors = [
                            '#888',
                            '#999',
                            '#aaa',
                            '#bbb',
                        ];
                        for ($i = 0; $i < 2; $i++) {
                            $nullsHtml[] = Html::tag(
                                'span',
                                $arr2[$i],
                                ['style' => 'color: ' . $colors[$i]]
                            );
                        }

                        return $arr[0] . '.' . join('', $nullsHtml);
                    },
                ],
                [
                    'header'         => $sort->link('day_volume_usd'),
                    'attribute'      => 'day_volume_usd',
                    'contentOptions' => [
                        'aria-label' => 'Объем за 24 ч. USD',
                        'style'      => 'text-align:right;',
                        'nowrap'     => 'nowrap',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'format'         => ['decimal', 0],
                ],

                [
                    'header'         => $sort->link('chanche_1d'),
                    'attribute'      => 'chanche_1d',
                    'contentOptions' => [
                        'aria-label' => '1 день %',
                        'style'      => 'text-align:right;',
                    ],
                    'headerOptions'  => [
                        'style' => 'text-align:right;',
                    ],
                    'content'        => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'chanche_1d', '');
                        if ($i == '') return '';

                        if ($i < 0) {
                            $style[] = 'color: #F45F5F';
                        } else {
                            $style[] = 'color: #6ED098';
                        }

                        return Html::tag('span', (($i > 0) ? '+' : '') . Yii::$app->formatter->asDecimal($i, 2), ['style' => join(';', $style)]);
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>


