<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

$id = 1;
$page = \common\models\exchange\Page::findOne($id);


$pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
if (is_null($pageLang)) {
    $name = $page->name;
    $content = $page->content;
} else {
    $name = $pageLang->name;
    $content = $pageLang->content;
}

$this->title = $name;


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
        <ul class="nav nav-tabs">
           <!-- <li><a href="/shop/index">Магазин</a></li>-->
            <li><a href="/page/dogovor-oferty">Договор Оферты</a></li>
            <li class="active"><a href="/page/opisanie-tovarov-uslug">Описание товаров и услуг</a></li>
            <li><a href="/page/yuridicheskaya-informatsiya">Контакты и реквизиты</a></li>
        </ul>
    </div>

    <div class="col-lg-1">
    </div>
    <div class="col-sm-10" style="margin-top: 50px;">
        <?= $content ?>
    </div>
</div>


