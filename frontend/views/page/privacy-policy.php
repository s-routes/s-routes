<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$id = 9;
$page = \common\models\exchange\Page::findOne($id);


$pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
if (is_null($pageLang)) {
    $name = $page->name;
    $content = $page->content;
} else {
    $name = $pageLang->name;
    $content = $pageLang->content;
}

$this->title = $name;
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="col-lg-6 col-lg-offset-3">
        <?= $content ?>
    </div>
</div>


