<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */

$this->title = Yii::t('c.iaMMvnmasA', 'Инструкции');


$rows = \common\models\Instrukciya::find()
    ->innerJoin('page', 'page.id = instrukciya.page_id')
    ->select([
        'page.*'
    ])
    ->orderBy(['sort_index' => SORT_ASC])
    ->asArray()
    ->all();
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

    </div>

    <div class="col-lg-1">

    </div>
    <div class="col-sm-10">
        <?php /** @var \common\models\exchange\Page $page */ ?>
        <?php foreach ($rows as $page) { ?>
            <?php
            $pageLang = \common\models\PageLang::findOne(['parent_id' =>$page['id'], 'language' => Yii::$app->language]);
            if (is_null($pageLang)) {
                $name = $page['name'];
            } else {
                $name = $pageLang->name;
            }
            ?>
            <p><a href="/page/item?id=<?= $page['id'] ?>"><?= $name ?></a></p>
        <?php } ?>

    </div>
</div>


