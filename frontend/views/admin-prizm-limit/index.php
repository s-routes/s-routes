<?php


/** @var \yii\web\View $this */
/** @var \avatar\models\forms\AdminPrizmLimit $model */

$this->title = 'PRIZM лимит';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header">PRIZM лимит</h1>

        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function(e) {
        window.location = '/admin/index'; 
    }).modal();
}
JS
            ,

        ]); ?>
        <?= $form->field($model, 'param') ?>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>

    </div>
</div>

<!-- Large modal -->
<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Информация</h4>
            </div>
            <div class="modal-body">
                <p class="alert alert-success">Успешно</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>