<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Заявки на ввод';


?>

<div style="margin: 0px 50px 0px 50px;">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS

$('.rowTable').click(function() {
    // window.location = '/admin-buh/out-item' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonCancel').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-buh/cancel',
            data: {id: id},
            success: function (ret) {
                window.location = '/admin-buh/input' + '?' + 'id' + '=' + id;
            }, 
            errorScript: function (ret) {
                if (ret.id == 102) {
                    var errorString = [];
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            errorString.push(key + ': ' + value.join('<br>'));
                        }
                    }
                    new Noty({
                        timeout: 1000,
                        theme: 'relax',
                        type: 'success',
                        layout: 'bottomLeft',
                        text: errorString.join('<br>')
                    }).show();
                }
            } 
        });
    }
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\TaskInput::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => ['defaultOrder' => ['created_at' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Клиент',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'user_id', '');
                        if ($i == '') return '';
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                            'title' => $user->getName2(),
                            'data' => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Wallet',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'account', '');
                        if ($i == '') return '';
                        return $i;

                    },
                ],
                [
                    'header'         => 'Заказ',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        if ($item['status'] != 1) {
                            return '';
                        }

                        $v = \yii\helpers\ArrayHelper::getValue($item, 'amount', 0);
                        $cio = \common\models\CurrencyIO::findOne($item['currency_io_id']);
                        $cint = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                        $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                        return Yii::$app->formatter->asDecimal($a, $cint->decimals_view);

                    },
                ],
                [
                    'header'         => 'Монет',
                    'content'        => function ($item) {
                        $cio = \common\models\CurrencyIO::findOne($item['currency_io_id']);
                        $c = \common\models\avatar\Currency::findOne($cio->currency_ext_id);
                        $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cio->currency_int_id, $item['user_id']);
                        $bill = $data['billing'];
                        $code = Html::a($c->code, ['cabinet-wallet/item', 'id' => $bill->id]);
                        $cHtml = Html::tag('span', $code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                        return $cHtml;
                    },
                ],
                [
                    'header'         => 'Блокчейн',
                    'content'        => function ($item) {
                        if (\cs\Application::isEmpty($item['modification_id'])) return '';
                        return \common\models\CurrencyIoModification::findOne($item['modification_id'])->name;
                    },
                ],
                'txid',
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'  => 'Статус',
                    'content' => function ($item) {
                        $s = [
                            0 => 'создана',
                            1 => 'задача подтверждена магазином',
                            2 => 'отменена',
                        ];
                        $color  = [
                            0 => 'default',
                            1 => 'success',
                            2 => 'warning',
                        ];

                        return Html::tag('span', $s[$item['status']], ['class' => 'label label-'.$color[$item['status']]]);
                    }
                ],
                [
                    'header'  => 'Карточка',
                    'content' => function ($item) {
                        return Html::a('Карточка', ['cabinet-users/view',  'id' => $item['user_id']], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Отменить',
                    'content' => function ($item) {
                        if ($item['status'] == 0) {
                            return Html::button('Отменить', [
                                'class' => 'btn btn-default btn-xs buttonCancel',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);
                        }
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>