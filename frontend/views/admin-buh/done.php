<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\AdminBuhDone */

$this->title = 'Завершить вывод';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (!is_null($id = Yii::$app->session->getFlash('form'))) : ?>

            <p class="alert alert-success">
                Успешно завершено.
            </p>

            <a class="btn btn-info" href="/admin-buh/index">Все заявки</a>

        <?php else: ?>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>

                    <?php
                    $currency_id = \common\models\avatar\UserBill::findOne($model->task['billing_id'])->currency;
                    if (is_null($currency_id)) {
                        return '';
                    }
                    $CIO = \common\models\CurrencyIO::findOne(['currency_ext_id' => $currency_id]);
                    $cint = \common\models\piramida\Currency::findOne($CIO->currency_int_id);
                    ?>

                        <?php if (in_array($currency_id, [
                            \common\models\avatar\Currency::ETH,
                            \common\models\avatar\Currency::EGOLD,
                            \common\models\avatar\Currency::USDT,
                            \common\models\avatar\Currency::BNB,
                            \common\models\avatar\Currency::NEIRO,
                            \common\models\avatar\Currency::LEGAT,
                            \common\models\avatar\Currency::PZM,
                            \common\models\avatar\Currency::BTC,
                            \common\models\avatar\Currency::LTC,
                            \common\models\avatar\Currency::DASH,
                            \common\models\avatar\Currency::TRX,
                        ])) { ?>

                            <?php
                            $amount = $model->task['amount'];
                            $comission_user = $model->task['comission_user'];
                            $amount_exit = $amount - $comission_user;

                            $amount = bcdiv($amount, bcpow(10, $cint->decimals), $cint->decimals);
                            $comission_user = bcdiv($comission_user, bcpow(10, $cint->decimals), $cint->decimals);
                            $amount_exit = bcdiv($amount_exit, bcpow(10, $cint->decimals), $cint->decimals);
                            ?>

                            <p>Заявка на вывод: <?= Yii::$app->formatter->asDecimal($amount, $cint->decimals_view) ?></p>
                            <p>К выводу: <?= Yii::$app->formatter->asDecimal($amount_exit, $cint->decimals_view) ?> (Комиссия: <?= Yii::$app->formatter->asDecimal($comission_user, $cint->decimals_view) ?>)</p>
                        <?php } ?>

                    <?= $form->field($model, 'txid') ?>
                    <?= $form->field($model, 'comission') ?>
                    <?= $form->field($model, 'code') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Завершить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
