<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Заявки на вывод';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
?>

<h1 class="page-header"><?= Html::encode($this->title) ?></h1>

<div style="margin: 0px 50px 0px 50px;">
<?php \yii\widgets\Pjax::begin(); ?>
<?php

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {
    
    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
$arrayOutBinance = \yii\helpers\Json::encode(\common\base\Application::$binanceAutoOutput);
$cidPZM = \common\models\avatar\Currency::PZM;

$this->registerJS(<<<JS

$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите сокрытие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-buh/hide',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});

$('.buttonBinance').click(function (e) {
    var id = $(this).data('id');
    var currency_id = $(this).data('currency_id');
    
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/admin-buh/done-ajax2',
            data: {
                id: id
            },
            success: function (ret) {
                window.location.reload();
            },
            errorScript: function(ret2) {
                if (ret2.id == 102) {
                    var errorString = [];
                    for (var key in ret2.data) {
                        if (ret2.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret2.data[key];
                            errorString.push(name + ': ' + value.join('<br>'));
                        }
                    }
                    
                    new Noty({
                        timeout: 1000,
                        theme: 'relax',
                        type: 'warning',
                        layout: 'bottomLeft',
                        text: errorString.join('<br>')
                    }).show();
                }
            }
        });  
    }
});

$('.buttonPzm').popover({
    trigger: 'focus',
    html: true,
    placement: 'bottom',
    title: 'Способ вывода',
    content: '<p><a href="javascript:void(0);" class="btn btn-default buttonAuto">Автоматически</button> <a href="javascript:void(0);" style="margin-left: 10px;" class="btn btn-default buttonManual">В ручную</a>'
}).on('shown.bs.popover', function(e1,c) {

    $('.buttonManual').click(function (e) {
        var b = $(e1.currentTarget);
        window.location = '/admin-buh/done?id=' + b.data('id');
    });
    
    $('.buttonAuto').click(function (e) {
        var b = $(e1.currentTarget);
        var id = b.data('id');
        var currency_id = $(this).data('currency_id');
        
        if (confirm('Вы уверены?')) {
            ajaxJson({
                url: '/admin-buh/done-pzm-ajax',
                data: {
                    id: id
                },
                success: function (ret) {
                    window.location.reload();
                },
                errorScript: function(ret2) {
                    if (ret2.id == 102) {
                        var errorString = [];
                        for (var key in ret2.data) {
                            if (ret2.data.hasOwnProperty(key)) {
                                var name = key;
                                var value = ret2.data[key];
                                errorString.push(name + ': ' + value.join('<br>'));
                            }
                        }
                        
                        new Noty({
                            timeout: 1000,
                            theme: 'relax',
                            type: 'warning',
                            layout: 'bottomLeft',
                            text: errorString.join('<br>')
                        }).show();
                    }
                }
            }); 
        }
    });
});
$('.buttonStart').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите начало обработки заявки')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-buh/start',
            data: {id: id},
            success: function (ret) {
                window.location = '/admin-buh/done' + '?' + 'id' + '=' + id;
            }, 
            errorScript: function (ret) {
                if (ret.id == 102) {
                    var errorString = [];
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            errorString.push(key + ': ' + value.join('<br>'));
                        }
                    }
                    new Noty({
                        timeout: 1000,
                        theme: 'relax',
                        type: 'success',
                        layout: 'bottomLeft',
                        text: errorString.join('<br>')
                    }).show();
                }
            } 
        });
    }
});

$('.buttonDone').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var id = $(this).data('id');
    window.location = '/admin-buh/done' + '?' + 'id' + '=' + id;
});

$('.rowTable').click(function() {
});
JS
);

$currencyETH = \common\models\avatar\Currency::findOne(['code' => 'ETH']);
Yii::$app->session->set('$currencyETH', $currencyETH->price_usd);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ActiveDataProvider([
        'query'      => \common\models\TaskOutput::find()
        ,
        'pagination' => [
            'pageSize' => 20,
        ],
        'sort'       => ['defaultOrder' => ['created_at' => SORT_DESC]],
    ]),
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        'id',
        [
            'header'  => 'Клиент',
            'content' => function ($item) {
                $i = ArrayHelper::getValue($item, 'user_id', '');
                if ($i == '') return '';
                $user = \common\models\UserAvatar::findOne($i);

                return Html::img($user->getAvatar(), [
                    'class'  => "img-circle",
                    'width'  => 60,
                    'height' => 60,
                    'style'  => 'margin-bottom: 0px;',
                    'title' => $user->getName2(),
                    'data' => [
                        'toggle' => 'tooltip',
                    ],
                ]);
            },
        ],
        [
            'header'  => 'Wallet',
            'content' => function ($item) {
                $html = [];
                $i = ArrayHelper::getValue($item, 'account', '');
                if ($i == '') return '';
                $html[] = Html::tag('abbr', $i, ['data' => ['toggle' => 'tooltip', 'clipboard-text' => $i], 'title' => 'Нажмите чтобы скопировать', 'class' => 'buttonCopy']);;

                if ($item['currency_id'] == \common\models\avatar\Currency::PZM) {
                    $i = $item['prizm_key'];
                    $html[] = Html::tag('abbr', $i, ['data' => ['toggle' => 'tooltip', 'clipboard-text' => $i], 'title' => 'Нажмите чтобы скопировать', 'class' => 'buttonCopy']);;
                }

                return join('<br>', $html);
            },
        ],
        [
            'header'         => 'txid',
            'content'        => function ($item) {
                $txid = \yii\helpers\ArrayHelper::getValue($item, 'txid', '');
                $currency_io_id = $item['currency_io_id'];
                $CIO = \common\models\CurrencyIO::findOne($currency_io_id);

                // PRIZM
                if (in_array($CIO->currency_int_id, [\common\models\piramida\Currency::PZM])) {
                    return Html::a('http://blockchain.prizm.space/index.html', 'http://blockchain.prizm.space/index.html', ['target' => '_blank']) . '<br>' . Html::tag('abbr', $txid, ['data' => ['toggle' => 'tooltip', 'clipboard-text' => $txid], 'title' => 'Нажмите чтобы скопировать', 'class' => 'buttonCopy']);
                }
                // ETH
                if (
                        in_array($CIO->currency_int_id, [
                            \common\models\piramida\Currency::ETH,
                            \common\models\piramida\Currency::BNB,
                        ])
                        or
                        (
                            ($CIO->currency_int_id == \common\models\piramida\Currency::USDT) and
                            ($item['modification_id'] == 1)
                        )
                ) {
                    $d = substr($txid,0,6) . '...';
                    return Html::a($d, 'https://etherscan.io/tx/' . $txid, ['target' => '_blank']);
                }
                // TRX
                if (in_array($CIO->currency_int_id, [\common\models\piramida\Currency::TRX])) {
                    if (\cs\Application::isEmpty($txid)) {
                        return $item['binance_id'];
                    }
                    $d = substr($txid,0,6) . '...';
                    return $item['binance_id'] . '<br>' .Html::a($d, 'https://tronscan.org/#/transaction/' . $txid, ['target' => '_blank']) . Html::tag('abbr', 'Копировать', ['data' => ['toggle' => 'tooltip', 'clipboard-text' => $txid], 'title' => 'Нажмите чтобы скопировать', 'class' => 'buttonCopy']) ;
                }

                return $txid;
            },
        ],
        [
            'header'         => 'Заказ',
            'headerOptions'  => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right'],
            'content'        => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'amount', 0);
                $currency_id = \common\models\avatar\UserBill::findOne($item['billing_id'])->currency;
                if (is_null($currency_id)) {
                    return '';
                }
                $CIO = \common\models\CurrencyIO::findOne(['currency_ext_id' => $currency_id]);
                $cint = \common\models\piramida\Currency::findOne($CIO->currency_int_id);
                $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                return Yii::$app->formatter->asDecimal($a, $cint->decimals_view);
            },
        ],
        [
            'header'         => 'Монет',
            'content'        => function ($item) {
                $currency_id = \common\models\avatar\UserBill::findOne($item['billing_id'])->currency;
                if (is_null($currency_id)) {
                    return '';
                }

                $c = \common\models\avatar\Currency::findOne($currency_id);

                $a1 = Html::a($c->code, ['cabinet-wallet/item', 'id' => $item['billing_id']], ['data' => ['pjax' => 0]]);
                $cHtml = Html::tag('span', $a1, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                return $cHtml;
            },
        ],
        [
            'header'         => 'Блокчейн',
            'content'        => function ($item) {
                if (\cs\Application::isEmpty($item['modification_id'])) return '';
                return \common\models\CurrencyIoModification::findOne($item['modification_id'])->name;
            },
        ],
        'comment:text:Комментарий',
        [
            'header'  => 'Создано',
            'content' => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            },
        ],
        [
            'header'  => 'К выводу',
            'content' => function ($item) {
                $currency_id = \common\models\avatar\UserBill::findOne($item['billing_id'])->currency;
                $CIO = \common\models\CurrencyIO::findOne(['currency_ext_id' => $currency_id]);
                $cint = \common\models\piramida\Currency::findOne($CIO->currency_int_id);
                $v = $item['amount'] - $item['comission_user'];
                $v1 = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                return Yii::$app->formatter->asDecimal($v1, $cint->decimals_view);
            },
        ],
        [
            'header'  => 'Статус',
            'content' => function ($item) {
                switch ($item['status']) {
                    case \common\models\TaskOutput::STATUS_CREATED:
                        $html = Html::tag('span', 'Создана', ['class' => 'label label-default']);
                        break;
                    case \common\models\TaskOutput::STATUS_HIDE:
                        $html = Html::tag('span', 'Отменена', ['class' => 'label label-warning']);
                        break;
                    case \common\models\TaskOutput::STATUS_INIT:
                        $html = Html::tag('span', 'Начата', ['class' => 'label label-info']);
                        break;
                    case \common\models\TaskOutput::STATUS_DONE:
                        $html = Html::tag('span', 'Выполнена', ['class' => 'label label-success']);
                        break;
                    default:
                        $html = '';
                }
                return $html;
            }
        ],
        [
            'header'  => 'Выполнить',
            'content' => function ($item) {
                $class = 'buttonBinance';
                if ($item['currency_id'] == \common\models\avatar\Currency::PZM) {
                    $class = 'buttonPzm';
                }

                return Html::button('Выполнить', [
                    'class' => 'btn btn-success btn-xs ' . $class,
                    'data'  => [
                        'id'              => $item['id'],
                        'status'          => $item['status'],
                        'currency_id'     => $item['currency_id'],
                        'modification_id' => $item['modification_id'],
                    ],
                ]);
            }
        ],
        [
            'header'  => 'Перейти в заявку',
            'content' => function ($item) {
                return Html::a('Перейти в заявку', ['admin-buh/out-item', 'id' => $item['id']], [
                    'class' => 'btn btn-primary btn-xs',
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            }
        ],
        [
            'header'  => 'Чат',
            'content' => function ($item) {
                return Html::a('Чат', ['admin-support/chat',  'id' => $item['user_id']], [
                    'class' => 'btn btn-default btn-xs',
                    'data'  => [
                        'id' => $item['id'],
                    ]
                ]);
            }
        ],
        'txid_internal',
    ],
]) ?>
<?php \yii\widgets\Pjax::end(); ?>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
