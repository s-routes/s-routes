<?php

/** @var $this \yii\web\View */
/** @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;



?>
<?php $this->beginPage() ?>

<?= $content ?>

<?php $this->endPage() ?>
