<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

?>

<?php if (!Yii::$app->user->isGuest)  { ?>
    <?php $c = \common\models\shop\Basket::getCount(); ?>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brands" href="#">
                    <a class="navbar-brands" href="/">
                        <img src="/images/controller/landing/index/N3-w.png" alt="logo" class="logo_img"
                        style="margin-top: 26px;"
                        >

                    </a>
                </a>

                <li class="hidden-sm hidden-md hidden-lg"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><button type="button" class="btns btn-danger" data-toggle="button"><img src="<?= Yii::$app->user->identity->getAvatar() ?>" class="img-profile" width="30"><?= \Yii::t('c.cKsbnadvgD', 'МОЙ КАБИНЕТ') ?></button></a>
                    <?= $this->render('user-menu') ?>
                </li>


            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-list mr-1" style="padding: 7px 6px;"></i><?= \Yii::t('c.cKsbnadvgD', 'СЕРВИСЫ') ?></a>
                        <ul class="dropdown-menu">
                            <li><a href="/cabinet-exchange/index"><?= \Yii::t('c.cKsbnadvgD', 'СЕРВИС P2P ОБМЕНА') ?></a></li>
                            <li><a href="/page/statistic"><?= \Yii::t('c.cKsbnadvgD', 'КУРСЫ КРИПТОВАЛЮТ') ?></a></li>
                        </ul>
                    </li>
                    <li class="hidden-xs">
                        <a href="/cabinet-exchange/index"
                           data-toggle="tooltip"
                           title="<?= \Yii::t('c.cKsbnadvgD', 'Сервис P2P обмена') ?>"
                           data-placement="bottom"
                           style="margin-top: -6px;"
                           >
                            <img src="/images/exchange2.png" width="40">
                        </a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">


                    <li class="hidden-xs" style="margin-top: 35px;">
                        <?php
                        $this->registerJs(<<<JS
$('.js-basketCounter').click(function (e) {
    window.location = '/shop/cart';
});
JS
                        )
                        ?>
                        <span class="label label-success js-basketCounter" role="button" title="Корзина" data-toggle="tooltip" data-placement="bottom"><?= \common\models\shop\Basket::getCount() ?></span>
                    </li>
                    <li class="hidden-xs">
                        <a href="/cabinet-bills/index">
                            <img src="/images/controller/cabinet-bills/index/wallet2.png" width="40" alt=""
                             data-toggle="tooltip"
                             title="<?= \Yii::t('c.cKsbnadvgD', 'Мои балансы') ?>"
                             data-placement="bottom"
                            >
                        </a>
                    </li>
                    <li class="hidden-xs" id="dLabel">
                        <?php
                        /** @var \common\models\UserAvatar $user */
                        $user = Yii::$app->user->identity;

                        $lastNewsItem = \common\models\NewsItem::find()->orderBy(['id' => SORT_DESC])->one();
                        if (is_null($lastNewsItem)) {
                            $kolokol = '/images/layouts/cabinet/0001.png';
                        } else {
                            if (is_null($user->last_see_kolokol)) {
                                $kolokol = '/images/layouts/cabinet/0001.png';
                                $last_see_kolokol = null;
                            } else {
                                $last_see_kolokol = $user->last_see_kolokol;
                                // Если повледняя новость свежее чем я последний раз смотрел в колокольчик, то анимация
                                if ($lastNewsItem['created_at'] > $user->last_see_kolokol) {
                                    $kolokol = '/images/layouts/cabinet/6ccff0e8fbc8d4b2a1a1a4ca6f683b83.gif';
                                } else {
                                    // иначе фиксированный колокольчик
                                    $kolokol = '/images/layouts/cabinet/0001.png';
                                }
                            }
                        }

                        $this->registerJs(<<<JS
$('#dLabel').on('shown.bs.dropdown', function () {
  ajaxJson({
    url: '/cabinet/last-see-kolokol',
    data: {id: 1},
    success: function (ret) {
        
    }
  });
});
JS
);
                        ?>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?= $kolokol ?>" class="img-profile" width="35"></a>
                        <ul class="dropdown-menu" >
                            <?php foreach (\common\models\NewsItem::find()->orderBy(['id' => SORT_DESC])->limit(10)->all() as $n) { ?>
                                <?php
                                $name = $n['name'];
                                $title = '';
                                if (\cs\services\Str::length($n['name']) > 15) {
                                    $name = \cs\services\Str::sub($n['name'], 0, 15) . ' ...';
                                    $title = ' title="' . $n['name'] . '"';
                                }
                                $isNew = false;
                                $new = '';
                                if (!is_null($user->last_see_kolokol)) {
                                    if ($n['created_at'] > $user->last_see_kolokol) {
                                        $isNew = true;
                                        $new = ' ' . Html::tag('span', '(новое)', ['style' => 'color: red; font-size: 75%;']);
                                    }
                                }
                                ?>
                                <li><a href="<?= Url::to(['news/item', 'id' => $n['id']])?>" <?= $title ?>><?= $name ?><?= $new ?></a></li>
                            <?php } ?>

                            <li role="separator" class="divider"></li>
                            <li><a href="/news/index" data-method="post">Все новости</a></li>
                        </ul>
                    </li>
                    <li class="hidden-xs">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?= Yii::$app->user->identity->getAvatar() ?>" class="img-profile" width="35"><?= \Yii::t('c.cKsbnadvgD', 'МОЙ КАБИНЕТ') ?></a>
                        <?= $this->render('user-menu') ?>
                    </li>
                    <li class="hidden-xs">
                        <?php
                        $lang = \common\models\Language::findOne(['code' => Yii::$app->language]);
                        ?>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($lang->image, 'crop') ?>" class="img-profile" width="35"><?= $lang->title ?></a>
                        <?php \avatar\assets\Language\Asset::register($this); ?>
                        <ul class="dropdown-menu">
                            <?php foreach (\common\models\Language::find()->where(['status' => \common\models\Language::STATUS_DONE])->all() as $l) { ?>
                                <li>
                                    <a href="javascript:void(0);" class="buttonLanguageSet" data-code="<?= $l->code ?>">
                                        <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($l->image, 'crop') ?>" width="20">
                                        <?= $l->title ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li role="separator" class="divider"></li>
                            <li><a href="/page/item?id=27">ImTranslator universal</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

<?php } else { ?>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brands" href="#">
                    <a class="navbar-brands" href="/" target="_blank"><img
                    src="/images/controller/landing/index/N3-w.png" alt="logo"
                    style="margin-top: 26px;"
                    ></a>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/auth/login"><i class="fa fa-user rounded-circle bg-white mr-1" style="padding: 7px 9px;"></i>ВОЙТИ</a></li>
                    <li><a href="/auth/registration"><i class="fa fa-user-plus rounded-circle bg-white mr-1" style="padding: 7px 6px;"></i>РЕГИСТРАЦИЯ</a></li>
                </ul>
            </div>
        </div>
    </nav>
<?php } ?>


<div style="flex-grow: 1;">

    <?php
    $tstart30 = Yii::$app->params['technical-pause']['start'] - 30 * 60;
    $tstart = Yii::$app->params['technical-pause']['start'];
    $tfinish = Yii::$app->params['technical-pause']['finish'];
    ?>

    <?php
    $t = time();
    $isShowTechnicalInfo = ($t > $tstart30 && $t < $tfinish);
    ?>

    <div class="row js-technical-pause" style="display: <?= ($isShowTechnicalInfo)? 'block' : 'none' ?>">
        <div class="col-lg-10 col-md-8">
            <p class="alert alert-danger
         " style="
    margin-left: 20px;
    margin-right: 20px;
">Уважаемые пользователи! Мы планируем обновить программное обеспечение с <b><?= Yii::$app->formatter->asDatetime(Yii::$app->params['technical-pause']['start']) ?></b> по <b><?= Yii::$app->formatter->asDatetime(Yii::$app->params['technical-pause']['finish']) ?></b>. Перед началом технических работ запущен таймер. Просьба корректно выйти из системы, т.к. сервисы все равно временно будут недоступны. После окончания техработ, весь функционал будет доступен вам в полном объеме.

            </p>
        </div>
        <div class="col-lg-2 col-md-4">



            <?php
            if ($isShowTechnicalInfo) {
                \avatar\assets\CountDown\Asset::register($this);
                $this->registerJs(<<<JS
var d1 = new Date();
d1.setTime({$tstart}000);
var y = d1.getFullYear();
var m = d1.getMonth() + 1;
if (m < 10) m = '0' + m;
var d = d1.getDate();
var h = d1.getHours();
if (h < 10) h = '0' + h;
var i = d1.getMinutes();
if (i < 10) i = '0' + i;
var s = d1.getSeconds();
if (s < 10) s = '0' + s;
$('.countdown2').downCount({
    date: m + '/' + d + '/' + y + ' ' + h + ':' + i + ':' + s,
    offset: -(d1.getTimezoneOffset() / 60)
}, function () {
    $('.countdown2').html($('<p>').html('Ведутся технические работы'));
});
JS
                );
            }
            ?>

            <ul class="countdown2" style="text-align: left;">
                <li>
                    <span class="minutes">00</span>
                    <p class="minutes_ref">минут</p>
                </li>
                <li class="seperator">:</li>
                <li>
                    <span class="seconds">00</span>
                    <p class="seconds_ref">секунд</p>
                </li>
            </ul>
        </div>
    </div>


    <?php // admin-notify  ?>
    <?php
    $adminNotify = null;
    $adminNotifyData = \common\models\Config::get('admin-notify');
    $isData = false;
    if ($adminNotifyData !== false) {
        if (!is_null($adminNotifyData)) {
             if ($adminNotifyData != '') {
                $isData = true;
            }
        }
    }
    ?>

    <?php if ($isData) { ?>
        <?php
        $adminNotify = \yii\helpers\Json::decode($adminNotifyData);
        $t = time();
        $start_date = $adminNotify['start_date'];
        $start_time = $adminNotify['start_time'];
        $start = DateTime::createFromFormat('d.m.Y H:i', $start_date . ' ' . $start_time);
        $finish_date = $adminNotify['finish_date'];
        $finish_time = $adminNotify['finish_time'];
        $finish = DateTime::createFromFormat('d.m.Y H:i', $finish_date . ' ' . $finish_time);
        $adminNotifyText = $adminNotify['text'];

        $isShowAdminNotify = ($t > $start->format('U') && $t < $finish->format('U'));
        ?>
        <?php if ($isShowAdminNotify) { ?>

            <div class="row js-technical-pause">
                <div class="col-lg-10 col-md-8 col-lg-offset-1 col-md-offset-2">
                    <p class="alert alert-danger" style="
margin-left: 20px;
margin-right: 20px;
"><?= $adminNotifyText ?></p>
                </div>
            </div>

        <?php } ?>
    <?php } ?>