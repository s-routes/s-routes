<?php
/**
 * Created by PhpStorm.
 * User: Ramha
 * Date: 14.03.2018
 * Time: 2:59
 */
?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173391471-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-173391471-1');
</script>

