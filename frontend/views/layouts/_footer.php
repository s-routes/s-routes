<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 21.01.2017
 * Time: 18:16
 */

/** @var \avatar\services\CustomizeService $CustomizeService */
/** @var array $companySocialNets telegram, facebook, vk, skype, youtube */

/**
 * Last Update.
 * User: slpv
 * Date: 12.12.2019
 * Time: 13:29
 */
$user_id = Yii::$app->user->id;
\avatar\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);
\avatar\assets\CountDown\Asset::register($this);

?>

</div>

<footer>
    <div class="sections">
        <div class="section">

            <div class="messegers">
            </div>

            <div class="title"><?= \Yii::t('c.oQgAWT3MNM', '© Все права защищены. neiro-n.com 2019-2020') ?></div>

        </div>
    </div>
</footer>

<?php
$serverName = \avatar\assets\SocketIO\Asset::getHost();

$str1038 = Yii::t('c.AXve2MgR8k', 'Статус сделки');
$str1039 = Yii::t('c.AXve2MgR8k', 'Скрыть');
$str1040 = Yii::t('c.AXve2MgR8k', 'Сделка');
$str1041 = Yii::t('c.AXve2MgR8k', 'Новое сообщение от');
$str1042 = Yii::t('c.AXve2MgR8k', 'Перейти в сделку');
$str1043 = Yii::t('c.AXve2MgR8k', 'Открыта сделка');
$str1044 = Yii::t('c.AXve2MgR8k', 'Ведутся технические работы');
$str1045 = Yii::t('c.AXve2MgR8k', 'Скоро ожидается проведение технических работ, ознакомьтесь с объявлением на верху страницы');
$str1046 = Yii::t('c.AXve2MgR8k', 'Сделка разобрана арбитром и завершена');
$str1047 = Yii::t('c.AXve2MgR8k', 'Решение было принято в пользу');
$str1048 = Yii::t('c.AXve2MgR8k', 'Партнер вызвал арбитра');
$str1049 = Yii::t('c.AXve2MgR8k', 'Приглашен арбитр');
$str1050 = Yii::t('c.AXve2MgR8k', 'Сделкка требует арбитра');
$str1051 = Yii::t('c.AXve2MgR8k', 'Принять и перейти в сделку');
$str1052 = Yii::t('c.AXve2MgR8k', 'Сделка открыта, ожидайте подтверждения');
$str1053 = Yii::t('c.AXve2MgR8k', 'Сделка подтверждена и средства заблокированы');
$str1054 = Yii::t('c.AXve2MgR8k', 'Средства отправлены');
$str1055 = Yii::t('c.AXve2MgR8k', 'Средства получены');
$str1056 = Yii::t('c.AXve2MgR8k', 'Партнер поставил оценку и сделка завершена');
$str1057 = Yii::t('c.AXve2MgR8k', 'Арбитр принял сделку');
$str1058 = Yii::t('c.AXve2MgR8k', 'Сделка отменена');

/**
 * УВЕДОМЛЕНИЯ типа ПУШ (начало)
 */
if (!Yii::$app->user->isGuest) {
    $this->registerJs(<<<JS

if (typeof socket == 'undefined') {
    var socket = io.connect('{$serverName}');
}

let myid_f = {$user_id};

function nott(deal, name, mes) {
    var noty = new Noty({
        timeout: 10000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: '<a href="/cabinet-exchange/deal?id=' + deal + '"><b>' + '{$str1040}' + ' №' + deal + '</b></a>' +
              '<br>' + '{$str1041}' + ' ' + name + 
              '<br>' + mes + '<br>',
        buttons: [
            Noty.button(
                '{$str1042}', 'btn btn-success', 
                function () {
                    location.href = "/cabinet-exchange/deal?id=" + deal
                },
                {
                id: 'button1',
                'data-status': 'ok'
                }),
                
            Noty.button('{$str1039}', 'btn btn-success', 
                function () {
                    noty.close();
                })
        ]
    });

    noty.show();
}
function PushDeal(deal, status) {
    
    let statuses = [
        " ", 
        "{$str1052}", 
        "{$str1053}", 
        "{$str1054}", 
        "{$str1055}", 
        "{$str1056}", 
        "", 
        "{$str1057}", 
        "", 
        "", 
        "{$str1058}"
    ];
    
    var noty = new Noty({
        theme: 'nest',
        type: 'info',
        layout: 'bottomLeft',
        text: '<b>' + '{$str1038}' + ' #' + deal + '</b>' +
              '<br>' + statuses[status] + '<br>',
        buttons: [
            Noty.button('{$str1039}', 'btn btn-success', 
                function () {
                    noty.close();
                })
        ]
    });
    noty.show();
}

socket.emit('new-user', 0, myid_f);

socket.on('deal-opened', function(deal_id, user_id, partner_user_id) {
    console.log(["deal-opened", deal_id, user_id, partner_user_id]);
    if (partner_user_id == myid_f) {
        STATUS = 1;
        var noty = new Noty({
            theme: 'relax',
            type: 'warning',
            layout: 'bottomLeft',
            text: '<a href="/cabinet-exchange/deal-action?id=' + deal_id + '"><b>' + '{$str1040}' + ' №' + deal_id + '</b></a>' +
                  '<br>' + '{$str1043}',
            buttons: [
                Noty.button(
                    '{$str1042}', 'btn btn-success', 
                    function () {
                        location.href = "/cabinet-exchange/deal-action?id=" + deal_id
                    },
                    {
                    id: 'button1',
                    'data-status': 'ok'
                    }
                ),
                    
                Noty.button('{$str1039}', 'btn btn-success', 
                    function () {
                        noty.close();
                    })
            ]
        });
    
        noty.show();
    }     
});

socket.on('push', datap => {
    data = datap;
    if ((new URL(window.location.href)).searchParams.get("id") != data.deal) {
        if ($.inArray(myid_f, data.to_user_id) >= 0) {
            nott(data.deal, data.name, data.message);
        } 
    }
});

/**
* Технический перерыв
* @param int start время начала * 1000
*/
socket.on('technical-pause-alert', function(start) {
    console.log(["technical-pause-alert"]);
    
    var d1 = new Date();
    d1.setTime(start);
    var y = d1.getFullYear();
    var m = d1.getMonth() + 1;
    if (m < 10) m = '0' + m;
    var d = d1.getDate();
    var h = d1.getHours();
    if (h < 10) h = '0' + h;
    var i = d1.getMinutes();
    if (i < 10) i = '0' + i;
    var s = d1.getSeconds();
    if (s < 10) s = '0' + s;
    $('.countdown2').downCount({
        date: m + '/' + d + '/' + y + ' ' + h + ':' + i + ':' + s,
        offset: -(d1.getTimezoneOffset() / 60)
    }, function () {
        $('.countdown2').html($('<p>').html('{$str1044}'));
    });
    
    var noty = new Noty({
        theme: 'nest',
        type: 'info',
        layout: 'bottomLeft',
        text: '{$str1045}'
    });
    noty.show();
    
    $('.js-technical-pause').show();
});

socket.on('reload-status', data => {
    console.log(["reload-status", 'footer', data]);
    if (data.user_id == myid_f) {
        STATUS = data.status;
        PushDeal(data.deal, data.status);
    }     
});

socket.on('accept-arbitr', (deal_id, user_id, to) => {
    console.log(["accept-arbitr", deal_id, user_id, to]);
    if ($.inArray(myid_f, to) >= 0) {
        PushDeal(deal_id, 7);
    }     
});

/**
* 
* @param int deal_id идентификатор сделки
* @param int user_id идентификатор пользователя от кого пришло уведомление
* @param array to массив идентификаторов пользователей кому нужно показать уведомление
* @param object to_user объект пользователя в чью пользу принято решение по сделке {id,name, avatar}
*/
socket.on('arbitr-result-deal', (deal_id, user_id, to, to_user) => {
    console.log(["arbitr-result-deal", deal_id, user_id, to, to_user]);
    if ($.inArray(myid_f, to) >= 0) {
        var noty = new Noty({
            timeout: 5000,
            theme: 'nest',
            type: 'info',
            layout: 'bottomLeft',
            text: '<b>'+'{$str1038}'+' #' + deal_id + '</b>' +
                   '<br>' + '{$str1046}' + '<br>' + '{$str1047} ' + to_user.name,
        });
        noty.show();
        
        // Перекрашиваю кнопки в красный
        $('.c-iconStep span').removeClass('label-warning').addClass('label-danger');
        
        // Статвлю статус сделки
        $('#label-status').html('{$str1046}');
    }     
});

/**
* 
* @param int    deal_id     идентификатор сделки
* @param int    user_id     идентификатор пользователя от кого пришло уведомление
* @param array  to          массив идентификаторов пользователей кому нужно показать уведомление
* @param object to_user     объект пользователя в чью пользу принято решение по сделке {id,name, avatar}
*/
socket.on('arbitr-result-offer', (deal_id, user_id, to, to_user) => {
    console.log(["arbitr-result-offer", deal_id, user_id, to, to_user]);
    if ($.inArray(myid_f, to) >= 0) {
        var noty = new Noty({
            timeout: 5000,
            theme: 'nest',
            type: 'info',
            layout: 'bottomLeft',
            text: '<b>' + '{$str1038}' + ' #' + deal_id + '</b>' +
                   '<br>' + '{$str1046}' + '<br>' + '{$str1047} ' + to_user.name,
        });
        noty.show();
        
        // Перекрашиваю кнопки в красный
        $('.c-iconStep span').removeClass('label-warning').addClass('label-danger');

        // Статвлю статус сделки
        $('#label-status').html('{$str1046}');
    }     
});
            
/**
* Уведомление от партнера что он вызвал арбитра
* @param int deal_id
* @param int user_id
* @param int to_user
*/
socket.on('need-arbitr2', function(deal_id, user_id, to_user) {
    console.log('need-arbitr2', deal_id, user_id, to_user);
    
    if (myid_f == to_user) {
        var noty = new Noty({
            timeout: 10000,
            theme: 'relax',
            type: 'warning',
            layout: 'bottomLeft',
            text: '<b>' + '{$str1038}' + ' #' + deal_id + '</b>' +
                   '<br>' + '{$str1048}'
        });
    
        noty.show();
        
        // статусы закрасить в оранжевый
        $('.c-iconStep span').removeClass('label-success').removeClass('label-default').addClass('label-warning');
        
        // Скрыть кнопки
        $('.buttonSellMoneySended').hide();
        $('.buttonSellMoneyReceived').hide();
        $('.buttonBuyMoneySended').hide();
        $('.buttonBuyMoneyReceived').hide();
        $('.buttonInviteArbitrator').hide();
        
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str1049}'));
        
        // Вместо счетчика вывести надпись
        $('#label-status').removeClass('label-info').addClass('label-warning').html('{$str1049}');
    }
});

JS
    );
    $isProd = (YII_ENV_PROD)? 1 : 2;
if (Yii::$app->user->can('permission_arbitrator')) {
    $this->registerJs(<<<JS

socket.on('new-arbitr', function(user_id) {
    console.log('new-arbitr', user_id);
});

/**
* Уведомление для арбитра
* @param int deal_id
* @param int user_id
* @param int partner_user_id
*/
socket.on('need-arbitr', function(deal_id, user_id, partner_user_id) {
    console.log('need-arbitr', deal_id, user_id, partner_user_id);
    
    var noty = new Noty({
        timeout: 10000,
        theme: 'relax',
        type: 'warning',
        layout: 'bottomLeft',
        text: '<a href="/cabinet-arbitrator/item?id=' + deal_id + '"><b>'+'{$str1040}'+' №' + deal_id + '</b></a>' +
              '<br>' + '{$str1050}',
        buttons: [
            Noty.button(
                '{$str1051}', 'btn btn-success', 
                function () {
                    ajaxJson({
                        url: '/cabinet-arbitrator/accept' + '?' + 'id' + '=' + deal_id,
                        success: function (ret) {
                            socket.emit('accept-arbitr', deal_id, {$user_id}, [user_id, partner_user_id]);
                            window.location = '/cabinet-arbitrator/item?id=' + deal_id;
                        }
                    });
                },
                {
                    "id": 'button1',
                    'data-status': 'ok'
                }),
                
            Noty.button('{$str1039}', 'btn btn-success', 
            function () {
                noty.close();
            })
        ]
    });

    noty.show();
});

socket.emit('new-arbitr', myid_f);

JS
);
}

}
?>
