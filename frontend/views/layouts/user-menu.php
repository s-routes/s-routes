<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 18.05.2016
 * Time: 7:45
 *
 */

use yii\helpers\Url;
use yii\helpers\Html;

/** @var $this \yii\web\View */
?>
<ul class="dropdown-menu">
    <li><a href="/cabinet/index"><?= \Yii::t('c.cKsbnadvgD', 'ПРОФИЛЬ') ?></a></li>
    <li><a href="/cabinet-bills/index"><?= \Yii::t('c.cKsbnadvgD', 'МОИ БАЛАНСЫ') ?></a></li>
    <li><a href="/cabinet-shop-requests/index">Мои заказы</a></li>

    <?php if (Yii::$app->user->can('permission_admin')) { ?>
        <li><a href="/admin/index"><?= \Yii::t('c.cKsbnadvgD', 'Админка') ?></a></li>
    <?php } ?>
    <?php if (Yii::$app->user->can('permission_arbitrator')) { ?>
        <li><a href="/cabinet-arbitrator/index"><?= \Yii::t('c.cKsbnadvgD', 'Арбитр') ?></a></li>
    <?php } ?>
    <?php if (Yii::$app->user->can('permission_buh')) { ?>
        <li><a href="/admin-buh/index"><?= \Yii::t('c.cKsbnadvgD', 'Бухгалтер') ?></a></li>
    <?php } ?>
    <li><a href="<?= Url::to(['page/help']) ?>"><?= \Yii::t('c.cKsbnadvgD', 'Инструкции') ?></a></li>
    <li><a href="/cabinet-support/chat"><?= \Yii::t('c.cKsbnadvgD', 'Служба поддержки') ?></a></li>
    <li role="separator" class="divider"></li>
    <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post"><?= \Yii::t('c.cKsbnadvgD', 'ВЫЙТИ') ?></a></li>
</ul>