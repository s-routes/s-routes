<?php

/** $this \yii\web\View  */

use common\models\avatar\Currency;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Отчет';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php
        $rows = [];

        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        // Проверяю баланс на бинансе
        if (YII_ENV_PROD) {
            $data = $provider->_callApiKeyAndSigned('/api/v3/account', []);
        } else {
            $data = ['balances' => []];
        }

        /**
         * @var array $balanceList
         * [
         *  'BTC' => '0.154',
         *  'LTC' => '0.0154',
         *  ...
         * ]
         */
        $balanceList = ArrayHelper::map($data['balances'], 'asset', 'free');
        $cList = [
            Currency::PZM,
            Currency::ETH,
            Currency::USDT,

            Currency::BTC,
            Currency::LTC,
            Currency::BNB,
            Currency::DASH,
            Currency::NEIRO,
            Currency::EGOLD,
            Currency::TRX,
            Currency::MARKET,
            Currency::DAI,
            Currency::DOGE,
            Currency::LOT,
        ];
        $rows2 = [];

        $CuList = Currency::find()->where(['id' => $cList])->orderBy(['sort_index' => SORT_ASC])->all();

        /**
         * @var \common\models\avatar\Currency $c
         */
        foreach ($CuList as $c) {
            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($c);
            $ids = \common\models\avatar\UserBill::find()->where(['currency' => $c->id])->select(['address'])->column();
            $sum = \common\models\piramida\Wallet::find()->where(['in', 'id', $ids])->select('sum(amount)')->scalar();
            $item = [
                'int_id' => $cInt->id,
                'code' => $cInt->code,
                'sum'  => \common\models\piramida\Currency::getValueFromAtom($sum, $cInt->id),
            ];
            $item['binance'] = '';
            foreach ($data['balances'] as $row) {
                if ($row['asset'] == $cInt->code) {
                    $item['binance'] = $row['free'];
                }
            }
            $rows2[] = $item;
        }
        $rows = $rows2;
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $data = [
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns' => [
                [
                    'header' => 'code',
                    'content' => function($item) {
                        $cExt = Currency::findOne(['code' => $item['code']]);
                        if (is_null($cExt)) return $item['code'];
                        if (\cs\Application::isEmpty($cExt->image)) return $item['code'];

                        $imgOptions = [
                            'width' => 50,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $cExt->code,
                            ],
                        ];

                        return Html::img($cExt->image, $imgOptions);
                    }

                ],
                'sum',
                'binance',
                [
                    'header' => 'balance',
                    'content' => function($item) {

                        if ($item['code'] == 'PZM') {
                            $cio = \common\models\CurrencyIO::findFromExt(Currency::PZM);
                            ;
                            /** @var \common\services\PrizmApi $PrizmApi */
                            $PrizmApi = Yii::$app->PrizmApi;

                            $ret = $PrizmApi->post('index', [
                                'requestType' => 'getBalance',
                                'account'     => $cio->master_wallet,
                            ]);
                            $b = $ret['balanceNQT'];

                            return Yii::$app->formatter->asDecimal($b/100, 2);
                        }
                        return '';
                    }

                ],
                [
                    'header' => 'main_wallet',
                    'content' => function($item) {

                        if (in_array($item['int_id'], [
                            \common\models\piramida\Currency::NEIRO,
                            \common\models\piramida\Currency::LOT,
                            \common\models\piramida\Currency::MARKET
                        ])) {
                            $cio = \common\models\CurrencyIO::findFromInt($item['int_id']);
                            $w = \common\models\piramida\Wallet::findOne($cio->main_wallet);

                            return Yii::$app->formatter->asDecimal($w->getAmountWithDecimals(), 2);
                        }

                        return '';
                    }

                ],
                [
                    'header' => 'diff',
                    'content' => function($item) {
                        if ($item['code'] == 'PZM') {
                            $cio = \common\models\CurrencyIO::findFromExt(Currency::PZM);
                            ;
                            /** @var \common\services\PrizmApi $PrizmApi */
                            $PrizmApi = Yii::$app->PrizmApi;

                            $ret = $PrizmApi->post('index', [
                                'requestType' => 'getBalance',
                                'account'     => $cio->master_wallet,
                            ]);
                            $b = $ret['balanceNQT'];

                            return Yii::$app->formatter->asDecimal($item['sum'] - ($b/100), 2);
                        }

                        if (in_array($item['int_id'], [
                                \common\models\piramida\Currency::NEIRO,
                                \common\models\piramida\Currency::LOT,
                                \common\models\piramida\Currency::MARKET
                        ])) {
                            $cio = \common\models\CurrencyIO::findFromInt($item['int_id']);
                            $w = \common\models\piramida\Wallet::findOne($cio->main_wallet);

                            return Yii::$app->formatter->asDecimal($item['sum'] - $w->getAmountWithDecimals(), 2);
                        }

                        if (!\cs\Application::isDouble($item['sum'])) return '';
                        if (!\cs\Application::isDouble($item['binance'])) return '';

                        return $item['sum'] - $item['binance'];
                    }

                ],

            ]
        ]) ?>
    </div>
</div>
