<?php
/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 10.01.2021
 * Time: 22:04
 */

/** @var $CurrencyIO    \common\models\CurrencyIO */
/** @var $mod           \common\models\MasterTransaction[] */
/** @var $currencyExt   \common\models\avatar\Currency */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$attribute = $CurrencyIO->field_name_wallet;
$modification_id = Yii::$app->request->get('modification_id', null);
if (is_null($modification_id)) {
    $modification_id = $mod[0]['id'];
}
/** @var \common\models\CurrencyIoModification $mod */
$mod2 = \common\models\CurrencyIoModification::findOne($modification_id);
$attribute = $mod2->wallet_field_name;
?>


<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
    <?php /** @var \common\models\MasterTransaction $m */ ?>
    <?php foreach ($mod as $m) { ?>
        <?php
        $options_a = [
            'href' => \yii\helpers\Url::to(['', 'id' => $CurrencyIO->id, 'modification_id' => $m['id']]),
        ];
        $options_li = [];
        $class = '';
        if ($m['id'] == $modification_id) {
            $options_li['class'] = 'active';
            $options_a['style'] = Html::cssStyleFromArray(['background-color' => '#ccc']);
        }
        ?>
        <li <?= Html::renderTagAttributes($options_li)?>><a <?= Html::renderTagAttributes($options_a)?>><?= $m['name'] ?></a></li>
    <?php } ?>
</ul>




<!-- Tab panes -->
<div class="tab-content" style="margin-top: 20px;">
    <?php /** @var \common\models\CurrencyIoModification $m */ ?>
    <?php foreach ($mod as $m) { ?>
        <?php
        $class = '';
        if ($m['id'] == $modification_id) {
            $class = ' active';
        }
        ?>
        <div role="tabpanel" class="tab-pane<?= $class ?>" id="tab<?= $m['id'] ?>">

            <?php if ($m['id'] == $modification_id) { ?>
                <?= $this->render('item_mod2', [
                    'CurrencyIO'             => $CurrencyIO,
                    'currencyExt'            => $currencyExt,
                    'CurrencyIoModification' => $m,
                ]) ?>
            <?php } ?>

        </div>
    <?php } ?>
</div>