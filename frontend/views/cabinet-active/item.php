<?php

use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use \Endroid\QrCode\ErrorCorrectionLevel;

/** @var $this          \yii\web\View */
/** @var $CurrencyIO    \common\models\CurrencyIO */
/** @var $form          \yii\bootstrap\ActiveForm */

$currencyExt = \common\models\avatar\Currency::findOne($CurrencyIO->currency_ext_id);
$this->title = $CurrencyIO->tab_title;

$attribute = $CurrencyIO->field_name_wallet;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$str900 = Yii::t('c.TTaRzGgiYr', 'Скопировано');
$modification_id = Yii::$app->request->get('modification_id', null);

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a></h1>
        <!-- Large modal -->

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <?php
                        $id = 26;
                        $page = \common\models\exchange\Page::findOne($id);

                        $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                        if (is_null($pageLang)) {
                            $name = $page->name;
                            $content = $page->content;
                        } else {
                            $name = $pageLang->name;
                            $content = $pageLang->content;
                        }
                        ?>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?= $name ?></h4>
                    </div>
                    <div class="modal-body">
                        <?= $content ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
        );
        ?>
    </div>

    <div class="col-lg-12">

        <?php
        $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str900}'
    }).show();

});

JS
        );
        ?>
        <?php if (\common\models\TaskInput::find()
            ->where([
                'user_id'        => Yii::$app->user->id,
                'status'         => 0,
                'currency_io_id' => $CurrencyIO->id,
                'modification_id' => $modification_id,
            ])->count() > 0
        ) { ?>



        <?php }  ?>

        <?php
        $mod = \common\models\CurrencyIoModification::find()->where(['currency_io_id' => $CurrencyIO->id])->all();

        ?>
        <?php if (count($mod) > 0) { ?>
            <?= $this->render('item_mod', [
                'CurrencyIO'  => $CurrencyIO,
                'currencyExt' => $currencyExt,
                'mod'         => $mod,
            ]) ?>
        <?php } else { ?>
            <?= $this->render('item_standart', [
                'CurrencyIO'  => $CurrencyIO,
                'currencyExt' => $currencyExt,
            ]) ?>
        <?php } ?>







        <!-- Large modal -->
        <div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title"><?= \Yii::t('c.TTaRzGgiYr', 'Пополнение средств') ?></h4>
                    </div>
                    <div class="modal-body">
                        <p class="alert alert-success"><?= \Yii::t('c.TTaRzGgiYr', 'Отлично!') ?></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
