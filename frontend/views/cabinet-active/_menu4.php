<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\Url;




/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;

$rows = \common\models\CurrencyIO::find()
    ->where(['currency_io.is_io' => 1])
    ->innerJoin('currency', 'currency.id = currency_io.currency_ext_id')
    ->select(['currency_io.*'])
    ->orderBy(['currency.sort_index' => SORT_ASC])
    ->all();

$menu = [];
/** @var \common\models\CurrencyIO $row */
foreach ($rows as $row) {
    $currencyExt = \common\models\avatar\Currency::findOne($row->currency_ext_id);
    $currencyInt = \common\models\piramida\Currency::findOne($row->currency_int_id);
    $menu[] = [
        'url'     => '/cabinet-active/item?id=' . $row->id,
        'label'   => $currencyInt->name,
        'urlList' => [
            ['pathWithParam', '/cabinet-active/item', ['id' => $row->id]],
        ],
    ];
}

/**
 * @param \cs\services\Url $u
 * @param array $params параметры которые должны содержаться в $u
 * @return bool
 * true все параметры $params существуют в $u
 * false один из параметров $params отсутствуют в $u
 */
function hasRoute12_params($u, $params)
{
    foreach ($params as $k => $v) {
        if (!isset($u->params[$k])) {
            return false;
        }
        if ($u->params[$k] != $v) {
            return false;
        }
    }

    return true;
}

function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;

                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith(Yii::$app->request->url, $i[1])) return true;
                    break;

                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;

                case 'url':
                    if (Yii::$app->request->url == $i[1]) return true;
                    break;

                case 'pathWithParam':
                    $u = new \cs\services\Url(Yii::$app->request->getAbsoluteUrl());
                    if ($u->path == $i[1] && hasRoute12_params($u, $i[2])) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<ul class="nav nav-tabs">
    <?php
    foreach ($menu as $item) {
        $optionsLI = [];
        $options = ['class' => []];
        if (hasRoute12($item, Yii::$app->requestedRoute)) {
            $optionsLI['class'][] = 'active';
        }

        $optionsClass = (isset($optionsLI['class']))? join(' ', $options['class']) : '';
        if ($optionsClass != '') $options['class'] = $optionsClass;
        $optionsLiClass = (isset($optionsLI['class']))? join(' ', $optionsLI['class']) : '';
        if ($optionsLiClass != '') $optionsLI['class'] = $optionsLiClass;

        echo Html::tag(
            'li',
            Html::a($item['label'], $item['url'], $options),
            $optionsLI
            );

    }
    ?>
</ul>
