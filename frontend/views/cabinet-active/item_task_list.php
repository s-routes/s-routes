<?php

/** @var  \common\models\CurrencyIO $CurrencyIO */

use yii\helpers\Html;

?>


<style>

    ul.countdown {
        list-style: none;
        margin: 5px 0;
        padding: 0;
        display: block;
        text-align: center;
    }
    ul.countdown li {
        display: inline-block;
    }
    ul.countdown li span {
        font-size: 40px;
        font-weight: 300;
        line-height: 80px;
    }
    ul.countdown li.seperator {
        font-size:30px;
        line-height: 70px;
        vertical-align: top;
    }
    ul.countdown li p {
        color: #000;
        font-size: 13px;
        text-align: center;
        margin-top: -10px;
    }

</style>

<?php
$this->registerJs(<<<JS

$('.buttonClose').click(function(e) {
    ajaxJson({
        url: '/cabinet-active/close',
        data: {id: $(this).data('id')},
        success: function(ret) {
            window.location.reload();
        }
    });
});
JS
);
?>

<p style="font-size: 20px;">Задачи на ввод:</p>
<?= \yii\grid\GridView::widget([
    'dataProvider' => new \yii\data\ActiveDataProvider([
        'query'      => \common\models\TaskInput::find()
            ->where([
                'user_id'        => Yii::$app->user->id,
                'status'         => 0,
                'currency_io_id' => $CurrencyIO->id,
            ])
            ->orderBy(['created_at' => SORT_DESC])
        ,
        'pagination' => [
            'pageSize' => 20,
        ],
    ]),
    'tableOptions' => [
        'class' => 'table table-striped table-hover',
    ],
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'summary'      => '',
    'columns'      => [
        'id',
        [
            'header'  => 'Тип',
            'content' => function ($item) {
                if ($item['type_id'] == \common\models\TaskInput::TYPE_ACCOUNT) return 'По адресу кошелька';
                if ($item['type_id'] == \common\models\TaskInput::TYPE_TRANSACTION) return 'По адресу транзакции';

                return '';
            },
        ],
        'account',

        [
            'header'  => 'Монет',
            'content' => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'amount_user', 0);
                if ($v == 0) return '';
                $CIO = \common\models\CurrencyIO::findOne($item['currency_io_id']);

                return \common\models\piramida\Currency::getValueFromAtom($v, $CIO->currency_int_id);
            },
        ],
        [
            'header'  => 'txid',
            'content' => function ($item) {
                $v = $item['txid'];
                if (\cs\Application::isEmpty($v) ) return '';
                if (strlen($v) > 8) {
                    $last4 = substr($v, 0, 8) . '...';
                } else {
                    $last4 = $v;
                }
                return Html::tag('abbr', $last4, ['data' => ['toggle' => 'tooltip','clipboard-text' => $v], 'class' => 'buttonCopy', 'title' => 'Нажмите чтобы скопировать']);
            },
        ],
        [
            'header'  => Yii::t('c.TTaRzGgiYr', 'Создано'),
            'content' => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            },
        ],
        [
            'header'  => Yii::t('c.TTaRzGgiYr', 'Статус'),
            'content' => function ($item) {
                return Yii::t('c.TTaRzGgiYr', 'Ожидается поступление');
            },
        ],
        [
            'header'  => '',
            'content' => function ($item) {
                return Html::button(Yii::t('c.TTaRzGgiYr', 'Отменить заявку на ввод'), [
                    'class' => 'btn btn-info buttonClose',
                    'data'  => [
                        'id' => $item['id'],
                    ]
                ]);
            }
        ],
    ],
]) ?>

<?php
\avatar\assets\CountDown\Asset::register($this);
/** @var \common\models\TaskInput $one */
$one = \common\models\TaskInput::find()
    ->where([
        'user_id'        => Yii::$app->user->id,
        'status'         => 0,
        'currency_io_id' => $CurrencyIO->id,
    ])->one();
if ($one->created_at > time() - 60*60*24) {
    $isShowCounter = true;
} else {
    $isShowCounter = false;
}
?>
<ul class="countdown" style="text-align: left;">
    <?php if ($isShowCounter) { ?>
        <li>
            <span class="hours">00</span>
            <p class="hours_ref"><?= \Yii::t('c.TTaRzGgiYr', 'часов') ?></p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="minutes">00</span>
            <p class="minutes_ref"><?= \Yii::t('c.TTaRzGgiYr', 'минут') ?></p>
        </li>
        <li class="seperator">:</li>
        <li>
            <span class="seconds">00</span>
            <p class="seconds_ref"></p>
        </li>
    <?php } else { ?>
        <p class="alert alert-warning"><?= \Yii::t('c.TTaRzGgiYr', 'Заявка завершена') ?>.</p>
    <?php } ?>
</ul>

<?php
$str908 = Yii::t('c.TTaRzGgiYr', 'Заявка завершена');
$this->registerJs(<<<JS
var d1 = new Date();
d1.setTime({$one->created_at}000 + 60 * 60 * 24 * 1000);
var y = d1.getFullYear();
var m = d1.getMonth() + 1;
if (m < 10) m = '0' + m;
var d = d1.getDate();
var h = d1.getHours();
if (h < 10) h = '0' + h;
var i = d1.getMinutes();
if (i < 10) i = '0' + i;
var s = d1.getSeconds();
if (s < 10) s = '0' + s;
$('.countdown').downCount({
    date: m + '/' + d + '/' + y + ' ' + h + ':' + i + ':' + s,
    offset: -(d1.getTimezoneOffset() / 60)
}, function () {
    $('.countdown').html($('<p>').html('{$str908}.'));
});
JS
);
?>
