<?php
/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 11.01.2021
 * Time: 16:37
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use \Endroid\QrCode\ErrorCorrectionLevel;

/** @var $this   \yii\web\View */
/** @var $CurrencyIoModification   \common\models\CurrencyIoModification */
/** @var $CurrencyIO               \common\models\CurrencyIO */
/** @var $currencyExt              \common\models\avatar\Currency */

?>

<?php


/** @var \common\models\TaskInput $task */
$task = \common\models\TaskInput::find()
    ->where([
        'user_id'        => Yii::$app->user->id,
        'status'         => 0,
        'currency_io_id' => $CurrencyIO->id,
        'modification_id' => $CurrencyIoModification->id,
    ])
    ->one();
$this->registerJs(<<<JS
$('.js-tab2').click(function () {
    $('.js-tab2').css('background-color', '');
    $(this).css('background-color', '#ccc')
});
JS
);

?>

<?php if (is_null($task)) { ?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <?php if ($CurrencyIoModification->is_api) { ?>
            <li role="presentation" class="active"><a href="#tab2-account" aria-controls="tab2-account" role="tab" data-toggle="tab" class="js-tab2" style="background-color: #ccc;">Пополнение с личного кошелька</a></li>
            <li role="presentation"><a href="#tab2-transaction" aria-controls="tab2-transaction" role="tab" data-toggle="tab" class="js-tab2">Пополнение с любого кошелька</a></li>
        <?php } else { ?>
            <li role="presentation" class="active"><a href="#tab2-transaction" aria-controls="tab2-transaction" role="tab" data-toggle="tab" class="js-tab2" style="background-color: #ccc;">Пополнение с любого кошелька</a></li>
        <?php } ?>

    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <?php if ($CurrencyIoModification->is_api) { ?>
            <?php $activeTransaction = ''; ?>
            <?php $isShowAccount = true; ?>
        <?php } else { ?>
            <?php $activeTransaction = ' active'; ?>
            <?php $isShowAccount = false; ?>
        <?php } ?>


        <?php if ($isShowAccount) { ?>
            <div role="tabpanel" class="tab-pane active" id="tab2-account" style="margin-top: 50px;">

                <?php
                $attribute = $CurrencyIoModification->wallet_field_name;
                $UserWallet = \common\models\UserWallet::findOne(['user_id' => Yii::$app->user->id]);
                $isShow = false;
                if (!is_null($UserWallet)) {
                    if (!\cs\Application::isEmpty($UserWallet->$attribute)) {
                        $isShow = true;
                    }
                }
                ?>
                <?php if (!$isShow) { ?>
                    <p class="alert alert-danger">
                        <b><?= \Yii::t('c.TTaRzGgiYr', 'Security spoiler') ?></b>: <?= \Yii::t('c.TTaRzGgiYr', 'Важно! Для безопасности') ?><br>
                        <br>
                        <?= \Yii::t('c.TTaRzGgiYr', 'При вводе средств') ?><br>
                        <br>
                        <?= \Yii::t('c.TTaRzGgiYr', 'Рекомендация! Вводите') ?>
                    </p>
                    <p><a href="/cabinet/wallets" class="btn btn-success btn-lg"><?= \Yii::t('c.TTaRzGgiYr', 'Указать кошелек для ввода') ?></a></p>
                <?php } else { ?>
                    <?php
                    $model = new \avatar\models\forms\ActiveInputMod(['account' => $UserWallet->$attribute]);
                    ?>
                    <p class="alert alert-danger">Этот способ пополнения наиболее безопасный и защищенный, но менее быстрый и простой, чем пополнение с любых кошельков массовых рассылок. Мы рекомендуем использовать всегда именно этот способ ввода средств.</p>
                    <p class="alert alert-danger"><?= $CurrencyIoModification->warning ?></p>

                    <p><?= \Yii::t('c.TTaRzGgiYr', 'Внимание! Для пополнения баланса вашего счета, необходимо последовательно выполнить следующие шаги:') ?></p>
                    <p><?= \Yii::t('c.TTaRzGgiYr', 'Шаг-1: Проверьте адрес кошелька, с которого вы будете производить пополнение. При желании, адрес можете изменить, пройдя по ссылке') ?> <a href="https://neiro-n.com/cabinet/wallets">https://neiro-n.com/cabinet/wallets</a></p>
                    <?php $form = ActiveForm::begin([
                        'id'                 => 'formAdd',
                        'enableClientScript' => false,
                        'options'            => ['enctype' => 'multipart/form-data'],
                    ]); ?>
                    <?= $form->field($model, 'account', ['inputOptions' => ['disabled' => 'disabled']]) ?>
                    <?= Html::hiddenInput(Html::getInputName($model, 'cid'), $CurrencyIO->id) ?>
                    <?= Html::hiddenInput(Html::getInputName($model, 'mod_id'), $CurrencyIoModification->id) ?>

                    <p><?= \Yii::t('c.TTaRzGgiYr', 'Шаг-2: Нажмите кнопку, расположенную ниже,') ?></p>

                    <hr>
                    <div class="form-group">
                        <?php
                        $str927 = Yii::t('c.TTaRzGgiYr', 'Добавить');
                        $this->registerJs(<<<JS
var functionAdd = function(e) {
    e.preventDefault();
    e.stopPropagation();
    var b = $(this);
    var bText = b.html();
    b.off('click');
    b.html($('<i>', {class: 'fa fa-spinner fa-spin fa-fw'}));
    b.attr('disabled', 'disabled');
    
    ajaxJson({
        url: '/cabinet-active/add-mod',
        data: $('#formAdd').serializeArray(),
        success: function(ret) {
            b.on('click', functionAdd);
            b.html(bText);
            b.removeAttr('disabled');
            $('#modalInfo').on('hidden.bs.modal', function(e) {
                window.location.reload(); 
            }).modal();
        },
        errorScript: function(ret) {
            switch (ret.id) {
                case 102:
                    var f = $('#formAdd');
                    for (var key in ret.data) {
                        if (ret.data.hasOwnProperty(key)) {
                            var name = key;
                            var value = ret.data[key];
                            var t = f.find('.field-activeinput-' + name);
                            t.addClass('has-error');
                            t.find('p.help-block-error').html(value.join('<br>')).show();
                        }
                    }
                    break;
            }
            var b = $('.buttonAdd');
            b.off('click');
            b.on('click', functionAdd);
            b.html('{$str927}');
            b.removeAttr('disabled');
        }
    });
};

$('.buttonAdd').click(functionAdd);

$('#formAdd .form-control').on('focus', function() {
    var o = $(this);
    var p = o.parent().parent();
    
    p.find('.form-group').each(function(i, e) {
        $(e).removeClass('has-error');
        $(e).find('p.help-block-error').hide();
    });
});
JS
                        );
                        ?>
                        <p class="text-center"><?= Html::button(Yii::t('c.TTaRzGgiYr', 'Создать задачу на пополнение'), [
                                'class' => 'btn btn-success btn-lg buttonAdd',
                                'name'  => 'contact-button',
                            ]) ?></p>
                    </div>
                    <?php ActiveForm::end(); ?>

                    <p><?= \Yii::t('c.TTaRzGgiYr', 'Шаг-3: Переведите монеты на мастер-кошелек платформы') ?>:</p>

                    <div class="panel panel-default">
                        <div class="panel-body text-center">

                            <?php
                            $fc = $CurrencyIoModification->function_check;
                            $is_func = false;
                            if (!\cs\Application::isEmpty($fc)) {
                                $o = new $fc();
                                if (method_exists($o, 'qrcode')) {
                                    $is_func = true;
                                }
                            }
                            ?>
                            <?php if ($is_func) { ?>
                                <?= $o->qrcode($CurrencyIoModification); ?>
                            <?php } else { ?>
                                <b style="color: red; font-size: 26px;" class="buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= $CurrencyIoModification->master_wallet ?>" title="<?= \Yii::t('c.TTaRzGgiYr', 'Нажмите чтобы скопировать') ?>"><?= $CurrencyIoModification->master_wallet ?></b>


                                <p>
                                    <?php
                                    $wallet_address = $CurrencyIoModification->master_wallet;

                                    $qr = new \Endroid\QrCode\QrCode();
                                    $qr->setText($wallet_address);
                                    $qr->setSize(180);
                                    $qr->setMargin(20);
                                    $qr->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
                                    $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
                                    $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
                                    $qr->setLabelFontSize(16);
                                    $content = $qr->writeString();
                                    $src = 'data:image/png;base64,' . base64_encode($content);
                                    ?>
                                    <img src="<?= $src ?>">
                                </p>
                            <?php } ?>


                        </div>
                    </div>

                    <p><?= \Yii::t('c.TTaRzGgiYr', 'Внимание! Лимит') ?></p>

                    <p class="alert alert-danger">
                        <i class="glyphicon glyphicon-alert"></i> <?= \Yii::t('c.TTaRzGgiYr', 'Внимание-1') ?><br> <br>
                        <i class="glyphicon glyphicon-alert"></i> <?= \Yii::t('c.TTaRzGgiYr', 'Внимание-2') ?></p>

                    <p><?= \Yii::t('c.TTaRzGgiYr', 'При <b>вводе</b> средств: адрес кошелька подставляется автоматически, и до завершения операции ввода <b>не может быть изменен</b>') ?>.</p>
                    <p><?= \Yii::t('c.TTaRzGgiYr', 'При <b>выводе</b> средств: адрес кошелька подставляется автоматически, но по вашему желанию, в любое время <b>может быть изменен</b>') ?>.</p>

                <?php } ?>

            </div>
        <?php } ?>

        <div role="tabpanel" class="tab-pane<?= $activeTransaction ?>" id="tab2-transaction" style="margin-top: 50px;">

            <p class="alert alert-danger">(!!!) Внимание: этот способ пополнения более быстрый и простой, но менее безопасный, чем пополнение со своего личного кошелька. Есть ненулевая вероятность сканирования транзакций с кошельков массовых рассылок бирж, пулов, и т.д. Вы должны это знать, и учитывать при выборе способа ввода.</p>
            <p class="alert alert-danger"><?= $CurrencyIoModification->warning ?></p>

            <p>Внимание! Для пополнения баланса вашего счета, необходимо последовательно выполнить следующие шаги:</p>
            <p>Шаг-1: В поле введите точное количество монет, которое должно поступить на ваш счет, включая дробную часть числа и соответственную разрядность. Это есть ваш ключ. (Например: вы отправляете с биржи 100.1234 монет, а в поле ввода количества указываете 100.1200 - зачисления не произойдет! Цифры должны совпадать полностью).</p>

            <p>Шаг-2: Нажмите кнопку, расположенную ниже, она называется “Создать задачу на пополнение”, - с этого момента СИСТЕМА БУДЕТ ЖДАТЬ поступление средств от вас.</p>

            <p>Шаг-3: На следующей открывшейся странице с таймером ожидания зачисления, скопируйте красный адрес мастер-кошелька для ввода монет (просто нажав на него). Переведите на этот мастер-кошелек средства с любого кошелька.
                После перевода, в соответствующее поле введите TxID или хеш (hash) транзакции. Монеты зачислятся на ваш счет при создании очередного блока в блокчейне.</p>
            <?php if ($CurrencyIoModification->master_wallet_is_binance == 1) { ?>
                <p>Если вы переводите крипто-монеты напрямую с биржи Binance (Бинанс), вам необходимо вставить скопированный на Binance TxID (хэш транзакции) в соответствующее поле ввода, ОБЯЗАТЕЛЬНО вводя перед ним слова "Internal transfer", - пример: “Internal transfer 54410018974”. Или просто на Binance копируете строку “Примечание” в информации к транзакции. Путь в ЛК Binance: Кошелек->Обзор кошелька->История транзакций->Ввод и вывод средств->Вывод. Подробная инструкция по переводам с биржи Бинанс <a href="/page/item?id=87">ЗДЕСЬ</a></p>
            <?php } ?>


            <div class="col-lg-4 row">
                <?php
                $model1 = new \avatar\models\forms\ActiveInputTransaction(['mod_id' => $CurrencyIoModification->id]);
                $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model1,
                    'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function(e) {
        window.location.reload(); 
    }).modal();
}
JS
                    ,
                    'formUrl' => '/cabinet-active/add2',

                ]); ?>
                <?= $form->field($model1, 'amount_user') ?>
                <?= Html::hiddenInput(Html::getInputName($model1, 'mod_id'), $CurrencyIoModification->id) ?>
                <?= Html::hiddenInput(Html::getInputName($model1, 'cid'), $CurrencyIO->id) ?>
                <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => Yii::t('c.TTaRzGgiYr', 'Создать задачу на пополнение')]); ?>
            </div>

        </div>
    </div>

<?php } else { ?>

    <?= $this->render('item_task_list', ['CurrencyIO' => $CurrencyIO]) ?>


    <?php if ($task->type_id == \common\models\TaskInput::TYPE_ACCOUNT) { ?>
        <p><?= \Yii::t('c.TTaRzGgiYr', 'Шаг-3: Переведите монеты на мастер-кошелек платформы') ?>:</p>

        <div class="panel panel-default">
            <div class="panel-body text-center">
                <?php
                $fc = $CurrencyIoModification->function_check;
                $is_func = false;
                if (!\cs\Application::isEmpty($fc)) {
                    $o = new $fc();
                    if (method_exists($o, 'qrcode')) {
                        $is_func = true;
                    }
                }
                ?>
                <?php if ($is_func) { ?>
                    <?= $o->qrcode($CurrencyIoModification); ?>
                <?php } else { ?>
                    <b style="color: red; font-size: 26px;" class="buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= $CurrencyIoModification->master_wallet ?>" title="<?= \Yii::t('c.TTaRzGgiYr', 'Нажмите чтобы скопировать') ?>"><?= $CurrencyIoModification->master_wallet ?></b>


                    <p>
                        <?php
                        $wallet_address = $CurrencyIoModification->master_wallet;

                        $qr = new \Endroid\QrCode\QrCode();
                        $qr->setText($wallet_address);
                        $qr->setSize(180);
                        $qr->setMargin(20);
                        $qr->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
                        $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
                        $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
                        $qr->setLabelFontSize(16);
                        $content = $qr->writeString();
                        $src = 'data:image/png;base64,' . base64_encode($content);
                        ?>
                        <img src="<?= $src ?>">
                    </p>
                <?php } ?>

            </div>
        </div>

        <p><?= \Yii::t('c.TTaRzGgiYr', 'Внимание! Лимит') ?></p>

        <p class="alert alert-danger">
            <i class="glyphicon glyphicon-alert"></i> <?= \Yii::t('c.TTaRzGgiYr', 'Внимание-1') ?><br> <br>
            <i class="glyphicon glyphicon-alert"></i> <?= \Yii::t('c.TTaRzGgiYr', 'Внимание-2') ?></p>

        <p><?= \Yii::t('c.TTaRzGgiYr', 'При <b>вводе</b> средств: адрес кошелька подставляется автоматически, и до завершения операции ввода <b>не может быть изменен</b>') ?>.</p>
        <p><?= \Yii::t('c.TTaRzGgiYr', 'При <b>выводе</b> средств: адрес кошелька подставляется автоматически, но по вашему желанию, в любое время <b>может быть изменен</b>') ?>.</p>

    <?php } ?>
    <?php if ($task->type_id == \common\models\TaskInput::TYPE_TRANSACTION) { ?>
        <?php if (\cs\Application::isEmpty($task->txid)) { ?>
            <p>Переведите средства на этот кошелек:</p>
            <div class="panel panel-default">
                <div class="panel-body text-center">
                    <?php
                    $fc = $CurrencyIoModification->function_check;
                    $is_func = false;
                    if (!\cs\Application::isEmpty($fc)) {
                        $o = new $fc();
                        if (method_exists($o, 'qrcode')) {
                            $is_func = true;
                        }
                    }
                    ?>
                    <?php if ($is_func) { ?>
                        <?= $o->qrcode($CurrencyIoModification); ?>
                    <?php } else { ?>
                        <b style="color: red; font-size: 26px;" class="buttonCopy" data-toggle="tooltip" data-clipboard-text="<?= $CurrencyIoModification->master_wallet ?>" title="<?= \Yii::t('c.TTaRzGgiYr', 'Нажмите чтобы скопировать') ?>"><?= $CurrencyIoModification->master_wallet ?></b>


                        <p>
                            <?php
                            $wallet_address = $CurrencyIoModification->master_wallet;

                            $qr = new \Endroid\QrCode\QrCode();
                            $qr->setText($wallet_address);
                            $qr->setSize(180);
                            $qr->setMargin(20);
                            $qr->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
                            $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
                            $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
                            $qr->setLabelFontSize(16);
                            $content = $qr->writeString();
                            $src = 'data:image/png;base64,' . base64_encode($content);
                            ?>
                            <img src="<?= $src ?>">
                        </p>
                    <?php } ?>

                </div>
            </div>
            <?php if ($CurrencyIoModification->master_wallet_is_binance == 1) { ?>
                <p>Если вы переводите крипто-монеты напрямую с биржи Binance (Бинанс), вам необходимо вставить скопированный на Binance TxID (хэш транзакции) в соответствующее поле ввода, ОБЯЗАТЕЛЬНО вводя перед ним слова "Internal transfer", - пример: “Internal transfer 54410018974”. Или просто на Binance копируете строку “Примечание” в информации к транзакции. Путь в ЛК Binance: Кошелек->Обзор кошелька->История транзакций->Ввод и вывод средств->Вывод. Подробная инструкция по переводам с биржи Бинанс <a href="/page/item?id=87">ЗДЕСЬ</a></p>
            <?php } ?>

            <?php
            $model2 = new \avatar\models\forms\ActiveInputTransactionStep2();
            $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model2,
                'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function(e) {
        window.location.reload(); 
    }).modal();
}
JS
                ,
                'formUrl' => '/cabinet-active/add3',

            ]); ?>
            <?= $form->field($model2, 'txid') ?>
            <?= Html::hiddenInput(Html::getInputName($model2, 'task_id'), $task->id) ?>
            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => Yii::t('c.TTaRzGgiYr', 'Обновить')]); ?>
        <?php } ?>
    <?php } ?>
<?php } ?>





