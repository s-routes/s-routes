<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = Yii::t('c.9GmqVLwFq6', 'Заявки на вывод');

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $str933 = Yii::t('c.9GmqVLwFq6', 'Скопировано');
        $this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str933}'
    }).show();

});

JS
        );
        $this->registerJS(<<<JS

$('.rowTable').click(function() {
    window.location = '/cabinet-active/out-item' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\TaskOutput::find()
                    ->where(['user_id' => Yii::$app->user->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => ['defaultOrder' => ['created_at' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'         => Yii::t('c.9GmqVLwFq6', 'Кошелек'),
                    'content'        => function ($item) {
                        return $item['account'];
                    },
                ],

                [
                    'header'         => 'txid',
                    'content'        => function ($item) {
                        $txid = \yii\helpers\ArrayHelper::getValue($item, 'txid', '');
                        if ($txid == '') return '';

                        $currency_io_id = $item['currency_io_id'];
                        $CIO = \common\models\CurrencyIO::findOne($currency_io_id);
                        // PRIZM
                        if (in_array($currency_io_id, [1])) {
                            return Html::a('http://blockchain.prizm.space/index.html', 'http://blockchain.prizm.space/index.html') . '<br>' . Html::tag('abbr', $txid, ['data' => ['toggle' => 'tooltip', 'clipboard-text' => $txid], 'title' => Yii::t('c.9GmqVLwFq6', 'Нажмите чтобы скопировать'), 'class' => 'buttonCopy']);
                        }
                        // ETH
                        if (in_array($currency_io_id, [2,3,4])) {
                            $d = substr($txid,0,6) . '...';
                            return Html::a($d, 'https://etherscan.io/tx/' . $txid);
                        }

                        return $txid;
                    },
                ],
                [
                    'header'         => Yii::t('c.9GmqVLwFq6', 'Монет'),
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'amount', 0);
                        $currency_id = \common\models\avatar\UserBill::findOne($item['billing_id'])->currency;
                        if (is_null($currency_id)) {
                            return '';
                        }
                        $CIO = \common\models\CurrencyIO::findOne(['currency_ext_id' => $currency_id]);
                        $c = \common\models\avatar\Currency::findOne($currency_id);
                        $cint = \common\models\piramida\Currency::findOne($CIO->currency_int_id);
                        $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);
                        $a = bcdiv($v, bcpow(10, $cint->decimals), $cint->decimals);

                        return Yii::$app->formatter->asDecimal($a, $cint->decimals_view) . $cHtml;
                    },
                ],
                [
                    'header'  => Yii::t('c.9GmqVLwFq6', 'Создано'),
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'         => Yii::t('c.9GmqVLwFq6', 'Комментарий'),
                    'content'        => function ($item) {
                        return $item['comment'];
                    },
                ],
                [
                    'header'  => Yii::t('c.9GmqVLwFq6', 'Статус'),
                    'content' => function ($item) {
                        switch ($item['status']) {
                            case \common\models\TaskPrizmOutput::STATUS_CREATED:
                                $html = Html::tag('span', Yii::t('c.9GmqVLwFq6', 'Создана'), ['class' => 'label label-info']);
                                break;
                            case \common\models\TaskPrizmOutput::STATUS_HIDE:
                                $html = Html::tag('span', 'Отменена', ['class' => 'label label-default']);
                                break;
                            case \common\models\TaskPrizmOutput::STATUS_DONE:
                                $html = Html::tag('span', Yii::t('c.9GmqVLwFq6', 'Выполнена'), ['class' => 'label label-success']);
                                break;
                            default:
                                $html = '';
                        }
                        return $html;
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>