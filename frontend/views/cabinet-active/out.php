<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this       \yii\web\View */
/* @var $form       \yii\bootstrap\ActiveForm */
/* @var $model      \avatar\models\forms\TaskOutput */
/* @var $CurrencyIO \common\models\CurrencyIO */

$this->title = Yii::t('c.TB9ZAPoWIy', 'Сделать заявку на вывод');


$modList = \common\models\CurrencyIoModification::find()->where(['currency_io_id' => $CurrencyIO->id])->all();
if (count($modList) > 0) {
    $modification_id = Yii::$app->request->get('modification_id', null);
    if (is_null($modification_id)) {
        $modification_id = $modList[0]['id'];
    }
    /** @var \common\models\CurrencyIoModification $modList */
    $mod2 = \common\models\CurrencyIoModification::findOne($modification_id);
    $attribute = $mod2->wallet_field_name;

    $output_min = $mod2->output_min;
    $comission_out = $mod2->comission_out;
    $function_class = $mod2->function_check;
} else {
    $attribute = $CurrencyIO->field_name_wallet;

    $output_min = $CurrencyIO->output_min;
    $comission_out = $CurrencyIO->comission_out;
    $function_class = $CurrencyIO->function_check;
}
/** @var \avatar\services\currency\CurrencyInterface $function_check */
$function_check = new $function_class();

$UserWallet = \common\models\UserWallet::findOne(['user_id' => Yii::$app->user->id]);
$isShow = false;
if (is_null($UserWallet)) {

} else {
    if (!\cs\Application::isEmpty($UserWallet->$attribute)) {
        $isShow = true;
    }
}

if ($isShow) {
    $model->account = $UserWallet->$attribute;
}
$currencyExt = \common\models\avatar\Currency::findOne($CurrencyIO->currency_ext_id);
$currencyInt = \common\models\piramida\Currency::findOne($CurrencyIO->currency_int_id);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header" style="font-size: 30px; font-weight: bold; text-transform: uppercase;"><?= Html::encode($this->title) ?></h1>



        <?php if (Yii::$app->session->hasFlash('form')) : ?>

            <p class="alert alert-success">
                <?= \Yii::t('c.TB9ZAPoWIy', 'Успешно добавлено.') ?>
            </p>
            <p>
                <a class="btn btn-info" href="/cabinet-active/out-index"><?= \Yii::t('c.TB9ZAPoWIy', 'Все заявки') ?></a>
            </p>

        <?php else: ?>

            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <?php if (count($modList) > 0) { ?>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist" style="margin-bottom: 20px;">
                            <?php /** @var \common\models\MasterTransaction $m */ ?>
                            <?php foreach ($modList as $m) { ?>
                                <?php
                                $class = '';
                                if ($m['id'] == $modification_id) {
                                    $class = ' class="active"';
                                }
                                ?>
                                <li <?= $class ?>><a href="<?= \yii\helpers\Url::to(['cabinet-active/out', 'id' => $CurrencyIO->id, 'modification_id' => $m['id']]) ?>"><?= $m['name'] ?></a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>

                    <?php $form = ActiveForm::begin([
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $form->field($model, 'account') ?>

                    <?php
                    $billingData = \common\models\avatar\UserBill::getInternalCurrencyWallet($CurrencyIO->currency_int_id);

                    /** @var \common\models\piramida\Wallet $wSB */
                    $wallet = $billingData['wallet'];
                    $currency = \common\models\piramida\Currency::findOne($wallet->currency_id);
                    $a = bcdiv($billingData['wallet']['amount'], bcpow(10, $currency->decimals), $currency->decimals);
                    $rows = [
                        $billingData['billing']['id'] => $wallet->getAddressShort() . ' (' . Yii::$app->formatter->asDecimal($a, $currency->decimals_view) . ' ' . $currency->code . ') ' ,
                    ];
                    $bid = $billingData['billing']['id'];
                    ?>

                    <?php if ($wallet->currency_id == \common\models\piramida\Currency::PZM) { ?>

                        <?= $form->field($model, 'prizm_key')->label(Yii::t('c.TB9ZAPoWIy', 'Публичный ключ')) ?>

                    <?php } ?>


                    <p class="alert alert-success"><?= \Yii::t('c.TB9ZAPoWIy', 'Счет списания') ?>: <?= $wallet->getAddressShort() . ' (' . Yii::$app->formatter->asDecimal($a, $currency->decimals_view) . ' ' . $currency->code . ') ' ?></p>
                    <?php
                    $min = bcdiv($output_min, bcpow(10, $currencyInt->decimals), $currencyInt->decimals);

                    if ($CurrencyIO->currency_int_id == \common\models\piramida\Currency::USDT) {
                        $usd = null;
                    } else {
                        $usd = $currencyExt->price_usd * $min;
                        $usd = Yii::$app->formatter->asDecimal($usd, 2);
                    }
                    if (!is_null($usd)) $usd = "(≈ $usd USDT)"
                    ?>

                    <?php if (!\cs\Application::isEmpty($output_min)) { ?>
                        <p><?= \Yii::t('c.TB9ZAPoWIy', 'Обратите внимание! Минимальное количество монет на вывод') ?> <?= $min ?> <?= $currencyInt->code ?> <?= $usd ?></p>
                    <?php } ?>


                    <?php
                    $this->registerJs(<<<JS
$('#taskoutput-billing_id').val({$bid});
JS
);
?>

                    <?php if (count($modList) > 0) { ?>
                        <?= Html::hiddenInput(Html::getInputName($model, 'modification_id'), $modification_id) ?>
                    <?php } ?>


                    <?= Html::activeHiddenInput($model, 'billing_id') ?>
                    <?= $form->field($model, 'amount') ?>
                    <p><?= \Yii::t('c.TB9ZAPoWIy', 'Комиссия нашей системы равна нулю. Комиссии блокчейнов монет могут быть как стабильными, так и меняться во времени.') ?></p>
                    <p><?= \Yii::t('c.TB9ZAPoWIy', 'Комиссия системы') ?>: <?= \Yii::t('c.TB9ZAPoWIy', 'нет комиссии') ?></p>


                    <?php
                    if (method_exists($function_check, 'out_algoritm')) {
                        $out_algoritm = $function_check->out_algoritm($currencyInt, $currencyExt, ['comission_out' => $comission_out]);
                    } else {
                        $out_algoritm = null;
                    }
                    ?>

                    <?php if (!is_null($out_algoritm)) { ?>
                        <?php if (ArrayHelper::getValue($out_algoritm, 'is_show_comission', false)) { ?>
                            <p><?= \Yii::t('c.TB9ZAPoWIy', 'Ориентир по комиссии блокчейна на текущий момент') ?>: <b><span id="comission_blockchain"></span></b></p>
                        <?php } ?>
                    <?php } ?>

                    <?php if (!is_null($out_algoritm)) { ?>
                        <?php $comission_options = ArrayHelper::getValue($out_algoritm, 'comission_options', null) ?>
                        <?php if (!\cs\Application::isEmpty($comission_options)) { ?>
                            <?= $form->field($model, 'comission_id')->radioList($comission_options) ?>
                        <?php } ?>
                    <?php } ?>


                    <?= Html::activeHiddenInput($model, 'comission_user'); ?>

                    <div class="form-group">
                        <p><b>Кол-во монет, которое я хочу получить (с учетом комиссии)</b></p>
                        <?= Html::input('text', 'amount2', '', ['class' => 'form-control', "id" => "sum_exit"]); ?>
                    </div>

                    <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('c.TB9ZAPoWIy', 'Вывести'), [
                            'class' => 'btn btn-success',
                            'name'  => 'contact-button',
                            'style' => 'width:100%; font-size: 20px; font-weight: bold; text-transform: uppercase;',
                        ]) ?>
                    </div>
                    <p class="alert alert-danger"><?= \Yii::t('c.TB9ZAPoWIy', 'Внимание! Заявки на вывод менее 100 USD (в эквиваленте), могут исполнятся медленнее.') ?></p>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php endif; ?>
    </div>
</div>
