<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\models\avatar\Currency;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\CabinetSilverStep3 */

$this->title = 'Серебро: заказ и доставка. Шаг 3';




?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet-silver/step4';
    
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'phone') ?>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Указать']); ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



