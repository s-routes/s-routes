<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $item \common\models\shop\Product */


$this->title = 'Товар #'.$item->id;


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= $item->name ?>
        </h1>
    </div>


    <div class="col-lg-8">
        <div class="col-lg-4">
            <p><img src="<?= $item->image ?>" width="100%"></p>
        </div>
        <div class="col-lg-8">
            <?php
            $currency = Currency::findOne(Currency::NEIRO);
            $price = $item->price / pow(10, $currency->decimals);
            ?>

            <p>
                Стоимость: <?= Yii::$app->formatter->asDecimal($price, $currency->decimals) ?> <span class="label label-info"><?= $currency->code ?></span>
            </p>
            <p>
                <?= nl2br($item->content) ?>
            </p>
            <?php if (\common\models\shop\ProductImage::find()->where(['product_id' => $item->id])->count() > 0) { ?>
                <?php
                \common\assets\iLightBox\Asset::register($this);
                \Yii::$app->view->registerJs(<<<JS
    $('.ilightbox').iLightBox();
JS
                );
                ?>
                <p>Картинки</p>
                <div class="row">

                    <?php foreach (\common\models\shop\ProductImage::find()->where(['product_id' => $item->id])->all() as $i) { ?>
                        <div class="col-lg-4">
                            <a href="<?= $i['image'] ?>" class="ilightbox">
                                <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($i['image'], 'crop') ?>" width="100%" class="thumbnail" style="margin-bottom: 0px;">
                            </a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <?php if (\common\models\shop\ProductVideo::find()->where(['product_id' => $item->id])->count() > 0) { ?>
                <p>Видео</p>
                <div class="row">

                    <?php foreach (\common\models\shop\ProductVideo::find()->where(['product_id' => $item->id])->all() as $i) { ?>
                        <div class="col-lg-2">
                            <a href="<?= $i['url'] ?>">
                                <img src="<?= \common\widgets\FileUpload7\FileUpload::getFile($i['image'], 'crop') ?>" width="100%">
                            </a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>

            <hr>
            <?php

            $this->registerJs(<<<JS
$('.buttonAddToCart').click(function(e) {
    ajaxJson({
        url: '/shop/cart-add',
        data: {id: $(this).data('id')},
        success: function(ret) {
            $('.js-basketCounter').html(ret.counter);
            new Noty({
                timeout: 3000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Товар успешно добавлен в корзину.'
            }).show();
        }
    })
});
JS
    );
            ?>
            <p><button class="btn btn-success buttonAddToCart" style="width: 100%" data-id="<?= $item->id ?>">Заказать</button></p>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>


