<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\models\avatar\Currency;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Серебро: заказ и доставка';



$this->registerJs(<<<JS
$('.buttonAddToCart').click(function(e) {
    ajaxJson({
        url: '/shop/cart-add',
        data: {id: $(this).data('id')},
        success: function(ret) {
            $('.js-basketCounter').html(ret.counter);
            new Noty({
                timeout: 3000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Товар успешно добавлен в корзину.'
            }).show();
        }
    })
});
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <?php /** @var \common\models\shop\Product $product */  ?>
        <?php foreach (\common\models\shop\Product::find()->all() as $product) {  ?>
            <div class="col-lg-3">
                <p>
                    <a href="<?= Url::to(['item','id' => $product->id]) ?>">
                        <img class="thumbnail" src="<?= \common\widgets\FileUpload7\FileUpload::getFile($product->image, 'crop') ?>" width="100%">
                    </a>
                </p>
                <p class="text-center"><?= $product->name ?></p>
                <p class="text-center">
                    <?php
                    $cInt = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);
                    $price = \common\models\piramida\Currency::getValueFromAtom($product->price, $cInt);
                    ?>

                    <?= Yii::$app->formatter->asDecimal($price, $cInt->decimals) ?> <span class="label label-info">NEIRO</span>
                </p>
                <p><button class="btn btn-success buttonAddToCart" data-id="<?= $product->id ?>" style="width: 100%">Заказать</button></p>
            </div>
        <?php } ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



