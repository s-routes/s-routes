<?php

use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\piramida\Wallet;
use common\models\shop\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use common\models\avatar\Currency;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\CabinetSilverStep4 */

$this->title = 'Серебро: заказ и доставка. Шаг 4';

$request1 = \Yii::$app->session->get('shop-request2');
$c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::NEIRO);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <p>Нужно оплатить стоимость заказанных монет = <?= Yii::$app->formatter->asDecimal(
                \common\models\piramida\Currency::getValueFromAtom(\common\models\shop\Basket::getPrice(), \common\models\piramida\Currency::NEIRO),
                $c->decimals) ?> <span class="label label-info"><?= $c->code ?></span></p>

        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = '/cabinet-shop-requests/view?id=' + ret.id;
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'count', ['inputOptions' => ['disabled' => 'disabled']]) ?>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Оплатить']); ?>


    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



