<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $id int */

if ($id != 1) {
    throw new \cs\web\Exception('Не найдено');
}
$this->title = 'О том как восстановить доступ к кошельку через ключи третьего уровня доступа';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/NSYUHGqq1UI" frameborder="0" allowfullscreen></iframe>
        </p>
    </div>
</div>



