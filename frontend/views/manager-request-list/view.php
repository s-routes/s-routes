<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $request \common\models\ShopRequest */

$this->title = 'Заказ №' . $request->id;


if (is_null($request->chat_room_id)) {
    $chatRoom = \common\models\ChatRoom::add([
        'last_message' => null,
    ]);
    $request->chat_room_id = $chatRoom->id;
    $request->save();
}

$room_id = $request->chat_room_id;

$user_id = Yii::$app->user->id;

\avatar\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);

$serverName = \avatar\assets\SocketIO\Asset::getHost();

$isShowCharMessage = true;


?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php
$c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::RUB);
$user = \common\models\UserAvatar::findOne($request->user_id);
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model' => $request,
            'attributes' => [
                [
                    'attribute'      => 'id',
                    'captionOptions' => ['style' => 'width: 30%'],
                ],
                'created_at:datetime:Создан',
                [
                    'label'      => 'Сумма',
                    'attribute'   => 'price',
                    'format'      => ['decimal', $c->decimals_view_shop],
                    'value'       => $request->price / pow(10, $c->decimals),
                ],
                [
                    'label'      => 'Валюта',
                    'attribute'   => 'currency_id',
                    'format'      => 'html',
                    'value'       => Html::tag('span', $c->code, ['class' => 'label label-info']),
                ],
                [
                    'label'      => 'Пользователь',
                    'attribute'   => 'user_id',
                    'format'      => 'html',
                    'value'       => Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $user->getName2() . ' (' . $user->getAddress() . ')']) . $user->getName2() . ' (' . $user->getAddress() . ')',
                ],
                'delivery:text:Адрес доставки',
                'delivery_dop:text:Дополнительная информация и пожелания по доставке',
                'user_name:text:Имя',
                'user_phone:text:Телефон',
                'user_email:text:Email',
                'user_telegram:text:Telegram',
                'user_whatsapp:text:Whatsapp',
            ]
        ]) ?>

        <h2 class="page-header">Список товаров</h2>
        <?php $c = 0; ?>
        <?php $sum = 0; ?>
        <?php foreach (\common\models\ShopRequestProduct::find()->where(['request_id' => $request->id])->all() as $p) { ?>
            <div class="row rowProduct" data-count="<?= $c ?>" style="margin-bottom: 20px;">
                <div class="col-lg-8"><?= $p['link'] ?></div>
                <div class="col-lg-2"><?= $p['price'] ?> RUB</div>
                <div class="col-lg-2"><?= $p['count'] ?> шт</div>
            </div>
            <?php $sum += $p['price'] * $p['count']; ?>
            <?php $c++; ?>
        <?php } ?>
        <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
            <div class="col-lg-8 text-right" style="font-weight: bold">Итого:</div>
            <div class="col-lg-4" style="font-weight: bold"><span class="js-product-list-sum"><?= Yii::$app->formatter->asDecimal($sum, 2) ?></span> RUB</div>
        </div>

        <div id="chat">

            <?php

            $myid = Yii::$app->user->id;
            $this->registerJs(<<<JS

let myid =  {$myid};

var socket = io.connect('{$serverName}');

function chatScroll() {
    var chat = $(".panel-body");
    chat.scrollTop(chat.prop("scrollHeight"));
}

chatScroll();

var name;
const messageInput = document.getElementById('bt-input');
const buttonSend = document.getElementById('btn-chat');

var roomName = 'room' + {$room_id};

socket.emit('chat1-new-user', roomName, myid);

var message_text;
var file2;

function getFile()
{
    var s = $('.fileUploadedUrl').html();
    if (typeof s == 'undefined') {
        return '';
    } else {
        if (s.length == 0) return '';
        return $('.fileUploadedUrl').html();
    }
}

$("#btn-input").keyup(function(event) {
    message_text = $('#btn-input').val();
    
    if (event.keyCode == 13 && (message_text.length > 0 || getFile() != '')) {
        appendMessage(message_text);
    }
});

$('#btn-chat').click(function(e) {
    e.preventDefault();

    message_text = $('#btn-input').val();
    if (message_text.length > 0 || getFile() != '') {
        appendMessage(message_text);
    }    
});


socket.on('chat-message2', (user_sender, data) => {
    console.log(['chat-message2', user_sender, data]);
    appendMessageGet(data);
});

/**
* Выводит сообщение пришедшее от NODEJS
* @param data
*/
function appendMessageGet(data) 
{
    $('.chat').append(data.message.html);

    chatScroll(); 
}

function appendMessage(text)
{
    ajaxJson({
        url: '/manager-request-list/send',
        data: {
            message: text,
            room_id: {$room_id},
            file: getFile()
        },
        success: function(ret) {
            // отправляю сообщение на сервер
            socket.emit('chat-message2', roomName, {$user_id}, ret);
            
            $('.chat').append(ret.message.html);
            
            // обнуляю текстовое поле
            $('#btn-input').val('');
            $('.fileUploadedUrl').html('');
            
            chatScroll();
        }
    });
};
JS
            );
            ?>

            <style>
                .chat
                {
                    list-style: none;
                    margin: 0;
                    padding: 0;
                }

                .chat li
                {
                    margin-bottom: 10px;
                    padding-bottom: 5px;
                    border-bottom: 1px dotted #B3A9A9;
                }

                .chat li.left .chat-body
                {
                    margin-left: 60px;
                }

                .chat li.right .chat-body
                {
                    margin-right: 60px;
                }


                .chat li .chat-body p
                {
                    margin: 0;
                    color: #777777;
                }

                .panel .slidedown .glyphicon, .chat .glyphicon
                {
                    margin-right: 5px;
                }

                .panel-body
                {
                    overflow-y: scroll;
                    height: 600px;
                }

                ::-webkit-scrollbar-track
                {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                    background-color: #F5F5F5;
                }

                ::-webkit-scrollbar
                {
                    width: 30px;
                    background-color: #F5F5F5;
                }

                ::-webkit-scrollbar-thumb
                {
                    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                    background-color: #555;
                }

                .panel-primary > .panel-heading {
                    background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
                }

                .panel-primary {
                    border-color: #750f0b !important;
                }

                .btn-warning {
                    background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
                }

                .btn-warning:hover, .btn-warning:focus {
                    background-color: #5cb85c !important;
                    background-position: 0 -15px;
                }

                .btn-warning:hover {
                    color: #fff;
                    background-color: #5cb85c !important;
                    border-color: #5cb85c !important;
                }

            </style>


            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> <?= \Yii::t('c.zpiXUyz0nd', 'Чат') ?>

                </div>
                <div class="panel-body">

                    <ul class="chat">
                        <?php
                        $rows = \common\models\ChatMessage2::find()
                            ->where(['room_id' => $room_id])
                            ->andWhere(['>', 'created_at', time() - 60*60*24*14])
                            ->all();
                        ?>
                        <?php /** @var \common\models\exchange\ChatMessage $message */ ?>
                        <?php foreach ($rows as $message) { ?>
                            <?= $this->render('../cabinet-exchange/message', ['message' => $message]); ?>

                        <?php } ?>
                    </ul>
                </div>

                <?php if ($isShowCharMessage) { ?>
                    <div class="panel-footer js-chat-panel-footer">
                        <div class="input-group">
                            <input id="btn-input" type="text" class="form-control input-sm" placeholder="<?= \Yii::t('c.zpiXUyz0nd', 'Напиши свое сообщение здесь...') ?>" />
                            <span class="input-group-btn">
                <button class="btn btn-warning btn-sm" id="btn-chat">
                    <?= \Yii::t('c.zpiXUyz0nd', 'Отправить') ?></button>
            </span>
                        </div>
                        <p><?= \Yii::t('c.zpiXUyz0nd', 'Если необходимо') ?>:</p>
                        <div style="margin-top: 10px;">
                            <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                                'id'        => 'upload' . 1,
                                'name'      => 'upload' . 1,
                                'attribute' => 'upload' . 1,
                                'model'     => new \avatar\models\forms\ChatFile(),
                                'update'    => [],
                                'settings'  => [
                                    'maxSize'           => 5 * 1000,
                                    'controller'        => 'upload4',
                                    'accept'            => 'image/*',
                                    'button_label'      => Yii::t('c.zpiXUyz0nd', 'Прикрепите файл'),
                                    'allowedExtensions' => ['jpg', 'jpeg', 'png'],
                                    'functionSuccess'   => new \yii\web\JsExpression(<<<JS
function (response) {
    // response.url
}
JS
                                    ),
                                ],
                            ]); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
