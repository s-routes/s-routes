<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все заказы';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/manager-request-list/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\ShopRequest::find()
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                'id',
                'delivery:text:Адрес доставки',
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'user_id', '');
                        if ($i == '') return '';
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                            'title' => $user->getName2(),
                            'data' => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header'         => 'Стоимость заказа',
                    'contentOptions' => ['class' => 'text-right'],
                    'headerOptions'  => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $currency_id = $item['currency_id'];
                        $cInt = \common\models\piramida\Currency::findOne($currency_id);
                        if (is_null($cInt)) return '';
                        $v = bcdiv($item['price'], bcpow(10, $cInt->decimals), $cInt->decimals_view_shop);

                        return $v;
                    },
                ],

                [
                    'header'  => '',
                    'content' => function ($item) {
                        $currency_id = $item['currency_id'];
                        $c = \common\models\piramida\Currency::findOne($currency_id);
                        if (is_null($c)) return '';

                        return Html::tag('span', $c->code, ['class' => 'label label-info']);
                    },
                ],
                [
                    'header'  => 'Оплачен?',
                    'content' => function ($item) {
                        if (!is_null($item['is_paid'])) {
                            if ($item['is_paid'] == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        }

                        return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    },
                ],
                [
                    'header'  => 'RUB',
                    'content' => function ($item) {
                        $currency_id = \common\models\piramida\Currency::RUB;
                        $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($currency_id, $item['user_id']);
                        /** @var \common\models\avatar\UserBill $bill */
                        $bill = $data['billing'];
                        $c = \common\models\piramida\Currency::findOne($currency_id);

                        return Html::tag('span', Html::a($c->code,['cabinet-wallet/item', 'id' => $bill->id], ['data-pjax' => 0]), ['class' => 'label label-info']);
                    },
                ],

                [
                    'header'  => 'MARKET',
                    'content' => function ($item) {
                        $currency_id = \common\models\piramida\Currency::MARKET;
                        $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($currency_id, $item['user_id']);
                        /** @var \common\models\avatar\UserBill $bill */
                        $bill = $data['billing'];
                        $c = \common\models\piramida\Currency::findOne($currency_id);

                        return Html::tag('span', Html::a($c->code,['cabinet-wallet/item', 'id' => $bill->id], ['data-pjax' => 0]), ['class' => 'label label-info']);
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>