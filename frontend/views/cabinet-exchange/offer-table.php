<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $ActiveDataProvider \yii\data\ActiveDataProvider */

$str819 = Yii::t('c.jPZ5zCpMJX', 'Подтвердите удаление');
$str820 = Yii::t('c.jPZ5zCpMJX', 'Нельзя удалить предложение со сделками');
$str821 = Yii::t('c.jPZ5zCpMJX', 'Подтвердите закрытие');

$this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.buttonBuy').click(function() {
    window.location = '/cabinet-exchange/deal-open' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('{$str819}')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-exchange/offer-delete',
            data: {
                id: $(this).data('id')  
            },
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            },
            errorScript: function(ret) {
                if (ret.id == 103) {
                    alert('{$str820}');
                }
            }
        });
    }
});

$('.buttonClose').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('{$str821}')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-exchange/offer-close',
            data: {
                id: $(this).data('id')  
            },
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});

$('.buttonRedo').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-exchange/offer-add' + '?' + 'id' + '=' + $(this).data('id');
});

JS
);


?>

<?= \yii\grid\GridView::widget([
    'dataProvider' => $ActiveDataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-hover table_exchange',
    ],
    'summary'      => '',
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        [
            'header'        => 'Id',
            'contentOptions' => ['aria-label' => 'Id'],
            'content'       => function ($item) {
                return $item['id'];
            },
        ],
        [
            'header'        => Yii::t('c.jPZ5zCpMJX', 'Тип предложения'),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Тип предложения')],
            'content'       => function ($item) {
                if ($item['type_id'] == \common\models\exchange\Offer::TYPE_ID_BUY) {
                    return Yii::t('c.jPZ5zCpMJX', 'Я покупаю');
                }
                if ($item['type_id'] == \common\models\exchange\Offer::TYPE_ID_SELL) {
                    return Yii::t('c.jPZ5zCpMJX', 'Я продаю');
                }
                return '';
            },
        ],

        [
            'label'          => Yii::t('c.jPZ5zCpMJX', 'Монету'),
            'filter'         => \yii\helpers\ArrayHelper::map(
                \common\models\CurrencyIO::find()->all(),
                'id',
                'tab_title'
            ),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Монету')],
            'content'        => function ($item,$p2,$p3,$p4) {
                return $p4->filter[$item['currency_io']];
            },
        ],
        [
            'header'        => Yii::t('c.jPZ5zCpMJX', 'в объеме'),
            'headerOptions'  => ['class' => 'text-right'],
            'contentOptions' => [
                'class' => 'text-nowrap text-right',
                'aria-label' => Yii::t('c.jPZ5zCpMJX', 'в объеме'),
            ],
            'content'       => function ($item) {
                $IO = \common\models\CurrencyIO::findOne($item['currency_io']);
                $c = \common\models\piramida\Currency::findOne($IO->currency_int_id);
                $data[0] = bcdiv($item['volume_io_start'], pow(10, $c->decimals), $c->decimals);
                $data[1] = bcdiv($item['volume_io_finish'], pow(10, $c->decimals), $c->decimals);
                $from = Yii::$app->formatter->asDecimal($data[0], $c->decimals_view);
                $to = Yii::$app->formatter->asDecimal($data[1], $c->decimals_view);
                $print = Html::tag('span',Yii::t('c.jPZ5zCpMJX', 'от').' ', ['style' => 'color: #ccc;']) . $from . '<br>' . Html::tag('span',' '.Yii::t('c.jPZ5zCpMJX', 'до').' ',['style' => 'color: #ccc;']) . $to;

                return $print;
            },
        ],
        [
            'header'         => Yii::t('c.jPZ5zCpMJX', 'По цене'),
            'headerOptions'  => ['class' => 'text-right'],
            'contentOptions' => [
                'class'      => 'text-nowrap text-right',
                'aria-label' => Yii::t('c.jPZ5zCpMJX', 'По цене'),
            ],
            'content'       => function ($item) {
                $OfferPay = \common\models\exchange\OfferPay::findOne($item['offer_pay_id']);
                $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $OfferPay->currency_id]);
                $currency = \common\models\piramida\Currency::findOne($link->currency_int_id);
                $price = $item['price'];
                $pricePrint = bcdiv($price, bcpow(10, $currency->decimals), $currency->decimals);
                $decimalsPrint = $currency->decimals_view;
                $c = Html::tag('span', $OfferPay->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint) . $c;
            },
        ],
        [
            'header'        => Yii::t('c.jPZ5zCpMJX', 'Условия сделки и платежные системы'),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Условия сделки и платежные системы')],
            'content'       => function ($item) {
                $list = \common\models\exchange\OfferPayLink::find()->where(['offer_id' => $item['id']])->select('pay_id')->column();

                $listName1 = \common\models\PayMethodLang::find()
                    ->select(['pay_method_lang.name', 'pay_method.id'])
                    ->innerJoin('pay_method', 'pay_method.id = pay_method_lang.parent_id')
                    ->where(['in', 'pay_method_lang.parent_id', $list])
                    ->andWhere(['language' => Yii::$app->language])
                    ->orderBy(['pay_method.sort_index' => SORT_ASC])
                    ->all();

                $list2 = \yii\helpers\ArrayHelper::getColumn($listName1, 'id');
                $list3 = [];
                // Ищу что еще не переведено
                foreach ($list as $id) {
                    if (!in_array($id, $list2)) {
                        $list3[] = $id;
                    }
                }

                $listName2 = \common\models\exchange\PayMethod::find()->where(['in', 'id', $list3])->select('name')->orderBy(['sort_index' => SORT_ASC])->column();

                $listName = \yii\helpers\ArrayHelper::merge(
                    \yii\helpers\ArrayHelper::getColumn($listName1, 'name'),
                    $listName2
                );

                return join(', ', $listName);
            },
        ],
        [
            'header'  => Yii::t('c.jPZ5zCpMJX', 'Создано'),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Создано')],
            'content' => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            }
        ],
        [
            'header'        => Yii::t('c.jPZ5zCpMJX', 'Сделок'),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Сделок')],
            'content'       => function ($item) {
                $count = Deal::find()
                    ->where(['offer_id' => $item['id']])
//                    ->andWhere(['not', ['status' => Deal::STATUS_CREATE]])
                    ->count();

                return $count;
            },
        ],
        [
            'header'         => Yii::t('c.jPZ5zCpMJX', 'Редактировать'),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Редактировать')],
            'content'        => function ($item) {
                if ($item['is_hide']) {
                    return  '';
                } else {
                    if (Deal::find()->where(['offer_id' => $item['id']])->count() == 0) {
                        return Html::a(Yii::t('c.jPZ5zCpMJX', 'Редактировать'), ['cabinet-exchange/offer-edit', 'id' => $item['id']], [
                            'class' => 'btn btn-primary btn-xs',
                            'data'  => [
                                'id'     => $item['id'],
                            ],
                        ]);
                    } else {
                        return  '';
                    }
                }
            },
        ],
        [
            'header'         => Yii::t('c.jPZ5zCpMJX', 'Закрыть'),
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Закрыть')],
            'content'        => function ($item) {
                if ($item['is_hide']) {
                    return Html::button('Перевыставить', [
                        'class' => 'btn btn-default btn-xs buttonRedo',
                        'title' => 'Создать новую копию этого предложения с теми же параметрами',
                        'data'  => [
                            'id'     => $item['id'],
                            'toggle' => 'tooltip',
                        ],
                    ]);
                } else {
                    return Html::button(Yii::t('c.jPZ5zCpMJX', 'Закрыть'), [
                        'class' => 'btn btn-warning btn-xs buttonClose',
                        'title' => Yii::t('c.jPZ5zCpMJX', 'Скрыть предложение из общего списка'),
                        'data'  => [
                            'id'     => $item['id'],
                            'toggle' => 'tooltip',
                        ],
                    ]);
                }
            },
        ],

        [
            'header'         => '<i class="glyphicon glyphicon-trash" aria-hidden="true" title="Скрыть предложение из общего списка и у меня" data-toggle="tooltip"></i>',
            'contentOptions' => ['aria-label' => Yii::t('c.jPZ5zCpMJX', 'Удалить')],
            'content'        => function ($item) {
                return Html::button('<i class="glyphicon glyphicon-trash" aria-hidden="true"></i>', [
                    'class' => 'btn btn-danger btn-xs buttonDelete',
                    'title' => Yii::t('c.jPZ5zCpMJX', 'Скрыть предложение из общего списка и у меня'),
                    'data'  => [
                        'toggle' => 'tooltip',
                        'id'     => $item['id'],
                    ],
                ]);
            },
        ],
    ],
]) ?>