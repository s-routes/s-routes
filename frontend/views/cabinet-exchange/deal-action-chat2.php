<?php


/** @var \yii\web\View $this */
/** @var int $room_id Идентификатор комнаты */

?>

<?php

$myid = Yii::$app->user->id;

$this->registerJs(<<<JS
let myid =  {$myid};

function chatScroll() {
    var chat = $(".panel-body");
    chat.scrollTop(chat.prop("scrollHeight"));
}

chatScroll();

var name;
const messageInput = document.getElementById('bt-input');
const buttonSend = document.getElementById('btn-chat');
let roomName = 'room' + {$room_id};

console.log("roomName: " + roomName);

socket.emit('new-user', roomName, myid);

var message_text;
var file2;

function getFile()
{
    var s = $('.fileUploadedUrl');
    if (s.length == 0) return '';
    return $('.fileUploadedUrl').html();
}

$("#btn-input").keyup(function(event) {
    
    message_text = $('#btn-input').val();
    if (message_text.length > 0) {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = message_text + ' ' + file2;
        }
    } else {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = file2;
        }
    }
    
    if (event.keyCode == 13 && message_text.length > 0) {
        appendMessage(message_text);
    }
});

buttonSend.addEventListener('click', e => {
    
    message_text = $('#btn-input').val();
    if (message_text.length > 0) {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = message_text + ' ' + file2;
        }
    } else {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = file2;
        }
    }
    
    if (message_text.length > 0) {
        e.preventDefault();
        appendMessage(message_text);
    }           
});

socket.on('chat-message', data => {
    console.log(data.name + " // " + data.image + " // " + data.message);
    appendMessageGet(data.name, data.image, data.message);
});

socket.on('user-connected', name => {
    console.log("user-connected: " + name +' вошел')
});

socket.on('user-disconnected', name => {
    console.log("user-disconnected: " + name +' вышел')
});

function appendMessageGet(name, image, message) 
{
    var Data = new Date();
    var Hour = Data.getHours();
    if (Hour < 10) Hour = '0' + Hour;
    var Minutes = Data.getMinutes();
    if (Minutes < 10) Minutes = '0' + Minutes;
    var Seconds = Data.getSeconds();
    if (Seconds < 10) Seconds = '0' + Seconds;
    var time2 = Hour + ":" + Minutes + ":" + Seconds;
    
    var liObject = $('<li>', {class:'left clearfix'});
    var spanObject = $('<span>', {class:'chat-img pull-left'});
    var imgObject = $('<img>', {src: image, class: 'img-circle', width: 50});
    spanObject.append(imgObject);
    liObject.append(spanObject);
    var div1Object = $('<div>', {class: 'chat-body clearfix'});
    var div2Object = $('<div>', {class: 'header2'});
    var strongObject = $('<strong>', {class: 'primary-font'}).html(name);
    var smallObject = $('<small>', {class: 'pull-right text-muted'});
    var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
    div2Object.append(strongObject);
    smallObject.append(spanTimeObject);
    smallObject.append(time2);
    strongObject.append(smallObject);
    var pObject = $('<p>').html(message);
    div1Object.append(div2Object);
    div1Object.append(pObject);
    liObject.append(div1Object);
    
    $('.chat').append(liObject);
    
    chatScroll(); 
}

function appendMessage(text) 
{
    if (text.length > 0) {
        ajaxJson({
            url: '/cabinet-chat-tet/send',
            data: {
                id: {$room_id},
                message: text
            },
            success: function(ret) {
                
                socket.emit('chat-message2', roomName, {$myid}, ret);
                
                var user = ret.message.user;
                var liObject = $('<li>', {class:'left clearfix', "data-id": ret.message.id});
                var spanObject = $('<span>', {class:'chat-img pull-left'});
                var imgObject = $('<img>', {src: user.avatar, class: 'img-circle', width: 50});
                spanObject.append(imgObject);
                liObject.append(spanObject);
                var div1Object = $('<div>', {class: 'chat-body clearfix'});
                var div2Object = $('<div>', {class: 'header2'});

                var strongObject = $('<strong>', {class: 'primary-font'}).html(user.name2);
                var smallObject = $('<small>', {class: 'pull-right text-muted'});
                var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
                div2Object.append(strongObject);
                smallObject.append(spanTimeObject);
                smallObject.append(ret.message.timeFormatted);
                strongObject.append(smallObject);
                var pObject = $('<p>').html(ret.message.message);
                div1Object.append(div2Object);
                div1Object.append(pObject);
                liObject.append(div1Object);
                $('.chat').append(liObject);
                
                // обнуляю текстовое поле
                $('#btn-input').val('');
                $('.fileUploadedUrl').html('');
                
                chatScroll();
            }
        });
    }
}


JS
);
?>

<style>
    .chat
    {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    .chat li
    {
        margin-bottom: 10px;
        padding-bottom: 5px;
        border-bottom: 1px dotted #B3A9A9;
    }

    .chat li.left .chat-body
    {
        margin-left: 60px;
    }

    .chat li.right .chat-body
    {
        margin-right: 60px;
    }


    .chat li .chat-body p
    {
        margin: 0;
        color: #777777;
    }

    .panel .slidedown .glyphicon, .chat .glyphicon
    {
        margin-right: 5px;
    }

    .panel-body
    {
        overflow-y: scroll;
        height: 250px;
    }

    ::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar
    {
        width: 12px;
        background-color: #F5F5F5;
    }

    ::-webkit-scrollbar-thumb
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
        background-color: #555;
    }

    .panel-primary > .panel-heading {
        background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
    }

    .panel-primary {
        border-color: #750f0b !important;
    }

    .btn-warning {
        background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
    }

    .btn-warning:hover, .btn-warning:focus {
        background-color: #5cb85c !important;
        background-position: 0 -15px;
    }

    .btn-warning:hover {
        color: #fff;
        background-color: #5cb85c !important;
        border-color: #5cb85c !important;
    }

</style>

<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="glyphicon glyphicon-comment"></span> Чат
    </div>
    <div class="panel-body">
        <ul class="chat">
            <?php /** @var \common\models\ChatMessage2 $message */ ?>
            <?php foreach (\common\models\ChatMessage2::find()->where(['room_id' => $room_id])->all() as $message) { ?>
                <?= $this->render('@frontend/views/cabinet-exchange/message.php', ['message' => $message]) ?>
            <?php } ?>
        </ul>
    </div>

    <div class="panel-footer js-chat-panel-footer">
        <div class="input-group">
            <input id="btn-input" type="text" class="form-control input-sm" placeholder="Напиши свое сообщение здесь..." />
            <span class="input-group-btn">
                <button class="btn btn-warning btn-sm" id="btn-chat">
                    Отправить</button>
            </span>
        </div>
        <p>Если необходимо:</p>
        <div style="margin-top: 10px;">
            <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                'id'        => 'upload' . 1,
                'name'      => 'upload' . 1,
                'attribute' => 'upload' . 1,
                'model'     => new \avatar\models\forms\ChatFile(),
                'update'    => [],
                'settings'  => [
                    'maxSize'           => 5 * 1000,
                    'controller'        => 'upload4',
                    'accept'            => 'image/*',
                    'button_label'      => 'Прикрепите файл',
                    'allowedExtensions' => ['jpg', 'jpeg', 'png'],
                    'functionSuccess'   => new \yii\web\JsExpression(<<<JS
function (response) {
    // response.url
}
JS
                    ),
                ],
            ]); ?>
        </div>


    </div>
</div>
