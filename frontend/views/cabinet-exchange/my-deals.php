<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $shapeShiftAvailable array */
/* @var $avatarCurrency array  \common\models\avatar\Currency[] */
/* @var $ret array  пересечение */


$this->title = Yii::t('c.MHL9nEyCNY', 'Журнал сделок');


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
        <!-- Large modal -->

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <?php
                        $id = 17;
                        $page = \common\models\exchange\Page::findOne($id);

                        $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                        if (is_null($pageLang)) {
                            $name = $page->name;
                            $content = $page->content;
                        } else {
                            $name = $pageLang->name;
                            $content = $pageLang->content;
                        }
                        ?>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?= $name ?></h4>
                    </div>
                    <div class="modal-body">
                        <?= $content ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
        );
        ?>

    </div>

    <div class="row">
        <div class="col-lg-10">
            <?= $this->render('../cabinet/_menu3') ?>

        </div>
        <div class="col-lg-2">
            <a href="offer-add" class="btn c-offer-add" style="float: right; margin-right: 10px;"><?= \Yii::t('c.MHL9nEyCNY', 'Добавить предложение') ?></a>
        </div>
    </div>
    <br>
    <div class="col-lg-12">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?= $this->render('my-deals-table', ['ActiveDataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\exchange\Deal::find()
                ->innerJoin('offer', 'offer.id = deal.offer_id')
                ->select(['deal.*'])
                ->where([
                    'or',
                    ['deal.user_id' => Yii::$app->user->id],
                    ['offer.user_id' => Yii::$app->user->id],
                ])
            ,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
            'pagination' => [
                'pageSize' => 20,
            ],
        ])]) ?>
        <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>
