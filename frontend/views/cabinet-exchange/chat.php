<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $chat_id int */

$this->title = ($chat_id % 2 == 1)? Yii::t('c.oMqYe6h5yY', 'Чат болталка') : Yii::t('c.oMqYe6h5yY', 'Чат обменника');

$is_search = false;
$search_text = Yii::$app->request->get('text');
if (!\cs\Application::isEmpty($search_text)) {
    $is_search = true;
}

$user_id = Yii::$app->user->id;

\avatar\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);

$serverName = \avatar\assets\SocketIO\Asset::getHost();

$isShowCharMessage = true;

/** @var \common\models\UserBan $UserBan */
$UserBan = \common\models\UserBan::find()->where(['user_id' => $user_id, 'chat_id' => $chat_id])->one();
if (!is_null($UserBan)) {
    if ($UserBan->type == \common\models\UserBan::TYPE_BAN) {
        $isShowCharMessage = false;
    }
}


function drawMessage($text, $search)
{
    $start = 0;
    $rows = [];
    do  {
        $pos = \cs\services\Str::pos($search,$text, $start);
//        $pos = strpos($text, $search, $start);
        if ($pos === false) break;
        $rows[] = $pos;
        $start = $pos + \cs\services\Str::length($search);
    } while (true);
    $start = 0;
    $ret = '';
    for ($i = 0; $i < count($rows); $i++) {
        $ret .= \cs\services\Str::sub($text, $start, $rows[$i] - $start);
        $ret .= Html::tag('span', $search, ['style' => 'background-color: yellow']);
        if ($rows[$i] == 50)
        $start += $rows[$i] + \cs\services\Str::length($search);
    }
    if ($start < \cs\services\Str::length($text)) {
        $ret .= \cs\services\Str::sub($text, $start);
    }

    return $ret;
}
function drawMessage2($text, $search)
{
    $rows = \cs\services\Str::explode($search, $text);
    $ret = '';

    for ($i = 0; $i < count($rows); $i++) {
        $ret .= $rows[$i];
        if ($i != (count($rows) -1) ) {
            $ret .= Html::tag('span', $search, ['style' => 'background-color: yellow']);
        }
    }

    return $ret;
}
$str793 = Yii::t('c.oMqYe6h5yY', 'Успешно');
$str794 = Yii::t('c.oMqYe6h5yY', 'Уверены?');
$str795 = Yii::t('c.oMqYe6h5yY', 'Удалить');
$str796 = Yii::t('c.oMqYe6h5yY', 'Забанить');
$str797 = Yii::t('c.oMqYe6h5yY', 'Удалить пользователя');
?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>

            <!-- Large modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <?php
                            $id = ($chat_id % 2 == 1)? 19 : 20;
                            $page = \common\models\exchange\Page::findOne($id);

                            $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                            if (is_null($pageLang)) {
                                $name = $page->name;
                                $content = $page->content;
                            } else {
                                $name = $pageLang->name;
                                $content = $pageLang->content;
                            }
                            ?>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?= $name ?></h4>
                        </div>
                        <div class="modal-body">
                            <?= $content ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
            );
            ?>
        </h1>
    </div>

    <?= $this->render('../cabinet/_menu3') ?>


    <div class="row">
        <div class="col-lg-3" style="margin-top: 50px;">

            <form action="<?= ($chat_id % 2 == 1)? '/cabinet-exchange/chat' : '/cabinet-exchange/chat-exchange' ?>" method="get">
                <input class="form-control" name="text">
                <hr>
                <button type="submit" class="btn btn-success" style="width: 100%;"><?= \Yii::t('c.oMqYe6h5yY', 'Найти') ?></button>
            </form>
        </div>
        <div class="col-lg-6" style="margin-top: 50px;">

            <div id="chat">
                <?php if (Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin_chat')) { ?>
                    <?php
                    $this->registerJs(<<<JS
var functionDelete = function(e) {
    var id =  $(this).data('id');
    if (confirm('Уверены?')) {
        ajaxJson({
            url: '/cabinet-exchange-chat/delete',
            data: {
                id: id
            },
            success: function(ret) {
                // отправляю сообщение на сервер
                socket.emit('chat1-delete', roomName, id);
                
                // Вывожу уведомление
                new Noty({
                    timeout: 1000,
                    theme: 'relax',
                    type: 'success',
                    layout: 'bottomLeft',
                    text: '{$str793}'
                }).show();
                
                // Удаляю сообщение
                $('.js-chat-message[data-id=' + id + ']').remove();
            }
        });
    }
};
var functionBan = function(e) {
    if (confirm('')) {
        ajaxJson({
            url: '/cabinet-exchange-chat/ban',
            data: {
                id: $(this).data('id'),
                chat_id: {$chat_id}
            },
            success: function(ret) {
                // отправляю сообщение на сервер
                socket.emit('chat1-ban', roomName, ret.user_id);
                
                // Вывожу уведомление
                new Noty({
                    timeout: 1000,
                    theme: 'relax',
                    type: 'success',
                    layout: 'bottomLeft',
                    text: '{$str793}'
                }).show();
            }
        });
    }
};
var functionDeleteUser = function(e) {
    if (confirm('{$str794}')) {
        ajaxJson({
            url: '/cabinet-exchange-chat/delete-user',
            data: {
                id: $(this).data('id'),
                chat_id: {$chat_id}
            },
            success: function(ret) {
                // отправляю сообщение на сервер
                socket.emit('chat1-delete-user', roomName, ret.user_id);
                
                // Вывожу уведомление
                new Noty({
                    timeout: 1000,
                    theme: 'relax',
                    type: 'success',
                    layout: 'bottomLeft',
                    text: '{$str793}'
                }).show();
            }
        });
    }
};

$('.buttonDelete').click(functionDelete);
$('.buttonBan').click(functionBan);
$('.buttonDeleteUser').click(functionDeleteUser);

JS
                    );
                    ?>
                <?php } ?>
                <?php



                $myid = Yii::$app->user->id;
                $chat_id_mod = $chat_id % 2;
                $this->registerJs(<<<JS

let myid =  {$myid};

var socket = io.connect('{$serverName}');

function chatScroll() {
    var chat = $(".panel-body");
    chat.scrollTop(chat.prop("scrollHeight"));
}

chatScroll();

var name;
const messageInput = document.getElementById('bt-input');
const buttonSend = document.getElementById('btn-chat');

var roomName;
if ($chat_id_mod == 1) {
    roomName = 'exchange1';
} else {
    roomName = 'exchange2';
}

socket.emit('chat1-new-user', roomName, myid);

var message_text;
var file2;

function getFile()
{
    var s = $('.fileUploadedUrl');
    if (s.length == 0) return '';
    return $('.fileUploadedUrl').html();
}

$("#btn-input").keyup(function(event) {
    message_text = $('#btn-input').val();
    if (message_text.length > 0) {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = message_text + ' ' + file2;
        }
    } else {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = file2;
        }
    }
    
    if (event.keyCode == 13 && message_text.length > 0) {
        appendMessage(message_text);
    }
});

buttonSend.addEventListener('click', e => {
    e.preventDefault();

    message_text = $('#btn-input').val();
    if (message_text.length > 0) {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = message_text + ' ' + file2;
        }
    } else {
        file2 = getFile();
        if (file2.length > 0) {
            message_text = file2;
        }
    }
    
    if (message_text.length > 0) {
        appendMessage(message_text);
    }           
});

socket.on('chat1-message', (data) => {
    console.log(['chat1-message', data]);
    appendMessageGet(data);
});
socket.on('chat1-delete', (id) => {
    console.log(['chat1-delete', id]);
    $('.js-chat-message[data-id='+id+']').remove();
});
socket.on('chat1-ban', (id) => {
    console.log(['chat1-ban', id]);
    if (id == myid) {
        window.location.reload();
    }
});
socket.on('chat1-delete-user', (id) => {
    console.log(['chat1-delete-user', id]);
    if (id == myid) {
        window.location.reload();
    }
});

var functionDelete2 = function(e) {
    var id =  $(this).data('id');
    if (confirm('Уверены?')) {
        ajaxJson({
            url: '/cabinet-exchange-chat/delete2',
            data: {
                id: id
            },
            success: function(ret) {
                // отправляю сообщение на сервер
                socket.emit('chat1-delete', roomName, id);
                
                // Вывожу уведомление
                new Noty({
                    timeout: 1000,
                    theme: 'relax',
                    type: 'success',
                    layout: 'bottomLeft',
                    text: '{$str793}'
                }).show();
                console.log(id);
                
                // Удаляю сообщение
                $('.js-chat-message[data-id=' + id + ']').remove();
            }
        });
    }
};

/**
* Выводит сообщение пришедшее от NODEJS
* @param data
*/
function appendMessageGet(data) 
{
    var Data = new Date();
    var Hour = Data.getHours();
    if (Hour < 10) Hour = '0' + Hour;
    var Minutes = Data.getMinutes();
    if (Minutes < 10) Minutes = '0' + Minutes;
    var Seconds = Data.getSeconds();
    if (Seconds < 10) Seconds = '0' + Seconds;
    var time2 = Hour + ":" + Minutes + ":" + Seconds;
    
    var liObject = $('<li>', {class:'left clearfix js-chat-message', "data-id": data.message.id});
    var spanObject = $('<span>', {class:'chat-img pull-left'});
    var imgObject = $('<img>', {src: data.user.avatar, class: 'img-circle', width: 50});
    spanObject.append(imgObject);
    liObject.append(spanObject);
    var div1Object = $('<div>', {class: 'chat-body clearfix'});
    var div2Object = $('<div>', {class: 'header2'});
    var strongObject = $('<strong>', {class: 'primary-font'}).html(data.user.name2);
    var smallObject = $('<small>', {class: 'pull-right text-muted'});
    var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
    div2Object.append(strongObject);
    smallObject.append(spanTimeObject);
    smallObject.append(time2);
    strongObject.append(smallObject);
    var pObject;
    if (data.user.is_admin) {
        pObject = $('<p>', {style: 'color:red'}).html(data.message.message);    
    } else {
        pObject = $('<p>').html(data.message.message);
    }
    div1Object.append(div2Object);
    div1Object.append(pObject);
    liObject.append(div1Object);
    
    $('.chat').append(liObject);
    
    chatScroll(); 
}

function appendMessage(text)
{
    ajaxJson({
        url: '/cabinet-exchange-chat/send2',
        data: {
            message: text,
            chat_id: {$chat_id}
        },
        success: function(ret) {
            
            // отправляю сообщение на сервер
            socket.emit('chat1-message', roomName, {$user_id}, ret);
            
            var user = ret.user;
            var liObject = $('<li>', {class:'left clearfix js-chat-message', "data-id": ret.message.id});
            var spanObject = $('<span>', {class:'chat-img pull-left'});
            var imgObject = $('<img>', {src: user.avatar, class: 'img-circle', width: 50});
            spanObject.append(imgObject);
            liObject.append(spanObject);
            var div1Object = $('<div>', {class: 'chat-body clearfix'});
            var div2Object = $('<div>', {class: 'header2'});

            var strongObject = $('<strong>', {class: 'primary-font'}).html(user.name2);
            var smallObject = $('<small>', {class: 'pull-right text-muted'});
            var spanTimeObject = $('<span>', {class: 'glyphicon glyphicon-time'});
            div2Object.append(strongObject);
            smallObject.append(spanTimeObject);
            smallObject.append(ret.message.timeFormatted);
            strongObject.append(smallObject);
            var pObject;
            var pObject3;
            if (ret.user.is_admin) {
                pObject = $('<p>', {style: 'color:red'}).html(ret.message.message);
                
                pObject3 = $('<p>')
                .append(
                    $('<a>', {href:'javascript:void(0)',"data-id":ret.message.id}).html('{$str795}').click(functionDelete)
                )
                .append(
                    $('<a>', {href:'javascript:void(0)',"data-id":ret.message.id,style:"margin-left:5px;"}).html('{$str796}').click(functionBan)
                )
                .append(
                    $('<a>', {href:'javascript:void(0)',"data-id":ret.message.id,style:"margin-left:5px;"}).html('{$str797}').click(functionDeleteUser)
                )
                ;

            } else {
                pObject = $('<p>').html(ret.message.message);
            }
            var pObject2;
            pObject2 = $('<p>').append($('<a>', {href:'javascript:void(0)',"data-id":ret.message.id}).html('{$str795}').click(functionDelete2));
            div1Object.append(div2Object);
            div1Object.append(pObject);
            div1Object.append(pObject3);
            div1Object.append(pObject2);
            liObject.append(div1Object);
            $('.chat').append(liObject);
            
            // обнуляю текстовое поле
            $('#btn-input').val('');
            $('.fileUploadedUrl').html('');
            
            chatScroll();
        }
    });
};
JS
                );
                ?>

                <style>
                    .chat
                    {
                        list-style: none;
                        margin: 0;
                        padding: 0;
                    }

                    .chat li
                    {
                        margin-bottom: 10px;
                        padding-bottom: 5px;
                        border-bottom: 1px dotted #B3A9A9;
                    }

                    .chat li.left .chat-body
                    {
                        margin-left: 60px;
                    }

                    .chat li.right .chat-body
                    {
                        margin-right: 60px;
                    }


                    .chat li .chat-body p
                    {
                        margin: 0;
                        color: #777777;
                    }

                    .panel .slidedown .glyphicon, .chat .glyphicon
                    {
                        margin-right: 5px;
                    }

                    .panel-body
                    {
                        overflow-y: scroll;
                        height: 900px;
                    }

                    ::-webkit-scrollbar-track
                    {
                        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
                        background-color: #F5F5F5;
                    }

                    ::-webkit-scrollbar
                    {
                        width: 30px;
                        background-color: #F5F5F5;
                    }

                    ::-webkit-scrollbar-thumb
                    {
                        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
                        background-color: #555;
                    }

                    .panel-primary > .panel-heading {
                        background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
                    }

                    .panel-primary {
                        border-color: #750f0b !important;
                    }

                    .btn-warning {
                        background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
                    }

                    .btn-warning:hover, .btn-warning:focus {
                        background-color: #5cb85c !important;
                        background-position: 0 -15px;
                    }

                    .btn-warning:hover {
                        color: #fff;
                        background-color: #5cb85c !important;
                        border-color: #5cb85c !important;
                    }

                </style>


                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-6">
                                <span class="glyphicon glyphicon-comment"></span> <?= \Yii::t('c.oMqYe6h5yY', 'Чат') ?>

                            </div>
                            <div class="col-lg-6">
                                <p class="text-right"><a href="/page/item?id=7" target="_blank" style="color: #fff;"><?= \Yii::t('c.oMqYe6h5yY', 'Правила') ?></a></p>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php
                        $this->registerJs(<<<JS

$('.buttonDelete2').click(functionDelete2);
JS
                        );

                        ?>

                        <ul class="chat">
                            <?php
                            if ($is_search) {
                                $rows = \common\models\exchange\ChatMessage::find()
                                    ->where(['deal_id' => $chat_id])
                                    ->andWhere(['like', 'message', $search_text])
                                    ->andWhere(['>', 'created_at', time() - 60*60*24*14])
                                    ->all();
                            } else {
                                $rows = \common\models\exchange\ChatMessage::find()
                                    ->where(['deal_id' => $chat_id])
                                    ->andWhere(['>', 'created_at', time() - 60*60*24*14])
                                    ->all();
                            }
                            ?>
                            <?php /** @var \common\models\exchange\ChatMessage $message */ ?>
                            <?php foreach ($rows as $message) { ?>
                                <li class="left clearfix js-chat-message" data-id="<?= $message->id ?>"><span class="chat-img pull-left">
                                        <img width="50" src="<?= $message->getUser()->getAvatar() ?>" alt="<?= $message->getUser()->getName2() ?>" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
                                        <div class="header2">
                                            <strong class="primary-font"><?= $message->getUser()->getName2() ?></strong> <small class="pull-right text-muted">
                                                <span class="glyphicon glyphicon-time"></span><?= Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s') ?></small>
                                        </div>
                                        <?php if (Yii::$app->authManager->checkAccess($message->user_id, 'permission_admin_chat')) { ?>
                                            <p style="color: red;">
                                                <?= ($is_search)? drawMessage2($message->message, $search_text) : $message->message ?>
                                            </p>
                                        <?php } else { ?>
                                            <p>
                                                <?= ($is_search)? drawMessage2($message->message, $search_text) : $message->message ?>
                                            </p>
                                        <?php } ?>

                                        <?php if (Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin_chat')) { ?>
                                            <p>
                                                <a href="javascript:void(0)" class="buttonDelete" data-id="<?= $message->id ?>"><?= \Yii::t('c.oMqYe6h5yY', 'Удалить') ?></a>
                                                <a href="javascript:void(0)" class="buttonBan" data-id="<?= $message->id ?>"><?= \Yii::t('c.oMqYe6h5yY', 'Забанить') ?></a>
                                                <a href="javascript:void(0)" class="buttonDeleteUser" data-id="<?= $message->id ?>"><?= \Yii::t('c.oMqYe6h5yY', 'Удалить пользователя') ?></a>
                                            </p>
                                        <?php } ?>
                                        <?php if ($message->user_id == Yii::$app->user->id) { ?>
                                            <p>
                                                <a href="javascript:void(0)" class="buttonDelete2" data-id="<?= $message->id ?>"><?= \Yii::t('c.oMqYe6h5yY', 'Удалить') ?></a>
                                            </p>
                                        <?php } ?>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <?php if ($isShowCharMessage) { ?>
                        <div class="panel-footer js-chat-panel-footer">
                            <div class="input-group">
                                <input id="btn-input" type="text" class="form-control input-sm" placeholder="<?= \Yii::t('c.oMqYe6h5yY', 'Напиши свое сообщение здесь...') ?>" />
                                <span class="input-group-btn">
                <button class="btn btn-warning btn-sm" id="btn-chat">
                    <?= \Yii::t('c.oMqYe6h5yY', 'Отправить') ?></button>
            </span>
                            </div>
                            <p><?= \Yii::t('c.oMqYe6h5yY', 'Если необходимо') ?>:</p>
                            <div style="margin-top: 10px;">
                                <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                                    'id'        => 'upload' . 1,
                                    'name'      => 'upload' . 1,
                                    'attribute' => 'upload' . 1,
                                    'model'     => new \avatar\models\forms\ChatFile(),
                                    'update'    => [],
                                    'settings'  => [
                                        'maxSize'           => 5 * 1000,
                                        'controller'        => 'upload4',
                                        'accept'            => 'image/*',
                                        'button_label'      => Yii::t('c.oMqYe6h5yY', 'Прикрепите файл'),
                                        'allowedExtensions' => ['jpg', 'jpeg', 'png'],
                                        'functionSuccess'   => new \yii\web\JsExpression(<<<JS
function (response) {
    // response.url
}
JS
                                        ),
                                    ],
                                ]); ?>
                            </div>


                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
