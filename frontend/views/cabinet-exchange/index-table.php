<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $ActiveDataProvider \yii\data\ActiveDataProvider */
/** @var $filter \avatar\models\search\Offer2 */
/** @var $sort \yii\data\Sort */


$isTelegram = \cs\Application::isEmpty(Yii::$app->user->identity->telegram_chat_id) ? 0 : 1;
$this->registerJS(<<<JS

$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {});

$('.buttonBuy').click(function() {
    var id = $(this).data('id');
    if ({$isTelegram} == 0) {
        $('#modalTelegram').modal();
        return;
    }
    ajaxJson({
        url: '/cabinet-exchange/deal-open-ajax',
        data: {id: id},
        success: function (ret) {
            window.location = '/cabinet-exchange/deal-open' + '?' + 'id' + '=' + ret.id;
        }
    });
});

JS
);

?>

<div class="row">
    <div class="col-lg-1">
        <p class="text-right"><?= \Yii::t('c.XwLuZKjn6M', 'Я хотел бы') ?></p>
    </div>
    <div class="col-lg-2">
        <?php
        $e = $_SERVER['REQUEST_SCHEME'] . '://' . Yii::$app->request->hostName . Yii::$app->request->url;
        $u = new \cs\services\Url($e);
        $type_id_value = null;
        if (isset($u->params['Offer2[type_id]'])) {
            $type_id_value = $u->params['Offer2[type_id]'];
            $u->params['Offer2[type_id]'] = 1;
            $url1 = $u->__toString();
            $u->params['Offer2[type_id]'] = 2;
            $url2 = $u->__toString();
            $u->params['Offer2[type_id]'] = '';
            $url3 = $u->__toString();
        } else {
            $u->params['Offer2[type_id]'] = 1;
            $url1 = $u->__toString();
            $u->params['Offer2[type_id]'] = 2;
            $url2 = $u->__toString();
            $u->params['Offer2[type_id]'] = '';
            $url3 = $u->__toString();
        }

        $this->registerJs(<<<JS
$('#type_id2').on('change', function(ret) {
    if ($(this).val() == '') {
        window.location = '{$url3}';
    }
    if ($(this).val() == 1) {
        window.location = '{$url1}';
    }
    if ($(this).val() == 2) {
        window.location = '{$url2}';
    }
});
JS
);
        $opt1 = '';
        $opt2 = '';
        if (!is_null($type_id_value)) {
            if ($type_id_value == 1) {
                $opt1 = ' selected="selected"';
                $opt2 = '';
            }
            if ($type_id_value == 2) {
                $opt1 = '';
                $opt2 = ' selected="selected"';
            }
        }
        ?>
        <select class="form-control" id="type_id2">
            <option></option>
            <option value="1"<?= $opt1 ?>><?= \Yii::t('c.XwLuZKjn6M', 'Продать') ?></option>
            <option value="2"<?= $opt2 ?>><?= \Yii::t('c.XwLuZKjn6M', 'Купить') ?></option>
        </select>
    </div>
</div>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $ActiveDataProvider,
    'filterModel'  => $filter,
    'tableOptions' => [
        'class' => 'table table-striped table-hover table_exchange',
    ],
    'summary'      => '',
    'rowOptions'   => function ($item) {
        $data = [
            'data'  => ['id' => $item['id']],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        [
            'header'         => $sort->link('assessment_positive'),
            'attribute'      => 'assessment_positive',
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '15%'])],
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Пользователь')],
            'content'        => function ($item) {
                $html = [];
                $u = \common\models\UserAvatar::findOne($item['user_id']);
                $size = 40;
                $i = $u->getAvatar();
                $params = [
                    'class'  => "img-circle",
                    'width'  => $size,
                    'height' => $size,
                    'style'  => ['margin-bottom' => '0px',],
                    'data'   => ['toggle' => 'tooltip'],
                    'title'  => $u->getName2(),
                ];

                // Если offline
                if (time() - $u->last_action > Yii::$app->params['timeOnline']) {
                    $params['style']['border'] = '3px solid silver';
                } else {
                    $params['style']['border'] = '3px solid green';
                }

                if (isset($params['style'])) {
                    if (is_array($params['style'])) {
                        $params['style'] = Html::cssStyleFromArray($params['style']);
                    }
                }

                // считаю рейтинги
                $r_minus1 = $item['assessment_negative'];
                $r1 = $item['assessment_positive'];

                $html_r_minus1 = Html::tag('span', $r_minus1, ['class' => 'label label-danger', 'title' => Yii::t('c.XwLuZKjn6M', 'Негативных оценок'), 'data' => ['toggle' => 'tooltip']]);
                $html_r1 = Html::tag('span', $r1, ['class' => 'label label-success', 'title' => Yii::t('c.XwLuZKjn6M', 'Позитивных оценок'), 'data' => ['toggle' => 'tooltip']]);
                if ($item['type_id'] == \common\models\exchange\Offer::TYPE_ID_BUY) {
                    $html_r2 = Html::tag('span', Yii::t('c.XwLuZKjn6M', 'Покупает'), ['class' => 'label label-success', 'style' => 'margin-left:10px;']);;
                } else {
                    $html_r2 = Html::tag('span', Yii::t('c.XwLuZKjn6M', 'Продает'), ['class' => 'label label-danger', 'style' => 'margin-left:10px;']);;
                }

                $html[] = $u->name_first;

                $html[] = Html::img($i, $params);
                $html[] = $html_r_minus1 . $html_r1 . $html_r2;

                return  join('<br>', $html);
            },
        ],
        [
            'header'         => $sort->link('last_action3'),
            'attribute'      => 'last_action3',
            'contentOptions' => ['aria-label' => ''],
            'content'        => function ($item) {
                $html = [];
                $u = \common\models\UserAvatar::findOne($item['user_id']);
                $offline = Html::tag('span', 'ONLINE', ['style' => 'margin-left:0px;']);
                if (time() - $item['last_action'] > Yii::$app->params['timeOnline']) {
                    if (!is_null($u->last_action)) {
                        $offline = Html::tag('span', 'OFF ' . \cs\services\DatePeriod::back($u->last_action, ['isShort' => true] ), ['style' => 'color: #aaa; margin-left:0px;']);
                    }
                }
                $html[] = $offline;

                // проверяю на достоверный аккаунт
                $ids = \common\models\Config::get('trusted-users');
                if (\cs\Application::isEmpty($ids)) $ids = [];
                else $ids = \yii\helpers\Json::decode($ids);
                if (in_array($item['user_id'], $ids) && $item['currency_io'] == 12) {
                    $html[] = '<img src="/images/trust1.png" width="25" data-toggle="tooltip" title="Доверенный аккаунт">';
                }

                return  join('<br>', $html);
            },
        ],

        [
            'label'          => Yii::t('c.XwLuZKjn6M', 'Монету'),
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '9%'])],
            'attribute'      => 'currency_io',
            'filter'         => \yii\helpers\ArrayHelper::map(
                \common\models\CurrencyIO::find()
                    ->innerJoin('currency', 'currency.id = currency_io.currency_ext_id')
                    ->select(['currency_io.*'])
                    ->where(['currency_io.is_exchange' => 1])
                    ->orderBy(['currency.sort_index' => SORT_ASC])
                    ->all(),
                'id',
                'tab_title'
            ),
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Монету'), 'class' => 'text-center'],
            'content'        => function ($item, $p2, $p3, $p4) {
                $cio = \common\models\CurrencyIO::findOne($item['currency_io']);
                $currency = Currency::findOne($cio->currency_ext_id);
                $size = 60;
                $params = [
                    'class'  => "img-circle",
                    'width'  => $size,
                    'height' => $size,
                    'style'  => ['margin-bottom' => '0px',],
                    'data'   => ['toggle' => 'tooltip'],
                    'title'  => $p4->filter[$item['currency_io']],
                ];

                return Html::img($currency->image, $params) . '<br>' . $p4->filter[$item['currency_io']];
            },
        ],

        [
            'header'         => $sort->link('price', ['data' => ['toggle' => 'tooltip'], 'title' => Yii::t('c.XwLuZKjn6M', 'Целое число')]),
            'attribute'      => 'price',
            'headerOptions'  => ['class' => 'text-right','style' => Html::cssStyleFromArray(['width' => '10%'])],
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'По цене'), 'nowrap' => 'nowrap', 'class' => 'text-right'],
            'content'        => function ($item) {
                $price_is_free = $item['price_is_free'];
                if ($price_is_free == 1) {
                    return 'Предложите<br>вашу цену';
                }
                $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $item['currency_id']]);
                $currency = \common\models\piramida\Currency::findOne($link->currency_int_id);
                $price = $item['price'];
                $decimalsPrint = $currency->decimals_view;
                $pricePrint = bcdiv($price, bcpow(10, $currency->decimals), $currency->decimals);

                // проверяю "Доверенный продает NEIRON?"
                if ($item['type_id'] == \common\models\exchange\Offer::TYPE_ID_SELL) {
                    if ($item['currency_io'] == 12) { // NEIRON
                        $ids = \common\models\Config::get('trusted-users');
                        if (\cs\Application::isEmpty($ids)) $ids = [];
                        else $ids = \yii\helpers\Json::decode($ids);
                        if (in_array($item['user_id'], $ids)) {
                            if ($item['currency_id'] == Currency::RUB) {
                                $c = Currency::findOne(Currency::NEIRO);
                                $pricePrint = $c->price_rub;
                            }
                            if ($item['currency_id'] == Currency::USDT) {
                                $c = Currency::findOne(Currency::NEIRO);
                                $pricePrint = $c->price_usd;
                            }
                        }
                    }
                }

                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint);
            },
        ],
        [
            'header'         => Yii::t('c.XwLuZKjn6M', 'За валюту'),
            'attribute'      => 'offer_pay_id',
            'filter'         => \yii\helpers\ArrayHelper::map(
                \common\models\exchange\OfferPay::find()->all(),
                'id',
                'code'
            ),
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '5%'])],
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'За валюту')],
            'content'        => function ($item) {
                $OfferPay = \common\models\exchange\OfferPay::findOne($item['offer_pay_id']);
                $c = Html::tag('span', $OfferPay->code, ['class' => 'label label-info']);

                return $c;
            },
        ],
        [
            'header'         => $sort->link('volume_io_start', ['data' => ['toggle' => 'tooltip'], 'title' => Yii::t('c.XwLuZKjn6M', 'Целое число')]),
            'attribute'      => 'volume_io_start',
            'headerOptions'  => [
                'style' => Html::cssStyleFromArray(['width' => '10%']),
                'class' => 'text-right',
            ],
            'contentOptions' => [
                'nowrap'     => 'nowrap',
                'aria-label' => Yii::t('c.XwLuZKjn6M', 'В объеме'),
                'class'      => 'text-right',
            ],
            'content'        => function ($item) {
                $IO = \common\models\CurrencyIO::findOne($item['currency_io']);
                $c = \common\models\piramida\Currency::findOne($IO->currency_int_id);
                $data[0] = bcdiv($item['volume_io_start'], pow(10, $c->decimals), $c->decimals);
                $data[1] = bcdiv($item['volume_io_finish_deal'], pow(10, $c->decimals), $c->decimals);
                $from = Yii::$app->formatter->asDecimal($data[0], $c->decimals_view);
                $to = Yii::$app->formatter->asDecimal($data[1], $c->decimals_view);
                $print = Html::tag('span','от ', ['style' => 'color: #ccc;']) . $from . '<br>' . Html::tag('span',' до ', ['style' => 'color: #ccc;']) . $to;

                return $print;

            },
        ],
        [
            'header'         => Yii::t('c.XwLuZKjn6M', 'Условия сделки и платежные системы'),
            'filter'         => \yii\helpers\ArrayHelper::map(
                \common\models\exchange\PayMethod::find()->orderBy(['id' => SORT_ASC])->all(),
                'id',
                'name'
            ),
            'attribute'      => 'pay_id',
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '19%'])],
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Условия сделки и платежные системы')],
            'content'        => function ($item) {
                $list = \common\models\exchange\OfferPayLink::find()->where(['offer_id' => $item['id']])->select('pay_id')->column();

                $listName1 = \common\models\PayMethodLang::find()
                    ->select(['pay_method_lang.name', 'pay_method.id'])
                    ->innerJoin('pay_method', 'pay_method.id = pay_method_lang.parent_id')
                    ->where(['in', 'pay_method_lang.parent_id', $list])
                    ->andWhere(['language' => Yii::$app->language])
                    ->orderBy(['pay_method.sort_index' => SORT_ASC])
                    ->all();

                $list2 = \yii\helpers\ArrayHelper::getColumn($listName1, 'id');
                $list3 = [];
                // Ищу что еще не переведено
                foreach ($list as $id) {
                    if (!in_array($id, $list2)) {
                        $list3[] = $id;
                    }
                }

                $listName2 = \common\models\exchange\PayMethod::find()->where(['in', 'id', $list3])->select('name')->orderBy(['sort_index' => SORT_ASC])->column();

                $listName = \yii\helpers\ArrayHelper::merge(
                    \yii\helpers\ArrayHelper::getColumn($listName1, 'name'),
                    $listName2
                );

                return join(', ', $listName);
            },
        ],
        [
            'header'         => $sort->link('created_at'),
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Создано')],
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '8%'])],
            'content'        => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            },
        ],
        [
            'header'         => $sort->link('deals_count_all', ['data' => ['toggle' => 'tooltip'], 'title' => Yii::t('c.XwLuZKjn6M', 'Сделок всего в которых участвовал')]),
            'attribute'      => 'deals_count_all',
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '8%'])],
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Сделок всего в которых участвовал')],
        ],
        [
            'header'         => $sort->link('avg_deal'),
            'attribute'      => 'avg_deal',
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '10%'])],
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Ср. время сделки')],
            'content'        => function ($item) {
                $c = $item['avg_deal'];
                if (is_null($c)) return '';
                $c = (int)$c;
                $min = (int)($c / 60);
                $sec = $c - ($min * 60);

                return $min . ' ' . Yii::t('c.hD8QKgnMrG', 'мин') . '<br>' . $sec . ' ' . Yii::t('c.hD8QKgnMrG', 'сек');
            },
        ],
        [
            'header'         => Yii::t('c.XwLuZKjn6M', 'Действие'),
            'filter'         => [
                1 => Yii::t('c.XwLuZKjn6M', 'Продать'),
                2 => Yii::t('c.XwLuZKjn6M', 'Купить'),
            ],
            'headerOptions'  => ['style' => Html::cssStyleFromArray(['width' => '5%'])],
            'attribute'      => 'type_id',
            'contentOptions' => ['aria-label' => Yii::t('c.XwLuZKjn6M', 'Действие')],
            'content'        => function ($item) {
                if ($item['user_id'] == Yii::$app->user->id) return '';

                $currency = Currency::findOne($item['currency_id']);
                $price = $item['price'];
                $pricePrint = $price / pow(10, $currency->decimals);
                $decimalsPrint = ($currency->decimals > 2) ? 2 : $currency->decimals;
                $print = Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint);

                $action = ($item['type_id'] == \common\models\exchange\Offer::TYPE_ID_BUY) ? Yii::t('c.XwLuZKjn6M', 'Продать') : Yii::t('c.XwLuZKjn6M', 'Купить');
                $class = ($item['type_id'] == \common\models\exchange\Offer::TYPE_ID_BUY) ? 'btn buttonBuy cbuttonSell' : 'btn buttonBuy cbuttonBuy';

                return Html::button($action, [
                    'class' => $class,
                    'data'  => [
                        'id' => $item['id'],
                    ],
                ]);
            },
        ],
    ],
]) ?>

