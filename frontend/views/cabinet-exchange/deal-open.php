<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this  yii\web\View */
/** @var $form  yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\exchange\DealOpen */
/** @var $offer \common\models\exchange\Offer */
/** @var $deal  \common\models\exchange\Deal */


$this->title = Yii::t('c.hD8QKgnMrG', 'Открытие сделки');

$currency_io = $offer->currency_io;
$cio = \common\models\CurrencyIO::findOne($currency_io);
$c1 = \common\models\piramida\Currency::findOne($cio->currency_int_id);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
        <p class="alert alert-danger">
            <?= \Yii::t('c.hD8QKgnMrG', 'Внимание! Чтобы партнер') ?>
            <br>
            <br>
            <?= \Yii::t('c.hD8QKgnMrG', 'Вы сейчас находитесь') ?>
        </p>
        <?php
        $user = \common\models\UserAvatar::findOne($offer->user_id);
        $offerPay = \common\models\exchange\OfferPay::findOne($offer['offer_pay_id']);
        ?>
        <?php if (Yii::$app->user->id == $deal->user_id) { ?>
            <h2><?= (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL)? Yii::t('c.hD8QKgnMrG', 'Я покупаю') : Yii::t('c.hD8QKgnMrG', 'Я продаю')) ?> <?= $c1->name ?> <?= \Yii::t('c.hD8QKgnMrG', 'за') ?>  <?= $offerPay->code ?></h2>
        <?php } else { ?>
            <h2><?= (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL)? Yii::t('c.hD8QKgnMrG', 'Я продаю') : Yii::t('c.hD8QKgnMrG', 'Я покупаю')) ?> <?= $c1->name ?> <?= \Yii::t('c.hD8QKgnMrG', 'за') ?>  <?= $offerPay->code ?></h2>
        <?php } ?>
    </div>


    <div class="col-lg-6">

        <?= $this->render('@shop/views/cabinet-exchange/chat', [
            'room_id'         => $deal->chat_room_id,
            'user_id'         => Yii::$app->user->id,
            'send_url'        => '/cabinet-exchange-chat/send?id=' . $deal->id,
        ]) ?>

        <?php
        $this->registerJs(<<<JS
$('#dealopen-volume_io').on('input',function(e) {
    var priceVVB = $('#priceVVB').val();
    var thisVal = $(this).val();
    $('#dealopen-volume22').html(thisVal * priceVVB);
});

JS
        );
        ?>

        <?php if (Yii::$app->user->id == $deal->user_id) { ?>
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <hr>
            <?php

            $currency = Currency::findOne($offer['currency_id']);
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $offer['currency_id']]);
            $currency_p = \common\models\piramida\Currency::findOne($link->currency_int_id);
            $price = $offer['price'];
            $pricePrint = bcdiv($price, bcpow(10, $currency->decimals), $currency->decimals_view);
            $decimalsPrint = $c1->decimals_view;
            $data[0] = bcdiv($offer['volume_io_start'], pow(10, $c1->decimals), $c1->decimals_view);
            $data[1] = bcdiv($offer['volume_io_finish_deal'], pow(10, $c1->decimals), $c1->decimals_view);
            $from = Yii::$app->formatter->asDecimal($data[0], $decimalsPrint);
            $to = Yii::$app->formatter->asDecimal($data[1], $decimalsPrint);

            $print = Html::tag('span',Yii::t('c.hD8QKgnMrG', 'от').' ', ['style' => 'color: #ccc;']) . $from . Html::tag('span',' '.Yii::t('c.hD8QKgnMrG', 'до').' ', ['style' => 'color: #ccc;']) . $to;
            $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);
            ?>

            <?= $form->field($model, 'volume_io')->label(Yii::t('c.hD8QKgnMrG', 'Объём') . ' ' . $c1->code . ' ' . $print) ?>

            <div class="panel panel-default">
                <div class="panel-body">
                    <p><?= \Yii::t('c.hD8QKgnMrG', 'Курс обмена') ?></p>
                    <h2>1 <?= $c1->code ?> = <?= Yii::$app->formatter->asDecimal(bcdiv($deal->price, bcpow(10, $currency_p->decimals), $currency_p->decimals_view), $currency_p->decimals_view) ?> <?= $offerPay->title ?></h2>
                </div>
            </div>

            <?php
            $offerPay = \common\models\exchange\OfferPay::findOne($offer->offer_pay_id);
            ?>
            <p class="alert alert-info"><?= \Yii::t('c.hD8QKgnMrG', 'Итоговая сумма сделки') ?>: <span id="dealopen-volume22">0</span> <?= $offerPay->code ?></p>

        <?php if ($offer->price_is_free != 1) { ?>
            <p class="alert alert-success"><?= \Yii::t('c.hD8QKgnMrG', 'Продолжите если цена вам подходит') ?></p>
        <?php } ?>
        <?php if ($offer->price_is_free == 1) { ?>
            <p class="alert alert-success">Договоритесь с партнером о цене за монету. Затем открывайте новую сделку по согласованной цене, или сформируйте персональное предложение.</p>
        <?php } ?>

            <?php
            $priceVVB = '';
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $offer->currency_id]);
            $c = \common\models\piramida\Currency::findOne($link->currency_int_id);
            $priceVVB = bcdiv($deal->price, bcpow(10, $c->decimals), $c->decimals);
            ?>
            <?= Html::hiddenInput(null, $priceVVB, ['id' => 'priceVVB']) ?>
            <hr>
            <div class="form-group">
                <?php if ($offer->price_is_free != 1) { ?>
                    <?= Html::submitButton(Yii::t('c.hD8QKgnMrG', 'Открыть сделку'), [
                        'class' => 'btn cbtn-deal',
                        'name'  => 'contact-button',
                        'style' => 'width:100%',
                    ]) ?>
                <?php } ?>
            </div>
            <?php ActiveForm::end(); ?>
        <?php } else { ?>
            <?php
            $currency = Currency::findOne($offer['currency_id']);
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $offer['currency_id']]);
            $currency_p = \common\models\piramida\Currency::findOne($link->currency_int_id);

            $decimalsPrint = $c1->decimals_view;
            $data[0] = bcdiv($offer['volume_io_start'], pow(10, $c1->decimals), $c1->decimals_view);
            $data[1] = bcdiv($offer['volume_io_finish_deal'], pow(10, $c1->decimals), $c1->decimals_view);
            $from = Yii::$app->formatter->asDecimal($data[0], $decimalsPrint);
            $to = Yii::$app->formatter->asDecimal($data[1], $decimalsPrint);

            $print = Html::tag('span',Yii::t('c.hD8QKgnMrG', 'от').' ', ['style' => 'color: #ccc;']) . $from . Html::tag('span',' '.Yii::t('c.hD8QKgnMrG', 'до').' ', ['style' => 'color: #ccc;']) . $to;
            $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);
            ?>
            <p><b><?= Yii::t('c.hD8QKgnMrG', 'Объём') . ' ' . $c1->code . ' ' . $print ?></b></p>
            <div class="panel panel-default">
                <div class="panel-body">
                    <p><?= \Yii::t('c.hD8QKgnMrG', 'Курс обмена') ?></p>
                    <h2>1 <?= $c1->code ?> = <?= Yii::$app->formatter->asDecimal(bcdiv($deal->price, bcpow(10, $currency_p->decimals), $currency_p->decimals_view), $currency_p->decimals_view) ?> <?= $offerPay->title ?></h2>
                </div>
            </div>
        <?php } ?>

    </div>
    <div class="col-lg-6">
        <?php if (Yii::$app->user->id == $deal->user_id) { ?>
            <h2><?= (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL)? Yii::t('c.hD8QKgnMrG', 'О продавце') : Yii::t('c.hD8QKgnMrG', 'О покупателе')) ?></h2>
        <?php } else { ?>
            <h2><?= (($deal->type_id == \common\models\exchange\Deal::TYPE_ID_SELL)? Yii::t('c.hD8QKgnMrG', 'О продавце') : Yii::t('c.hD8QKgnMrG', 'О покупателе')) ?></h2>
            <?php $user = $deal->getUser() ?>
        <?php } ?>

        <p><img src="<?= $user->getAvatar() ?>" width="80" class="img-circle"></p>
        <p><?= $user->getName2() ?></p>

        <?php
        $ids = \common\models\Config::get('trusted-users');
        if (\cs\Application::isEmpty($ids)) $ids = [];
        else $ids = \yii\helpers\Json::decode($ids);
        ?>
        <?php if (in_array($user->id, $ids) && $offer['currency_io'] == 12) { ?>
            <img src="/images/trust2.png" width="150" data-toggle="tooltip" title="Доверенный аккаунт">
        <?php } ?>

        <?php
        // считаю рейтинги
        $r1 = $user->assessment_positive;
        $r_minus1 = $user->assessment_negative;

        $html_r1 = Html::tag('span', $r1, ['class' => 'label label-success', 'title' => Yii::t('c.hD8QKgnMrG', 'Позитивных оценок'), 'data' => ['toggle' => 'tooltip']]);
        $html_r_minus1 = Html::tag('span', $r_minus1, ['class' => 'label label-danger', 'title' => Yii::t('c.hD8QKgnMrG', 'Негативных оценок'), 'data' => ['toggle' => 'tooltip']]);
        ?>
        <p><?= $html_r_minus1 . $html_r1 ?></p>
        <p><?= \Yii::t('c.hD8QKgnMrG', 'Сделок всего в которых участвовал') ?>: <?= $user->deals_count_all ?></p>
        <p><?= \Yii::t('c.hD8QKgnMrG', 'Сделок по всем своим предложениям') ?>: <?= $user->deals_count_offer ?></p>

        <p><?= \Yii::t('c.hD8QKgnMrG', 'Среднее время сделки') ?>: <?php
            $c = \common\models\exchange\Deal::find()
                ->innerJoin('offer', 'offer.id = deal.offer_id')
                ->where(['offer.user_id' => $offer->user_id])
                ->andWhere([
                    'deal.status' => [
                        \common\models\exchange\Deal::STATUS_CLOSE,
                        \common\models\exchange\Deal::STATUS_MONEY_RECEIVED,
                    ],
                ])
                ->andWhere(['not', ['deal.time_finish' => null]])
                ->select('avg(deal.time_finish - deal.time_accepted)')
                ->scalar();

            if (is_null($c)) {

                echo '';

            } else {

                $c = (int)$c;
                $min = (int)($c / 60);
                $sec = $c - ($min * 60);

                echo $min . ' ' . Yii::t('c.hD8QKgnMrG', 'мин') . ' ' . $sec . ' '  . Yii::t('c.hD8QKgnMrG', 'сек');

            }
            ?></p>

        <?php if ($user->is_show_phone) { ?>
            <p><?= \Yii::t('c.hD8QKgnMrG', 'Телефон') ?>: <?= $user->phone ?></p>
        <?php } ?>
        <?php if ($user->is_show_telegram) { ?>
            <p><?= \Yii::t('c.hD8QKgnMrG', 'Telegram') ?>: <?= $user->public_telegram ?></p>
        <?php } ?>

        <h2><?= \Yii::t('c.hD8QKgnMrG', 'Условия сделки') ?></h2>
        <p><?= Html::encode($offer->condition) ?></p>

    </div>
</div>