<?php
// получаю идентификатор чата, нужно кто с кем чатится
$user1 = $offer->user_id;
$user2 = Yii::$app->user->id;
if ($user1 > $user2) {
    $user3 = $user1;
    $user1 = $user2;
    $user2 = $user3;
}
$tet = \common\models\ChatTet::findOne(['user1_id' => $user1, 'user2_id' => $user2]);
if (is_null($tet)) {
    $ChatRoom = \common\models\ChatRoom::add(['last_message' => null]);
    $tet = \common\models\ChatTet::add([
        'user1_id' => $user1,
        'user2_id' => $user2,
        'room_id'  => $ChatRoom->id,
    ]);
} else {
    $ChatRoom = \common\models\ChatRoom::findOne($tet->room_id);
}
?>
<div id="chat">
    <?= $this->render('deal-action-chat2', ['room_id' => $ChatRoom->id]) ?>
</div>