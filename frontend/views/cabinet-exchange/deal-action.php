<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $Deal Deal */
/** @var $model \avatar\models\forms\exchange\DealOpen */

$this->title = Yii::t('c.XE1gntn5b0', 'Сделка') . ' #' . $Deal->id;

$offer = $Deal->getOffer();
$user = \common\models\UserAvatar::findOne($offer->user_id);
$method = \common\models\exchange\PayMethod::findOne($offer->pay_id);


$user_id = Yii::$app->user->id;
$user_address = Yii::$app->user->identity->getAddress();

$partner_user_id = ($user_id == $Deal->user_id) ? $offer->user_id : $Deal->user_id;
$partner_user = \common\models\UserAvatar::findOne($partner_user_id);
$partner_user_address = $partner_user->getAddress();

\avatar\assets\SocketIO\Asset::register($this);

$serverName = \avatar\assets\SocketIO\Asset::getHost();


$this->registerJs(<<<JS
var socket = io.connect('{$serverName}');
console.log(socket);
var STATUS = {$Deal->status};

var Data;
var Hour;
var Minutes;
var Seconds;
var time2;

function addHistory(text)
{
    Data = new Date();
    Hour = Data.getHours();
    if (Hour < 10) Hour = '0' + Hour;
    Minutes = Data.getMinutes();
    if (Minutes < 10) Minutes = '0' + Minutes;
    Seconds = Data.getSeconds();
    if (Seconds < 10) Seconds = '0' + Seconds;
    time2 = Hour + ":" + Minutes + ":" + Seconds;
    var td1 = $('<td>').html(time2);
    var td2 = $('<td>').html(text);
    $('#history_table').append($('<tr>').append(td1).append(td2));
}

JS
);

\avatar\assets\CountDown\Asset::register($this);

// $isSend
// переменная которая хранит статус, что уведомление об открытой сделке отправлено deal-opened-# в сессии
// После обновления страницы уже не отправится, хранится статус 30 мин.
// 1 нужно отправить
// 2 не нужно отправить
{
    $isSend = Yii::$app->cache->get('deal-opened-' . $Deal->id);

    if ($isSend === false) $isSend = 1;

    if ($Deal->user_id == Yii::$app->user->id) {
        if ($isSend == 1) {
            $this->registerJs(<<<JS
socket.emit('deal-opened', {$Deal->id}, {$user_id}, {$partner_user_id});
console.log(['deal-opened', {$Deal->id}, {$user_id}, {$partner_user_id}]);

ajaxJson({
    url: '/cabinet-exchange/deal-open-send',
    data: {
        id: {$Deal->id}
    },
    success: function(ret) {
        
    }
});


JS
            );
            Yii::$app->cache->set('deal-opened-' . $Deal->id, 2, 60 * 30);
        }
    }
}

// определение экрана
if ($offer->user_id == Yii::$app->user->id) {
    $screen_id = Assessment::SCREEN_OFFER;
} else if ($Deal->user_id == Yii::$app->user->id) {
    $screen_id = Assessment::SCREEN_DEAL;
} else {
    $screen_id = Assessment::SCREEN_DEAL;
}


$this->registerJs(<<<JS
socket.emit('new-user', {$Deal->id}, {$user_id});

socket.on('deal-opened', (deal, user_id, partner_user_id) => {
    if (deal == {$Deal->id}) {
        switchInformer(1);
    }
});

socket.on('reload-status', data => {
    console.log('reload-status');
    STATUS = data.status;
    if (data.deal == {$Deal->id}) {
        switchInformer(STATUS);
    }
});
JS
);

/**
 * Это сценарии для перерисовки дисплея
 */
if (true) {
// если Я (предложение) продаю
    $str703 = Yii::t('c.XE1gntn5b0', 'Сделка оценена и завершена пользователем');
    $str1059 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, ожидайте подтверждения');

    if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL) {
        if ($screen_id == Assessment::SCREEN_OFFER) {

            switch ($Deal->status) {
                case Deal::STATUS_OPEN:
                    $isShowButtonCancel = true;
                    $StateStatus1 = 0;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = true;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, подтвердите сделку');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_BLOCK:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_SENDED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = true;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_RECEIVED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = true;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства получены');
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_CLOSE:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 1;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;

                    $a_offer = Assessment::findOne(['user_id' => $offer->user_id, 'deal_id' => $Deal->id]);
                    $a_deal = Assessment::findOne(['user_id' => $Deal->user_id, 'deal_id' => $Deal->id]);
                    if (is_null($a_offer) && !is_null($a_deal)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }
                    if (!is_null($a_offer) && is_null($a_deal)) {
                        $isShowBlockRate = true;
                        $isShowCharMessage = true;
                    }
                    if (!is_null($a_offer) && !is_null($a_deal)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }

                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
                    $isShowCounter = false;
                    break;
                case Deal::STATUS_HIDE:
                case Deal::STATUS_CANCEL:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 3;
                    $StateStatus2 = 3;
                    $StateStatus3 = 3;
                    $StateStatus4 = 3;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
                    $isShowCounter = false;
                    $isShowCharMessage = false;
                    break;
                case Deal::STATUS_AUDIT_WAIT:
                case Deal::STATUS_AUDIT_ACCEPTED:
                case Deal::STATUS_AUDIT_FINISH:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 2;
                    $StateStatus2 = 2;
                    $StateStatus3 = 2;
                    $StateStatus4 = 2;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = 'Сделка отменена';
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
            }
            $str1 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, подтвердите сделку');
            $str2 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
            $str3 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
            $str4 = Yii::t('c.XE1gntn5b0', 'Средства получены');
            $str5 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
            $str6 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
            $this->registerJs(<<<JS
function switchInformer(status) {
    if (status == 1) {
        document.getElementById('label-status').textContent = "{$str1}";
        document.getElementsByClassName('buttonSellAccept')[0].style.display = "";

        $('.buttonInviteArbitrator').hide();
    }
    // Deal::STATUS_BLOCK
    else if (status == 2) {
        document.getElementById('label-status').textContent = "{$str2}";
        $('.buttonSellAccept').hide();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();
        $('.buttonCancel').hide();

        // Добавляю историю сделок
        addHistory('{$str2}');
    }
    // Deal::STATUS_MONEY_SENDED
    else if (status == 3) {
        document.getElementById('label-status').textContent = "{$str3}";
        $('.js-buttonSellMoneyReceived').show();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');

        $('.buttonInviteArbitrator').show();

        // Добавляю историю сделок
        addHistory('{$str3}');
    }
    // Deal::STATUS_MONEY_RECEIVED
    else if (status == 4) {
        document.getElementById('label-status').textContent = "{$str4}"
        $('.js-buttonSellMoneyReceived').hide();
        $('.js-block-rank').show();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');
        
        // Добавляю историю сделок
        addHistory('{$str4}');
                
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str5}'));
    }
    // Deal::STATUS_CLOSE
    else if (status == 5) {
        document.getElementById('label-status').textContent = "{$str5}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');
        
        // Добавляю историю сделок
        addHistory('Сделка оценена и завершена пользователем: {$partner_user_address}');
    }
    else if (status == 10) {
        document.getElementById('label-status').textContent = "{$str6}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step4"]').removeClass('label-default').addClass('label-danger');
        $('.buttonSellAccept').hide();
        $('.buttonCancel').hide();
        
        // Добавляю историю сделок
        addHistory('{$str6}');
        
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str6}'));
    }
}
JS
            );
        } else if ($screen_id == Assessment::SCREEN_DEAL) {

            switch ($Deal->status) {
                case Deal::STATUS_OPEN:
                    $isShowButtonCancel = true;
                    $StateStatus1 = 0;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, ожидайте подтверждения');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_BLOCK:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = true;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_SENDED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_RECEIVED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = true;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства получены');
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_CLOSE:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 1;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;

                    $a_offer = Assessment::findOne(['user_id' => $offer->user_id, 'deal_id' => $Deal->id]);
                    $a_deal = Assessment::findOne(['user_id' => $Deal->user_id, 'deal_id' => $Deal->id]);
                    if (is_null($a_deal) && !is_null($a_offer)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }
                    if (!is_null($a_deal) && is_null($a_offer)) {
                        $isShowBlockRate = true;
                        $isShowCharMessage = true;
                    }
                    if (!is_null($a_deal) && !is_null($a_offer)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }

                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
                    $isShowCounter = false;
                    break;
                case Deal::STATUS_HIDE:
                case Deal::STATUS_CANCEL:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 3;
                    $StateStatus2 = 3;
                    $StateStatus3 = 3;
                    $StateStatus4 = 3;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
                    $isShowCounter = false;
                    $isShowCharMessage = false;
                    break;
                case Deal::STATUS_AUDIT_WAIT:
                case Deal::STATUS_AUDIT_ACCEPTED:
                case Deal::STATUS_AUDIT_FINISH:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 2;
                    $StateStatus2 = 2;
                    $StateStatus3 = 2;
                    $StateStatus4 = 2;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = 'Сделка отменена';
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
            }

            $str2 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
            $str3 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
            $str4 = Yii::t('c.XE1gntn5b0', 'Средства получены');
            $str5 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
            $str6 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
            $this->registerJs(<<<JS
            
function switchInformer(status) {
    if (status == 1) {
        document.getElementById('label-status').textContent = "{$str1059}";
        $('.buttonInviteArbitrator').hide();
        socket.emit('deal-open', $Deal->id, $offer->user_id, status);
    }   
    else if (status == 2) {
        document.getElementById('label-status').textContent = "{$str2}";
        $('.buttonSellCancel').hide();
        document.getElementsByClassName('buttonSellMoneySended')[0].style.display = "";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();
        
        // Добавляю историю сделок
        addHistory('{$str2}');

        // Скрываю кнопку отменить сделку
        $('.buttonCancel').hide();
    }
    // STATUS_MONEY_SENDED
    else if (status == 3) {
        document.getElementById('label-status').textContent = "{$str3}";
        document.getElementsByClassName('buttonSellMoneySended')[0].style.display = "none";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');   
        $('.buttonInviteArbitrator').show();

        // Добавляю историю сделок
        addHistory('{$str3}');
    }    
    // STATUS_MONEY_RECEIVED
    else if (status == 4) {
        document.getElementById('label-status').textContent = "{$str4}"
        $('.js-block-rank').show();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');             
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');    
        $('.buttonInviteArbitrator').show();
        
        // Добавляю историю сделок
        addHistory('{$str4}');
                
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str5}'));
    } 
    else if (status == 5) {
        document.getElementById('label-status').textContent = "{$str5}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');             
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');             
        $('.buttonInviteArbitrator').hide();
        
        // Добавляю историю сделок
        addHistory('{$str703}: {$partner_user_address}');
    } 
    else if (status == 10) {
        document.getElementById('label-status').textContent = "{$str6}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step4"]').removeClass('label-default').addClass('label-danger');
        $('.buttonInviteArbitrator').show();
        $('.buttonSellAccept').hide();
        $('.buttonCancel').hide();
        
        // Добавляю историю сделок
        addHistory('{$str6}');
        
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str6}'));
    }
}
JS
            );
        }
    }
    // если Я (предложение) покупаю
    else if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_BUY) {
        if ($screen_id == Assessment::SCREEN_OFFER) {

            switch ($Deal->status) {
                case Deal::STATUS_OPEN:
                    $isShowButtonCancel = true;
                    $StateStatus1 = 0;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = true;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, подтвердите сделку');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_BLOCK:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = true;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_SENDED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_RECEIVED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = true;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства получены');
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_CLOSE:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 1;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;

                    $a_offer = Assessment::findOne(['user_id' => $offer->user_id, 'deal_id' => $Deal->id]);
                    $a_deal = Assessment::findOne(['user_id' => $Deal->user_id, 'deal_id' => $Deal->id]);
                    if (is_null($a_offer) && !is_null($a_deal)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }
                    if (!is_null($a_offer) && is_null($a_deal)) {
                        $isShowBlockRate = true;
                        $isShowCharMessage = true;
                    }
                    if (!is_null($a_offer) && !is_null($a_deal)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }

                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
                    $isShowCounter = false;
                    break;
                case Deal::STATUS_HIDE:
                case Deal::STATUS_CANCEL:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 3;
                    $StateStatus2 = 3;
                    $StateStatus3 = 3;
                    $StateStatus4 = 3;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
                    $isShowCounter = false;
                    $isShowCharMessage = false;
                    break;
                case Deal::STATUS_AUDIT_WAIT:
                case Deal::STATUS_AUDIT_ACCEPTED:
                case Deal::STATUS_AUDIT_FINISH:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 2;
                    $StateStatus2 = 2;
                    $StateStatus3 = 2;
                    $StateStatus4 = 2;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = 'Сделка отменена';
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
            }
            $str1 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, подтвердите сделку');
            $str2 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
            $str3 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
            $str4 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
            $str5 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
            $str6 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
            $this->registerJs(<<<JS
            
function switchInformer(status) {
    if (status == 1) {
        document.getElementById('label-status').textContent = "{$str1}";
        document.getElementsByClassName('buttonBuyAccept')[0].style.display = "";
        $('.buttonInviteArbitrator').hide();
    }
    else if (status == 2) {
        document.getElementById('label-status').textContent = "{$str2}";
        document.getElementsByClassName('buttonBuyAccept')[0].style.display = "none";
        document.getElementsByClassName('buttonBuyMoneySended')[0].style.display = "";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();

        // Добавляю историю сделок
        addHistory('{$str2}');
    }
    else if (status == 3) {
        document.getElementById('label-status').textContent = "{$str3}";
        document.getElementsByClassName('buttonBuyMoneySended')[0].style.display = "none";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();

        // Добавляю историю сделок
        addHistory('{$str3}');
        
        // Скрываю кнопку отменить сделку
        $('.buttonCancel').hide();
    }
    else if (status == 4) {
        document.getElementById('label-status').textContent = "{$str4}"
        $('.js-block-rank').show();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();
        
        // Добавляю историю сделок
        addHistory('{$str4}');
                
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str5}'));
    }
    else if (status == 5) {
        document.getElementById('label-status').textContent = "{$str5}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();
        
        // Добавляю историю сделок
        addHistory('{$str703}: {$partner_user_address}');
    }
    else if (status == 10) {
        document.getElementById('label-status').textContent = "{$str6}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step4"]').removeClass('label-default').addClass('label-danger');
        $('.buttonInviteArbitrator').show();
        $('.buttonBuyAccept').hide();
        $('.buttonCancel').hide();
        
        // Добавляю историю сделок
        addHistory('{$str6}');
        
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str6}'));
    }
}
JS
            );
        } else if ($screen_id == Assessment::SCREEN_DEAL) {

            switch ($Deal->status) {
                case Deal::STATUS_OPEN:
                    $isShowButtonCancel = true;
                    $StateStatus1 = 0;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка открыта, ожидайте подтверждения');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_BLOCK:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 0;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_SENDED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 0;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = true;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = true;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
                    $isShowCounter = true;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_MONEY_RECEIVED:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 0;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = true;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Средства получены');
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
                case Deal::STATUS_CLOSE:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 1;
                    $StateStatus2 = 1;
                    $StateStatus3 = 1;
                    $StateStatus4 = 1;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;

                    $a_offer = Assessment::findOne(['user_id' => $offer->user_id, 'deal_id' => $Deal->id]);
                    $a_deal = Assessment::findOne(['user_id' => $Deal->user_id, 'deal_id' => $Deal->id]);
                    if (is_null($a_deal) && !is_null($a_offer)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }
                    if (!is_null($a_deal) && is_null($a_offer)) {
                        $isShowBlockRate = true;
                        $isShowCharMessage = true;
                    }
                    if (!is_null($a_deal) && !is_null($a_offer)) {
                        $isShowBlockRate = false;
                        $isShowCharMessage = false;
                    }

                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
                    $isShowCounter = false;
                    break;
                case Deal::STATUS_HIDE:
                case Deal::STATUS_CANCEL:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 3;
                    $StateStatus2 = 3;
                    $StateStatus3 = 3;
                    $StateStatus4 = 3;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
                    $isShowCounter = false;
                    $isShowCharMessage = false;
                    break;
                case Deal::STATUS_AUDIT_WAIT:
                case Deal::STATUS_AUDIT_ACCEPTED:
                case Deal::STATUS_AUDIT_FINISH:
                    $isShowButtonCancel = false;
                    $StateStatus1 = 2;
                    $StateStatus2 = 2;
                    $StateStatus3 = 2;
                    $StateStatus4 = 2;
                    $isShowButtonSellAccept = false;
                    $isShowButtonSellMoneySend = false;
                    $isShowButtonSellMoneyRecived = false;
                    $isShowButtonBuyAccept = false;
                    $isShowButtonBuyMoneySend = false;
                    $isShowButtonBuyMoneyRecived = false;
                    $isShowBlockRate = false;
                    $isShowButtonArbitr = false;
                    $status1 = 'Сделка отменена';
                    $isShowCounter = false;
                    $isShowCharMessage = true;
                    break;
            }
            $str2 = Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и монеты заблокированы');
            $str3 = Yii::t('c.XE1gntn5b0', 'Средства переведены');
            $str4 = Yii::t('c.XE1gntn5b0', 'Средства получены');
            $str5 = Yii::t('c.XE1gntn5b0', 'Сделка завершена');
            $str6 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
            $this->registerJs(<<<JS
            
function switchInformer(status) {
    if (status == 1) {
        document.getElementById('label-status').textContent = "{$str1059}";
        $('.buttonInviteArbitrator').hide();
        socket.emit('deal-open', $Deal->id, $offer->user_id, status);
    }   
    else if (status == 2) {
        document.getElementById('label-status').textContent = "{$str2}";
        $('.buttonBuyCancel').hide();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();

        // Добавляю историю сделок
        addHistory('{$str2}');

        // Скрываю кнопку отменить сделку
        $('.buttonCancel').hide();
    }
    else if (status == 3) {
        document.getElementById('label-status').textContent = "{$str3}";
        $('.js-buttonBuyMoneyReceived').show();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');
        $('.buttonInviteArbitrator').show();

        // Добавляю историю сделок
        addHistory('{$str3}');
    }         
    else if (status == 4) {
        document.getElementById('label-status').textContent = "{$str4}"
        $('.js-buttonBuyMoneyReceived').hide();
        $('.js-block-rank').show();
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');             
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');    
        $('.buttonInviteArbitrator').show();
        
        // Добавляю историю сделок
        addHistory('{$str4}');
        
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str5}'));
    } 
    else if (status == 5) {
        document.getElementById('label-status').textContent = "{$str5}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-success');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-success');             
        $('[data-target="step3"]').removeClass('label-default').addClass('label-success');             
        $('.buttonInviteArbitrator').show();
        
        // Добавляю историю сделок
        addHistory('{$str703}: {$partner_user_address}');
    } 
    else if (status == 10) {
        document.getElementById('label-status').textContent = "{$str6}";
        $('[data-target="step1"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step2"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step3"]').removeClass('label-default').addClass('label-danger');
        $('[data-target="step4"]').removeClass('label-default').addClass('label-danger');
        $('.buttonInviteArbitrator').show();
        $('.buttonBuyAccept').hide();
        $('.buttonCancel').hide();
        
        // Добавляю историю сделок
        addHistory('{$str6}');
        
        // Вместо счетчика вывести надпись
        $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str6}'));
    }
}
JS
            );
        }
    }
}

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>

            <!-- Large modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <?php
                            $id = 23;
                            $page = \common\models\exchange\Page::findOne($id);

                            $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                            if (is_null($pageLang)) {
                                $name = $page->name;
                                $content = $page->content;
                            } else {
                                $name = $pageLang->name;
                                $content = $pageLang->content;
                            }
                            ?>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?= $name ?></h4>
                        </div>
                        <div class="modal-body">
                            <?= $content ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
            );
            ?>
        </h1>
    </div>

    <?= $this->render('../cabinet/_menu3') ?>

    <div class="row">
        <div class="col-lg-6">
            <?php $offerPay = \common\models\exchange\OfferPay::findOne($offer->offer_pay_id); ?>
            <?php
            $currency = Currency::findOne($offer['currency_id']);
            $link20200622 = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $offer['currency_id']]);
            $c20200622p = \common\models\piramida\Currency::findOne($link20200622->currency_int_id);
            ?>
            <?php $currencyIO = \common\models\CurrencyIO::findOne($offer['currency_io']); ?>
            <?php $currencyIO_object = \common\models\piramida\Currency::findOne($currencyIO->currency_int_id) ?>

            <?php $i = ($screen_id == Assessment::SCREEN_DEAL) ? $Deal->user_id : $offer->user_id ?>
            <?php $iObject = \common\models\UserAvatar::findOne($i); ?>
            <h2 ><?= \Yii::t('c.XE1gntn5b0', 'Я') ?> (<?= $iObject->getAddress() ?>) <?= (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL) ? (($Deal->user_id == Yii::$app->user->id) ? Yii::t('c.XE1gntn5b0', 'покупаю') : Yii::t('c.XE1gntn5b0', 'продаю')) : (($Deal->user_id == Yii::$app->user->id) ? Yii::t('c.XE1gntn5b0', 'продаю') : Yii::t('c.XE1gntn5b0', 'покупаю')))?> <?= $currencyIO_object->code ?> <?= \Yii::t('c.XE1gntn5b0', 'за') ?>  <?= $offerPay->code ?></h2>

            <h3 style="margin-top: 30px;"><?= \Yii::t('c.XE1gntn5b0', 'Время, отведённое на сделку') ?>.</h3>
            <ul class="countdown" style="text-align: left;">
                <?php if ($isShowCounter) { ?>
                    <li>
                        <span class="days">00</span>
                        <p class="days_ref"><?= \Yii::t('c.XE1gntn5b0', 'дней') ?></p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="hours">00</span>
                        <p class="hours_ref"><?= \Yii::t('c.XE1gntn5b0', 'часов') ?></p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="minutes">00</span>
                        <p class="minutes_ref"><?= \Yii::t('c.XE1gntn5b0', 'минут') ?></p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="seconds">00</span>
                        <p class="seconds_ref"><?= \Yii::t('c.XE1gntn5b0', 'секунд') ?></p>
                    </li>
                <?php } else { ?>
                    <p class="alert alert-warning"><?= \Yii::t('c.XE1gntn5b0', 'Сделка завершена') ?>.</p>
                <?php } ?>
            </ul>

            <h3 style="margin-top: 40px;"><?= \Yii::t('c.XE1gntn5b0', 'Курс') ?>: 1 <?= $currencyIO_object->code ?> = <?= Yii::$app->formatter->asDecimal(bcdiv($Deal->price, bcpow(10, $c20200622p->decimals), $c20200622p->decimals_view), $c20200622p->decimals_view) ?> <?= $offerPay->code ?></h3>
            <?php
            $cLink = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $Deal->currency_id]);
            $cDeal = \common\models\piramida\Currency::findOne($cLink->currency_int_id);
            ?>

            <p><?= \Yii::t('c.XE1gntn5b0', 'Объем') ?>: <?= Yii::$app->formatter->asDecimal(bcdiv($Deal->volume, bcpow(10, $cDeal->decimals), $cDeal->decimals_view), $cDeal->decimals_view) ?> <?= $offerPay->code ?></p>


            <p><?= \Yii::t('c.XE1gntn5b0', 'Объем монет') ?>: <?= Yii::$app->formatter->asDecimal(bcdiv($Deal->volume_vvb, bcpow(10, $currencyIO_object->decimals), $currencyIO_object->decimals), $currencyIO_object->decimals_view) ?> <?= $currencyIO_object->code ?></p>
            <?php
$str681 = Yii::t('c.XE1gntn5b0', 'Внимание рекомендуемое время на сделку истекло. Рейтинг не будет засчитан');
            $this->registerJs(<<<JS
var d1 = new Date();
d1.setTime({$Deal->created_at}000 + 60 * 60 * 72 * 1000);
var y = d1.getFullYear();
var m = d1.getMonth() + 1;
if (m < 10) m = '0' + m;
var d = d1.getDate();
var h = d1.getHours();
if (h < 10) h = '0' + h;
var i = d1.getMinutes();
if (i < 10) i = '0' + i;
var s = d1.getSeconds();
if (s < 10) s = '0' + s;
$('.countdown').downCount({
    date: m + '/' + d + '/' + y + ' ' + h + ':' + i + ':' + s,
    offset: -(d1.getTimezoneOffset() / 60)
}, function () {
    $('.countdown').html($('<p>').html('{$str681}.'));
});
JS
            );
            ?>


            <p class="label-p"><?= \Yii::t('c.XE1gntn5b0', 'Начало сделки') ?>: <?= Yii::$app->formatter->asDatetime($Deal->created_at) . ' ' . '(' . \cs\services\DatePeriod::back($Deal->created_at) . ')' ?></p>
            <?php
            if ($screen_id == Assessment::SCREEN_DEAL) {
                $text = Yii::t('c.usGELYsA0a', Deal::$statusList2[$screen_id][$Deal->status]);
            } else {
                $text = Yii::t('c.l3dvAPlALc', Deal::$statusList2[$screen_id][$Deal->status]);
            }
            ?>
            <p class="label-p"><?= \Yii::t('c.XE1gntn5b0', 'Статус') ?>: <span class="label label-info" id="label-status"><?= $text ?></span></p>
        </div>
        <div class="col-lg-6">
            <?php
            if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL) {
                if ($screen_id == Assessment::SCREEN_DEAL) {
                    $header_partner = Yii::t('c.XE1gntn5b0', 'О продавце');
                } else {
                    $header_partner = Yii::t('c.XE1gntn5b0', 'О покупателе');
                }
            } else {
                if ($screen_id == Assessment::SCREEN_DEAL) {
                    $header_partner = Yii::t('c.XE1gntn5b0', 'О покупателе');
                } else {
                    $header_partner = Yii::t('c.XE1gntn5b0', 'О продавце');
                }
            }
            ?>
            <h2 class="text-center"><?= $header_partner ?></h2>

            <div class="row">
                <div class="col-lg-6">
                    <p class="text-right"><img src="<?= $partner_user->getAvatar() ?>" width="80" class="img-circle"></p>
                    <?php
                    $ids = \common\models\Config::get('trusted-users');
                    if (\cs\Application::isEmpty($ids)) $ids = [];
                    else $ids = \yii\helpers\Json::decode($ids);
                    ?>
                    <?php if (in_array($user->id, $ids) && $offer['currency_io'] == 12) { ?>
                        <img src="/images/trust2.png" width="150" data-toggle="tooltip" title="Доверенный аккаунт">
                    <?php } ?>
                    <p class="text-right label-p"><?= $partner_user->getName2() ?></p>
                    <p class="text-right label-p"><?= $partner_user->getAddress() ?></p>


                    <p style="text-align: center">
                        <?php
                        $str686 = Yii::t('c.XE1gntn5b0', 'Арбитр успешно приглашен');
                        $str687 = Yii::t('c.XE1gntn5b0', 'Приглашен арбитр');
                        $this->registerJs(<<<JS
$('.buttonInviteArbitrator').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/invite-arbitrator' + '?id=' + {$Deal->id},
        success: function(ret) {
            socket.emit('need-arbitr', {$Deal->id}, {$user_id}, {$partner_user_id} );
            
            var noty = new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: '{$str686}'
            });
        
            noty.show();
            
            // статусы закрасить в оранжевый
            $('.c-iconStep span').removeClass('label-success').removeClass('label-default').addClass('label-warning');
            
            // Скрыть кнопки
            $('.buttonSellMoneySended').hide();
            $('.js-buttonSellMoneyReceived').hide();
            $('.buttonBuyMoneySended').hide();
            $('.js-buttonBuyMoneyReceived').hide();
            $('.buttonInviteArbitrator').hide();

            // Вместо счетчика вывести надпись
            $('.countdown').html($('<p>', {class: 'alert alert-warning'}).html('{$str687}'));
            
            // Вместо счетчика вывести надпись
            $('#label-status').removeClass('label-info').addClass('label-warning').html('{$str687}');
        }
    });
});
JS
                        ) ?>
                        <button class="btn buttonInviteArbitrator" data-id="<?= $Deal->id ?>" style="display: <?= $isShowButtonArbitr ? 'block' : 'none' ?>"><?= \Yii::t('c.XE1gntn5b0', 'Пригласить арбитра') ?></button>
                    </p>

                </div>
                <div class="col-lg-6">
                    <?php
                    // вывожу рейтинги
                    $r_neg = $partner_user->assessment_negative;
                    $r_pos = $partner_user->assessment_positive;

                    $html_r_neg = Html::tag('span', $r_neg, ['class' => 'label label-danger', 'title' => Yii::t('c.XE1gntn5b0', 'Негативных оценок'), 'data' => ['toggle' => 'tooltip']]);
                    $html_r_pos = Html::tag('span', $r_pos, ['class' => 'label label-success', 'title' => Yii::t('c.XE1gntn5b0', 'Позитивных оценок'), 'data' => ['toggle' => 'tooltip']]);
                    ?>
                    <p class="label-p"><?= $html_r_neg . $html_r_pos ?></p>

                    <p class="label-p"><?= \Yii::t('c.XE1gntn5b0', 'Сделок всего в которых участвовал') ?>: <?= $partner_user->deals_count_all ?></p>
                    <p class="label-p"><?= \Yii::t('c.XE1gntn5b0', 'Сделок по всем своим предложениям') ?>: <?= $partner_user->deals_count_offer ?></p>

                    <p class="label-p"><?= \Yii::t('c.XE1gntn5b0', 'Среднее время сделки') ?>: <?php
                        $c = \common\models\exchange\Deal::find()
                            ->innerJoin('offer', 'offer.id = deal.offer_id')
                            ->where(['offer.user_id' => $partner_user->id])
                            ->andWhere([
                                'deal.status' => [
                                    \common\models\exchange\Deal::STATUS_CLOSE,
                                    \common\models\exchange\Deal::STATUS_MONEY_RECEIVED,
                                ],
                            ])
                            ->andWhere(['not', ['deal.time_finish' => null]])
                            ->select('avg(deal.time_finish - deal.time_accepted)')
                            ->scalar();
                        if (is_null($c)) {
                            echo '';
                        } else {
                            $c = (int)$c;
                            $min = (int)($c / 60);
                            $sec = $c - ($min * 60);

                            echo $min . ' ' . Yii::t('c.XE1gntn5b0', 'мин') . ' ' . $sec . ' ' . Yii::t('c.XE1gntn5b0', 'сек');
                        }

                        ?></p>

                    <?php if ($Deal->status == Deal::STATUS_OPEN) { ?>
                        <?php

                        $str674 = Yii::t('c.XE1gntn5b0', 'Сделка отменена');
                        $this->registerJs(<<<JS
$('.buttonCancel').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/cancel' + '?id=' + {$Deal->id},
        success: function(ret) {
            socket.emit('buy-cancel', {$Deal->id}, {$offer->user_id}, 10);
            switchInformer(10);
            
            // Добавляю историю сделок
            addHistory('{$str674}');
        },
        errorScript: function (ret) {
            if (ret.id == 403) {
                var noty = new Noty({
                    timeout: 5000,
                    theme: 'relax',
                    type: 'warning',
                    layout: 'bottomLeft',
                    text: ret.data
                });
            
                noty.show();
            }
        }
    });
})
JS
                        );

                        ?>
                        <p class="text-center"><button style="float: right; display: <?= $isShowButtonCancel ? 'inline' : 'none' ?>" class="btn btn-danger buttonCancel" data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Отменить сделку') ?></button></p>
                    <?php } ?>
                </div>
            </div>
            <p class="text-right"><img src="/images/stopwatch-transparen.gif" width="50">

        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">




            <h2 style="font-size: 30px;color: #750f0b;">
                <i class="fas fa-tasks" style="color: #750f0b;font-size: 20px;padding-right: 10px;" aria-hidden="true"></i>
                <?= \Yii::t('c.XE1gntn5b0', 'Условия сделки') ?></h2>
            <p style="padding-bottom: 100px; font-size: 23px;"><?= Html::encode($offer->condition) ?></p>




            <?php if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL) { ?>
                <?php if ($offer->user_id == Yii::$app->user->id) { ?>
                    <?php
                    $str698 = Yii::t('c.XE1gntn5b0', 'Недостаточно монет для блокировки, сделка не может быть продолжена');
                    $this->registerJs(<<<JS
$('.buttonSellAccept').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/sell-open-accept' + '?id=' + {$Deal->id},
        success: function(ret) {
            socket.emit('sell-open-accept', {$Deal->id}, {$Deal->user_id}, 2);
            switchInformer(2);
        },
        errorScript: function(ret) {
            if (ret.id == 104) {
                var noty = new Noty({
                    timeout: 5000,
                    theme: 'relax',
                    type: 'warning',
                    layout: 'bottomLeft',
                    text: '{$str698}'
                });
            
                noty.show();
            }
        }
   });
})
JS
                    ) ?>

                    <p style="text-align: center">
                        <button class="btn btn-default buttonSellAccept" <?= ($isShowButtonSellAccept)? '' : 'style="display: none;margin-top: 20px;margin-bottom: 10px;"' ?> data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Принять сделку') ?></button>
                    </p>

                        <?php $this->registerJs(<<<JS
$('.buttonSellMoneyReceived').click(function(e) {
    if (confirm('Вы уверены, что видите поступившие средства?')) {
        ajaxJson({
            url: '/cabinet-exchange/sell-money-received' + '?id=' + {$Deal->id},
            success: function(ret) {
                socket.emit('sell-money-received', {$Deal->id}, {$Deal->user_id}, 4);
                switchInformer(4);
            }
        });
    }
});
JS
                ) ?>
                    <div class="js-buttonSellMoneyReceived" <?= ($isShowButtonSellMoneyRecived)? '' : 'style="display: none;"' ?>>
                        <p class="alert alert-danger"><?= \Yii::t('c.XE1gntn5b0', 'Внимание! Нажмите кнопку “Средства получены” только в том случае, если на своем счете в банке. вы уже видите поступившие деньги.') ?></p>
                        <p style="text-align: center">
                            <button class="btn btn-default buttonSellMoneyReceived" data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Средства получены') ?></button>
                        </p>
                    </div>
                <?php } ?>



                <?php $this->registerJs(<<<JS
    $('.buttonSellMoneySended').click(function(e) {
        ajaxJson({
            url: '/cabinet-exchange/sell-money-sended' + '?id=' + {$Deal->id},
            success: function(ret) {
                socket.emit('sell-money-sended', {$Deal->id}, {$offer->user_id}, 3);
                switchInformer(3);
            }
        });
    })
JS
            ) ?>

            <p style="text-align: center">
                <button class="btn buttonSellMoneySended" <?= ($isShowButtonSellMoneySend)? '' : 'style="display: none;"' ?> data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Средства отправлены') ?></button>
            </p>
            <?php } ?>


            <?php if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_BUY) { ?>
                <?php if ($offer->user_id == Yii::$app->user->id) {?>
                    <?php
                    $str701 = Yii::t('c.XE1gntn5b0', 'Недостаточно монет у продавца, сделка не может быть продолжена');
                    $this->registerJs(<<<JS
    $('.buttonBuyAccept').click(function(e) {
        ajaxJson({
            url: '/cabinet-exchange/buy-open-accept' + '?id=' + {$Deal->id},
            success: function(ret) {
                // Отправляю сообщение другому пользователю
                socket.emit('buy-open-accept', {$Deal->id}, {$Deal->user_id}, 2);

                // Меняю отображение на странице
                switchInformer(2);
            },
            errorScript: function(ret) {
                if (ret.id == 103) {
                    var noty = new Noty({
                        timeout: 5000,
                        theme: 'relax',
                        type: 'warning',
                        layout: 'bottomLeft',
                        text: '{$str701}'
                    });
                
                    noty.show();
                }
            }
        });
    })
JS
                    ) ?>

                    <button class="btn btn-default buttonBuyAccept" <?= ($isShowButtonBuyAccept)? '' : 'style="display: none;"' ?> data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Принять сделку') ?></button>
                    <?php $this->registerJs(<<<JS
$('.buttonBuyMoneySended').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/buy-money-sended' + '?id=' + {$Deal->id},
        success: function(ret) {
            socket.emit('buy-money-sended', {$Deal->id}, {$Deal->user_id}, 3 );
            switchInformer(3);
        }
    });
})
JS
                ) ?>
                    <button class="btn btn-default buttonBuyMoneySended" <?= ($isShowButtonBuyMoneySend)? '' : 'style="display: none;"' ?> data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Средства отправлены') ?></button>
                <?php } ?>


                <?php if ($Deal->user_id == Yii::$app->user->id) { ?>
                    <?php $this->registerJs(<<<JS
$('.buttonBuyMoneyReceived').click(function(e) {
    if (confirm('Вы уверены, что видите поступившие средства?')) {
        ajaxJson({
            url: '/cabinet-exchange/buy-money-received' + '?id=' + {$Deal->id},
            success: function(ret) {
                socket.emit('buy-money-received', {$Deal->id}, {$offer->user_id}, 4 );
                switchInformer(4);
            }
        });
    }
});
JS
                    ) ?>


                    <div class="js-buttonBuyMoneyReceived" <?= ($isShowButtonBuyMoneyRecived)? '' : 'style="display: none;"' ?>>
                        <p class="alert alert-danger"><?= \Yii::t('c.XE1gntn5b0', 'Внимание! Нажмите кнопку “Средства получены” только в том случае, если на своем счете в банке. вы уже видите поступившие деньги.') ?></p>
                        <p style="text-align: center">
                            <button class="btn btn-default buttonBuyMoneyReceived" data-id="<?= $Deal->id ?>"><?= \Yii::t('c.XE1gntn5b0', 'Средства получены') ?></button>
                        </p>
                    </div>

                <?php } ?>
            <?php } ?>

            <div class="row row-p">
                <?php

                if ($StateStatus1 == 0) $StateStatus1Color = 'default';
                if ($StateStatus1 == 1) $StateStatus1Color = 'success';
                if ($StateStatus1 == 2) $StateStatus1Color = 'warning';
                if ($StateStatus1 == 3) $StateStatus1Color = 'danger';

                if ($StateStatus2 == 0) $StateStatus2Color = 'default';
                if ($StateStatus2 == 1) $StateStatus2Color = 'success';
                if ($StateStatus2 == 2) $StateStatus2Color = 'warning';
                if ($StateStatus2 == 3) $StateStatus2Color = 'danger';

                if ($StateStatus3 == 0) $StateStatus3Color = 'default';
                if ($StateStatus3 == 1) $StateStatus3Color = 'success';
                if ($StateStatus3 == 2) $StateStatus3Color = 'warning';
                if ($StateStatus3 == 3) $StateStatus3Color = 'danger';

                if ($StateStatus4 == 0) $StateStatus4Color = 'default';
                if ($StateStatus4 == 1) $StateStatus4Color = 'success';
                if ($StateStatus4 == 2) $StateStatus4Color = 'warning';
                if ($StateStatus4 == 3) $StateStatus4Color = 'danger';

                ?>
                <style>
                    .c-iconStep {
                        font-size: 300%;
                    }
                </style>
                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step1" class="label label-<?= $StateStatus1Color ?>"><span class="fa fa-lock"></span></span></p>
                    <p class="text-center"><?= \Yii::t('c.XE1gntn5b0', 'Сделка подтверждена и cредства заблокированы') ?></p>
                </div>

                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step2" class="label label-<?= $StateStatus2Color ?>"><span class="fa fa-money-bill-alt"></span></span></p>
                    <p class="text-center"><?= \Yii::t('c.XE1gntn5b0', 'Средства отправлены') ?></p>
                </div>

                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step3" class="label label-<?= $StateStatus3Color ?>"><span class="fa fa-unlock"></span></span></p>
                    <p class="text-center"><?= \Yii::t('c.XE1gntn5b0', 'Средства получены') ?></p>
                </div>

                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step4" class="label label-<?= $StateStatus4Color ?>"><span class="glyphicon glyphicon-ok"></span></span></p>
                    <p class="text-center"><?= \Yii::t('c.XE1gntn5b0', 'Сделка завершена') ?></p>
                </div>
            </div>

            <?php
            $this->registerJs(<<<JS
$('.buttonAssessmentDown').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/set-assessment' + '?id=' + {$Deal->id},
        data: {
            id: {$Deal->id},
            value: -1,
            screen: {$screen_id}
        },
        success: function(ret) {
            $('#modalInfo').modal();
            $('[data-target="step4"]').removeClass('label-default').addClass('label-success');              
            $('.js-block-rank').hide();
            socket.emit('assessment-down', {$Deal->id}, {$partner_user_id}, 5 );
            
            // Добавляю историю сделок
            addHistory('{$str703}: {$user_address}' );
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                for (var key in ret.data) {
                    if (ret.data.hasOwnProperty(key)) {
                        var name = key;
                        var value = ret.data[key];
                        var noty = new Noty({
                            timeout: 5000,
                            theme: 'relax',
                            type: 'warning',
                            layout: 'bottomLeft',
                            text: value.join('<br>')
                        });
                    
                        noty.show();
                    }
                }
            }
        }
    });
});

$('.buttonAssessmentUp').click(function(e) {
    ajaxJson({
        url: '/cabinet-exchange/set-assessment' + '?id=' + {$Deal->id},
        data: {
            id: {$Deal->id},
            value: 1,
            screen: {$screen_id}
        },
        success: function(ret) {
            $('#modalInfo').modal();
            $('[data-target="step4"]').removeClass('label-default').addClass('label-success');              
            $('.js-block-rank').hide();
            socket.emit('assessment-up', {$Deal->id}, {$partner_user_id}, 5 );
            
            // Добавляю историю сделок
            addHistory('{$str703}: {$user_address}');
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                for (var key in ret.data) {
                    if (ret.data.hasOwnProperty(key)) {
                        var name = key;
                        var value = ret.data[key];
                        var noty = new Noty({
                            timeout: 5000,
                            theme: 'relax',
                            type: 'warning',
                            layout: 'bottomLeft',
                            text: value.join('<br>')
                        });
                    
                        noty.show();
                    }
                }
            }
        }
    });
});
JS
                ) ?>
            <div class="btn-group js-block-rank" role="group" aria-label="..." style="display: <?= $isShowBlockRate ? 'block' : 'none' ?>">
                <div class="row">
                    <p style="font-size: 20px;text-align: center;padding-top: 20px;"><?= \Yii::t('c.XE1gntn5b0', 'Дайте оценку сделке, чтобы ее завершить') ?></p>
                    <div class="col-lg-3 heartwrap" style="padding-right: 100px;margin-left: 80px;">
                        <i class="fas fa-thumbs-up buttonAssessmentUp" style="font-size: 25px; color: #5cb85c;margin-right: 15px;margin-top: 15px;padding-bottom: 15px;padding-left: 30px;"></i>
                        <p><?= \Yii::t('c.XE1gntn5b0', 'Положительно') ?></p>
                    </div>

                    <div class="col-lg-3 heartwrap">
                        <i class="fas fa-thumbs-down buttonAssessmentDown" style="font-size: 25px; color: #d9534f;margin-top: 15px;padding-bottom: 15px;padding-left: 30px;"></i>
                        <p><?= \Yii::t('c.XE1gntn5b0', 'Отрицательно') ?></p>
                    </div>

                </div>
            </div>








        </div>
        <div class="col-lg-6">
            <p class="alert alert-warning" style="font-weight: bold;"><?= \Yii::t('c.XE1gntn5b0', 'Security disclaimer Neiro-N: -!!!- Во избежании потери ваших средств, НИКОГДА не делайте перевод фиата до того, как ваш партнер примет сделку, и вы увидите изменение её статуса на "Сделка подтверждена и монеты заблокированы".') ?></p>

            <?= $this->render('@shop/views/cabinet-exchange/chat', [
                'room_id'         => $Deal->chat_room_id,
                'user_id'         => Yii::$app->user->id,
                'send_url'        => '/cabinet-exchange-chat/send?id=' . $Deal->id,
                'isSocketConnect' => true,
            ]) ?>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header"><?= \Yii::t('c.XE1gntn5b0', 'История сделки') ?></h2>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => \common\models\exchange\DealStatus::find()
                        ->where(['deal_id' => $Deal->id])
                    ,
                    'sort'  => ['defaultOrder' => ['created_at' => SORT_ASC]],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                    'id'    => 'history_table',
                ],
                'summary'      => '',
                'columns'      => [
                    'created_at:time:' . Yii::t('c.XE1gntn5b0', 'Время'),
                    [
                        'header'    => Yii::t('c.XE1gntn5b0', 'Комментарий'),
                        'attribute' => 'status',
                        'content'   => function ($item) {
                            if (!\cs\Application::isEmpty($item['comment'])) {
                                if (\yii\helpers\StringHelper::startsWith($item['comment'], '[')) {
                                    $data  = \yii\helpers\Json::decode($item['comment']);
                                    $data0 = \yii\helpers\ArrayHelper::getValue($data, 0);
                                    $data1 = \yii\helpers\ArrayHelper::getValue($data, 1, []);

                                    return Yii::t('c.38DjBe9FdT', $data0, $data1);
                                } else {

                                    return $item['comment'];
                                }
                            }
                            $d = Deal::findOne($item['deal_id']);
                            if ($d['user_id'] == Yii::$app->user->id) {
                                $type = \common\models\exchange\Assessment::SCREEN_DEAL;
                                $text = Yii::t('c.usGELYsA0a', \common\models\exchange\Deal::$statusList2[$type][$item['status']]);
                            } else {
                                $type = \common\models\exchange\Assessment::SCREEN_OFFER;
                                $text = Yii::t('c.l3dvAPlALc', \common\models\exchange\Deal::$statusList2[$type][$item['status']]);
                            }

                            return $text;
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.XE1gntn5b0', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.XE1gntn5b0', 'Оценка поставлена. Сделка успешно завершена!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.XE1gntn5b0', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>

