<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $ActiveDataProvider \yii\data\ActiveDataProvider */


$this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    var t1 = $(this);
    if (t1.data('status') == 11) {
        window.location = '/cabinet-exchange/deal-open' + '?' + 'id' + '=' + $(this).data('id');
    } else {
        window.location = '/cabinet-exchange/deal-action' + '?' + 'id' + '=' + $(this).data('id');
    }
});
JS
);
?>
<?= \yii\grid\GridView::widget([
    'dataProvider' => $ActiveDataProvider,
    'tableOptions' => [
        'class' => 'table table-striped table-hover table_exchange'
    ],
    'summary'      => '',
    'rowOptions'   => function ($item) {
        $data = [
            'data' => [
                'id'     => $item['id'],
                'status' => $item['status'],
            ],
            'role'  => 'button',
            'class' => 'rowTable',
        ];
        return $data;
    },
    'columns'      => [
        //'id',
        [
            'header'         => 'Id',
            'contentOptions' => ['aria-label' => 'Id'],
            'content'        => function ($item) {
                return $item['id'];
            },
        ],
        [
            'header'         => Yii::t('c.MHL9nEyCNY', 'Я инициатор?'),
            'contentOptions' => ['aria-label' => Yii::t('c.MHL9nEyCNY', 'Я инициатор?')],
            'content'        => function ($item) {
                if ($item['user_id'] == Yii::$app->user->id) {
                    return Html::tag('span', Yii::t('c.MHL9nEyCNY', 'Да'), ['class' => 'label label-success']);
                }

                return Html::tag('span', Yii::t('c.MHL9nEyCNY', 'Нет'), ['class' => 'label label-default']);
            },
        ],
        [
            'header'         => Yii::t('c.MHL9nEyCNY', 'Тип сделки'),
            'contentOptions' => ['aria-label' => Yii::t('c.MHL9nEyCNY', 'Тип сделки')],
            'content'        => function ($item) {
                // Если я автор сделки
                if ($item['user_id'] == Yii::$app->user->id) {
                    if ($item['type_id'] == \common\models\exchange\Deal::TYPE_ID_BUY) {
                        return Html::tag('span', Yii::t('c.MHL9nEyCNY', 'Я покупаю'), ['class' => 'label label-success']);
                    }
                    if ($item['type_id'] == \common\models\exchange\Deal::TYPE_ID_SELL) {
                        return Html::tag('span', Yii::t('c.MHL9nEyCNY', 'Я продаю'), ['class' => 'label label-danger']);
                    }
                } else {
                    // Если не я автор сделки, если я автор предложения
                    if ($item['type_id'] == \common\models\exchange\Deal::TYPE_ID_BUY) {
                        return Html::tag('span', Yii::t('c.MHL9nEyCNY', 'Я продаю'), ['class' => 'label label-danger']);
                    }
                    if ($item['type_id'] == \common\models\exchange\Deal::TYPE_ID_SELL) {
                        return Html::tag('span', Yii::t('c.MHL9nEyCNY', 'Я покупаю'), ['class' => 'label label-success']);
                    }
                }

                return '';
            },
        ],
        [
            'header'         => Yii::t('c.MHL9nEyCNY', 'Объем монет'),
            'headerOptions'  => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right', 'aria-label' => Yii::t('c.MHL9nEyCNY', 'Объем монет')],
            'content'        => function ($item) {
                $CurrencyIO = \common\models\CurrencyIO::findOne($item['currency_io']);
                $currency = \common\models\piramida\Currency::findOne($CurrencyIO->currency_int_id);
                $v = \yii\helpers\ArrayHelper::getValue($item, 'volume_vvb', 0);
                if ($v == 0) return '';

                return Yii::$app->formatter->asDecimal(bcdiv($v, bcpow(10, $currency->decimals), $currency->decimals_view), $currency->decimals_view) . Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);
            }
        ],
        [
            'header'         => Yii::t('c.MHL9nEyCNY', 'Стоимость'),
            'headerOptions'  => ['class' => 'text-right'],
            'contentOptions' => ['class' => 'text-right', 'aria-label' => Yii::t('c.MHL9nEyCNY', 'Стоимость')],

            'content' => function ($item) {
                $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $item['currency_id']]);
                $curInt = \common\models\piramida\Currency::findOne($link->currency_int_id);
                $price = $item['price'];
                $pricePrint = bcdiv($price, bcpow(10, $curInt->decimals), $curInt->decimals_view);
                $decimalsPrint = $curInt->decimals_view;

                $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                $offer_pay_id = $offer->offer_pay_id;
                $offerPay = \common\models\exchange\OfferPay::findOne($offer_pay_id);

                $c = Html::tag('span', $offerPay->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint) . $c;
            },
        ],
        [
            'header'         => Yii::t('c.MHL9nEyCNY', 'Статус'),
            'contentOptions' => ['aria-label' => Yii::t('c.MHL9nEyCNY', 'Статус')],
            'content'        => function ($item) {
                if (!isset(Deal::$statusList[$item['status']])) {
                    return '';
                }
                $statusClass = [
                    Deal::STATUS_CREATE         => 'default',
                    Deal::STATUS_OPEN           => 'success',
                    Deal::STATUS_BLOCK          => 'info',
                    Deal::STATUS_MONEY_SENDED   => 'success',
                    Deal::STATUS_MONEY_RECEIVED => 'info',
                    Deal::STATUS_CLOSE          => 'success',
                    Deal::STATUS_AUDIT_WAIT     => 'warning',
                    Deal::STATUS_AUDIT_ACCEPTED => 'warning',
                    Deal::STATUS_AUDIT_FINISH   => 'warning',
                    Deal::STATUS_HIDE           => 'danger',
                    Deal::STATUS_CANCEL         => 'danger',
                ];

                if ($item['user_id'] == Yii::$app->user->id) {
                    $type = \common\models\exchange\Assessment::SCREEN_DEAL;
                    $text = Yii::t('c.usGELYsA0a', Deal::$statusList2[$type][$item['status']]);
                } else {
                    $type = \common\models\exchange\Assessment::SCREEN_OFFER;
                    $text = Yii::t('c.l3dvAPlALc', Deal::$statusList2[$type][$item['status']]);
                }

                return Html::tag('span', $text, ['class' => 'label label-' . $statusClass[$item['status']]]);
            },
        ],
        [
            'header'         => Yii::t('c.MHL9nEyCNY', 'Создано'),
            'contentOptions' => ['aria-label' => Yii::t('c.MHL9nEyCNY', 'Создано')],
            'content'        => function ($item) {
                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                if ($v == 0) return '';

                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
            }
        ],
    ],
]) ?>