<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\piramida\Wallet;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\exchange\OfferEdit */


$this->title = Yii::t('c.svE9ShjLps', 'Изменить предложение');


$all = \common\models\CurrencyIO::find()->where(['is_io' => 1])->all();
$rows = [];
/** @var \common\models\CurrencyIO $cio */
foreach ($all as $cio) {
    $currencyInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);

    // получаю кошелек VVB
    $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id);

    /** @var Wallet $wallet */
    $wallet = $data['wallet'];
    $max = bcdiv($wallet->amount, bcpow(10, $currencyInt->decimals), $currencyInt->decimals_view);
    $rows[] = [
        'id'  => $cio->id,
        'max' => $max,
    ];
}
$rowsJson = \yii\helpers\Json::encode($rows);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>

            <!-- Large modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <?php
                            $id = 55;
                            $page = \common\models\exchange\Page::findOne($id);

                            $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                            if (is_null($pageLang)) {
                                $name = $page->name;
                                $content = $page->content;
                            } else {
                                $name = $pageLang->name;
                                $content = $pageLang->content;
                            }
                            ?>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?= $name ?></h4>
                        </div>
                        <div class="modal-body">
                            <?= $content ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $strMax = Yii::t('c.HWRG4KACAF', 'максимум до');
            $cEGOLD = Currency::findOne(['code' => 'EGOLD']);
            $priceEgold = $cEGOLD->kurs;

            $cNEIRON = Currency::findOne(['code' => 'NEIRO']);
            $cio = CurrencyIO::findOne(['currency_ext_id' => $cNEIRON->id]);
            $cNEIRONint = \common\models\piramida\Currency::findOne($cio->currency_int_id);
            $priceEgold = $cEGOLD->kurs;
            $decimalsNeiron = $cNEIRONint->decimals;
            $p = pow(10, $decimalsNeiron);
            $priceNEIRON = (int) ($cNEIRON->kurs * $p) / $p;

            $this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});

var cioList = {$rowsJson};

// устанавливаю куда выводить
var pMAX = $('.field-offeredit-volume_io_finish .help-block'); 
var pMAX2; 
if (pMAX.hasClass('help-block-error')) {
    pMAX2 = $('<p>', {class: 'help-block'});
    pMAX.parent().append(pMAX2);
} else {
    pMAX2 = pMAX;
}
function getMax(j, list) {
    var i;
    for(i=0; i < list.length; i++) {
        if (list[i].id == j) {
            return list[i].max;
        }
    }
    
    return '';
}
$('#offeredit-currency_io').on('change', function (e) {
    if ($('#offeredit-type_id').val() == 2) {
        pMAX2.html('{$strMax}' + ' ' + getMax($('#offeredit-currency_io').val(), cioList));
    } else {
        pMAX2.html('');
    }
    if ($(this).val() == 13) {
        $('#offeredit-price').val({$priceEgold});
        $('.js-price').collapse('hide');
        $('.js-price2').collapse('show');
        $('#egoldPrice').html({$priceEgold});
        $('#offeredit-offer_pay_id').val(1);
    } if ($(this).val() == 12) {
        $('#offeradd-price').val({$priceNEIRON});
        $('#offeradd-offer_pay_id').val(1);
        $('.js-price').collapse('show');
        $('.js-price2').collapse('hide');
    } else {
        $('#offeredit-price').val('');
        $('.js-price').collapse('show');
        $('.js-price2').collapse('hide');
    }
});

$('#offeredit-type_id').on('change', function (e) {
    if ($('#offeredit-type_id').val() == 2) {
        pMAX2.html('максимум до ' + getMax($('#offeredit-currency_io').val(), cioList));
    } else {
        pMAX2.html('');
    }
});

$('#offeredit-currency_io').change();
JS
            );
            ?>
        </h1>

    </div>
    <?= $this->render('../cabinet/_menu3') ?>

    <div class="col-lg-6 col-lg-offset-3" style="padding-top: 50px">

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function(ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-exchange/offer';
    }).modal();
}
JS

        ]); ?>

        <?= $form->field($model, 'type_id')
            ->dropDownList(
                [
                    \common\models\exchange\Offer::TYPE_ID_BUY  => Yii::t('c.HWRG4KACAF', 'Купить'),
                    \common\models\exchange\Offer::TYPE_ID_SELL => Yii::t('c.HWRG4KACAF', 'Продать'),
                ]
            ) ?>
        <?= $form->field($model, 'currency_io')->dropDownList(
            \yii\helpers\ArrayHelper::map(
                \common\models\CurrencyIO::find()
                    ->innerJoin('currency', 'currency.id = currency_io.currency_ext_id')
                    ->select(['currency_io.*'])
                    ->orderBy(['currency.sort_index' => SORT_ASC])
                    ->where(['currency_io.is_exchange' => 1])
                    ->all(),
                'id',
                'tab_title'
            )
        ) ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($model, 'volume_io_start') ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($model, 'volume_io_finish') ?>
            </div>
        </div>

        <div class="collapse in js-price">
        <div class="row">
            <div class="col-lg-8">
                <?= $form->field($model, 'price')->hint(Yii::t('c.HWRG4KACAF', 'дробное число пишется через точку')) ?>
            </div>
            <div class="col-lg-4">
                <?php
                $rows = \yii\helpers\ArrayHelper::map(
                    \common\models\exchange\OfferPay::find()
                        ->orderBy(['sort_index' => SORT_ASC])
                        ->all(),
                    'id',
                    'title'
                )
                ?>
                <?= $form->field($model, 'offer_pay_id')->dropDownList($rows) ?>
            </div>
        </div>
        </div>
        <div class="collapse js-price2">
            <div class="row">
                <div class="col-lg-8">
                    <p>Цена: <span id="egoldPrice"></span> RUB</p>
                </div>
            </div>
        </div>


        <div class="collapse in" id="div_pay_list">
            <?php

            $list = \common\models\exchange\PayMethod::find()->orderBy(['sort_index' => SORT_ASC])->select('id')->column();

            $listName1 = \common\models\PayMethodLang::find()
                ->select(['pay_method_lang.name', 'pay_method.id'])
                ->innerJoin('pay_method', 'pay_method.id = pay_method_lang.parent_id')
                ->where(['in', 'pay_method_lang.parent_id', $list])
                ->andWhere(['language' => Yii::$app->language])
                ->orderBy(['pay_method.sort_index' => SORT_ASC])
                ->all();

            $list2 = \yii\helpers\ArrayHelper::getColumn($listName1, 'id');
            $list3 = [];

            // Ищу что еще не переведено
            foreach ($list as $id) {
                if (!in_array($id, $list2)) {
                    $list3[] = $id;
                }
            }

            $listName2 = \common\models\exchange\PayMethod::find()->where(['in', 'id', $list3])->select(['name', 'id'])->orderBy(['sort_index' => SORT_ASC])->all();

            $listName = \yii\helpers\ArrayHelper::merge(
                $listName1,
                $listName2
            );

            ?>
            <?= $form->field($model, 'pay_list')
                ->checkboxList(
                    \yii\helpers\ArrayHelper::map(
                        $listName,
                        'id',
                        'name'
                    )
                ) ?>
        </div>


        <?= $form->field($model, 'condition')->textarea(['rows' => 10]) ?>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => Yii::t('c.FMg0mLqFR9', 'Обновить')]); ?>

    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
