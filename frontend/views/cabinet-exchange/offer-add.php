<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\piramida\Wallet;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \avatar\models\forms\exchange\OfferAdd */


$this->title = Yii::t('c.HWRG4KACAF', 'Добавить предложение');



$all = \common\models\CurrencyIO::find()->where(['is_io' => 1])->all();
$rows = [];
/** @var \common\models\CurrencyIO $cio */
foreach ($all as $cio) {
    $currencyInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);

    // получаю кошелек VVB
    $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id);

    /** @var Wallet $wallet */
    $wallet = $data['wallet'];
    $max = bcdiv($wallet->amount, bcpow(10, $currencyInt->decimals), $currencyInt->decimals_view);
    $rows[] = [
        'id'  => $cio->id,
        'max' => $max,
    ];
}
$rowsJson = \yii\helpers\Json::encode($rows);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>

            <!-- Large modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <?php
                            $id = 56;
                            $page = \common\models\exchange\Page::findOne($id);

                            $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                            if (is_null($pageLang)) {
                                $name = $page->name;
                                $content = $page->content;
                            } else {
                                $name = $pageLang->name;
                                $content = $pageLang->content;
                            }
                            ?>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?= $name ?></h4>
                        </div>
                        <div class="modal-body">
                            <?= $content ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $strMax = Yii::t('c.HWRG4KACAF', 'максимум до');
            $cEGOLD = Currency::findOne(['code' => 'EGOLD']);
            $priceEgold = $cEGOLD->kurs;

            // Платежные системы для NEIRON
            $priceNEIRON2 = [
                1,  // rub
                2,  // USD
                3,  // USDT (ERC-20)
                15, // USDT (TRC-20)
                4,  // ETH
                6,  // MARKET
                8,  // PZM
            ];
            $price_NEIRON_array = [];
            $rows2 = [];

            $cNEIRON = Currency::findOne(['code' => 'NEIRO']);
            foreach ($priceNEIRON2 as $p) {
                $OfferPay = \common\models\exchange\OfferPay::findOne($p);
                if (is_null($OfferPay)) \cs\services\VarDumper::dump($p);
                $currency_id = $OfferPay->currency_id;
                $c = Currency::findOne($currency_id);
                $cio = CurrencyIO::findOne(['currency_ext_id' => $c->id]);
                $cInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                $decimals = $cInt->decimals;
                $p1 = pow(10, $decimals);
                $price = $cNEIRON->price_usd / $c->price_usd;
                $price_NEIRON_array[$p] = (int) ($price * $p1) / $p1;
                $rows2[] = [$p, $price_NEIRON_array[$p]];
            }
            $rows2json = \yii\helpers\Json::encode($rows2);
            $priceNEIRON2json = \yii\helpers\Json::encode($priceNEIRON2);

            $cio = CurrencyIO::findOne(['currency_ext_id' => $cNEIRON->id]);
            $cNEIRONint = \common\models\piramida\Currency::findOne($cio->currency_int_id);
            $decimalsNeiron = $cNEIRONint->decimals;
            $p = pow(10, $decimalsNeiron);
            $priceNEIRON = (int) ($cNEIRON->kurs * $p) / $p;

            $this->registerJs(<<<JS
// при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});

var neironPrice = {$rows2json};
var neironOfferPay = {$priceNEIRON2json};
var cioList = {$rowsJson};

// устанавливаю куда выводить
var pMAX = $('.field-offeradd-volume_io_finish .help-block'); 
var pMAX2; 
if (pMAX.hasClass('help-block-error')) {
    pMAX2 = $('<p>', {class: 'help-block'});
    pMAX.parent().append(pMAX2);
} else {
    pMAX2 = pMAX;
}
function getMax(j, list) {
    var i;
    for(i=0; i < list.length; i++) {
        if (list[i].id == j) {
            return list[i].max;
        }
    }
    
    return '';
}
if ($('#offeradd-currency_io').val() == 13) { // EGOLD
    $('#offeradd-offer_pay_id').val(1);
    $('#offeradd-price').val({$priceEgold});
    $('.js-price').collapse('hide');
    $('.js-price2').collapse('show');
    $('#egoldPrice').html({$priceEgold});
}
$('#offeradd-currency_io').on('change', function (e) {
    if ($('#offeradd-type_id').val() == 2) {
        pMAX2.html('{$strMax}' + ' ' + getMax($('#offeradd-currency_io').val(), cioList));
    } else {
        pMAX2.html('');
    }
    if ($(this).val() == 13) { // EGOLD
        $('#offeradd-offer_pay_id').val(1);
        $('#offeradd-price').val({$priceEgold});
        $('.js-price').collapse('hide');
        $('.js-price2').collapse('show');
        $('#egoldPrice').html({$priceEgold});
    } if ($(this).val() == 12) {
        $('#offeradd-price').val({$priceNEIRON});
        $('#offeradd-offer_pay_id').val(1);
        $('.js-price').collapse('show');
        $('.js-price2').collapse('hide');
    } else {
        $('#offeradd-price').val('');
        $('.js-price').collapse('show');
        $('.js-price2').collapse('hide');
    }
});
$('#offeradd-type_id').on('change', function (e) {
    if ($('#offeradd-type_id').val() == 2) {
        pMAX2.html('{$strMax}' + ' ' + getMax($('#offeradd-currency_io').val(), cioList));
    } else {
        pMAX2.html('');
    }
});

function getPrice(p)
{
    var i;
    var OfferPay_id;
    var price;
    for (i = 0; i < neironPrice.length; i++) {
        OfferPay_id = neironPrice[i][0];
        price = neironPrice[i][1];
        if (OfferPay_id == p) {
            return price;
        }
    }
    
    return null;
}

$('#offeradd-offer_pay_id').on('change', function (e) {
    // если это NEIRON
    if ($('#offeradd-currency_io').val() == 12) {
        if ($.inArray(parseInt($(this).val()), neironOfferPay) >= 0) {
            $('#offeradd-price').val(getPrice($(this).val()));
        } 
    }
});
JS
            );
            ?>
        </h1>

    </div>
    <?= $this->render('../cabinet/_menu3') ?>

    <div class="col-lg-6 col-lg-offset-3">
        <?php if (Yii::$app->session->hasFlash('form')) : ?>

            <div class="alert alert-success">
                <?= \Yii::t('c.HWRG4KACAF', 'Успешно добавлено') ?>.
            </div>

            <P><a class="btn btn-info" href="/cabinet-exchange/offer"><?= \Yii::t('c.HWRG4KACAF', 'Мои предложения') ?></a></P>

        <?php else: ?>


            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data', 'style' => 'margin-top: 50px;' ],
            ]); ?>

            <?= $form->field($model, 'type_id')
                ->dropDownList(
                    [
                        \common\models\exchange\Offer::TYPE_ID_BUY  => Yii::t('c.HWRG4KACAF', 'Купить'),
                        \common\models\exchange\Offer::TYPE_ID_SELL => Yii::t('c.HWRG4KACAF', 'Продать'),
                    ]
                ) ?>
            <?= $form->field($model, 'currency_io')->dropDownList(
                \yii\helpers\ArrayHelper::map(
                    \common\models\CurrencyIO::find()
                        ->innerJoin('currency', 'currency.id = currency_io.currency_ext_id')
                        ->select(['currency_io.*'])
                        ->orderBy(['currency.sort_index' => SORT_ASC])
                        ->where(['currency_io.is_exchange' => 1])
                        ->all(),
                    'id',
                    'tab_title'
                )
            ) ?>

            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($model, 'volume_io_start') ?>
                </div>
                <div class="col-lg-6">
                    <?= $form->field($model, 'volume_io_finish') ?>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <?= $form->field($model, 'price_is_free')->radioList([
                        0 => 'Точная цена',
                        1 => 'Свободная цена (предложите вашу цену)',
                    ]) ?>
                </div>
                <?php
                $this->registerJs(<<<JS
$('[name="OfferAdd[price_is_free]"]').each(function (i,o) {
    if ($(o).attr('value') == 0) {
        $(o).prop('checked', 'checked');
    }
});
JS
);
                ?>
            </div>

            <div class="collapse in js-price">
                <div class="row">
                    <div class="col-lg-8">
                        <?= $form->field($model, 'price')->hint(Yii::t('c.HWRG4KACAF', 'дробное число пишется через точку')) ?>
                    </div>
                    <div class="col-lg-4">
                        <?php
                        $rows = \yii\helpers\ArrayHelper::map(
                            \common\models\exchange\OfferPay::find()
                                ->orderBy(['sort_index' => SORT_ASC])
                                ->all(),
                            'id',
                            'title'
                        )
                        ?>
                        <?= $form->field($model, 'offer_pay_id')->dropDownList($rows) ?>
                    </div>
                </div>
            </div>

            <div class="collapse js-price2">
                <div class="row">
                    <div class="col-lg-8">
                        <p><?= Yii::t('c.HWRG4KACAF', 'Цена') ?>: <span id="egoldPrice"></span> RUB</p>
                    </div>
                </div>
            </div>



            <div class="collapse in" id="div_pay_list">
                <?php

                $list = \common\models\exchange\PayMethod::find()->orderBy(['sort_index' => SORT_ASC])->select('id')->column();

                $listName1 = \common\models\PayMethodLang::find()
                    ->select(['pay_method_lang.name', 'pay_method.id'])
                    ->innerJoin('pay_method', 'pay_method.id = pay_method_lang.parent_id')
                    ->where(['in', 'pay_method_lang.parent_id', $list])
                    ->andWhere(['language' => Yii::$app->language])
                    ->orderBy(['pay_method.sort_index' => SORT_ASC])
                    ->all();

                $list2 = \yii\helpers\ArrayHelper::getColumn($listName1, 'id');
                $list3 = [];

                // Ищу что еще не переведено
                foreach ($list as $id) {
                    if (!in_array($id, $list2)) {
                        $list3[] = $id;
                    }
                }

                $listName2 = \common\models\exchange\PayMethod::find()->where(['in', 'id', $list3])->select(['name', 'id'])->orderBy(['sort_index' => SORT_ASC])->all();

                $listName = \yii\helpers\ArrayHelper::merge(
                    $listName1,
                    $listName2
                );

                ?>
                <?= $form->field($model, 'pay_list')
                    ->checkboxList(
                        \yii\helpers\ArrayHelper::map(
                            $listName,
                            'id',
                            'name'
                        )
                    ) ?>
            </div>




            <?= $form->field($model, 'condition')->textarea(['rows' => 10]) ?>


            <hr>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.HWRG4KACAF', 'Добавить'), [
                    'class' => 'btn btn-success',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>


</div>

