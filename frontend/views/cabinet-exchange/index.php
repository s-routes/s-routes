<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */


$this->title = Yii::t('c.XwLuZKjn6M', 'P2P предложения о покупке и продаже');

$currencyIO = \common\models\CurrencyIO::findOne(1);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-6">
            <p><a href="https://topmate.one/" target="_blank" class="btn btn-primaru" style="width:100%;background-color: #037394; color: #fff;">TOPMATE.ONE - ИНТЕРНЕТ-ШОПИНГ С ОПЛАТОЙ КРИПТОАКТИВАМИ</a> </p>
        </div>
        <div class="col-lg-6">
            <p><a href="https://elottery.cloud/" target="_blank" class="btn btn-primaru" style="width:100%;background-color: #67237F; color: #fff;">ELOTTERY.CLOUD - ONLINE-РОЗЫГРЫШИ ПРИЗОВ ЗА КРИПТОАКТИВЫ</a> </p>
        </div>
    </div>
    <div class="col-lg-12">
        <h1 class="page-header text-center">

            <?= $this->title ?>

            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
        <!-- Large modal -->

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <?php
                        $id = 16;
                        $page = \common\models\exchange\Page::findOne($id);

                        $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                        if (is_null($pageLang)) {
                            $name = $page->name;
                            $content = $page->content;
                        } else {
                            $name = $pageLang->name;
                            $content = $pageLang->content;
                        }
                        ?>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?= $name ?></h4>
                    </div>
                    <div class="modal-body">
                        <?= $content ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.XwLuZKjn6M', 'Закрыть') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10">
            <?= $this->render('../cabinet/_menu3.php') ?>
        </div>
        <div class="col-lg-2">
            <?php
            $isTelegram = \cs\Application::isEmpty(Yii::$app->user->identity->telegram_chat_id) ? 0 : 1;
            $this->registerJs(<<<JS
$('.c-offer-add').click(function (e) {
    if ({$isTelegram} == 0) {
        $('#modalTelegram').modal();
        return;
    }
    window.location = '/cabinet-exchange/offer-add';
});
JS
)
            ?>
            <button class="btn c-offer-add" style="float: right; margin-right: 10px;"><?= \Yii::t('c.XwLuZKjn6M', 'Добавить предложение') ?></button>
        </div>
    </div>

    <br>
    <div class="row">
        <?php
        $this->registerJs(<<<JS

//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});

JS
);
        ?>


    </div>
    <div class="row">
        <div class="col-lg-12">
            <?php $currencyPZM = Currency::findOne(['code' => 'PZM']) ?>

            <?php \yii\widgets\Pjax::begin(); ?>

            <?php
            $t = time() - Yii::$app->params['timeOnline'];
            $sort = new \yii\data\Sort([
                'attributes' => [
                    'price'               => ['label' => Yii::t('c.XwLuZKjn6M', 'По цене')],
                    'created_at'          => ['label' => Yii::t('c.XwLuZKjn6M', 'Создано')],
                    'avg_deal'            => ['label' => Yii::t('c.XwLuZKjn6M', 'Ср. время сделки')],
                    'assessment_positive' => ['label' => Yii::t('c.XwLuZKjn6M', 'Пользователь')],
                    'deals_count_all'     => ['label' => Yii::t('c.XwLuZKjn6M', 'Сделок')],
                    'last_action3'=> [
                        'label' => Yii::t('c.XwLuZKjn6M', 'Статус'),
                        'asc'   => ['last_action2' => SORT_ASC, 'last_action' => SORT_ASC],
                        'desc'  => ['last_action2' => SORT_DESC, 'last_action' => SORT_DESC],
                    ],
                    'last_action2',
                    'volume_io_start'        => [
                        'label' => Yii::t('c.XwLuZKjn6M', 'В объеме'),
                        'asc'   => ['volume_io_start' => SORT_ASC, 'volume_io_finish' => SORT_ASC],
                        'desc'  => ['volume_io_start' => SORT_DESC, 'volume_io_finish' => SORT_DESC],
                    ],
                ],
                'defaultOrder' => [
                    'last_action2' => SORT_DESC,
                    'created_at'   => SORT_DESC,
                ],
            ]);
            $model = new \avatar\models\search\Offer2();
            $ActiveDataProvider = $model->search(
                Yii::$app->request->get(),
                [
                    'and',
                    ['is_hide' => 0],
                    ['>', 'offer.created_at', time() - 60 * 60 * 24 * 7],
                    ['>=', 'offer.volume_io_finish_deal', new \yii\db\Expression('`offer`.`volume_io_start`')],
                ],
                $sort
            );
            ?>
            <?= $this->render('index-table', [
                'ActiveDataProvider' => $ActiveDataProvider,
                'filter'             => $model,
                'sort'               => $sort,
            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>
</div>


<div class="modal fade" id="modalTelegram" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.XwLuZKjn6M', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.XwLuZKjn6M', 'Уважаемый пользователь!') ?> <a href="tg://resolve?domain=Neiro_n_Nommy_bot">@Neiro_n_Nommy_bot</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.XwLuZKjn6M', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>