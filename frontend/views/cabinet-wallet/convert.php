<?php

/** @var $this          \yii\web\View */
/** @var $model         \avatar\models\forms\ConvertAny */
/** @var $currencyFrom  \common\models\avatar\Currency */
/** @var $currencyTo    \common\models\avatar\Currency */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


$fromCurrencyExt = $currencyFrom;

$fromCio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $fromCurrencyExt->id]);
$fromCurrencyInt = \common\models\piramida\Currency::findOne($fromCio->currency_int_id);


$data = UserBill::getInternalCurrencyWallet($fromCurrencyInt->id);
$wallet = $data['wallet'];


$this->title = 'Конвертировать ' . $currencyFrom->code . ' в ' . $currencyTo->code;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

// получаю комиссию системы
$dataJson = \common\models\Config::get('ConvertParams');
$data = \yii\helpers\Json::decode($dataJson);
$s = '';
$id = $currencyFrom->id . '_' . $currencyTo->id;
foreach ($data as $o1) {
    if ($o1['id'] == $id) {
        $s = $o1;
    }
}
if (!is_array($s)) {
    throw new Exception('Не найдены проценты');
}

$binanceFrom = [
    \common\models\avatar\Currency::BTC,
    \common\models\avatar\Currency::ETH,
    \common\models\avatar\Currency::LTC,
    \common\models\avatar\Currency::TRX,
    \common\models\avatar\Currency::DASH,
    \common\models\avatar\Currency::BNB,
    \common\models\avatar\Currency::DAI,
    \common\models\avatar\Currency::DOGE
];

$toCurrencyExt = $currencyTo;
$kurs = $fromCurrencyExt->price_usd * (1-(($s['percent']) / 100)) / $toCurrencyExt->price_usd;
$toCio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $toCurrencyExt->id]);
$toCurrencyInt = \common\models\piramida\Currency::findOne($toCio->currency_int_id);
?>
<?php
$str847 = Yii::t('c.EQFlAIQvBL', 'Скопировано');
$fromUsd = $fromCurrencyExt->price_usd;
$toUsd = $toCurrencyExt->price_usd;
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str847}'
    }).show();

});
$('#convertany-amount').on('input', function(e) {
    var v = $(this).val();
    var v2 = v * {$kurs};
    $('#convertany-amount2').val((v2).toFixed({$toCurrencyInt->decimals_view}));
    $('.js-from-usd').html('~ ' +  (v * {$fromUsd}).toFixed(2) + ' USDT');
    $('.js-to-usd').html('~ ' +  (v2 * {$toUsd}).toFixed(2) + ' USDT');
});
$('#convertany-amount2').on('input', function(e) {
    var v2 = $(this).val();
    var v = v2 / {$kurs};
    $('#convertany-amount').val((v).toFixed({$fromCurrencyInt->decimals_view}));
    $('.js-from-usd').html('~ ' +  (v * {$fromUsd}).toFixed(2) + ' USDT');
    $('.js-to-usd').html('~ ' +  (v2 * {$toUsd}).toFixed(2) + ' USDT');
});
JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>


    <div class="col-lg-6 col-lg-offset-3">
        <p class="alert alert-danger"><?= \Yii::t('c.WxnFTlXT5m', 'Обратите внимание! Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT.') ?></p>


        <?php if (false) { ?>
            <?php if ($fromCurrencyExt->id == \common\models\avatar\Currency::PZM) { ?>
                <?php
                $c = \common\models\Config::get('pzm_exchange_limit');
                $v = \common\models\piramida\Currency::getValueFromAtom($c, \common\models\piramida\Currency::PZM);
                ?>
                <p class="alert alert-danger">Во избежании просадки курса низколиквидных биржевых криптоактивов, их конвертация лимитирована определенным суточным объемом. На текущий момент общий доступный для конвертации остаток монет составляет  <?= $v ?> PRIZM.</p>
            <?php } ?>
        <?php } ?>

        <?php if (in_array($currencyFrom->id, $binanceFrom)) { ?>
            <p class="alert alert-danger">
                Disclaimer<br>
                <br>
                Внимание! Работа сервиса конвертации - автоматизированный процесс биржевой online-активности,
                с вынужденным привлечением третьих сторон. При конвертации Вы получите курс базового актива
                по усредненной цене исполнения возможной последовательности маркет-ордеров.<br>
                <br>
                Мы предупреждаем, а Вы должны быть готовы к тому, что в процессе конвертации:<br>
                <br>
                Время обмена может затянуться, например, при панических движениях биржевых котировок
                базового актива, особенно для высоковолатильных валют, иногда на достаточно долгий период времени.<br>
                <br>
                Цена котировки базового актива так-же может быть далека от той, которую Вы видите в текущем
                моменте, например, из-за проскальзывания курса при резком движении цены высоковолатильных монет.
                Финишный курс конвертации будет высчитан как производная от средних величин разбега
                цен и объемов в паре при обмене активов, зафиксированных в моментах фактического исполнения
                возможной череды маркет-ордеров, с учетом соответствующих комиссий.<br>
                <br>
                Мы так подробно описали для вас эти вводные потому, что вы должны понимать, устраивают
                ли вас такие условия. Процессы автоматизированы, начав конвертацию, отменить обмен будет
                невозможно, и претензии рассматриваться не будут. Поэтому примите для себя решение сами.<br>
                <br>
                (!!!) Итак, нажимая на кнопку "Конвертировать", вы соглашаетесь со всеми вышеперечисленными
                условиями. Если не согласны, - вы должны покинуть эту страницу конвертации, и провести обмен
                своими силами, например на P2P-площадке, или самостоятельно на любой из бирж.
            </p>
        <?php } ?>

        <p class="text-center">По курсу 1 <?= $fromCurrencyExt->code ?> = <?= Yii::$app->formatter->asDecimal($kurs, 2) ?> <?= $toCurrencyExt->code ?></p>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-bills/index';
    }).modal();
}
JS

        ]); ?>
        <div class="row">
            <div class="col-lg-6">
                <p class="text-center">
                    <?php
                    $image = $fromCurrencyExt->image;
                    if (\yii\helpers\StringHelper::startsWith($image, 'http')) {

                    } else {
                        $image = 'https://neiro-n.com' . $currencyFrom->image;
                    }
                    ?>
                    <img src="<?= $image ?>" width="100" style="margin-top: 30px;" class="img-circle">
                </p>

                <?php
                $address = $wallet->getAddress();
                $options = [
                    'data'  => [
                        'toggle'         => 'tooltip',
                        'clipboard-text' => $address,
                    ],
                    'class' => 'buttonCopy',
                    'title' => Yii::t('c.EQFlAIQvBL', 'Адрес кошелька. Нажми чтобы скопировать'),
                ];
                ?>

                <p class="text-center"><code <?= Html::renderTagAttributes($options) ?>><?= $address ?></code></p>
                <p class="text-center"><?= $fromCurrencyExt->title ?></p>
                <h3 class="page-header text-center"><?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $fromCurrencyInt->decimals_view) ?></h3>
                <p class="text-center"><span class="label label-info"><?= $fromCurrencyInt->code ?></span></p>


            </div>
            <div class="col-lg-6">
                <p class="text-center">
                    <?php
                    $image = $toCurrencyExt->image;
                    if (\yii\helpers\StringHelper::startsWith($image, 'http')) {

                    } else {
                        $image = 'https://neiro-n.com' . $toCurrencyExt->image;
                    }
                    ?>
                    <img src="<?= $image ?>" width="100" style="margin-top: 30px;" class="img-circle">
                </p>
                <p class="text-center"><?= $toCurrencyExt->title ?></p>
                <p class="text-center"><span class="label label-info"><?= $toCurrencyExt->code ?></span></p>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6">
                <?= $form->field($model, 'amount')->label(Yii::t('c.WxnFTlXT5m', 'Отдаете') . ' ' . $fromCurrencyExt->code)->hint(\Yii::t('c.WxnFTlXT5m', 'дробное число пишется через точку')) ?>
                <p class="js-from-usd"></p>
            </div>
            <div class="col-lg-6">
                <div class="form-group field-convertlot-amount required">
                    <label class="control-label" ><?= \Yii::t('c.WxnFTlXT5m', 'Получаете') ?> <?= $toCurrencyExt->code ?></label>
                    <input type="text" id="convertany-amount2" class="form-control" aria-required="true">
                    <p class="help-block"><?= \Yii::t('c.WxnFTlXT5m', 'дробное число пишется через точку') ?></p>
                    <p class="js-to-usd"></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => Yii::t('c.WxnFTlXT5m', 'Конвертировать')]); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>
