<?php

/** @var $this \yii\web\View */
/** @var $bill \common\models\avatar\UserBill */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var  $currencyELXT \common\models\avatar\Currency */
$currencyELXT = $bill->getCurrencyObject();

$this->title = $currencyELXT->title;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


?>
<?php
$str838 = Yii::t('c.BBXvjwPu8f', 'Скопировано');
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str838}'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php


        $userWallet = $bill;
        $wallet = \common\models\piramida\Wallet::findOne($bill->address);
        $currency = $wallet->getCurrency();
        Yii::$app->session->set('currency.decimals', $currency->decimals);
        Yii::$app->session->set('currency.decimals_view', $currency->decimals_view);
        ?>

        <p class="text-center">
            <img src="<?= $currencyELXT->image ?>" width="300" style="margin-top: 30px;" class="img-circle">
        </p>
        <?php
        $address = $wallet->getAddress();
        $options = [
            'data'  => [
                'toggle'         => 'tooltip',
                'clipboard-text' => $address,
            ],
            'class' => 'buttonCopy',
            'title' => Yii::t('c.BBXvjwPu8f', 'Адрес кошелька. Нажми чтобы скопировать'),
        ];
        ?>
        <p class="text-center"><code <?= Html::renderTagAttributes($options) ?>><?= $address ?></code></p>

        <?php
        // Вычисление баланса для стейкинга
        $rows = \common\models\piramida\Operation::find()
            ->leftJoin('neiron_transaction', 'neiron_transaction.operation_id = operations.id')
            ->select([
                'operations.*',
                'neiron_transaction.type_id',
            ])
            ->where(['operations.wallet_id' => $wallet->id])
            ->orderBy(['operations.datetime' => SORT_ASC])
            ->asArray()
            ->all();

        $sum = 0;

        foreach ($rows as $o) {
            switch ($o['type_id']) {
                case \common\models\NeironTransaction::TYPE_REFERAL:
                case \common\models\NeironTransaction::TYPE_REFERAL_CONVERT:
                case \common\models\NeironTransaction::TYPE_REFERAL_CONVERT_BINANCE:
                case \common\models\NeironTransaction::TYPE_BUY_TRUSTED:
                case \common\models\NeironTransaction::TYPE_HOLD_PERCENT:
                case \common\models\NeironTransaction::TYPE_CONVERT:
                case \common\models\NeironTransaction::TYPE_CONVERT_GOOD:
                    if ($o['type'] == \common\models\piramida\Operation::TYPE_IN) {
                        $sum += $o['amount'];
                    }
                    break;
            }
        }
        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::NEIRO]);
        $c = \common\models\piramida\Currency::findOne($cio->currency_int_id);
        if ($sum > $wallet->amount) $sum = $wallet->amount;
        $sum = bcdiv($sum, pow(10, $c->decimals), $c->decimals);

        ?>

        <h3 class="page-header text-center">
            <?php if ($currencyELXT->id == \common\models\avatar\Currency::NEIRO) { ?>
                Общий баланс: <?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currency->decimals_view) ?>
                <small>
                    (Доступно для стейкинга: <?= Yii::$app->formatter->asDecimal($sum, $currency->decimals_view) ?>)
                    <a href="/page/item?id=69"><span class="label label-success">ПРАВИЛА</span></a>
                </small>
            <?php } else { ?>
                <?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currency->decimals_view) ?>
            <?php } ?>
        </h3>

        <p class="text-center"><span class="label label-info"><?= $currencyELXT->code ?></span></p>

        <!--        Показывать кнопку Отправить-->
        <?php

        $isShowSend = true;
        if ($bill->currency == \common\models\avatar\Currency::MARKET) {
            $cInt = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $UserBill2 = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cInt->id]);
            $w = \common\models\piramida\Wallet::findOne($UserBill2->wallet_id);
            if ((Yii::$app->user->identity->market_till < time()) || (Yii::$app->user->identity->market_till == 0) || ($w->amount == 0)) {
                $isShowSend = false;
            }
        }
        ?>
        <?php if ($isShowSend) { ?>
            <p>
                <a href="<?= Url::to(['cabinet-wallet/send', 'id' => $bill->id]) ?>" class="btn btn-default">Отправить</a>
            </p>
        <?php } else { ?>
            <p class="alert alert-info">
                Сейчас вы можете только пополнять счет МБ. Активируйте аккаунт покупкой любого "Пакета возможностей" здесь <a href="https://topmate.one/cabinet-market/index">https://topmate.one/cabinet-market/index</a> и вам станут доступны все опции: online-шопинг, внутренние переводы, зачисление на счет реферальных.
            </p>
        <?php } ?>
        <!--        /Показывать кнопку Отправить-->


        <hr>

        <?php
        $this->registerJs(<<<JS

$('[data-toggle="tooltip"]').tooltip();

JS
        );
        $columns = [
            [
                'header'        => 'OID',
                'attribute'     => 'id',
                'headerOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'width' => '12%',
                    ]),
                ],
                'content'       => function (\common\models\piramida\Operation $item) {
                    $address = $item->getAddress();
                    $addressShort = $item->getAddressShort();

                    return Html::tag('code', $addressShort, [
                        'data'  => [
                            'toggle'         => 'tooltip',
                            'clipboard-text' => $address,
                        ],
                        'class' => 'buttonCopy',
                        'title' => Yii::t('c.BBXvjwPu8f', 'Операция. Нажми чтобы скопировать'),
                    ]);
                },
            ],
            [
                'header'        => 'TID',
                'attribute'     => 'id',
                'headerOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'width' => '12%',
                    ]),
                ],
                'content'       => function (\common\models\piramida\Operation $item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'transaction_id');
                    if (is_null($v)) return '';
                    $transaction = new \common\models\piramida\Transaction(['id' => $v]);
                    $address = $transaction->getAddress();
                    $addressShort = $transaction->getAddressShort();

                    return Html::tag('code', $addressShort, [
                        'data'  => [
                            'toggle'         => 'tooltip',
                            'clipboard-text' => $address,
                        ],
                        'class' => 'buttonCopy',
                        'title' => Yii::t('c.BBXvjwPu8f', 'Транзакция. Нажми чтобы скопировать'),
                    ]);
                },
            ],
            [
                'header'        => Yii::t('c.BBXvjwPu8f', 'Тип'),
                'attribute'     => 'type',
                'headerOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'width' => '5%',
                    ]),
                ],
                'content'       => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'type', 0);
                    if ($v == 0) return '';
                    if ($v == 2) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-save', 'style' => 'color: #57b257']);
                    if ($v == 1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-import', 'style' => 'color: #57b257']);
                    if ($v == -1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-export', 'style' => 'color: #d54d49']);
                    if ($v == -2) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-open', 'style' => 'color: #d54d49']);

                    return '';
                },
            ],
            [
                'header'         => Yii::t('c.BBXvjwPu8f', 'сумма'),
                'attribute'      => 'amount',
                'headerOptions'  => [
                    'style' => Html::cssStyleFromArray([
                        'width'      => '10%',
                        'text-align' => 'right',
                    ]),
                ],
                'contentOptions' => [
                    'nowrap' => 'nowrap',
                    'style'  => Html::cssStyleFromArray([
                        'text-align' => 'right',
                    ]),
                ],
                'content'        => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'amount', 0);
                    if ($v == 0) return 0;
                    $v = bcdiv($v, bcpow(10, Yii::$app->session->get('currency.decimals')), Yii::$app->session->get('currency.decimals_view'));
                    $color = 1;
                    $prefix = '';
                    if (in_array($item['type'], [1, 2])) {
                        $prefix = '+';
                        $color = 'green';
                    }
                    if (in_array($item['type'], [-1, -2])) {
                        $prefix = '-';
                        $color = 'red';
                    }

                    return
                        Html::tag('span', $prefix . Yii::$app->formatter->asDecimal($v, Yii::$app->session->get('currency.decimals_view')), ['style' => 'color: ' . $color]);
                },
            ],
            [
                'header'        => Yii::t('c.BBXvjwPu8f', 'Время'),
                'headerOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'width' => '10%',
                    ]),
                ],
                'content'       => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                    if ($v == 0) return '';
                    $v = (int)($v / 1000);

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],

            [
                'header'        => Yii::t('c.BBXvjwPu8f', 'Комментарий'),
                'attribute'     => 'comment',
                'headerOptions' => [
                    'style' => Html::cssStyleFromArray([
                        'width' => '55%',
                    ]),
                ],
                'format' => 'html',
                'content'       => function ($item) {
                    if (\yii\helpers\StringHelper::startsWith($item['comment'], '[')) {
                        $data = \yii\helpers\Json::decode($item['comment']);
                        return Yii::t('c.GGWWLBoXB4', $data[0], $data[1]);
                    } else {
                        return $item['comment'];
                    }
                },
            ],
        ];
        $a = [
            'header'        => 'email',
            'content'       => function ($item) {
                if ($item['type'] == -1) {
                    $t = \common\models\piramida\Transaction::findOne($item['transaction_id']);
                    try {
                        $bill = UserBill::findOne(['address' => $t->to]);
                        $user = \common\models\UserAvatar::findOne($bill->user_id);
                        return $user->email;
                    } catch (Exception $e) {
                        return '';
                    }
                }
                if ($item['type'] == 1) {
                    $t = \common\models\piramida\Transaction::findOne($item['transaction_id']);
                    try {
                        $bill = UserBill::findOne(['address' => $t->from]);
                        $user = \common\models\UserAvatar::findOne($bill->user_id);
                        return $user->email;
                    } catch (Exception $e) {
                        return '';
                    }
                }
            }
        ];
        if (
                Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_buh') ||
                Yii::$app->authManager->checkAccess(Yii::$app->user->id, 'permission_admin')
        ) {
            $columns = ArrayHelper::merge($columns, [$a]);
        }

        ?>
        <?php \yii\widgets\Pjax::begin(); ?>
            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => \common\models\piramida\Operation::find()
                        ->where(['wallet_id' => $userWallet->address])
                        ->orderBy(['datetime' => SORT_DESC])
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'rowOptions'   => function ($item) {
                    $data = [
                        'data'  => ['id' => $item['id']],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];
                    return $data;
                },
                'columns'      => $columns,
            ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>
