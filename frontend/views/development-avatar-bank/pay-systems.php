<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Карта Криптовалют';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p><img src="/images/controller/development-avatar-bank/pay-systems/paysytems_1.png"></p>

        <?= \common\services\documentation\DbTable::widget([
            'name' => 'paysystems',
            'columns' => [
                [
                    'name' => 'id',
                    'isRequired' => true,
                    'description' => 'Идентификатор платежной системы',
                    'type' => 'int',
                ],
                [
                    'name' => 'code',
                    'isRequired' => true,
                    'description' => 'Код платежной системы, малые английские буквы и/или дефис',
                ],
                [
                    'name' => 'title',
                    'isRequired' => true,
                    'description' => 'Название платежной системы (по русски)',
                ],
                [
                    'name' => 'comission_percent',
                    'description' => 'Комиссия в процентах (не используется)',
                ],
                [
                    'name' => 'comission_fix',
                    'description' => 'Комиссия фиксированная (не используется)',
                ],
                [
                    'name' => 'class_name',
                    'isRequired' => true,
                    'description' => 'Название класса (только клас, без пути) \common\models\piramida\WalletSource\...',
                ],
                [
                    'name' => 'image',
                    'isRequired' => true,
                    'description' => 'Картинка',
                    'type' => 'varchar(255)',
                ],
            ],
        ])?>

        <?= \common\services\documentation\DbTable::widget([
            'name' => 'paysystems',
            'columns' => [
                [
                    'name' => 'id',
                    'isRequired' => true,
                    'description' => 'Идентификатор конфига платежной системы',
                    'type' => 'int',
                ],
                [
                    'name' => 'parent_id',
                    'isRequired' => true,
                    'description' => 'идентификатор списка платежных систем',
                    'type' => 'int',
                ],
                [
                    'name' => 'paysystem_id',
                    'isRequired' => true,
                    'description' => 'идентификатор платежной системы',
                    'type' => 'int',
                ],
                [
                    'name' => 'config',
                    'isRequired' => true,
                    'description' => 'конфигурация платежной системы',
                ],
            ],
        ])?>

        <h2 class="page-header">PerfectMoney</h2>
        <p>docs/perfectmoney-sci-2.0.doc</p>

        <h2 class="page-header">SberBank</h2>
        <h2 class="page-header">AlfaBank</h2>
        <h2 class="page-header">BitCoin</h2>
        <h2 class="page-header">AdvCash</h2>
        <h2 class="page-header">OkPay</h2>
        <h2 class="page-header">PayPal</h2>
        <h2 class="page-header">Pochta</h2>
        <h2 class="page-header">Qiwi</h2>
        <h2 class="page-header">VisaMasterCard</h2>
        <h2 class="page-header">Yandex</h2>


        <h3 class="page-header">Подтверждение транзакций</h3>

        <p>В некоторых классах платежных систем есть функция получения информации о транзакции</p>
        <p>\common\models\piramida\WalletSource\DogeCoin::getTransaction</p>

    </div>
</div>



