<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кошелек Token. Отправка';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>

        <h2 class="page-header">
            Отправка на карту
        </h2>
        <p>Если на карте нет кошелька токена то он создается от эфировского.</p>
        <p>Если карта еще не активирована и нет кошелька эфира то он создается.</p>

    </div>
</div>



