<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Avatar Network API';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">API</h2>

        <p>Главный Url: <code>https://avatarnetwork.io/api</code></p>
        <p>TEST Url: <code>http://z1ilsbiccbegpazhsadi.avatarnetwork.io/api</code></p>


        <h3 class="page-header">Классический запрос</h3>
        <p>Метод: <code>POST</code></p>
        <h3 class="page-header">Классический ответ</h3>
        <p><b>Положительно</b></p>
<pre>
{
    success: true,
    data: mixed
}</pre>
        <p><b>Отрицательно</b></p>
        <pre>
{
    success: false,
    data: mixed
}</pre>

        <p>в <code>data</code> хранится объект:</p>

        <p><b>1. Ошибка с идентификатором</b></p>
        <pre>
{
    id: 101,
    message: 'Не найден документ',
}
        </pre>
        <p><b>2. Ошибки формы</b></p>
        <pre>
{
    id: 101,
    data: {
        phone: ['Телефон должен начинаться с плюса','Телефон должен состоять из цифр'],
        password: ['Пароль должен быть более 3 символов'],
        name: []
    },
}
        </pre>
        <p><b>3. Ошибка текстом</b></p>
        <pre>
{
    message: 'Не найден документ',
}
        </pre>

        <h3 class="page-header">Исключение</h3>
        <pre>
{
    скоро будет информация
}
        </pre>

        <h3 class="page-header">Авторизация</h3>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXZmYyWFcwWFJ4M2s/view?usp=sharing" target="_blank">
            <img src="/images/controller/development-avatar-bank/index/register.png" style="width: 100%; max-width: 801px;">
            </a>
        </p>

        <p>В приложении используется метод авторизации <a href="https://www.netangels.ru/support/hosting-howto/php-http-auth/" target="_blank">Basic Authentication Scheme</a>.</p>
        <pre>
$credentials = "calcuser:testpwd";

header("POST /svcs/CalculationEng HTTP/1.0");
header("Content-Type: text/xml; charset=utf-8");
header("Accept: application/soap+xml, application/dime, multipart/related, text/*");
header("Cache-Control: no-cache");
header("Pragma: no-cache");
header("SOAPAction: \"run\"");
header("Content-Length: 2815");
header("Authorization: Basic " . base64_encode($credentials));
</pre>
        <p>Используется авторизация настраиваемая через Модуль.</p>
        <p>Телефон передается в переменной <code>$_SERVER['PHP_AUTH_USER'];</code> без + вначале и без спец символов</p>
        <p>Пароль в <code>$_SERVER['PHP_AUTH_PW'];</code></p>
        <p>Если логин и пароль будут не верны то будет выдана HTTP ошибка 401 (HTTP/1.1 401 Unauthorized).</p>

        <h2 class="page-header">Организация сессий</h2>
        <p>Сейчас реализовано так что пользователь с каждым запросом передает телефон и пароль и так с каждым запросом логинится</p>
        <p>Но у меня есть подозрение, что сессия так не будет работать, а это очень надо или нет?</p>
        <p>Проведу небольшое исследование: а зачем мне сессия? где она реально используется?</p>
        <p>Не понятно только в исключительных целях</p>
        <p>Поэтому предложение: сделать без сессии а там где понадобится то проведем исследование.</p>
        <p>Как реально можно сделать сессии? Нужно передавать COOKIE для PHPSESSIONID.</p>
        <p>Как конкретно? Я авторизовался потом передается только sessionID и CSRF токен. Нужно почитать как работает сессия по COOKIE</p>



        <h2 class="page-header">Функции</h2>
        <h3 class="page-header">Отправляет CMC</h3>
        <p class="alert alert-success">/auth/phone-sms</p>
        <p>Отправляет CMC</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'phone',
                    'isRequired'  => true,
                    'description' => 'телефон начиная с +7',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    key: 'fde...'
}
</pre>

        <h3 class="page-header">Проверяет CMC</h3>
        <p class="alert alert-success">/auth/phone-sms-validate</p>
        <p>Проверяет CMC</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'код пришедший на телефон',
                ],
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'description' => 'ключ возвращенный из функции /auth/phone-sms',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    key: 'fde...'
}
</pre>

        <h3 class="page-header">Проверяет 2FA код</h3>
        <p class="alert alert-success">/auth/2fa-validate</p>
        <p>Проверяет CMC</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'код пришедший на телефон',
                ],
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'description' => 'ключ возвращенный из функции /auth/phone-sms',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <p>Пусто (успешно) или ошибка</p>

        <h3 class="page-header">Создать кошелек</h3>
        <p class="alert alert-success">/auth/registration-phone</p>
        <p>Регистрирует телефон</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'phone',
                    'isRequired'  => true,
                    'description' => 'телефон без пробела и начинается с +7',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль',
                ],
                [
                    'name'        => 'name',
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'surname',
                    'description' => 'Фамилия',
                ],
                [
                    'name'        => 'patronymic',
                    'description' => 'Отчество',
                ],
                [
                    'name'        => 'birthDate',
                    'description' => 'Дата рождения',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'birthPlace',
                    'description' => 'Место рождения',
                ],
                [
                    'name'        => 'avatarId',
                    'description' => 'avatarId',
                    'type'        => 'int',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    id: int - идентификатор пользователя
}
</pre>
        <p>
            * 101, string 'Не загружены данные'
            * 102, ошибка валидации формы object {}
            * 103, не удалось зарегистрировать string 'не удалось зарегистрировать'
        </p>

        <h2 class="page-header" style="margin-top: 300px;">BTC</h2>

        <h3 class="page-header">Создать кошелек</h3>
        <p class="alert alert-success">/btc/new</p>
        <p>Создает кошелек BTC</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль от кошелька',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    address: '0xfde...'
}
</pre>


        <h3 class="page-header">Перевести деньги</h3>
        <p class="alert alert-success">/btc/send</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'description' => 'идентификатор счета списания',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'адрес назначения кошелька начинающийся 0x',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'description' => 'кол-во биткойноы, десятичные доли отделяюются точкой',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'коментарий для перевода',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    transaction: {
        address: '0x...'
    }
}</pre>

        <p>Ошибки: - 101, недостаточно денег в кошельке</p>
        <p>Ошибки: - 102, недостаточно денег для оплаты комиссии</p>

        <h3 class="page-header">Возвращает баланс в счете</h3>
        <p class="alert alert-success">/btc/get-balance</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
    &lt;confirmed&gt; - float (BTC),
    &lt;unconfirmed&gt; - float (BTC),
]</pre>

        <p>confirmed - подтвержденный</p>
        <p>unconfirmed - не подтвержденный</p>


        <h3 class="page-header">Возвращает список транзакций по счету</h3>
        <p class="alert alert-success">/btc/transactions-list</p>
        <p>Возвращает список транзакций по счету по 20 шт на страницу</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
                [
                    'name'        => 'page',
                    'type'        => 'int',
                    'description' => 'Страница, по умолчанию = 1',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
    {},
]</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'hash',
                    'isRequired'  => true,
                    'description' => 'Хеш транзации, идентификатор тразакции в блокчейне BTC',
                ],
                [
                    'name'        => 'time',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время создания транзакции',
                ],
                [
                    'name'        => 'wallet_value_change',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Сумма на которую изменился кошелек после траннзакции. Измеряется в Сатоши. Если вывод - то сумма будет отрицтельной, если приход - то положительной. Сумма не включает в себя комиссиию, то есть при отправке денег из кошелька, это реальная сумма отправки денег, а к ней еще плюсуется сумма комиссии',
                ],
                [
                    'name'        => 'confirmations',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Кол-во подтверждений',
                ],
                [
                    'name'        => 'block_height',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'В каком блоке зарегистрирована транзакция. Если не подтвержена, то = null',
                ],
                [
                    'name'        => 'block_hash',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Хеш блока. Если не подтвержена транзакция, то = null',
                ],
                [
                    'name'        => 'total_fee',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Комиссия за транзакцию в сатоши',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'Коментарий к транзакции',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'int',
                    'description' => 'Тип транзакции (направление операции), 1 - приход в кошелек, -1 - изъятие из кошелька',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'object',
                    'description' => 'Данные о партнере сделки (счет, пользователь)',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>data</code></p>
        <p>Если есть параметр <code>user_id</code> то значит будет и <code>billing_id</code>.
            Если задан параметр <code>user_id</code> то будет и параметр <code>user</code>.
            Если задан параметр <code>billing_id</code> то будет и параметр <code>billing</code>.</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя, от кого или кому была транзакция',
                ],
                [
                    'name'        => 'user',
                    'type'        => 'object',
                    'description' => 'Объект пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета. С какого счта или на какой была транзакция',
                ],
                [
                    'name'        => 'billing',
                    'type'        => 'object',
                    'description' => 'Объект счета',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>user</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'email',
                    'isRequired'  => true,
                    'description' => 'Почта клиента',
                ],
                [
                    'name'        => 'avatar',
                    'description' => 'Путь к аватарке',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>billing</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название счета',
                ],
            ]
        ]) ?>










        <h2 class="page-header" style="margin-top: 300px;">ETH</h2>
        <h3 class="page-header">Создать кошелек</h3>
        <p class="alert alert-success">/eth/new</p>
        <p>Создает кошелек ETH</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль от кошелька',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    address: '0xfde...'
}
</pre>


        <h3 class="page-header">Создать кошелек</h3>
        <p class="alert alert-success">/eth/new</p>
        <p>Создает кошелек ETH</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль от кошелька',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    address: '0xfde...'
}
</pre>


        <h3 class="page-header">Перевести деньги</h3>
        <p class="alert alert-success">/eth/send</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'description' => 'идентификатор счета списания',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'адрес назначения кошелька начинающийся 0x',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'description' => 'кол-во биткойноы, десятичные доли отделяюются точкой',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'Коментарий для перевода',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    transaction: {
        address: '0x...'
    }
}</pre>

        <p>Ошибки: - 101, недостаточно денег в кошельке</p>
        <p>Ошибки: - 102, недостаточно денег для оплаты комиссии</p>

        <h3 class="page-header">Возвращает баланс в счете</h3>
        <p class="alert alert-success">/eth/get-balance</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
    &lt;confirmed&gt; - float (ETH),

]</pre>

        <p>confirmed - подтвержденный</p>
        <p>unconfirmed - не подтвержденный</p>



        <h3 class="page-header">Забрать деньги с карты Аватара</h3>
        <p class="alert alert-success">/btc/get-from-card</p>
        <p class="alert alert-warning">На тестировании</p>

        <p>Алгоритм:</p>
        <ul>
            <li>Человек хочет снять денег с карты друга</li>
            <li>Он сканирует его карту своим телефоном</li>
            <li>После этого на телефоне выскакивает окно с вводом пин кода, кол-вом денег для отправки и комментарием</li>
            <li>После этого хозяин телефона предлагает другу ввести пин код от карты</li>
            <li>Нажимает отправить</li>
            <li>Сразу после этого отправляется запрос на <code>/btc/get-from-card</code></li>
            <li>Если все успешно то деньги отправляются хозяину телефона на счет по умолчанию</li>
            <li>Если нет то вы водится соответствующая ошибка</li>
        </ul>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'адрес карты с которой взять деньги',
                ],
                [
                    'name'        => 'pin',
                    'isRequired'  => true,
                    'description' => 'Пин код',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'кол-во денег - сколько денег, разделитель - точка',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'код валюты в которой хочет взять, по умолчанию '.Html::tag('code', 'BTC'),
                ],
                [
                    'name'        => 'comment',
                    'description' => 'коментарий',
                ],
                [
                    'type'        => 'int',
                    'name'        => 'billing_destination_id',
                    'description' => 'идентификатор счета назначения, по умолчанию деньги поступят на кошелек по умолчанию',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    'transaction': {
        'hash': $t,
    }
}</pre>
        <p><b>Ошибки</b></p>

        <p>101, 'Это не ваш счет назначения'</p>
        <p>102, 'Нет обязательного параметра address'</p>
        <p>103, 'Не найдена карта с адресом {address}'</p>
        <p>104, 'Нет обязательного параметра amount'</p>
        <p>105, 'Запрещенные символы в amount'</p>
        <p>106, 'Значение amount не может быть меньше или равно 0'</p>
        <p>107, 'Нет обязательного параметра pin'</p>
        <p>108, 'На карте не установлен PIN код'</p>
        <p>109, 'Не верный PIN код'</p>



        <h3 class="page-header">Возвращает список транзакций по счету</h3>
        <p class="alert alert-success">/eth/transactions-list</p>
        <p>Возвращает список транзакций по счету по 20 шт на страницу</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
                [
                    'name'        => 'page',
                    'type'        => 'int',
                    'description' => 'Страница, по умолчанию = 1',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
    {
      "blockNumber": "65204",
      "timeStamp": "1439232889",
      "hash": "0x98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c",
      "nonce": "0",
      "blockHash": "0x373d339e45a701447367d7b9c7cef84aab79c2b2714271b908cda0ab3ad0849b",
      "transactionIndex": "0",
      "from": "0x3fb1cd2cd96c6d5c0b5eb3322d807b34482481d4",
      "to": "0xde0b295669a9fd93d5f28d9ec85e40f4cb697bae",
      "value": "0",
      "gas": "122261",
      "gasPrice": "50000000000",
      "isError": "0",
      "input": "0xf00d4b5d000000000000000000000000036c8cecce8d8bbf0831d840d7f29c9e3ddefa63000000000000000000000000c5a96db085dda36ffbe390f455315d30d6d3dc52",
      "contractAddress": "",
      "cumulativeGasUsed": "122207",
      "gasUsed": "122207",
      "confirmations": "3745410"
      },
]</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'hash',
                    'isRequired'  => true,
                    'description' => 'Хеш транзации, идентификатор тразакции в блокчейне BTC',
                ],
                [
                    'name'        => 'timeStamp',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время создания транзакции',
                ],
                [
                    'name'        => 'value',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Сумма на которую изменился кошелек после траннзакции. Измеряется в wei (10e-18). Сумма всегда положительна. Сумма не включает в себя комиссиию, то есть при отправке денег из кошелька, это реальная сумма отправки денег, а к ней еще плюсуется сумма комиссии',
                ],
                [
                    'name'        => 'confirmations',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Кол-во подтверждений',
                ],
                [
                    'name'        => 'blockNumber',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'В каком блоке зарегистрирована транзакция. Если не подтвержена, то = null',
                ],
                [
                    'name'        => 'blockHash',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Хеш блока. Если не подтвержена транзакция, то = null',
                ],
                [
                    'name'        => 'total_fee',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Комиссия за транзакцию. Измеряется в wei (10e-18)',
                ],
                [
                    'name'        => 'nonce',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'transactionIndex',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'from',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'to',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'gas',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'gasPrice',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'isError',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'input',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'contractAddress',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'cumulativeGasUsed',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => '',
                ],
                [
                    'name'        => 'gasUsed',
                    'isRequired'  => true,
                    'description' => '',
                ],

                [
                    'name'        => 'comment',
                    'description' => 'Коментарий к транзакции',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'int',
                    'description' => 'Тип транзакции (направление операции), 1 - приход в кошелек, -1 - изъятие из кошелька',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'object',
                    'description' => 'Данные о партнере сделки (счет, пользователь)',
                ],
                [
                    'name'        => 'value_converted',
                    'type'        => 'double',
                    'description' => 'Кол-во переданных монет. Значение в валюте которой хочет видеть пользователь. Если настройка не задана, то не будет этого параметра. Кол-во запятых после запятой соответствует валюте и значение задается в монетах указанной валюты.',
                ],
                [
                    'name'        => 'fee_converted',
                    'type'        => 'double',
                    'description' => 'Кол-во заплаченной комиссии. Значение в валюте которой хочет видеть пользователь. Если настройка не задана, то не будет этого параметра. Кол-во запятых после запятой соответствует валюте и значение задается в монетах указанной валюты.',
                ],
            ]
        ]) ?>


        <p>Всего есть три типа транзакций:</p>
        <p>1. Перевод эфира</p>
        <p><code>contractAddress</code> = "", <code>input</code> = "0x"</p>
        <p>Если <code>from</code> = настоящему кошельку, то перевод производится из кошелька (OUT)</p>
        <p>Если <code>to</code> = настоящему кошельку, то перевод производится в кошельек (IN)</p>

        <p>2. Исполнение контракта</p>
        <p><code>to</code> = адрес контракта, <code>contractAddress</code> = "", <code>input</code> - "0x...", <code>value</code> = "0"</p>

        <p>3. Создание контракта</p>
        <p><code>to</code> = 0, <code>contractAddress</code> - адрес созданного контракта, <code>input</code> - код контрата</p>


        <p>Параметры объекта <code>data</code></p>
        <p>Если есть параметр <code>user_id</code> то значит будет и <code>billing_id</code>.
            Если задан параметр <code>user_id</code> то будет и параметр <code>user</code>.
            Если задан параметр <code>billing_id</code> то будет и параметр <code>billing</code>.</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя, от кого или кому была транзакция',
                ],
                [
                    'name'        => 'user',
                    'type'        => 'object',
                    'description' => 'Объект пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета. С какого счта или на какой была транзакция',
                ],
                [
                    'name'        => 'billing',
                    'type'        => 'object',
                    'description' => 'Объект счета',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>user</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'email',
                    'isRequired'  => true,
                    'description' => 'Почта клиента',
                ],
                [
                    'name'        => 'avatar',
                    'description' => 'Путь к аватарке',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>billing</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название счета',
                ],
            ]
        ]) ?>


        <h2 class="page-header" style="margin-top: 300px;">Токен</h2>
        <h3 class="page-header">Создать кошелек</h3>
        <p class="alert alert-success">/token/new</p>
        <p>Создает кошелек для токена</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название кошелька',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'Пароль от кабинета',
                ],
                [
                    'name'        => 'currency',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор валюты',
                ],
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Идентификатор счета ETH, к которому будет привязан токен',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    address: '0xfde...'
}
</pre>


        <h3 class="page-header">Перевести токены</h3>
        <p class="alert alert-success">/token/send</p>

        <p>Валюта/токен определяется по счету отправителя <code>billing_id</code></p>
        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'description' => 'идентификатор счета списания',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'пароль от кабинета',
                ],
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'Адрес назначения кошелька начинающийся 0x, можно указать адрес счета, почту или телефон клиента. Но если у получателя не будет эфировского кошелька то будет возвращена ошибка',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'кол-во монет, десятичные доли отделяюются точкой',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'Коментарий для перевода',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    transaction: {
        address: '0x...'
    }
}</pre>


        <h3 class="page-header">Возвращает баланс в счете</h3>
        <p class="alert alert-success">/token/get-balance</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
    &lt;confirmed&gt; - float ,

]</pre>

        <p>confirmed - подтвержденный</p>



        <h3 class="page-header">Забрать деньги с карты Аватара</h3>
        <p class="alert alert-success">/token/get-from-card</p>
        <p class="alert alert-danger">В раработке</p>

        <p>Алгоритм:</p>
        <ul>
            <li>Человек хочет снять денег с карты друга</li>
            <li>Он сканирует его карту своим телефоном</li>
            <li>После этого на телефоне выскакивает окно с вводом пин кода, кол-вом денег для отправки и комментарием</li>
            <li>После этого хозяин телефона предлагает другу ввести пин код от карты</li>
            <li>Нажимает отправить</li>
            <li>Сразу после этого отправляется запрос на <code>/btc/get-from-card</code></li>
            <li>Если все успешно то деньги отправляются хозяину телефона на счет по умолчанию</li>
            <li>Если нет то вы водится соответствующая ошибка</li>
        </ul>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'address',
                    'isRequired'  => true,
                    'description' => 'адрес карты с которой взять деньги',
                ],
                [
                    'name'        => 'pin',
                    'isRequired'  => true,
                    'description' => 'Пин код',
                ],
                [
                    'name'        => 'amount',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'кол-во денег - сколько денег, разделитель - точка',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'код валюты в которой хочет взять, по умолчанию billing_destination_id',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'коментарий',
                ],
                [
                    'type'        => 'int',
                    'name'        => 'billing_destination_id',
                    'description' => 'идентификатор счета назначения, по умолчанию деньги поступят на кошелек по умолчанию',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
{
    'transaction': {
        'hash': $t,
    }
}</pre>
        <p><b>Ошибки</b></p>

        <p>101, 'Это не ваш счет назначения'</p>
        <p>102, 'Нет обязательного параметра address'</p>
        <p>103, 'Не найдена карта с адресом {address}'</p>
        <p>104, 'Нет обязательного параметра amount'</p>
        <p>105, 'Запрещенные символы в amount'</p>
        <p>106, 'Значение amount не может быть меньше или равно 0'</p>
        <p>107, 'Нет обязательного параметра pin'</p>
        <p>108, 'На карте не установлен PIN код'</p>
        <p>109, 'Не верный PIN код'</p>



        <h3 class="page-header">Возвращает список транзакций по счету</h3>
        <p class="alert alert-success">/token/transactions-list</p>
        <p>Возвращает список транзакций по счету по 20 шт на страницу</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета по банку Аватар',
                ],
                [
                    'name'        => 'page',
                    'type'        => 'int',
                    'description' => 'Страница, по умолчанию = 1',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
       [
           'txid'      => '0xdf57c61e66bfa574625ad7dc4b61f8a8261a9564ee953d13d5a4e1f71caf3193'
           'time'      => '1501851527'
           'from'      => '0x64f9a89cf477fa58cb4c439515368f328260c072'
           'to'        => '0x214aa8a188691a942e268595674b17e2d38ac5de'
           'direction' => -1
           'value'     => '10000' // измеряется в монетах
           'comment'   => 'Перевод от клиента №'
           "data": "{}"
       ],
       //...
]</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'txid',
                    'isRequired'  => true,
                    'description' => 'Хеш транзации, идентификатор тразакции',
                ],
                [
                    'name'        => 'time',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время создания транзакции',
                ],
                [
                    'name'        => 'value',
                    'isRequired'  => true,
                    'type'        => 'double',
                    'description' => 'Кол-во монет переведенных, десятичный знак - точка',
                ],
                [
                    'name'        => 'direction',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Направление перевода 1 - в кошелек (приход), -1 - из кошелька (расход)',
                ],
                [
                    'name'        => 'from',
                    'isRequired'  => true,
                    'description' => 'Адрес от кого, начинается с 0x',
                ],
                [
                    'name'        => 'to',
                    'isRequired'  => true,
                    'description' => 'Адрес кому, начинается с 0x',
                ],
                [
                    'name'        => 'comment',
                    'description' => 'Коментарий к транзакции',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'object',
                    'description' => 'Данные о партнере сделки (счет, пользователь)',
                ],
            ]
        ]) ?>

        <p>Параметры объекта <code>data</code></p>
        <p>Если есть параметр <code>user_id</code> то значит будет и <code>billing_id</code>.
            Если задан параметр <code>user_id</code> то будет и параметр <code>user</code>.
            Если задан параметр <code>billing_id</code> то будет и параметр <code>billing</code>.</p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'description' => 'Идентификатор пользователя, от кого или кому была транзакция',
                ],
                [
                    'name'        => 'user',
                    'type'        => 'object',
                    'description' => 'Объект пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета. С какого счта или на какой была транзакция',
                ],
                [
                    'name'        => 'billing',
                    'type'        => 'object',
                    'description' => 'Объект счета',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>user</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Имя',
                ],
                [
                    'name'        => 'email',
                    'isRequired'  => true,
                    'description' => 'Почта клиента',
                ],
                [
                    'name'        => 'avatar',
                    'description' => 'Путь к аватарке',
                ],
            ]
        ]) ?>
        <p>Параметры объекта <code>billing</code></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название счета',
                ],
            ]
        ]) ?>

        <h2 class="page-header" style="margin-top: 300px;">Общие</h2>
        <h3 class="page-header">Возвращает список всех активных счетов</h3>
        <p class="alert alert-success">/btc/wallet-list</p>
        <p class="alert alert-success">/auth-common/wallet-list</p>

        <p>Возвращает списк всех кошельков</p>

        <p><b>Входные параметры</b></p>
        <p>Нет</p>

        <p><b>Выходные данные</b></p>
        <pre>
[
 {
    id: int
    name: string
    address: string
    is_pin: int 0/1
    created_at: int
    is_merchant: int 0/1
    currency: int
 }
]</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентифкатор счета',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название счета',
                ],
                [
                    'name'        => 'is_pin',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Стоит ли пин код? 0 - нет, 1 - да',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Время создвания счета',
                ],
                [
                    'name'        => 'is_merchant',
                    'type'        => 'int',
                    'description' => 'Есть ли мерчант? 0 - нет, 1 - да, по умолчанию - 0',
                ],
                [
                    'name'        => 'currency',
                    'type'        => 'int',
                    'description' => 'Идентификатор валюты',
                ],
            ]
        ]) ?>

        <h3 class="page-header">Возвращает список всех валют</h3>
        <p class="alert alert-success">/common/currency-list</p>

        <p><span class="label label-success">Не нужна авторизация</span></p>
        <p>Возвращает списк всех валют</p>

        <p><b>Входные параметры</b></p>
        <p>Нет</p>

        <p><b>Выходные данные</b></p>
        <pre>
[
 {
    id: int
    name: string
    code: string
    kurs: double
    decimals: int
    is_view: int
    image: string
 }
]</pre>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентифкатор счета',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название валюты',
                ],
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'Код валюты',
                ],
                [
                    'name'        => 'decimals',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Кол-во знаков после запятой',
                ],
                [
                    'name'        => 'kurs',
                    'isRequired'  => true,
                    'type'        => 'doube',
                    'description' => 'Курс валюты относительно рубля',
                ],
                [
                    'name'        => 'is_view',
                    'isRequired'  => true,
                    'type'        => 'int',
                    'description' => 'Показывать в списке при переводе денег? 0 - нет, 1 - да (под вопросом)',
                ],
                [
                    'name'        => 'image',
                    'isRequired'  => true,
                    'description' => 'Картинка на монету, формат - квадратная (1:1), путь относительно корня сайта URL',
                ],
            ]
        ]) ?>

        <h3 class="page-header">Загрузить Аватарку</h3>
        <p class="alert alert-success">/data-base/upload</p>
        <p class="alert alert-danger">В разработке</p>

        <p>Файл передается так же как в форме под именем поля <code>image</code>, то есть как <code>multipart-form-data</code></p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'data',
                    'description' => 'Дополнительные данные для аватарки',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
[
 {
    id: int
    hash: string
 }
]</pre>


        <p><code>id</code> - Идентифкатор счета</p>
        <p><code>hash</code> - hash аватарки и картинки</p>

        <p><b>Ошибки</b></p>
        <p>101 - Не загружены данные</p>
        <p>102 - Не верная валидация, возвращается array</p>


        <h3 class="page-header">Получение курса</h3>
        <p class="alert alert-success">/money-rate</p>
        <p class="alert alert-danger">В разработке</p>
        <p><b>Выходные данные</b></p>
        <pre>
{
    btc: float
    eth: float
}</pre>








        <h2 class="page-header">Восстановление пароля</h2>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXc3BFSGthVDB1VG8/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/index/reset_password.png">
            </a>
        </p>

        <h3 class="page-header">Запрашивает сброс пароля</h3>
        <p class="alert alert-success">/auth/reset-password</p>
        <p>Запрашивает сброс пароля</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'phone',
                    'isRequired'  => true,
                    'description' => 'телефон на который зарегистрирован пользователь +7 и без спец символов, только цифры',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
нет</pre>

        <p><b>Ошибки</b></p>

        <h3 class="page-header">Запрашивает сброс пароля</h3>
        <p class="alert alert-success">/auth/reset-password-validate</p>
        <p>Запрашивает сброс пароля</p>

        <p><b>Входные параметры</b></p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'description' => 'ключ который был выслан в функции '. Html::tag('code', '/auth/reset-password'),
                ],
                [
                    'name'        => 'code',
                    'isRequired'  => true,
                    'description' => 'код который был выслан в смс',
                ],
                [
                    'name'        => 'password',
                    'isRequired'  => true,
                    'description' => 'новый пароль',
                ],
            ]
        ]) ?>

        <p><b>Выходные данные</b></p>
        <pre>
нет</pre>

        <p><b>Ошибки</b></p>
        <p>102, 'Нет кода'</p>
        <p>103, 'Не верный код'</p>


        <h2 class="page-header">Вход в мобильное приложение</h2>

        <p>При первом входе пользователю предлагается ввести пин код который будет запрашиваться всегда при входе вместо авторизации. Пин код сохраняется в памяти телефона.</p>



        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXNUwtWHVBdVZ6eTQ/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/index/enter.png">
            </a>
        </p>


    </div>
</div>



