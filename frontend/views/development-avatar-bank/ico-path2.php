<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Страница проекта. Путь 2';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>Здесь два шага</p>
        <p><code>/ico-path2/validate-pin</code> - проверяет PIN</p>
        <p><code>/ico-path2/create-wallet</code> - создается пользователь, создается заказ</p>
        <p><code>/ico-path1/create-payment</code> - выставляется счет, генерируется HTML</p>

        <p><img src="/images/controller/development-avatar-bank/ico-path2/2017-12-30_20-17-02.png"></p>

        <h2 class="page-header">Исследование вопроса на выставление счета</h2>
        <p>Я хочу сделать функцию указать транзакцию для подтверждения, но не знаю куда идти и обращаться</p>
        <p>Она сохраняется в таблице <code>ico_request_address</code>. И я вот думаю а может ее запихнуть в счета ведь они понадобятся и там.</p>
        <p>Вот я и думаю что если я сделаю универсальную функцию, то любой кто попало может всавить транзакцию.</p>
        <p>Нужно начать делать.</p>
        <p>Сразу встает вопрос какой адрес ссылки будет на подтверждение?</p>
        <p>/projects/confirm-transaction</p>

    </div>
</div>



