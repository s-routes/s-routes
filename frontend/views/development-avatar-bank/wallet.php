<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Кошельки';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
        </h1>

        <p>Что такое счет по умолчанию? Это счет который используется для выбора по умолчанию если отправитель монет указывает почту или телефон получателя.</p>
        <p>Используется для ситуации: </p>

        <p>Класс: <code>\common\models\avatar\UserBillDefault</code>.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'user_bill_default',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'user_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'currency_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты',
                    'type'        => 'int',
                ],
            ],
        ]) ?>
        <p>У пользователя может быть два счета по умолчанию BTC и ETH.</p>
        <h2 class="page-header">Как получить счет по умолчанию</h2>
        <pre>$user->getPiramidaBilling($this->_currencyObject->id);</pre>
        <h2 class="page-header">Карты и привязанные счета</h2>

        <p>
            На карту может буть привязано сколько угодно валют, но по одному счету.
            Сзади карты отображается биткойн кошелек. Он определяет владельца и все счета привязанные к этой карте.
        </p>

        <p>Счет к карте привязывается через таблицу <code>card</code>. К одной карте привязывается # счетов</p>

    </div>
</div>



