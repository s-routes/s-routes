<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Логирование';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <?= \common\services\documentation\DbTable::widget([
            'name' => 'log_access',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор логируемой строки',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'time',
                    'isRequired'  => true,
                    'description' => 'Время создания записи',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'session',
                    'description' => 'Дамп переменной SESSION',
                    'type'        => 'varchar(4000)',
                ],
                [
                    'name'        => 'cookie',
                    'description' => 'Дамп переменной COOKIE',
                    'type'        => 'varchar(2000)',
                ],
                [
                    'name'        => 'server',
                    'description' => 'Дамп переменной SERVER',
                    'type'        => 'varchar(4000)',
                ],
                [
                    'name'        => 'post',
                    'description' => 'Дамп переменной POST',
                    'type'        => 'varchar(2000)',
                ],
                [
                    'name'        => 'headers',
                    'description' => 'Дамп заголовков',
                    'type'        => 'varchar(2000)',
                ],
                [
                    'name'        => 'ipv4',
                    'description' => 'IP v4',
                    'type'        => 'varchar(20)',
                ],
                [
                    'name'        => 'method',
                    'description' => 'Метод',
                    'type'        => 'tinyint',
                ],
            ]
        ]) ?>

        <h3 class="page-header">Где пишется?</h3>
        <p><code>/avatar-bank/config/main.php 'on beforeRequest'</code></p>


    </div>
</div>



