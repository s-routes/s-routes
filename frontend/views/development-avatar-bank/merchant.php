<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Мерчант (Платежный терминал)';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Описание</h2>
        <p>Предназначен для сбора отплаты с клиента за заказ магазина и аккамулирование этих средств на счете</p>
        <p>При настройке мерчанта вы можете не тнолько аккамулировать средства на счете а еще и перенаправлять их на
            нужный вам кошелек в автоматическом режиме</p>
        <p>Для того чтобы создать мерчант вам нужно в банке на счете нажать "Создать мерчант"</p>
        <p>Ссылка на которую нужно передать запрос: <code>https://www.avatar-bank.com/merchant</code></p>
        <p>Метод подачи запроса: <code>POST</code>.</p>

        <h2 class="page-header">Модель данных</h2>
        <p><img src="/images/controller/development-avatar-bank/merchant/data_model.png" class="thumbnail"> </p>
        <p><code>user_bill</code> - счет пользователя</p>
        <p><code>bill_merchant</code> - настройка мерчанта</p>
        <p><code>merchant_request</code> - запрос на ввод денег</p>
        <p><code>billing</code> - глобальный инвойс для запроса на ввод денег <code>merchant_request</code> на счет <code>user_bill</code></p>
        <p><code>paysystems</code> - Список платежных систем</p>
        <p><code>paysystems_config</code> - настройка платежной системы через которую была осуществлена оплата</p>
        <p><code>payments_bit_coin</code> - настройка для запроса на ввод денег <code>merchant_request</code> на счет <code>user_bill</code> через платежную систему <code>paysystems.code = 'bit-coin'</code></p>


        <h2 class="page-header">Схема мерчанта</h2>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXRjRGa2tMbEZ2T2M/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/merchant/merchant3.png" width="100%"/>
            </a>
        </p>





        <h2 class="page-header">Формирование заказа</h2>



        <p>Запрос отправляется методом POST с сайта</p>
        <p>Входные параметры:</p>

        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'description' => 'Открытый ключ мерчанта',
                ],
                [
                    'name'        => 'title',
                    'isRequired'  => true,
                    'description' => 'Наименование заказа оплаты',
                ],
                [
                    'name'        => 'price',
                    'isRequired'  => true,
                    'description' => 'стоимость заказа, поддерживанется как точка так и запятая для разделения дробной части',
                    'type'        => 'double',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'трехбуквенный код валюты (см. раздел валюты), по умолчанию BTC. Сейчас работает RUB,EUR,USD,ETH,BTC. Если заказ выставлен в валюте отличной от BTC, то она автоматически будет сконвертирована в BTC.',
                ],
                [
                    'name'        => 'data',
                    'description' => 'Дополнительные данные для заказа, которые будут возвращены в ответе (поддтверждении), не более 2000 символов',
                ],
                [
                    'name'        => 'successUrl',
                    'description' => 'url куда будет перенаправлен клиент после удачной оплаты',
                ],
                [
                    'name'        => 'errorUrl',
                    'description' => 'url куда будет перенаправлен клиент после не удачной оплаты',
                ],
            ]
        ]) ?>
        <h2 class="page-header">Уведомление о заказе</h2>
        <p>Уведомление может приходить в двух форматах</p>
        <p>1. по почте</p>
        <p>2. по HTTP протоколу</p>
        <h2 class="page-header">Уведомление по почте</h2>
        <p>Если включено уведомление по почте, то после подтверждения факта платежа, приходит уведомление</p>
        <h2 class="page-header">Уведомление по HTTP</h2>
        <p>Если включено уведомление по HTTP, то после подтверждения факта платежа, приходит уведомление по HTTP протоколу путем отправки уведомления через POST запрос. Уведомление о заказе приходит по адресу указонному вами в личном кабинете в формате POST.</p>
        <pre>
[
    'id' => '356',
    'data' => 'product_id=152&order_id=789',
    'price' => '0.00018439',
    'label' => 'Заказ #789',
    'created_at' => '1487971776',
    'paid_at' => '1487971790',
    'secret' => '9ZsM6lceEbGoiNHOsBBPyBacszAqGLDfJ1zhsKcH',
    'currency' => 'RUB',
    'paysystem_id' => '1',
    'transaction' => '{"txid":"625c7ca75f93af88e719328c33c9122cad3c638239ec08dfb4f1393bb672cfd4"}',
]</pre>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заказа на оплату в банке-мерчанте',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'paysystem_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'secret',
                    'isRequired'  => true,
                    'description' => 'Секретный ключ посылаемый сервером Аватар Банка на точку приема уведомлений платежей',
                ],
                [
                    'name'        => 'label',
                    'isRequired'  => true,
                    'description' => 'Наименование платежа',
                ],
                [
                    'name'        => 'price',
                    'isRequired'  => true,
                    'description' => 'Стоимость товара, разделитель - точка',
                    'type'        => 'double',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'description' => 'Время создания заказа на оплату',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'paid_at',
                    'isRequired'  => true,
                    'description' => 'Время подтверждения оплаты',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'currency',
                    'isRequired'  => true,
                    'description' => 'Валюта которая была указана при выставлении счета',
                ],
                [
                    'name'        => 'transaction',
                    'isRequired'  => true,
                    'description' => 'Транзакция биткойн сети, если это оплата на счет или QR код. Идентификатор транзакции если это оплата по карте',
                ],
                [
                    'name'        => 'data',
                    'description' => 'Дополнительные данные для заказа, которые будут возвращены в ответе (поддтверждении), не более 2000 символов',
                ],
            ]
        ]) ?>

        <h2 class="page-header">Таблица bill_merchant</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name' => 'bill_merchant',
            'columns' => [
                [
                    'name'        => 'key',
                    'isRequired'  => true,
                    'description' => 'ключ API для идентификации мерчанта',
                ],
                [
                    'name'        => 'is_sms',
                    'type'        => 'tinyint',
                    'description' => 'Флаг показывающий установлена ли подтверждение по CMC (пока не используется) 0 - нет, 1 - да, по умолчанию - 0',
                ],
                [
                    'name'        => 'is_email',
                    'type'        => 'tinyint',
                    'description' => 'Флаг показывающий установлена ли подтверждение по CMC (пока не используется) 0 - нет, 1 - да, по умолчанию - 0',
                ],
                [
                    'name'        => 'is_http',
                    'type'        => 'tinyint',
                    'description' => 'Флаг показывающий установлена ли подтверждение по CMC (пока не используется) 0 - нет, 1 - да, по умолчанию - 0',
                ],
                [
                    'name'        => 'is_http_address',
                    'description' => 'Адрес на который будут отсылаться уведомления HTTP, обязательно если поле is_http = 1',
                ],
                [
                    'name'        => 'image_top',
                    'description' => 'Брендированная картинка, рекомендуемый размер 1600*500 px',
                ],
                [
                    'name'        => 'name',
                    'isRequired'  => true,
                    'description' => 'Название мерчанта',
                ],
                [
                    'name'        => 'confirmations',
                    'isRequired'  => true,
                    'description' => 'Кол-во подтверждений в Биткойн сети для транзакции после которых отсылаются уведомления, 0 - не подтверждена, 1 - записана в блок (подтверждена), по умолчанию = 0',
                ],
            ]
        ]) ?>

        <p><img src="/images/controller/development-avatar-bank/merchant/merchant_1.png"
                style="width: 100%; max-width: 1919px;"/></p>
        <p>
            <a href="/images/controller/development-avatar-bank/merchant/merchant_3.png" target="_blank">
                <img src="/images/controller/development-avatar-bank/merchant/merchant_3.png" style="width: 100%; max-width: 3286px;"/>
            </a>
        </p>
        <p><a href="https://drive.google.com/file/d/0BzHYNoEyPNTXOF9ZVFV0UENhbFE/view?usp=sharing" target="_blank">Редактировать</a></p>
        <h2 class="page-header">Пример</h2>
        <p>
            http://avatar-bank.localhost/merchant/index?
            key=V9oly0J-0lLlkzdbLNnvWzdjGv2uNfYAzkJO2N2R&
            price=1000&
            title=%D0%90%D0%BA%D1%81%D0%B5%D0%BB%D0%B5%D1%80%D0%B0%D1%82%D0%BE%D1%80%20%D0%BC%D0%BE%D0%B7%D0%B3%D0%B0
        </p>
        <h2 class="page-header">Порядок от заявки до прихода денег</h2>
        <ul>
            <li>На странице <code>/merchant/index</code> показывается сводная информация для оплаты.</li>
            <li>Клиент нажимает подтвердить и переходит на страницу <code>/merchant/step2</code>.</li>
            <li>Здесь он видит список платежных систем для оплаты.</li>
            <li>Клиент нажимает выбрать и вызывается AJAX где формируется заявка на оплату `merchant_request`</li>
        </ul>
        <p>
            Можно оплатить картой Аватар Банка введя номер, после этого нужно будет ввести СМС.
        </p>
        <p>
            На следующем этапе выводится интерфейс для оплаты выбранным способом. `/merchant/step3`.
        </p>
        <h2 class="page-header">Порядок от заявки до прихода денег</h2>
        <p>
            Человек оплатил по QR коду. Подтверждение об оплате может прийти через пару минут. И сообщение об операции нужно создавать не во время ее прихода,
            так как это может быть через 2 минуты а по времени
            транзакции.
        </p>
        <h2 class="page-header">Шаги</h2>
        <div class="row">
            <div class="col-lg-4">
                <img src="/images/controller/development-avatar-bank/merchant/Screenshot_1.png" width="100%">
            </div>
            <div class="col-lg-4">
                <img src="/images/controller/development-avatar-bank/merchant/Screenshot_3.png" width="100%">
            </div>
            <div class="col-lg-4">
                <img src="/images/controller/development-avatar-bank/merchant/Screenshot_4.png" width="100%">
            </div>
        </div>
        <p>
            Решение: ...
        </p>
        <h2 class="page-header">Заявка на оплату заказа</h2>
        <p>
            Таблица <code>merchant_request</code>
        </p>
        <?= \avatar\services\Params::widget([
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'идентификатор',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'merchant_id',
                    'isRequired'  => true,
                    'description' => 'идентификатор мерчанта',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'paysystem_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'paysystem_config_id',
                    'isRequired'  => true,
                    'description' => 'идентификатор платежной системы',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'billing_id',
                    'isRequired'  => true,
                    'description' => 'идентификатор счета',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'created_at',
                    'isRequired'  => true,
                    'description' => 'Время создания заявки',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'title',
                    'description' => 'Описание заявки',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'label',
                    'description' => 'Идентификатор заказа',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'data',
                    'description' => 'Данные заказа',
                    'type'        => 'varchar(4000)',
                ],
                [
                    'name'        => 'price',
                    'description' => 'Цена для перевода',
                    'type'        => 'double(15,10)',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'Валюта в которой выставлен был счет',
                    'type'        => 'double(15,10)',
                ],
                [
                    'name'        => 'is_paid',
                    'description' => 'Оплачено?',
                    'type'        => 'tinyint(1)',
                ],
                [
                    'name'        => 'is_mail_send',
                    'description' => 'Отправлено письмо?',
                    'type'        => 'tinyint(1)',
                ],
                [
                    'name'        => 'user_id',
                    'description' => 'Пользователь для которого был выставлен счет',
                    'type'        => 'int(11)',
                ],
            ]
        ]) ?>
        <h3 class="page-header">Настройка мерчанта</h3>
        <p>Настройка платежной системы осуществляется на странице <code>/cabinet-merchant/paysystem</code>.</p>
        <p>Параметры id - идентификатор мерчанта, paysysytem_id - идентификатор платежной системы</p>
        <p><b>Ethereum</b></p>
        <p>Условия получения и учета: </p>
        <p>Показывается переведите # эфира на счет ####.</p>
        <p>В ответ клиент пишет идентификатор транзакции.</p>
        <p>Создается таблица для сопряжения с заказами <code>payments_ethereum</code></p>
        <p>Модель <code>\common\models\PaymentEthereum</code></p>
        <p>Список платежных систем:</p>



        <h2 class="page-header">Смена структуры</h2>
        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXcm1iaE9iazFlZkE/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/merchant/new.png" class="thumbnail" width="100%"/>
            </a>
        </p>
        <p>Соединение <code>user_bill.id</code> = <code>bill_merchant.id</code> не поддерживается.</p>
        <h3 class="page-header">Проведение ICO</h3>
        <p>
            Клиент может купить любое кол-во токенов.
            Закзазы отображаются в списке заказов.
            Попасть в него можно в меню пользователя нажав на пункт "Заказы".
        </p>
        <p class="alert alert-warning">Предположение</p>
        <p>Делается при помощи мерчанта. Заказы формируются при каждом заказе.</p>


        <h3 class="page-header">Процедура покупки токенов</h3>


    </div>
</div>



