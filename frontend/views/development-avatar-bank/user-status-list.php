<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Статусы пользователей';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</p>

        <h2 class="page-header">Описание</h2>
        <p>Верификация представляет собой процедуру проверки идентификационных данных указанны при верификации со сканом паспорта.</p>
        <p>Эта процедура предоставлят безопасность хранения персональных даннных.</p>

        <h2 class="page-header">Анализ</h2>
        <p>Пользователь может верифицироваться по паспорту но не верифицироваться по ДНК</p>
        <p>Пользователь может верифицироваться по паспорту и верифицироваться по ДНК</p>
        <p>Пользователь может не верифицироваться по паспорту и верифицироваться по ДНК</p>
        <p>Пользователь может не верифицироваться по паспорту и не верифицироваться по ДНК</p>
        <p>По этому верификация по паспорту и по ДНК это отдельные флаги для пользователя</p>

        <h2 class="page-header">Решение</h2>
        <p>Таблица <code>user</code>.</p>
        <p>Поле у клиента <code>verified_passport</code>.</p>
        <p>Поле у клиента <code>verified_dnk</code>.</p>

        <h2 class="page-header">Алгоритм верификации по паспорту</h2>

        <p>
            <a href="https://drive.google.com/file/d/0BzHYNoEyPNTXeUpGTjhvNXhJdW8/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/user-status-list/passport-verify.png" >
            </a>
        </p>

        <h2 class="page-header">Безопасность хранения персональных данных</h2>


    </div>
</div>



