<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'ETH Token Залог';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p class="alert alert-danger">В разработке</p>


        <p>Есть Клиент 1 и Клиент 2</p>
        <p>Клиент 1 - тот кто закладывает</p>
        <p>Клиент 2 - кто принимает закладывает</p>
        <p>Схема</p>
        <p>Клиент 2 открывает залоговую ячейку для счета токена внутри страницы токена</p>
        <p>Вводит email клиента 1 и пароль и на сколько дней блокировать счет</p>
        <p>Клиенту 1 приходит письмо с кодом разблокировки токенов если чтото пойдет не так.</p>
        <p>Клиенту 2 видит при этом <code>Деньги заблокированы на # дней для совершения сделки</code></p>
        <p>Клиенту 2 видит кнопку "вернуть токены обратно".</p>

        <h2 class="page-header">Таблица</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name' => 'user_zalog',
            'columns' => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'owner_billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета хозяина',
                ],
                [
                    'name'        => 'owner_user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор клиента хозяина',
                ],
                [
                    'name'        => 'partner_billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета партнера, куда будут возвращаться токены',
                ],
                [
                    'name'        => 'partner_user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор клиента партнера, кому будут возвращаться токены',
                ],
                [
                    'name'        => 'finish',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент завершения сделки',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания сделки',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'varchar(70)',
                    'isRequired'  => true,
                    'description' => 'Кол-во токенов для блокировки в копейках',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Статус заказа',
                ],
            ]
        ]) ?>

        <h2 class="page-header">Счета</h2>
        <p>В "счетах" счет, который находится в залоге будет прозрачный и без возможности перевести деньги. Если залог действует.</p>
        <p><img src="/images/controller/development-avatar-bank/eth-token-zalog/zalog.png" class="thumbnail" style="width: 100%; max-width: 1000px;"> </p>

        <p>В самом счете не будет кнопок "вызвать контракт". А кнопка "Залог" будет вести на инфу о залоге.</p>
        <p><img src="/images/controller/development-avatar-bank/eth-token-zalog/2018-02-23_17-41-05.png" class="thumbnail" style="width: 100%; max-width: 422px;"> </p>
        <p>Страница залога</p>
        <p><img src="/images/controller/development-avatar-bank/eth-token-zalog/2018-02-23_17-33-36.png" class="thumbnail" style="width: 100%; max-width: 1182px;"> </p>

        <h2 class="page-header">Отзыв токенов</h2>

        <p>Если клиент нарушил условия, то партнер может отозвать токены.</p>

        <p>Есть список залогов, их видит и тот кто заложил и кому заложили.</p>

        <p>Там же можно и отозвать токены.</p>
        <h2 class="page-header">Статус заказа</h2>
        <p>0 - Сделка активна</p>
        <p>1 - Токены возвращены</p>


    </div>
</div>



