<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Путь регистрации';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <p>
            <a href="https://drive.google.com/file/d/0B9lrcmdS-YbTU2gwS05xTkExSWs/view?usp=sharing" target="_blank">
                <img src="/images/controller/development-avatar-bank/path-registration/new.png" width="100%" class="thumbnail">
            </a>
        </p>

    </div>
</div>




