<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Токены MWC';



?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p>

        </p>
        <p>
            <img src="/images/controller/development-avatar-bank/ar/00011.jpg" width="100%">
        </p>

        <h3 class="page-header">Личный Кабинет Аватара</h3>

        <?= \common\services\documentation\DbTable::widget([
            'name' => 'mwc',
            'columns' => [
                [
                    'name'        => 'bill_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета user_bill.id',
                ],
                [
                    'name'        => 'counter',
                    'type'        => 'int',
                    'description' => 'Счетчик, сколько операций было произведено после запрета',
                ],
            ]
        ]) ?>


    </div>
</div>



