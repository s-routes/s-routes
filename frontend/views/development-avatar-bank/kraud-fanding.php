<?php
use common\services\UsersInCache;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'Краудфандиг';

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <p>Структура данных</p>
        <p>Сбор денег происходит через Биткойн</p>
        <p>Деньги зачисляются на счет не доступный пользователю (<code>user_id</code> = null) а пароль задается
            случайным и отсылается на почту админа краудфандинга.</p>
        <p>После завершения времени краудфандинга возможны два варианта:</p>
        <p>1. Если сумма собрана, то админу отсылается письмо админу. Он вводит пароль. Пароль переназначает
            пользователь и ему присваивается счет. 5% уходит на счет банка.</p>
        <p>2. Если сумма не собрана, то админу отсылается письмо. Он вводит пароль. Деньги отсылаются инвесторам
            обратно. Инвесторы вводят счет куда им переслать деньги в случае неудачи краудфандинга.</p>
        <p>При размещении проекта нужно указать сумму в рублях или в BTC.</p>

        <h2 class="page-header">Таблица проектов</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'projects',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор проекта',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'key',
                    'description' => 'Идентификатор проекта, случайная строка из 10 символов',
                    'type'        => 'VARCHAR(10)',
                ],
                [
                    'name'        => 'content',
                    'description' => 'Текст описания проекта',
                    'type'        => 'TEXT',
                ],
                [
                    'name'        => 'user_id',
                    'description' => 'Идентификатор пользователя кому принадлежит проект',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'is_verified',
                    'description' => 'Флаг. Прошел врификацию? 0 - не прошел верификацию, 1 - прошел верификацию успешно',
                    'type'        => 'TINYINT',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка для проекта. 1600*500 обрезается',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'time_start',
                    'description' => 'Время начала краудфандинга',
                    'type'        => 'INT',
                ],
                [
                    'name'        => 'time_end',
                    'description' => 'Время окончания краудфандинга',
                    'type'        => 'INT',
                ],
                [
                    'name'        => 'sum',
                    'description' => 'Сумма необходимая для сбора денег',
                    'type'        => 'DOUBLE',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'Идентификатор валюты по таблице ' . Html::tag('code', 'currency'),
                    'type'        => 'int',
                ],
            ]
        ]) ?>
        <h2 class="page-header">Таблица подарков проекта</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'   => 'projects_gifts',
            'params' => [
                [
                    'name'        => 'id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор проекта',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'project_id',
                    'isRequired'  => true,
                    'description' => 'Идентификатор проекта',
                    'type'        => 'int',
                ],
                [
                    'name'        => 'name',
                    'description' => 'Наименование подарка',
                    'type'        => 'VARCHAR(10)',
                ],
                [
                    'name'        => 'content',
                    'description' => 'Текст описания подпрка',
                    'type'        => 'TEXT',
                ],
                [
                    'name'        => 'image',
                    'description' => 'Картинка для проекта. 300*300 обрезается',
                    'type'        => 'varchar(255)',
                ],
                [
                    'name'        => 'sum',
                    'description' => 'Сумма вложений',
                    'type'        => 'DOUBLE',
                ],
                [
                    'name'        => 'currency',
                    'description' => 'Идентификатор валюты по таблице ' . Html::tag('code', 'currency'),
                    'type'        => 'int',
                ],
            ]
        ]) ?>
    </div>
</div>



