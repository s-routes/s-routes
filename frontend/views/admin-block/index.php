<?php
use common\models\avatar\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\AdminBlock */

$this->title = 'Блокировка сайта';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model' => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin/index';
    }).modal();
}
JS
,
        ]);

        ?>
<?php
$this->registerJs(<<<JS
$('#adminblock-param').change(function (e) {
    var value;
    if ($(this).prop('checked')) {
        value = 1;
    } else {
        value = 0;
    }
    ajaxJson({
        url: '/admin-block/set',
        data: {value: value},
        success: function (ret) {
            console.log(1);
        }
    });
});
JS
)
?>

        <?= $form->field($model, 'param') ?>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['isHide' => true]); ?>


    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>