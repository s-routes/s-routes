<?php

/** @var $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Управление DNS зонами';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <ul id="dns">
        </ul>

        <p>PROD - <?= Yii::$app->params['cloudflare']['ip']['prod'] ?></p>
        <p>RESERV - <?= Yii::$app->params['cloudflare']['ip']['reserv'] ?></p>

        <?php
        $this->registerJs(<<<JS
$('#buttonProd').click(function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/admin-cloudflare/prod',
            data: {'id': 1},
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();              
            }
        })
    }
});
$('#buttonReserv').click(function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/admin-cloudflare/reserv',
            data: {'id': 1},
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();              
            }
        })
    }
});

ajaxJson({
    url: '/admin-cloudflare/get-status',
    data: {'id': 1},
    success: function(ret) {
        for(i=0;i<ret.length;i++) {
            $('#dns').append($('<li>').html(ret[i].name + ': ' + ret[i].ip))
        }          
    }
})
JS
)
        ?>

        <p><button class="btn btn-default" id="buttonProd">Установить на PROD</button></p>
        <p><button class="btn btn-default" id="buttonReserv">Установить на RESERV</button></p>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>