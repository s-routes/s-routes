<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все языки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <a href="<?= Url::to(['admin-languages/add']) ?>" class="btn btn-default">Добавить</a>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-languages/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});


$('.rowTable').click(function() {
    window.location = '/admin-languages/edit' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Language::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img(\iAvatar777\widgets\FileUpload7\FileUpload::getFile($i, 'crop'), [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                'title',
                'name',
                'code',
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>