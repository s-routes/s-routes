<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Language */

$this->title = $model->name;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')): ?>

            <div class="alert alert-success">
                Успешно обновлено.
            </div>

        <?php else: ?>


            <div class="row">
                <div class="col-lg-5">
                    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model' => $model,
                        'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-languages/index';
    }).modal();
}
JS

                    ]); ?>
                    <?= $form->field($model, 'name') ?>
                    <?= $form->field($model, 'title') ?>
                    <?= $form->field($model, 'code') ?>
                    <?= $form->field($model, 'image') ?>
                    <?= $form->field($model, 'status')->dropDownList([
                        \common\models\Language::STATUS_EDIT => 'На редактировании',
                        \common\models\Language::STATUS_DONE => 'Production',
                    ], ['prompt' => '- Ничего не выбрано -']) ?>


                    <hr>
                    <?php \iAvatar777\services\FormAjax\ActiveForm::end(['title' => 'Обновить']); ?>

                </div>
            </div>
        <?php endif; ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>