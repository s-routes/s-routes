<?php

/** @var $this \yii\web\View  */
/** @var $request \common\models\shop\Request */

use cs\services\DatePeriod;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\shop\Request;


$this->title = 'Заказ #' . $request->id;


function getHtml($text)
{
    $rows = explode("\n", $text);
    $rows2 = [];
    foreach ($rows as $row) {
        $arr = explode(' ', $row);
        $arr2 = [];
        foreach ($arr as $word) {
            $w = trim($word);
            if (StringHelper::startsWith($w, 'https://') or StringHelper::startsWith($w, 'http://') or StringHelper::startsWith($w, 'blob:http')) {
                $w = Html::a($w, $w, ['target' => '_blank']);
                $arr2[] = $w;
            } else {
                $arr2[] = Html::encode($w);
            }
        }
        $rows2[] = join(' ', $arr2);
    }

    return join('<br>', $rows2);
}
?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.buttonPaid').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите действие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-school-shop-requests/paid' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});

JS
    );
    $c = \common\models\avatar\Currency::findOne($request->currency_id);
    $user = \common\models\UserAvatar::findOne($request->user_id);
    $d = \common\models\shop\DeliveryItem::findOne($request->dostavka_id);
    $billing = \common\models\BillingMain::findOne($request->billing_id);


    ?>


    <?= \yii\widgets\DetailView::widget([
        'model'      => $request,
        'attributes' => [

            'price'       => [
                'label'          => 'Стоимость без доставки',
                'value'          => \common\models\piramida\Currency::getValueFromAtom($request->price, \common\models\piramida\Currency::RUB),
                'format'         => ['decimal', $c->decimals],

            ],
            'currency_id' => [
                'label'          => 'Валюта',
                'value'          => Html::tag('span', $c->code, ['class' => 'label label-info']),
                'format'         => 'html',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'user_id'     => [
                'label'          => 'Заказчик',
                'value'          => Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $user->getName2()]),
                'format'         => 'html',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],
            'created_at'  => [
                'label'          => 'Создан',
                'value'          => $request->created_at,
                'format'         => 'datetime',
                'captionOptions' => [
                    'style' => 'width: 30%;',
                ],
            ],

            'is_paid_client'     => [
                'label'          => 'Оплачен? (клиент)',
                'value'          => !\cs\Application::isEmpty($request->txid_coin) ? Html::tag('span', 'Да', ['class' => 'label label-success']) : Html::tag('span', 'Нет', ['class' => 'label label-default']),
                'format'         => 'html',
            ],

            'comment'     => [
                'label'          => 'Комментарий',
                'format'         => 'html',
                'value'          => getHtml($request->comment),
            ],
            'name'     => [
                'label'          => 'Имя',
                'value'          => $request->name,
            ],
            'phone'     => [
                'label'          => 'Телефон',
                'value'          => $request->phone,
            ],
        ],
    ]) ?>
    <h2 class="page-header">Товары</h2>
        <?php
        $rows = \common\models\shop\RequestProduct::find()->where(['request_id' => $request->id])->asArray()->all();
        $ids = ArrayHelper::getColumn($rows, 'product_id');
        $products = \common\models\shop\Product::find()->where(['id' => $ids])->all();
        $p = ArrayHelper::map($products,'id', function ($item) { return $item; });
        $r2 = [];
        foreach ($rows as $r) {
            $d = $r;
            $d['product'] = $p[$r['product_id']];
            $r2[] = $d;
        }
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels'  => $r2,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'product.image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],
                [
                    'header'  => 'Наименование',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'product.name', '');
                        if ($i == '') return '';

                        return $i;
                    }
                ],
                [
                    'header'  => 'Цена',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'product.price', '');
                        if ($i == '') return '';

                        return Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($i, \common\models\piramida\Currency::RUB), 2);
                    }
                ],
                [
                    'header'  => 'Кол-во',
                    'attribute'  => 'count',
                ],
            ],
        ]) ?>


        <?= $this->render('../cabinet-exchange2/chat', [
            'room_id'  => $request->chat_id,
            'user_id'  => Yii::$app->user->id,
            'send_url' => '/cabinet-school-shop-requests/send',
        ]); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Сообщение</h4>
            </div>
            <div class="modal-body">
                <textarea class="form-control" rows="10"></textarea>
                <hr>
                <button class="btn btn-primary" style="width:100%;" id="buttonSendMessageForm">Отправить</button>
            </div>
        </div>
    </div>
</div>