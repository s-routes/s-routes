<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Наш канал в телеграм';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
        <p class="text-center"><a href="tg://resolve?domain=iAvatarChannel"><img src="/images/controller/telegram/group/2019-02-09_18-13-50.png" class="img-circle" width="200"></a></p>
        <p class="text-center"><a href="tg://resolve?domain=iAvatarChannel" class="btn btn-success">@iAvatarChannel</a></p>
    </div>
</div>


