<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все доверенные представители';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-trust/delete',
            data: {id: id},
            success: function (ret) {
                infoWindow('Успешно', function() {
                    button.parent().parent().remove();
                });
            }
        });
    }
});

JS
        );
        $ids = \common\models\Config::get('trusted-users');
        if (\cs\Application::isEmpty($ids)) $ids = [];
        else $ids = \yii\helpers\Json::decode($ids);
        $rows = \common\models\UserAvatar::find()->where(['id' => $ids])->all();
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows
            ])
                ,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns' => [
                    'id',
                    [
                        'header'  => 'Аватар',
                        'content' => function ($item) {
                            $user = \common\models\UserAvatar::findOne($item['id']);

                            return Html::img($user->getAvatar(), [
                                'class'  => "img-circle",
                                'width'  => 60,
                                'height' => 60,
                                'style'  => 'margin-bottom: 0px;',
                                'title' => $user->getName2(),
                                'data' => [
                                    'toggle' => 'tooltip',
                                ],
                            ]);
                        },
                    ],
                    [
                        'header'  => 'Удалить',
                        'content' => function ($item) {
                            return Html::button('Удалить', [
                                'class' => 'btn btn-danger btn-xs buttonDelete',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);
                        }
                    ],
            ]

        ]) ?>

        <?php

        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-trust/index';
    }).modal();
}
JS
,
        ])
        ?>
        <?= $form->field($model, 'id') ?>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end() ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>