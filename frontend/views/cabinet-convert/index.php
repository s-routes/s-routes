<?php

/** $this \yii\web\View  */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'История конвертации';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/cabinet-convert/item' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\BinanceOrder::find()
                    ->where(['user_id' => Yii::$app->user->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'ИЗ',
                    'content' => function ($item) {
                        $userBill = UserBill::findOne($item['billing_id']);
                        $cExt = \common\models\avatar\Currency::findOne($userBill->currency);
                        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $cExt->id]);
                        $cInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                        $a = $item['amount'];
                        $am = bcdiv($a, pow(10, $cInt->decimals), $cInt->decimals);

                        return $am;
                    }
                ],
                [
                    'header'  => 'ИЗ',
                    'content' => function ($item) {
                        $userBill = UserBill::findOne($item['billing_id']);
                        $c = \common\models\avatar\Currency::findOne($userBill->currency);

                        return Html::tag('span', $c->code, ['class' => 'label label-info']);
                    }
                ],
                [
                    'header'  => 'В',
                    'content' => function ($item) {
                        $a = $item['amount_to'];
                        if (is_null($a)) return '';
                        $cExt = \common\models\avatar\Currency::findOne($item['currency_to']);
                        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $cExt->id]);
                        $cInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);
                        $am = bcdiv($a, pow(10, $cInt->decimals), $cInt->decimals);

                        return $am;
                    }
                ],
                [
                    'header'  => 'В',
                    'content' => function ($item) {
                        $c = \common\models\avatar\Currency::findOne($item['currency_to']);

                        return Html::tag('span', $c->code, ['class' => 'label label-info']);
                    }
                ],
                [
                    'header'  => Yii::t('c.xaREK0rYPp', 'Создано'),
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Статус',
                    'content' => function ($item) {
                        if ($item['status'] == \common\models\BinanceOrder::STATUS_CREATED) {
                            return Html::tag('span', 'Создан', ['class' => 'label label-default']);
                        }
                        if ($item['status'] == \common\models\BinanceOrder::STATUS_FILLED) {
                            return Html::tag('span', 'Исполнен', ['class' => 'label label-success']);
                        }
                        return '';
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>


    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>