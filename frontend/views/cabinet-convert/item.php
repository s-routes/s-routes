<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $item \common\models\BinanceOrder */

$this->title = $item->id;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">
        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => [
                'id',
                'binance_id',
                'binance_order_id',
                'created_at:datetime',
                'status',
                'amount',
                'billing_id',
                'tx_from_id',
                'tx_to_id',
                'currency_to',
            ],
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>