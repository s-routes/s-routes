<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\validate\CabinetRequestAdd */
/** @var $request \common\models\shop\Request */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Доплатить за заказ №' . $request->id;

$cMarket = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::MARKET);
$cRub = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::RUB);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-shop-requests/view?id=' + {$request->id};
    }).modal();
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'comment') ?>
        <p>Оплата будет в MARKET</p>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>