<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Заказы (магазин)';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-shop-requests/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
    );
    $sort = new \yii\data\Sort([
        'defaultOrder' => ['created_at' => SORT_DESC],
    ]);
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\shop\Request::find()
                ->where(['user_id' => Yii::$app->user->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => $sort,
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            [
                'header'  => 'Оплачено? (клиент)',
                'content' => function ($item) {
                    $request = $item;

                    if ($request->is_paid_client) {
                        $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                    } else {
                        $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    }

                    return $html;
                },
            ],
            [
                'header'  => 'Оплачено? (магазин)',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'is_paid', 0);

                    if ($v) {
                        $html = Html::tag('span', 'Да', ['class' => 'label label-success']);
                    } else {
                        $html = Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    }

                    return $html;
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'  => 'Последнее сообщение',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message_time', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'         => 'Стоимость',
                'headerOptions'  => ['class' => 'text-right'],
                'contentOptions' => ['class' => 'text-right'],
                'content'        => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'price', 0);
                    if ($v == 0) return '';
                    $currency_id = \yii\helpers\ArrayHelper::getValue($item, 'currency_id');
                    if (is_null($currency_id)) {
                        return '';
                    }
                    $c = \common\models\avatar\Currency::findOne($currency_id);
                    $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                    return Yii::$app->formatter->asDecimal($v / 100, 2) . $cHtml ;
                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>