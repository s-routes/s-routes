<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\GoogleTest2 */

$this->title = 'Вы успешно отписались от рассылки';

\cs\assets\JqueryForm\Asset::register($this);

$this->registerJs(<<<JS
$('#myForm').ajaxForm(function() {
    alert("Thank you for your comment!");
});
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <form id="myForm" action="comment.php" method="post">
            Name: <input type="text" name="name" />
            Comment: <textarea name="comment"></textarea>
            <input type="submit" value="Submit Comment" />
        </form>
    </div>
</div>
