<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Тестирование';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>используется <a href="https://codeception.com/for/yii" target="_blank">codeception</a></p>
        <p>Команда запуска:</p>
        <pre>./vendor/bin/codecept run</pre>

        <p>Сборка конфига для frontend происходит из файлов:</p>
        <ol>
            <li>avatar-bank/config/main.php</li>
            <li>common/config/main.php</li>
        </ol>
        <p>Чтобы менять конфигурацию приложения, надо настраивать файл <code>.env</code></p>
        <p>Особенность. При запуске пришлось сконфигурировать в файле <code>avatar-bank/config/main.php</code> параметр <code>components.assetManager.basePath</code> на значение <code>@avatar/../public_html/assets</code>. Без него выдавал ошибку и при значении <code>@webroot/assets</code>, хотя файл с ассоциациями (alias) подключается в самом начале <code>avatar-bank/config/test.php</code></p>

        <p>PHP UP | Практика: Cоздаем Instagram: урок №7 ч.1 | Тестирование с Yii 2 и Codeception</p>
        <p>https://www.youtube.com/watch?v=XGNoiPqa6PI</p>

        <p>Изучаем Codeception #1. Установка Yii2 с Codeception</p>
        <p>https://www.youtube.com/watch?v=WivaGekEfL0&ab_channel=ShelestKonstantin</p>

    </div>
</div>



