<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Telegram Bot';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Алгоритм определения зыка в боте</h2>
        <p>При начале вывода вызывается скрипт <code>/admin-buh/start</code> для того чтобы присоединить бухгалтера, чтобы повторный вызов уже не дал сделать это
        и к тому же заявки где бухгалтер уже начал обрабатывать, там не будет кнопки "выполнить".</p>

        <h2 class="page-header">Бот для NEIRON и TOPMATEONE и e-LOTTERY</h2>
        <p>существуют дубликаты полей и таблиц:</p>
        <p>user.telegram_username - user.telegram_shop_username - telegram_lot_username</p>
        <p>user.telegram_chat_id - user.telegram_shop_chat_id - telegram_lot_chat_id</p>
        <p>user_telegram_connect - user_telegram_shop_connect - user_telegram_lot_connect</p>
        <p>user_telegram_temp - user_telegram_shop_temp - user_telegram_lot_temp</p>
        <p>\common\models\UserTelegramTemp - \common\models\UserTelegramShopTemp - \common\models\UserTelegramLotTemp</p>
        <p>\common\models\UserTelegramConnect - \common\models\UserTelegramShopConnect - \common\models\UserTelegramLotConnect</p>
        <p>Компонент: <code>Yii::$app->telegram</code> - <code>Yii::$app->telegramShop</code> - <code>Yii::$app->telegramLot</code></p>

        <h2 class="page-header">callback</h2>
        <p>Для основного бота callback стоит на https://neiro-n.com/telegram/call-back</p>
        <p>Для TOPMATEONE callback стоит на https://topmate.one/telegram/call-back</p>
        <p>Для e-LOTTERY callback стоит на https://neiro-n.com/telegram/call-back-lot</p>

    </div>
</div>


