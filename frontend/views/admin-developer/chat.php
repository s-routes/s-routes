<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Чат';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Путь на сервере где лежит проект: <code>/var/www/s-routes-chat/www</code></p>
        <p>В автозапуске прописано <code>@reboot /var/www/s-routes-chat/start.sh</code></p>
        <p>Файл содержит запуск ноды:</p>
        <pre>#!/bin/sh
cd /var/www/s-routes-chat/www/
node index > /var/www/s-routes-chat/log-node/node.log 2>&1</pre>


        <h2 class="page-header">Установка Чата</h2>

        <h3 class="page-header">Установка NodeJS</h3>

        <p><a href="https://www.digitalocean.com/community/tutorials/node-js-ubuntu-18-04-ru" target="_blank">https://www.digitalocean.com/community/tutorials/node-js-ubuntu-18-04-ru</a></p>

        <p>Устанавливаем библиотеку</p>
        <pre>npm install socket.io</pre>
        <p>Устанавливаем наш чат</p>
        <pre>cd /var/www</pre>
        <pre>git clone git@github.com:dram1008/s-routes-chat.git</pre>

        <h2 class="page-header">Общие сведения</h2>
        <p>Боевой чат работает на порту 3005 <code>http://chat.s-routes.com</code></p>
        <p>Тестовый чат работает на порту 3006 <code>http://chat-test.s-routes.com</code></p>

        <h2 class="page-header">БД</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'chat_message',
            'description' => 'Чат',
            'model'       => '\common\models\exchange\ChatMessage',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'message',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'сообщение, формат текстовый с переносом строк',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
            ],
        ]) ?>
        <p><code>/avatar-bank/views/cabinet-exchange/deal-action-chat.php</code></p>

        <h2 class="page-header">Чат</h2>
        <p><code>/cabinet-exchange-chat/send</code> - отправка<br>
            <code>/cabinet-exchange-chat/get</code> - получение чата</p>
        <hr style="margin-top: 200px;">

        <h2 class="page-header">Виды событий</h2>
        <p><b>От сервера</b></p>
        <p><code>user-connected</code> - вызывается после <code>new-user</code></p>
        <p><code>push</code> - вызываетс после <code>send-chat-message</code> в нулевую комнату</p>
        <p><code>chat-message</code> - вызывается после <code>send-chat-message</code> в комнату</p>
        <p><code>reload-status</code> - </p>
        <p><code>list-notifications-new-item</code> - вызываетс после <code>new-notification</code> в нулевую комнату</p>

        <p><b>От клиента</b></p>
        <p><code>new-room</code> - </p>
        <p><code>new-user</code> - </p>
        <p><code>send-chat-message</code> - </p>

        <p><code>need-arbitr</code> - Вызывается когда пользователь нажимает кнопку пригласить арбитра.</p>


        <p><code>new-notification</code> - </p>
        <p><code>get-notifications</code> - </p>
        <p><code>sell-open-accept</code> - (deal, user_id, status)</p>
        <p><code>buy-open-accept</code> - (deal, user_id, status)</p>
        <p><code>sell-money-sended</code> - (deal, user_id, status)</p>
        <p><code>buy-money-sended</code> - (deal, user_id, status)</p>
        <p><code>sell-money-received</code> - (deal, user_id, status)</p>
        <p><code>buy-money-received</code> - (deal, user_id, status)</p>
        <p><code>sell-assessment-down</code> - (deal, user_id, status)</p>
        <p><code>buy-assessment-down</code> - (deal, user_id, status)</p>
        <p><code>sell-assessment-normal</code> - (deal, user_id, status)</p>
        <p><code>buy-assessment-normal</code> - (deal, user_id, status)</p>
        <p><code>sell-assessment-up</code> - (deal, user_id, status)</p>
        <p><code>buy-assessment-up</code> - (deal, user_id, status)</p>


        <h2 class="page-header">Реализация на стороне клиента</h2>
        <p>Уведомления висят в файле <code>avatar-bank/views/layouts/_footer.php</code></p>
        <p>Чат висит в файле <code>avatar-bank/views/cabinet-exchange/deal-action.php</code>
            <code>avatar-bank/views/cabinet-exchange/deal-action-chat.php</code>
        </p>

        <h2 class="page-header">Чат общий</h2>

        <p>Комната болталки <code>exchange1</code></p>
        <p>Комната обменника <code>exchange2</code></p>

        <pre>socket.emit('chat1-new-user', roomName, myid);</pre>
        <pre>socket.emit('chat1-message', roomName, {$user_id}, message_text);</pre>

        <p>Сделка 2000000001 чат болталка<br>
            2000000002 чат обмениника</p>

        <p>Банить - может читать, но не может писать<br>
            удалить - не может читать и не может писать</p>

        <p>Роль <code>admin_chat</code> админ чата.</p>

        <h3 class="page-header">Удаление сообщения</h3>
        <p>AJAX <code>/cabinet-exchange-chat/delete</code></p>
        <p>После удаления сообщения, посылается событие <code>chat1-delete</code> и у всех удаляется сообщение.</p>
        <pre>socket.emit('chat1-delete', roomName, $(this).data('id'));</pre>

        <h3 class="page-header">Бан пользователя</h3>
        <p>AJAX <code>/cabinet-exchange-chat/ban</code></p>
        <pre>socket.emit('chat1-ban', roomName, ret.user_id);</pre>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_ban',
            'description' => 'Список бана',
            'model'       => '\common\models\UserBan',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'chat_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор чата/сделки',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Тип 1 - бан, 2 - удалено',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
            ],
        ]) ?>
        <p>У пользователя перегружается страница</p>

        <h3 class="page-header">Удаление пользователя</h3>
        <p>AJAX <code>/cabinet-exchange-chat/delete-user</code></p>
        <pre>socket.emit('chat1-delete-user', roomName, ret.user_id);</pre>
        <p>У пользователя перегружается страница</p>

        <h3 class="page-header">Чат в предбаннике</h3>
        <p>В предбаннике идентификатора сделки нет, значит надо сделать создание сделки при нажатии кнопки "купить/продать". А как сделать чтобы при обновлении был тот же чат?</p>
        <p>Если я сохраню в сессии то повторное нажатие кнопки приведет в туже сделку.</p>

        <h3 class="page-header">Чат в тет-а-тет</h3>
        <p>Делаю сущность чат "тет-а-тет"</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'chat_tet',
            'description' => 'Чат тет-а-тет',
            'model'       => '\common\models\ChatTet',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user1_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя 1. user1_id < user2_id',
                ],
                [
                    'name'        => 'user2_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя 2. user1_id < user2_id',
                ],
                [
                    'name'        => 'room_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор комнаты в NodeJS. в БД сохраняется целое значение. а в NodeJS комната называется [room{room_id}]',
                ],
                [
                    'name'        => 'last_message',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент Последнего сообщения',
                ],
            ],
        ]) ?>



        <p>в БД сохраняется целое значение. а в NodeJS комната называется <code>room{room_id}</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'chat_room',
            'description' => 'Комната',
            'model'       => '\common\models\ChatRoom',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'last_message',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент Последнего сообщения',
                ],
            ],
        ]) ?>
        <p>файл <code>deal-action-chat2</code> генерирует форму для чата тет-а-тет. Входной параметр <code>room_id</code></p>

        <h3 class="page-header">Чат поддержки</h3>

        <pre>$uid = $this->chat_id - 2000000000;</pre>
        <p>Если клиент пишет первый раз то уведомляются все админы, Если нет то только тот что уже отвечал.</p>

    </div>
</div>


