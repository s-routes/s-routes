<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Prizm нода';

$asset = Yii::$app->assetManager->getBundle('\common\assets\App\Asset');
$baseUrl = $asset->baseUrl;
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Кабинет</h2>
        <p><code>/cabinet/packet</code></p>
        <p><img src="<?= $baseUrl ?>/controller/admin-developer/prizm/2019-10-29_13-04-05.png"></p>

        <h2 class="page-header">Письмо от Александра</h2>
        <p><code>http://localhost:9976/test</code> - посмотреть все доступные команды и протестировать их</p>
        <p><code>http://127.0.0.1:7742/index.html</code> - Web Interface</p>
        <p><code>/root/prizm-dist/conf/prizm.default.properties</code> - конфигурация</p>
        <p><a href="http://blockchain.prizm.space/api-doc/PRIZM_API.html" target="_blank">http://blockchain.prizm.space/api-doc/PRIZM_API.html</a></p>
        <p><a href="https://nxtwiki.org/wiki/The_Nxt_API" target="_blank">https://nxtwiki.org/wiki/The_Nxt_API</a> - А тут пояснение по командам</p>
        <p><a href="https://bitcointalk.org/index.php?topic=2052831.msg42175386#msg42175386" target="_blank">https://bitcointalk.org/index.php?topic=2052831.msg42175386#msg42175386</a> - Это пояснение по JSON</p>

        <pre>Доброе время суток Святослав!
-----------------------------------------------------------------------------------------------------------------------------
 http://localhost:9976/test - посмотреть все доступные команды и протестировать их

 Так как PRIZM написан на NXT, то и его команды можно посмотреть. Большинство подходит: http://testnxt.jelurida.com:6876/test

 Вот тут идут примеры запросов и ответов: https://nxtwiki.org/wiki/The_Nxt_API_Examples

 А тут пояснение по командам: https://nxtwiki.org/wiki/The_Nxt_API

 Это пояснение:  https://bitcointalk.org/index.php?topic=2052831.msg42175386#msg42175386  - по JSON
-----------------------------------------------------------------------------------------------------------------------------
VPS server (SSH)
IP: 5.187.3.161
username: root
password: ***
-------------------------------------------
Web Interface
https://127.0.0.1:7742/index.html
http://127.0.0.1:7742/index.html
-------------------------------------------
конфигурация:
/root/prizm-dist/conf/prizm.default.properties

-
Alex(Soft) corporation</pre>

        <p><a href="http://prizm-api.s-routes.com" target="_blank">http://prizm-api.s-routes.com</a></p>
        <p>GIT:mc <code>https://github.com/dram1008/s-routes-prizm-api.git</code></p>
        <p>на сервере установлен: <code>/root/prizm-dist</code></p>

        <p>
            <a href="https://www.draw.io/#G14r-xtaMj_ZSxQnFLi2bgimkh6v3yLIEc" target="_blank">
                <img src="/images/controller/admin-developer/prizm/prizm.png">
            </a>
        </p>

        <p><a href="https://yadi.sk/d/lYDEou6mmXhKBA" target="_blank">https://yadi.sk/d/lYDEou6mmXhKBA</a> - Установка nginx and php7-fpm</p>
        <p><a href="http://prizm-api.s-routes.com/site/prizm777" target="_blank">http://prizm-api.s-routes.com/site/prizm777</a> - если передать сюда запрос через GET команды то будет выдан ответ в виде JSON</p>

        <h2 class="page-header">Как работать с нодой програмным путем</h2>
        <p>Нужно отправить GET запрос сюда <code>http://localhost:7742/prizm</code></p>

        <h2 class="page-header">Сделать внесение монет призм в наш фонд</h2>
        <p>Все действие происходит на странице <code>http://s-routes.com/cabinet/packet</code></p>
        <p>Проверка прихода денег при пополнении фонда:</p>
        <p>
            <a href="https://www.draw.io/#G1B9t2reDvOoSn6DNN2h81NLS2W24plMcS" target="_blank">
                <img src="/images/controller/admin-developer/prizm/check1.png">
            </a>
        </p>
        <p>Модель поведения при вводе призма:</p>
        <p>
            <a href="https://www.draw.io/#G1odA-iWvUkoA82kbtZDn3dCX7RjlAbit-" target="_blank">
                <img src="/images/controller/admin-developer/prizm/model-input.png">
            </a>
        </p>
        <p>Карта состояний фонда призм:</p>
        <p>
            <a href="https://www.draw.io/#G1ZR2rY5gnikD7GSHgfHM6YmmKf6_4DtKh" target="_blank">
                <img src="/images/controller/admin-developer/prizm/map_state.png">
            </a>
        </p>

        <h2 class="page-header">Как проверить статус ноды что она успешно синхронизирована</h2>
        <p>Проверяется по высоте блоков.</p>
        <p>как считать высоту блоков?</p>
        <p><code>requestType=getBlockchainStatus</code> Пример ответа:</p>
        <pre>[
    'apiProxy' => false
    'correctInvalidFees' => true
    'ledgerTrimKeep' => 30000
    'maxAPIRecords' => 100
    'blockchainState' => 'UP_TO_DATE'
    'currentMinRollbackHeight' => 748200
    'numberOfBlocks' => 749623
    'isTestnet' => false
    'includeExpiredPrunable' => true
    'isLightClient' => false
    'services' => []
    'requestProcessingTime' => 0
    'version' => '1.10.3.18'
    'maxRollback' => 800
    'lastBlock' => '9441054303559410644'
    'application' => 'PZM'
    'isScanning' => false
    'isDownloading' => false
    'cumulativeDifficulty' => '801802807165551'
    'lastBlockchainFeederHeight' => 749622
    'maxPrunableLifetime' => 7776000
    'time' => 40602273
    'lastBlockchainFeeder' => '109.173.20.103'
]</pre>
        <p>параметр <code>numberOfBlocks</code> показывает какой сейчас номер блока в сети внутри нашей ноды.</p>
        <p><code>https://prizm-api.s-routes.com/site/prizm777?requestType=getBlocks&lastIndex=1</code></p>

        <h2 class="page-header">Сделать ввод PRIZM в фонд</h2>
        <p>Для того чтобы ввсести призмы в фонд нужно создать задачу.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'task_prizm_input',
            'description' => 'Задача по вводу призм в фонд',
            'model'       => '\common\models\TaskPrizmInput',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'account',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Адрес кошелька призм с которого производилась транзакция',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор транзакции по которой клиент завел свои призмы',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус задачи 0 - задача создана, клиент скинул деньги. 1 - задача подтверждена магазином. 2 - задача отклонена магазином',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания задачи',
                ],
                [
                    'name'        => 'txid_internal',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции внутренней по зачислению монет LETN на кошелек польщователя',
                ],
                [
                    'name'        => 'txid_internal_letm',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции внутренней по зачислению монет LETM на кошелек польщователя',
                ],
                [
                    'name'        => 'finished_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент закрытия задачи, то есть принятия решения о принятии задачи или отклонении',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов которые были переведены в PZM',
                ],
                [
                    'name'        => 'amount_letm',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов letm',
                ],
                [
                    'name'        => 'amount_letn',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов letn',
                ],
            ],
        ]) ?>


        <p>Клиент переводит монеты в фонд и ему зачисляются монеты SB</p>
        <p>
            <a href="https://www.draw.io/#G1O9j8EHf3V7a2wUxy1cgl-5H7Joghwjp6" target="_blank">
                <img src="/images/controller/admin-developer/prizm/in.png">
            </a>
        </p>

        <h2 class="page-header">PrizmApi</h2>

        <p>Предназначено для того чтобы общаться с нодой.</p>
        <p>
            <a href="https://www.draw.io/#G14r-xtaMj_ZSxQnFLi2bgimkh6v3yLIEc" target="_blank">
                <img src="/images/controller/admin-developer/prizm/prizm-api.png">
            </a>
        </p>
        <pre>/** @var \common\services\PrizmApi $PrizmApi */
$PrizmApi = Yii::$app->PrizmApi;
$response = $PrizmApi->post('request', []);</pre>
        <p>Для того чтобы им пользоваться надо добавить в главную конфигурацию приложения компонент <code>PrizmApi</code> в файле <code>/common/config/main.php</code>.</p>
        <pre>return [
    'components' => [
        'PrizmApi'   => [
            'class' => '\common\services\PrizmApi',
        ],
    ],
];
</pre>
        <p>В файле <code>/common/config/main-local.php</code> добавить ключ.</p>
        <pre>return [
    'components' => [
        'PrizmApi'   => [
            'key' => '***********************',
        ],
    ],
];
</pre>

        <h2 class="page-header">Сделать проверку приема PRIZM</h2>
        <p><code>\avatar\models\validate\PrizmInputCheck</code></p>
        <pre>/** @var \common\services\PrizmApi $PrizmApi */
$PrizmApi = Yii::$app->PrizmApi;
$response = $PrizmApi->post('request', []);</pre>

        <p>Проверяются последние 20 транзакций.</p>
        <pre>'requestProcessingTime' => 9
'transactions' => [
0 => [
    'senderPublicKey' => '6b94a98e9ccf6c4c73d4f44e917d66adc1a8285ae2a6420e6e67723e4ee7aa79'
    'signature' => '1aeb52d17d3c2b09d1b4421d7fe74f6714223b3875e0d0bb38cdbbc7c8171f0b89c4c39559700664ab7be04714981068383a0e99e5d421689e98fdf223b09fd7'
    'feeNQT' => '5'
    'transactionIndex' => 3
    'type' => 0
    'confirmations' => 54911
    'fullHash' => 'd0d9d0444f88d68813a4bcf1ef62c45297cc51be86ab76651dc4d0a548c5841a'
    'version' => 1
    'ecBlockId' => '14398214579928914643'
    'signatureHash' => '1efb9f2a768fef1388e8edd08ab4c0191312f8cfcd66d1716dcb831b84e04094'
    'attachment' => [
        'encryptedMessage' => [
            'data' => '4422ed5e25ad763a236e459cae2fa8aeee91d20bc6b890063a7e408984e777e8133f22f05b9d3371a8e5c07fe28aa19f'
            'nonce' => '58e8fd68baaf3f505b3bf4fcfd6a589be1b361f7275568345ced2e2c05dd0b12'
            'isText' => true
            'isCompressed' => true
        ]
        'encryptToSelfMessage' => [
            'data' => '66a25f7e4f1d24b1ccb7c44a183c26941683c91bab73f0a1e8d9008944ca1692ce3e4dae6f1cd71304ac3fb9bad2c859'
            'nonce' => '77448b66c34c5f94f378c05b4b3bffb29d051a790d23e74e4822d95250110ca7'
            'isText' => true
            'isCompressed' => true
        ]
        'version.EncryptedMessage' => 1
        'version.PublicKeyAnnouncement' => 1
        'recipientPublicKey' => '759cc07249a9a45f063a2615f144931bb6fc5219385a496fb13df4bafea8812e'
        'version.EncryptToSelfMessage' => 1
        'version.OrdinaryPayment' => 0
    ]
    'senderRS' => 'PRIZM-TKWH-ZHSY-P8ZY-CREEE'
    'subtype' => 0
    'amountNQT' => '300'
    'sender' => '16735940354766624691'
    'recipientRS' => 'PRIZM-GPN2-8CZ7-PNYP-8CEHG'
    'recipient' => '12199704192462947044'
    'ecBlockHeight' => 695243
    'block' => '6521468641580754259'
    'blockTimestamp' => 37286237
    'deadline' => 1440
    'transaction' => '9860218308212611536'
    'timestamp' => 37286206
    'height' => 695964
]
1 => [</pre>

        <p>Возможны два варианта: 1. Если пользователь указал кошелек. 2. Если пользователь указал транзакцию.</p>
        <h4 class="page-header">Если пользователь указал кошелек</h4>
        <p>Ищу транзакцию где <code>senderRS</code> равен указанному. Причем нужно исключить из сравнения те транзакции которые ранее уже могли участвовать в зачислении.</p>
        <p>как определить список транзакций которые уже были успешными? там где <code>task_prizm_input.status</code> = 1.</p>
        <p>Монеты будут начисляться с кошелька который указан в переменной <code>Yii::$app->params['walletSBall']</code>.</p>
        <p>Статусы возвращаемых значений:</p>
        <pre>0 - Ничего не найдено
1 - Найдено и зачислено
2 - Пользователь указал в задаче транзакцию а она уже была обработана как положительная</pre>

        <h2 class="page-header">Сделать Внесение SB в пул</h2>
        <p>
            <a href="https://www.draw.io/#G1BdLcPOP1bbV3QGXYyJuaZXMvwPlSP6mp" target="_blank">
                <img src="/images/controller/admin-developer/prizm/pull-input.png">
            </a>
        </p>
        <p>Делаю таблицу для учета внесения в пул</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'task_prizm_pull_input',
            'description' => 'Задача по внесение SB в пул',
            'model'       => '\common\models\TaskPrizmPullInput',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя который вносит',
                ],
                [
                    'name'        => 'txid_sb',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции по снятию SB',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во монет в атомах',
                ],
                [
                    'name'        => 'txid_sbp',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции по добавлению SBP',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания задачи и исполнения',
                ],
            ],
        ]) ?>
        <p>По кнопке делается запрос <code>/cabinet-task-input/pull-input</code>.</p>
        <p>В нем списываются монеты SB в общий кошелек, выдаются из общего кошелька SBP и формируется задача по внесению в пул <code>\common\models\TaskPrizmPullInput</code></p>
        <p>Кошелек который указан в <code>Yii::$app->params['walletSBPall']</code> это кошелек со всеми SBP монетами откуда выдаются монеты.</p>
        <p>Когда списываются SB то пишется коментарий
            <code>Списание в связи с внеснием в пулл по задаче id=</code></p>
        <p>Когда начисляются SBP то пишется комментарий
            <code>Начисление в связи с внеснием в пулл по задаче id=</code></p>

        <h2 class="page-header">Сделать счетчик увеличение баллов PZM и начисление каждый день</h2>
        <p>Начислять нужно по 0.15% в день.<br>
            Начисление делается в 23:59 МСК.<br>
            Значит смотрю баланс на 00:00 и он него начисляю процент 0,15 в 23:59.<br>
            если в течение дня еще были доначисления то от времени начисления до 23,59 начисляю процент на эту сумму доначисления.<br>
            Если было снятие в этот день в T1 неполностью то начисляю процент 0,15 от 0:00 до T1 на сумму полную и от T1 до 23:59 на неполную сумму.<br>
            Если было снятие всех монет то ничего не будет начислено.</p>
        <p>на странице сделаю начисление каждую 0,1 сек то есть 100 мс.
            Значит формула такая:<br>
            balance = баланс кошелька<br>
            за день = balance * 1.0015<br>
            за милисекунду = (balance * 1.0015 - balance)/864000
        </p>
        <p>Параметр который определяет кол-во процентов в день находится здесь <code>Yii::$app->params['prizm']['mining-percent']</code></p>

        <p>Консольный контроллер который будет начислять монеты в 23:59 <code>\console\controllers\FundController</code></p>
        <p>команда вызова <code>php yii prizm/mining</code></p>
        <p>Логика программы:
            собираю все кошельки у которых баланс больше 0 и кроме главного кошелька
            и прохожусь циклом по ним алгоритмом что выше описан</p>
        <pre>0 0 * * * /usr/bin/php /var/www/54.38.54.52/yii fund/add-sbp</pre>

        <h2 class="page-header">Вывод призма</h2>
        <p>
            <a href="https://www.draw.io/#G11neoWemFFKCxk39lJHtEP7owy6vfU14q" target="_blank">
                <img src="/images/controller/admin-developer/prizm/prizm-exit.png">
            </a>
        </p>
        <p><a href="https://yadi.sk/i/-UyBRzShwFpDvg" target="_blank">https://yadi.sk/i/-UyBRzShwFpDvg</a></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'task_prizm_output',
            'description' => 'Задача по выводу призм из фонда',
            'model'       => '\common\models\TaskPrizmOutput',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'account',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Адрес кошелька призм на который надо вывести деньги',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор транзакции по которой будут выведены призмы',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус задачи 0 - задача создана. 1 - задача выполнена, транзакция указана',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета с которого списывать монеты',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов для вывода, разрядность после запятой 2',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания задачи',
                ],
                [
                    'name'        => 'finished_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент закрытия задачи, то есть принятия решения о принятии задачи или отклонении',
                ],
                [
                    'name'        => 'txid_internal',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции внутренней по списасанию монет SB/SBP c кошелька пользователя',
                ],
            ],
        ]) ?>
        <p>Задача: Вывести монеты SB или SBP в призм. Значит нужно чтобы человек указал кошелек для вывода.</p>
        <p>Бизнес процесс: Клиент нажимает вывести монеты, создается заявка, уведомление приходит бухгалтеру, он берет заявку в работу, отправляет монеты, указывает транзакцию на вывод в заявке и пишет выполнено. На этом заявка закрывается.</p>
        <p>Страница формы вывода: <code>/cabinet-prizm/out</code></p>
        <p>Роль бухгалтера <code>role_buh</code></p>
        <p>Контроллер:  <code>\avatar\controllers\AdminBuhController</code></p>
        <p>Какие статусы у заявки на вывод могут быть?<br>
            0 - заявка создана клиентом<br>
            1 - успешно пререведены ПРИЗМЫ и указана транзакция (txid) + finished_at + txid_internal<br>
            2 - скрыта бухгалтером, потому что не актуальна.</p>
        <p>На странице <code>/admin-buh/index.php</code> указываются только заявки которые требуют обработки<br>
            На странице <code>/admin-buh/index-all.php</code> указываются все заявки</p>

        <p>Письмо для бухгалтера:</p>
        <p><img src="/images/controller/admin-developer/prizm/2019-11-30_14-48-54.png" width="100%" class="thumbnail"></p>

        <p>Письмо для клиента:</p>
        <p><img src="/images/controller/admin-developer/prizm/2019-11-30_14-58-55.png" width="100%" class="thumbnail"></p>

        <h2 class="page-header">Проверить статус ноды что она успешно синхронизирована</h2>
        <p>Нужно сравнивать с прогнозом или сравнивать что идет прогресс хотябы</p>
        <pre>php yii prizm/ping</pre>
        <p>Если высота последнего блока остает больше чем на 20 блоков то отправляется уведомление</p>
        <p>params['prizmAdmin'] - в этот параметр записывается массив почт куда будет отправлено письмо в случае ошибки.</p>
        <p><code>params['prizm']['blockDeltaAlert']</code> - разница между нашим блокчейном и внешним. кол-во блоков после которого отсылается письмо.</p>

        <hr style="margin-top: 200px;">
    </div>
</div>


