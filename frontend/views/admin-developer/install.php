<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Инсталяция проекта';


?>
<div class="container">
    <div class="col-lg-12">
        <h1>Инсталяция проекта</h1>
        <h2>Новый сервер</h2>
        <p>1. Сгенерировать ключ</p>
        <pre>ssh-keygen</pre>
        <pre>cat /root/.ssh/id_rsa.pub</pre>
        <p>2. Добавить ключ в репозиторий bitbucket.org:s-routes/s-routes.git и git@bitbucket.org:Ra-m-ha/s-routes-env-test.git</p>
        <p>3. Склонировать репозиторий в домашнюю папку</p>
        <pre>cd ~</pre>
        <pre>git clone git@bitbucket.org:s-routes/s-routes.git</pre>
        <p>4. Склонировать репозиторий в домашнюю папку</p>
        <pre>cd ~</pre>
        <pre>git clone git@bitbucket.org:Ra-m-ha/s-routes-env-test.git</pre>
        <p>5. Скопировать содержимое репозитория s-routes-env-test.git в папку s-routes</p>
        <p>6. выполнить скрипт <code>install.sh</code></p>
        <pre>cd s-routes</pre>
        <pre>chmod a+x install.sh</pre>
        <pre>./install.sh $1 $2 $3</pre>
        <p>$1 - <code>PROD_DB_TOKEN</code></p>
        <p>$2 - .env.prod-ovh51.210.242.237</p>
        <p>$3 - <code>MYSQL_PASSWORD</code></p>
        <p>$4 - <code>файл .env</code></p>
        <p>7. Скопировать БД (s_routes_prod_main.sql s_routes_prod_stat.sql s_routes_prod_wallet.sql) проекта в корневую папку проекта и выполнить</p>
        <pre>./install_db.sh $1</pre>
        <p>$1 - <code>PROD_DB_TOKEN</code></p>
        <p>$2 - <code>сайт откуда брать БД</code></p>
        <p>$3 - <code>MYSQL_PASSWORD</code></p>

        <pre>./d_db_load.sh $1</pre>
        <p>$1 - <code>MYSQL_PASSWORD</code></p>
        <p>8. Прописать для домена neiro-n.com ip сервера, topmate.one, neironcoin.com.</p>
        <p>9. Скопировать файлы пользователей в папку ~/s-routes/public_html/upload.</p>
        <p>10. Прописать крон из файла <code>https://bitbucket.org/Ra-m-ha/s-routes-env-test/src/master/cron_prod.txt</code></p>
        <pre>sudo crontab -e</pre>

        <h2>БД</h2>
        <p>
            <a href="https://app.diagrams.net/#G16qxjKDkHcZYyEI6rmtGDrl4eV7BgtMwq" target="_blank">
                <img src="/images/controller/admin-developer/install/db_copy.png">
            </a>
        </p>

        <h2>PRIZM server</h2>

    </div>
</div>
