<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'env';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php
        $rows = [
            [
                'yii_code'    => 'prod',
                'yii_debug'   => false,
                'dns'         => 's-routes.com',
                'ip'          => '54.38.54.52',
                'db'          => 'av_network_prod',
                'dbStatistic' => 'av_network_prod_stat',
                'dbWallet'    => 's_roues_wallet',
                'chat'        => 'chat.s-routes.com',
            ],
            [
                'yii_code'    => 'test',
                'yii_debug'   => true,
                'dns'         => 'stage.s-routes.com',
                'ip'          => '95.216.174.204',
                'db'          => 'av_network3_prod',
                'dbStatistic' => 'av_network3_prod_stat',
                'dbWallet'    => 's_roues3_wallet',
                'chat'        => 'chat-stage.s-routes.com',
            ],
            [
                'yii_code'    => 'dev',
                'yii_debug'   => true,
                'dns'         => 'test888.s-routes.com',
                'ip'          => '95.216.174.204',
                'db'          => 'av_network2_prod',
                'dbStatistic' => 'av_network2_prod_stat',
                'dbWallet'    => 's_roues2_wallet',
                'chat'        => 'chat-test.s-routes.com',
            ],
            [
                'yii_code'    => 'dev',
                'yii_debug'   => true,
                'dns'         => 'test999.s-routes.com',
                'ip'          => '95.216.174.204',
                'db'          => 'av_network_prod',
                'dbStatistic' => 'av_network_prod_stat',
                'dbWallet'    => 's_roues_wallet',
                'chat'        => 'chat-test.s-routes.com',
            ],
        ];

        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'columns'      => [
                [
                    'attribute' => 'yii_code',
                    'header'    => 'yii_code',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['yii_code']);
                    }
                ],
                [
                    'attribute' => 'yii_debug',
                    'header'    => 'yii_debug',
                    'content' => function ($item) {
                        if ($item['yii_debug']) {
                            $html = Html::tag('span', 'true', ['class' => 'label label-success']);
                        } else {
                            $html = Html::tag('span', 'false', ['class' => 'label label-default']);
                        }

                        return $html;
                    },
                ],
                [
                    'attribute' => 'dns',
                    'header'    => 'dns',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['dns']);
                    }
                ],
                [
                    'attribute' => 'ip',
                    'header'    => 'ip',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['ip']);
                    }
                ],
                [
                    'attribute' => 'db',
                    'header'    => 'db',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['db']);
                    }
                ],
                [
                    'attribute' => 'dbStatistic',
                    'header'    => 'dbStatistic',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['dbStatistic']);
                    }
                ],
                [
                    'attribute' => 'dbWallet',
                    'header'    => 'dbWallet',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['dbWallet']);
                    }
                ],
                [
                    'attribute' => 'chat',
                    'header'    => 'chat',
                    'content'   => function ($item) {
                        return Html::tag('code', $item['chat']);
                    }
                ],
            ]
        ]) ?>
    </div>
</div>
