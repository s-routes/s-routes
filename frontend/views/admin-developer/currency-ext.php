<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Монета внешняя, чьято';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Концепт</h2>
        <p>Для монеты формируется пул ликвидности, по факту конвертации в маркет с пула списывается часть.
            Пул ликвидности в USDT.
            Пул ликвидности это кошелек USDT куда зачисляются монеты по факту перечисления налички нам.
            Пул ликвидности (wid) указывается в параметре <code>currency_io.extended_info.pool_usdt</code></p>
        <p>Для просмотра статистики есть роль управляющего монетой.
            Список этих людей будет определяться таблицей <code>currency_admin</code>.
            В своей админке они будут смотреть сделки.</p>

    </div>
</div>
