<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Языки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Google captcha</h2>
        <p><a href="https://dle-faq.ru/faq/common/21612-kak-pomenyat-yazyk-vyvoda-recaptcha.html" target="_blank">Перевод</a></p>



        https://app.diagrams.net/#G1gKa_0z0iMuppNNt9xdeDcB4IXsSpArPf
        <h2 class="page-header">Перевод страниц</h2>
        <p>Сделаю таблицу <code>page_lang</code> <code>\common\models\PageLang</code></p>
        <ul>
            <li>id</li>
            <li>parent_id</li>
            <li>language</li>
            <li>name</li>
            <li>content</li>
        </ul>
        <h2 class="page-header">Перевод стран</h2>
        <p>Сделаю таблицу <code>country_lang</code> <code>\common\models\СountryLang</code></p>

        <h2 class="page-header">Перевод коментариев в кощельке</h2>
        <p>Сделаю сохранение в коментариях операции в JSON</p>
        <pre>["Начисление процентов за {date}", "10.10.2020"]</pre>

        <h2 class="page-header">Перевод коментариев в кошельке</h2>
        <pre>["зачисление средств от сделки {deal} в пользу пользователя {user} в кошелек {wallet}", {"deal":681, "user":4198, "wallet":808}]</pre>
        <p>В переводах нужно оставлять то что в фигурных скобках не тронутым, то есть не переводить.</p>
        <p>Где происходит вывод? <code>cabinet-wallet/item?id=13178</code></p>
        <p>Если я при создании транзакции буду указывать новый вид коментария, то при выводе мне надо просто учитывать. Если JSON то перевожу.</p>
        <p>Значит нужно сделать категорию для переводимых транзакций. Это будет в корне "Транзакции".</p>

        <h2 class="page-header">Перевод бота</h2>
        <p>В виду того что нужно отправлять не себе (кто находится в сессии) а другому человеку то язык в сессии становится не доступен, поэтому ввожу доп поле к пользователяю <code>language</code>. В нем сохраняется язык, если не установлен и если отличается от записанного. Запись происходит в функции <code>Application::beforeRequest</code></p>
        <p>Если у пользователя не установлен язык, то используется "русский".</p>
        <pre>$language = (Application::isEmpty($User->language)) ? 'ru' : $User->language;</pre>

        <h2 class="page-header">Перевод статусов в сделке</h2>

        <p>При обновлении страницы, нужно выводить через Yii в PHP.
            статусы хранятся в комментарии <code>deal_status.comment</code>. Значит в этом поле нужно хранить ключ статуса.
            а сами статусы где брать? c.38DjBe9FdT</p>
        <p>Ключ хранится в JSON формате</p>
        <pre>["Сделка открыта #{deal}", {"deal": 2}]</pre>
        <p>При выводе статусов, если JSON то делаю перевод, иначе просто вывожу.</p>

    </div>
</div>


