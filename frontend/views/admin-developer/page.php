<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Редактирование страниц';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'page',
            'description' => 'Страница',
            'model'       => '\common\models\exchange\Page',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Название',
                ],
                [
                    'name'        => 'content',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'HTML страницы',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания',
                ],
            ],
        ]) ?>


        <hr style="margin-top: 200px;">
    </div>
</div>


