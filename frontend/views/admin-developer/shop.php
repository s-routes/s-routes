<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Магазин';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Заказ</h2>
        <p>При заказе отправляется код для того чтобы подписать договор оферты. Код сохраняется в переменной сессии в
            переменной <code>dogovor_code</code>.</p>
        <p>При формированиии заказа он сохраяется в переменной сессии <code>request</code></p>


        <h2 class="page-header">Каталог</h2>
        <p>один товар может быть только в одном разделе</p>

        <h2 class="page-header">Оформление корзины</h2>
        <p>Оформление корзины означает что если в корзине несколько продуктов, то оформляется заказ со всеми этими
            продуктами.</p>
        <p>Оформление происходит по ссылке <code>/shop/order</code> без параметров.</p>
        <p>После оплаты корзина лчищается.</p>
        <p>Если корзина пустая, а клиент нажимает оформить то что делать? Показать что бы покупал.</p>

        <h2 class="page-header">Доверенное лицо</h2>
        <p>Еще один вопрос: При заполнении данных о заказчике они берутся из профиля. А они записываются отдельно для
            каждого заказа?</p>
        <p>Сейчас все данные записываюстя в Заказ в доп поле <code>gs_users_shop_requests.dop</code></p>
        <p><code>country_id</code> - Страна<br>
            <code>town</code> - город<br>
            <code>address</code> - адрес</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'country',
            'description' => 'Список стран',
            'model'       => '\common\models\Country',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Наименование',
                ],
            ],
        ]) ?>
        <p><img src="/images/controller/admin-developer/shop/2019-11-24_18-43-24.png" class="thumbnail" width="100%"></p>
        <p>Данные в сессии:</p>
        <pre>'address' => [
    'address' => 'Белореченская 1 кв 16'
    'comment' => ''
    'phone' => ''
    'name_first' => 'Святослав'
    'name_last' => 'Архангельский'
    'town' => 'Москва'
    'country_id' => '1'
    'dov' => [
        'name_first' => ''
        'name_last' => ''
        'country_id' => ''
        'email' => ''
        'town' => ''
        'address' => null
        'phone' => ''
    ]
]</pre>


        <h2 class="page-header">Форма оплаты</h2>
        <p>
            <a href="https://www.draw.io/#G1iwyseHB8y8O_U0KdZSOTsC6ls59gFp4p" target="_blank">
                <img src="/images/controller/admin-developer/shop/pay.png" class="thumbnail" width="100%">
            </a>
        </p>


        <p>https://app.diagrams.net/#G1gKY5m00o6xCzYIDb-UVxtkuIoQ8JDQso</p>
        <h2 class="page-header">Список магазинов</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'shop_all',
            'description' => 'Магазин',
            'model'       => '\common\models\ShopAll',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'link',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Ссылка',
                ],
                [
                    'name'        => 'description',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Описание',
                ],
                [
                    'name'        => 'about',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Описание полное',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Тип \common\models\ShopAll::TYPE_*',
                ],
                [
                    'name'        => 'type',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Тип \common\models\ShopAll::TYPE_*',
                ],
                [
                    'name'        => 'account_login',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Аккаунт в магазине, логин',
                ],
                [
                    'name'        => 'account_password',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Аккаунт в магазине, пароль',
                ],
                [
                    'name'        => 'rekvisit_karti',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'реквизиты банковской карты (для быстрого пополнения баланса аккаунта)',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Заказ</h2>
        <p><a href="https://app.diagrams.net/#G1E1X-mbC71vU_obLQBuI8Q3icXRFmbohZ" target="_blank">https://app.diagrams.net/#G1E1X-mbC71vU_obLQBuI8Q3icXRFmbohZ</a> </p>
        <p><img src="/images/controller/admin-developer/shop/1.png" width="100%"> </p>
        <p>Данные заказа сохраняются в переменной сессии <code>shop-request</code></p>

        <p>Список переменных в заказе</p>
        <?= \avatar\services\Params::widget([
            'params'     => [
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency.id',
                ],
                [
                    'name'        => 'link',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Ссылка на товар',
                ],
            ],
        ]) ?>


        <h2 class="page-header">Роль бухгалтера</h2>
        <p><code>role_shop_buh</code> <code>permission_shop_buh</code></p>

        <h2 class="page-header">Кнопка доплатить</h2>
        <p>Если я нажимаю доплатить, то значит добавляется заявка на доплату. По факту успеха заявки на доплату выводится список заявок на доплату в заказе.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'shop_request_add',
            'description' => 'Заявка на доплату',
            'model'       => '\common\models\ShopRequestAdd',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'request_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заявки в магазин',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов для доплаты',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета db.billing',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кому принадлежит заказ',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Время создания заявки',
                ],
            ],
        ]) ?>
        <p>По нажанию на кнопку человек перенаправляется на форму создания заявки на пополнение. Форма располагается по ссылке <code>/cabinet-shop-requests/add</code></p>

        <h2 class="page-header">Отмена заказа</h2>
        <p>Клиент может отменить уже сформированный заказ полностью, - если заказ еще не взят в работу менеджером. Оплата/оплаты полностью возвращается на счета клиента (и маркеты, и рубли).</p>
        <p>Eсли заказ уже взят в работу менеджером - клиент сам уже не может отменить заказ (кнопки возврата у клиента - неактивны).  Такой заказ может отменить теперь только сам менеджер. Оплата может быть возвращена полностью или частично - это уже на усмотрение менеджера. У менеджера есть 2 кнопки: 1.”ОТМЕНА ЗАКАЗА ПОЛНОСТЬЮ” - заказ снимается, а оплата/оплаты полностью возвращается на счета клиента (и маркеты, и рубли); 2.”ОТМЕНА ЗАКАЗА ЧАСТИЧНО” - заказ снимается, а оплата/оплаты возвращается на счета клиента частично (возможно нужно будет удержать часть средств за что-то).</p>
        <p>Вычет</p>
        <p>Если пользователь оплачивал в рублях то и вычет в них, если пользователь оплачивал в маркетах то и вычет в них</p>
        <p>а)при ПОЛНОМ возврате: средства возвращались на те счета, с которых они были списаны при оплате заказа.</p>

        <p>б)при ЧАСТИЧНОМ возврате: средства возвращались на тот счет, на который менеджер посчитает необходимым вернуть. </p>

        <h2 class="page-header">Платное оформление если выбираетя 1 или 2 пункт</h2>
        <p>Если при оформлении заказа выбираетя 1 или 2 пункт то к стоимости заказа прибавляется 300 руб.</p>
        <p><code>\common\base\Shop::isPaidManager()</code> - функция Выдает ответ "нужно ли прибавлять к заказу 300 руб. для оплаты работы менеджера"</p>

        <h2 class="page-header">Оплата мобильника</h2>
        <p>Будет два шага: выбор оператора и оплата. /phone/step1 /phone/step2</p>

        <h2 class="page-header">MAR</h2>
        <p>shop_packet_coin_main_wallet - параметр</p>

        <h2 class="page-header">Окончание пакета</h2>
        <p>После окончания действия пакета нужно обнулять пакет. Этим будет заниматься комольная команда <code>yii packet/finish</code>. и вызываться она будет каждый день.</p>


        <hr style="margin-top: 200px;">
    </div>
</div>


