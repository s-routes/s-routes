<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Безопасность';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2  class="page-header">Сделать, чтобы нельзя было сломать систему (напр. нарисовать доп. монеты). Сверка контр.сумм и т.д.</h2>
        <p>В консоли вызывается:</p>
        <pre>php yii security/test-wallets</pre>
        <p>Что делается при нарушении контрольной суммы? Вводится <b>Чрезвычайное положение</b>. Во время чрезвычайного положения: <br>
            все страницы блокируются и показывается заглушка "Ведутся технические работы".</p>
        <p>Какие проверки проводятся:</p>
        <ul>
            <li>Проверяю балансы операций</li>
            <li>Проверяю Контрольную сумму в операциях</li>
            <li>Проверяю балансы в dbWallet.currency</li>
            <li>Проверяю Контрольную сумму в транзакциях</li>
        </ul>
        <p>Вызывается на PROD раз в 10 мин. Если происходит какая либо ошибка то об этом сразу приходит уведомление в бота NEIRON</p>



        <h2 class="page-header">Атаки и решения</h2>

        <h3>PROD упал</h3>
        <p>Решение: Запустить RESERV</p>
        <p>
            Инструкция: <br>
            - Зайти на STAGE<br>
            - Зайти на меню "Админ / Управление DNS зонами"<br>
            - Нажать "Установить на RESERV"
        </p>
        <p>После наладки нужно вернуть на PROD</p>

        <h3>DDOS атака</h3>
        <p>Проплатить защиту от DDOS</p>


        <h2 class="page-header">PING PROD</h2>

        <p>Вызывает проверку сервера PROD на доступность. Сервер должен выдать статус 200 на запрос страницы <code>https://neiron-n.com</code> через GET. Результат записывается в таблицу dbStatisctic.statistic_ping. <code>server_id</code> = 13. Если сервер не доступен то отправляется Телеграм уведомление всем админам. Команда вписывается в крон STAGE.</p>
        <pre>docker exec s-routes-php-fpm php yii security/ping</pre>
        <pre>* * * * * docker exec s-routes-php-fpm php yii security/ping</pre>


        <h2  class="page-header">Google captcha</h2>
        <p><a href="https://www.google.com/recaptcha/about/" target="_blank">https://www.google.com/recaptcha/about/</a></p>
        <p><a href="https://dle-faq.ru/faq/common/21612-kak-pomenyat-yazyk-vyvoda-recaptcha.html" target="_blank">Перевод</a></p>

        <h2  class="page-header">Реализация заглушки</h2>
        <p>Устанавливается параметр <code>catchAll</code> для объекта <code>yii\web\Application</code> он устанавливается из параметра файла <code>@frontend/config/catchAll.php</code>. Если ставится параметр <code>['offline/index']</code> то значит показывается заглушка. Она устанавливается в консольном файле.</p>

        <h2 class="page-header">Профилактический осмотр</h2>
        <p>Проводится раз в месяц. Для этого первого числа выдается сообщение админам что нужно провести осмотр в телеграм, напоминание.</p>
        <p>За уведомление отвечает команда </p>
        <pre>d_yii.sh security/check-view</pre>
        <p>На PROD указана cron запись</p>
        <pre># Делать Уведомление о проверке RESERV
0 0 1 * * docker exec s-routes-php-fpm php yii security/check-view</pre>


    </div>
</div>



