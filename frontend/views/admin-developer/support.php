<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Служба поддержки';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Поддержка организовывается в виде чата с админами. Один пользователь общается сразу со всеми админами с одном окошке. Админ видит каждого пользователя по отдельности и общается с каждым отдельно, причем в этом же чате сидят и все остальные админы.</p>
        <p>Идентификатор комнаты чата = 2000000000 + user_id</p>
        <p>chat-message2</p>
        <p>socket.on('chat-message2', (room, user_sender, message)</p>
        <p>socket.to(room).broadcast.emit('chat-message2', user_sender, message);</p>
        <p>Отправление сообщения: <code>/cabinet-support-chat/send</code></p>
        <p><b>Уведомление в телеграм</b></p>
        <p>Если написал админ Уведомляю пользователя<br>
Если написал пользователь то<br>
- Если из админов никто не написал? Уведомляю всех<br>
- Иначе Уведомляю тех кто уже в чате</p>

        <h2 class="page-header">Служба поддержки в магазине</h2>
        <p>Работает отдельно от поддержки NEIRON</p>

        <h3 class="page-header">Модель данных</h3>
        <p><code>support_shop</code> - список пользователей с которыми идет переписка</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'support_shop',
            'description' => 'Список пользователей с которыми идет переписка',
            'model'       => '\common\models\SupportShop',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'room_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор комнаты convert_operation.id',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'last_message',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время последнего ответа',
                ],
            ],
        ]) ?>

    </div>
</div>



