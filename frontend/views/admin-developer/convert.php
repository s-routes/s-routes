<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Конвертация';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Файлы</h2>

        <?php
        $rows = [
            [
                'file' => 'convert-neiro.php',
                'desc' => 'COIN / NEIRO',
                'coin' => 'BTC, LTC, DASH, ETH, TRX, BNB',
            ],
            [
                'file' => 'convert-neiro-binance.php',
                'desc' => 'COIN / NEIRO',
                'coin' => 'USDT, PZM, EGOLD',
            ],
            [
                'file' => 'convert-market.php',
                'desc' => 'COIN / MARKET',
                'coin' => 'BTC, LTC, DASH, ETH, TRX, BNB',
            ],
            [
                'file' => 'convert-market-binance.php',
                'desc' => 'COIN / MARKET',
                'coin' => 'USDT, PZM, EGOLD',
            ],
            [
                'file' => 'convert-market-coin.php',
                'desc' => 'MARKET / COIN',
                'coin' => 'NEIRO, USDT',
            ],
            [
                'file' => 'convert-neiro-coin.php',
                'desc' => 'NEIRO / COIN',
                'coin' => 'USDT',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
        ]) ?>

        <p>MarketConvertParams - проценты конвертации</p>
        <p>NeironConvertParams - проценты конвертации</p>


        <h2 class="page-header">Конвертация</h2>
        <p>
            <a href="https://www.draw.io/#G1H7I6JXCY3QcMV4XzPoUY4sIu2HReFM3o" target="_blank">
                <img src="/images/controller/admin-developer/exchange/exchange.png" class="thumbnail">
            </a>
        </p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'convert_operation',
            'description' => 'Операция конвертации',
            'model'       => '\common\models\ConvertOperation',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Временная метка создания операции',
                ],
                [
                    'name'        => 'wallet_from_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька dbWallet.wallet.id откуда будет проводиться конвертация у пользователя',
                ],
                [
                    'name'        => 'wallet_to_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька dbWallet.wallet.id куда будет проводиться конвертация пользователю',
                ],
                [
                    'name'        => 'kurs',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'соотношение курса * 1000000. Какой курс?',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Сколько атомов было списано с кошлька с которого проводилась конвертация',
                ],
                [
                    'name'        => 'amount_to',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Сколько атомов было зачислено на кошлек на который проводилась конвертация',
                ],
                [
                    'name'        => 'from_currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты с чего меняю dbWallet.currency.id',
                ],
                [
                    'name'        => 'to_currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты на что меняю dbWallet.currency.id',
                ],
                [
                    'name'        => 'to_currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты на что меняю dbWallet.currency.id',
                ],
                [
                    'name'        => 'from_price_usd',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'Цена валюлы FROM в USD на момент конвертации',
                ],
                [
                    'name'        => 'to_price_usd',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'Цена валюлы TO в USD на момент конвертации',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя, Кто менял',
                ],
            ],
        ]) ?>

    </div>
</div>
