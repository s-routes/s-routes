<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'UseCase. Автоматизированный вывод PRIZM';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <pre>чтобы сделать вывод надо вызвать <code>https://prizm-api.neiro-n.com/request/withdraw</code></pre>

        <p>amount - <br>
        address -<br>
        public_key -<br>
        comment - </p>

        <p>транзитный кошелек для вывода PZM: PRIZM-66WT-N9Y3-HN8S-Z929P</p>
        <p>https://pzm.space/pzm-integration/#rec140350273</p>
        <p>http://blockchain.prizm.space/api-doc/PRIZM_API.html</p>
        <p>git clone git@bitbucket.org:gsss/prizm-api.git</p>


    </div>
</div>


