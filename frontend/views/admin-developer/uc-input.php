<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'UseCase. Ввод крипты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://app.diagrams.net/#G1D_6UoonjA29pTDoLhK1vRLn3cHQNNoqz" target="_blank">
                <img src="/images/controller/admin-developer/uc-input/in.png"/>
            </a>
        </p>
        <pre>Исследование
заявка создается отдельно на модификацию или на валюту?
я думаю что на модификацию, потому что я могу независимо сделать две заявки по одной модификации и по другой
нормально</pre>
        <p>Страница ввода: <code>/cabinet-active/item?id=##</code></p>

        <p>Эта картинка поясняет когда какой поиск включается</p>
        <p>
            <a href="https://app.diagrams.net/#G1XnQPnL3rCITqxN9U7iJy6d0uMvyjjBPT" target="_blank">
                <img src="/images/controller/admin-developer/uc-input/binance.png"/>
            </a>
        </p>
        <p>Функция проверки входящих монет:</p>
        <pre>yii input/check</pre>

    </div>
</div>


