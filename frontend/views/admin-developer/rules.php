<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Правила программирования';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="https://sroutes.atlassian.net/" target="_blank">JIRA</a></p>
        <p><a href="https://youtu.be/pKwM_rAgz-U" target="_blank">Git Flow обзор технологии (Алексей Пустоутов) (Youtube)</a></p>
        <p><a href="https://ru.hexlet.io/blog/posts/environment" target="_blank">Среды разработки</a></p>
        <p><a href="https://habr.com/ru/post/106912/" target="_blank">Удачная модель ветвления для Git</a></p>
        <p><a href="https://youtu.be/TeKHk_FGu88" target="_blank">Как правильно использовать трекер Jira + git из PhpStorm (Youtube)</a></p>
    </div>
</div>
