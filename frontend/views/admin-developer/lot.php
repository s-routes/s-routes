<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Лотерея';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Введение</h2>

        <p>Организатор (система) выставляет на розыгрыш Продукт. Продукт разбивается на несколько частей и выставляется на продажу.</p>
        <p>В чем можно купить лотерею? В какой валюте?</p>
        <p>В течение какаого времени разыгрывается лотерея?</p>



        <h2 class="page-header">Система</h2>
        <p>Пользователь (аккаунт) такой же на на NEIRON, 2FA и telegram свои</p>
        <p>Сайт: <code>lot.neiro-n.com</code></p>
        <p>Папка в проекте: <code>lot</code></p>
        <p>Локальный сайт: <code>http://localhost:3085</code></p>
        <p>ID приложения: <code>sroutes-lot</code></p>
        <p>namespace: <code>lot</code></p>

        <h2 class="page-header">Сущности</h2>
        <p>Лот: <code>lot</code></p>
        <p>Билет: <code>bilet</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'lot',
            'description' => 'Лот для лотереи',
            'model'       => '\common\models\Lot',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Флаг. Скрыт лот? 0 - показывается, 1 - да скрыт, не показывается' ,
                ],
                [
                    'name'        => 'content',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'Полное описание' ,
                ],
                [
                    'name'        => 'room_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор комнаты для чата, для участников розыгрыша' ,
                ],
                [
                    'name'        => 'is_counter_start',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Флаг. Стартовал таймер розыгрыша? 0 - нет, 1 - стартовал' ,
                ],
                [
                    'name'        => 'is_counter_stop',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Флаг. Закончился таймер розыгрыша? 0 - нет, 1 - да остановился и победитель определен' ,
                ],
                [
                    'name'        => 'bilet_count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Всего кол-во билетов',
                ],
                [
                    'name'        => 'bilet_sold',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во проданных билетов',
                ],
                [
                    'name'        => 'winner_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор пользователя победителя',
                ],
                [
                    'name'        => 'time_start',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Время начала продаж',
                ],
                [
                    'name'        => 'time_finish',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Время окончания продаж',
                ],
                [
                    'name'        => 'time_raffle',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Время розыгрыша',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Флаг. Скрыт лот? 0 - показывается, 1 - да скрыт, не показывается',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Статус лота 1 - Создан, 2 - Пул участников сформирован, 3 - Лот разыгран',
                ],
                [
                    'name'        => 'time_type',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Тип задания времени. 0 - time_finish и time_raffle, 1 - time_finish_add и time_raffle_add',
                ],
                [
                    'name'        => 'time_finish_add',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Добавка в мин к time_finish, если time_type=1. Время окончания продаж',
                ],
                [
                    'name'        => 'time_raffle_add',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Добавка в мин к time_raffle, если time_type=1. Время розыгрыша',
                ],

            ],
        ]) ?>

        <h2 class="page-header">Кеш бек</h2>
        <p>Для каждого билета можно купить "пакет компенсации". При его покупке действует следующая логика: если билет выигрывает, человеку достается приз, если нет то компенсация в указааном проценте от цены билета.</p>
        <p>Цена за пакет компенсации и процент указываются в лоте.</p>

        <h2 class="page-header">Сценарий лотереи</h2>
        <p>
            <a href="https://app.diagrams.net/#G1MgdRD8H2qNqHb83iHn-4XA63lmND4Z4B" target="_blank">
                <img src="/images/controller/admin-developer/lot/lot.png" width="100%">
            </a>
        </p>
        <h2 class="page-header">Розыгрыш</h2>
        <p>Под каждый розыгрыш формируется комната для чата. Там участники могут общаться.</p>
        <p>Вначале в кеш записываю время старта, после того как кто то остановит то там вычитаю время после - до и получаю остаток от деления на кол-во участников.</p>
        <p>Ключ для каждого лота будет такой <code>LOT-[ID]</code></p>
        <p>Событие в комнате остановки счетчика будет <code>chat1-counter-stop</code> и такое же событие будет возвращаться всем участикам на которое им надо подписаться чтобы узнать что лотерея остановлена.</p>
        <p>Запускать счетчик можно и в PHP а в JS считать миллисекунды. Я могу с клиентом отправлять значение старта лотереи . Нет лучше из БД взять.</p>
        <p>Если счетчик запущен то значит Флаг <code>is_counter_start</code> = 1. Если он остановлен то флаг <code>is_counter_stop</code> = 1 и в поле <code>winner_id</code> присутствует победитель.</p>
        <p>Сейчас сделаю кнопку у админа старта розыгрыша и событие будет <code>chat1-counter-start</code>.</p>
        <p>Для чата идентификатор комнаты будет храниться в поле <code>lot.room_id</code></p>

        <h2 class="page-header">Автоматический старт лотереи</h2>
        <p>Через NODEJS не пойму как сделать. Поэтому будем делать так: в базу запись будет проводиться через PHP, а если кто будет на странице сидеть там будет запрос AJAX каждые 10 сек.</p>

        <h2 class="page-header">Автовыкуп билетов</h2>
        <p>за 2 минуты до начала розыгрыша запускается крон. Крон запускается каждую минуту</p>
        <pre>yii lot/auto-redemption</pre>
        <p>Кто будет выкупать все билеты? задаю в параметре системы <code>Yii::$app->params['AUTO_REDEMPTION_USER_ID']</code> в файле .env <code>AUTO_REDEMPTION_USER_ID</code> , пока я.</p>

        <h2 class="page-header">Счетчик от окончания продаж до старта розыгрыша</h2>
        <h2 class="page-header">Страница розыгрыша</h2>
        <p><code>/cabinet-lot/play</code></p>
        <p>
            <a href="https://app.diagrams.net/#G1DSy6n2l0bwPZXGCR1DoGc-TETZf7IN04" target="_blank">
                <img src="/images/controller/admin-developer/lot/play.png">
            </a>
        </p>
        <p>Нужно чтобы страница работала online, статически на любом из этапов</p>
        <p>Идеальным решением будет конечно же чтобы все события вызывались через NODEJS через события. Но приложение запущено и слушает порт 3000 а как его вызвать еще по команде из консоли я не знаю</p>
        <p>Если приложение NODEJS работает постоянно на 3000 порту то как обратиться к приложению из консоли?</p>

        <h2 class="page-header">Остановка счтчика</h2>
        <p>Как только остановка происходит. запускается три стадии delay=10, 100, 500 и останавливается на победителе</p>

        <p><b>Если никто не купил билеты</b></p>
        <p>Если никто не купил билеты то возникает ситуация что не кому остановить счетчик. Потому что выкупает все билеты БОТ. Значит надо как то закрыть лотерею. Можно в том же скрипте и назначить победителя и остановить ее.</p>
        <p>Если никто ничего не купил то записываю победителем первый билет в <code>yii lot/auto-redemption</code>.</p>
        <p>Если никто не купил билеты, то в том же скрипте выкупа билетов, сразу происходит старт, остановка лота и первый купленный билет записывается победителем..</p>

        <h2 class="page-header">Автовыставление лотов</h2>
        <p>автовыставление происходит через консольную команду.</p>
        <pre>yii lot/auto-lot</pre>
        <p>Запускается раз в минуту и там вычисляется когда нужно выставить следующий лот. Для того чтобы определить каой последний лот был выставлено существует флаг <code>is_auto</code>.</p>
        <p>В виду того что время time_start должно меняться всегда то в конфиге автолота я буду указывать добавку до time_finish (add_time_finish) и добавку до time_raffle (add_time_raffle)</p>
        <p>Конфиг автолота содержится в БД config под ключом <code>lot-auto</code>.</p>

        <h2 class="page-header">Завершение лотереи</h2>
        <p>По факту остановка счетчика записывается флаг <code>is_counter_stop</code> и далее каждую минуту консольная команда проверяет выигрышные лотерии и выплачивает деньги кому надо и ставит флаг <code>is_send_win</code> означающий что выплаты были осуществлены</p>

        <h2 class="page-header">Лотерея "онлайн-трансляция"</h2>
        <p>Событие <code>chat1-counter-win</code> указывает победителя</p>
        <p>Вызов:</p>
        <pre>socket.to(room).emit(
    'chat1-counter-win',
    {
        room: room,
        lot_id: lot_id,
        ticket: {
            id: 'int',
            created_at: 'int',
            tx_id: 'int',
            is_cash_back: 'int',
            num: 'string',
            user: {
                id: 'int',
                name: 'string',
                avatar: 'string'
            }
        }

    }
);</pre>

    </div>
</div>


