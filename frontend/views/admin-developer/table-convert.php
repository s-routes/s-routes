
<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Таблица конвертации';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Хранится в БД config <code>ConvertParams</code></p>

        <p>Двухмерный массив</p>


        <pre>[
  {
    id: "db.currency.id",
    items: [
      {
        id: "db.currency.id",
        percent: "double",
        apply: "1/0"
      }
    ]
  }
]</pre>
        <p>Слева по горизонтали: из чего меняем, наверху - во что меняем</p>
        <p>Названия поле в форме</p>
        <p>v_{id1}_{id2}_percent</p>
        <p>id1 - идентификатор валюты для строк (из чего меняем)</p>
        <p>id2 - идентификатор валюты для столбцов (во что меняем)</p>
        <p>Условие на конвертацию Binance: если валюта из является криптовалютой, кроме USDT: BTC,ETH,LTC,TRX,DASH,BNB,DAI,DOGE</p>

        <h2 class="page-header">Как далее делать?</h2>
        <p>сделать отдельную страницу где и тестить <code>/cabinet-bills/index2</code></p>
        <p>Адрес для конвертации: <code>/cabinet-wallet/convert</code></p>

    </div>
</div>
