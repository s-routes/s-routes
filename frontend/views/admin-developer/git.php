
<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'GIT';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://www.draw.io/#G1yhqc4oBAxtYcSRoo63DnFXfhM109PA9U" target="_blank">
                <img src="/images/controller/admin-developer/git/s.png" width="100%">
            </a>
        </p>

        <p>Схема 1. Залить изменения из master (Святослав) в develop (Настя)</p>
        <pre>1 переключись на мастер
2 сделай PULL
3 переключись на develop
4 в меню выбери master и команду merge</pre>

        <p>Схема 2. Залить то что в develop на тестовый сервер</p>
        <pre>1 зайти на тестовый сервер через консоль
2 выполни cd ~/s-routes/www (если первый раз зашла)
3 сделай PULL</pre>

        <p>Схема 3. Залить то что в develop на свой компьютер</p>
        <pre>сделай PULL (CTRL+T)</pre>


        <p>Схема 4. Залить свои коммиты в develop</p>
        <pre>сделай PUSH (CTRL+SHIFT+K)</pre>

        <p>Если ты изменения Святослава хочешь увидеть на тестовом серврере то делай Схема 1, Схема 4 и Схема 2.</p>

        <h2 class="page-header">Жизненный цикл задачи</h2>
        <p>1. Запулить изменения из <code>master</code></p>
        <p>2. Сделать ветку для задачи от <code>master</code></p>
        <p>3. Сообщить Святославу о готовности задачи</p>
        <p>4. Проверка задачи на STAGE</p>
        <p>5a. Если успешно то заливаю на <code>master</code></p>
        <p>5b. Иначе доработка задачи</p>
    </div>
</div>
