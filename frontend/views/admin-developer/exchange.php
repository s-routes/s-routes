<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Обменник';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Путь: <code>/exchange</code></p>
        <p>Дизайн и интерфейс на подобии <a href="https://localethereum.com/ru/" target="_blank">https://localethereum.com/ru/</a>
        </p>

        <h2 class="page-header">Сущности</h2>
        <p>Пользователь <code>user</code></p>
        <p>Предложение <code>offer</code> нет еще такого</p>
        <p>Способ оплаты <code>pay_method</code> нет еще такого</p>
        <p>Местоположение <code>place</code> нет еще такого</p>
        <p>Сделка <code>deal</code> нет еще такого</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'offer',
            'description' => 'Предложение на продажу или покупку',
            'model'       => '\common\models\exchange\Offer',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кто сделал предложение',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Цена по которой предлагается купить продать VVB, указывается в атомах',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта `price`, указывает на db.currency',
                ],
                [
                    'name'        => 'pay_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Метод оплаты',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Вид сделки 1 - купить, 2 - продать. Относится к пользователю который добавил предложение',
                ],
                [
                    'name'        => 'volume',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Объем предложения, цена указана в атомах в валюте которая указана в цене',
                ],
                [
                    'name'        => 'condition',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Условия сделки, простой текст',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания предложения',
                ],
                [
                    'name'        => 'avg_deal',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Среднее время сделки в сек для создателя предложения user_id',
                ],
                [
                    'name'        => 'assessment_positive',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во положительных оценок для создателя предложения user_id',
                ],
                [
                    'name'        => 'assessment_negative',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во положительных оценок для создателя предложения user_id',
                ],
                [
                    'name'        => 'deals_count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во сделок для всех предложений автором которых является user_id',
                ],
                [
                    'name'        => 'currency_io',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор монеты для омена currency_io.id',
                ],
                [
                    'name'        => 'offer_pay_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор валюты для обмена offer_pay.id',
                ],
                [
                    'name'        => 'volume_start',
                    'type'        => 'varchar(30)',
                    'isRequired'  => false,
                    'description' => 'Объем предложения минимальный в валюте в которой указывается цена `currency_id`',
                ],
                [
                    'name'        => 'volume_finish',
                    'type'        => 'varchar(30)',
                    'isRequired'  => false,
                    'description' => 'Объем предложения максимальный в валюте в которой указывается цена `currency_id`',
                ],
                [
                    'name'        => 'volume_io_start',
                    'type'        => 'varchar(30)',
                    'isRequired'  => false,
                    'description' => 'Объем предложения минимальный в валюте которая предлагается `currency_io`',
                ],
                [
                    'name'        => 'volume_io_finish',
                    'type'        => 'varchar(30)',
                    'isRequired'  => false,
                    'description' => 'Объем предложения максимальный в валюте которая предлагается `currency_io`',
                ],
                [
                    'name'        => 'is_hide',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг. Скрыто предложение? Если максимальный порог опускается ниже минимального то сделка скрывается - 1, 0 - показывается. по умолчанию - 0',
                ],

            ],
        ]);
        ?>

        <p>На одно предложение может быть несколько сделок</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'offer_pay',
            'description' => 'Валюта для обмена в форме предложения',
            'model'       => '\common\models\exchange\OfferPay',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'валюта db.currency.id',
                ],
                [
                    'name'        => 'code',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'code',
                ],
                [
                    'name'        => 'title',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'title',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'pay_method',
            'description' => 'Метод оплаты',
            'model'       => '\common\models\exchange\PayMethod',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Название',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'deal',
            'description' => 'Сделка',
            'model'       => '\common\models\exchange\Deal',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя кто открыл сделку',
                ],
                [
                    'name'        => 'price',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Цена по которой предлагается купить продать 1 VVB, указывается в атомах',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта для price, указывает на db.currency',
                ],
                [
                    'name'        => 'offer_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор на предложение, offer.id',
                ],
                [
                    'name'        => 'volume',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Объем сделки в атомах currency_id',
                ],

                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания предложения',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус сделки',
                ],
                [
                    'name'        => 'time_finish',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Время окончания сделки',
                ],
                [
                    'name'        => 'time_accepted',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Время подтверждения сделки',
                ],
                [
                    'name'        => 'currency_io',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор монеты для обмена currency_io.id',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Тип сделки, инверсно предложению ($this->offer->type_id == Offer::TYPE_ID_SELL) ? Deal::TYPE_ID_BUY : Deal::TYPE_ID_SELL',
                ],
                [
                    'name'        => 'price_m',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Цена в монетах',
                ],
                [
                    'name'        => 'partner_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор пользователя указанный в реферальной ссылке',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Жизненный цикл сделки</h2>
        <p>
            <a href="https://www.draw.io/#G1ra4nc0JUAZv1dTkDAr8CzXavQ0BM8CQT" target="_blank">
                <img src="/images/controller/admin-developer/exchange/life.png" width="100%">
            </a>
        </p>
        <p>Рассмотреть ситуацию если Клиент 1 открыл сделку, Клиент 2 подтвердит, Клиент 1 отправил как положено, Клиент 2 говорит что деньги не получил, а реально получил.</p>


        <h2 class="page-header">В чем задается объем?</h2>
        <p>в валюте в которой ставится цена или от того кол-ва которое есть у человека * на цену?</p>
        <p>Сейчас объемы задаются в валюте в которой выставлена цена.</p>
        <p>Потом возможно будет и от колва баллов и будет пересчет по цене.</p>

        <h2 class="page-header">официальный курс обмена</h2>
        <p>официальный курс обмена вот отсюда лучше брать <a href="https://coinmarketcap.com/currencies/prizm/" target="_blank">https://coinmarketcap.com/currencies/prizm/</a></p>

        <h2 class="page-header">Страница просмотра предложений</h2>
        <p>Ссылка: <code>/cabinet-exchange/index</code>.</p>
        <p>По клику идет переход на открытие сделки <code>/cabinet-exchange/deal-open</code>.</p>

        <h2 class="page-header">Страница "Добавить предложение"</h2>
        <p>Страница: <code>/cabinet-exchange/offer-add</code></p>
        <p>Модель: <code>\avatar\models\forms\exchange\OfferAdd</code></p>
        <p>Цена - курс по которому будет продаваться VVB. Валюта указывает соответственно валюту для этой цены.</p>
        <p>Объемы указываются в валюте продажи или покупки</p>
        <p>1. При выставлении предложения на продажу и покупку в какаой валюте оно может быть выставлено? Для начала рубль, потом $</p>

        <p><b>Валидация формы</b></p>
        <p>"Объем до" должен быть больше "объема до"</p>
        <p>При продаже проверяется а есть ли данное кол-во на счету.</p>

        <h2 class="page-header">Страница "Открыть сделку"</h2>
        <p>Страница: <code>/cabinet-exchange/deal-open</code></p>
        <p>Модель: <code>\avatar\models\forms\exchange\DealOpen</code></p>
        <p>Как уведомить ответчика сделки о начале сделки? По почте пока.</p>
        <p>Если предложение на продажу, то сделка получается на покупку и обратно.</p>
        <p><b>Валидация формы</b></p>
        <p>Если я продаю, то проверяется, а есть ли данное кол-во у меня на счету.</p>

        <h2 class="page-header">Страница "Сделка"</h2>
        <p>Страница: <code>/cabinet-exchange/deal-action</code></p>
        <p>На странице есть элемент <code>#chat</code> в котором есть аттрибут <code>data-chat</code> в котором в JSON прописаны поля:</p>
        <p><code>user1</code> - тот, кто принял предложение и открыл сделку. <code>deal.user_id</code></p>
        <p><code>user2</code> - тот, кто сделал предложение. <code>offer.user_id</code></p>
        <p><a href="https://yadi.sk/i/itmpeDxVrs0JWQ" target="_blank">https://yadi.sk/i/itmpeDxVrs0JWQ</a></p>

        <p><b>Ситуация: пользователь хочет купить VVB и принимает сделку на продажу VVB</b></p>
        <p><img src="/images/controller/admin-developer/exchange/situation1.png"></p>
        <p><code>offer.type_id = \common\models\exchange\Offer::TYPE_ID_SELL</code> (2)</p>
        <p>После открытия сделки, user2 отправляется уведомление чтобы он принял сделку. Когда он открывает сделку там будет кнопка "Принять сделку". Вызывается событие <code>Deal::EVENT_AFTER_OPEN = 'afterOpen'</code></p>
        <p>Вызывается AJAX <code>/cabinet-exchange/seller-open-accept</code> и в нем Сделка меняется статус на <code>STATUS_BLOCK</code> и вызывается событие <code>Deal::EVENT_AFTER_BLOCK = 'afterBlock'</code>. <code>user2</code> уведомляется, что сделка принята. У <code>user1</code> блокируются монеты.</p>

        <p>
            <a href="https://www.draw.io/#G1hvf2PpyRu-sSkdMltPcWaH-ccgi9foFT" target="_blank">
                <img src="/images/controller/admin-developer/exchange/life2.png" width="100%">
            </a>
        </p>

        <p>Вызывается AJAX <code>/cabinet-exchange/buyer-money-sended</code> и в нем Сделка меняется статус на <code>STATUS_MONEY_SENDED</code> и вызывается событие <code>Deal::EVENT_AFTER_MONEY_SENDED = 'afterMoneySended'</code>. <code>user1</code> уведомляется, что деньги отправлены.</p>
        <p>Вызывается AJAX <code>/cabinet-exchange/seller-money-received</code> и в нем Сделка меняется статус на <code>STATUS_MONEY_RECEIVED</code> и вызывается событие <code>Deal::EVENT_AFTER_MONEY_RECEIVED = 'afterMoneyReceived'</code>. <code>user1</code> уведомляется, что деньги отправлены.</p>
        <p>После того как деньги отправлены Клиенту 1 предлагается оценить сделку Клиента 2. На оценку вызывается <code>/cabinet-exchange/buyer-assessment-*</code> в котором  формируется оценка, вызывается событие <code>EVENT_AFTER_CLOSE</code> и ставится статус <code>STATUS_CLOSE</code>.</p>
        <p>В итоге закрытыми сделками считаются со статусами <code>STATUS_MONEY_RECEIVED</code> и <code>STATUS_CLOSE</code>.</p>



        <h3 class="page-header">Осображение заголовка</h3>
        <p>В сделке вывести заголовок сделки зеркально для покупателя и продавца</p>
        <p>дело в том что если предложение = Offer::TYPE_ID_BUY это продажа, то для того кто сделал предложение это Я покупаю а для того кто открыл сделку это Я продаю.</p>
        <p>Итого так выглядит условие:</p>
        <pre>&lt;h2&gt;Я &lt;?= (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL)? (($Deal->user_id == Yii::$app->user->id)? 'покупаю' : 'продаю') : (($Deal->user_id == Yii::$app->user->id)? 'продаю' : 'покупаю')) . '. ' . $user->getName2() ?&gt;&lt;/h2&gt;</pre>


        <h2 class="page-header">Блокировка средств</h2>
        <p>Как блокировать деньги у пользователя?</p>
        <p>Они будут переводиться на кошелек "Эскроу". Он один. На него все монеты от сделок и поступают. Всего две транзакции на сделку.</p>
        <ul>
            <li>1. блокировка средств. Коментарий: блокировка средств от сделки ### от пользователя ### с кошелька ###</li>
            <li>2. разблокировка средств. Коментарий: разблокировка средств от сделки ### в пользу пользователя ### в кошелек ###</li>
        </ul>
        <p>Идентификатор кошелька Эскоу = 285. Хранится в переменной <code>Yii::$app->params['walletEscrow']</code></p>

        <h2 class="page-header">Оценка сделки</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'assessment',
            'description' => 'Оценка сделки',
            'model'       => '\common\models\exchange\Assessment',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя который оценивается',
                ],
                [
                    'name'        => 'user_id_author',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя который оценивает (автор оценки)',
                ],
                [
                    'name'        => 'role',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'В какой роли находится оцениваемый 1 - продавец, 2 - покупатель',
                ],
                [
                    'name'        => 'offer_id',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Идентификатор предложения offer.id',
                ],
                [
                    'name'        => 'offer_type',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Тип предложения offer.type_id',
                ],
                [
                    'name'        => 'value',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Оценка, возможно -1,0,1',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания предложения',
                ],
            ],
        ]) ?>

        <p>Сейчас надо решить как записывать оценку<br>
            сейчас учитывается кто оценивается<br>
            а надо еще записывать кем оценивается и<br>
            кем является оценивающий? 1 - тот кро сделал предложение или 2 - тот кто участвует в сделке.<br>
            и в какой роли оцениваемый 1 - продавец, 2 - покупатель<br>
            да так будет полная картина<br>
            и обратную оценку так же делать</p>
        <p>и алгоритм завершения сделки какой?<br>
            если один сделал оценку то у него загорается что сделка завершена и второму приходит уведомление что успешно</p>
        <p>Если у меня заданы параметры BUY SELL для типа сделки<br>
            то нужно понять а какой экран? и как их дентифицировать или обозначать<br>
            ранее я вычислял через параметр<br>
            но еще надо как то их называть<br>
            если я буду называть "экран предложения" или "экран сделки" нормально ли это?<br>
            <code>SCREEN_OFFER</code><br>
            <code>SCREEN_DEAL</code></p>

        <p>Тогда можно сделать одну функцию, но вызывать с разными параметрами. Например <code>/cabinet-exchange/set-assessment</code> (id - идентификатор сделки, value - оценка, screen - идентификатор экрана с которого производится оценка)</p>

        <p>
            <a href="https://www.draw.io/#G1W3RpMODnVJDWKlToPnkCm0R9cCeGSMw8" target="_blank">
                <img src="/images/controller/admin-developer/exchange/ass.png">
            </a>
        </p>

        <p>У пользователя <code>user</code> существует несколько параметров:</p>
        <pre>* @property integer   deals_count_offer    Кол-во сделок для всех предложений автором которых является offer.user_id
* @property integer   deals_count_all      Общее кол-во сделок в которых учествовал пользователь
* @property integer   assessment_positive  Общее кол-во положительных оценок пользователя
* @property integer   assessment_negative  Общее кол-во отрицательных оценок пользователя</pre>

        <p>Параметр <code>deals_count_offer</code> считается так:</p>
        <pre>$offerList = Offer::find()->where(['user_id' => $offer->user_id])->select('id')->column();
$dealsCount = Deal::find()
    ->where(['offer_id' => $offerList])
    ->andWhere([
        'status' => [
            Deal::STATUS_MONEY_RECEIVED,
            Deal::STATUS_CLOSE,
            Deal::STATUS_AUDIT_FINISH,
        ],
    ])
    ->count();</pre>
        <p>Параметр <code>deals_count_offer</code>  - это <code>deals_count_offer</code> + сделки deal.user_id=&lt;me&gt;</p>


        <h2 class="page-header">Арбитраж</h2>
        <p>Кнопка "Пригласить арбитра" видна и доступна обоим участникам сделки. Если кто то ее нажимает то арбитру посылается уведомление.</p>
        <pre>По арбитрам
Я так понимаю их может быть более чем один
и если кто то нажимает "Пригласить арбитра", то уведомления отсылаются всем сразу
и кто первый возмет сделку в разбор тот и решает спорный момент в сделке.
Верно?</pre>
        <p>Так как есть статус пригласить Арбитра и Арбитр принимает приглашение участвовать в разборе, то надо добавить еще один статус</p>
        <p>Надо сделать роль арбитра
            <code>role_arbitrator</code>
            <code>permission_arbitrator</code></p>
        <pre>на статус STATUS_AUDIT_WAIT
отсылается уведомление всем арбитрам

на статус STATUS_AUDIT_ACCEPTED
отсылается уведомление обоим участникам сделки

на статус STATUS_AUDIT_FINISH
отсылается уведомление обоим участникам сделки
        </pre>

        <p>Контроллер для арбитров: <code>cabinet-arbitrator</code><p>

        <p>По факту принятия сделки арбитром он записывается в ... Допустим сделаю таблицу arbitrator и там буду записывать статусы, когда отправлено приглашение</p>
        <pre>Записывать ли как меняются статусы сделки (отправлены деньги, получены), то есть время?
            для арбитра актуальная информация будет</pre>
        <p>По хорошему да, там как раз и статусы арбитража можно прописывать.</p>
        <p>А что еще нужно записывать? В чью пользу было принято решение в сделке.</p>
        <p>Какие коментарии в транзакции после принятия решения арбитра?</p>
        <ul>
            <li>1. разблокировка в пользу пользователя. Коментарий: Арбитр ### разблокировал средства от сделки ### в пользу пользователя ### в кошелек ###</li>
            <li>2. разблокировка в пользу пользователя. Коментарий: Арбитр ### разблокировал средства от сделки ### в пользу пользователя ### в кошелек ###</li>
        </ul>
        <p>Куда записывать какой арбитр взял сделку в разбор?</p>
        <p>Таблицу надо сделать. Кто взял, В чью пользу были отправлены деньги. Например arbitrator</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'arbitrator',
            'description' => 'Рабзор сделки арбитром',
            'model'       => '\common\models\exchange\Arbitrator',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'arbitrator_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор арбитра',
                ],
                [
                    'name'        => 'unblock_transaction_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'идентификатор транзакции разблокировки средств',
                ],
                [
                    'name'        => 'decision_user_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор пользовтеля в пользу которого были разблокированы монеты',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент принятия арбитром сделки на разбор',
                ],
                [
                    'name'        => 'finish_at',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Момент завершения арбитром сделки, то есть принятия статуса STATUS_AUDIT_FINISH',
                ],
            ],
        ]) ?>

        <p>Когда туда добавляется запись? После принятия сделки арбитром.</p>

        <h2 class="page-header">Покупка</h2>
        <p>
            <a href="https://www.draw.io/#G1hvf2PpyRu-sSkdMltPcWaH-ccgi9foFT" target="_blank">
                <img src="/images/controller/admin-developer/exchange/life3.png" width="100%">
            </a>
        </p>

        <h2 class="page-header">Статусы сделки</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'deal_status',
            'description' => 'Статусы сделки',
            'model'       => '\common\models\exchange\DealStatus',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор статуса',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Комментарий',
                ],
            ],
        ]) ?>
        <p>Статусы показываются для арбитра.</p>

        <h2 class="page-header">Списание объема после завершения сделки</h2>
        <p>После того как сделка завершается объем в сделке уменьшается. Где это происходит? <code>/cabinet-exchange/sell-money-received</code>, <code>/cabinet-exchange/buy-money-received</code>, <code>/cabinet-arbitrator/finish1</code>, <code>/cabinet-arbitrator/finish2</code></p>

        <h2 class="page-header">Добавить счетчик сделки и автозакрытие по факту окончания времени</h2>
        <p>Что делать когда люди пропадают? Ставить статус что сделка закрыта но не завершилась. Например STATUS_HIDE.</p>
        <p>где будет устанавливаться? в консольном контроллере который будет вызываться раз в минуту
            <code>\console\controllers\ExchangeController::actionCloseDeals()</code></p>
        <p>куда девать заблоченные деньги? Ситуация
            когда сделка открыта, деньги заблочились и никто арбитра так и не вызвал,
            я сделку закрою автоматом, а монеты зависшие в блоке куда девать? Пока оставлю там где заблочены.</p>
        <p>CRON: <code>* * * * * /usr/bin/php /var/www/54.38.54.52/yii exchange/close-deals</code></p>
        <p>Резервный фонд-держатель невыясненых платежей - <code>walletLost</code></p>

        <h2 class="page-header">Страница "Мои предложения"</h2>
        <p>Колонка сделки указывает завершенные сделки по данному предложению. То есть статусы
            <code>Deal::STATUS_MONEY_RECEIVED</code>,
            <code>Deal::STATUS_CLOSE</code>,
            <code>Deal::STATUS_AUDIT_FINISH</code>.
        </p>

        <h2 class="page-header">Страница "Пользователь"</h2>
        <p>Ссылка на страницу: <code>/user/{id}</code>.</p>
        <p>Что отображается? Кол-во сделок закрытых и процент успешных.</p>

        <p><b>Кол-во сделок</b></p>
        <p>Сделки со статусами:
            <code>Deal::STATUS_MONEY_RECEIVED</code>,
            <code>Deal::STATUS_CLOSE</code>,
            <code>Deal::STATUS_AUDIT_FINISH</code>.
        </p>
        <p>Это те где я принимал учатие как тот кто открывает сделку и тот кто предлагал сделку</p>
        <p>Тот кто открывал сделку это <code>['user_id' => Yii::$app->user->id]</code></p>
        <p>тот кто предлагал сделку это нужно присоединить еще и offer и выбрать сделки которые к ним присоединены по <code>offer.user_id</code></p>
        <p>В итоге вот формула:</p>
        <pre>$count = count(Deal::find()
->innerJoin(['offer', 'offer.id = deal.offer_id'])
->where(['in', 'deal.status', [Deal::STATUS_MONEY_RECEIVED, Deal::STATUS_CLOSE, Deal::STATUS_AUDIT_FINISH]])
->andWhere([
    'or',
    ['offer.user_id' => Yii::$app->user->id],
    ['deal.user_id' => Yii::$app->user->id],
])
->select([
    'deal.*'
])
->asArray()
->all());</pre>

        <p><b>Кол-во сделок</b></p>
        <p>Здесь надо думать. Сами оценки в присоединненной таблице, значит надо ее присоединять и вычислять так. Ну можно попробовать</p>
        <p>Второй вариант: добавлять оценку сделки в саму сделку. а кто оценивается таким образом? оценивается тот кто предлагал сделку. То есть <code>offer.user_id</code></p>
        <p>То есть получаетя что я мог учасвовать в 10 сделках, но в одной я только предлагал, и там меня оценили а в других 9-ти я был инициатором сделки.</p>
        <p>Вот здесь и вопрос.</p>
        <p>Буду считать пока учтенные сделки, то есть оцененные</p>

        <p><b>Объемы сделок</b></p>
        <p>Хорошо бы еще посчитать все объемы сделок.</p>

        <h2 class="page-header">Чат</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'chat_message',
            'description' => 'Чат',
            'model'       => '\common\models\exchange\ChatMessage',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'deal_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор сделки',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'message',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'сообщение, формат текстовый с переносом строк',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент указания статуса',
                ],
            ],
        ]) ?>
        <p><code>/avatar-bank/views/cabinet-exchange/deal-action-chat.php</code></p>
        <p>Уведомления в телеграм: Если пишет один пользователь то все участники чата получают уведомление о сообщении</p>

        <p><code>/cabinet-exchange-chat/send</code> - отправка<br>
        <code>/cabinet-exchange-chat/get</code> - получение чата</p>

        <h2 class="page-header">Платежные системы</h2>
        <p><code>/avatar-bank/views/cabinet-exchange/deal-action-chat.php</code></p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'offer_pay_link',
            'description' => 'Ссыдка предложения и платежной системы',
            'model'       => '\common\models\exchange\OfferPayLink',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'offer_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор предложения',
                ],
                [
                    'name'        => 'pay_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор платежной системы pay_method.id',
                ],
            ],
        ]) ?>
        <h2 class="page-header">Страница "Мои сделки"</h2>
        <p>Страница: <code>/cabinet-exchange/my-deals</code></p>
        <p>Здесь в выборку я включаю как мои сделки <code>deal.user_id</code> так и сделки в которых я участвую как предлагающий и со мной открыли сделку <code>offer.user_id</code>.</p>


        <h2 class="page-header">Удаление предложения</h2>
        <p>Для того чтобы удалить предложение сначала проверяется есть ли уже привязанные сделки. Если есть то нельзя удалить. Если нет то удаляется.</p>

        <h2 class="page-header">Отмена сделки после открытия до подтверждения</h2>
        <p>Ввел статус <code>Deal::STATUS_CANCEL</code></p>
        <p>AJAX <code>/cabinet-exchange/cancel</code></p>
        <p>Отмену сделки всегда делает только инициатор сделки, то есть на экране <code>SCREEN_DEAL</code>.</p>
        <pre>socket.emit('buy-cancel', {$Deal->id}, {$offer->user_id}, 10);</pre>
        <pre>http://i.imgur.com/vaxruWs.png</pre>

        <h2 class="page-header">Открытие сделки</h2>
        <p>Здесь кнопка Принять сделку должна быть автоматом. Уведомление тоже нужно.</p>
        <p>Для <code>offer.type_id=TYPE_ID_SELL</code> Показать кнопку "Подтвердить сделку" если <code>offer.user_id = user_id</code></p>
        <p>Для <code>offer.type_id=TYPE_ID_BUY</code> Показать кнопку "Подтвердить сделку" если <code>offer.user_id = user_id</code></p>


        <h2 class="page-header">Проверка по факту нажатия на кнопку принять сделку</h2>
        <p>Сделка "продать" у пользователя который покупает при нулевом балансе при нажатии на кнопку купить ничего не происходит. Нужно предусмотреть либо подпись пополните баланс, либо не давать возможность принять в ней участие до пополнения баланса(что логичнее)</p>

        <pre>Не понятно когда ситуация возникла

сейчас так
если это сделка продажи Offer::TYPE_SELL, то есть я продаю монеты, перед тем как разместить предложение идет проверка что монеты есть на счету
и когда я (продавец) принимаю то монеты у меня по любому есть.

Если сделка покупки Offer::TYPE_BUY то по факту нажатия кнопку монеты блокируются у того кто начал сделку
значит это условие надо проверить в момент открытия сделки

сейчас проверю</pre>
        <p>Добавил такое условие при открытии сделки</p>
        <pre>if ($offer->type_id == Offer::TYPE_ID_BUY) {
    $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::VVB, Yii::$app->user->id);
    /** @var \common\models\piramida\Wallet $w */
    $w = $data['wallet'];
    if ($w->amount < $this->volume_vvb) {
        $this->addError($attribute, 'Вы не можете начать сделку потому что у вас недостаточно монет на счету');
        return;
    }
}</pre>
        <p>Еще может быть вариант что после открытия сделки он еще одну откроет, первый подтвердит а второй уже нарвется на ошибку, чтобы жту ситуацию предусмотреть то надо добавить проверку в принятие сделки</p>
        <p>Значит вызов buy-accept \avatar\controllers\CabinetExchangeController::actionBuyOpenAccept</p>
        <p>Если у продавца будет недостаточно монет то выдаст</p>
        <pre>return self::jsonErrorId(103, 'Недостаточно монет у продавца');</pre>



        <h2 class="page-header">Отрисовка элементов окна в зависимости от статуса сделки</h2>
        <?php
        $rows = [
            [
                'name'        => 'Кнопка отмена сделки',
                'code'        => 'isShowButtonCancel',
                'description' => '',
            ],
            [
                'name'        => 'Статус 1',
                'code'        => 'StateStatus1',
                'description' => '0 - серая, 1 - зеленая, 2 - желтая, 3 - красная',
            ],
            [
                'name'        => 'Статус 2',
                'code'        => 'StateStatus2',
                'description' => '0 - серая, 1 - зеленая, 2 - желтая, 3 - красная',
            ],
            [
                'name'        => 'Статус 3',
                'code'        => 'StateStatus3',
                'description' => '0 - серая, 1 - зеленая, 2 - желтая, 3 - красная',
            ],
            [
                'name'        => 'Статус 4',
                'code'        => 'StateStatus4',
                'description' => '0 - серая, 1 - зеленая, 2 - желтая, 3 - красная',
            ],
            [
                'name' => 'КнопкаBUYпринять',
                'code' => 'isShowButtonBuyAccept',
            ],
            [
                'name' => 'КнопкаBUYотправлены',
                'code' => 'isShowButtonBuyMoneySend',
            ],
            [
                'name' => 'КнопкаBUYполучены',
                'code' => 'isShowButtonBuyMoneyRecived',
            ],
            [
                'name' => 'КнопкаSELLпринять',
                'code' => 'isShowButtonSellAccept',
            ],
            [
                'name' => 'КнопкаSELLотправлены',
                'code' => 'isShowButtonSellMoneySend',
            ],
            [
                'name' => 'КнопкаSELLполучены',
                'code' => 'isShowButtonSellMoneyRecived',
            ],
            [
                'name' => 'Блок для выставления оценки',
                'code' => 'isShowBlockRate',
            ],
            [
                'name' => 'Кнопка пригласить арбитра',
                'code' => 'isShowButtonArbitr',
            ],
        ];
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
        ]) ?>


        <h2 class="page-header">Рейтинг и время сделки</h2>
        <p>Чтобы сделать время сделки надо выработать формулу. Например Время сделки = время оценки - время подтверждения</p>
        <p>Добавлю поле в сделку время окончания сделки <code>time_finish</code></p>
        <p>Запись этого времени сделаю значит в <code>\avatar\controllers\CabinetExchangeController::actionBuyMoneyReceived</code>, <code>\avatar\controllers\CabinetExchangeController::actionSellMoneyReceived</code></p>
        <p>Далее надо сделать среднее время сделки.</p>
        <p>По расчету времени сделки<br>
            какие сделки учитывать в подсчете?<br>
            те в которых я являюсь тем кто предлагает сделку<br>
            те в которых я являюсь партнером у того к то предлагает сделку?</p>
        <p>Еще ситуация: добавлять ли в расчет сделки которые были с участием арбитра, которые заведомо будут дольше обычных?</p>
        <p>Еще добавлю поле <code>time_accepted</code> Время подтверждения сделки и установку в <code>\avatar\controllers\CabinetExchangeController::actionSellOpenAccept</code> <code>\avatar\controllers\CabinetExchangeController::actionBuyOpenAccept</code></p>
        <p>первая гипотеза расчета:</p>
        <pre>$c = Deal::find()
    ->innerJoin('offer', 'offer.id = deal.offer_id')
    ->where([
        'or',
        ['deal.user_id' => Yii::$app->user->id],
        ['offer.user_id' => Yii::$app->user->id],
    ])
    ->andWhere([
        'deal.status' => [
            Deal::STATUS_CLOSE,
            Deal::STATUS_MONEY_RECEIVED,
        ]
    ])
    ->andWhere(['not', ['deal.time_finish' => null]])
    ->select('avg(deal.time_finish - deal.time_accepted)')
    ->scalar()
;</pre>
        <p>Сделал пока те в которых я предлагаю, потому что в таблице этот же человек и указан</p>
        <pre>$c = Deal::find()
    ->innerJoin('offer', 'offer.id = deal.offer_id')
    ->where(['offer.user_id' => $item->user_id])
    ->andWhere([
        'deal.status' => [
            Deal::STATUS_CLOSE,
            Deal::STATUS_MONEY_RECEIVED,
        ]
    ])
    ->andWhere(['not', ['deal.time_finish' => null]])
    ->select('avg(deal.time_finish - deal.time_accepted)')
    ->scalar()
;</pre>
        <p><img src="/images/controller/admin-developer/exchange/time_deal.png" class="thumbnail"></p>


        <h2 class="page-header">Вызов админа</h2>
        <p>После вызова админа надо его уведомить<br>
            1. по почте<br>
            2. если он сидит на сайте то значит послать ему уведомление (от клиента посылаю need-arbitr) + нужно уведомить второго участника сделки.<br>
            Как уведомить всех арбитров? нужно создать их комнату куда они все сразу будут добавляться и ждать этих уведомлений, например комната arbitr</p>
        <p>Кнопка Вызвать арбитра: <code>.buttonInviteArbitrator</code> вызывается <code>socket.emit('need-arbitr', {$Deal->id}, {$user_id}, {$partner_user_id});</code></p>

        <p>Далее арбитр после того как он согласится принять разобрать сделку нажимает "принять сделку" и обоим участникам приходят уведомления.</p>

        <h2 class="page-header">Арбитр и его страница</h2>
        <p>Добавляю его в чат. Арбитру приходят сообщения от двух людей.</p>
        <p>Непонятно пока как в чат арбитра добавить. Точнее можно но непойму пока что будет. В принципе проблема пока только в одном. Если я арбитр отправляю сообщение в чат а какой то пользователь вышел из чата то как его уведомить что пришло сообщение? Только делать массив людей кого надо уведомлять. Да. Можно пока просто добавить арбитра чтобы был чат.</p>

        <p>Отправка сообщения:</p>
        <pre>socket.emit('send-chat-message', roomName, from, to, text);</pre>
        <p><code>roomName</code> - название комнаты</p>
        <p><code>from</code> - от кого сообщение, объект {image:'',name:'',id:''}</p>
        <p><code>to</code> - array кому уведомление, массив идентификаторов пользователей</p>
        <p><code>text</code> - текст сообщения</p>

        <p>
            <a href="https://www.draw.io/#G1o5rJ-O_nMnIfNXbqCgZOgGp4o8J8YA30" target="_blank">
                <img src="/images/controller/admin-developer/chat/arbitr.png">
            </a>
        </p>

        <h3 class="page-header">Арбитр присоединяется к комнате арбитров</h3>
        <p><code>room_arbitr</code> - Комната арбитра, туда помещаются все арбитры для того чтобы им можно было сообщать о новых сделках требующих разрешения. Как арбитр туда добавляется? Если у него стоит роль <code>role_arbitr</code> то вызывается событие на стороне клиента <code>emit('new-arbitr', user_id)</code></p>
        <pre>socket.on('new-arbitr', (user_id) => {
    socket.join('room_arbitr');
    socket.to('room_arbitr').broadcast.emit('new-arbitr', user_id);
});</pre>
        <p>от сервера приходит уведомление <code>new-arbitr</code> в комнату <code>room_arbitr</code></p>

        <h3 class="page-header">Один из участников нажал на "пригласить арбитра"</h3>
        <p>Вызываестся AJAX <code>/cabinet-exchange/invite-arbitrator</code>, генерируется событие <code>need-arbitr</code>. На сервере оно генерирует событие <code>need-arbitr</code> в комнате <code>room_arbitr</code></p>
        <pre>socket.emit('need-arbitr', {$Deal->id}, {$user_id}, {$partner_user_id});</pre>

        <p>На сервере:</p>
        <pre>socket.to(0).broadcast.emit('need-arbitr2', deal_id, user_id, partner_user_id);
socket.to('room_arbitr').broadcast.emit('need-arbitr', deal_id, user_id);</pre>


        <p>По факту нажатия на кнопку скрываются все кнопки, включая "пригласить арбитра" а кнопки становятся желтыми, у партнера аналогично</p>
        <p>И счетчик останавливать после приглашения арбитра</p>

        <h3 class="page-header">Арбитр принимает сделку на разбор</h3>

        <p>Вызываестся AJAX <code>/cabinet-arbitrator/accept</code>, генерируется событие <code>accept-arbitr</code>. На сервере оно генерирует событие <code>accept-arbitr</code> в комнате <code>{идентификатор сделки}</code></p>
        <p>На уровне клиента:</p>
        <pre>socket.emit('accept-arbitr', {$Deal->id}, {$user_id}, {$toIdArray});</pre>
        <p><code>$toIdArray</code> - массив идентификаторов для уведомления</p>

        <p>На уровне сервера:</p>
        <pre>socket.to(deal_id).broadcast.emit('accept-arbitr', deal_id, user_id, to);
socket.to(0).emit('accept-arbitr', deal_id, user_id, to);</pre>


        <h3 class="page-header">Разрешение сделки</h3>
        <p>Вызывается AJAX <code>/cabinet-arbitrator/finish2</code> или <code>/cabinet-arbitrator/finish1</code> далее отправляется на сервер чата уведомление:</p>
        <pre>socket.emit('arbitr-result-offer', id, {$user_id}, to, to_id);</pre>
        <pre>socket.emit('arbitr-result-deal',  id, {$user_id}, to, to_id);</pre>
        <p>где 1 параметр это идентификатор сделки</p>
        <p>где 2 параметр это идентификатор пользователя кто отправляет уведомление</p>
        <p>где 3 параметр это массив участников сделки кого надо уведомить</p>
        <p>где 4 параметр это объект пользователя в чью пользу разрешена сделка</p>

        <pre>/**
 * Арбитр принимает решение в сделке в пользу инициатора сделки (deal.user_id)
 *
 * deal_id  - int   - идентификатор сделки
 * user_id  - int   - идентификатор пользователя который принял разбирать сделку
 * to       - array - массив кому отправить уведомление
 * to_user  - object - в чью пользу разрешена сделка {id,avatar,name}
 */
socket.on('arbitr-result-deal', (deal_id, user_id, to, to_user) => {
    console.log(['arbitr-result-deal', deal_id, user_id, to, to_user]);
    socket.to(0).broadcast.emit('arbitr-result-deal', deal_id, user_id, to, to_user);
});

/**
 * Арбитр принимает решение в сделке в пользу инициатора предложения (offer.user_id)
 *
 * deal_id  - int   - идентификатор сделки
 * user_id  - int   - идентификатор пользователя который принял разбирать сделку
 * to       - array - массив кому отправить уведомление
 * to_user  - object - в чью пользу разрешена сделка {id,avatar,name}
 */
socket.on('arbitr-result-offer', (deal_id, user_id, to, to_user) => {
    console.log(['arbitr-result-offer', deal_id, user_id, to, to_user]);
    socket.to(0).broadcast.emit('arbitr-result-offer', deal_id, user_id, to, to_user);
});</pre>


        <h3 class="page-header">Алгоритм отрисовки появления кнопок</h3>
        <p>Сейчас они рисуются от начала сделки и если кто то нажмет обновить страницу то сделка вернется к началу, что не верно. В связи с этим нужно доделать вариант отрисовки окна после обновления в нужном статусе.</p>

        <h3 class="page-header">Открытие сделки</h3>
        <p><code>deal-opened</code> при этом уведомляется второй пользователь. Отправка:</p>

        <pre>socket.emit('deal-opened', {$Deal->id}, {$user_id}, {$partner_user_id});</pre>
        <pre>// переменная которая хранит статус что уведомление об открытой сделке отправлено deal-opened-# в сессии
// После обновления страницы уже не отправится, хранится статус 90 мин.</pre>


        <h3 class="page-header">Параметры avg_deal, assessment_positive, assessment_negative в предложении</h3>

        <p>Для того чтобы оптимизировать поиск я решил разместить данные <code>avg_deal</code>, <code>assessment_positive</code>, <code>assessment_negative</code> прямо в таблице <code>offer</code>.</p>
        <p>Хоть эти данные и перегружают таблицу, но позволяют сделать сортировку по этим полям и конечно же оптимизируют вывод этих значений в таблице предложений.</p>
        <p>Поле <code>avg_deal</code> расчитывается после окончания сделки, то есть после <code>STATUS_MONEY_RECEIVED</code>. Именно после установки этого статуса записывается значение <code>time_finish</code> на основании которого можно расчитать <code>avg_deal</code>. Событие происходит в route=<code>cabinet-exchange/buy-money-received</code> или <code>cabinet-exchange/sell-money-received</code>.</p>
        <p>Поля <code>assessment_positive</code> и <code>assessment_negative</code> расчитывается после того как будет выставлена оценка. Событие происходит в route=<code>cabinet-exchange/set-assessment</code>. <code>\avatar\models\validate\CabinetExchangeSetAssessment</code></p>
        <p>При помощи команды можно обновить</p>
        <pre>php yii exchange/offer-assessment</pre>

        <h3 class="page-header">Параметры deals_count в предложении</h3>
        <p>сделать поле<br>
            обновить значение<br>
            заменить вывод в таблице<br>
            сделать добавление при создании предложения.</p>
        <pre>Deal::find()
->where(['offer_id' => $item['id']])
->andWhere([
    'status' => [
        Deal::STATUS_MONEY_RECEIVED,
        Deal::STATUS_CLOSE,
        Deal::STATUS_AUDIT_FINISH,
    ],
])
->count();</pre>

        <p>
            <a href="https://app.diagrams.net/#G14b_KVasvR_hBKXZm2WinIlhlcZ6Tpg3I" target="_blank">
                <img src="/images/controller/admin-developer/exchange/ex.png">
            </a>
        </p>

        <p>Пересчет происходит в <code>\common\models\exchange\Deal::finish()</code></p>

        <h3 class="page-header">Ситуация. После продажи максимальный порог опустился ниже минимального</h3>

        <p><img src="http://i.imgur.com/tmFTiKA.png" class="thumbnail"></p>
        <p>Здесь может быть такая ситуация<br>
            например человек выставил 2-10 монет<br>
            кто то купил у него 9<br>
            осталось 1<br>
            в итоге это даже ниже минимального порога и в итоге эта сделка не сможет пройти<br>
            Поэтому нужно или не допускать такой ситуации, чтобы человек выкупал сразу все<br>
            или чтобы нижний порог опускался после сделки то есть становился от 1 до 1</p>
        <p><b>Решение</b></p>
        <p>Если после вычитания максимального порога по факту завершения сделки максимальный порог опустился ниже минимального, то уведомляю пользователя по почте и скрываю предложение из списка</p>

<pre>
    /**
     * Вычитает объем из предложения (должна вызываться после завершения сделки)
     */
    public function finish()
    {
        // ...

        // Если выкуплены все монеты
        if (bccomp($offer->volume_io_finish, '0') == 0) {
            // скрываю сделку
            $offer->is_hide = 1;
        } else {
            // Если верхних предел стал ниже минимального
            if (bccomp($offer->volume_io_finish, $offer->volume_io_start) == -1) {
                // скрываю сделку
                $offer->is_hide = 1;

                // Отправляю уведомление пользователю
                $user = $offer->getUser();
                \common\services\Subscribe::sendArray([$user->email], 'В предложении осталось монет меньше минимального порога', 'offer_min', [
                    'offer' => $offer,
                ]);
            }
        }

        $offer->save();
    }
</pre>
        <h2 class="page-header">История Сделки</h2>
        <pre>что выводить?
когда Деньги заблокированы
когда Деньги разблокированы</pre>
        <p>Идентификатор таблицы на странице <code>history_table</code></p>


        <h2 class="page-header">Бухгалтер</h2>
        <p>При создании заявки средства блокируются в эскроу счет. По факту успешного вывода средства переводятся в основной кошелек.</p>


        <h2 class="page-header">История заявки на вывод</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'task_output_history',
            'description' => 'Элемент истории заявки на вывод',
            'model'       => '\common\models\TaskOutputHistory',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Временная метка создания операции',
                ],
                [
                    'name'        => 'task_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заявки на вывод',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Комментарий',
                ],
            ],
        ]) ?>

        <p>Страница для истории заявки на вывод: <code>/</code></p>

        <h2 class="page-header">Ситуация 20200708</h2>
        <p>Задача: https://sroutes.atlassian.net/browse/SR-327</p>
        <p>Исходная позиция: сделки 273 и 274 были созданы почти одновременно (!-монет на продажу выставлено только 1 PZM). Сделка 273 принята, и по ней пошла работа (1pzm заблокирован). Сделка 274 принята следом, но по ней система работать не дала (кнопка перевода монет была зеленая, но при ее нажатии ничего не происходило). Но косяк в том, что по сделке 274 у продавца ПОВТОРНО ЗАБЛОКИРОВАЛИСЬ монеты в количестве 1pzm, хотя они выходят за рамки продаваемого объема (объем = от 1 до 1 pzm). а это скин, который видит покупатель: для него сделка корректная и он жде монет, которых нет…:</p>
        <p>Решение: сделать поле offer.volume_io_finish_deal<br>
Поле при создании предложения дублирует offer.volume_io_finish<br>
после открытия сделки вичитать из volume_io_finish значение deal.volume_vvb<br>
в процессе открытия сделки проверять поле volume_io_finish на ситуацию достаточного кол-ва монет<br>
Если сделка отменяется то поле offer.volume_io_finish_deal прибавляется обратно на значение deal.volume_vvb<br>
По факту завершения сделки поле offer.volume_io_finish вычтется на значение deal.volume_vvb и станет равным offer.volume_io_finish_deal</p>

        <h2 class="page-header">Стутус ONLINE</h2>
        <p>Указывается в <code>Yii::$app->params['timeOnline']</code></p>

        <h2 class="page-header">Отрисовка страницы после перезагрузки</h2>
        <p>Если один пользователь оценил а второй еще нет то получается так</p>
        <p>Если до то если статус RECIVED то отображаю у обоих</p>
        <p>Если кто-то оценил то сделка перешла в статус завершено и надо понять кто оценил и как показывать</p>
        <p>Если оценил </p>

        <p>Для OFFER</p>

        <pre>$a_offer = Assessment::findOne(['user_id' => $offer->user_id, 'deal_id' => $Deal->id]);
$a_deal = Assessment::findOne(['user_id' => $Deal->user_id, 'deal_id' => $Deal->id]);
if (is_null($a_offer) && !is_null($a_deal)) $isShowBlockRate = false;
if (!is_null($a_offer) && is_null($a_deal)) $isShowBlockRate = true;
if (!is_null($a_offer) && !is_null($a_deal)) $isShowBlockRate = false;</pre>

        <p>Для DEAL</p>

        <pre>$a_offer = Assessment::findOne(['user_id' => $offer->user_id, 'deal_id' => $Deal->id]);
$a_deal = Assessment::findOne(['user_id' => $Deal->user_id, 'deal_id' => $Deal->id]);
if (is_null($a_deal) && !is_null($a_offer)) $isShowBlockRate = false;
if (!is_null($a_deal) && is_null($a_offer)) $isShowBlockRate = true;
if (!is_null($a_deal) && !is_null($a_offer)) $isShowBlockRate = false;</pre>


        <h2 class="page-header">Страница сделки. Информация о партнере</h2>

        <p><img src="https://neiro-n.com/u/15967/16350_K8xwzQqDdI.png"></p>

        <h2 class="page-header">Ситуация 75280808</h2>
        <p>Убирать из списка предложений в обменнике те предложения по продаже монет, кошельки с которыми имеют баланс не менее, чем максимальная сумма монет в предложении.</p>
        <p>Сделаю в консоли раз в 10 мин: ищет такие предложения:</p>
        <p>ищет такие предложения и скрывает их:</p>
        <pre>php yii offer/test-empty-and-hide</pre>
        <p>Выводит при закрытии лог категории <code>avatar\console\offer\test-empty-and-hide</code></p>

        <p>ищет такие предложения и показывает:</p>
        <pre>php yii offer/test-empty</pre>

        <h2 class="page-header">Логика удаления предложения</h2>
        <p>1."Закрыть" - скрывает из предложений в обменнике.<br>
            2."Удалить" - скрывает из предложений в обменнике и скрывает из личного списка</p>
        <p><code>is_hide</code> - скрывается из общего списка, но в своем остается виден, Если = 2 то скрывается из общего списка и своего</p>

        <h2 class="page-header">Открытие сделки</h2>
        <p>Сделка открывается на нажатие кнопки Купить или продать. При этом вызывается AJAX <code>/cabinet-exchange/deal-open-ajax</code> и в сделке ставится статус <code>STATUS_CREATE</code>.</p>

        <h2 class="page-header">Таблица конвертации нейрона (NEIRO)</h2>
        <p>Параметры проценты сохраняются в БД под ключом <code>NeironConvertParams</code>.</p>
        <p>Параметры в интерфейсе сохраняется на странице <code>/admin-neiro-convert/index</code>.</p>
        <p>[{"id":1,"in":2,"out":0},{"id":2,"in":2,"out":0}]</p>
        <p><code>id</code> - идентификатор валюты db.currency.id<br>
            <code>in</code> - coin > NEIRO<br>
            <code>out</code> - NEIRO > con</p>

        <pre>
Если я меняю PZM на NEIRO
то формула расчета через доллар
PZM = {кол-во монет NEIRO} * {цена NEIRO в USD} / {цена PZM в USD}

Если вставлять сюда добавочный процент то куда?
Логично если я хочу себе в карман положить то я отдам просто меньше
значит допустим если я хочу оставить 2% у себя то формула такая
PZM = {кол-во монет NEIRO} * {цена NEIRO в USD} * (1 - 0,02) / {цена PZM в USD}

Если я хочу поменять PZM на NEIRO то формула такая
NEIRO = {кол-во монет PZM} * {цена PZM в USD} / {цена NEIRO в USD}
c процентом аналогично
NEIRO = {кол-во монет PZM} * {цена PZM в USD} * (1 - 0,02) / {цена NEIRO в USD}
        </pre>
        <h2 class="page-header">Таблица конвертации MARKET</h2>
        <p>Параметры проценты сохраняются в БД под ключом <code>MarketConvertParams</code>.</p>

        <h2 class="page-header">Кнопка перевыставить ордер</h2>
        <p>Эта функция предназначена для того чтобы быстро можно было перевыставить ордер после снятия его по истечению времени размещения в срок 6 суток.</p>
        <p>Анализ показа кнопки: Если ордер скрыт, то будет показываться кнопка "Перевыставить"</p>

        <h2 class="page-header">Ограничитель перевода PZM</h2>
        <p>100,000 в день. Для всех. Буду хранить переменную в БД config под ключом <code>pzm_exchange_limit</code></p>
        <p>Раз в день запускается</p>
        <pre>php yii prizm/reset-counter</pre>
        <pre>0 0 * * * docker exec s-routes-php-fpm php yii prizm/reset-counter</pre>




        <hr style="margin-top: 200px;">
    </div>
</div>


