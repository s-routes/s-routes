<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Колокольчик';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Концепт</h2>

        <p>В меню показываются новости</p>
        <p>Алгоритм когда колокольчик качается (мигает)</p>
        <p>Для реализации вводится значение - Время последнего просмотра меню колокольчика. Оно располагается в <code>user.last_see_kolokol</code></p>
        <p>Если время самой  свежей новости больше чем <code>user.last_see_kolokol</code> то колокольчик мигает. Если меньше то не мигает.</p>
        <p>В меню отображается 10 последних записей.</p>


    </div>
</div>
