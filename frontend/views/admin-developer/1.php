<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Валюты вывод';


?>

<div class="container">
    <div class="col-lg-12">
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'currency_crypto',
            'description' => 'Монета Crypto',
            'model'       => '\common\models\CurrencyCrypto',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'code',
                    'type'        => 'varchar(6)',
                    'isRequired'  => false,
                    'description' => 'Код монеты',
                ],
                [
                    'name'        => 'title',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Наименование монеты',
                ],

            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'user_bill_crypto',
            'description' => 'Монета Crypto',
            'model'       => '\common\models\avatar\UserBillCrypto',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(46)',
                    'isRequired'  => false,
                    'description' => 'Крипто адрес',
                ],
                [
                    'name'        => 'password',
                    'type'        => 'varchar(256)',
                    'isRequired'  => false,
                    'description' => 'Зашифрованный пароль',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Наименование счета',
                ],
                [
                    'name'        => 'mark_deleted',
                    'type'        => 'tinyint',
                    'isRequired'  => false,
                    'description' => 'Флаг Удален?',
                ],
                [
                    'name'        => 'currency',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор крипто монеты',
                ],
            ],
        ]) ?>
    </div>
</div>
