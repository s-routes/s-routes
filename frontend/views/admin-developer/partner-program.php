<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Партнерская программа';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>
            <a href="https://app.diagrams.net/#G1Td2rd76cX4mGum7dSA8XgAGKN_70KUMl" target="_blank">
                <img src="https://neiro-n.com/u/16071/43289_9AesrQjaEm.jpg"/>
            </a>
        </p>

        <p>Начисление партнерских вознаграждений происходит после успешного события покупки монет NEIRO. Покупка монет осуществляется на обменнике P2P через официальный аккаунт.</p>
        <p>Начисление партнерских вознаграждений происходит монетами NEIRO.</p>
        <ul>
            <li>· с первой линии – 5%</li>
            <li>· со второй – 3%</li>
            <li>· с третьей – 1%</li>
            <li>· с четвёртой – 3%</li>
            <li>· с пятой – 5 %</li>
        </ul>

        <h2 class="page-header">Параметры</h2>
        <p>Сохраняются в файле <code>common/config/params.php</code></p>
        <p><code>referal-program</code> - массив где</p>
        <p><code>user_id</code> - пользователь который продает нейроны</p>
        <p><code>wallet_id</code> - кошелек откуда берутся реферальные нейроны</p>

        <h2 class="page-header">Модель данных</h2>
        <p><code>school_referal_level</code> - список процентов по уровням</p>
        <p><code>school_user_partner</code> - кто за кем стоит</p>
        <p><code>school_referal_transaction</code> - реферальные начисления</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_user_partner',
            'description' => 'Кто за кем стоит',
            'model'       => '\common\models\UserPartner',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор школы, не используется',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя для которого существует запись о том кто его родитель',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Родитель',
                ],
            ],
        ]) ?>
        <p>Если у пользователя нет родителя то и записи значит нет.</p>

        <h2 class="page-header">Событие покупки монет</h2>
        <p>По факту этого события происходит начисление реферальных начислений.</p>
        <p>Где оно в коде? По факту в сделке после перевода монет клиенту. Событие <code>Deal::EVENT_AFTER_MONEY_SENDED</code>. <code>\common\models\exchange\Deal::init()</code></p>

        <h2 class="page-header">Как учитывается реферал?</h2>
        <p>В ссылке параметр <code>partner_id</code></p>
        <p>В переменной сессии <code>ReferalProgram</code> массив <code>['partner_id' => (int)]</code></p>

        <h2 class="page-header">Параметр который указывает можно ли участвовать в рефералке</h2>
        <p><code>user.is_referal_program</code></p>
        <p>Если человек указывает ссылку пользователя у кого не включена рефералка то он не сможет зарегистрироваться</p>

        <h2 class="page-header">Рефералка от конвертации</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'school_referal_transaction',
            'description' => 'Реферальные начисления',
            'model'       => '\common\models\ReferalTransaction',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'school_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор школы, не используется',
                ],
                [
                    'name'        => 'request_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор операции, referal_operation.id',
                ],
                [
                    'name'        => 'level',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Уровень рефералки, например 1 - на одного вышестоящего и тд',
                ],
                [
                    'name'        => 'from_uid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя, от кого возникла рефералка',
                ],
                [
                    'name'        => 'to_uid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя, кому начислена рефералка',
                ],
                [
                    'name'        => 'to_wid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька, кому начислена рефералка',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во начисленных нейронов, в атомах',
                ],
                [
                    'name'        => 'transaction_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент транзакции',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты, dbWallet.currency.id',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'referal_operation',
            'description' => 'Операция после которой возникла рефералка',
            'model'       => '\common\models\ReferalOperation',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Тип операции, 1 - покупка у доверенного представителя, 2 - конвертация',
                ],
                [
                    'name'        => 'object_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор строки объекта в зависимости от type_id, 1 - deal.id 2 - convert_operation.id, 3 - binance_order.id',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во денег которое пришло в систему, в атомах',
                ],
                [
                    'name'        => 'amount_referal',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во денег которое ушло на рефералку, в атомах',
                ],
            ],
        ]) ?>

        <hr style="margin-top: 200px;">
    </div>
</div>


