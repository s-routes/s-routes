<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Валюты вывод';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>В интерфейсе <code>\avatar\services\currency\CurrencyInterface</code> существует функция <code>out_algoritm</code> которая вызывается в форме вывода средств</p> <p>В этой фнукции регистрируется JS код для обработки полей. Эта функция выдает массив с параметрами:</p>
        <p>comission_options - массив для выбора комиссии или null</p>
        <p>is_show_comission - bool показывать строку с комиссией?</p>

        <pre>$('#taskoutput-comission_user') комиссия которую заплатит пользователь, в атомах
$('#sum_exit') сумма которую пользователь получит на руки
$('#taskoutput-amount') сумма которую пользователь хочет вывести у себя</pre>

        <h2 class="page-header">Жизненный цикл заявки на вывод</h2>
        <p>
            <a href="https://app.diagrams.net/#G1P7k4Eo0jqcIhGkBdpisFp0mYOgRyXluf" target="_blank">
                <img src="/images/controller/admin-developer/currency-out/bi.png">
            </a>
        </p>
        <p>Есть список валют которые для вывода на бинанс <code>\common\base\Application::$binanceAutoOutput</code>. Эти валюты проходят по первому сценарию. По факту кнопки "Выполнить" выдается запрос 2FA и заявка на выввод инициируется. По url <code>/admin-buh/done-ajax</code> проверяется 2FA, а по url <code>/admin-buh/done-ajax2</code> инициализируется вывод. А в консольной команде <code>yii output/binance-finish</code> проверяется когда он завершится (достигнет достаточного кол-ва подтверждений).</p>
        <p>Другие валюты выводятся через форму <code>/admin-buh/done</code></p>

        <h2 class="page-header">Вывод</h2>

        <p>При выводе формируется заявка на вывод которая ниже <code>task_output</code>, далее бухгалтер заходит в админку и переводит монеты пользователю, внутренние монеты списываются во внутрений кошелек <code>currency_io.main_wallet</code>.</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'task_output',
            'description' => 'Задача по выводу монет из фонда',
            'model'       => '\common\models\TaskOutput',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_io_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'идентификатор монеты для ввода/вывода currency_io.id',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус задачи 0 - задача создана. 1 - задача выполнена, транзакция указана, 2 - Задача отклонена. 4 - инициализирован вывод для Binance. default 0',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания задачи',
                ],
                [
                    'name'        => 'finished_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент закрытия задачи, то есть принятия решения о принятии задачи или отклонении',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов для вывода, разрядность после запятой 2',
                ],
                [
                    'name'        => 'txid_internal',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции внутренней по списасанию монет внутренних c кошелька пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета с которого списывать монеты',
                ],
                [
                    'name'        => 'buh_user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя совершивший вывод',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'db.currency.id',
                ],
                [
                    'name'        => 'account',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Адрес кошелька призм на который надо вывести деньги',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор транзакции по которой будут выведены живые монеты',
                ],
                [
                    'name'        => 'comment',
                    'type'        => 'varchar(1000)',
                    'isRequired'  => false,
                    'description' => 'Комментарий к заявке',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя который выводит монеты',
                ],
                [
                    'name'        => 'prizm_key',
                    'type'        => 'varchar(160)',
                    'isRequired'  => true,
                    'description' => 'Публичный ключ для PRIZM',
                ],
                [
                    'name'        => 'comission',
                    'type'        => 'bigint',
                    'isRequired'  => true,
                    'description' => 'Комиссия которую указывает бухгалтер, если она превышает стандартную',
                ],
                [
                    'name'        => 'comission_id',
                    'type'        => 'tinyint',
                    'isRequired'  => true,
                    'description' => 'Идентификатор комиссии 1 - малая 2 - средняя 3 - высокая',
                ],
                [
                    'name'        => 'comission_user',
                    'type'        => 'bigint',
                    'isRequired'  => true,
                    'description' => 'Величина комиссии в атомах которая считается на момент создания заявки',
                ],
                [
                    'name'        => 'binance_id',
                    'type'        => 'varchar(40)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор заявки на вывод для бинанса',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Форма вывода у бухгалтера</h2>
        <p>Если бухгалтер указал комиссию больше чем обычно, то она сохраняется в поле <code>comission</code></p>

        <h2 class="page-header">Места указания комиссий</h2>
        <p>- Форма вывода у клиента</p>
        <p>- Таблица бухгалтера</p>
        <p>- Форма вывода у бухгалтера</p>

        <h2 class="page-header">PRIZM</h2>

        <h2 class="page-header">ETHEREUM</h2>
        <p>что нужно сделать?</p>
        <p>Вывести среднюю цену транзакции. Где ее сохранять? в Бд <code>config</code>.</p>
        <p><code>EthereumSafeGasPrice</code> - низкая цена газа для медленных транзакций</p>
        <p><code>EthereumProposeGasPrice</code> - средняя цена газа</p>
        <p><code>EthereumFastGasPrice</code> - дорогая цена газа для быстрых транзакций</p>
        <p>Как часто ее обновлять? Раз в мин.</p>
        <pre>php yii ethereum/gas-price</pre>
        <p>Далее вывожу эту цену при выводе. Для ETH считаю 21,000 газа. Для токенов 70,000 газа.</p>

        <h2 class="page-header">ERC-20</h2>
        <p>Все тоже самое что и для эфира только цена газа равна 70,000 газа.</p>
        <p>Цена комиссии в токенах = цена комиссии в эфире * цена эфира в USD / цена токена в USD.</p>


        <h2 class="page-header">BITCOIN</h2>
        <p>Для BTC, LTC, DASH стоимость транзакции берется из API <code>https://api.blockcypher.com/v1/{coin}/main</code>.</p>
        <p>Вместо <code>{coin}</code> монета btc, ltc или dash.</p>
        <p>Параметры (такие же как и у эфира, только вместо блокчейна указана монета в CamelCaseStyle):</p>

        <p><code>BtcSafeGasPrice</code> - низкая цена за транзакцию из 500 байт для медленных транзакций, satoshi</p>
        <p><code>BtcProposeGasPrice</code> - средняя цена за транзакцию из 500 байт, satoshi</p>
        <p><code>BtcFastGasPrice</code> - дорогая цена за транзакцию из 500 байт для быстрых транзакций, satoshi</p>

        <h2 class="page-header">Функция формы вывода</h2>
        <p>Для отображения формы вывода существует функция <code>\avatar\services\currency\CurrencyInterface::out_algoritm($cInt, $cExt, $options)</code></p>
        <p>Возврат:</p>

        <pre>[
    'comission_options' = массив для выбора комиссии / null
    'is_show_comission' = bool показывать строку с комиссией
]</pre>

        <h2 class="page-header">Вывод через Binance</h2>
        <p>Для вывода необходим ключ с правом на вывод</p>
        <p>Функция: <code>wapi/v3/withdraw.html</code></p>
        <pre>/** @var \frontend\modules\Binance\Binance $provider */
$provider = Yii::$app->Binance;
$data = $provider->_callApiKeyPostAndSigned('wapi/v3/withdraw.html', [
    'asset'   => 'TRX',
    'address' => 'TPLg4hDR2jwybEwerJC3xsGWSR2B3URrGU',
    'amount'  => '5',
    'timestamp'  => time() * 1000,
]);</pre>
        <p>Монеты для вывода: BTC, LTC, DASH, ETH, USDT/ETH, USDT/TRX, TRX</p>
    </div>
</div>
