<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Роли';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><code>role_arbitrator</code> - Арбитр</p>
        <p><code>role_admin</code> - Админ</p>
        <p><code>role_buh</code> - Бухгалтер</p>

    </div>
</div>