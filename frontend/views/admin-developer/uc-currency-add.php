<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'UC. Добавление новой монеты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <p>Добавить в таблицу db.currency.id</p>
        <p>Добавить в таблицу dbWallet.currency.id</p>
        <p>Добавить в таблицу db.currency_io.id</p>
        <p>Создать всем пользователям кошельки</p>
        <pre>/**
 * @var \common\models\UserAvatar $u
 */
foreach (\common\models\UserAvatar::find()->all() as $u) {
    $data = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::DAI, $u->id);
    echo 'uid='.$u->id.' bid=' . $data['billing']['id'] . "\n";
}</pre>
        <p>Добавить в список на странице <code>/cabinet-bills/index.php</code></p>
        <p>Добавить в список на странице <code>/cabinet/wallets.php</code></p>

        <p>Проценты на вывод: <code>/admin-comission/index</code></p>
        <p>Минимальный вывод через бинанс: <code>console/controllers/OutputController.php:164</code></p>


        <h2>Создание новой монеты</h2>
        <p>\common\base\Application::addCoin</p>
        <pre>$o = [
    'code'                      => 'DOGE',
    'title'                     => 'DOGE',
    'decimals'                  => 8,
    'decimals_view'             => 8,
    'master_wallet'             => '',
    'master_wadllet_is_binance' => 1,
    'function_check'            => '',
    'validate_account'          => '',
    'field_name_wallet'         => 'doge',
    'warning'                   => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме DOGE - приведет к их потере.',
];

$cInt = \common\models\piramida\Currency::add([
    'name'               => $o['title'],
    'code'               => $o['code'],
    'decimals'           => $o['decimals'],
    'decimals_view'      => $o['decimals_view'],
]);
$cExt = \common\models\avatar\Currency::findOne(['code' => $o['code']]);
$wMain = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
$wBlock = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
$wLost = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
$p = \common\models\exchange\Page::add(['name' => $o['code']]);
$CIO = \common\models\CurrencyIO::add([
    'currency_ext_id'          => $cExt->id,
    'currency_int_id'          => $cInt->id,
    'main_wallet'              => $wMain->id,
    'block_wallet'             => $wBlock->id,
    'lost_wallet'              => $wLost->id,
    'is_exchange'              => 1,
    'is_io'                    => 1,
    'info_page_id'             => $p->id,
    'info_page_shop_id'        => $p->id,
    'master_wallet_is_binance' => $o['master_wadllet_is_binance'],
    'function_check'           => $o['function_check'],
    'validate_account'         => $o['validate_account'],
    'field_name_wallet'        => $o['field_name_wallet'],
]);
$this->addColumn('user_wallet', $o['field_name_wallet'], 'varchar(100)');

// Добавляю модификацию
if (false) {
    \common\models\CurrencyIoModification::add([
        'currency_io_id'           => $CIO->id,
        'master_wallet'            => '0x3a736b425ed07f2c7f033a15c51d81e6c424c7e0',
        'master_wallet_is_binance' => 1,
        'name'                     => 'DAI ERC-20',
        'wallet_field_name'        => 'dai',
        'function_check'           => '\avatar\services\currency\Dai',
        'code'                     => 'ETH',
        'warning'                  => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме DAI ERC20 - приведет к их потере.',
    ]);
}

// Добавляю кошельки
/**
 * @var \common\models\UserAvatar $u
 */
foreach (\common\models\UserAvatar::find()->all() as $u) {
    $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cInt->id, $u->id);
    echo 'uid='.$u->id.' bid=' . $data['billing']['id'] . "\n";
}

// Устанавливаю sort_index
$c = \common\models\avatar\Currency::findOne($cExt->id);
$s = \common\models\avatar\Currency::find()->select('max(sort_index)')->scalar();
$c->sort_index = $s + 1;
$c->save();</pre>

        <h2>Добавление модификации к уже существующей монете</h2>
        <pre>$CIO = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::BNB);
\common\models\CurrencyIoModification::add([
    'currency_io_id'           => $CIO->id,
    'master_wallet'            => 'bnb136ns6lfw4zs5hg4n85vdthaad7hq5m4gtkgf23',
    'master_wallet_is_binance' => 1,
    'name'                     => 'BNB BEP2',
    'extended_info'            => \yii\helpers\Json::encode(['memo' => '100232884']),
    'wallet_field_name'        => 'bnb_bep2',
    'function_check'           => '',
    'code'                     => 'BNB',
    'warning'                  => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме BNB - приведет к их потере.',
]);
$this->addColumn('user_wallet', 'bnb_bep2', 'varchar(100) default null');</pre>

    </div>
</div>



