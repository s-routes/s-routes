<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Резервное копирование БД';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2>Резервное копирование на PROD</h2>

        <p>Всего баз данных три: главная, кошельки и статистика</p>
        <p>Копируются две бд (главная и кошельки)</p>
        <p>в течение суток каждые 5 мин.</p>
        <p>в течение недели каждый день в 00:00.</p>
        <pre>sudo tail -F /var/log/cron.log</pre>





        <h2>Заливка БД PROD->STAGE</h2>
        <p>Файлы лежат в папке db</p>


        <ul>
            <li>Зайти на PROD</li>
            <li>Вызвать scp</li>
            <li>Зайти на STAGE</li>
            <li>Вызвать <code>./backup_app_stage_get_actual.sh</code></li>
            <li>Вызвать <code>./d_db_load.sh [DB_PASSWORD]</code></li>
        </ul>

        <p>Копирует БД но спрашивает пароль, без вывода на экран</p>
        <pre>scp -r /home/ubuntu/backup_app/dumps ubuntu@51.210.242.240:/home/ubuntu/backups_db_prod &>/dev/null</pre>

        <p>Копирует БД но спрашивает пароль, с выводом на экран</p>
        <pre>scp -r /home/ubuntu/backup_app/dumps ubuntu@51.210.242.240:/home/ubuntu/backups_db_prod</pre>

        <p>Чтобы не спрашивал пароль:</p>
        <p><a href="https://www.dmosk.ru/miniinstruktions.php?mini=scp-without-password" target="_blank">https://www.dmosk.ru/miniinstruktions.php?mini=scp-without-password</a></p>

        <p>Зайти на STAGE</p>
        <pre>cp /home/ubuntu/backups_db_prod/dumps/day/0000/* /home/ubuntu/s-routes/</pre>
        <pre>gunzip -d /home/ubuntu/s-routes/s_routes_prod_main.sql.gz</pre>
        <pre>gunzip -d /home/ubuntu/s-routes/s_routes_prod_wallet.sql.gz</pre>
        <pre>./d_db_load.sh password</pre>

        <pre>При копировании на STAGE существует проблема
        при заливке таблиц старые не удаляются, а перед заливкой надо удалить все таблицы
            как это сделать?</pre>
        <p>Можно перед заливкой удалить и создать БД.</p>








        <h2>Как сделать копирование БД на другие серваки через SSH</h2>


        <p>БД на PROD dump'ятся в папку <code>./db/dump</code> . DUMP делается раз в 5 мин.</p>

        <p>БД на STAGE заливаются с PROD в папку <code>/home/ubuntu/backups_db_prod</code></p>
        <p>Скопировать с PROD ключ:</p>
        <pre>sudo scp /root/.ssh/id_rsa.pub root@161.97.172.91:/root/.ssh/authorized_keys</pre>
        <pre>sudo scp /root/.ssh/id_rsa.pub root@161.97.172.92:/root/.ssh/authorized_keys</pre>



        <p>Скопировать БД с PROD на RESERV:</p>
        <pre>sudo scp -r /root/s-routes/db/dumps root@161.97.172.91:/root/backups_db_prod</pre>
        <pre>sudo scp -r /root/s-routes/db/dumps root@161.97.172.92:/root/backups_db_prod</pre>

        <h2>Скопировать БД для DEV</h2>
        <pre>ритерии завершения:
Решение 1
Берется полная база, удаляется все лишнее, делается экспорт БД
Решение 2
Берется пустая БД, заливается добавка,
вопрос как формировать добавку, при том что проект постоянно растет, значит и добавка будет меняться
значит надо сделать методологию формирования добавки.
Добавку можно сделать в виде JSON и функцию которая добавляет и экспортирует эту добавку

какой вариант лучше?
мне нравится второй
хотя не понятно пока насколько он оптимальнее</pre>

        <pre>При удалении БД будет более актуальнее
Удалить проще чем при добавлении полей или реструктуризации БД добавлять еще и в добавку, точнее учитывать ее.
В первом варианте по любому тоже надо будет учитывать, но все же думаю что первый случай в этой ситуации более актуальный.

Какой контекст этой ситуации?
Приходит новый человек и ему надо подготовить рабочее место. Для этого ему надо выдать обрезанную БД.

Что в этой ситуации делаю?
Копирую себе полную БД
Обрезаю
Делаю экспорт

По моему нормально

Делаю</pre>
        <p>Копирую себе полную БД</p>
        <pre>LINUX
WINDOWS
        </pre>


        <p>Обрезаю</p>
        <pre>yii db/cut-for-dev</pre>


        <p>Делаю экспорт</p>
        <pre></pre>


        <p>
            <a href="https://app.diagrams.net/#G16qxjKDkHcZYyEI6rmtGDrl4eV7BgtMwq" target="_blank">
                <img src="/images/controller/admin-developer/install/db_copy.png">
            </a>
        </p>
        <p>RESERV отвечает на <code>http://reserv.neiro-n.com</code></p>

    </div>
</div>



