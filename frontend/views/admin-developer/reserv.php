<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'RESERV';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2>Резервный сервер</h2>
        <p><a href="https://app.diagrams.net/#G1gXlGgBEETsmsv7-RzpN2sucF-u5f3bBH" target="_blank">https://app.diagrams.net/#G1gXlGgBEETsmsv7-RzpN2sucF-u5f3bBH</a> </p>

        <p>Алгоритм запуска резервного сервера<br>
            1. Залить на него БД<br>
            2. Обновить гит<br>
            3. Сменить IP на доменном имени</p>

        <p>С какого docker файла запускается RESERV? <code>docker-compose_prod.yml</code></p>
        <pre>docker-compose -f docker-compose_prod.yml up -d</pre>

        <p><b>Заливка БД STAGE-RESERV</b></p>

        <p>На PROD</p>
        <pre>scp -r /home/ubuntu/backup_app/dumps ubuntu@51.210.5.164:/home/ubuntu/s-routes/db</pre>

        <p>На RESERV</p>
        <pre>./d_db_load_reserv.sh {DB_ROOT_PASSWORD}</pre>

        <p><b>Старт сервера после перезагрузки</b></p>
        <pre>cd /root/ubuntu/s-routes
docker-compose -f docker-compose_prod.yml up -d</pre>


        <p><b>Обновить гит</b></p>
        <p>Можно сделать и автозаливку на РЕЗЕРВ и останется только сделать </p>
        <pre>cd /root/ubuntu/s-routes
git pull</pre>

        <p><b>Смена IP</b></p>
        <ul>
            <li>neiro-n.com <code>49c7be171cd66eff417c9ddbfa04cc93</code> dns_record_id:<code>d2f4e4265e6df4a4c9669ce7607fce9d</code> dns_record_id:<code>fe0ed7e0f6fd9eec3cbcb4369b9d016f</code></li>
            <li>topmate.one <code>c2e4029ab4e9a466331b53e7ca2b8050</code> dns_record_id:<code>1cb13da3cb5144381d594aeeda30f678</code></li>
            <li>neironcoin.com <code>a2970a2131c2c55b80ddcde0da457464</code> dns_record_id:<code>d2f4e4265e6df4a4c9669ce7607fce9d</code> dns_record_id:<code>fe0ed7e0f6fd9eec3cbcb4369b9d016f</code></li>
        </ul>
        <p>В виду того что все имена находятся на cloudlare то можно сделать управление с STAGE переключение IP</p>

    </div>
</div>
