<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Магазин рефералка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>




        <p>❶ Процент конвертации. Увеличиваем процент конвертации в маркет-баллы на 3% и распределяем их в линейный маркетинг для сетевиков. 3% маржи с каждой конвертации распределяются так:</p>
        <p>1 уровень 1.0%</p>
        <p>2 уровень 1.0%</p>
        <p>3 уровень 0.5%</p>
            -------------------------
        <p>Оставшиеся 0,5% остаются нам для поддержки проекта</p>

        <p>❷ Подписка на пользование сервисом. Это еще один из инструментов для сетевиков и инструмент для создания ценности сервиса для пользователя. Есть 2 этапа:</p>

        <p>А) «Тест-драйв» => Подписку покупать не нужно, но: максимальный общий объем покупок без подписки на сервис ограниченный, и =10.000 маркет-баллов. Время «тест-драйва» неограничено.</p>
        <p>Б) Если человек хочет делать покупки более 10.000 маркет-баллов – он должен купить подписку.</p>
        <ul>
            <li>Подписка – на 30 суток (ежемесячная).</li>
            <li>Подписка – лимитная. Есть семь градаций по суммам планируемых покупок: (пакет 10.000 МБ -> его цена 300 МБ) (пакет 20.000 МБ -> его цена 600 МБ), (пакет 50.000 МБ -> его цена 1.500 МБ), (пакет 100.000 МБ -> его цена 3.000 МБ), (пакет 300.000 МБ -> его цена 9.000 МБ), (пакет 500.000 МБ -> его цена 15.000 МБ), (пакет 1.000.000 МБ -> его цена 30.000 МБ). Т.е. цена пакета - 3% от суммы планируемых покупок.</li>
            <li>Если человек выбрал стоимость пакета или ему не хватает остатков подписки, он покупает следующий пакет (остатки от суммы предыдущего - сгорают), - и у него запускается новый отсчет с новыми лимитами и сроком истечения пакета в 30дней.  Пример: клиент купил пакет за 3.000МБ (лимит 100.000МБ) -> израсходовал 80.000 МБ (20.000МБ осталось от пакета) -> ему мало -> он покупает новый за 9.000 МБ -> и у него открывается пакет с лимитом 300.000МБ сроком на 30 дней. При этом остаток в 20.000МБ от предыдущего пакета в 100т.МБ – сгорает.</li>
        </ul>

        <p>❸ Средства, собранные от подписок распределяются так:</p>
        <p>А) 30% от подписок отдаем в МЛМ-сеть, и распределяются они в линейный маркетинг на пять уровней:</p>

        <ul>
            <li>1 уровень 10%</li>
            <li>2 уровень 8%</li>
            <li>3 уровень 6%</li>
            <li>4 уровень 4%</li>
            <li>5 уровень 2%</li>
        </ul>

        <p>Б) Остальные 70% идут в компанию на развитие проекта, накладные расходы, и оплату staffа.</p>

        <p>Средства 30% и 70% нужно будет сделать с возможностью изменения, т.к. распределение 30/70% будет только на начальном этапе. По мере развития и расширения сети, в неё придется отдавать больший процент, а нам оставлять меньший.</p>





        <h2 class="page-header">Параметры</h2>
        <p>Сохраняются в файле <code>common/config/params.php</code></p>
        <p><code>shop-referal-program</code> - массив где</p>

        <h2 class="page-header">Модель данных</h2>
        <p><code>shop_user_partner</code> - кто за кем стоит</p>
        <p><code>shop_referal_transaction</code> - реферальные начисления</p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'shop_user_partner',
            'description' => 'Кто за кем стоит',
            'model'       => '\common\models\ShopUserPartner',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя для которого существует запись о том кто его родитель',
                ],
                [
                    'name'        => 'parent_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Родитель',
                ],
            ],
        ]) ?>
        <p>Если у пользователя нет родителя то и записи значит нет.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'shop_referal_transaction',
            'description' => 'Реферальные начисления',
            'model'       => '\common\models\ShopReferalTransaction',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'request_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор операции, referal_operation.id',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Тип заявки 1 - convert_operation, 2 - binance_order',
                ],
                [
                    'name'        => 'level',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Уровень рефералки, например 1 - на одного вышестоящего и тд',
                ],
                [
                    'name'        => 'from_uid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя, от кого возникла рефералка',
                ],
                [
                    'name'        => 'to_uid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя, кому начислена рефералка',
                ],
                [
                    'name'        => 'to_wid',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор кошелька, кому начислена рефералка',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во начисленных нейронов, в атомах',
                ],
                [
                    'name'        => 'transaction_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент транзакции',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты, dbWallet.currency.id',
                ],
            ],
        ]) ?>


        <h2 class="page-header">Регистрация</h2>
        <p>Есть два случая</p>
        <p>Пользователь если заходит по ссылке и регистрируется то он становится под этим человеком - <code>shop/models/forms/Registration.php:100</code></p>
        <p>Пользователь если заходит по ссылке и Совершает конвертацию то он становится под этим человеком - перед конвертацией пользователь добвляется в таблицу</p>

        <h2 class="page-header">Событие конвертации</h2>
        <p>Есть два вида конвертации и в обоих будет рефералка</p>
        <p>binance_order</p>
        <p>convert_operation - https://neiro-n.com/cabinet-wallet/convert-market?id=172</p>

        <h2 class="page-header">Как учитывается реферал?</h2>
        <p>В ссылке параметр <code>partner_id</code></p>
        <p>В переменной сессии <code>ReferalProgram</code> массив <code>['partner_id' => (int)]</code></p>


        <h2 class="page-header">Пакеты</h2>
        <p>Счетчик покупок у каждого клиента. Где в <code>user.market_counter</code>. Этот счетчик записывает на какое кол-во баллов совершает пользователь покупок.</p>
        <p>Сделать "докупить". Если человек купил 1 пакет то он может докупить только вышестоящий в период действия этого пакета.</p>
        <p>Если пакет не выкуплен то он сгорает</p>
        <p>Если пакет покупается новый, то старый сгорает и время течет заново.</p>
        <p>Значит надо запоминать какой пакет сейчас куплен. <code>user.market_packet</code></p>
        <p>Сущность покупки пакета <code>purchase_packet</code></p>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'purchase_packet',
            'description' => 'Покупка пакета',
            'model'       => '\common\models\PurchasePacket',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'тип 1 - покупка, 2 - докупка',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'цена покупки, в атомах MARKET',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'кто купил',
                ],
                [
                    'name'        => 'packet_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Какой пакет',
                ],
                [
                    'name'        => 'tx_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции покупки',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент транзакции',
                ],
            ],
        ]) ?>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'packet',
            'description' => 'Пакет',
            'model'       => '\common\models\Packet',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Кол-во получаемых единиц на покупки',
                ],
                [
                    'name'        => 'count',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Цена пакета в MARKET в атомах',
                ],

            ],
        ]) ?>

        <p>Параметр определяющий сколько отдаем в сеть лежит в бд config под названием <code>mlm_percent</code></p>

        <h2 class="page-header">Учет пакетов</h2>
        <p>Для баланса буду учитывать во внутренней монете MAR</p>
        <p>\common\models\Config:: <code>shop_packet_coin_main_wallet</code> - идентификатор главного кошелька</p>


        <h2 class="page-header">Списать у пользователя баллы</h2>
        <p>Сначала их надо проверить а потом списать</p>
        <p>Проверка</p>
        проверка перед началом оформления

        <p>Списание</p>
        <p>\shop\models\validate\ManagerRequestListClose::action()</p>

        <h2 class="page-header">Рефералка 1 и 2</h2>
        <p>При конвертации в МБ. \avatar\models\forms\Convert3::referal()</p>
        <p>При покупке пакета баллов. \shop\models\forms\CabinetMarkeyBuy::referal()</p>


        <h2 class="page-header">Условие рефералки</h2>
        <p>При любом построении реферальной структуры, для того, чтобы маркет-баллы начали зачисляться на счет, - он должен быть активирован покупкой хотя-бы минимального "Пакета возможностей" в 100,000 МБ (покупка за 3,000 МБ)</p>
        <p>Если куплен пакет то user.market_packet > 0 and user.market_till < time() and wallet.amount > 0</p>

        <h2 class="page-header">Рефералка от конвертации на бинансе</h2>
        <p>Рефералка образовывается после исполненного ордера</p>

    </div>
</div>


