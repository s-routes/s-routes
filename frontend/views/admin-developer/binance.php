<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Binance';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Как в коде подключиться к API</h2>

        <pre>/** @var \frontend\modules\Binance\Binance $provider */
$provider = Yii::$app->Binance;</pre>

        <h2 class="page-header">Как проверить API через URL</h2>
        <pre>https://neiro-n.com/test/binance</pre>
        <p>Если все успешно то должно выйти время бинанса</p>

        <h2 class="page-header">Алгоритм конвертации</h2>

        <p>Если пользователь нажал конвертацию </p>
        <p>
            <a href="https://app.diagrams.net/#G1QQZeI2scSME8bYgaGHhtSCnl25io8D06" target="_blank">
                <img src="/images/controller/admin-developer/binance/binance3.png">
            </a>
        </p>
        <p>
            посчитать сколько надо вернуть монет клиенту с учетом комиссии<br>
            я получил на бинансе 210 USD (amountReturnBinance)<br>
            как мне их перевести в NEIRO?<br>
            percentNeiro - сколько система берет денег за конвертацию<br>
            amountNeiro = amountReturnBinance * ((100-percentNeiro)/100) / Neirokurs
        </p>

        <p>
            Как проверить статус заявки на бинансе?
        </p>
        <pre>./d_yii.sh convert/check</pre>

        <h2 class="page-header">Тестирование</h2>
        <p><a href="https://testnet.binance.vision/" target="_blank">https://testnet.binance.vision/</a> </p>
        <p><a href="https://testnet.binance.vision/api" target="_blank">https://testnet.binance.vision/api</a> </p>

        <p>API Key: bHZOqznSFuuGN0581I4nORyrbgVPJDHee4K1Y7wvaw8pe8McyxuYzCjv2DaVbfHO</p>
        <p>Secret Key: 1okkOLMxrdTLyApJPIRve3xbR02WBQEDEFrc3D9SF36KOK424LsTfzT1ORSuOrKc</p>

        <h2 class="page-header">API</h2>
        <p><a href="https://binance-docs.github.io/apidocs/spot/en/" target="_blank">https://binance-docs.github.io/apidocs/spot/en/</a></p>


        <h2 class="page-header">БД</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'binance_order',
            'description' => 'Ордер на продаже на Бинансе',
            'model'       => '\common\models\BinanceOrder',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'convert_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор конвертации convert_operation.id',
                ],
                [
                    'name'        => 'user_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор пользователя',
                ],
                [
                    'name'        => 'billing_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор счета',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во сконвертированных монет',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время конвертации',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус ордера 0 - только создан, 1 - исполнен',
                ],
                [
                    'name'        => 'binance_id',
                    'type'        => 'varchar',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ордера от бинанса, binance.clientOrderId',
                ],
                [
                    'name'        => 'binance_order_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор ордера от бинанса, binance.orderId',
                ],
                [
                    'name'        => 'binance_return',
                    'type'        => 'varchar',
                    'isRequired'  => true,
                    'description' => 'то что бинанс выдает первый раз, сохраняется в виде JSON',
                ],
                [
                    'name'        => 'currency_to',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты во что конвертируем? db.currency.id',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Список конвертаций</h2>
        <p>BTC => NEIRO = BTC => USDT</p>
        <p>BTC => MERKET = BTC => USDT</p>

        <p>ETH => NEIRO = ETH => USDT</p>
        <p>ETH => MERKET = ETH => USDT</p>

        <p>LTC => NEIRO = LTC => USDT</p>
        <p>LTC => MERKET = LTC => USDT</p>

        <p>TRX => NEIRO = TRX => USDT</p>
        <p>TRX => MERKET = TRX => USDT</p>

        <p>DASH => NEIRO = DASH => USDT</p>
        <p>DASH => MERKET = DASH => USDT</p>

    </div>
</div>


