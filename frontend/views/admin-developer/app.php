
<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Application';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Концепт</p>
        <p>Сделать так чтобы Neiron::$app содержал объект <code>\common\base\Application</code></p>
        <p>Значит надо класс <code>Neiron</code> создать от <code>Yii</code>, а <code>\common\base\Application</code> от <code>\yii\web\Application</code></p>
        <p>Значит надо класс <code>Neiron</code> прописать в index.php</p>
    </div>
</div>
