<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'UC. Сброс 2FA';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2  class="page-header">Сделать, чтобы нельзя было сломать систему (напр. нарисовать доп. монеты). Сверка контр.сумм и т.д.</h2>
        <p>Путь к странице: <code>/auth/second-fa-reset</code></p>
        <p>После формы к кеш сохраняется объект восстановления</p>
        <pre>{
    id: int
    code: string
}</pre>
        <p>id - идентификатор пользователя</p>
        <p>code - код восстановления</p>
        <p>объект восстановления хранится в кеше 60 минут под ключом 'SECOND-FA-RESET'.</p>
        <p>После перехода по ссылке пользователь должен ввести логин и пароль. После этого он сбрасывает 2FA и входит в свой кабинет.</p>
        <p>Путь к странице: <code>/auth/second-fa-reset-step2</code></p>

    </div>
</div>



