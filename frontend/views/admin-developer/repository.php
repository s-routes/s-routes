<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Репозиторий';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <pre>git@bitbucket.org:s-routes/s-routes.git</pre>
        <h1 class="page-header">Ветки</h1>
        <p>master - боевой код (права доступа: только для чтения)</p>
        <p>develop - для разработки (права доступа: для всех)</p>

        <hr style="margin-top: 200px;">
    </div>
</div>


