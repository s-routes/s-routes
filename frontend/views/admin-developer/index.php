<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Разработка';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2>Система</h2>
        <p><a href="devops">DEVOPS</a></p>
        <p><a href="devops-db">Резервное копирование БД</a></p>
        <p><a href="devops-db-replication">БД репликация</a></p>
        <p><a href="reserv">RESERV</a></p>
        <p><a href="docker">docker</a></p>
        <p><a href="delete">Расчистка БД</a></p>
        <p><a href="git">git</a></p>
        <p><a href="env">env</a></p>
        <p><a href="cron">Cron</a></p>
        <p><a href="repository">Репозиторий и ветки</a></p>
        <p><a href="install">Инсталяция проекта</a></p>
        <p><a href="security">Безопасность</a></p>
        <p><a href="prizm">Prizm</a></p>

        <h2>Страницы</h2>
        <p><a href="page-admin-buh-index">Вывод</a></p>

        <h2>Модели данных и программирование</h2>

        <p><a href="rules">Правила программирования</a></p>
        <p><a href="main">VIM SERVER</a></p>
        <p><a href="chat">Чат</a></p>
        <p><a href="roles">Роли</a></p>
        <p><a href="currency">Валюты</a></p>
        <p><a href="test">Тестирование</a></p>
        <p><a href="notifications">Уведомления</a></p>
        <p><a href="support">Служба поддержки</a></p>
        <p><a href="profile">Прорфиль</a></p>
        <p><a href="language">Языки</a></p>
        <p><a href="telegram-bot">telegram-bot</a></p>
        <p><a href="buh">Бухгалтер</a></p>

        <h2>Обменник</h2>
        <p><a href="exchange">Обменник</a></p>
        <p><a href="hold">Холд токенов</a></p>
        <p><a href="partner-program">Партнерская программа</a></p>
        <p><a href="currency-out">Вывод монет</a></p>
        <p><a href="binance">Binance</a></p>
        <p><a href="convert">Конвертация</a></p>
        <p><a href="prizm-withdraw">Автовывод PRIZM</a></p>

        <h2>Магазин</h2>
        <p><a href="shop">Магазин</a></p>
        <p><a href="shop-referal">Лотерея</a></p>


        <h2>Лотерея</h2>
        <p><a href="lot">Лотерея</a></p>


        <h2>Сценарии</h2>
        <p><a href="uc-currency-add">Добавление новой монеты</a></p>
        <p><a href="uc-input">Ввод монет</a></p>
        <p><a href="uc-admin-notify">Возможность админу вывешивать и убирать транспарант предупреждения для клиентов</a></p>
        <p><a href="uc-prizm-out">Автоматизированный вывод PRIZM</a></p>
        <p><a href="second-fa-reset">Сброс 2FA</a></p>
        <p><a href="silver-dostavka">Заказ серебрянных монет</a></p>
        <p><a href="uc-market-report">Отчет по маркетам</a></p>

    </div>
</div>