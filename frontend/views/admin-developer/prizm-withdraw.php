<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Автовывод PRIZM';


?>

<div class="container">
    <div class="col-lg-12">
        <p>Осуществляется через PRIZM API</p>
        <p>https://pzm.space/pzm-integration/</p>
        <p>http://blockchain.prizm.space/api-doc/Examples.html#Send_Money</p>


        <pre>/** @var \common\services\PrizmApi $PrizmApi */
$PrizmApi = Yii::$app->PrizmApi;

$PrizmApi->post('withdraw', [
    'amountNQT'             => 1,
    'recipient'             => 'PRIZM-PP8C-HY3J-G5VU-PMQJV',
    'recipientPublicKey'    => '.....'
]);</pre>

        <?= \avatar\services\Params::widget([
            'params'     => [
                [
                    'name'        => 'amountNQT',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во копеек призма при условии 2 десятичных знаков',
                ],
                [
                    'name'        => 'recipient',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Кошелек назначения',
                ],
                [
                    'name'        => 'recipientPublicKey',
                    'type'        => 'string',
                    'isRequired'  => true,
                    'description' => 'Публичный ключ для кошелька назначения',
                ],
            ],
        ]) ?>



    </div>
</div>
