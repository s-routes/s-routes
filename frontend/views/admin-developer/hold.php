<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Холд токенов';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p>Надо сдеалать таблицу вкладов - <code>user_contribution</code><br>
            список типов вкладов - <code>contribution</code><br>
            каждый вклад пользователя имеет счет на котором и лежат токены</p>

        <h2 class="page-header">Таблицы</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'contribution',
            'description' => 'Вклад',
            'model'       => '\common\models\Contribution',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'time',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'время закладки токенов в сек',
                ],
                [
                    'name'        => 'percent',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'проценты за месяц * 1000',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'кол-во атомов заложенных во вклад (для монеты NEIRON) min',
                ],
                [
                    'name'        => 'amount_max',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'кол-во атомов заложенных во вклад (для монеты NEIRON) max',
                ],
                [
                    'name'        => 'name',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'название пакета',
                ],
            ],
        ]) ?>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'neiron_transaction',
            'description' => 'Типы транзакций для кошелька нейрон',
            'model'       => '\common\models\NeironTransaction',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'transaction_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'operation_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор операции',
                ],
                [
                    'name'        => 'type_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'тип транзакции',
                ],
            ],
        ]) ?>
        <p>
            <a href="https://app.diagrams.net/#G1oWJoCQCeIS8_GcJ9qKfuIkyTDdNYtFLo" target="_blank">
                <img src="/images/controller/admin-developer/hold/1.png" >
            </a>
        </p>

        <p>В кошельке NEIRON отображается баланс монет и в скобках показывается сколько монет доступно для стейкинга</p>
        <p>Как определить сколько монет доступно для стейкинга?</p>
        <p>Гипотеза 1</p>
        <p>Маркировать транзакции в кошельке</p>
        <p>1 - реферальные начисления</p>
        <p>2 - покупка нейронов с крана</p>
        <p>3 - перевод другу</p>
        <p>4 - конвертация</p>
        <p>5 - вклад</p>
        <p>Буду вручную считать от начала транзакций. Счетчик = 0. </p>
        <p>Если 1 тип то прибавляю</p>
        <p>Если 2 тип то прибавляю</p>
        <p>Если 3 тип. Если вывод то вычитаю, если ввод то ничего не делаю.</p>
        <p>Если 4 тип. Если вывод то вычитаю, если ввод то ничего не делаю.</p>
        <p>Если 5 тип. Если вывод то вычитаю, если ввод то ничего не делаю.</p>
        <p>Если нет типа, то ничего не делаю.</p>

        <p>Вроде нормально, а где будут храниться типы транзакций</p>
        <p>Например: <code>neiron_transactions</code></p>
        <p>id, transaction_id, operation_id, type_id</p>

        <h2 class="page-header">Завершение стейкинга</h2>
        <p>Есть функция консоли которая запускается раз в день</p>
        <pre>yii staking/close</pre>

        <h2 class="page-header">Уведомление о стейкинге ежедневный</h2>
        <p>Есть функция консоли которая запускается раз в день</p>
        <pre>yii staking/telegram-ping</pre>


    </div>
</div>


