<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'DevOps';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2>Доки Алексея</h2>

        <p><iframe width="560" height="315" src="https://www.youtube.com/embed/GSAkrZntuyg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
        <h2>Настройка сервера простого</h2>
        <h3>Подключение bitbucket к серверу</h3>

        <ul>
            <li>Создаю ключ на машине под root</li>
            <li>Копирую его в ключи репозитория bitbucket</li>
        </ul>

        <h3>Скачивание и установка проекта</h3>
        <ul>
            <li>Скачиваю репозиторий git clone git@bitbucket.org:alex3319/neuro_n.git</li>
            <li>Ставлю гит и другое ПО: apt-get install -y git make docker-compose</li>
            <li>mv .env.prod .env</li>
            <li>docker network create webproxy (Создаем сеть для ssl docker)</li>
            <li>sudo apt -V install gnupg2 pass</li>
            <li>docker login (авторизация в dockerhub)</li>
            <li>docker-compose up -d</li>
            <li>make stage-php-fpm</li>
            <li>php composer.phar update</li>
            <li>php yii migrate</li>
        </ul>

        <h3>Cron для Production</h3>
        <ul>
            <li><code>crontab -e</code></li>
            <li>Данные лежат в корне репозитория файл <code>cron.txt</code> Поправляем пути в зависимости от местоположения проекта. Держим файл в актуальном состоянии</li>
        </ul>




        <h2>Google Cloud Platform команды</h2>
        <p>
            Установка gcloud SDK - библиотека надстпройка над командой ssh для простой авторизации на VM
            и взаимодействия с API вашего гугл аккаунта. <br>
            <a href="https://cloud.google.com/sdk/install?hl=ru">https://cloud.google.com/sdk/install?hl=ru</a>
        </p>
        <pre>
gcloud config set project reference-flux-283315

gcloud config set project My First Project
gcloud config set project "My First Project"

gcloud compute ssh --project reference-flux-283315 --zone us-central1-a neuro-n-prod --command='ls'
gcloud compute ssh --project reference-flux-283315 --zone us-central1-a neuro-n-stage
        </pre>

        <h2>Установка gcloud</h2>
        <p>https://cloud.google.com/sdk/docs/downloads-apt-get</p>

        <h2>Вход в GCP через gcloud</h2>
        <p>gcloud init<br>
            Далее авторизоваться
            <br>

        </p>
        <h2>Запуск Stage на GCP</h2>
        <h3>Вход в GCP Stage</h3>
        <p>
            gcloud beta compute ssh --zone "us-central1-a" "neuro-n-stage" --project "reference-flux-283315"
        </p>
        <pre>
Старт Stage контейнеров
docker-compose --file docker-compose_gstage.yml up -d
composer global require "fxp/composer-asset-plugin:^1.2.0"
        </pre>

        <h3>Подключение Bucket к GCP Compute Engine</h3>
        <pre>
gcloud init - инициализируемся в проекте. Выбираем ответ Y и N в двух вопросах которые будут заданы

$ export GCSFUSE_REPO=gcsfuse-`lsb_release -c -s` $ echo "deb http://packages.cloud.google.com/apt $GCSFUSE_REPO main" | sudo tee /etc/apt/sources.list.d/gcsfuse.list$ curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -


$ sudo apt-get update
$ sudo apt-get install gcsfuse

72  mkdir backups_db_bucket
73  cd backups_db_bucket/
74  pwd
75  gcsfuse stage_neuro_n /home/alex3319/backups_db_bucket


Где example-bucket название бакета на странице
https://console.cloud.google.com/storage/browser?project=reference-flux-283315&prefix=
В столбце имя
например prod_neuro-n_bucket

Ждем минуту и заходим
        </pre>

        <h2>INSTALL</h2>
        <pre>
ssh-keygen
git clone git@bitbucket.org:s-routes/s-routes.git
cd s-routes
./install.sh
        </pre>

        <h2>STAGE</h2>
        поднять докер
        залить бд
        настроить копирование БД и ее заливку
    </div>
</div>



