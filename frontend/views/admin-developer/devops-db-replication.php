<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'БД репликация';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2>Исследование</h2>

        <p>https://www.youtube.com/watch?v=JXDuVypcHNA</p>
        <p>https://vitalyzhakov.github.io/post/mysql-replication-docker/</p>
        <p>https://habr.com/ru/post/521952/ - Mysql 8.x Group Replication (Master-Slave) with Docker Compose</p>

        <h2>Работа</h2>
        <p>RESERV - master</p>
        <p>STAGE - slave</p>
        <p>что сделать? отключить копирование в STAGE в RESERV</p>

        <h2>Команды</h2>
        <p>Конфигурационный файл находится по адресу /etc/mysql/conf.d/my.cnf</p>
        <p>Конфигурация mysql <code>./mysql-conf</code></p>



        <h3>PROD - MASTER</h3>
        <p>Старт контейнера:</p>
        <pre>docker-compose -f docker-compose_reserv_repl.yml up -d</pre>
        <p>Вход в контейнер:</p>
        <pre>docker exec -it s-routes-mysql mysql -u root -pYFT7ZU2RtgP32fJFD83W3TtoyTSqfJ</pre>
        <p>запуск</p>
        <pre>CREATE USER repl@'%' IDENTIFIED WITH mysql_native_password BY '1RFgHNZKKFTbK9fiL3xp';
GRANT REPLICATION SLAVE ON *.* TO repl@'%';</pre>


        <p>Проверка MASTER:</p>
        <pre>show slave hosts;</pre>
        <pre>show master status\G;</pre>

        <h3>STAGE - SLAVE</h3>
        <p>Старт контейнера:</p>
        <pre>docker-compose -f docker-compose_stage_repl.yml up -d</pre>
        <p>Вход в контейнер:</p>
        <pre>docker exec -it s-routes-mysql mysql -u root -p2JdFE0O55MeOxAzRcwev</pre>
        <p>запуск</p>
        <pre>CHANGE MASTER TO MASTER_HOST='***', MASTER_USER='repl', MASTER_PASSWORD='***', MASTER_PORT=***;
start slave;</pre>


        <p>docker:</p>
        <pre>version: "3.1"
services:
    mysql:
      image: mysql:8.0
      command: --max_allowed_packet=1073741824 --default-authentication-plugin=mysql_native_password
      container_name: s-routes-mysql
      working_dir: /application
      volumes:
        - .:/application
        - ./mysql:/var/lib/mysql
        - ./mysql-conf/slave.cnf:/etc/mysql/conf.d/repl.cnf
      environment:
        - MYSQL_ROOT_PASSWORD=***
      ports:
        - "***:3306"</pre>

        <p>Проверка SLAVE:</p>
        <pre>show slave status\G;</pre>



        <p>
            <a href="https://app.diagrams.net/#G1P4o5mlFa64_Qc6hRNe6urxilc-smjH5f" target="_blank">
                <img src="/images/controller/admin-developer/devops-db-replication/replication.png">
            </a>
        </p>
        <p>Успешный запуск:</p>
        <pre>
            1. PROD Снять дамп
            2. PROD Удалить старый mysql
            3. PROD Запустить новую БД
            4. SLAVE Удалить старый mysql
            5. SLAVE Запустить новую БД
            6. PROD Залить новую БД
        </pre>


    </div>
</div>



