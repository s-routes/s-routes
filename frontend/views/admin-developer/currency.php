<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Currency';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Концепт</h2>
        <p>В базе данных сохраняется атомах <code>pow(10, dbWallet.currency.decimals)</code>    </p>

        <?php
        $rows = [
            [
                'name'                             => 'USD',
                'db_currency_id'                   => 2,
                'db_currency_decimals'             => 2,
                'db_currency_decimals_view'        => 2,

                'db_wallet_currency_id'            => 15,
                'db_wallet_currency_decimals'      => 6,
                'db_wallet_currency_decimals_view' => 2,
            ],
            [
                'name'                             => 'ETH',
                'db_currency_id'                   => 4,
                'db_currency_decimals'             => 18,
                'db_currency_decimals_view'        => 8,

                'db_wallet_currency_id'            => 14,
                'db_wallet_currency_decimals'      => 18,
                'db_wallet_currency_decimals_view' => 8,
            ],
            [
                'name'                             => 'LET',
                'db_currency_id'                   => 229,
                'db_currency_decimals'             => 18,
                'db_currency_decimals_view'        => 2,

                'db_wallet_currency_id'            => 12,
                'db_wallet_currency_decimals'      => 18,
                'db_wallet_currency_decimals_view' => 2,
            ],
            [
                'name'                             => 'PZM',
                'db_currency_id'                   => 172,
                'db_currency_decimals'             => 2,
                'db_currency_decimals_view'        => 2,

                'db_wallet_currency_id'            => 13,
                'db_wallet_currency_decimals'      => 2,
                'db_wallet_currency_decimals_view' => 2,
            ],
        ];
        ?>
        <p><b>db_currency_decimals</b> знаков у валюты</p>
        <p><b>db_currency_decimals_view</b> знаков для отображения у валюты</p>
        <p><b>db_wallet_currency_decimals</b> знаков у валюты в блокчейне и для учета в нашей системе</p>
        <p><b>db_wallet_currency_decimals_view</b> знаков для отображения у валюты</p>
        <p><b>db_wallet_currency_decimals_view</b> идентично <b>db_currency_decimals_view</b></p>
        <p><b>db_wallet_currency_decimals_view_shop</b> идентично <b>db_wallet_currency_decimals_view</b> только для магазина</p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped'
            ],
            'summary' => '',
        ]) ?>

        <h2 class="page-header">Таблицы</h2>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'currency_io',
            'description' => 'Монета для ввода вывода',
            'model'       => '\common\models\CurrencyIO',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_ext_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты db.currency.id',
                ],
                [
                    'name'        => 'currency_int_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор валюты dbWallet.currency.id',
                ],
                [
                    'name'        => 'master_wallet',
                    'type'        => 'varchar(2000)',
                    'isRequired'  => false,
                    'description' => 'Мастер кошелек',
                ],
                [
                    'name'        => 'function_check',
                    'type'        => 'varchar(255)',
                    'isRequired'  => false,
                    'description' => 'Ссылка на функцию для проверки прихода денег',
                ],
                [
                    'name'        => 'validate_account',
                    'type'        => 'varchar(100)',
                    'isRequired'  => false,
                    'description' => 'Название функции в \avatar\models\forms\ActiveInput для проверки кошелка пользователя',
                ],
                [
                    'name'        => 'extended_info',
                    'type'        => 'text',
                    'isRequired'  => false,
                    'description' => 'JSON: допинфо tokenAddress - адрес токена',
                ],
                [
                    'name'        => 'main_wallet',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'идентификатор кошелька где лежат все монеты dbWallet.wallet.id',
                ],
                [
                    'name'        => 'tab_title',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Заголовок табуляции',
                ],
                [
                    'name'        => 'block_wallet',
                    'type'        => 'bigint',
                    'isRequired'  => false,
                    'description' => 'Идентификатор кошелька для блокировки dbWallet.wallet.id',
                ],
                [
                    'name'        => 'lost_wallet',
                    'type'        => 'bigint',
                    'isRequired'  => false,
                    'description' => 'Идентификатор кошелька для отправки на него монет которые в сделке были заблокированы и вышли по таймауту',
                ],
                [
                    'name'        => 'comission_out',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Комиссия при выводе, в той же монете которая выводится',
                ],
                [
                    'name'        => 'is_show_comission',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Показывать комиссию? 0 - нет, 1 - да. фиксированная комиссия',
                ],
                [
                    'name'        => 'benance_fee',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'комиссия бинанс на вывод, в монетах',
                ],
                [
                    'name'        => 'warning',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Предупреждение для формы ввода',
                ],
            ],
        ]) ?>
        <h2 class="page-header">Ввод</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'task_input',
            'description' => 'Задача по вводу призм в фонд',
            'model'       => '\common\models\TaskInput',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_io_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Адрес кошелька призм с которого производилась транзакция',
                ],
                [
                    'name'        => 'account',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Адрес кошелька призм с которого производилась транзакция',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'string',
                    'isRequired'  => false,
                    'description' => 'Идентификатор транзакции по которой клиент завел свои призмы',
                ],
                [
                    'name'        => 'status',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Статус задачи 0 - задача создана, клиент скинул деньги. 1 - задача подтверждена магазином. 2 - задача отклонена магазином',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент создания задачи',
                ],
                [
                    'name'        => 'txid_internal',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции внутренней по зачислению монет LETN на кошелек польщователя',
                ],
                [
                    'name'        => 'finished_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Момент закрытия задачи, то есть принятия решения о принятии задачи или отклонении',
                ],
                [
                    'name'        => 'amount',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Кол-во атомов которые были переведены в PZM',
                ],
                [
                    'name'        => 'modification_id',
                    'type'        => 'int',
                    'isRequired'  => false,
                    'description' => 'Идентификатор модификации currency_io_modification.id',
                ],
            ],
        ]) ?>

        <p>Если модийикаций нет то показывать стандартную форму. Если модификации есть то показывать вкладки и форму модификации</p>
        <p>в URL параметр id указывает на валюту, а если есть еще modification_id то значит указывается и модификация. Если не указана то используется первая</p>






        <p>Эта таблица появилась вследствии того что в форме предложения понадобилось разделить USD и USDT</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'offer_pay',
            'description' => 'Платежная система оплаты',
            'model'       => '\common\models\exchange\OfferPay',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта db.currency.id',
                ],
                [
                    'name'        => 'code',
                    'type'        => 'varchar(6)',
                    'isRequired'  => false,
                    'description' => 'Крипто адрес',
                ],
                [
                    'name'        => 'title',
                    'type'        => 'varchar(256)',
                    'isRequired'  => false,
                    'description' => 'Наименование',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Сопоставление полей с их валютами</h2>
        <pre>deal
price => db.currency_id
volume => currency_id
volume_vvb => currency_io

offer
price => db.currency_id
volume_start => db.currency_id
volume_finish => db.currency_id
price_m => currency_id
volume_io_start => currency_io
volume_io_finish => currency_io
volume_io_start_m => currency_io
volume_io_finish_m => currency_io
volume_io_finish_deal => currency_io</pre>

        <p>Для <code>deal</code> если разбирать то для <code>price</code> <code>volume</code> валюта <code>currency_id = db.currency.id</code> а ее отображение через <code>offer.offer_pay_id</code></p>

        <h2 class="page-header">Добавление новых монет</h2>
        <ul>
            <li>Проверить есть ли у нас такая монета, если нет то добавить</li>
            <li>Добавить в функцию автоматического генерирования кошельков у пользователя</li>
            <li>Сделать у всех кошельки</li>

            <li>Сделать проверку зачисления</li>
            <li>Добавить в статистику</li>
            <li>Сделать функцию валидации адреса (при зачислении проверяется)</li>
            <li>Учесть комиссию на вывод</li>
            <li>На странице редактирования кошельков</li>
            <li>Добавить поле в таблицу <code>user_wallet</code></li>
            <li>На странице отображения кошельков показать его <code>/cabinet-bills/index</code></li>
        </ul>

        <p>Эта таблица возникла из-за того чтобы в блокчейнах BTC, LTC, DASH кахжый раз не обращаться к API при поиске.</p>
        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'master_transaction',
            'description' => 'хранит информацию о транзакциях которые пришли из API',
            'model'       => '\common\models\MasterTransaction',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта db.currency.id',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'информацию о транзакциях которая пришла из API',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Адрес в блокчейне кошелька',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время записи в БД',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Добавление новой валюты расплаты за монеты (за что купить)</h2>
        <p>Проверить что она есть в <code>\common\models\exchange\OfferPay</code> <code>\common\models\avatar\Currency</code> <code>\common\models\piramida\Currency</code> <code>\common\models\avatar\CurrencyLink</code></p>

        <h2 class="page-header">Изменение разрядности валюты расплаты за монеты (за что купить)</h2>
        <p>Нужно изменить поле currency.decimals и currency.decimals_view а также изменить разрядность поля deal.price и offer.price</p>

        <h2 class="page-header">Тест кошельков</h2>

        <p>Проверяет реальный баланс всех кошельков с записанным в БД:</p>
        <pre>docker exec -it s-routes-php-fpm php yii wallet/test</pre>

        <p>Проверяет Хеши</p>
        <pre>docker exec -it s-routes-php-fpm php yii wallet/test2</pre>

        <p>Проверяет баланс в монетах и в операциях</p>
        <pre>docker exec -it s-routes-php-fpm php yii wallet/test3</pre>

        <h2 class="page-header">TRON</h2>
        <pre>$b58 = "TA62hPNp5h2WiZy8uTvP4GD4wkKg2MH1JS";
$result = \frontend\services\BaseConvert::base58_hex($b58);
$this->assertEquals($result, "41014A4F4F64BC971D667DF33A73E41A261D7380E79D269513");</pre>

        <pre>$b16_3 = "41014A4F4F64BC971D667DF33A73E41A261D7380E79D269513"; // выдает конвертация из адреса трона TA62hPNp5h2WiZy8uTvP4GD4wkKg2MH1JS
$B16_1 = "41014A4F4F64BC971D667DF33A73E41A261D7380E7"; // выдает API для адреса TA62hPNp5h2WiZy8uTvP4GD4wkKg2MH1JS</pre>

        <p>API выдает шестнадцатеричный адрес в 42 символа первых. Отстальные (8 символов) куда деваются и почему - не понятно. Поэтому я сравниваю только первые 42</p>
        <pre>if (StringHelper::startsWith(self::hexToBase58($task->account), $t['raw_data']['contract'][0]['parameter']['value']['owner_address'])) {
    //
}</pre>

        <h2 class="page-header">Ввод и вывод</h2>

        <?= \common\services\documentation\DbTable::widget([
            'name'        => 'currency_io_modification',
            'description' => 'хранит информацию данных для ввода и вывода монеты для блокчейна, пример USDT',
            'model'       => '\common\models\CurrencyIoModification',
            'columns'     => [
                [
                    'name'        => 'id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Идентификатор',
                ],
                [
                    'name'        => 'currency_id',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Валюта db.currency.id',
                ],
                [
                    'name'        => 'txid',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Идентификатор транзакции',
                ],
                [
                    'name'        => 'data',
                    'type'        => 'text',
                    'isRequired'  => true,
                    'description' => 'информацию о транзакциях которая пришла из API',
                ],
                [
                    'name'        => 'address',
                    'type'        => 'varchar(100)',
                    'isRequired'  => true,
                    'description' => 'Адрес в блокчейне кошелька',
                ],
                [
                    'name'        => 'created_at',
                    'type'        => 'int',
                    'isRequired'  => true,
                    'description' => 'Время записи в БД',
                ],
                [
                    'name'        => 'benance_fee',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'комиссия бинанс на вывод, в монетах',
                ],
                [
                    'name'        => 'comission_out',
                    'type'        => 'double',
                    'isRequired'  => true,
                    'description' => 'комиссия на вывод у нас, в монетах',
                ],
                [
                    'name'        => 'warning',
                    'type'        => 'varchar(200)',
                    'isRequired'  => true,
                    'description' => 'Предупреждение для формы ввода',
                ],
            ],
        ]) ?>

        <h2 class="page-header">Транзакции</h2>
        <p>Для того чтобы отличить типы транзакций существует тип транзакции в коментарии. В коментарии указывается в третьем элементе массива. Параметр называется <code>type_id</code></p>
        <p>Заявка на вывод <code>type_id</code> = 1</p>


        <h2 class="page-header">Если нет API для монеты</h2>
        <p>Если нет API для монеты то делаем ввод только по транзакции. За это отвечает флаг <code>db.currency.is_api</code></p>

    </div>
</div>
