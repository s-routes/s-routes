<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Доставка серебра';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <h2 class="page-header">Сущности</h2>
        <p><code>\common\models\shop\Product</code>         - <code>gs_unions_shop_product</code>                   - продукт</p>
        <p><code>\common\models\shop\ProductImage</code>    - <code>gs_unions_shop_product_images</code>            - Картинка продукта</p>
        <p><code>\common\models\shop\ProductVideo</code>    - <code>gs_unions_shop_product_video</code>             - Видео продукта</p>
        <p><code>\common\models\shop\ProductKorobka</code>  - <code>gs_unions_shop_product_korobka</code>           - Коробка для продукта</p>
        <p><code>\common\models\shop\Request</code>         - <code>gs_users_shop_requests</code>                   - Заказ</p>
        <p><code>\common\models\shop\DeliveryItem</code>    - <code>gs_unions_shop_dostavka</code>                  - Тип доставки</p>
        <p><code>\common\models\shop\RequestProduct</code>  - <code>gs_users_shop_requests_products</code>          - Товар в заказе</p>
        <p><code>\common\models\shop\DeliveryItem</code>    - <code>gs_unions_shop_dostavka</code>                  - Доставка</p>
        <p><code>\common\models\shop\RequestAdd</code>      - <code>gs_users_shop_requests_add</code>               - Доплата</p>


        <h2 class="page-header">Заказ</h2>

        <p>Заказываются монета и какая то для нее коробочка.</p>
        <p>Цена монеты в NEIRO.</p>
        <p>Цена коробочки в RUB. Может оплачиваться в RUB или NEIRO.</p>
        <p>В процессе заказа человек указывает тип доставки и адрес доставки.</p>
        <p>В процессе заказа человек указывает персональные данные заказчика.</p>
        <p>
            <a href="https://app.diagrams.net/#G1CGZuG-fu65Ry6FpTEn63Aw_Q4Erda6pH" target="_blank">
                <img src="/images/controller/admin-developer/silver-dostavka/2.png" class="thumbnail">
            </a>
        </p>

        <p>Данные заказа сохраняются в переменной сессии <code>shop-request2</code></p>




        <h2 class="page-header">Кнопка доплатить</h2>
        <p>Если я нажимаю доплатить, то значит добавляется заявка на доплату. По факту успеха заявки на доплату выводится список заявок на доплату в заказе.</p>

        <p>По нажанию на кнопку человек перенаправляется на форму создания заявки на пополнение. Форма располагается по ссылке <code>/cabinet-shop-requests/add</code></p>
        <p>Пополнить можно через MARKET и RUB. Если RUB то создается счет и его оплата. Сделаю пока MARKET.</p>


        <hr style="margin-top: 200px;">
    </div>
</div>


