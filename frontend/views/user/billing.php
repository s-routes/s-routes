<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $billing \common\models\avatar\UserBill */

$this->title = $billing->name;

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                <?= $this->title ?>
            </h1>
            <p>
                <code><?= $billing->address ?></code>
            </p>
            <p>
                <?php if (is_null($billing->user_id)) { ?>
                    Никому не принадлежит
                <?php } else { ?>
                    <?php $user = \common\models\UserAvatar::findOne($billing->user_id) ?>
                    Принадлежит: <a href="/user/<?= $billing->user_id ?>"><?= $user->getName2() ?>
                <?php }?>
            </p>
        </div>
    </div>
</div>


