<?php
use common\models\exchange\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user \common\models\UserAvatar */

$this->title = $user->getName2();
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">
                <?= $this->title ?>
            </h1>
            <p class="text-center">
                <img src="<?= $user->getAvatar() ?>" width="300" class="img-circle">
            </p>

            <?php
            $count = count(Deal::find()
                ->innerJoin(['offer', 'offer.id = deal.offer_id'])
                ->where(['in', 'deal.status', [Deal::STATUS_MONEY_RECEIVED, Deal::STATUS_CLOSE, Deal::STATUS_AUDIT_FINISH]])
                ->andWhere([
                    'or',
                    ['offer.user_id' => $user->id],
                    ['deal.user_id' => $user->id],
                ])
                ->select([
                    'deal.id'
                ])
                ->asArray()
                ->all());

            ?>
            <p>Кол-во сделок: <?= $count ?></p>
            <p>Кол-во оценок: <?= \common\models\exchange\Assessment::find()->where(['user_id' => $user->id])->count() ?></p>
            <p>Кол-во -1: <?= \common\models\exchange\Assessment::find()->where(['user_id' => $user->id])->andWhere(['value' => -1])->count() ?></p>
            <p>Кол-во 0: <?= \common\models\exchange\Assessment::find()->where(['user_id' => $user->id])->andWhere(['value' => 0])->count() ?></p>
            <p>Кол-во 1: <?= \common\models\exchange\Assessment::find()->where(['user_id' => $user->id])->andWhere(['value' => 1])->count() ?></p>
        </div>
    </div>
</div>


