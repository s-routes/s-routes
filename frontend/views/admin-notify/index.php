<?php
use common\models\avatar\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\AdminNotify */

$this->title = 'Возможность админу вывешивать и убирать транспарант предупреждения для клиентов';


?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin/index';
    }).modal();
}
JS
,
        ]);
        ?>

        <?= $form->field($model, 'start_date')->widget('\common\widgets\DatePicker\DatePicker')  ?>
        <?= $form->field($model, 'start_time')->hint('чч:мм')  ?>
        <?= $form->field($model, 'finish_date')->widget('\common\widgets\DatePicker\DatePicker') ?>
        <?= $form->field($model, 'finish_time')->hint('чч:мм') ?>
        <?= $form->field($model, 'text')->textarea(['rows' => 5])   ?>
        <hr>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end([]); ?>


    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>