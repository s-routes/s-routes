<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Stable Coins & Service Coins';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\ConvertOperation::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => ['defaultOrder' => ['id' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        try {
                            $userBill = \common\models\avatar\UserBill::findOne(['address' => $item['wallet_from_id']]);
                        } catch (Exception $e) {
                            return '';
                        }
                        $user = \common\models\UserAvatar::findOne($userBill->user_id);
                        $i = $user->getAvatar();

                        return Html::img($i, [
                            'class'  => "img-circle",
                            'data'   => ['toggle' => 'tooltip'],
                            'title'  => $user->getName2(),
                            'width'  => 40,
                            'height' => 40,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],
                [
                    'header'         => 'amount',
                    'headerOptions'  => ['class' => 'text-right',],
                    'contentOptions' => ['class' => 'text-right',],
                    'content'        => function ($item) {
                        $cInt = \common\models\piramida\Currency::findOne($item['from_currency_id']);
                        if (is_null($cInt)) return '';
                        $v = bcdiv($item['amount'], bcpow(10, $cInt->decimals), $cInt->decimals_view);

                        return $v;
                    },
                ],
                [
                    'header'  => 'from',
                    'content' => function ($item) {
                        $cInt = \common\models\piramida\Currency::findOne($item['from_currency_id']);
                        if (is_null($cInt)) return '';
                        $bill = \common\models\avatar\UserBill::findOne(['address' => $item['wallet_from_id']]);

                        return Html::tag('span',
                            Html::a($cInt->code, ['cabinet-wallet/item', 'id' => $bill->id], ['data'  => ['pjax' => 0]]),
                            [
                                'class' => "label label-info",
                                'data'  => [
                                    'toggle' => 'tooltip',
                                    'pjax' => 0
                                ],
                                'title' => $item['wallet_from_id'],
                            ]
                        );
                    },
                ],
                [
                    'header'         => 'amount_to',
                    'headerOptions'  => ['class' => 'text-right',],
                    'contentOptions' => ['class' => 'text-right',],
                    'content'        => function ($item) {
                        $cInt = \common\models\piramida\Currency::findOne($item['to_currency_id']);
                        if (is_null($cInt)) return '';
                        $v = bcdiv($item['amount_to'], bcpow(10, $cInt->decimals), $cInt->decimals_view);

                        return $v;
                    },
                ],
                [
                    'header'  => 'to',
                    'content' => function ($item) {
                        $cInt = \common\models\piramida\Currency::findOne($item['to_currency_id']);
                        if (is_null($cInt)) return '';
                        $bill = \common\models\avatar\UserBill::findOne(['address' => $item['wallet_to_id']]);

                        return Html::tag('span',
                            Html::a($cInt->code, ['cabinet-wallet/item', 'id' => $bill->id], ['data'  => ['pjax' => 0]]),
                            [
                                'class' => "label label-info",
                                'data'  => [
                                    'toggle' => 'tooltip',
                                    'pjax' => 0
                                ],
                                'title' => $item['wallet_to_id'],
                            ]
                        );
                    },
                ],
                'tx_from_id',
                'tx_to_id',
                [
                    'header'         => 'kurs',
                    'headerOptions'  => ['class' => 'text-right',],
                    'contentOptions' => ['class' => 'text-right',],
                    'content'        => function ($item) {
                        if (\cs\Application::isEmpty($item['kurs'])) return '';
                        return $item['kurs'] / 1000000;
                    },
                ],
                'to_price_usd',
                'from_price_usd',
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>