<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Alternative Coins';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\BinanceOrder::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort'       => ['defaultOrder' => ['id' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns' => [
                'id',
                [
                    'attribute' => 'binance_id',
                    'content' => function($item) {
                        return Html::tag('code', $item['binance_id']);
                    }
                ],
               [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $user = \common\models\UserAvatar::findOne($item['user_id']);
                        $i = $user->getAvatar();

                        return Html::img($i, [
                            'class'  => "img-circle",
                            'data'   => ['toggle' => 'tooltip'],
                            'title'  => $user->getName2(),
                            'width'  => 40,
                            'height' => 40,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
               ],

               [
                    'header'  => 'Из',
                    'content' => function ($item) {
                        try {
                            $bill = \common\models\avatar\UserBill::findOne($item['billing_id']);
                            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($bill['currency']);
                            return Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($item['amount'], $cInt), $cInt->decimals);
                        } catch (Exception $e) {
                            return '';
                        }

                    },
               ],
               [
                    'header'  => '',
                    'content' => function ($item) {
                        try {
                            $bill = \common\models\avatar\UserBill::findOne($item['billing_id']);
                            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($bill['currency']);

                            return Html::tag('span',
                                Html::a($cInt->code, ['cabinet-wallet/item', 'id' => $bill->id], ['data'  => ['pjax' => 0]]),
                                [
                                    'class' => "label label-info",
                                    'data'  => [
                                        'pjax' => 0
                                    ],
                                ]
                            );
                        } catch (Exception $e) {
                            return '';
                        }
                    },
                ],
                [
                    'header'  => 'В',
                    'content' => function ($item) {
                        try {
                            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($item['currency_to']);
                            return Yii::$app->formatter->asDecimal(
                                    \common\models\piramida\Currency::getValueFromAtom($item['amount_to'], $cInt),
                                    $cInt->decimals
                            );
                        } catch (Exception $e) {
                            return '';
                        }

                    },
               ],
               [
                    'header'  => '',
                    'content' => function ($item) {
                        try {
                            $cInt = \common\models\piramida\Currency::initFromCurrencyExt($item['currency_to']);
                            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cInt->id, $item['user_id']);
                            $bill = $data['billing'];
                            return Html::tag('span',
                                Html::a($cInt->code, ['cabinet-wallet/item', 'id' => $bill->id], ['data'  => ['pjax' => 0]]),
                                [
                                    'class' => "label label-info",
                                    'data'  => [
                                        'pjax' => 0
                                    ],
                                ]
                            );
                        } catch (Exception $e) {
                            return '';
                        }

                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>