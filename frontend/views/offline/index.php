<?php
use common\widgets\FileUpload3\FileUpload;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Ведутся технические работы';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
        <p class="lead text-center">Внимание!<br>Производится обновление программного обеспечения.<br>В этот период необходимо воздержаться от работы с ресурсом.</p>
    </div>
</div>


