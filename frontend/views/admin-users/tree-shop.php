<?php

/** @var $this \yii\web\View  */
/** @var $user \common\models\UserAvatar  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Структура TopMate';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= $this->render('@shop/views/cabinet/referal-structure-one', ['userObject' => $user]) ?>

    </div>
</div>