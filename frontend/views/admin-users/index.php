<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Все пользователи';

?>
<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
</style>
<div style="margin: 0px 20px 0px 20px;">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS


$('.buttonWalletList').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-users/permissions' + '?' + 'id' + '=' + $(this).data('id');
});

$('[data-toggle="tooltip"]').tooltip();

JS
        );
        ?>
        <?php
        $model = new \avatar\models\search\UserAvatar();
        $provider = $model->search(Yii::$app->request->get(), null, ['defaultOrder' => ['id' => SORT_DESC]]);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $provider,
            'filterModel'   => $model,
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'headerOptions' => [
                        'style' => 'width: 15px;',
                    ],
                    'content' => function (\common\models\UserAvatar $item) {
                        $i = $item->getAvatar();
                        $params = [
                            'class'  => "img-circle",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => ['margin-bottom' => '0px'],
                        ];
                        if (\yii\helpers\StringHelper::endsWith($i, '/images/iam.png')) {
                            $params['style']['opacity'] = '0.3';
                            $params['style']['border'] = '1px solid #888';
                        }
                        if (isset($params['style'])) {
                            if (is_array($params['style'])) {
                                $params['style'] = Html::cssStyleFromArray($params['style']);
                            }
                        }

                        return Html::img($i, $params);
                    }
                ],
                'email',
                [
                    'header'    => 'Email подтвержден?',
                    'filter'    => [
                        0 => 'Нет',
                        1 => 'Опубликован',
                    ],
                    'content'   => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'email_is_confirm', 0);
                        if ($v == 0) return Html::tag('span', 'Нет', ['class' => 'label label-default']);

                        return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    },
                    'attribute' => 'is_published',
                ],
                'name_last',
                'name_first',
                [
                    'header'  => 'Создан',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), [
                            'data'  => ['toggle' => 'tooltip'],
                            'title' => Yii::$app->formatter->asDatetime($v),
                            'class' => 'textDecorated'
                        ]);
                    }
                ],
                [
                    'header'  => 'Кошельки',
                    'content' => function ($item) {
                        return Html::a('Кошельки', ['admin-users/wallet-list',  'id' => $item['id']], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Редактировать',
                    'content' => function ($item) {
                        return Html::a('Редактировать', ['admin-users/edit',  'id' => $item['id']], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Роли',
                    'content' => function ($item) {
                        return Html::a('Роли', ['admin-users/permissions',  'id' => $item['id']], [
                            'class' => 'btn btn-default btn-xs buttonWalletList',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Карточка',
                    'content' => function ($item) {
                        return Html::a('Карточка', ['cabinet-users/view',  'id' => $item['id']], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Структура NEIRON',
                    'content' => function ($item) {
                        return Html::a('Структура', ['admin-users/tree',  'id' => $item['id']], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Структура TopMate',
                    'content' => function ($item) {
                        return Html::a('Структура', ['admin-users/tree-shop',  'id' => $item['id']], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ]
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>