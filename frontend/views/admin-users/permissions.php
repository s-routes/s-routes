<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $item \common\models\UserAvatar */

$this->title = $item->getName2() . '. Роли';


\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<div class="container">
    <div class="col-lg-12">
        <h1  class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <?php
        $rows = Yii::$app->authManager->getRolesByUser($item->id);
        Yii::$app->session->set('avatar-bank/views/admin-users/permissions.php', $rows);
        $rowsAll = Yii::$app->authManager->getRoles();


        $this->registerJs(<<<JS

$('input[type="checkbox"]').on('change', function(e) {
    var d1 = $(this);
    console.log(d1.is(':checked'));
    console.log($(this));
    ajaxJson({
        url: '/admin-users/permissions-ajax',
        data: {
            user_id: {$item->id},
            role: d1.data('id'),
            value: d1.is(':checked')
        },
        success: function (ret) {
            new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Успешно'
            }).show();
        }
    });
});

JS
)
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rowsAll,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'summary' => '',
            'columns' => [
                'name',
                'description',
                [
                    'header'  => '',
                    'content' => function ($item) {
                        $rows = Yii::$app->session->get('avatar-bank/views/admin-users/permissions.php');
                        $a = ArrayHelper::getColumn(array_values($rows), 'name');
                        $v = null;
                        if (in_array($item->name, $a)) {
                            $v = 1;
                        } else {
                            $v = 0;
                        }

                        return \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\validate\AdminUsersPermissions(['id' => $v]),
                            'attribute' => 'id',
                            'options' => ['data' => ['id' => $item->name]]
                        ]);
                    },
                ],
            ]
        ])  ?>
    </div>
</div>
