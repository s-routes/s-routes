<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $item \common\models\ConvertOperation */

$this->title = 'Конвертация #' . $item->id;

$wFrom = \common\models\piramida\Wallet::findOne($item['wallet_from_id']);
$amount = $item['amount'];
$wTo = \common\models\piramida\Wallet::findOne($item['wallet_to_id']);
$amount_to = $item['amount_to'];
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">
        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => [
                [
                    'label' => 'Создано',
                    'captionOptions' => ['style'],
                    'format' => 'datetime',
                    'value' => $item->created_at,
                ],
                [
                    'label' => 'ИЗ',
                    'format' => ['decimal', $wFrom->getCurrency()->decimals],
                    'value' => \common\models\piramida\Currency::getValueFromAtom($amount, $wFrom->getCurrency()),
                ],
                [
                    'label' => 'ИЗ',
                    'format' => 'html',
                    'value' => Html::tag('span', $wFrom->getCurrency()->code, ['class' => 'label label-info']),
                ],
                [
                    'label' => 'В',
                    'format' => ['decimal', $wTo->getCurrency()->decimals],
                    'value' => \common\models\piramida\Currency::getValueFromAtom($amount_to, $wTo->getCurrency()),
                ],
                [
                    'label' => 'В',
                    'format' => 'html',
                    'value' => Html::tag('span', $wTo->getCurrency()->code, ['class' => 'label label-info']),
                ],
                [
                    'label' => 'Курс',
                    'format' => ['decimal', 6],
                    'value' => $item['kurs'] / 1000000,
                ],
            ]
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>