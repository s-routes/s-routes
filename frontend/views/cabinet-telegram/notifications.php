<?php
use common\models\school\PotokUser3Link;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Subscribe */

$this->title = 'Настройка уведомлений';


$this->registerJs(<<<JS
$('.checkbox_m3').change(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/subscribe-set',
        data: {
            name: $(this).attr('id'),
            value: $(this).is(':checked') 
        },
        success: function(ret) {
            
        }
    });
});
JS
);

?>
<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php
        $rows = [];
        $s_rows = \common\models\UserSubscribe::find()->where(['user_id' => Yii::$app->user->id])->all();
        $actions = \common\models\UserSubscribeAction::find()->all();
        /** @var \common\models\UserSubscribeAction $action */
        foreach ($actions as $action) {
            $form = [
                'mail'     => false,
                'telegram' => false,
            ];
            /** @var \common\models\UserSubscribe $s */
            foreach ($s_rows as $s) {
                if ($s->action_id == $action->id) {
                    $form = [
                        'mail'     => ($s->is_mail == 1) ? true : false,
                        'telegram' => ($s->is_telegram == 1) ? true : false,
                    ];
                    break;
                }
            }
            $rows[] = [
                'id'   => $action->id,
                'name' => $action->name,
                'form' => $form,
            ];
        }

        $rows2 = [];
        foreach ($rows as $row) {
            foreach ($row['form'] as $name => $value) {
                if ($value) $rows2[] = $name . $row['id'];
            }
        }
        Yii::$app->session->set(__FILE__, $rows2);
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $rows,
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'summary'      => '',
            'columns'      => [
                'name:text:Наименование',
                [
                    'header'  => 'Почта',
                    'content' => function ($item) {

                        return \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\M3(['user_roles' => Yii::$app->session->get(__FILE__)]),
                            'attribute' => 'mail' . $item['id'],
                            'options' => ['class' => 'checkbox_m3'],
                        ]);
                    },
                ],
                [
                    'header'  => 'Telegram',
                    'content' => function ($item) {

                        return \common\widgets\CheckBox2\CheckBox::widget([
                            'model'     => new \avatar\models\M3(['user_roles' => Yii::$app->session->get(__FILE__)]),
                            'attribute' => 'telegram' . $item['id'],
                            'options' => ['class' => 'checkbox_m3'],
                        ]);
                    },
                ],
            ],
        ]) ?>


    </div>
    <div class="col-lg-4">
    </div>


</div>