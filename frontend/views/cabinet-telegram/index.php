<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = Yii::t('c.AmyzFVMQnh', 'Telegram Nommy');

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

$this->registerJs(<<<JS
$('.buttonPin').click(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/pin',
        success: function(ret) {
            window.location.reload();
        }
    });
});
$('.buttonUnPin').click(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/un-pin',
        success: function(ret) {
            window.location.reload();
        }
    });
})
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div  style="margin-top: 20px;">
            <?php if (!empty($user->telegram_chat_id)): ?>

                <p class="alert alert-info">
                    <?= \Yii::t('c.AmyzFVMQnh', 'Аккаунт') ?>: <?= (empty($user->telegram_username))? $user->telegram_chat_id . '(chat_id)' : $user->telegram_username ?>
                </p>

                <button class="btn btn-primary buttonUnPin"><?= \Yii::t('c.AmyzFVMQnh', 'Отсоединить') ?></button>

            <?php else: ?>

                <p><?= \Yii::t('c.AmyzFVMQnh', 'Краткая инструкция по активации нашей информационной помощницы Nommy.') ?>:</p>
                <ul>
                    <li><?= \Yii::t('c.AmyzFVMQnh', 'Зайдите в Telegram') ?> <a href="tg://resolve?domain=Neiro_n_Nommy_bot" target="_blank">@Neiro_n_Nommy_bot</a></li>
                    <li><?= \Yii::t('c.AmyzFVMQnh', 'Напишите "Привет"') ?></li>
                    <li><?= \Yii::t('c.AmyzFVMQnh', 'Ответьте словом «да» на вопрос, есть ли аккаунт на платформе Neiro-N') ?></li>
                    <li><?= \Yii::t('c.AmyzFVMQnh', 'Напишите ваш e-mail, который указывали при регистрации') ?></li>
                    <li><?= \Yii::t('c.AmyzFVMQnh', 'Далее есть два пути. Можно выбрать любой') ?></li>
                    <li>
                        <ul>
                            <li>~ <?= \Yii::t('c.AmyzFVMQnh', 'В своем ЛК вы обновляете страницу (F5) на вкладке «Telegram» , и нажимаете кнопку "Присоединить"') ?>.</li>
                            <li>~ <?= \Yii::t('c.AmyzFVMQnh', 'Зайдите на e-mail, перейдите по ссылке в письме, попав в свой ЛК, нажмите кнопку "Присоединить"') ?>.</li>
                        </ul>
                    </li>
                </ul>

                <?php if (\common\models\UserTelegramTemp::findOne(['email' => Yii::$app->user->identity->email])) { ?>
                    <button class="btn btn-primary buttonPin" style="margin-bottom: 30px;"><?= \Yii::t('c.AmyzFVMQnh', 'Присоединить') ?></button>
                <?php } ?>

            <?php endif; ?>

            <p style="margin-top: 30px;"><a href="<?= Url::to('/page/item?id=24', true)?>"><?= \Yii::t('c.AmyzFVMQnh', 'Подробная инструкция') ?></a></p>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



