<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $id int */

$this->title = 'Чат поддержки';

$user_id = $id;
$supportChat = \common\models\NeironSupport::findOne(['user_id' => $user_id]);
if (is_null($supportChat)) {
    $room = \common\models\ChatRoom::add(['last_message' => null]);
    $supportChat = \common\models\NeironSupport::add([
        'user_id' => $user_id,
        'room_id' => $room->id,
    ]);
}
$room_id = $supportChat->room_id;

\avatar\assets\Notify::register($this);



?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>


    <div class="row">

        <div class="col-lg-6" style="margin-top: 50px;">

            <?= $this->render('@shop/views/cabinet-exchange/chat', [
                'room_id'  => $room_id,
                'user_id'  => $user_id,
                'send_url' => '/admin-support/send',
            ]); ?>

        </div>
    </div>
</div>
