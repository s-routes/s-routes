<?php
/**
 * Created by PhpStorm.
 * User: Metatron
 * Date: 22.12.2018
 * Time: 5:44
 */
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */


$this->title = 'Проводник монет';
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <p><a href="/coin-explorer/currency">Список монет</a></p>
        <p><a href="/coin-explorer/transaction">Список всех транзакций</a></p>
        <p><a href="/coin-explorer/operation">Список всех операций</a></p>
        <p><a href="/coin-explorer/wallet">Список всех кошельков</a></p>
    </div>
</div>



