<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $Deal Deal */


$this->title = 'Арбитраж. Сделка #' . $Deal->id;

$user_id = Yii::$app->user->id;


$serverName = \avatar\assets\SocketIO\Asset::getHost();

$this->registerJs(<<<JS
var socket = io.connect('{$serverName}');
var STATUS = {$Deal->status};

socket.emit('new-user', {$Deal->id}, {$user_id});

JS
);


$offer = $Deal->getOffer();
$user = \common\models\UserAvatar::findOne($offer->user_id);
$method = \common\models\exchange\PayMethod::findOne($offer->pay_id);
?>
<?php $this->registerJs(<<<JS
$('.buttonFinishUser1').click(function(e) {
    $('#modal2fa1').modal();
});
$('.buttonFinishUser2').click(function(e) {
    $('#modal2fa2').modal();
});
$('.buttonRatingDown1').click(function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/cabinet-arbitrator/rating-down1',
            data: {id: $(this).data('id')},
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location = '/cabinet-arbitrator/index';
                }).modal();
            }
        });
    }
});
$('.buttonRatingDown2').click(function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/cabinet-arbitrator/rating-down2',
            data: {id: $(this).data('id')},
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location = '/cabinet-arbitrator/index';
                }).modal();
            }
        });
    }
});

JS
) ?>
<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-4">
            <h3 class="page-header text-center">
                Пользователь 1
            </h3>
            <?php $user1 = \common\models\UserAvatar::findOne($Deal->user_id); ?>
            <p class="text-center"><img src="<?= $user1->getAvatar() ?>" width="100" class="img-circle"></p>
            <p class="text-center"><?= $user1->email ?></p>
            <p class="text-center"><?= $user1->id ?></p>
            <p class="text-center"><?= $user1->name_first ?></p>
            <p class="text-center"><?= $user1->name_last ?></p>

            <?php
            // считаю рейтинги
            $r_pos = $user1->assessment_positive;
            $r_neg = $user1->assessment_negative;

            $html_r_pos = Html::tag('span', $r_pos, ['class' => 'label label-success', 'title' => 'Позитивных оценок', 'data' => ['toggle' => 'tooltip']]);
            $html_r_neg = Html::tag('span', $r_neg, ['class' => 'label label-danger', 'title' => 'Негативных оценок', 'data' => ['toggle' => 'tooltip']]);
            ?>
            <p><?= $html_r_neg . $html_r_pos ?></p>
            <p>Сделок всего в которых участвовал: <?= $user1->deals_count_all ?></p>
            <p>Сделок по всем своим предложениям: <?= $user1->deals_count_offer ?></p>

            <p>Среднее время сделки: <?php
                $c = \common\models\exchange\Deal::find()
                    ->innerJoin('offer', 'offer.id = deal.offer_id')
                    ->where(['offer.user_id' => $user1->id])
                    ->andWhere([
                        'deal.status' => [
                            \common\models\exchange\Deal::STATUS_CLOSE,
                            \common\models\exchange\Deal::STATUS_MONEY_RECEIVED,
                        ],
                    ])
                    ->andWhere(['not', ['deal.time_finish' => null]])
                    ->select('avg(deal.time_finish - deal.time_accepted)')
                    ->scalar();

                if (is_null($c)) {

                    echo '';

                } else {

                    $c = (int)$c;
                    $min = (int)($c / 60);
                    $sec = $c - ($min * 60);

                    echo $min . ' мин' . ' ' . $sec . ' сек';

                }
                ?></p>

            <p style="font-weight: bold">Сделка</p>
            <?php
            $CIO = \common\models\CurrencyIO::findOne($Deal->currency_io);
            $curInt = \common\models\piramida\Currency::findOne($CIO->currency_int_id);
            $offerPay = \common\models\exchange\OfferPay::findOne($Deal->getOffer()->offer_pay_id);
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $Deal->currency_id]);
            $curInt2 = \common\models\piramida\Currency::findOne($link->currency_int_id);
            ?>
            <?= \yii\widgets\DetailView::widget([
                'model'      => $Deal,
                'attributes' => [
                    'id',
                    [
                        'label' => 'Тип',
                        'value' => 'Пользователь ' . (($Deal->type_id == Deal::TYPE_ID_SELL) ? 'Продает' : 'Покупает')
                    ],
                    [
                        'label'  => 'Объем монет',
                        'format' => 'html',
                        'value'  => bcdiv($Deal->volume_vvb, bcpow(10, $curInt->decimals), $curInt->decimals_view) . ' ' . Html::tag('span', $curInt->code, ['class' => 'label label-info']),
                    ],
                    [
                        'label'  => 'Стоимость',
                        'format' => 'html',
                        'value'  => bcdiv($Deal->volume, bcpow(10, $curInt2->decimals), $curInt2->decimals_view) . ' ' . Html::tag('span', $offerPay->code, ['class' => 'label label-info']),
                    ],
                    'created_at:datetime:Создана',
                ]
            ]) ?>

        </div>
        <div class="col-lg-4">
            <h1 class="page-header text-center">
                Арбитраж. Сделка <a href="/cabinet-exchange/deal-action?id=<?= $Deal->id ?>">#<?= $Deal->id ?></a>
            </h1>
            <p class="text-center">
                <?php if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL) { ?>
                    <img src="/images/controller/cabinet-arbitrator/item/sell.png">
                <?php } ?>
                <?php if ($offer->type_id == \common\models\exchange\Offer::TYPE_ID_BUY) { ?>
                    <img src="/images/controller/cabinet-arbitrator/item/buy.png">
                <?php } ?>

                <style>

                    ul.countdown {
                        list-style: none;
                        margin: 5px 0;
                        padding: 0;
                        display: block;
                        text-align: center;
                    }
                    ul.countdown li {
                        display: inline-block;
                    }
                    ul.countdown li span {
                        font-size: 40px;
                        font-weight: 300;
                        line-height: 80px;
                    }
                    ul.countdown li.seperator {
                        font-size:30px;
                        line-height: 70px;
                        vertical-align: top;
                    }
                    ul.countdown li p {
                        color: #000;
                        font-size: 13px;
                        text-align: center;
                        margin-top: -10px;
                    }

                </style>
                <?php

                $this->registerJs(<<<JS
var d1 = new Date();
d1.setTime({$Deal->created_at}000 + 60 * 60 * 72 * 1000);
var y = d1.getFullYear();
var m = d1.getMonth() + 1;
if (m < 10) m = '0' + m;
var d = d1.getDate();
var h = d1.getHours();
if (h < 10) h = '0' + h;
var i = d1.getMinutes();
if (i < 10) i = '0' + i;
var s = d1.getSeconds();
if (s < 10) s = '0' + s;
$('.countdown').downCount({
    date: m + '/' + d + '/' + y + ' ' + h + ':' + i + ':' + s,
    offset: -(d1.getTimezoneOffset() / 60)
}, function () {
    $('.countdown').html($('<p>').html('Внимание рекомендуемое время на сделку истекло. Рейтинг не будет засчитан.'));
});
JS
                );
                \avatar\assets\CountDown\Asset::register($this);
                ?>
            <h3 style="margin-top: 30px;">Время, отведённое на сделку.</h3>
            <ul class="countdown" style="text-align: left;">
                <?php if (true) { ?>
                    <li>
                        <span class="days">00</span>
                        <p class="days_ref">часов</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="hours">00</span>
                        <p class="hours_ref">часов</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="minutes">00</span>
                        <p class="minutes_ref">минут</p>
                    </li>
                    <li class="seperator">:</li>
                    <li>
                        <span class="seconds">00</span>
                        <p class="seconds_ref">секунд</p>
                    </li>
                <?php } else { ?>
                    <p class="alert alert-warning">Сделка завершена.</p>
                <?php } ?>
            </ul>

        </div>
        <div class="col-lg-4">
            <h3 class="page-header text-center">
                Пользователь 2
            </h3>
            <?php $user2 = \common\models\UserAvatar::findOne($offer->user_id); ?>
            <p class="text-center"><img src="<?= $user2->getAvatar() ?>" width="100" class="img-circle"></p>
            <p class="text-center"><?= $user2->email ?></p>
            <p class="text-center"><?= $user2->id ?></p>
            <p class="text-center"><?= $user2->name_first ?></p>
            <p class="text-center"><?= $user2->name_last ?></p>

            <?php
            // считаю рейтинги
            $r_pos = $user2->assessment_positive;
            $r_neg = $user2->assessment_negative;

            $html_r_pos = Html::tag('span', $r_pos, ['class' => 'label label-success', 'title' => 'Позитивных оценок', 'data' => ['toggle' => 'tooltip']]);
            $html_r_neg = Html::tag('span', $r_neg, ['class' => 'label label-danger', 'title' => 'Негативных оценок', 'data' => ['toggle' => 'tooltip']]);
            ?>
            <p><?= $html_r_neg . $html_r_pos ?></p>
            <p>Сделок всего в которых участвовал: <?= $user2->deals_count_all ?></p>
            <p>Сделок по всем своим предложениям: <?= $user2->deals_count_offer ?></p>

            <p>Среднее время сделки: <?php
                $c = \common\models\exchange\Deal::find()
                    ->innerJoin('offer', 'offer.id = deal.offer_id')
                    ->where(['offer.user_id' => $user2->id])
                    ->andWhere([
                        'deal.status' => [
                            \common\models\exchange\Deal::STATUS_CLOSE,
                            \common\models\exchange\Deal::STATUS_MONEY_RECEIVED,
                        ],
                    ])
                    ->andWhere(['not', ['deal.time_finish' => null]])
                    ->select('avg(deal.time_finish - deal.time_accepted)')
                    ->scalar();

                if (is_null($c)) {

                    echo '';

                } else {

                    $c = (int)$c;
                    $min = (int)($c / 60);
                    $sec = $c - ($min * 60);

                    echo $min . ' мин' . ' ' . $sec . ' сек';

                }
                ?></p>

            <p style="font-weight: bold">Предложение</p>
            <?php
            $CIO2 = \common\models\CurrencyIO::findOne($offer->currency_io);
            $curInt3 = \common\models\piramida\Currency::findOne($CIO2->currency_int_id);
            $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $offer->currency_id]);
            $curInt4 = \common\models\piramida\Currency::findOne($link->currency_int_id);
            ?>
            <?= \yii\widgets\DetailView::widget([
                'model' => $offer,
                'attributes' => [
                    'id',
                    'created_at:datetime:Создана',
                    [
                        'label' => 'Тип',
                        'value' => 'Пользователь ' . (($offer->type_id == \common\models\exchange\Offer::TYPE_ID_SELL) ? 'Продает' : 'Покупает')
                    ],
                    [
                        'label'  => 'Объем монет от',
                        'format' => 'html',
                        'value'  => bcdiv($offer->volume_io_start, bcpow(10, $curInt3->decimals), $curInt3->decimals_view) . ' ' . Html::tag('span', $curInt3->code, ['class' => 'label label-info']),
                    ],
                    [
                        'label'  => 'Объем монет до',
                        'format' => 'html',
                        'value'  => bcdiv($offer->volume_io_finish, bcpow(10, $curInt3->decimals), $curInt3->decimals_view) . ' ' . Html::tag('span', $curInt3->code, ['class' => 'label label-info']),
                    ],
                    [
                        'label'  => 'Цена',
                        'format' => 'html',
                        'value'  => '1 ' . Html::tag('span', $curInt3->code, ['class' => 'label label-info']) . ' = ' . bcdiv($offer->price, bcpow(10, $curInt4->decimals), $curInt4->decimals_view) . ' ' . Html::tag('span', $offerPay->code, ['class' => 'label label-info']),
                    ],
                ],
            ]) ?>

        </div>
    </div>
    <div class="row" style="margin-bottom: 50px;">
        <div class="col-lg-4">

            <hr>
            <?php
            $user = $Deal->getUser();
            $to = [
                'id'     => $user->id,
                'name'   => $user->getName2(),
                'avatar' => $user->getAvatar(),
            ];
            ?>
            <p>
                <button class="btn btn-default buttonFinishUser1"
                        data-id="<?= $Deal->id ?>"
                        data-to="<?= \yii\helpers\Json::encode([(int)$Deal->user_id, (int)$Deal->getOffer()->user_id]) ?>"
                        data-to_id="<?= Html::encode(\yii\helpers\Json::encode($to)) ?>"
                        data-toggle="tooltip"
                        title="Принять решение в пользу Пользователя 1">Принять решение в пользу Пользователя 1
                </button>
                <?php
                $cio = \common\models\CurrencyIO::findOne($Deal->currency_io);
                $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id,$user->id);
                /** @var \common\models\piramida\Wallet $wallet */
                $wallet = $data['wallet'];
                $billing = $data['billing'];
                ?>
                <a href="/cabinet-wallet/item?id=<?= $billing->id ?>">
                    <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" alt="" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="<?= $cio->tab_title ?>">
                </a>
            </p>
            <p>
                <button class="btn btn-danger buttonRatingDown1"
                        data-id="<?= $Deal->id ?>"
                        data-toggle="tooltip"
                        title="Поставить отрицательный рейтинг для Пользователя 1">Рейтинг -100
                </button>
            </p>
        </div>
        <div class="col-lg-4">
            <p>
                <a href="/admin-wallets/view?id=<?= $cio->block_wallet ?>">
                    <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" alt="" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Кошелек для блокировки: <?= $cio->tab_title ?>">
                </a>

                <a href="/admin-wallets/view?id=<?= $cio->lost_wallet ?>">
                    <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" alt="" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Кошелек потерянных монет: <?= $cio->tab_title ?>">
                </a>
            </p>
        </div>
        <div class="col-lg-4">

            <hr>
            <?php
            $user = $Deal->getOffer()->getUser();
            $to = [
                'id'     => $user->id,
                'name'   => $user->getName2(),
                'avatar' => $user->getAvatar(),
            ];
            ?>
            <p>

                <button class="btn btn-default buttonFinishUser2"
                        data-id="<?= $Deal->id ?>"
                        data-to="<?= \yii\helpers\Json::encode([(int)$Deal->user_id, (int)$Deal->getOffer()->user_id]) ?>"
                        data-to_id="<?= Html::encode(\yii\helpers\Json::encode($to)) ?>"
                        data-toggle="tooltip"
                        title="Принять решение в пользу Пользователя 2">Принять решение в пользу Пользователя 2
                </button>
                <?php
                $user = $Deal->getOffer()->getUser();
                $cio = \common\models\CurrencyIO::findOne($Deal->currency_io);
                $data = UserBill::getInternalCurrencyWallet($cio->currency_int_id, $user->id);
                /** @var \common\models\piramida\Wallet $wallet */
                $wallet = $data['wallet'];
                $billing = $data['billing'];
                ?>
                <a href="/cabinet-wallet/item?id=<?= $billing->id ?>">
                    <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" alt="" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="<?= $cio->tab_title ?>">
                </a>
            </p>
            <p>
                <button class="btn btn-danger buttonRatingDown2"
                        data-id="<?= $Deal->id ?>"
                        data-toggle="tooltip"
                        title="Поставить отрицательный рейтинг для Пользователя 2">Рейтинг -100
                </button>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2">
            <div class="row row-p">

                <?php
                switch ($Deal->status) {
                    case Deal::STATUS_OPEN:
                        $StateStatus1 = 0;
                        $StateStatus2 = 0;
                        $StateStatus3 = 0;
                        $StateStatus4 = 0;
                        break;
                    case Deal::STATUS_BLOCK:
                        $StateStatus1 = 1;
                        $StateStatus2 = 0;
                        $StateStatus3 = 0;
                        $StateStatus4 = 0;
                        break;
                    case Deal::STATUS_MONEY_SENDED:
                        $StateStatus1 = 1;
                        $StateStatus2 = 1;
                        $StateStatus3 = 0;
                        $StateStatus4 = 0;
                        break;
                    case Deal::STATUS_MONEY_RECEIVED:
                        $StateStatus1 = 1;
                        $StateStatus2 = 1;
                        $StateStatus3 = 1;
                        $StateStatus4 = 0;
                        break;
                    case Deal::STATUS_CLOSE:
                        $StateStatus1 = 1;
                        $StateStatus2 = 1;
                        $StateStatus3 = 1;
                        $StateStatus4 = 1;
                        break;
                    case Deal::STATUS_HIDE:
                    case Deal::STATUS_CANCEL:
                        $StateStatus1 = 3;
                        $StateStatus2 = 3;
                        $StateStatus3 = 3;
                        $StateStatus4 = 3;
                        break;
                    case Deal::STATUS_AUDIT_WAIT:
                    case Deal::STATUS_AUDIT_ACCEPTED:
                    case Deal::STATUS_AUDIT_FINISH:
                        $StateStatus1 = 2;
                        $StateStatus2 = 2;
                        $StateStatus3 = 2;
                        $StateStatus4 = 2;
                        break;
                }
                if ($StateStatus1 == 0) $StateStatus1Color = 'default';
                if ($StateStatus1 == 1) $StateStatus1Color = 'success';
                if ($StateStatus1 == 2) $StateStatus1Color = 'warning';
                if ($StateStatus1 == 3) $StateStatus1Color = 'danger';

                if ($StateStatus2 == 0) $StateStatus2Color = 'default';
                if ($StateStatus2 == 1) $StateStatus2Color = 'success';
                if ($StateStatus2 == 2) $StateStatus2Color = 'warning';
                if ($StateStatus2 == 3) $StateStatus2Color = 'danger';

                if ($StateStatus3 == 0) $StateStatus3Color = 'default';
                if ($StateStatus3 == 1) $StateStatus3Color = 'success';
                if ($StateStatus3 == 2) $StateStatus3Color = 'warning';
                if ($StateStatus3 == 3) $StateStatus3Color = 'danger';

                if ($StateStatus4 == 0) $StateStatus4Color = 'default';
                if ($StateStatus4 == 1) $StateStatus4Color = 'success';
                if ($StateStatus4 == 2) $StateStatus4Color = 'warning';
                if ($StateStatus4 == 3) $StateStatus4Color = 'danger';

                ?>
                <style>
                    .c-iconStep {
                        font-size: 300%;
                    }
                </style>
                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step1" class="label label-<?= $StateStatus1Color ?>"><span class="fa fa-lock"></span></span></p>
                    <p class="text-center">Сделка подтверждена и cредства заблокированы</p>
                </div>

                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step2" class="label label-<?= $StateStatus2Color ?>"><span class="fa fa-money-bill-alt"></span></span></p>
                    <p class="text-center">Средства отправлены</p>
                </div>

                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step3" class="label label-<?= $StateStatus3Color ?>"><span class="fa fa-unlock"></span></span></p>
                    <p class="text-center">Средства получены</p>
                </div>

                <div class="col-lg-3">
                    <p class="c-iconStep text-center"><span data-target="step4" class="label label-<?= $StateStatus4Color ?>"><span class="glyphicon glyphicon-ok"></span></span></p>
                    <p class="text-center">Сделка завершена</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-6">

        <?= $this->render('@shop/views/cabinet-exchange/chat', [
            'room_id'  => $Deal->chat_room_id,
            'user_id'  => $user_id,
            'send_url' => '/cabinet-exchange-chat/send?id=' . $Deal->id,
        ]); ?>

    </div>

    <div class="col-lg-6">
        <h1 class="page-header text-center">
            Статус сделки и информация о предложении
        </h1>
        <p>Начало
            сделки: <?= Yii::$app->formatter->asDatetime($Deal->created_at) . ' ' . '(' . \cs\services\DatePeriod::back($Deal->created_at) . ')' ?></p>
        <p>Статус: <span class="label label-info"><?= Deal::$statusList[$Deal->status] ?></span>
        </p>


        <h3 class="page-header">
            Статусы
        </h3>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\exchange\DealStatus::find()
                    ->where(['deal_id' => $Deal->id])
                ,
                'sort'  => ['defaultOrder' => ['created_at' => SORT_ASC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary'      => '',
            'columns'      => [
                'id',
                'created_at:time:Время',
                [
                    'header'    => 'Комментарий',
                    'attribute' => 'status',
                    'content'   => function ($item) {
                        if (!\cs\Application::isEmpty($item['comment'])) {
                            return $item['comment'];
                        }
                        $d = Deal::findOne($item['deal_id']);
                        if ($d['user_id'] == Yii::$app->user->id) {
                            $type = \common\models\exchange\Assessment::SCREEN_DEAL;
                        } else {
                            $type = \common\models\exchange\Assessment::SCREEN_OFFER;
                        }

                        return Deal::$statusList2[$type][$item['status']];
                    },
                ],
            ],
        ]) ?>


    </div>
</div>

<div class="modal fade" id="modal2fa1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">2FA</h4>
            </div>
            <div class="modal-body">
                <p>Введите 2FA код</p>
                <?php $model = new \avatar\models\forms\CabinetArbitratorFinish1(['id' => $Deal->id]) ?>
                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model' => $model,
                    'formUrl' => '/cabinet-arbitrator/finish1',
                    'success' => <<<JS
function (ret) {
    var b1 = $('.buttonFinishUser1');
    var id = b1.data('id');
    var to = b1.data('to');
    var to_id = b1.data('to_id');
        
    socket.emit('arbitr-result-deal', id, {$user_id}, to, to_id);
    
    $('#modal2fa1').on('hidden.bs.modal', function(e) {
        $('#modalInfo').on('hidden.bs.modal', function() {
            window.location = '/cabinet-arbitrator/index';
        }).modal();
    }).modal('hide');
}
JS
                ]) ?>

                <?= Html::activeHiddenInput($model, 'id') ?>
                <?= $form->field($model, 'code') ?>

                <?php \iAvatar777\services\FormAjax\ActiveForm::end() ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal2fa2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">2FA</h4>
            </div>
            <div class="modal-body">
                <p>Введите 2FA код</p>
                <?php $model = new \avatar\models\forms\CabinetArbitratorFinish2(['id' => $Deal->id]) ?>
                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'formUrl' => '/cabinet-arbitrator/finish2',
                    'success' => <<<JS
function (ret) {
    var b1 = $('.buttonFinishUser2');
    var id = b1.data('id');
    var to = b1.data('to');
    var to_id = b1.data('to_id');
        
    socket.emit('arbitr-result-offer', id, {$user_id}, to, to_id);
    
    $('#modal2fa2').on('hidden.bs.modal', function(e) {
        $('#modalInfo').on('hidden.bs.modal', function() {
            window.location = '/cabinet-arbitrator/index';
        }).modal();
    }).modal('hide');
}
JS
    ,
                ]) ?>

                <?= Html::activeHiddenInput($model, 'id') ?>
                <?= $form->field($model, 'code') ?>

                <?php \iAvatar777\services\FormAjax\ActiveForm::end() ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>