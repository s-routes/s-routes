<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Deal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */


$this->title = 'Сделки для арбитра';

$user_id = Yii::$app->user->id;

\avatar\assets\Notify::register($this);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="row">
        <div class="col-lg-12">

            <?php /** @var \common\models\UserAvatar  $user  */ ?>
            <?php $user = Yii::$app->user->identity; ?>
            <?php if (\cs\Application::isEmpty($user->google_auth_code)) { ?>
                <p class="alert alert-danger">У вас не установлен код 2FA</p>
            <?php } else { ?>
                <?php \yii\widgets\Pjax::begin(); ?>
                <?php
                $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonAccept').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите принятие сделки')) {
        var button = $(this);
        var id = $(this).data('id');
        var to = $(this).data('to');
        ajaxJson({
            url: '/cabinet-arbitrator/accept' + '?' + 'id' + '=' + id,
            success: function (ret) {
                socket.emit('accept-arbitr', id, {$user_id}, to);
                window.location = '/cabinet-arbitrator/item?id=' + id;
            }, 
            errorScript: function (ret) {
                if (ret.id == 403) {
                    new Noty({
                        timeout: 1000,
                        theme: 'sunset',
                        type: 'success',
                        layout: 'bottomLeft',
                        text: ret.data
                    }).show();
                }
            }
        });
    }
});


$('.rowTable').click(function() {
});
JS
                );
                $model = new \avatar\models\search\Deal();
                $provider = $model->search(
                    Yii::$app->request->get(),
                    null,
                    ['defaultOrder' => ['id' => SORT_DESC]]
                );
                ?>
                <?= \yii\grid\GridView::widget([
                    'dataProvider' => $provider,
                    'filterModel'  => $model,
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover',
                    ],
                    'summary'      => '',
                    'rowOptions'   => function ($item) {
                        $data = [
                            'data'  => ['id' => $item['id']],
                            'role'  => 'button',
                            'class' => 'rowTable',
                        ];
                        return $data;
                    },
                    'columns'      => [
                        'id',
                        [
                            'header'  => 'Пользователь 1',
                            'content' => function ($item) {
                                $u = \common\models\UserAvatar::findOne($item['user_id']);
                                $size = 40;
                                $i = $u->getAvatar();
                                $params = [
                                    'class'  => "img-circle",
                                    'width'  => $size,
                                    'height' => $size,
                                    'style'  => ['margin-bottom' => '0px'],
                                ];
                                if (\yii\helpers\StringHelper::endsWith($i, '/images/iam.png')) {
                                    $params['style']['opacity'] = '0.3';
                                    $params['style']['border'] = '1px solid #888';
                                }
                                if (isset($params['style'])) {
                                    if (is_array($params['style'])) {
                                        $params['style'] = Html::cssStyleFromArray($params['style']);
                                    }
                                }

                                return Html::img($i, $params);
                            },
                        ],
                        [
                            'header' => 'Тип сделки',

                            'content' => function ($item) {
                                $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                                $type_id = $offer->type_id;
                                $text = '';
                                if ($type_id == \common\models\exchange\Offer::TYPE_ID_BUY) {
                                    $text = 'Покупка';
                                }
                                if ($type_id == \common\models\exchange\Offer::TYPE_ID_SELL) {
                                    $text = 'Продажа';
                                }

                                return $text;
                            },
                        ],
                        [
                            'header' => 'Тип сделки',

                            'content' => function ($item) {
                                $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                                $type_id = $offer->type_id;
                                $text = '';
                                if ($type_id == \common\models\exchange\Offer::TYPE_ID_BUY) {
                                    $text = Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-right']);
                                }
                                if ($type_id == \common\models\exchange\Offer::TYPE_ID_SELL) {
                                    $text = Html::tag('i', null, ['class' => 'glyphicon glyphicon-chevron-left']);
                                }

                                return $text;
                            },
                        ],
                        [
                            'header' => 'Пользователь 2',

                            'content' => function ($item) {
                                $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                                $u = \common\models\UserAvatar::findOne($offer['user_id']);
                                $size = 40;
                                $i = $u->getAvatar();
                                $params = [
                                    'class'  => "img-circle",
                                    'width'  => $size,
                                    'height' => $size,
                                    'style'  => ['margin-bottom' => '0px'],
                                ];
                                if (\yii\helpers\StringHelper::endsWith($i, '/images/iam.png')) {
                                    $params['style']['opacity'] = '0.3';
                                    $params['style']['border'] = '1px solid #888';
                                }
                                if (isset($params['style'])) {
                                    if (is_array($params['style'])) {
                                        $params['style'] = Html::cssStyleFromArray($params['style']);
                                    }
                                }

                                return Html::img($i, $params);
                            },
                        ],
                        [
                            'header'         => 'Цена',
                            'headerOptions'  => ['class' => 'text-right'],
                            'contentOptions' => ['class' => 'text-right', 'aria-label' => 'Цена'],
                            'content'        => function ($item) {
                                $currency = Currency::findOne($item['currency_id']);
                                $link = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $item['currency_id']]);
                                $curInt = \common\models\piramida\Currency::findOne($link->currency_int_id);
                                $price = $item['price'];
                                $pricePrint = bcdiv($price, bcpow(10, $curInt->decimals), $curInt->decimals_view);
                                $decimalsPrint = $curInt->decimals_view;

                                $c = Html::tag('span', $currency->code, ['class' => 'label label-info', 'style' => 'margin-left: 5px;']);

                                return Yii::$app->formatter->asDecimal($pricePrint, $decimalsPrint) . $c;
                            },
                        ],
                        [
                            'header'    => 'Статус',
                            'attribute' => 'status2',
                            'filter'    => [
                                1 => 'Только ожидающие',
                            ],
                            'content'   => function ($item) {
                                $v = \yii\helpers\ArrayHelper::getValue($item, 'status', 0);
                                if ($v == 0) return '';

                                return Html::tag('span', Deal::$statusList[$v], ['class' => 'label label-success']);
                            },
                        ],
                        [
                            'header'  => 'Создано',
                            'content' => function ($item) {
                                $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                                if ($v == 0) return '';

                                return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                            },
                        ],
                        [
                            'header'  => '',
                            'content' => function ($item) {
                                if ($item['status'] == Deal::STATUS_AUDIT_WAIT) {
                                    $to = [];
                                    $offer = \common\models\exchange\Offer::findOne($item['offer_id']);
                                    $to[] = (int)$offer['user_id'];
                                    $to[] = (int)$item['user_id'];

                                    return Html::button('Принять', [
                                        'class' => 'btn btn-info buttonAccept',
                                        'data'  => [
                                            'id' => $item['id'],
                                            'to' => \yii\helpers\Json::encode($to),
                                        ],
                                    ]);
                                }
                                if (in_array(
                                    $item['status'],
                                    [
                                        Deal::STATUS_AUDIT_ACCEPTED,
                                        Deal::STATUS_AUDIT_FINISH,
                                        Deal::STATUS_AUDIT_WAIT,
                                    ]
                                )) {
                                    if ($item['arbitrator_id'] == Yii::$app->user->id) {
                                        return Html::a('Перейти в сделку',
                                            ['cabinet-arbitrator/item', 'id' => $item['id']],
                                            [
                                                'class' => 'btn btn-info',
                                            ]
                                        );
                                    }
                                }
                            },
                        ],
                    ],
                ]) ?>


                <?php \yii\widgets\Pjax::end(); ?>
            <?php } ?>
        </div>
    </div>


</div>