<?php
use common\models\avatar\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\AdminMarketConvert */

$this->title = 'Таблица конвертации для LOT';

Yii::$app->session->set('frontend/views/admin-lot-convert/index.php', $model);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>


        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model' => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin/index';
    }).modal();
}
JS
,
        ]);
        Yii::$app->session->set('frontend/views/admin-lot-convert/index.php_form', $form);

        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => Currency::find()
                    ->where(['id' => [
                        Currency::TRX,
                        Currency::PZM,
                        Currency::ETH,
                        Currency::USDT,

                        Currency::BTC,
                        Currency::LTC,
                        Currency::BNB,
                        Currency::DASH,
                        Currency::NEIRO,
                        Currency::EGOLD,
                        Currency::DAI,
                        Currency::DOGE,
                    ]]),
                'sort' => ['defaultOrder' => ['sort_index' => SORT_ASC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = \yii\helpers\ArrayHelper::getValue($item, 'image', '');
                        if ($i == '') return '';

                        return Html::img($i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],

                'code',
                'title',
                [
                    'header'  => 'LOT > coin',
                    'content' => function ($item) {
                        $model = Yii::$app->session->get('frontend/views/admin-lot-convert/index.php');

                        /** @var \iAvatar777\services\FormAjax\ActiveForm $form */
                        $form = Yii::$app->session->get('frontend/views/admin-lot-convert/index.php_form');

                        return $form->field($model, 'name' . $item['id'] . '_out')->label('', ['class' => 'hide']);
                    }
                ],
                [
                    'header'  => 'coin > LOT',
                    'content' => function ($item) {
                        $model = Yii::$app->session->get('frontend/views/admin-lot-convert/index.php');

                        /** @var \iAvatar777\services\FormAjax\ActiveForm $form */
                        $form = Yii::$app->session->get('frontend/views/admin-lot-convert/index.php_form');

                        return $form->field($model, 'name' . $item['id'] . '_in')->label('', ['class' => 'hide']);
                    }
                ],
            ],
        ]) ?>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end([
        ]); ?>

        <p style="margin-top: 20px;">LOT/MARKET смотри в тут <a href="/admin-market-convert/index">/admin-market-convert/index</a></p>
        <p>Формулы расчета:</p>
        <pre>
Если я меняю PZM на MARKET
то формула расчета через доллар
PZM = {кол-во монет MARKET} * {цена MARKET в USD} / {цена PZM в USD}

Если вставлять сюда добавочный процент то куда?
Логично если я хочу себе в карман положить то я отдам просто меньше
значит допустим если я хочу оставить 2% у себя то формула такая
PZM = {кол-во монет MARKET} * {цена MARKET в USD} * (1 - 0,02) / {цена PZM в USD}

Если я хочу поменять PZM на MARKET то формула такая
MARKET = {кол-во монет PZM} * {цена PZM в USD} / {цена MARKET в USD}
c процентом аналогично
MARKET = {кол-во монет PZM} * {цена PZM в USD} * (1 - 0,02) / {цена MARKET в USD}
        </pre>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>