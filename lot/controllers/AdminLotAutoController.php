<?php

namespace lot\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use common\models\avatar\Currency;
use common\models\Lot;
use cs\Application;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminLotAutoController extends \lot\controllers\AdminBaseController
{

    public function actions()
    {
        return [
        ];
    }

    public function actionEdit()
    {
        $model = new \lot\models\forms\AdminLotAuto();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

}
