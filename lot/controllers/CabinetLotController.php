<?php

namespace lot\controllers;

use avatar\controllers\actions\DefaultAjax;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\CompanyCustomizeItem;
use common\models\Lot;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class CabinetLotController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'buy-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\validate\BuyAjax',
            ],
            'send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\validate\CabinetSupportChatSend',
            ],
        ];
    }

    /**
     */
    public function actionBuy($id)
    {
        $lot = Lot::findOne($id);

        return $this->render(['item' => $lot]);
    }

    /**
     */
    public function actionPlay($id)
    {
        $lot = Lot::findOne($id);
        if (!Bilet::find()->where(['lot_id' => $lot->id, 'user_id' => Yii::$app->user->id])->exists()) {
            throw new ForbiddenHttpException('Вы не купили билет на данную лотерею');
        }

        return $this->render(['item' => $lot]);
    }

    /**
     */
    public function actionPlay2($id)
    {
        $lot = Lot::findOne($id);
        if (!Bilet::find()->where(['lot_id' => $lot->id, 'user_id' => Yii::$app->user->id])->exists()) {
            throw new ForbiddenHttpException('Вы не купили билет на данную лотерею');
        }

        return $this->render(['item' => $lot]);
    }

    /**
     */
    public function actionUsers($id)
    {
        $lot = Lot::findOne($id);
        if (!Bilet::find()->where(['lot_id' => $lot->id, 'user_id' => Yii::$app->user->id])->exists()) {
            throw new ForbiddenHttpException('Вы не купили билет на данную лотерею');
        }

        return $this->render(['item' => $lot]);
    }

    /**
     */
    public function actionUsersGet()
    {
        $id = Yii::$app->request->post('id');
        $lot = Lot::findOne($id);
        if (!Bilet::find()->where(['lot_id' => $lot->id, 'user_id' => Yii::$app->user->id])->exists()) {
            throw new ForbiddenHttpException('Вы не купили билет на данную лотерею');
        }

        return self::jsonSuccess(['count' => Bilet::find()->where(['lot_id' => $lot->id])->count()]);
    }

    /**
     */
    public function actionBuyMany($id)
    {
        $lot = Lot::findOne($id);
        $model = new \lot\models\validate\CabinetLotBuyMany(['lot' => $lot]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {

                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'item' => $lot,
        ]);
    }

}
