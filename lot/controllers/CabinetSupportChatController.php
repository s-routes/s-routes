<?php

namespace lot\controllers;

use avatar\controllers\actions\DefaultAjax;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\chat\Message;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestAddress;
use common\models\investment\IcoRequestSuccess;
use common\models\investment\Project;
use common\models\investment\ProjectIco;
use common\models\MapBankomat;
use common\models\PaySystemConfig;
use common\models\PaySystemListItem;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetSupportChatController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\avatar\models\validate\CabinetSupportChatSend',
            ],
        ];
    }
}
