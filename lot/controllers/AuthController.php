<?php

namespace lot\controllers;

use avatar\base\BaseController;
use avatar\controllers\actions\DefaultAjax;
use avatar\models\UserLogin;
use avatar\models\UserRecover;
use avatar\models\UserRegistration;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\piramida\Currency;
use common\models\User;
use common\models\UserAvatar;
use common\models\UserDevice;
use common\models\UserTelegramShopTemp;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\BadRequestHttpException;
use yii\web\Request;
use yii\web\Response;
use yii\widgets\ActiveForm;
use cs\web\Exception;

class AuthController extends BaseController
{
    public $timeOut = 31536000;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'registration-send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\AuthRegistrationSend',
            ],
        ];
    }

    /**  */
    public function actionSecondFaReset()
    {
        $model = new \lot\models\validate\AuthSecondFaReset();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**  */
    public function actionSecondFaResetStep2($code)
    {
        $rows = Yii::$app->session->get('SECOND-FA-RESET-LOT');
        if ($rows === false) {
            throw new Exception('Время сессии вышло');
        } else {
            $id = $this->findCode($rows, $code);
            if (is_null($id)) {
                throw new Exception('Код не найден или не верный');
            }
            $user = UserAvatar::findOne($id);
        }

        $model = new \lot\models\validate\AuthSecondFaResetStep2(['user' => $user]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess();
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    private function findCode($rows, $code)
    {
        foreach ($rows as $r)  {
            if ($r['code'] == $code) return $r['id'];
        }

        return null;
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['cabinet/index']);
        }

        $model = new \avatar\models\forms\Login();
        $model->load(Yii::$app->request->post()) && $model->validate();
//        VarDumper::dump($model);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if (!empty($user->lot_google_auth_code)) {
                Yii::$app->session->set('userLogin', $user->id);
            }
            $action = 'login';
            if ($user->is_2step_login == 0) {
                $action = 'login';
            } else {
                if ($user->is_2step_login == 1) {
                    if (Security::checkDeviceAfterLogin($model->getUser())) {
                        $action = 'login';
                    } else {
                        $action = 'confirm-device';
                    }
                } else if ($user->is_2step_login == 2) {
                    $action = 'confirm-device';
                }
            }

            if ($action == 'login') {
                if (!empty($user->lot_google_auth_code)) {
                    return $this->redirect(['auth/google-confirm']);
                }
                $model->login($this->timeOut);
                UserLogin::add();

                if (Yii::$app->getUser()->getReturnUrl() == '/') return $this->redirect(['cabinet/index']);

                return $this->goBack();
            }
            if ($action == 'confirm-device') {
                $this->sendVerifyCode($user);
                return $this->redirect(['auth/device-confirm']);
            }
        } else {
            return $this->render([
                'model' => $model,
            ]);
        }
    }

    /**
     * Генерирует код, высылает его
     *
     * @param \common\models\UserAvatar $user
     *
     */
    private function sendVerifyCode($user)
    {
        $code = substr(str_shuffle('0123456789012345678901234567890123456789012345678901234567890123456789'), 0, 8);
        Yii::$app->session->set('enter', [
            'code'    => $code,
            'user_id' => $user->id,
        ]);
        Yii::trace('$code=' . $code, 'avatar\controllers\AuthController::sendVerifyCode()');

        Application::mail($user->email, 'Подтверждение устройства', 'device-confirm', [
            'code'    => $code,
            'user_id' => $user->id,
        ]);
    }

    /**
     * Подтверждение нового устройства
     *
     * @return string
     */
    public function actionDeviceConfirm()
    {
        $model = new \avatar\models\forms\DeviceConfirm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            if ($user->is_2step_login == UserAvatar::IS_2STEP_LOGIN_DEVICE) {
                UserDevice::add([
                    'user_id' => $user->id,
                    'name'    => Yii::$app->request->userAgent,
                ]);
            }
            if (!empty($user->google_auth_code)) {
                Yii::$app->session->set('userLogin', $user->id);
                return $this->redirect(['auth/google-confirm']);
            }
            Yii::$app->user->login($user);
            UserLogin::add();

            if (Yii::$app->getUser()->getReturnUrl() == '/') return $this->redirect(['cabinet/index']);
            return $this->goBack();
        } else {
            return $this->render('device-confirm', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Подтверждение google code
     *
     * @return string
     */
    public function actionGoogleConfirm()
    {
        $model = new \lot\models\forms\GoogleConfirm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = $model->getUser();
            Yii::$app->user->login($user);
            UserLogin::add();

            if (Yii::$app->getUser()->getReturnUrl() == '/') return $this->redirect(['cabinet/index']);

            return $this->goBack();
        } else {
            return $this->render('google-confirm', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Провряет логин
     * REQUEST:
     * + email - string - логин - почта или телефон начинающийся на +7
     * + password - string - пароль
     *
     * @return string
     * errors
     * 101, 'Пользователь не найден'
     * 102, 'Пользователь не активирован'
     * 103, 'Пользователь заблокирован'
     * 104, 'Не верный пароль'
     * 105, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля'
     * 106, 'Email может содержать только буквы латинского алфавита'
     * 107, 'Нет обязательного параметра <name>'
     * 108, 'В телефоне должно быть 12 символов и никаких спецсимволов'
     * 109, 'В телефоне должны быть только цифры'
     *
     * @throws \yii\web\BadRequestHttpException
     */
    public function actionLoginAjax()
    {
        $email = strtolower(self::getParam('email'));
        $email = trim($email);
        $password = self::getParam('password');
        if (is_null($email)) {
            return self::jsonErrorId(107, 'Нет обязательного параметра mail');
        }
        if (is_null($password)) {
            return self::jsonErrorId(107, 'Нет обязательного параметра password');
        }

        if (StringHelper::startsWith($email, '+7')) {
            // Это телефон
            // +79252374501
            if (strlen($email) != 12) {
                return self::jsonErrorId(108, 'В телефоне должно быть 12 символов и никаких спецсимволов');
            }
            if (!$this->validatePhone(substr($email, 1))) {
                return self::jsonErrorId(109, 'В телефоне должны быть только цифры');
            }
            $user = UserAvatar::findOne(['phone' => substr($email, 1)]);
        } else {
            if (strpos($email, '@')) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    return self::jsonErrorId(106, 'Не верный формат email');
                }
            } else {
                return self::jsonErrorId(107, 'Email должен содержать @');
            }
            /** @var \common\models\UserAvatar $user */
            try {
                $user = UserAvatar::findOne(['email' => $email]);
            } catch (\Exception $e) {
                return self::jsonErrorId(101, 'Пользователь не найден');
            }
            if ($user->email_is_confirm != 1) {
                return self::jsonErrorId(102, 'Пользователь не активирован');
            }
        }

        if ($user->mark_deleted == 1) {
            return self::jsonErrorId(103, 'Пользователь заблокирован');
        }
        if ($user->password == '') {
            return self::jsonErrorId(105, 'Вы  не завели себе пароль для аккаунта. Зайдите в восстановление пароля');
        }
        if (!$user->validatePassword($password)) {
            return self::jsonErrorId(104, 'Не верный пароль');
        }
        if ($user->is_2step_login == 0) {
            if (!empty($user->google_auth_code)) {
                Yii::$app->session->set('userLogin', $user->id);
                return self::jsonSuccess([
                    'code'     => 201,
                    'redirect' => Url::to(['auth/google-confirm']),
                ]);
            }
            Yii::$app->user->login($user, $this->timeOut);
            UserLogin::add();

            return self::jsonSuccess(['code' => 200]);
        } else {
            if ($user->is_2step_login == 1) {
                if (Security::checkDeviceAfterLogin($user)) {
                    if (!empty($user->google_auth_code)) {
                        Yii::$app->session->set('userLogin', $user->id);
                        return self::jsonSuccess([
                            'code'     => 201,
                            'redirect' => Url::to(['auth/google-confirm']),
                        ]);
                    }
                    Yii::$app->user->login($user);
                    UserLogin::add();

                    return self::jsonSuccess(['code' => 200]);
                } else {
                    $this->sendVerifyCode($user);
                    return self::jsonSuccess([
                        'code'     => 201,
                        'redirect' => Url::to(['auth/device-confirm']),
                    ]);
                }
            } else if ($user->is_2step_login == 2) {
                $this->sendVerifyCode($user);
                return self::jsonSuccess([
                    'code'     => 201,
                    'redirect' => Url::to(['auth/device-confirm']),
                ]);
            }
        }
    }

    private function validatePhone($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'])) return false;
        }
        return true;
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionPasswordRecover()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception('Запрещено');
        }

        $model = new \avatar\models\forms\PasswordRecover();
        $model->setScenario('insert');

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            $model->setScenario('ajax');
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            \Yii::info(\yii\helpers\VarDumper::dumpAsString(Yii::$app->mailer), 'avatar\\1');
            $model->send();
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('password-recover', [
                'model' => $model,
            ]);
        }

    }

    /**
     * Активация восстановления пароля
     *
     * @param string $code
     *
     * @return string
     *
     * @throws Exception
     */
    public function actionPasswordRecoverActivate($code)
    {
        $row = UserRecover::findOne(['code' => $code]);
        if (is_null($row)) {
            throw new Exception('Не найден код активации');
        }
        $user = UserAvatar::findOne($row->parent_id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }

        $model = new \avatar\models\forms\PasswordNewRecover(['code' => $code]);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action($user);
            $row->delete();

            return $this->redirect(['auth/login']);
        }

        return $this->render('password-recover-activate', [
            'model' => $model,
            'user'  => $user,
        ]);
    }

    public function actionRegistration()
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        $model = new \lot\models\forms\Registration();

        if (Yii::$app->request->isAjax) {
            $model->setScenario('ajax');
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }
        $model->setScenario('insert');

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->register();
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    /**
     * @return string
     */
    public function actionRepareGoogleCode()
    {
        $model = new \avatar\models\forms\RepareGoogleCode();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
            Yii::$app->session->setFlash('form', 1);
        }

        return $this->render('repare-google-code', [
            'model' => $model,
        ]);

    }

    /**
     * Активация регистрации
     *
     * @param string $code
     *
     * @return Response
     * @throws Exception
     */
    public function actionRegistrationActivate($code)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        /** @var \avatar\models\UserRegistration $registration */
        $registration = UserRegistration::findOne(['code' => $code]);
        if (is_null($registration)) {
            throw new Exception('Срок ссылки истек или не верный код активации');
        }
        /** @var \common\models\UserAvatar $user */
        $user = UserAvatar::findOne($registration->parent_id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }

        try {
            // если уже есть счет привязанный к карте то не нужно создавать второй
            $billing = UserBill::findOne(['user_id' => $user->id]);
        } catch (\Exception $e) {

        }

        $user->activate();
        Yii::$app->user->login($user);
        $registration->delete();

        // Собзаю кошельки и формирую пакет покупок
        \avatar\controllers\AuthController::initUserAfterRegistration($user);

        return $this->redirect(['cabinet/index']);
    }

    /**
     * Активация регистрации
     *
     * @param string $code
     *
     * @return Response
     * @throws Exception
     */
    public function actionRegistrationOrderActivate($code)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        /** @var \avatar\models\UserRegistration $registration */
        $registration = UserRegistration::findOne(['code' => $code]);
        if (is_null($registration)) {
            throw new Exception('Срок ссылки истек или не верный код активации');
        }
        /** @var \common\models\UserAvatar $user */
        $user = UserAvatar::findOne($registration->parent_id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }

        try {
            // если уже есть счет привязанный к карте то не нужно создавать второй
            $billing = UserBill::findOne(['user_id' => $user->id]);
        } catch (\Exception $e) {

        }

        $user->activate();
        Yii::$app->user->login($user);
        $registration->delete();

        // Создаю для пользователя кошельки
        $all = [
            Currency::LEGAT,
            Currency::LETM,
            Currency::LETN,
            Currency::PZM,
            Currency::ETH,
            Currency::USDT,

            Currency::LTC,
            Currency::BNB,
            Currency::BTC,
            Currency::DASH,
            Currency::NEIRO,
            Currency::EGOLD,
            Currency::RUB,
        ];
        foreach ($all as $cid) {
            UserBill::getInternalCurrencyWallet($cid, $user->id);
        }

        return $this->redirect(['cabinet/index']);
    }


    /**
     * Активация кода для активации карты
     *
     * @param string $code
     *
     * @return Response
     * @throws Exception
     */
    public function actionQrEnterActivate($code)
    {
        if (\yii\helpers\ArrayHelper::getValue(Yii::$app->params, 'isTransfere', false) == true) {
            throw new Exception(Yii::$app->params['isTransfere_string']);
        }
        /** @var \avatar\models\UserRegistration $registration */
        $registration = \avatar\models\UserEnter::findOne(['code_registration' => $code]);
        if (is_null($registration)) {
            throw new Exception('Не верный код активации');
        }
        /** @var \common\models\UserAvatar $user */
        $user = UserAvatar::findOne($registration->id);
        if (is_null($user)) {
            throw new Exception('Пользователь не найден');
        }
        $user->activate();
        Yii::$app->user->login($user);
        $registration->delete();

        return $this->goHome();
    }

    /**
     * Присоединение телеграмма с регистрацией
     * REQUEST:
     * - hash
     *
     * @param string $hash
     *
     * @return string
     * @throws
     */
    public function actionRegistrationTelegram($hash)
    {
        $options = Yii::$app->params['telegramLot'];

        $modelConnect = $options['modelConnect'];
        /** @var \common\models\UserTelegramShopConnect $registration */
        $registration = $modelConnect::findOne(['hash' => $hash]);

        $model = new \lot\models\forms\SetPassword();
        if (Yii::$app->request->isAjax) {
            if ($model->load(Yii::$app->request->post())) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                Yii::$app->response->data = ActiveForm::validate($model);

                return Yii::$app->response;
            }
        } else {
            if (is_null($registration)) {
                throw new Exception('Не верный код активации');
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action($registration, $this->timeOut);

            return $this->redirect('/cabinet/index');
        }

        return $this->render(['model' => $model]);
    }

    /**
     * Присоединение телеграмма
     * REQUEST:
     * - hash
     *
     * @param string $hash
     *
     * @return Response
     * @throws
     */
    public function actionConfirmTelegram($hash)
    {
        $params = Yii::$app->params['telegramLot'];
        $modelConnect = $params['modelConnect'];
        $userField = $params['userField'];
        $userFieldAccount = $params['userFieldAccount'];
        $modelTemp = $params['modelTemp'];
        $component = $params['component'];
        $languageCategory = $params['languageCategory'];

        /** @var \common\models\UserTelegramShopConnect $registration */
        $registration = $modelConnect::findOne(['hash' => $hash]);
        if (is_null($registration)) {
            throw new Exception('Не верный код активации');
        }
        if (!is_null($registration->user_id)) {
            $user = UserAvatar::findOne($registration->user_id);
            if (is_null($user)) {
                throw new Exception('Не найден пользователь');
            }
            $user->$userField = $registration->chat_id;
            $user->$userFieldAccount = $registration->username;
            $user->save();
            Yii::$app->user->login($user, $this->timeOut);

            // удалаю \common\models\UserTelegramConnect
            $registration->delete();

            // удалаю \common\models\UserTelegramTemp
            $TelegramTemp = $modelTemp::findOne(['username' => $user->$userFieldAccount]);
            if (!is_null($TelegramTemp)) {
                $TelegramTemp->delete();
            }

            // Устанавливаю язык для бота
            $language = \avatar\controllers\TelegramController::getOptimalLanguage($user);

            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->$component;
            $text = Yii::t($languageCategory, $params['messages']['t']['actionConfirmTelegram'], [], $language);
            $telegram->sendMessage(['chat_id' => $user->$userField, 'text' => $text]);
        }

        return $this->redirect(['cabinet-telegram/index']);
    }

}
