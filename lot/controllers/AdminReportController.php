<?php

namespace lot\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;

class AdminReportController    extends AdminBaseController
{
    public function actionIndex()
    {
        $model = new \lot\models\forms\AdminReport();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->action();
        }

        return $this->render(['model' => $model]);
    }
}
