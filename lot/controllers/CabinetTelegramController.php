<?php

namespace lot\controllers;

use avatar\models\UserLogin;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\UserSubscribe;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\models\UserTelegramConnect;
use common\models\UserTelegramLotConnect;
use common\models\UserTelegramLotTemp;
use common\models\UserTelegramShopConnect;
use common\models\UserTelegramShopTemp;
use common\models\UserTelegramTemp;
use common\models\UserWallet;
use common\services\Security\AES;
use cs\Application;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;
use yii\web\Response;

class CabinetTelegramController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Присоединяет telegram_username и telegram_chat_id
     */
    public function actionPin()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        $temp = UserTelegramLotTemp::findOne(['email' => $user->email]);
        $connect = UserTelegramLotConnect::findOne(['username' => $temp->username]);
        if (!is_null($connect)) {
            $connect->delete();
        }
        $user->telegram_lot_username = $temp->username;
        $user->telegram_lot_chat_id = $temp->chat_id;
        $user->save();

        // Устанавливаю язык для бота
        $language = \avatar\controllers\TelegramController::getOptimalLanguage($user);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramLot;
        $telegram->sendMessage(['chat_id' => $user->telegram_lot_chat_id, 'text' => Yii::t('c.EJ5oxJAeD0', 'Вы молодец! У вас все получилось! Я буду на связи. До скорых встреч…', [], $language)]);
        $temp->delete();

        return self::jsonSuccess();
    }

    /**
     * Отсоединяет telegram_username и telegram_chat_id
     */
    public function actionUnPin()
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        // Устанавливаю язык для бота
        $language = \avatar\controllers\TelegramController::getOptimalLanguage($user);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramLot;
        $telegram->sendMessage(['chat_id' => $user->telegram_lot_chat_id, 'text' => Yii::t('c.EJ5oxJAeD0', 'Я отсоединяюсь! До скорых встреч!', [], $language)]);

        // удаляю прошлый статус разговора
        $connect = UserTelegramLotTemp::findOne(['chat_id' => $user->telegram_lot_chat_id]);
        if (!is_null($connect)) $connect->delete();

        // сбрасываю привязку у пользователя
        $user->telegram_lot_username = null;
        $user->telegram_lot_chat_id = null;
        $user->save();

        return self::jsonSuccess();
    }

    public function actionNotifications()
    {
        return $this->render();
    }


    /**
     * REQUEST:
     * - name - string (mail#,telegram#)
     * - value - bool
     *
     * @return string|Response
     */
    public function actionSubscribeSet()
    {
        $name = self::getParam('name');
        $value = self::getParam('value');
        $value = $value == 'false' ? false : true;
        $field = '';
        $name = substr($name,3);

        if (StringHelper::startsWith($name,'mail')) {
            $field = 'mail';
            $id = substr($name, strlen($field));
        } else {
            $field = 'telegram';
            $id = substr($name, strlen($field));
        }

        // таблица user_subscribe
        $userSubscribe = UserSubscribe::findOne(['user_id' => Yii::$app->user->id, 'action_id' => $id]);
        if (is_null($userSubscribe)) {
            UserSubscribe::add([
                'user_id'     => Yii::$app->user->id,
                'action_id'   => $id,
                'is_mail'     => ($field == 'mail') ? ($value ? 1 : 0) : 0,
                'is_telegram' => ($field == 'telegram') ? ($value ? 1 : 0) : 0,
            ]);
        } else {
            if ($field == 'mail') {
                $userSubscribe->is_mail = $value ? 1 : 0;
                $userSubscribe->save();
            }
            if ($field == 'telegram') {
                $userSubscribe->is_telegram = $value ? 1 : 0;
                $userSubscribe->save();
            }
        }

        return self::jsonSuccess();
    }

}
