<?php

namespace lot\controllers;

use avatar\controllers\actions\DefaultAjax;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\Lot;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class LotController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'is-start' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\validate\CabinetLotIsStart',
            ],
            'is-finish' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\validate\CabinetLotIsFinish',
            ],
        ];
    }

    /**
     */
    public function actionItem($id)
    {
        $lot = Lot::findOne($id);

        return $this->render(['item' => $lot]);
    }

    /**
     */
    public function actionView($id)
    {
        $lot = Lot::findOne($id);

        return $this->render(['item' => $lot]);
    }


}
