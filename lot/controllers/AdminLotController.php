<?php

namespace lot\controllers;

use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use common\models\avatar\Currency;
use common\models\Lot;
use cs\Application;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class AdminLotController extends \lot\controllers\AdminBaseController
{

    public function actions()
    {
        return [
            'delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormDelete',
                'model' => '\avatar\models\forms\LotDelete',
            ],

            // Уведомляет что пул участников сформирован
            'pool-start' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\LotPoolStart',
            ],

            // Указывает победителя
            'win' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\LotWin',
            ],

            'show' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\LotShow',
            ],

            'hide' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\LotHide',
            ],

            'cancel' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\LotCancel',
            ],

            'copy' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\LotCopy',
            ],

            // Выплатить приз в лотах
            'do-lot' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\lot\models\forms\DoLot',
            ],
        ];
    }

    public function actionAdd()
    {
        $model = new \avatar\models\forms\Lot();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $this->onAfterLoad($model) && $model->validate()) {
                $model->currency_id = Currency::RUB;
                $this->onBeforeUpdate($model);
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionWinner($id)
    {
        $model = \lot\models\validate\Winner::findOne($id);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function onAfterLoad($model)
    {
        $model->time_start = \DateTime::createFromFormat('Y-m-d H:i', $model->time_start);
        $model->time_finish = \DateTime::createFromFormat('Y-m-d H:i', $model->time_finish);
        $model->time_raffle = \DateTime::createFromFormat('Y-m-d H:i', $model->time_raffle);

        return true;
    }

    public function onAfterLoadDb($model)
    {

        if (!Application::isEmpty($model->time_start)) {
            $model->time_start = Yii::$app->formatter->asDatetime($model->time_start, 'php:Y-m-d H:i');
        }
        if (!Application::isEmpty($model->time_finish)) {
            $model->time_finish = Yii::$app->formatter->asDatetime($model->time_finish, 'php:Y-m-d H:i');
        }
        if (!Application::isEmpty($model->time_raffle)) {
            $model->time_raffle = Yii::$app->formatter->asDatetime($model->time_raffle, 'php:Y-m-d H:i');
        }

        return true;
    }

    public function onBeforeUpdate($model)
    {
        $model->time_start = $model->time_start->format('U');
        if ($model->time_finish !== false) {
            $model->time_finish = $model->time_finish->format('U');
        }
        if ($model->time_raffle !== false) {
            $model->time_raffle = $model->time_raffle->format('U');
        }

        return true;
    }

    /**
     *
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     * Редактирование лота
     *
     * @param int $id идентификатор лота
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionEdit($id)
    {
        $model = \avatar\models\forms\Lot::findOne($id);
        $this->onAfterLoadDb($model);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $this->onAfterLoad($model) && $model->validate()) {
                $this->onBeforeUpdate($model);
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Просмотр лота
     *
     * @param int $id идентификатор лота
     *
     * @return string|\yii\web\Response
     *
     * @throws \cs\web\Exception
     */
    public function actionView($id)
    {
        $model = Lot::findOne($id);

        return $this->render([
            'model' => $model,
        ]);
    }
}
