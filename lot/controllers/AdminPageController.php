<?php

namespace lot\controllers;

use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAdd;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;

class AdminPageController extends \lot\controllers\AdminBaseController
{
    public function actions()
    {
        return [
            'file-delete-ajax' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model' => '\avatar\models\validate\AdminPageFileDeleteAjax',
            ],
            'file-delete' => [
                'class' => '\iAvatar777\services\FormAjax\DefaultFormAdd',
                'model' => '\avatar\models\validate\AdminPageFileDeleteAjax',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionAdd()
    {
        $model = new \lot\models\forms\Page();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('form', $model->id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionEditLang($id, $lang)
    {
        $model = \lot\models\forms\PageLang::findOne(['parent_id' => $id, 'language' => $lang]);
        if (is_null($model)) {
            $model = new \lot\models\forms\PageLang(['parent_id' => $id, 'language' => $lang]);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->isNewRecord) {
                $model->insert();
            } else {
                $model->update();
            }
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionEdit($id)
    {
        $model = \lot\models\forms\Page::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('form', $id);
        }

        return $this->render([
            'model' => $model,
        ]);
    }


    public function actionDelete($id)
    {
        \lot\models\forms\Page::findOne($id)->delete();

        return self::jsonSuccess();
    }
}
