<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style type="text/css">
        .btn-primary {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
    </style>
</head>
<body>
    <?php $this->beginBody() ?>

    <table border="0" cellpadding="0" cellpadding="0" width="600" align="center" style="
    width: 100%; max-width: 600px;
">
        <tr>
            <td style="text-align: center;">
                <img src="<?= \yii\helpers\Url::to('/images/pdf/lovo-avr-04.png', true) ?>" width="200">
            </td>
        <tr>
        </tr>
            <td style="padding: 20px;">
                <?= $content ?>

                <hr>
                <p>Вы получили это письмо, потому что подписались на рассылку на сайте или зарегистрировались на сайте <a href="https://www.avatar-network.org">https://www.avatar-network.org</a></p>
                <p>Информация о нас: <br>
                    <a href="mailto:god@avatar-bank.com">avatarnetwork777@gmail.com</a><br>
                    <a href="https://www.avatar-network.org">https://www.avatar-network.org</a>
                </p>
                <p>Если вы хотите отписаться от этой рассылки то нажмите <a href="{linkUnsubscribe}" target="_blank">здесь</a>.</p>
            </td>
        <tr>

    </table>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
