<?php

namespace lot\models\validate;

use avatar\models\search\UserAvatar;
use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\User;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class AuthSecondFaResetStep2 extends \iAvatar777\services\FormAjax\Model
{
    /** @var  string  */
    public $email;

    /** @var  string  */
    public $password;

    /** @var \common\models\UserAvatar */
    public $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['email', 'email'],
            ['email', 'findEmail'],

            ['password', 'string'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'    => 'Ваш логин или e-mail в Нейро',
            'password' => 'Ваш пароль для входа в Нейро',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function findEmail($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $u = \common\models\UserAvatar::findOne(['email' => $this->email]);
            } catch (\Exception $e) {
                $this->addError($attribute, 'Не найден пользователь');
                return;
            }
            if ($u->id != $this->user->id) {
                $this->addError($attribute, 'Не тот пользователь');
                return;
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->user->validatePassword($this->password)) {
                $this->addError($attribute, 'Пароль не верный');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $this->user->lot_google_auth_code = null;
        $this->user->save();

        Yii::$app->user->login($this->user);

        return 1;
    }

}
