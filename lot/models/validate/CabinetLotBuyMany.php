<?php

namespace lot\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetLotBuyMany extends \iAvatar777\services\FormAjax\Model
{
    /** @var int */
    public $count;

    public $is_cash_back;

    /** @var \common\models\Lot */
    public $lot;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'integer'],
            ['count', 'validateWallet'],
            ['count', 'validateBilet'], // Проверяет чтобы заказанное кол-во билетов было в наличии

            ['is_cash_back', 'integer'],
        ];
    }


    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $w */
            $w = $data['wallet'];
            $lot = $this->lot;
            $price = $lot->price;
            if ($this->is_cash_back) {
                $price += $lot->cash_back_price;
            }
            $sum = $price * $this->count;

            if ($w->amount < $sum) {
                $this->addError($a, 'У вас недостаточно средств');
                return;
            }
        }
    }

    /**
     * Проверяет чтобы заказанное кол-во билетов было в наличии
     *
     * @param $a
     * @param $p
     *
     * @throws \Exception
     */
    public function validateBilet($a, $p)
    {
        if (!$this->hasErrors()) {
            // билетов в лоте
            $bilet_count = $this->lot->bilet_count;
            // билетов продано
            $bilet_sold = Bilet::find()->where(['lot_id' => $this->lot->id])->count();
            // билетов доступно для покупки
            $bilet_avaliable = $bilet_count - $bilet_sold;

            // Если заказанное кол-во билетов больше доступных?
            if ($this->count > $bilet_avaliable) {
                $this->addError($a, 'Такое кол-во билетов нет в наличии. Осталось билетов: ' . $bilet_avaliable);
                return;
            }
        }
    }

    public function attributeWidgets()
    {
        return [
            'is_cash_back' => '\common\widgets\CheckBox2\CheckBox',
        ];
    }

    /**
     *
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        // Списываю деньги
        $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, Yii::$app->user->id);
        /** @var \common\models\piramida\Wallet $w */
        $w = $data['wallet'];
        $cio = CurrencyIO::findFromExt(Currency::LOT);

        $rows = [];
        for ($i = 1; $i <= $this->count; $i++) {
            $t = $w->move2(
                $cio->main_wallet,
                $this->lot->price,
                Json::encode(['Покупка билета для лота №{id}', ['id' => $this->lot->id]])
            );

            if ($this->is_cash_back) {
                // Списываю деньги за кешбек
                $t = $w->move2(
                    $cio->main_wallet,
                    $this->lot->cash_back_price,
                    Json::encode(['Покупка кешбека для лота №{id}', ['id' => $this->lot->id]])
                );
            }

            // Оформление билета
            $i1 = Bilet::add([
                'lot_id'       => $this->lot->id,
                'user_id'      => Yii::$app->user->id,
                'tx_id'        => $t['transaction']['id'],
                'is_cash_back' => $this->is_cash_back,
            ]);
            $num = substr($i1['num'], 6);
            $num = intval($num);
            $rows[] = $i1['num'] . ' Билет №'.$num;
        }
        // Прибавляю кол-во купленных билетов
        $this->lot->bilet_sold += $this->count;
        $this->lot->save();

        $this->notTelegram($rows);

        return $rows;
    }

    public function notTelegram($ids)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramLot;

        $str = 'Ваш идентификатор при розыгрыше лота: '. join(', ', $ids);
        $u = Yii::$app->user->identity;
        if (!Application::isEmpty($u->telegram_lot_chat_id)) {

            $telegram->sendMessage(['chat_id' => $u->telegram_lot_chat_id, 'text' => $str]);
        }
    }
}
