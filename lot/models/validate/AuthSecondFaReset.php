<?php

namespace lot\models\validate;

use avatar\models\search\UserAvatar;
use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\User;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * \avatar\controllers\CabinetBillsController::actionActivateCard
 */
class AuthSecondFaReset extends \iAvatar777\services\FormAjax\Model
{
    /** @var  string  */
    public $telegram;
    public $user;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['telegram', 'string'],
            ['telegram', 'validateCardNumber'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'telegram' => 'Введите имя пользователя вашего Telegram (без символа “@”)',
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateCardNumber($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $u = \common\models\UserAvatar::findOne(['telegram_username' => $this->telegram]);
            if (is_null($u)) {
                $this->addError($attribute, 'Не найден пользователь');
                return;
            }
            $this->user = $u;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $rows = Yii::$app->session->get('SECOND-FA-RESET-LOT');
        $code = Security::generateRandomString();
        $o = [
            'id'   => $this->user->id,
            'code' => $code,
        ];
        if ($rows === false) {
            $rows = [$o];
        } else {
            $rows[] = $o;
        }
        Yii::$app->session->set('SECOND-FA-RESET-LOT', $rows);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramLot;

        $url = Url::to(['auth/second-fa-reset-step2', 'code' => $code], true);
        $telegram->sendMessage(['chat_id' => $this->user->telegram_lot_chat_id, 'text' => 'Для сброса 2FA пройдите по ссылке:' . "\n" . $url]);

        return 1;
    }

}
