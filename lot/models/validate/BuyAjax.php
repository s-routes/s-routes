<?php

namespace lot\models\validate;

use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 */
class BuyAjax extends Model
{
    /** @var  int */
    public $id;
    public $is_cash_back;

    /** @var \common\models\Lot */
    public $lot;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateWallet'],

            ['is_cash_back', 'integer'],
        ];
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            $data = UserBill::getInternalCurrencyWallet(Currency::LOT, Yii::$app->user->id);

            /** @var \common\models\piramida\Wallet $w */
            $w = $data['wallet'];
            $lot = Lot::findOne($this->id);
            $this->lot = $lot;
            $price = $lot->price;
            if ($this->is_cash_back) {
                $price += $lot->cash_back_price;
            }
            if ($w->amount < $price) {
                $this->addError($a, 'У вас недостаточно средств');
                return;
            }
        }
    }

    /**
     *
     */
    public function action()
    {
        // Списываю деньги
        $data = UserBill::getInternalCurrencyWallet(Currency::LOT, Yii::$app->user->id);
        /** @var \common\models\piramida\Wallet $w */
        $w = $data['wallet'];
        $cio = CurrencyIO::findFromInt(Currency::LOT);
        $t = $w->move2(
            $cio->main_wallet,
            $this->lot->price,
            Json::encode(['Покупка билета для лота №{id}', ['id' => $this->lot->id]])
        );

        if ($this->is_cash_back) {
            // Списываю деньги за кешбек
            $t = $w->move2(
                $cio->main_wallet,
                $this->lot->cash_back_price,
                Json::encode(['Покупка кешбека для лота №{id}', ['id' => $this->lot->id]])
            );
        }

        // Оформление билета
        $i = Bilet::add([
            'lot_id'       => $this->id,
            'user_id'      => Yii::$app->user->id,
            'tx_id'        => $t['transaction']['id'],
            'is_cash_back' => $this->is_cash_back,
        ]);

        // Прибавляю кол-во купленных билетов
        $this->lot->bilet_sold++;
        $this->lot->save();

        return $i;
    }
}
