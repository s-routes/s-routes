<?php

namespace lot\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\Application;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetLotIsFinish extends Model
{
    /** @var int */
    public $id;

    /** @var \common\models\Lot */
    public $lot;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateLot'],
        ];
    }


    public function validateLot($a, $p)
    {
        if (!$this->hasErrors()) {
            $lot = Lot::findOne($this->id);
            $this->lot = $lot;

            if (is_null($lot)) {
                $this->addError($a, 'Не найден лот');
                return;
            }
        }
    }


    /**
     *
     */
    public function action()
    {
        return [
            'is_time_finish' => $this->lot->time_finish < time() ? 1 : 0,
        ];
    }

}
