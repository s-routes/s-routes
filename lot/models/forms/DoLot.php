<?php

namespace lot\models\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class DoLot extends Model
{
    public $id;

    /** @var  Lot */
    public $lot;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validatePrize'],
        ];
    }

    public function validatePrize($a, $p)
    {
        if (!$this->hasErrors()) {
            $lot = Lot::findOne($this->id);
            if (is_null($lot)) {
                $this->addError($a, 'Lot не найден');
                return;
            }
            $this->lot = $lot;

            if ($lot->is_prize_payd == 1) {
                $this->addError($a, 'Приз уже выплачен');
                return;
            }

            if (Application::isEmpty($lot->prize_amount)) {
                $this->addError($a, 'Не указана цена приза');
                return;
            }
        }
    }

    public function action()
    {
        $lot = Lot::findOne($this->id);
        $cio = CurrencyIO::findFromExt(Currency::LOT);
        $mainWallet = Wallet::findOne($cio->main_wallet);

        // получаю кошелек пользователя
        $bilet = Bilet::findOne($lot->winner_id);
        $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, $bilet->user_id);
        $w = $data['wallet'];

        $t = $mainWallet->move2(
            $w,
            $lot->prize_amount,
            'Приз за лотерею #'. $lot->id
        );
        $tr = Transaction::findOne($t['transaction']);
        $lot->is_prize_payd = 1;
        $lot->save();

        $this->notificateTelegram($lot);

        return [
            'tx_id'      => $tr->id,
            'tx_address' => $tr->getAddress(),
        ];
    }

    /**
     * @param \common\models\Lot $lot
     */
    private function notificateTelegram($lot)
    {
        $params = Yii::$app->params['telegramLot'];
        $userField = $params['userField'];
        $component = $params['component'];

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;

        // Если пишет клиент
        $all = Yii::$app->authManager->getUserIdsByRole('role_lot_admin');

        $Bilet = Bilet::findOne($lot->winner_id);
        $winner = UserAvatar::findOne($Bilet->user_id);

        foreach ($all as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->$userField)) {
                $telegram->sendMessage([
                    'chat_id' => $u->$userField,
                    'text'    => 'Произведена выплата приза ' . Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($lot->prize_amount, \common\models\piramida\Currency::LOT), 2) . ' LOT пользователю ' . $winner->getAddress()
                ]);
            }
        }

    }
}
