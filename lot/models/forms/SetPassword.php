<?php

namespace lot\models\forms;

use common\models\avatar\UserBill;
use common\models\UserAvatar;
use common\models\UserTelegramTemp;
use cs\Application;
use Yii;
use yii\base\Model;
use yii\db\Query;


/**
 */
class SetPassword extends Model
{
    public $password1;
    public $password2;

    private $options;

    public function init()
    {
        $this->options = Yii::$app->params['telegramLot'];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['password1', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],
        ];
    }

    /**
     * @param \common\models\UserTelegramShopConnect $registration
     * @param int                                $timeOut
     *
     * @return UserAvatar
     * @throws
     */
    public function action($registration, $timeOut)
    {
        $component = $this->options['component'];
        $userField = $this->options['userField'];
        $userFieldAccount = $this->options['userFieldAccount'];
        $languageCategory = $this->options['languageCategory'];

        $user = UserAvatar::add([
            'email'            => $registration->email,
            'password'         => UserAvatar::hashPassword($this->password1),
            'registered_ad'    => time(),
            'email_is_confirm' => 1,
            'auth_key'         => \Yii::$app->security->generateRandomString(60),
            $userFieldAccount  => $registration->username,
            $userField         => $registration->chat_id,
            'language'         => $registration->language,
        ]);

        // удалаю \common\models\UserTelegramConnect
        $registration->delete();

        $modelTemp = $this->options['modelTemp'];
        // удалаю \common\models\UserTelegramTemp
        $modelTemp::deleteAll(['username' => $user->$userFieldAccount]);

        // Устанавливаю язык для бота
        $language = \avatar\controllers\TelegramController::getOptimalLanguage($user);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;
        $text = Yii::t($languageCategory, $this->options['messages']['t']['actionRegistrationTelegram'], [], $language);
        $telegram->sendMessage(['chat_id' => $user->$userField, 'text' => $text]);

        // авторизую пользователя
        Yii::$app->user->login($user, $timeOut);

        return $user;
    }
}
