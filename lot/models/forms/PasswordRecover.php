<?php

namespace lot\models\forms;

use avatar\models\UserRecover;
use common\models\UserAvatar;
use cs\Application;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class PasswordRecover extends Model
{
    public $email;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                'email',
                'required',
                'message' => 'Это поле должно быть заполнено обязательно'
            ],
            [
                'email',
                'email',
                'message' => 'Email должен быть верным'
            ],
            [
                'email',
                'validateEmail'
            ],
            ['verifyCode', '\cs\Widget\Google\reCaptcha\Validator', 'skipOnEmpty' => false],
        ];
    }

    public function scenarios()
    {
        return [
            'insert' => [
                'email',
                'verifyCode'
            ],
            'ajax'   => ['email'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Проверочный код',
        ];
    }

    public function validateEmail($attribute, $params)
    {
        $email = strtolower($this->email);
        try {
            $user = UserAvatar::findOne(['email' => $email]);
        } catch (\Exception $e) {
            $this->addError($attribute, 'Такой пользователь не найден');
            return;
        }

        if ($user->mark_deleted == 1) {
            $this->addError($attribute, 'Пользователь заблокирован');
            return;
        }
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param  string $email the target email address
     *
     * @return boolean whether the model passes validation
     *
     * @throws \cs\web\Exception
     */
    public function send()
    {
        $user = UserAvatar::findOne(['email' => $this->email]);
        $userRecover = UserRecover::add($user->id);

        $ret = \cs\Application::mail($this->email, 'Восстановление пароля', 'password_recover', [
            'user'     => $user,
            'url'      => Url::to([
                'auth/password-recover-activate',
                'code' => $userRecover->code
            ], true),
            'datetime' => Yii::$app->formatter->asDatetime($userRecover->date_finish),
        ]);

        // Уведомляю клиента по телеграму
        /** @var UserAvatar $user */
        if (!Application::isEmpty($user->telegram_chat_id)) {
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegram;
            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Создана заявка на смену пароля для E-mail {email}', ['email' => $user->email], $language)]);
        }

        // Уведомляю админа по телеграму
        $userList = Yii::$app->authManager->getUserIdsByRole('role_admin');
        foreach ($userList as $uid) {
            $user2 = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user2->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user2->language)) ? 'ru' : $user2->language;
                $telegram->sendMessage(['chat_id' => $user2->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Создана заявка на смену пароля для E-mail {email}', ['email' => $user->email], $language)]);
            }
        }

        return $ret;
    }
}
