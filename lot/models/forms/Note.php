<?php

namespace lot\models\forms;

use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Note extends ActiveRecord
{
    public static function tableName()
    {
        return 'lot_note';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['content', 'string'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => Yii::t('c.xaREK0rYPp', 'Наименование'),
            'content' => Yii::t('c.xaREK0rYPp', 'Содержание'),
        ];
    }

    public function attributeWidgets()
    {
        return [
        ];
    }

}
