<?php

namespace lot\models\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class LotCancel extends Model
{
    public $id;

    /** @var  Lot */
    public $lot;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateLot'],
        ];
    }

    public function validateLot($a, $p)
    {
        if (!$this->hasErrors()) {
            $lot = Lot::findOne($this->id);
            if (is_null($lot)) {
                $this->addError($a, 'Lot не найден');
                return;
            }
            $this->lot = $lot;

            if ($lot->is_cancel == 1) {
                $this->addError($a, 'Лот уже отменен');
                return;
            }
        }
    }

    public function action()
    {
        $lot = $this->lot;
        $cio = CurrencyIO::findFromExt(Currency::LOT);
        $mainWallet = Wallet::findOne($cio->main_wallet);

        /** @var \common\models\Bilet $bilet */
        foreach (Bilet::find()->where(['lot_id' => $lot->id])->all() as $bilet) {
            $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, $bilet->user_id);
            /** @var \common\models\piramida\Wallet $w */
            $w = $data['wallet'];

            $t = $mainWallet->move2(
                $w,
                $lot->price,
                'Возврат за билет '. $bilet->num
            );
        }

        $lot->is_cancel = 1;
        $lot->save();

        return 1;
    }
}
