<?php

namespace lot\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\ChatMessage2;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use function Symfony\Component\String\title;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 */
class LotCopy extends Model
{
    public $id;

    public $lot;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateLot'],
        ];
    }

    public function validateLot($a, $p)
    {
        if (!$this->hasErrors()) {
            $lot = Lot::findOne($this->id);
            if (is_null($lot)) {
                $this->addError($a, 'Не найден лот');
                return;
            }
            $this->lot = $lot;
        }
    }

    public function action()
    {
        $lot = $this->lot;
        $attributes = $lot->attributes;
        unset($attributes['id']);
        $attributes['bilet_sold'] = 0;
        $attributes['is_counter_stop'] = 0;
        $attributes['is_counter_start'] = 0;
        $attributes['is_counter_start2'] = 0;
        $attributes['room_id'] = null;
        $attributes['is_prize_payd'] = 0;
        $attributes['is_send_win'] = 0;
        $attributes['winner_id'] = null;
        $attributes['time_start'] = time() + 60*60*24;
        $attributes['time_finish'] = time() + 60*60*24 + 60;
        $attributes['time_raffle'] = time() + 60*60*24 + 60*2;

        $lot = Lot::add($attributes);

        return 1;
    }



}
