<?php

namespace lot\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 */
class LotPoolStart extends Model
{
    public $id;

    public function rules()
    {
        return [
            ['id', 'integer'],
//            ['id', 'validateLot'],
        ];
    }

    public function action()
    {
        $lot = Lot::findOne($this->id);

        $lot->status = Lot::STATUS_POOL_START;
        $lot->save();

        $uid_list = Bilet::find()->where(['lot_id' => $this->id])->groupBy(['user_id'])->select('user_id')->column();
        foreach ($uid_list as $uid) {
            $u = UserAvatar::findOne($uid);
            $this->notify1($u);
        }
    }

    /**
     * @param UserAvatar $u
     */
    public function notify1($u)
    {
        $text = 'Пул участников сформирован, запущен стартовый таймер';
        $this->notify(
            $u,
            [
                'subject' => 'Пул участников сформирован, запущен стартовый таймер',
                'view'    => 'bilet_win',
                'options' => [
                    'text' => $text
                ],
            ],
            $text
        );
    }

    /**
     * @param UserAvatar $user
     * @param array $mail
     * [
     *  'subject'
     *  'view'
     *  'options'
     * ]
     * @param string $telegram_text
     */
    public function notify($user, $mail, $telegram_text)
    {
        Subscribe::sendArray([$user->email], $mail['subject'], $mail['view'], $mail['options']);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramLot;
        if (!Application::isEmpty($user->telegram_lot_chat_id)) {
            $telegram->sendMessage(['chat_id' => $user->telegram_lot_chat_id, 'text' => $telegram_text]);
        }
    }
}
