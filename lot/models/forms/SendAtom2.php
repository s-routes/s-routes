<?php

namespace lot\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\NeironTransaction;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 */
class SendAtom2 extends \iAvatar777\services\FormAjax\Model
{
    /** @var  int Идентификатор счета user_bill.id */
    public $id;

    /** @var  int Кол-во монет */
    public $amount;

    /** @var  int Кол-во атомов для отправки */
    public $amountToSend;

    public $comment;

    /** @var string идентификатор пользователя U_0000... */
    public $user_email;

    /** @var  \common\models\UserAvatar */
    public $user;

    /** @var  \common\models\piramida\Wallet */
    public $walletIn;

    /** @var  \common\models\piramida\Wallet */
    public $walletOut;

    /** @var  \common\models\CurrencyIO */
    public $link;

    /** @var  \common\models\avatar\UserBill */
    public $billOut;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'exist', 'targetClass' => '\common\models\avatar\UserBill'],
            ['id', 'setItem'],

            ['amount', 'required'],
            ['amount', 'integer'],
            ['amount', 'validateDecimals'],
            ['amount', 'convertToAtom'],
            ['amount', 'validateOutWallet'],

            ['comment', 'string'],
            ['comment', 'default', 'value' => Yii::t('c.EQFlAIQvBL', 'Отправление')],

            ['user_email', 'required', 'message' => 'Необходимо заполнить User-ID получателя'],
            ['user_email', 'string'],
            ['user_email', 'setUser'],
            ['user_email', 'setWalletIn'],
            ['user_email', 'validateSelf'],
        ];
    }

    public function setItem($attribute, $params)
    {
        if (!$this->hasErrors()) {
            try {
                $billOut = UserBill::findOne($this->id);
            } catch (\Exception $e) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Счет не найден'));
                return;
            }
            if ($billOut->mark_deleted) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Счет удален'));
                return;
            }
            $link = CurrencyIO::findFromExt($billOut->currency);
            if (is_null($link)) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Эта монета не предназначена для перевода'));
                return;
            }
            $walletOut = Wallet::findOne($billOut->address);
            $this->link = $link;
            $this->walletOut = $walletOut;
            $this->billOut = $billOut;
        }
    }

    public function validateDecimals($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $a = $this->amount;
            $d = explode('.', $a);
            if (count($d) == 1) {
                return;
            }
            if (count($d) == 2) {
                $currency = \common\models\avatar\Currency::findOne($this->link->currency_ext_id);
                if (strlen($d[1]) > $currency->decimals_view) {
                    $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Разрядность монеты').' = ' . $currency->decimals_view);
                    return;
                }
            }
        }
    }

    public function convertToAtom($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $currency = \common\models\piramida\Currency::findOne($this->link->currency_int_id);
            $this->amountToSend = bcmul($this->amount, bcpow(10, $currency->decimals));
        }
    }

    public function validateOutWallet($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->walletOut->amount < $this->amountToSend) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Недостаточно средств в кошельке'));
                return;
            }
        }
    }

    public function setUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!StringHelper::startsWith($this->user_email, 'U_')) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Идентификатор должен быть в формате U_000...'));
                return;
            }

            $id = (int)substr($this->user_email, 2);
            try {
                $user = \common\models\UserAvatar::findOne($id);
            } catch (\Exception $e) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Не найден пользователь'));
                return;
            }

            $this->user = $user;
        }
    }

    public function validateSelf($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($this->user->id == Yii::$app->user->id) {
                $this->addError($attribute, Yii::t('c.EQFlAIQvBL', 'Нельзя отправить средства самому себе'));
                return;
            }
        }
    }

    /**
     * Устанавливает входящий кошелек. Выбирает кошелек по умолчанию, если нет, то создает
     */
    public function setWalletIn($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $bill = UserBill::getInternalCurrencyWallet($this->link->currency_int_id, $this->user->id);
            $wallet = Wallet::findOne($bill['billing']->address);

            $this->walletIn = $wallet;
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        /** @var \common\models\UserAvatar $user */
        $user = Yii::$app->user->identity;

        /**
         * [
         *  'transaction'   => '\common\models\piramida\Transaction'
         *  'operation_add' => '\common\models\Piramida\Operation'
         *  'operation_sub' => '\common\models\Piramida\Operation'
         * ]
         */
        $t = $this->walletOut->move2(
            $this->walletIn,
            $this->amountToSend,
            Json::encode([
                'Перевод средств. Отправитель {sender} ({sender_wallet}). Получатель {recipient} ({recipient_wallet}) Комментарий {comment}',
                [
                    'sender'           => $user->getAddress(),
                    'sender_wallet'    => $this->walletOut->getAddress(),
                    'recipient'        => $this->user->getAddress(),
                    'recipient_wallet' => $this->walletIn->getAddress(),
                    'comment'          => $this->comment,
                ],
            ]),
            NeironTransaction::TYPE_MOVE
        );

        // Делаю запись о типе транзакции если это NERON
        if ($this->walletOut->currency_id == \common\models\piramida\Currency::NEIRO) {
            NeironTransaction::add([
                'transaction_id' => $t['transaction']['id'],
                'operation_id'   => $t['operation_add']['id'],
                'type_id'        => NeironTransaction::TYPE_MOVE,
            ]);
            NeironTransaction::add([
                'transaction_id' => $t['transaction']['id'],
                'operation_id'   => $t['operation_sub']['id'],
                'type_id'        => NeironTransaction::TYPE_MOVE,
            ]);
        }

        return $t;
    }

}
