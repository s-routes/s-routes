<?php

namespace lot\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 */
class LotShow extends Model
{
    public $id;

    public function rules()
    {
        return [
            ['id', 'integer'],
        ];
    }

    public function action()
    {
        $lot = Lot::findOne($this->id);

        $lot->is_hide = 0;
        $lot->save();

    }
}
