<?php

namespace lot\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\NeironTransaction;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 */
class AdminLotAuto extends \iAvatar777\services\FormAjax\Model
{
    public $is_work;
    public $delta;
    public $name;
    public $image;
    public $price;
    public $bilet_count;
    public $add_time_finish;
    public $add_time_raffle;
    public $description;
    public $cash_back_percent;
    public $cash_back_price;
    public $content;

    public function rules()
    {
        return [
            ['is_work', 'integer'],

            ['name', 'required'],
            ['name', 'string'],

            ['image', 'required'],
            ['image', 'string'],

            ['price', 'required'],
            ['price', 'string'],

            ['bilet_count', 'required'],
            ['bilet_count', 'integer'],

            ['add_time_finish', 'required'],
            ['add_time_finish', 'integer'],

            ['add_time_raffle', 'required'],
            ['add_time_raffle', 'integer'],

            ['description', 'required'],
            ['description', 'string'],

            ['delta', 'required'],
            ['delta', 'integer'],

            ['cash_back_percent', 'required'],
            ['cash_back_percent', 'integer'],

            ['cash_back_price', 'required'],
            ['cash_back_price', 'integer'],

            ['content', 'required'],
            ['content', 'string'],
        ];
    }

    /**
     * @return \lot\models\forms\AdminLotAuto
     */
    public function init()
    {
        $lot = Config::get('lot-auto');
        if (is_null($lot) || $lot === false) {
            $lot = [
                'is_work'           => null,
                'name'              => null,
                'image'             => null,
                'price'             => null,
                'bilet_count'       => null,
                'delta'             => null,
                'add_time_finish'   => null,
                'add_time_raffle'   => null,
                'description'       => null,
                'cash_back_percent' => null,
                'cash_back_price'   => null,
                'content'           => null,
            ];
        } else {
            $lot = Json::decode($lot);
        }
        $this->is_work           = ArrayHelper::getValue($lot, 'is_work', null);
        $this->name              = ArrayHelper::getValue($lot, 'name', null);
        $this->image             = ArrayHelper::getValue($lot, 'image', null);
        $this->price             = ArrayHelper::getValue($lot, 'price', null);
        $this->bilet_count       = ArrayHelper::getValue($lot, 'bilet_count', null);
        $this->delta             = ArrayHelper::getValue($lot, 'delta', null);
        $this->add_time_finish   = ArrayHelper::getValue($lot, 'add_time_finish', null);
        $this->add_time_raffle   = ArrayHelper::getValue($lot, 'add_time_raffle', null);
        $this->description       = ArrayHelper::getValue($lot, 'description', null);
        $this->cash_back_percent = ArrayHelper::getValue($lot, 'cash_back_percent', null);
        $this->cash_back_price   = ArrayHelper::getValue($lot, 'cash_back_price', null);
        $this->content           = ArrayHelper::getValue($lot, 'content', null);
    }

    public function attributeLabels()
    {
        return [
            'is_work'           => 'Флаг. Выставлять лоты автоматически?',
            'delta'             => 'Время между двумя розыгрышами',
            'add_time_raffle'   => 'Время розырыша',
            'add_time_finish'   => 'Время окончания продаж',
            'description'       => 'Описание',
            'name'              => 'Наименование',
            'price'             => 'Цена билета',
            'bilet_count'       => 'Кол-во билетов',
            'cash_back_price'   => 'Цена пакета компенсации',
            'cash_back_percent' => 'Процент кешбека',
        ];
    }

    public function attributeHints()
    {
        return [
            'delta'           => 'мин',
            'price'           => 'LOT (RUB)',
            'add_time_raffle' => 'Добавка от "время старта продаж", мин',
            'add_time_finish' => 'Добавка от "время старта продаж", мин',
        ];
    }

    public function attributeWidgets()
    {
        return [
            'is_work'         => '\common\widgets\CheckBox2\CheckBox',
            'content'         => '\common\widgets\HtmlContent\HtmlContent',
            'price'           => '\avatar\widgets\Price',
            'cash_back_price' => '\avatar\widgets\Price',
            'image'           => [
                'class'    => '\iAvatar777\widgets\FileUpload7\FileUpload',
                'settings' => [
                    'controller'   => 'upload4',
                    'maxSize'      => 2000,
                    'server'       => '',
                    'button_label' => 'Выберите файл',
                ],
                'update'   => [
                    [
                        'function' => 'crop',
                        'index'    => 'crop',
                        'options'  => [
                            'width'  => '300',
                            'height' => '300',
                            'mode'   => 'MODE_THUMBNAIL_CUT',
                        ],
                    ],
                ],
            ]
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $lot = [
            'is_work'           => $this->is_work,
            'delta'             => $this->delta,
            'name'              => $this->name,
            'image'             => $this->image,
            'price'             => $this->price,
            'bilet_count'       => $this->bilet_count,
            'add_time_finish'   => $this->add_time_finish,
            'add_time_raffle'   => $this->add_time_raffle,
            'description'       => $this->description,
            'cash_back_percent' => $this->cash_back_percent,
            'cash_back_price'   => $this->cash_back_price,
            'content'           => $this->content,
        ];
        $lot = Json::encode($lot);
        Config::set('lot-auto', $lot);
    }

}
