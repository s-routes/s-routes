<?php
namespace lot\models\forms;

use common\models\school\File;
use cs\base\FormActiveRecord;
use cs\services\Url;
use cs\services\VarDumper;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 */
class Page extends FormActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lot_page';
    }

    public function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string',
                ['max' => 255],
            ],
            [
                'content',
                'Полный текст',
                0,
                'string',
                'widget' => [
                    '\common\widgets\HtmlContent\HtmlContent',
                ]
            ],
        ];
    }
}
