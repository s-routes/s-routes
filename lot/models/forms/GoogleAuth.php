<?php

namespace lot\models\forms;

use common\models\avatar\UserBill;
use common\models\UserDocument;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use Dompdf\Dompdf;
use yii\base\Exception;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use Yii;

/**
 */
class GoogleAuth extends Model
{
    public $code;
    public $pin;

    public function rules()
    {
        return [
            ['code', 'required'],

            ['pin', 'required'],
            ['pin', 'trim'],
            ['pin', 'removeSpace'],
            ['pin', 'validatePassword'],
        ];
    }

    public function removeSpace()
    {
        if (!$this->hasErrors()) {
            $this->pin = str_replace(' ','', $this->pin);
        }
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $secret = $this->code;
            $pinExternal = $this->pin;
            $file = \Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
            require_once($file);
            $ga = new \GoogleAuthenticator;
            $pinInternal = $ga->getCode($secret);
            if ($pinInternal != $pinExternal) {
                $this->addError($attribute, Yii::t('c.DLo6pHtmzQ', 'Пин код не верный'));
                return;
            }
        }
    }

    public function action()
    {
        // Сохраняю код
        /** @var \common\models\UserAvatar $user */
        $user = \Yii::$app->user->identity;
        $user->lot_google_auth_code = $this->code;
        $user->save();

        // готовлю html
        $filePath = \Yii::getAlias('@lot/views/cabinet-google-code/_qr-pdf.php');
        $contentHtml = \Yii::$app->view->renderFile($filePath, [
            'code'  => $this->code,
            'email' => \Yii::$app->user->identity->email,
        ]);

        // Генерирую файл
        $dompdf = new Dompdf();
        $dompdf->loadHtml($contentHtml);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $path = \Yii::getAlias('@runtime/'.time().'.pdf');
        file_put_contents($path, $dompdf->output());

        // Отправляю на почту
        Application::mail(\Yii::$app->user->identity->email, Yii::t('c.DLo6pHtmzQ', 'Код 2FA авторизации'), 'code_2fa', [
            'code'  => $this->code,
            'email' => \Yii::$app->user->identity->email,
        ], null, [
            $path => 'auth.pdf'
        ]);
    }
}