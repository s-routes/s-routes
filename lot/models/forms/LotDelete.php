<?php

namespace lot\models\forms;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use iAvatar777\services\FormAjax\ActiveRecord;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class LotDelete extends Model
{
    public $id;

    /** @var  Lot */
    public $lot;

    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validatePrize'],
        ];
    }

    public function validatePrize($a, $p)
    {
        if (!$this->hasErrors()) {
            $lot = Lot::findOne($this->id);
            if (is_null($lot)) {
                $this->addError($a, 'Lot не найден');
                return;
            }
            $this->lot = $lot;

            if ($lot->is_prize_payd == 1) {
                $this->addError($a, 'Приз уже выплачен');
                return;
            }

            if (Application::isEmpty($lot->prize_amount)) {
                $this->addError($a, 'Не указана цена приза');
                return;
            }
        }
    }

    public function action()
    {
        $lot = Lot::findOne($this->id);
        $cio = CurrencyIO::findFromExt(Currency::LOT);
        $mainWallet = Wallet::findOne($cio->main_wallet);

        // получаю кошелек пользователя
        $bilet = Bilet::findOne($lot->winner_id);
        $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, $bilet->user_id);
        $w = $data['wallet'];

        $t = $mainWallet->move2(
            $w,
            $lot->prize_amount,
            'Приз за лотерею #'. $lot->id
        );
        $tr = Transaction::findOne($t['transaction']);
        $lot->is_prize_payd = 1;
        $lot->save();

        return [
            'tx_id'      => $tr->id,
            'tx_address' => $tr->getAddress(),
        ];
    }
}
