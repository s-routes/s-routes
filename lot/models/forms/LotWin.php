<?php

namespace lot\models\forms;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\Bilet;
use common\models\ChatMessage2;
use common\models\CurrencyIO;
use common\models\Lot;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 */
class LotWin extends Model
{
    public $id;

    public function rules()
    {
        return [
            ['id', 'integer'],
        ];
    }

    public function action()
    {
        $bilet = Bilet::findOne($this->id);
        $lot = Lot::findOne($bilet->lot_id);
        $lot->winner_id = $this->id;
        $lot->is_counter_stop = 1;
        $lot->save();
        $user = $bilet->getUser();

        $biletWiiner = $bilet;
        $winner = UserAvatar::findOne($biletWiiner->user_id);

        $cio = CurrencyIO::findOne(['currency_int_id' => Currency::LOT]);
        $mainWalletObject = Wallet::findOne($cio->main_wallet);

        if ($lot->prize_type == Lot::PRIZE_TYPE_MONEY) {
            // Отправляю главный приз
            $data = UserBill::getInternalCurrencyWallet(Currency::LOT, $biletWiiner->user_id);
            $walletWin = $data['wallet'];
            $t = $mainWalletObject->move2($walletWin, $lot['prize_amount'], 'E-LOTTERY: Главный Приз розыгрыша #'.$lot['id']);
        }

        // Отправляю кешбек
        $cashBackRows = Bilet::find()
            ->where(['lot_id' => $lot->id, 'is_cash_back' => 1])
            ->andWhere(['not', ['user_id' => $biletWiiner->user_id]])
            ->all();
        /** @var \common\models\Bilet $bilet */
        foreach ($cashBackRows as $bilet) {
            $walletTo = UserBill::getInternalCurrencyWallet(Currency::LOT, $bilet->user_id);
            $cashBack = (int)($lot['price'] * $lot->cash_back_percent / 100);
            $t = $mainWalletObject->move2($walletTo['wallet'], $cashBack, 'Кешбек за лотерею #' . $lot['id']);
        }

        // Отправляю письмо
        Subscribe::sendArray([$winner->email], 'Вы выиграли Главный Приз розыгрыша #' . $lot->id, 'win', ['lot' => $lot], ['layout' => '@lot/mail/layouts/html.php', 'namespace' => 'lot']);

        // ставлю флаг
        $lot->is_send_win = 1;
        $lot->save();
        $ticket = ArrayHelper::toArray($biletWiiner);
        $ticket['user'] = [
            'id'         => $user->id,
            'name'       => $user->getName2(),
            'avatar'     => $user->getAvatar(),
            'name_first' => $user->name_first,
            'name_last'  => $user->name_last,
            'is_bot'     => Yii::$app->params['AUTO_REDEMPTION_USER_ID'] == $user->id ? 1 : 0,
        ];

        return [
            'ticket'          => $ticket,
            'chat'            => $this->actionChat($biletWiiner, $lot),
            'support_room_id' => \common\models\LotSupport::get(['user_id' => $biletWiiner->user_id])->room_id,
        ];
    }


    /**
     * @param \common\models\Bilet $bilet
     * @param \common\models\Lot $lot
     * @return array
     */
    public function actionChat($bilet, $lot)
    {
        $params = Yii::$app->params['telegramLot'];
        $userField = $params['userField'];
        $component = $params['component'];

        $messageText = 'Уважаемый Победитель!
Вы выиграли Главный Приз розыгрыша #' . $lot->id . '.
Мы рады, что сегодня изменчивая Фортуна выбрала именно Вас!
Примите поздравления и наилучшие пожелания от команды проекта eLOTTERY!

В зависимости от вида призовой формы, WINNER получает выигрыш:

При автоматизированном розыгрыше пула монет LOT - немедленно после подведения итогов, на свой аккаунт.

При online-розыгрыше, проводимом в ручном режиме - в течении короткого времени после подведения итогов.

При розыгрыше Главного Приза в материальной форме, победитель может выбрать любой из трех вариантов:
а) забрать приз физически из пункта выдачи или постамата, предварительно согласовав контактную информацию для доставки - город, адрес, телефон и т.д.
б) забрать эквивалент стоимости приза монетами LOT - затем оформить доставку самостоятельно через свой ЛК маркетплейса - затем забрать приз физически из пункта выдачи, постамата, у курьера.
в) забрать эквивалент стоимости приза монетами LOT - они будут переведены на счет победителя. В дальнейшем победитель распоряжается монетами по своему усмотрению.

Если у Вас будут вопросы, всегда можно связаться с нами здесь, через приватный чат в ЛК, либо написав письмо по адресу elcloud@openmail.cc

e-LOTTERY Team';

        $supportShop = \common\models\LotSupport::get(['user_id' => $bilet->user_id]);
        $supportShop->last_message = time();
        $supportShop->save();

        $message = ChatMessage2::add([
            'room_id' => $supportShop->room_id,
            'user_id' => Yii::$app->user->id,
            'message' => Json::encode([
                'text' => $messageText,
                'file' => '',
            ]),
        ]);

        $html = Yii::$app->view->renderFile('@shop/views/cabinet-exchange/message.php', ['message' => $message]);

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s');

        $room = \common\models\ChatRoom::findOne($lot->room_id);
        $room->last_message = time();
        $room->save();


        // Уведомляю по телеграму
        try {
            $this->notificateTelegram($bilet->user_id, $messageText);
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'shop\models\validate\CabinetSupportChatSend::action');
        }

        return [
            'message' => [
                'object'        => $data,
                'html'          => $html,
                'timeFormatted' => Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s'),
            ],
            'user'    => [
                'id'       => Yii::$app->user->id,
                'avatar'   => Yii::$app->user->identity->getAvatar(),
                'name2'    => Yii::$app->user->identity->getName2(),
            ],
        ];
    }


    private function notificateTelegram($uid, $messageText)
    {
        $params = Yii::$app->params['telegramLot'];
        $userField = $params['userField'];
        $component = $params['component'];

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->$component;

        $u = UserAvatar::findOne($uid);
        if (!Application::isEmpty($u->$userField)) {
            /** @var \common\models\UserAvatar $uCurrent */
            $link = Url::to(['cabinet-support/chat'], true);
            $telegram->sendMessage(['chat_id' => $u->$userField, 'text' => $messageText  . "\n" .  "\n" . 'Ссылка на чат: ' . $link]);
        }
    }

}
