<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $model \avatar\models\forms\Lot */

$this->title = 'Авто лот';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');

clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin/index';
    }).modal();
}
JS
            ,
        ]);

        ?>
        <?= $form->field($model, 'is_work') ?>
        <?= $form->field($model, 'delta') ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'bilet_count') ?>
        <?= $form->field($model, 'cash_back_price') ?>
        <?= $form->field($model, 'cash_back_percent') ?>
        <?= $form->field($model, 'add_time_finish') ?>
        <?= $form->field($model, 'add_time_raffle') ?>
        <?= $form->field($model, 'image') ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
        <?= $form->field($model, 'content') ?>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end([]); ?>

        <hr>
        <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
            'id'        => 'upload' . 1,
            'name'      => 'upload' . 1,
            'attribute' => 'upload' . 1,
            'model'     => new \avatar\models\forms\ChatFile(),
            'update'    => [],
            'settings'  => [
                'maxSize'           => 50 * 1000,
                'controller'        => 'upload4',
                'button_label'      => Yii::t('c.xgKKA2JpVp', 'Прикрепите файл'),
                'functionSuccess'   => new \yii\web\JsExpression(<<<JS
function (response) {
    // response.url
    $('.buttonCopy').removeClass('hide');
    $('.buttonCopy').attr('data-clipboard-text', response.url);
    return false;
}
JS
                ),
            ],
        ]); ?>
        <button class="btn btn-default btn-xs hide buttonCopy" role="">Копировать</button>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>