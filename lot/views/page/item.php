<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $page \common\models\LotPage */


$pageLang = \common\models\LotPageLang::findOne(['parent_id' => $page->id, 'language' => Yii::$app->language]);
if (is_null($pageLang)) {
    $name = $page->name;
    $content = $page->content;
} else {
    $name = $pageLang->name;
    $content = $pageLang->content;
}

$this->title = $name;

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

    </div>

    <div class="col-lg-1">

    </div>
    <div class="col-sm-10">

        <?= $content ?>

    </div>
</div>


