<?php
use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

/** @var \yii\web\View $this */
$this->title = $user->getName2();



$carListObjects = \common\models\Card::find()->where(['id' => $cardList])->all();

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

\avatar\assets\SocketIO\Asset::register($this);
$serverName = \avatar\assets\SocketIO\Asset::getHost();


$uid = Yii::$app->user->id;

$one = \common\models\UserBill3::find()
    ->where(['address' => '0xf9229814847a1bfdffd4272b76d7110b98e01851'])
    ->asArray()
    ->one();

$from = $one['address'];
$pk = $one['pk'];
$to = '0x996d4d79ee798e9e092fd305d99601170d61dc50';
$gasPrice = 50 * 1000000000;
$amount = $one['balance'] - ($gasPrice * 21000);

$this->registerJs(<<<JS

var socket = io.connect('{$serverName}');
var roomName = 'room' + 1;
var counter = 0;

socket.on('eth_pass2', function(hash) {
    console.log(hash);
});

socket.emit('eth_pass2', roomName, '{$from}', '{$pk}', '{$to}', $amount, $gasPrice);

JS
)


?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>
<!-- Large modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                $id = 80;
                $page = \common\models\exchange\Page::findOne($id);

                $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                if (is_null($pageLang)) {
                    $name = $page->name;
                    $content = $page->content;
                } else {
                    $name = $pageLang->name;
                    $content = $pageLang->content;
                }
                ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $name ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS
// при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>

<div class="container" style="padding-bottom: 70px;">

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $user->getName2() ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
    </div>
    <div class="col-lg-8">
        <br>
        <p class="text-center">
            <img src="<?= $user->getAvatar() ?>" width="300" class="img-circle">
        </p>

        <p class="text-center" style="font-size: 30px;">
            <?= \Yii::t('c.sVz21NGD71', 'Ваш') ?> USER-ID:
        </p>
        <p class="text-center">
            <code title="<?= \Yii::t('c.sVz21NGD71', 'Нажми чтобы скопировать') ?>" data-toggle="tooltip" data-clipboard-text="<?= $user->getAddress() ?>" class="buttonCopy" style="font-size: 30px;"><?= $user->getAddress() ?></code>
        </p>
        <p class="text-center" style="font-size: 30px;">
            <?= \Yii::t('c.sVz21NGD71', 'Ваш') ?> е-mail:
        </p>
        <p class="text-center">
            <code title="<?= \Yii::t('c.sVz21NGD71', 'Нажми чтобы скопировать') ?>" data-toggle="tooltip" data-clipboard-text="<?= $user->email ?>" class="buttonCopy" style="font-size: 30px;"><?= $user->email ?></code>
        </p>
        <p id="counter"></p>
        <p id="address_eth"><?= $from ?></p>


    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>


