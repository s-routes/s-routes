<?php
use common\models\piramida\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'РОЗЫГРЫШИ';

$c = Currency::findOne(Currency::LOT);

$cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);
$type = [
    \common\models\Lot::TYPE_AUTO   => 'Автоматизированный',
    \common\models\Lot::TYPE_MANUAL => 'Онлайн-трансляция',
];
$prize_type = [
    \common\models\Lot::PRIZE_TYPE_MONEY => 'Пул монет',
    \common\models\Lot::PRIZE_TYPE_THING => 'Материальный',
];
?>






<div style="margin: 0px 50px 0px 50px;">
    <div class="row">
        <div class="col-lg-6">
            <p><a href="https://topmate.one/" target="_blank" class="btn btn-primaru" style="width:100%;background-color: #037394; color: #fff;">TOPMATE.ONE - ИНТЕРНЕТ-ШОПИНГ С ОПЛАТОЙ КРИПТОАКТИВАМИ</a> </p>
        </div>
        <div class="col-lg-6">
            <p><a href="https://neiro-n.com/cabinet-exchange/index" target="_blank" class="btn btn-primaru" style="width:100%;background-color: #750F0B; color: #fff;">NEIRO-N.COM - P2P ОБМЕН И КОНВЕРТАЦИЯ КРИПТОАКТИВОВ В LOT</a> </p>
        </div>
    </div>

        <h1 class="page-header text-center"><?= $this->title ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Lot::find()->where(['is_hide' => 0])->andWhere(['>', 'time_raffle', time() - 60*60*24])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover table_exchange',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'attribute'      => 'id',
                    'header'         => 'ID',
                    'contentOptions' => ['aria-label' => 'ID'],
                ],
                [
                    'header'  => 'Лот',
                    'contentOptions' => ['aria-label' => 'Лот'],
                    'content' => function ($item) {
                        return Html::img(\iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop'), ['class' => 'thumbnail', 'width' => 100, 'style' => 'margin-bottom: 0px;', 'data' => ['toggle' => 'tooltip'], 'title' => $item->name]);
                    },
                ],
                [
                    'header'  => 'Главный приз',
                    'contentOptions' => ['aria-label' => 'Главный приз'],
                    'content' => function ($item) {
                        $lot =     $item;

                        $c = Currency::findOne(Currency::LOT);

                        $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                        $prize = '';
                        if ($lot->prize_type == \common\models\Lot::PRIZE_TYPE_MONEY) {
                            if (!\cs\Application::isEmpty($lot['prize_name'])) {
                                $prize = $lot['prize_name'];
                            } else {
                                $prize = Currency::getValueFromAtom($lot['prize_amount'], Currency::LOT) . $cHtml;
                            }
                        }
                        if ($lot->prize_type == \common\models\Lot::PRIZE_TYPE_THING) {
                            $prize = $lot->prize_name;
                        }

                        return $prize;
                    },
                ],
                [
                    'attribute'      => 'bilet_count',
                    'header'         => 'Кол-во билетов',
                    'contentOptions' => ['aria-label' => 'Кол-во билетов'],
                ],
                [
                    'header'  => 'Цена билета',
                    'contentOptions' => ['aria-label' => 'Цена билета'],
                    'content' => function ($item) {
                        $c = Currency::findOne(Currency::LOT);

                        $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);
                        return \common\models\piramida\Currency::getValueFromAtom($item['price'], \common\models\piramida\Currency::LOT) . $cHtml;
                    },
                ],
                [
                    'header'  => 'Шансы на выигрыш',
                    'contentOptions' => ['aria-label' => 'Шансы на выигрыш'],
                    'content' => function ($item) {
                        $v = Yii::$app->formatter->asDecimal(((1/$item->bilet_count) *100), 2) . '%';

                        return $v;
                    },
                ],
                [
                    'header'  => 'Тип розыгрыша',
                    'contentOptions' => ['aria-label' => 'Тип розыгрыша'],
                    'content' => function ($item) {
                        $type = [
                            \common\models\Lot::TYPE_AUTO   => 'Автоматизированный',
                            \common\models\Lot::TYPE_MANUAL => 'Онлайн-трансляция',
                        ];

                        return $type[$item['type']];
                    },
                ],
                [
                    'header'  => 'Тип Главного Приза',
                    'contentOptions' => ['aria-label' => 'Тип Главного Приза'],
                    'content' => function ($item) {
                        $prize_type = [
                            \common\models\Lot::PRIZE_TYPE_MONEY => 'Пул монет',
                            \common\models\Lot::PRIZE_TYPE_THING => 'Материальный',
                        ];

                        return $prize_type[$item['prize_type']];
                    },
                ],
                [
                    'header'  => 'Билетов осталось',
                    'contentOptions' => ['aria-label' => 'Билетов осталось'],
                    'content' => function ($item) {
                        $bilet_sold = \common\models\Bilet::find()->where(['lot_id' => $item->id])->count();

                        return $item->bilet_count - $bilet_sold . 'шт';
                    },
                ],
                [
                    'attribute'      => 'time_raffle',
                    'header'         => 'Время розыгрыша',
                    'contentOptions' => ['aria-label' => 'Время розыгрыша'],
                    'format'         => 'datetime'
                ],
                [
                    'header'  => '',
                    'contentOptions' => ['aria-label' => 'Статус'],
                    'content' => function ($item) {
                        $html = [];
                        if ($item['time_raffle'] < time()) {
                            $html[] =  '<p class="label label-danger">Разыграна</p>';
                        }

                        $html[] =   '<p><a href="/lot/item?id='.$item->id.'" class="btn btn-primary">Перейти к лоту</a></p>';

                        return join('', $html);
                    },
                ],


            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>


</div>


