<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\validate\AuthSecondFaReset */

$this->title = 'Сброс 2FA';
?>

<div class="container">
    <div class="col-lg-4 col-lg-offset-4">
        <h1 class="page-header text-center">
            <?= Html::encode($this->title) ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model,
                'success' => <<<JS
function (ret) {
    $('#modalInfo').modal();
}
JS

        ]) ?>

        <?= $form->field($model, 'telegram') ?>
        <hr>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Сбросить']) ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно! Перейдите в телеграм и пройдите по ссылке и далее введите логин и пароль от своего аккаунта.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

    <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <?php
                    $id = 79;
                    $page = \common\models\exchange\Page::findOne($id);
                    if (is_null($page)) {
                        $page = \common\models\exchange\Page::findOne(1);
                    }

                    $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                    if (is_null($pageLang)) {
                        $name = $page->name;
                        $content = $page->content;
                    } else {
                        $name = $pageLang->name;
                        $content = $pageLang->content;
                    }
                    ?>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?= $name ?></h4>
                </div>
                <div class="modal-body">
                    <?= $content ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJs(<<<JS
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>