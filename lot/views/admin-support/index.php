<?php

/** $this \yii\web\View  */
/** $modelSupport '\common\models\LotSupport'  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Все чаты';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/admin-support/chat' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => $modelSupport::find()
                    ->orderBy(['last_message' => SORT_DESC])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['user_id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                'user_id',
                [
                    'header'  => '',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'user_id', '');
                        if ($i == '') return '';
                        $user = \common\models\UserAvatar::findOne($i);

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                            'title' => $user->getName2(),
                            'data' => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Последнее сообщение',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'last_message', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>