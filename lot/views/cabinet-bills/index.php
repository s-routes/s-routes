<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('c.TsqnzVaJuC', 'Мои балансы');

$strCopy = Yii::t('c.TsqnzVaJuC', 'Скопировано');

\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS
var clipboard = new Clipboard('.js-buttonTransactionInfo');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: '{$strCopy}',
        placement: 'bottom'
    });
    $(e.trigger).tooltip('show');
    setTimeout(function(ee) {
        $(e.trigger).tooltip('destroy');
    }, 1000);
});
JS
);

$this->registerJs(<<<JS

$('.rowTable').click(function(e) {
    window.location = '/cabinet-wallet/item' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonThisProcessing').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-wallet/send?id=' + $(this).data('id');
});

$('.buttonInput').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-active/item?id=' + $(this).data('currency_io');
});

$('.buttonInput2').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-wallet/convert2?id=' + $(this).data('id');
});

$('.buttonOutput').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-active/out?id=' + $(this).data('currency_io');
});

$('.buttonConvert').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-wallet/convert?id=' + $(this).data('id');
});

$('.buttonConvert3').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/cabinet-wallet/convert-market?id=' + $(this).data('id');
});
$('.buttonConvert4').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
});


JS
);

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;


?>

<style>
    .textDecorated {
        text-decoration-line: underline;
        text-decoration-style: dotted;
    }
    .balance1 {
        font-size: 150%;
    }
</style>



<div class="container" style="padding-bottom: 70px;">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-center">Ваши балансы
                <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a></h1>
            <!-- Large modal -->

            <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <?php
                            $id = 7;
                            $page = \common\models\LotPage::findOne($id);

                            $pageLang = \common\models\LotPageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                            if (is_null($pageLang)) {
                                if (is_null($page)) {
                                    $name = '';
                                    $content = '';
                                } else {
                                    $name = $page->name;
                                    $content = $page->content;
                                }
                            } else {
                                $name = $pageLang->name;
                                $content = $pageLang->content;
                            }
                            ?>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><?= $name ?></h4>
                        </div>
                        <div class="modal-body">
                            <?= $content ?>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $this->registerJs(<<<JS
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
            );
            ?>


            <?php
            $this->registerJs(<<<JS
$('[data-toggle="popover"]').popover({
    html:true,
    placement:'bottom',
});
JS
)
            ?>



            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query' => UserBill::find()
                        ->innerJoin('currency', 'currency.id = user_bill.currency')
                        ->where([
                            'user_bill.user_id'      => Yii::$app->user->id,
                            'user_bill.mark_deleted' => 0,
                            'user_bill.currency'     => [
                                Currency::LOT,
                            ],
                        ])
                        ->select(['user_bill.*'])
                        ->orderBy(['currency.sort_index' => SORT_ASC])
                        ,
                    'pagination' => [
                        'pageSize' => 100,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-hover table-striped',
                    'id'    => 'tableTransaction',
                ],
                'summary' => '',
                'rowOptions'   => function (UserBill $item) {
                    $data = [
                        'data'  => [
                            'id'             => $item['id'],
                            'currency'       => $item['currency'],
                            'currencyObject' => \yii\helpers\ArrayHelper::toArray(Currency::findOne($item['currency'])),
                        ],
                        'role'  => 'button',
                        'class' => 'rowTable',
                    ];

                    return $data;
                },
                'columns'      => [
                    [
                        'header'  => '',
                        'headerOptions'  => [
                            'style' => 'width: 5%;'
                        ],
                        'content' => function ($item) {
                            return Html::img('/images/controller/cabinet-bills/index/wallet.png', ['width' => 30, 'style' => 'margin-top: 13px;']);
                        },
                    ],
                    [
                        'header'        => '',
                        'headerOptions' => [
                            'style' => 'text-align: center;width: 10%;',
                        ],
                        'content'       => function ($item) {
                            $c = Currency::findOne($item['currency']);

                            $imgOptions = [
                                'width' => 50,
                                'class' => 'img-circle',
                                'data'  => [
                                    'toggle' => 'tooltip',
                                    'title'  => $c->code,
                                ],
                            ];

                            return Html::img($c->image, $imgOptions);
                        },
                    ],
                    [
                        'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                        'headerOptions'  => [
                            'style' => 'width: 10%;',
                        ],
                        'content' => function ($item) {

                            $html = [];

                            /** @var UserBill $u */
                            $u = $item;
                            $w = \common\models\piramida\Wallet::findOne($u->address);
                            if (is_null($w)) {
                                \cs\services\VarDumper::dump($item);
                            }
                            $shortAddress = $w->getAddressShort();
                            $fullAddress = $w->getAddress();
                            $html[] = Html::tag(
                                'span',
                                $shortAddress,
                                [
                                    'class' => 'js-buttonTransactionInfo textDecorated',
                                    'style' => 'font-family: "Courier New", Courier, monospace;',
                                    'role'  => 'button',
                                    'data' => [
                                        'placement'      => 'bottom',
                                        'clipboard-text' => $fullAddress,
                                    ],
                                ]
                            );

                            return join('<br>', $html);
                        },
                    ],
                    [
                        'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                        'headerOptions'  => [
                            'style' => 'width: 15%;',
                        ],
                        'attribute' => 'name',
                    ],
                    [
                        'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                        'headerOptions'  => [
                            'style' => 'text-align: right;width: 30%;'
                        ],
                        'contentOptions' => function ($item) {
                            return [
                                'class' => 'rowBill',
                                'id'    => 'bill_confirmed_' . $item['id'],
                                'data'  => ['id' => $item['id']],
                                'style' => 'text-align: right;',
                            ];
                        },
                        'content'        => function (UserBill $item) {
                            $a = $item->address;
                            $w = \common\models\piramida\Wallet::findOne($a);
                            $c = \common\models\piramida\Currency::findOne($w->currency_id);
                            $v = $w->amount / 100;

                            $html = [];
                            $html[] = Html::tag('code', Yii::$app->formatter->asDecimal($v, 2), ['class' => 'balance1']) . Html::tag('span', $c->code, ['class' => 'label label-info']);
                            if ($item->currency != Currency::USDT) {
                                $CIO = \common\models\CurrencyIO::findOne(['currency_int_id' => $c->id]);
                                $cExt = Currency::findOne($CIO->currency_ext_id);
                                $html[] = '~ ' . Yii::$app->formatter->asDecimal($v * $cExt->price_usd, 2) . ' USDT';
                            }
                            return join('<br>', $html);
                        },
                    ],
                    [
                        'header'  => Yii::t('c.TsqnzVaJuC', 'Отправить'),
                        'headerOptions'  => [
                            'style' => 'width: 10%;'
                        ],
                        'content' => function ($item) {
                            $options = [
                                'class' => ['btn', 'btn-info'],
                                'data'  => [
                                    'id'       => $item['id'],
                                    'currency' => $item['currency'],
                                ],
                            ];

                            /** @var \common\models\UserAvatar $user */
                            $user = Yii::$app->user->identity;
                            if (!$user->wallets_is_locked) {
                                $options['class'][] = 'buttonThisProcessing';

                                if (is_array($options['class'])) {
                                    $options['class'] = join(' ', $options['class']);
                                }

                                return Html::button(Yii::t('c.TsqnzVaJuC', 'Отправить'), $options);
                            }
                        },
                    ],
                    [
                        'header'  => Yii::t('c.TsqnzVaJuC', 'Пополнить'),
                        'headerOptions'  => [
                            'style' => 'width: 10%;'
                        ],
                        'content' => function ($item) {

                            return '';
                        },
                    ],
                    [
                        'header'  => Yii::t('c.TsqnzVaJuC', 'Вывести'),
                        'headerOptions'  => [
                            'style' => 'width: 10%;'
                        ],
                        'content' => function ($item) {
                            return '';
                        },
                    ],
                    [
                        'header'        => 'Конвертировать',
                        'headerOptions' => [
                            'style' => 'width: 10%;',
                        ],
                        'content'       => function ($item) {
                            return '';
                        },
                    ],
                ],
            ]) ?>
        </div>
    </div>
</div>