<?php

/** @var $this \yii\web\View  */
/** @var $model \lot\models\forms\AdminReport  */

use common\models\ConvertOperation;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Отчет';



\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


$str838 = Yii::t('c.BBXvjwPu8f', 'Скопировано');

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str838}'
    }).show();

});

JS
);
$cLot = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LOT);
?>

<div style="margin: 0px 50px 0px 50px;">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-6">
                <?php $form = \yii\bootstrap\ActiveForm::begin([

                ]) ?>
                <?= $form->field($model, 'date_start')->widget('\yii\jui\DatePicker', ['options' => ['class' => 'form-control']]) ?>
                <?= $form->field($model, 'date_end')->widget('\yii\jui\DatePicker', ['options' => ['class' => 'form-control']]) ?>
                <hr>
                <?= Html::submitButton('Показать', [
                        'class' => 'btn btn-success',
                        'style' => 'width: 100%',
                ]) ?>

                <?php \yii\bootstrap\ActiveForm::end() ?>
                <p>
                    первая дата считается от 00:00:00 включительно<br>
                    вторая дата учитывается до начала следующего дня (00:00:00)<br>
                    фильтруется время покупки билета
                </p>

            </div>
            <div class="col-lg-6">
                <h3>Покупка билетов</h3>
                <?php
                $date_start = null;
                $date_end = null;
                if (\yii\helpers\StringHelper::startsWith(Yii::$app->formatter->dateFormat, 'php')) {
                    $dateFormat = substr(Yii::$app->formatter->dateFormat, 3);
                } else {
                    $dateFormat = \yii\helpers\FormatConverter::convertDateIcuToPhp(Yii::$app->formatter->dateFormat);
                }
                if (!\cs\Application::isEmpty($model->date_start)) {

                    $d = DateTime::createFromFormat($dateFormat . ' H:i:s', $model->date_start . ' 00:00:00');
                    $date_start = $d->format('U');
                }
                if (!\cs\Application::isEmpty($model->date_end)) {
                    $d = DateTime::createFromFormat($dateFormat . ' H:i:s', $model->date_end . ' 00:00:00');
                    $d->add(new DateInterval('P1D'));
                    $date_end = $d->format('U');
                }


                $rowsQuery = \common\models\Bilet::find()
                    ->select([
                        'max(bilet.id) as id',
                        'count(bilet.id) as c1',
                        'bilet.lot_id',
                        'lot.bilet_count',
                        'lot.price',
                        'lot.prize_amount',
                        'sum(bilet.is_cash_back) as sum_is_cash_back',
                        'sum(bilet.cash_bask_result) as sum_cash_bask_result',
                        'lot.cash_back_percent',
                    ])
                    ->where(['not', ['user_id' => Yii::$app->params['AUTO_REDEMPTION_USER_ID']]])
                    ->andWhere(['not', ['lot.is_hide' => 1]])
                    ->innerJoin('lot', 'lot.id = bilet.lot_id')
                    ->groupBy(['bilet.lot_id'])
                    ->asArray()
                ;
                if (!is_null($date_start)) {
                    $rowsQuery->andWhere(['>=', 'lot.time_start', $date_start]);
                }
                if (!is_null($date_end)) {
                    $rowsQuery->andWhere(['<', 'lot.time_start', $date_end]);
                }

                $rowsQuery2 = \common\models\Bilet::find()
                    ->select([
                        'max(bilet.id) as id',
                        'count(bilet.id) as c1',
                        'bilet.lot_id',
                        'lot.bilet_count',
                        'lot.price',
                        'lot.prize_amount',
                        'sum(bilet.is_cash_back) as sum_is_cash_back',
                        'sum(bilet.cash_bask_result) as sum_cash_bask_result',
                        'lot.cash_back_percent',
                    ])
                    ->where(['not', ['user_id' => Yii::$app->params['AUTO_REDEMPTION_USER_ID']]])
                    ->andWhere(['not', ['lot.is_hide' => 1]])
                    ->innerJoin('lot', 'lot.id = bilet.lot_id')
                    ->groupBy(['bilet.lot_id'])
                    ->asArray()
                ;
                if (!is_null($date_start)) {
                    $rowsQuery2->andWhere(['>=', 'lot.time_start', $date_start]);
                }
                if (!is_null($date_end)) {
                    $rowsQuery2->andWhere(['<', 'lot.time_start', $date_end]);
                }
                $c1 = 0;
                $c2 = 0;
                $c3 = 0;
                $c4 = 0;
                $c5 = 0;
                $c6 = 0;
                foreach ($rowsQuery2->all() as $item) {
                    $c1++;
                    $c2 += $item['c1'];
                    $c3 += $item['price'] * $item['c1'];
                    $c4 += $item['prize_amount'];
                    $c5 += $item['sum_cash_bask_result'] * (($item['cash_back_percent']/100) * $item['price']);

                    $viruchka = $item['price'] * $item['c1'];
                    $prize_amount = $item['prize_amount'];
                    $cash_back = $item['sum_cash_bask_result'] * (($item['cash_back_percent']/100) * $item['price']);
                    $s = $viruchka - $prize_amount - $cash_back;

                    $c6 += $s;
                }

                ?>
                <p>а) Общее количество розыгрышей: <?= $c1 ?></p>

                <p>б) Общее количество проданных билетов: <?= $c2 ?> шт</p>

                <p>в) Общая выручка: <?= Yii::$app->formatter->asDecimal(
                        \common\models\piramida\Currency::getValueFromAtom($c3, \common\models\piramida\Currency::LOT)
                    ) ?> LOT</p>

                <p>г) Общая сумма всех призов: <?= Yii::$app->formatter->asDecimal(
                        \common\models\piramida\Currency::getValueFromAtom($c4, \common\models\piramida\Currency::LOT)
                    ) ?> LOT</p>

                <p>д) Общая сумма всех кэшбеков: <?= Yii::$app->formatter->asDecimal(
                        \common\models\piramida\Currency::getValueFromAtom($c5, \common\models\piramida\Currency::LOT)
                    ) ?> LOT</p>

                <p>е) Общая прибыль за период: <?= Yii::$app->formatter->asDecimal(
                        \common\models\piramida\Currency::getValueFromAtom($c6, \common\models\piramida\Currency::LOT)
                    ) ?> LOT</p>
                <h3>Конвертация в LOT</h3>
                <?php

                $query2 = ConvertOperation::find()
                    ->select([
                        'to_currency_id as id',
                        'sum(amount_to) as sum_amount_to',
                    ])
                    ->where(['to_currency_id' => \common\models\piramida\Currency::LOT])
                    ->groupBy(['to_currency_id'])
                    ->asArray()
                ;

                if (!is_null($date_start)) {
                    $query2->andWhere(['>=', 'created_at', $date_start]);
                }
                if (!is_null($date_end)) {
                    $query2->andWhere(['<', 'created_at', $date_end]);
                }
                $c7 = $query2->one();
                $c7 = $c7['sum_amount_to'];


                $query3 = \common\models\BinanceOrder::find()
                    ->select([
                        'currency_to as id',
                        'sum(amount_to) as sum_amount_to',
                    ])
                    ->where(['currency_to' => \common\models\avatar\Currency::LOT])
                    ->groupBy(['currency_to'])
                    ->asArray()
                ;

                if (!is_null($date_start)) {
                    $query3->andWhere(['>=', 'created_at', $date_start]);
                }
                if (!is_null($date_end)) {
                    $query3->andWhere(['<', 'created_at', $date_end]);
                }
                $c8 = $query3->one();
                if (!is_null($c8)) {
                    $c8 = $c8['sum_amount_to'] + $c7;
                } else {
                    $c8 = $c7;
                }
                ?>

                <p>ИТОГО получено LOT: <?= Yii::$app->formatter->asDecimal(
                        \common\models\piramida\Currency::getValueFromAtom($c8, \common\models\piramida\Currency::LOT), $cLot->decimals
                    ) ?> LOT</p>

            </div>
        </div>
    <div class="row">
        <div class="col-lg-6">
            <h2 class="page-header">Покупка билетов</h2>

            <?php \yii\widgets\Pjax::begin(); ?>

            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => $rowsQuery
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                    'sort' => ['defaultOrder' => ['lot_id' => SORT_DESC]]
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'columns' => [
                    [
                        'header'  => 'lot_id',
                        'content' => function ($item) {
                            return Html::a($item['lot_id'], ['admin-lot/view', 'id' => $item['lot_id']], ['data' => ['pjax' => 0]]);
                        },
                    ],
                    'c1:text:Билетов продано',
                    'bilet_count',
                    [
                        'header'  => 'Стоимость билета',
                        'content' => function ($item) {
                            return Yii::$app->formatter->asDecimal(
                                \common\models\piramida\Currency::getValueFromAtom($item['price'], \common\models\piramida\Currency::LOT)
                            );
                        },
                    ],

                    [
                        'header'         => 'Выручка',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($item) {
                            return Yii::$app->formatter->asDecimal(
                                \common\models\piramida\Currency::getValueFromAtom($item['price'] * $item['c1'], \common\models\piramida\Currency::LOT)
                            );
                        },
                    ],
                    [
                        'header'         => 'Стоимость Главного Приза',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($item) {
                            if (\cs\Application::isEmpty($item['prize_amount'])) return '';
                            return Yii::$app->formatter->asDecimal(
                                \common\models\piramida\Currency::getValueFromAtom($item['prize_amount'], \common\models\piramida\Currency::LOT)
                            );
                        },
                    ],
                    [
                        'header'         => 'Кэшбек',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($item) {
                            return $item['sum_cash_bask_result'] * (($item['cash_back_percent']/100) * $item['price']);
                        },
                    ],
                    [
                        'header'         => 'Прибыль',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($item) {
                            // Прибыль = Выручка - Стоимость Главного Приза - Кэшбек
                            $viruchka = $item['price'] * $item['c1'];
                            $prize_amount = $item['prize_amount'];
                            $cash_back = $item['sum_cash_bask_result'] * (($item['cash_back_percent']/100) * $item['price']);
                            $s = $viruchka - $prize_amount - $cash_back;

                            return Yii::$app->formatter->asDecimal(
                                \common\models\piramida\Currency::getValueFromAtom($s, \common\models\piramida\Currency::LOT)
                            );
                        },
                    ],
                ]

            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
        <div class="col-lg-6">
            <h2 class="page-header">Конвертация в LOT</h2>


            <?php \yii\widgets\Pjax::begin(); ?>

            <?php

            $query2 = ConvertOperation::find()
                ->select([
                    'from_currency_id as id',
                    'sum(amount) as sum_amount',
                    'sum(amount_to) as sum_amount_to',
                    'avg(kurs) as avg_kurs',
                ])
                ->where(['to_currency_id' => \common\models\piramida\Currency::LOT])
                ->groupBy(['from_currency_id'])
                ->asArray()
            ;

            if (!is_null($date_start)) {
                $query2->andWhere(['>=', 'created_at', $date_start]);
            }
            if (!is_null($date_end)) {
                $query2->andWhere(['<', 'created_at', $date_end]);
            }
            ?>

            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => $query2
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'columns' => [
                    [
                        'header'  => 'Конвертация из',
                        'content' => function ($item) {
                            $cInt = \common\models\piramida\Currency::findOne($item['id']);
                            if (is_null($cInt)) return '';

                            return Html::tag('span', $cInt->code, [
                                'class' => "label label-info",
                            ]);
                        },
                    ],
                    [
                        'header'  => 'Общая сумма',
                        'content' => function($i) {
                            $cInt = \common\models\piramida\Currency::findOne($i['id']);
                            return
                                Yii::$app->formatter->asDecimal(
                                    \common\models\piramida\Currency::getValueFromAtom($i['sum_amount'], $i['id']),
                                    $cInt->decimals
                                );
                        }
                    ],
                    [
                        'header'         => 'Получено LOT',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($i) {
                            return Yii::$app->formatter->asDecimal($i['sum_amount_to'] / 100, 2);
                        }
                    ],
                    [
                        'header'  => 'курс ср',
                        'content' => function($i) {
                            return $i['avg_kurs'] / 1000000;
                        }
                    ],
                ]

            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>

            <?php \yii\widgets\Pjax::begin(); ?>

            <?php

            $query2 = \common\models\BinanceOrder::find()
                ->select([
                    'user_bill.currency as id',
                    'sum(amount) as sum_amount',
                    'sum(amount_to) as sum_amount_to',
                ])
                ->innerJoin('user_bill', 'user_bill.id = binance_order.billing_id')
                ->where(['currency_to' => \common\models\avatar\Currency::LOT])
                ->groupBy(['user_bill.currency'])
                ->asArray()
            ;

            if (!is_null($date_start)) {
                $query2->andWhere(['>=', 'binance_order.created_at', $date_start]);
            }
            if (!is_null($date_end)) {
                $query2->andWhere(['<', 'binance_order.created_at', $date_end]);
            }
            ?>

            <?= \yii\grid\GridView::widget([
                'dataProvider' => new \yii\data\ActiveDataProvider([
                    'query'      => $query2
                    ,
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]),
                'tableOptions' => [
                    'class' => 'table table-striped table-hover',
                ],
                'columns' => [
                    [
                        'header'  => 'Конвертация из',
                        'content' => function ($item) {
                            $cInt = \common\models\avatar\Currency::findOne($item['id']);
                            if (is_null($cInt)) return '';

                            return Html::tag('span', $cInt->code, [
                                'class' => "label label-info",
                            ]);
                        },
                    ],
                    [
                        'header'  => 'Общая сумма',
                        'content' => function($i) {
                            $cext = \common\models\avatar\Currency::findOne($i['id']);
                            $cio = \common\models\CurrencyIO::findFromExt($i['id']);
                            return
                                Yii::$app->formatter->asDecimal(
                                    \common\models\piramida\Currency::getValueFromAtom($i['sum_amount'], $cio->currency_int_id),
                                    $cext->decimals
                                );
                        }
                    ],
                    [
                        'header'         => 'Получено LOT',
                        'headerOptions'  => ['class' => 'text-right'],
                        'contentOptions' => ['class' => 'text-right'],
                        'content'        => function ($i) {
                            return Yii::$app->formatter->asDecimal($i['sum_amount_to'] / 100, 2);
                        }
                    ],
                    [
                        'header'  => 'курс ср',
                        'content' => function($i) {
                            $cio = \common\models\CurrencyIO::findFromExt($i['id']);
                            return ($i['sum_amount_to'] / 100) /
                                \common\models\piramida\Currency::getValueFromAtom($i['sum_amount'], $cio->currency_int_id);
                        }
                    ],
                ]

            ]) ?>
            <?php \yii\widgets\Pjax::end(); ?>
        </div>
    </div>




    </div>
