<?php
use yii\helpers\Url;
?>

<li><a href="/cabinet/index">ПРОФИЛЬ</a></li>
<li><a href="/cabinet-bills/index">МОИ БАЛАНСЫ</a></li>
<li><a href="/cabinet-shop-requests/index">МОИ БИЛЕТЫ (архив)</a></li>
<li><a href="/page/item?id=6">КАК ПОПОЛНИТЬ БАЛАНС</a></li>
<li><a href="/page/help">ИНСТРУКЦИИ</a></li>
<?php if (Yii::$app->user->can('permission_lot_admin')) { ?>
    <li><a href="/admin/index">Админ</a></li>
<?php } ?>
<li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post">ВЫЙТИ</a></li>
