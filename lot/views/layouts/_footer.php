<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 21.01.2017
 * Time: 18:16
 */

/** @var \avatar\services\CustomizeService $CustomizeService */
/** @var array $companySocialNets telegram, facebook, vk, skype, youtube */

/**
 * Last Update.
 * User: slpv
 * Date: 12.12.2019
 * Time: 13:29
 */
\avatar\assets\Notify::register($this);

?>

</div>


<style>
    .white {color: #fff;}
</style>

<?php if (Yii::$app->deviceDetect->isMobile()) { ?>
    <footer>
        <div class="sections">
            <div class="section">
                <div class="col-lg-3">
                    <p><a href="/page/item?id=2" class="white">О лотереях</a></p>
                    <p><a href="/page/item?id=11" class="white">Способы оплаты</a></p>
                    <p><a href="/page/item?id=12" class="white">Как получить выигрыш</a></p>
                </div>
                <div class="col-lg-3">
                    <p><a href="/page/item?id=3" class="white">Условия использования сервиса</a></p>
                    <p><a href="/page/item?id=4" class="white">Политика конфиденциальности</a></p>
                    <p><a href="/page/item?id=5" class="white">Пользовательское соглашение</a></p>
                </div>
                <div class="col-lg-3">
                    <p><a href="/page/item?id=13" class="white">Дисклеймер</a></p>
                    <p><a href="/page/item?id=14" class="white">О нас</a></p>
                    <p><a href="/page/item?id=8" class="white">Контакты</a></p>
                </div>
                <div class="col-lg-3">
                    <p><a href="https://www.instagram.com/_e_lottery" class="white" target="_blank">Instagram</a></p>
                    <p><a href="https://t.me/+AXKNBvN9lXo3OTMy" class="white" target="_blank">Telegram</a></p>
                    <p><a href="//www.youtube.com/channel/UCWxO14LiwEt9NN84sKBRyUg" class="white" target="_blank">YouTube</a></p>
                </div>


            </div>

            <div class="col-lg-12">
                <div class="title">© Все права защищены. elottery.cloud 2021</div>

            </div>
        </div>
    </footer>
<?php } else { ?>
    <footer>
        <div class="sections">
            <div class="section">
                <table style="margin-top: 20px;">
                    <tr>
                        <td align="left" valign="top" style="padding-right: 30px;">
                            <p><a href="/page/item?id=2" class="white">О лотереях</a></p>
                            <p><a href="/page/item?id=11" class="white">Способы оплаты</a></p>
                            <p><a href="/page/item?id=12" class="white">Как получить выигрыш</a></p>
                        </td>
                        <td align="left" valign="top" style="padding-right: 30px;">
                            <p><a href="/page/item?id=3" class="white">Условия использования сервиса</a></p>
                            <p><a href="/page/item?id=4" class="white">Политика конфиденциальности</a></p>
                            <p><a href="/page/item?id=5" class="white">Пользовательское соглашение</a></p>
                        </td>
                        <td align="left" valign="top" style="padding-right: 30px;">
                            <p><a href="/page/item?id=13" class="white">Дисклеймер</a></p>
                            <p><a href="/page/item?id=14" class="white">О нас</a></p>
                            <p><a href="/page/item?id=8" class="white">Контакты</a></p>
                        </td>

                        <td align="left" valign="top" style="padding-right: 30px;">
                            <p><a href="https://www.instagram.com/_e_lottery" class="white" target="_blank">Instagram</a></p>
                            <p><a href="https://t.me/+AXKNBvN9lXo3OTMy" class="white" target="_blank">Telegram</a></p>
                            <p><a href="//www.youtube.com/channel/UCWxO14LiwEt9NN84sKBRyUg" class="white" target="_blank">YouTube</a></p>
                        </td>

                    </tr>
                </table>

                <div class="title">© Все права защищены. elottery.cloud 2021</div>

            </div>
        </div>
    </footer>

<?php }  ?>
