<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 01.11.2016
 * Time: 9:42
 */

/** @var $this \yii\web\View */
/** @var $content */

\lot\assets\App\Asset::register($this);
\avatar\assets\LayoutMenu\Asset::register($this);
\avatar\assets\FontAwesome::register($this);
\avatar\assets\Prizmology\Asset::register($this);
\avatar\assets\BootstrapSroutes::register($this);

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $this->title ?></title>
    <link rel="shortcut icon" href="/images/e-Lottery1.png">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody(); ?>
<?= $this->render('top') ?>

<?= $content ?>
<?php if (\Yii::$app->user->isGuest) : ?>
    <?= $this->render('_modalLogin') ?>
<?php endif; ?>

<?= $this->render('_footer') ?>

<?php if (Yii::$app->controller->layout != 'cabinet') { ?>
    <?= $this->render('_counters') ?>
<?php } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>