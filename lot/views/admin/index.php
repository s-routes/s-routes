<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Админка';

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <h2 class="page-header">Лотерея</h2>
        <p><a class="btn btn-default" href="/admin-report/index">Отчет</a></p>
        <p><a class="btn btn-default" href="/admin-lot-auto/edit">Авто лотерея</a></p>
        <p><a class="btn btn-default" href="/admin-lot/index">Все лотереи</a></p>
        <p><a class="btn btn-default" href="/admin-page/index">Все страницы</a></p>
        <p><a class="btn btn-default" href="/admin-lot-insrukciya/index">Инструкции</a></p>
        <p><a class="btn btn-default" href="/admin-support/index">Служба поддержки</a></p>

    </div>
</div>