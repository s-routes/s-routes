<?php

use common\models\piramida\Currency;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \common\models\Lot */

\avatar\assets\Notify::register($this);
\avatar\assets\SocketIO\Asset::register($this);

$this->title = $model->id;
$c = Currency::findOne(Currency::LOT);

$cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

$serverName = \avatar\assets\SocketIO\Asset::getHost();

if (is_null($model->room_id)) {
    $room = \common\models\ChatRoom::add([]);
    $model->room_id = $room->id;
    $room_id = $room->id;
    $model->save();
} else {
    $room_id = $model->room_id;
}

Yii::$app->cache->set('/lot/views/admin-lot/view.php:$lot', $model);
$uid = Yii::$app->user->id;

$this->registerJs(<<<JS

var socket = io.connect('{$serverName}');
var roomName = 'room' + {$room_id};

var myid2 = {$uid};
socket.emit('chat1-new-user', roomName, myid2);

socket.on('chat1-counter-start', function(data) {
    console.log(['chat1-counter-start', data]);
});

$('.buttonWin').click(function(ret) {
    ajaxJson({
        url: '/admin-lot/win',
        data: {
            id: $(this).data('id')
        },
        success: function(ret) {
            
            var support_room_id = ret.support_room_id;
            var supportRoomName = 'room' + support_room_id;
            
            socket.emit('chat1-counter-win', roomName, {$model->id}, ret.ticket);
            
            // отправляю сообщение на сервер в чат поддержки
            socket.emit('chat-message2', supportRoomName, myid2, ret.chat);
            
            new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Успешно'
            }).show();
            
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location.reload();
            }).modal();
        }
    });
});
JS
)
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $model,
            'attributes' => [
                [
                    'label'  => 'Номер разыгрываемой лотереи',
                    'value'  => $model['id'],
                ],
                [
                    'label'  => 'Цена билета',
                    'format' => 'html',
                    'value'  => Currency::getValueFromAtom($model['price'], Currency::LOT) . $cHtml,
                ],
                'time_start:datetime:Время начала продаж',
                'time_finish:datetime:Время окончания продаж',
                'time_raffle:datetime:Время розыгрыша',
            ],
        ]) ?>
        <?php if ($model['time_raffle'] < time()) { ?>
            <p class="label label-danger">Разыграна</p>
        <?php } ?>
        <?php
        // кол-во купленных билетов
        $bilet_pay_count = \common\models\Bilet::find()->where(['lot_id' => $model['id']])->count();

        // кол-во всех билетов
        $bilet_count = $model['bilet_count'];

        $percent = (int)($bilet_pay_count / $bilet_count * 100);
        ?>
        <p>Кол-во купленных билетов: <?= $bilet_pay_count ?>/<?= $bilet_count ?></p>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<?= $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent ?>%;">
                <span class="sr-only"><?= $percent ?>% Complete</span>
            </div>
        </div>
        <h2>Купленные билеты</h2>
        <?php
        $columns = [
            'id',
            'tx_id',
            [
                'header'  => 'Пользователь',
                'content' => function ($item) {
                    $lot = \common\models\Lot::findOne($item['lot_id']);
                    $user = \common\models\UserAvatar::findOne($item['user_id']);

                    return Html::img($user->getAvatar(), ['class' => 'thumbnail', 'width' => 70, 'style' => 'margin-bottom: 0px;', 'data' => ['toggle' => 'tooltip'], 'title' => $user->getName2()]);
                },
            ],
            [
                'header'  => 'User ID',
                'content' => function ($item) {
                    $user = \common\models\UserAvatar::findOne($item['user_id']);

                    return $user->getAddress();
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            'num:text:Идентификатор',
            [
                'header'  => 'Номер билета',
                'content' => function ($item) {
                    if (is_null($item['num'])) return '';
                    $num = substr($item['num'], 6);

                    return intval($num);
                },
            ],
            [
                'header'  => 'Пакет компенсации',
                'content' => function ($item) {
                    if (!is_null($item['is_cash_back'])) {
                        if ($item['is_cash_back'] == 1) return Html::tag('span', 'Есть', ['class' => 'label label-success']);
                    }

                    return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                },
            ],
        ];
        if (($model->is_counter_start == 1) && ($model->is_counter_stop == 0)) {
            $columns[] = [
                'header'  => 'Победитель',
                'content' => function ($item) {
                    return Html::button('Победитель', [
                        'class' => 'btn btn-info btn-xs buttonWin',
                        'data'  => [
                            'id' => $item['id'],
                        ]
                    ]);
                }
            ];
        }
        if (($model->is_counter_start == 1) && ($model->is_counter_stop == 1)) {
            $columns[] = [
                'header'  => '',
                'content' => function ($item) {
                    $lot = Yii::$app->cache->get('/lot/views/admin-lot/view.php:$lot');
                    $html = [];

                    if ($lot['winner_id'] == $item['id']) {
                        $html[] =  Html::tag('span', 'winner', ['class' => 'label label-info']);
                        if ($lot['is_prize_payd'] == 1) {
                            $html[] =  '<br>';
                            $html[] =  'Произведена выплата ' .  Yii::$app->formatter->asDecimal(Currency::getValueFromAtom($lot->prize_amount, Currency::LOT), 2) . ' LOT';
                        }
                    }


                    return join('', $html);

                },
            ];
        }
        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Bilet::find()
                ->where(['lot_id' => $model->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['created_at' => SORT_ASC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => $columns,
        ]) ?>

        <?php if ($model->type == \common\models\Lot::TYPE_AUTO) { ?>


            <?php if ($model->is_counter_start == 1 && $model->is_counter_stop == 0) { ?>
                <p>Счетчик запущен</p>
            <?php } ?>
            <?php if ($model->is_counter_start == 1 && $model->is_counter_stop == 1) { ?>
                <h2>Победитель</h2>
                <?php
                $bid = $model['winner_id'];
                $bilet = \common\models\Bilet::findOne($bid);
                $user = \common\models\UserAvatar::findOne($bilet->user_id);
                $num = substr($bilet['num'], 6);
                ?>
                <p><img src="<?= $user->getAvatar() ?>" width="50" class="img-circle"> <?= $user->getAddress() ?> <?= $user->getName2() ?> Идентификатор <?= $bilet['num'] ?> Билет №<?= $num ?></p>

                <?php if ($model->prize_type == \common\models\Lot::PRIZE_TYPE_THING) { ?>
                    <?php if ($model->is_prize_payd == 0) { ?>
                        <?php
                        $this->registerJs(<<<JS
$('.buttonLot').click(function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/admin-lot/do-lot',
            data: {id: {$model->id}},
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
JS
)
                        ?>
                        <p><button class="btn btn-default buttonLot">Выдать в LOT</button></p>
                    <?php } else { ?>
                        <p>Произведена выплата <?= Yii::$app->formatter->asDecimal(Currency::getValueFromAtom($model->prize_amount, Currency::LOT), 2) ?> LOT</p>
                    <?php } ?>
                <?php } ?>
                <p><a href="/admin-support/chat?id=<?= $user->id ?>" class="btn btn-primary">Чат с победителем</a></p>

            <?php } ?>

        <?php } ?>



        <?php if ($model->type == \common\models\Lot::TYPE_MANUAL) { ?>


            <?php if (($model->is_counter_start == 1) && ($model->is_counter_stop == 0)) { ?>




            <?php } ?>

            <?php if (($model->is_counter_start == 1) && ($model->is_counter_stop == 1)) { ?>
                <h2>Победитель</h2>
                <?php
                $bid = $model['winner_id'];
                $bilet = \common\models\Bilet::findOne($bid);
                $user = \common\models\UserAvatar::findOne($bilet->user_id);
                $num = substr($bilet['num'], 6);
                ?>
                <p><img src="<?= $user->getAvatar() ?>" width="50" class="img-circle"> <?= $user->getAddress() ?> <?= $user->getName2() ?> Идентификатор <?= $bilet['num'] ?> Билет №<?= $num ?></p>

                <?php if ($model->prize_type == \common\models\Lot::PRIZE_TYPE_THING) { ?>
                    <?php if ($model->is_prize_payd == 0) { ?>
                        <?php
                        $this->registerJs(<<<JS
$('.buttonLot').click(function(e) {
    if (confirm('Вы уверены?')) {
        ajaxJson({
            url: '/admin-lot/do-lot',
            data: {id: {$model->id}},
            success: function(ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    } 
});
JS
                        )
                        ?>
                        <p><button class="btn btn-default buttonLot">Выдать в LOT</button></p>
                    <?php } else { ?>
                        <p>Произведена выплата <?= Yii::$app->formatter->asDecimal(Currency::getValueFromAtom($model->prize_amount, Currency::LOT), 2) ?> LOT</p>
                    <?php } ?>
                <?php } ?>
                <p><a href="/admin-support/chat?id=<?= $user->id ?>" class="btn btn-primary">Чат с победителем</a></p>
            <?php } ?>

        <?php } ?>






    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


