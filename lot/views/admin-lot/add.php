<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Lot */

$this->title = 'Добавить лот';
\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});
$('#lot-time_type').on('change', function(e) {
    if ($('#lot-time_type').is(':checked')) {
        $('.js-time_type0').collapse('hide');
        $('.js-time_type1').collapse('show');
    } else {
        $('.js-time_type0').collapse('show');
        $('.js-time_type1').collapse('hide');
    }
});
JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model' => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/admin-lot/index';
    }).modal();
}
JS
            ,
        ]);

        ?>
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'type')->dropDownList([
            \common\models\Lot::TYPE_AUTO   => 'автоматизированный',
            \common\models\Lot::TYPE_MANUAL => 'онлайн трансляция',
        ]) ?>
        <?= $form->field($model, 'prize_type')->dropDownList([
            \common\models\Lot::TYPE_AUTO   => 'Денежный',
            \common\models\Lot::TYPE_MANUAL => 'Вещевой',
        ]) ?>
        <?= $form->field($model, 'prize_amount') ?>
        <?= $form->field($model, 'prize_name') ?>
        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'bilet_count') ?>
        <?= $form->field($model, 'cash_back_price') ?>
        <?= $form->field($model, 'cash_back_percent') ?>
        <?= $form->field($model, 'time_type') ?>
        <?= $form->field($model, 'time_start')->widget('\kartik\datetime\DateTimePicker') ?>

        <div class="collapse in js-time_type0">
            <?= $form->field($model, 'time_finish')->widget('\kartik\datetime\DateTimePicker') ?>
            <?= $form->field($model, 'time_raffle')->widget('\kartik\datetime\DateTimePicker') ?>
        </div>

        <div class="collapse js-time_type1">
            <?= $form->field($model, 'time_finish_add') ?>
            <?= $form->field($model, 'time_raffle_add') ?>
        </div>

        <?= $form->field($model, 'image') ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 10]) ?>
        <?= $form->field($model, 'link_youtube') ?>
        <?= $form->field($model, 'link_instagram') ?>
        <?= $form->field($model, 'link_telegram') ?>
        <?= $form->field($model, 'content') ?>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end([]); ?>
        <hr>
        <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
            'id'        => 'upload' . 1,
            'name'      => 'upload' . 1,
            'attribute' => 'upload' . 1,
            'model'     => new \avatar\models\forms\ChatFile(),
            'update'    => [],
            'settings'  => [
                'maxSize'           => 50 * 1000,
                'controller'        => 'upload4',
                'button_label'      => Yii::t('c.xgKKA2JpVp', 'Прикрепите файл'),
                'functionSuccess'   => new \yii\web\JsExpression(<<<JS
function (response) {
    // response.url
    $('.buttonCopy').removeClass('hide');
    $('.buttonCopy').attr('data-clipboard-text', response.url);
    return false;
}
JS
                ),
            ],
        ]); ?>
        <button class="btn btn-default btn-xs hide buttonCopy" role="">Копировать</button>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>