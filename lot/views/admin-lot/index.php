<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все лотереи';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <p><a href="<?= Url::to(['add']) ?>" class="btn btn-default">Добавить</a> Админ, не забудь проверить почту elcloud@openmail.cc здесь https://www.vfemail.net</p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите удаление')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-lot/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
                $('#contact-form').serializeArray()
            }
        });
    }
});

$('.buttonShow').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите действие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-lot/show',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
$('.buttonCancel').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите действие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-lot/cancel',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});


$('.buttonHide').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите действие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-lot/hide',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});

$('.buttonCopy').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Подтвердите действие')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/admin-lot/copy',
            data: {id: id},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});


$('.rowTable').click(function() {
    window.location = '/admin-lot/edit' + '?' + 'id' + '=' + $(this).data('id');
});

$('.buttonView').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    window.location = '/admin-lot/view' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Lot::find()
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Лот',
                    'content' => function ($item) {
                        return Html::img(\iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop'), ['class' => 'thumbnail', 'width' => 70, 'style' => 'margin-bottom: 0px;', 'data' => ['toggle' => 'tooltip'], 'title' => $item->name]);
                    },
                ],
                'name',
                'bilet_count:text:Билетов',
                [
                    'header'  => 'Цена билета',
                    'content' => function ($item) {
                        return \common\models\piramida\Currency::getValueFromAtom($item['price'], \common\models\piramida\Currency::LOT);
                    },
                ],
                [
                    'header'  => 'Старт',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time_start', 0);
                        if ($v == 0) return '';
                        if ($v < time()) {
                            return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v), 'class' => 'label label-default']);
                        } else {
                            return Html::tag('span', \cs\services\DatePeriod::forward($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v), 'class' => 'label label-success']);
                        }
                    },
                ],
                [
                    'header'  => 'Окончание',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time_finish', 0);
                        if ($v == 0) return '';
                        if ($v < time()) {
                            return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v), 'class' => 'label label-default']);
                        } else {
                            return Html::tag('span', \cs\services\DatePeriod::forward($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v), 'class' => 'label label-success']);
                        }
                    },
                ],
                [
                    'header'  => 'Розыгрыш',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'time_raffle', 0);
                        if ($v == 0) return '';
                        if ($v < time()) {
                            return Html::tag('span', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v), 'class' => 'label label-default']);
                        } else {
                            return Html::tag('span', \cs\services\DatePeriod::forward($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v), 'class' => 'label label-success']);
                        }
                    },
                ],
                [
                    'header'  => 'Просмотр',
                    'content' => function ($item) {
                        return Html::button('Просмотр', [
                            'class' => 'btn btn-info btn-xs buttonView',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Скрыть',
                    'content' => function ($item) {
                        if ($item['is_hide'] == 1) {
                            return Html::button('Показать', [
                                'class' => 'btn btn-success btn-xs buttonShow',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);

                        } else {
                            return Html::button('Скрыть', [
                                'class' => 'btn btn-danger btn-xs buttonHide',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);

                        }
                    }
                ],
                [
                    'header'  => 'Отменить',
                    'content' => function ($item) {
                        if ($item['is_cancel'] != 1) {
                            return Html::button('Отменить', [
                                'class' => 'btn btn-warning btn-xs buttonCancel',
                                'data'  => [
                                    'id' => $item['id'],
                                ]
                            ]);
                        }

                        return '';
                    }
                ],
                [
                    'header'  => 'Копировать',
                    'content' => function ($item) {
                        return Html::button('Копировать', [
                            'class' => 'btn btn-default btn-xs buttonCopy',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>