<?php

/** @var $this \yii\web\View */
/** @var $bill \common\models\avatar\UserBill */
/** @var $model \avatar\models\forms\ConvertSbp */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Конвертация';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php

$code = \common\models\avatar\Currency::findOne($bill->currency)->code;
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
var for1;


clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

$('#currency_from').on('change', function(e) {
    var i = $(this)[0].selectedIndex;
    var op = $($(this)[0].children[i]);
    for1 = op.data('code');
    $('.labelKurs').html('Курс конвертации<br>' + op.data('kurs_formatted') + ' ' + '{$code}' + ' за 1 ' + for1);
    if ($('#convertsbp-amount').val() != '') {
        var t = $('#convertsbp-amount').val();
        var s = t / op.data('kurs');
        $('.labelOut').html('Спишется<br>' + s + ' ' +  op.data('code'));
        $('.labelOut').attr('data-out', s);
    }
})

$('#convertsbp-amount').on('input', function(e) {
    var t = $(this).val();
    var i = $('#currency_from')[0].selectedIndex;
    var op = $($('#currency_from')[0].children[i]);
    var s = t / op.data('kurs');
    $('.labelOut').html('Спишется<br>' + s + ' ' +  op.data('code'));
    $('.labelOut').attr('data-out', s);
    
    for1 = op.data('code');
    $('.labelKurs').html('Курс конвертации<br>' + op.data('kurs_formatted') + ' ' + '{$code}' + ' за 1 ' + for1);
})

$('#contact-button').click(function(e) {
    var i = $('#currency_from')[0].selectedIndex;
    var op = $($('#currency_from')[0].children[i]);
    
    if ($('#convertsbp-amount').val() == '') {
        
        new Noty({
            timeout: 1000,
            theme: 'sunset',
            type: 'success',
            layout: 'bottomLeft',
            text: 'Заполните сумму'
        }).show();
        
    } else if ($('.labelOut').data('out')  > op.data('amount')) {
        
        new Noty({
            timeout: 1000,
            theme: 'sunset',
            type: 'success',
            layout: 'bottomLeft',
            text: 'Сумма списания превышает баланс кошелька'
        }).show();
        
    } else {
        
        var i = $('#currency_from')[0].selectedIndex;
        var op = $($('#currency_from')[0].children[i]);
        var t = $('#convertsbp-amount').val();
        var s = t / op.data('kurs');
        
        $('#modalConfirm').find('.code').html(op.data('code'));
        $('#modalConfirm').find('.price').html(s + ' ' + op.data('code'));
        $('#modalConfirm').modal();
        
    }
})

$('#modalConfirm .buttonAction').click(function(e) {
    
    var i = $('#currency_from')[0].selectedIndex;
    var op = $($('#currency_from')[0].children[i]);
    
    ajaxJson({
        url: '/cabinet-wallet/convert-ajax',
        data: {
            id: {$bill->id},
            value: $('#convertsbp-amount').val(),
            wallet: op.data('id')
        },
        success: function(ret) {
            $('#modalConfirm').on('hidden.bs.modal', function() {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location = '/cabinet-wallet/item?id=' + {$bill->id};
                }).modal(); 
            }).modal('hide');
        }
    })
});
JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
        <?php
        $letm = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETM);
        $letn = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETN);
        $items = [
            [
                'label' => \common\models\avatar\Currency::findOne($letm['billing']->currency)->code,
                'url'   => '/cabinet-wallet/convert2?id=' . $letm['billing']->id,
            ],
            [
                'label' => \common\models\avatar\Currency::findOne($letn['billing']->currency)->code,
                'url'   => '/cabinet-wallet/convert2?id=' . $letn['billing']->id,
            ],
        ];
        if ($bill->currency == \common\models\avatar\Currency::LETM) {
            $items[0]['active'] = true;
            $c = $items[0]['label'];
        }
        if ($bill->currency == \common\models\avatar\Currency::LETN) {
            $items[1]['active'] = true;
            $c = $items[1]['label'];
        }
        ?>
        <?= \yii\bootstrap\Tabs::widget(['items' => $items]) ?>
        <hr>
    </div>

    <div class="col-lg-6">

        <p>Пополнить баланс <?= $c ?></p>

        <p>Счет списания</p>
        <select class="form-control" id="currency_from">
            <?php if ($bill->currency == \common\models\avatar\Currency::LETM) { ?>
                <?php
                $rows = [];

                $pzm = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::PZM);
                $let = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::LEGAT);
                $eth = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::ETH);
                $usd = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::USDT);
                $letn = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::LETN);

                $pzm_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::PZM);
                $let_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LEGAT);
                $eth_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::ETH);
                $usd_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::USDT);
                $letn_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LETN);

                $bill_prm = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::PZM);
                $bill_let = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LEGAT);
                $bill_eth = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::ETH);
                $bill_usd = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::USDT);
                $bill_letn = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETN);

                $rows[] = [
                    'id'             => $pzm->id,
                    'code'           => $pzm->code,
                    'title'          => $pzm->title,
                    'kurs'           => $pzm->kurs,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal($pzm->kurs, 2),
                    'amount'         => $bill_prm['wallet']->amount,
                    'decimals'       => $pzm_int->decimals,
                    'decimals_view'  => $pzm_int->decimals_view,
                ];
                $rows[] = [
                    'id'             => $let->id,
                    'code'           => $let->code,
                    'title'          => $let->title,
                    'kurs'           => $pzm->kurs,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal($pzm->kurs, 2),
                    'amount'         => $bill_let['wallet']->amount,
                    'decimals'       => $let_int->decimals,
                    'decimals_view'  => $let_int->decimals_view,
                ];
                $rows[] = [
                    'id'             => $eth->id,
                    'code'           => $eth->code,
                    'title'          => $eth->title,
                    'kurs'           => $eth->kurs,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal($eth->kurs, 2),
                    'amount'         => $bill_eth['wallet']->amount,
                    'decimals'       => $eth_int->decimals,
                    'decimals_view'  => $eth_int->decimals_view,
                ];
                $rows[] = [
                    'id'             => $usd->id,
                    'code'           => 'USDT',
                    'title'          => 'USDT',
                    'kurs'           => $usd->kurs,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal($usd->kurs, 2),
                    'amount'         => $bill_usd['wallet']->amount,
                    'decimals'       => $usd_int->decimals,
                    'decimals_view'  => $usd_int->decimals_view,
                ];
                $rows[] = [
                    'id'             => $letn->id,
                    'code'           => $letn->code,
                    'title'          => $letn->title,
                    'kurs'           => $pzm->kurs,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal($pzm->kurs, 2),
                    'amount'         => $bill_letn['wallet']->amount,
                    'decimals'       => $letn_int->decimals,
                    'decimals_view'  => $letn_int->decimals_view,
                ];
                ?>
                <?php foreach ($rows as $row) { ?>
                    <?= Html::tag(
                        'option',
                        $row['code'] . ' (' . Yii::$app->formatter->asDecimal(bcdiv($row['amount'], bcpow(10, $row['decimals']), $row['decimals']), $row['decimals_view']) . ')',
                        [
                            'data' => [
                                'id'             => $row['id'],
                                'kurs'           => $row['kurs'],
                                'code'           => $row['code'],
                                'kurs_formatted' => $row['kurs_formatted'],
                                'amount'         => $row['amount'],
                            ],
                        ]
                    ) ?>
                <?php } ?>
            <?php } ?>
            <?php if ($bill->currency == \common\models\avatar\Currency::LETN) { ?>
                <?php
                $rows = [];
                $pzm = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::PZM);
                $letm = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::LETM);

                $pzm_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::PZM);
                $let_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LEGAT);
                $eth_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::ETH);
                $usd_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::USDT);
                $letn_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LETN);
                $letm_int = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LETM);

                $bill_prm = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::PZM);
                $bill_letm = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LETM);
                $rows[] = [
                    'id'             => $pzm->id,
                    'code'           => $pzm->code,
                    'title'          => $pzm->title,
                    'kurs'           => 1,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal(1, 2),
                    'amount'         => $bill_prm['wallet']->amount,
                    'decimals'       => $pzm_int->decimals,
                    'decimals_view'  => $pzm_int->decimals_view,
                ];
                $rows[] = [
                    'id'             => $letm->id,
                    'code'           => $letm->code,
                    'title'          => $letm->title,
                    'kurs'           => 1 / $pzm->kurs,
                    'kurs_formatted' => Yii::$app->formatter->asDecimal(1 / $pzm->kurs, 2),
                    'amount'         => $bill_letm['wallet']->amount,
                    'decimals'       => $letm_int->decimals,
                    'decimals_view'  => $letm_int->decimals_view,
                ];
                ?>
                <?php foreach ($rows as $row) { ?>
                    <?= Html::tag(
                        'option',
                        $row['code'] . ' (' . Yii::$app->formatter->asDecimal(bcdiv($row['amount'], bcpow(10, $row['decimals']), $row['decimals']), $row['decimals_view']) . ')',
                        [
                            'data' => [
                                'id'             => $row['id'],
                                'kurs'           => $row['kurs'],
                                'code'           => $row['code'],
                                'kurs_formatted' => $row['kurs_formatted'],
                                'amount'         => $row['amount'],
                            ],
                        ]
                    ) ?>
                <?php } ?>
            <?php } ?>
        </select>

        <hr>

        <p>Сумма к получению</p>
        <input id="convertsbp-amount" class="form-control">

        <hr>
        <div class="form-group">
            <?= Html::button('Пополнить с конвертацией', [
                'class' => 'btn btn-default',
                'id'    => 'contact-button',
                'style' => 'width:100%',
            ]) ?>
        </div>

    </div>
    <div class="col-lg-6">
        <p class="alert alert-success labelKurs"></p>
        <p class="alert alert-success labelOut"></p>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Пополнение баланса <?= $c ?></h4>
            </div>
            <div class="modal-body">
                Внимание с сервисного счета <span class="code"></span> спишется <span class="price"></span>!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-success buttonAction">Подтвердить</button>
            </div>
        </div>
    </div>
</div>