<?php

/** $this \yii\web\View  */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Кошелек';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function(e) {
    window.location = '/cabinet-wallet/item' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => UserBill::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->andWhere(['mark_deleted' => 0])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'    => 'WID',
                    'content'   => function (\common\models\avatar\UserBill $item) {
                        $currency = $item->currency;
                        if (!\common\models\avatar\CurrencyLink::find()->where(['currency_ext_id' => $currency])->exists()) {
                            return '';
                        }
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'address');
                        if (is_null($v)) return '';
                        $w = new \common\models\piramida\Wallet(['id' => $v]);
                        $addressShort = $w->getAddressShort();

                        return Html::tag('code', $addressShort);
                    },
                ],
                [
                    'header'  => 'Картинка',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'currency', '');
                        if ($i == '') return '';
                        $currency = \common\models\avatar\Currency::findOne($i);
                        if (is_null($currency)) return '';

                        return Html::img($currency->image, [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    },
                ],
                [
                    'header'  => 'Название',
                    'content' => function ($item) {
                        return $item['name'];
                    },
                ],

                [
                    'header'         => 'Монет',
                    'headerOptions'  => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content'        => function ($item) {
                        $i = ArrayHelper::getValue($item, 'currency', '');
                        if ($i == '') return '';
                        $currency = \common\models\avatar\Currency::findOne($i);
                        if (is_null($currency)) return '';

                        if (in_array($currency->id, \common\models\avatar\CurrencyLink::find()->select(['currency_ext_id'])->column())) {
                            $wallet = \common\models\piramida\Wallet::findOne($item['address']);
                            if (is_null($wallet)) return '';

                            return Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currency->decimals_view);
                        }

                        return '';
                    },
                ],
                [
                    'header'  => 'Валюта',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'currency', '');
                        if ($i == '') return '';
                        $currency = \common\models\avatar\Currency::findOne($i);
                        if (is_null($currency)) return '';

                        return Html::tag('span', $currency->code, ['class' => 'label label-info']);
                    },
                ],
                [
                    'header'  => 'Карта',
                    'content' => function ($item) {
                        $i = ArrayHelper::getValue($item, 'card_id', '');
                        if ($i == '') return '';
                        return Html::button('Карта', [
                            'class' => 'btn btn-primary buttonCard',
                            'data' => [
                                'id' => $item['card_id'],
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Удалить',
                    'content' => function ($item) {
                        return Html::button('Удалить', [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ],
                        ]);
                    },
                ], ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>