<?php

/** @var $this \yii\web\View */
/** @var $model \avatar\models\forms\ConvertNeiroBinance */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/** @var  $currencyELXT \common\models\avatar\Currency */
$cLink = \common\models\avatar\CurrencyLink::findOne(['currency_ext_id' => $model->currency->id]);
$data = UserBill::getInternalCurrencyWallet($cLink->currency_int_id);
$currencyExt = $model->currency;
$fromCurrencyExt = $model->currency;

$userWallet = $data['billing'];
$wallet = \common\models\piramida\Wallet::findOne($userWallet->address);
$currencyInt = $wallet->getCurrency();
$fromCurrencyInt = $fromCurrencyInt = $currencyInt;

$this->title = 'Конвертировать ' . $currencyInt->code . ' в NEIRO';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

// получаю комиссию системы
$dataJson = \common\models\Config::get('NeironConvertParams');
$data = \yii\helpers\Json::decode($dataJson);
$s = '';
foreach ($data as $o1) {
    if ($o1['id'] == $model->currency->id) {
        $s = $o1;
    }
}
if (!is_array($s)) {
    $s = [];
    $s['in'] = 0;
}
$toCurrencyExt = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::NEIRO);
$kurs = $model->currency->price_usd * (1-($s['in'] / 100)) / $toCurrencyExt->price_usd;

$cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $toCurrencyExt->id]);
$toCurrencyInt = \common\models\piramida\Currency::findOne($cio->currency_int_id);
?>
<?php
$str847 = Yii::t('c.EQFlAIQvBL', 'Скопировано');
$fromUsd = $fromCurrencyExt->price_usd;
$toUsd = $toCurrencyExt->price_usd;

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str847}'
    }).show();

});
$('#convertneirobinance-amount').on('input', function(e) {
    var v = $(this).val();
    var v2 = v * {$kurs};
    $('#convertneirobinance-amount2').val((v2).toFixed({$toCurrencyInt->decimals_view}));
    $('.js-from-usd').html('~ ' +  (v * {$fromUsd}).toFixed(2) + ' USDT');
    $('.js-to-usd').html('~ ' +  (v2 * {$toUsd}).toFixed(2) + ' USDT');
});
$('#convertneirobinance-amount2').on('input', function(e) {
    var v2 = $(this).val();
    var v = v2 / {$kurs};
    $('#convertneirobinance-amount').val((v).toFixed({$fromCurrencyInt->decimals_view}));
    $('.js-from-usd').html('~ ' +  (v * {$fromUsd}).toFixed(2) + ' USDT');
    $('.js-to-usd').html('~ ' +  (v2 * {$toUsd}).toFixed(2) + ' USDT');
});
JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-6 col-lg-offset-3">
        <p class="alert alert-danger">
            Disclaimer<br>
            <br>
            Внимание! Работа сервиса конвертации - автоматизированный процесс биржевой online-активности,
            с вынужденным привлечением третьих сторон. При конвертации Вы получите курс базового актива
            по усредненной цене исполнения возможной последовательности маркет-ордеров.<br>
            <br>
            Мы предупреждаем, а Вы должны быть готовы к тому, что в процессе конвертации:<br>
            <br>
            Время обмена может затянуться, например, при панических движениях биржевых котировок
            базового актива, особенно для высоковолатильных валют, иногда на достаточно долгий период времени.<br>
            <br>
            Цена котировки базового актива так-же может быть далека от той, которую Вы видите в текущем
            моменте, например, из-за проскальзывания курса при резком движении цены высоковолатильных монет.
            Финишный курс конвертации будет высчитан как производная от средних величин разбега
            цен и объемов в паре при обмене активов, зафиксированных в моментах фактического исполнения
            возможной череды маркет-ордеров, с учетом соответствующих комиссий.<br>
            <br>
            Мы так подробно описали для вас эти вводные потому, что вы должны понимать, устраивают
            ли вас такие условия. Процессы автоматизированы, начав конвертацию, отменить обмен будет
            невозможно, и претензии рассматриваться не будут. Поэтому примите для себя решение сами.<br>
            <br>
            (!!!) Итак, нажимая на кнопку "Конвертировать", вы соглашаетесь со всеми вышеперечисленными
            условиями. Если не согласны, - вы должны покинуть эту страницу конвертации, и провести обмен
            своими силами, например на P2P-площадке, или самостоятельно на любой из бирж.
        </p>
        <p class="alert alert-danger"><?= \Yii::t('c.WxnFTlXT5m', 'Обратите внимание! Минимальная сумма конвертации не может быть меньше эквивалентной 12 USDT.') ?></p>

        <p class="text-center">По курсу 1 <?= $model->currency->code ?> = <?= Yii::$app->formatter->asDecimal($kurs, $toCurrencyInt->decimals_view) ?> NEIRO</p>
        <div class="row">
            <div class="col-lg-6">
                <p class="text-center">
                    <?php
                    $image = $currencyExt->image;
                    if (\yii\helpers\StringHelper::startsWith($image, 'http')) {

                    } else {
                        $image = 'https://neiro-n.com' . $currencyExt->image;
                    }
                    ?>
                    <img src="<?= $image ?>" width="100" style="margin-top: 30px;" class="img-circle">
                </p>
                <?php
                $address = $wallet->getAddress();
                $options = [
                    'data'  => [
                        'toggle'         => 'tooltip',
                        'clipboard-text' => $address,
                    ],
                    'class' => 'buttonCopy',
                    'title' => Yii::t('c.EQFlAIQvBL', 'Адрес кошелька. Нажми чтобы скопировать'),
                ];
                ?>

                <p class="text-center"><code <?= Html::renderTagAttributes($options) ?>><?= $address ?></code></p>
                <p class="text-center"><?= $currencyExt->title ?></p>
                <h3 class="page-header text-center"><?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currencyInt->decimals_view) ?></h3>
                <p class="text-center"><span class="label label-info"><?= $currencyInt->code ?></span></p>


            </div>
            <div class="col-lg-6">
                <p class="text-center">
                    <?php
                    $image = $toCurrencyExt->image;
                    if (\yii\helpers\StringHelper::startsWith($image, 'http')) {

                    } else {
                        $image = 'https://neiro-n.com' . $toCurrencyExt->image;
                    }
                    ?>
                    <img src="<?= $image ?>" width="100" style="margin-top: 30px;" class="img-circle">
                </p>
                <p class="text-center"><?= $toCurrencyExt->title ?></p>
                <p class="text-center"><span class="label label-info"><?= $toCurrencyExt->code ?></span></p>
            </div>
        </div>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-convert/index';
    }).modal();
}
JS

        ]); ?>
        <div class="row">

            <div class="col-lg-6">
                <?= $form->field($model, 'amount')->label(Yii::t('c.WxnFTlXT5m', 'Отдаете') . ' ' . $currencyExt->code)->hint(\Yii::t('c.WxnFTlXT5m', 'дробное число пишется через точку')) ?>
                <p class="js-from-usd"></p>
            </div>
            <div class="col-lg-6">
                <div class="form-group field-convert-amount required">
                    <label class="control-label" for="convert-amount"><?= \Yii::t('c.WxnFTlXT5m', 'Получаете') ?> <?= $toCurrencyExt->code ?></label>
                    <input type="text" id="convertneirobinance-amount2" class="form-control" aria-required="true">
                    <p class="help-block"><?= \Yii::t('c.WxnFTlXT5m', 'дробное число пишется через точку') ?></p>
                    <p class="js-to-usd"></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <hr>
                <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Создать заявку']); ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>
