<?php

/** @var $this \yii\web\View */
/** @var $bill \common\models\avatar\UserBill */
/** @var $model \avatar\models\forms\SendAtom2 */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = $bill->name;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$str847 = Yii::t('c.EQFlAIQvBL', 'Скопировано');
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: '{$str847}'
    }).show();

});

JS
);
?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">

        <?php
        /** @var  $currencyELXT \common\models\avatar\Currency */
        $currencyELXT = $bill->getCurrencyObject();

        $userWallet = $bill;
        $wallet = \common\models\piramida\Wallet::findOne($userWallet->address);
        $currency = $wallet->getCurrency();
        Yii::$app->session->set('currency.decimals', $currency->decimals);
        ?>

        <p class="text-center">
            <img src="<?= $currencyELXT->image ?>" width="300" style="margin-top: 30px;" class="img-circle">
        </p>

        <?php
        $address = $wallet->getAddress();
        $options = [
            'data'  => [
                'toggle'         => 'tooltip',
                'clipboard-text' => $address,
            ],
            'class' => 'buttonCopy',
            'title' => Yii::t('c.EQFlAIQvBL', 'Адрес кошелька. Нажми чтобы скопировать'),
        ];
        ?>

        <p class="text-center"><code <?= Html::renderTagAttributes($options) ?>><?= $address ?></code></p>
        <p class="text-center"><?= $currencyELXT->title ?></p>
        <h3 class="page-header text-center"><?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), $currency->decimals_view) ?></h3>
        <p class="text-center"><span class="label label-info"><?= $currencyELXT->code ?></span></p>

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">


                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                    'model'   => $model,
                    'formUrl' => '/cabinet-wallet/send2',
                    'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-bills/index';
    }).modal();
}
JS

                ]); ?>
                <?= Html::hiddenInput(Html::getInputName($model, 'id'), $bill->id) ?>
                <?= $form->field($model, 'user_email', ['inputOptions' => ['placeholder' => 'U_000...']])->label(Yii::t('c.EQFlAIQvBL', 'User-ID получателя')) ?>
                <?= $form->field($model, 'amount')->label(Yii::t('c.EQFlAIQvBL', 'Количество монет для отправки')) ?>
                <?= $form->field($model, 'comment')->label(Yii::t('c.EQFlAIQvBL', 'Комментарий')) ?>

                <?php \iAvatar777\services\FormAjax\ActiveForm::end(['isHide' => true]); ?>

                <hr>
                <?php
                $this->registerJs(<<<JS

$('.buttonSend').click(function(e) {
    $('#modalConfirm').modal();
});

$('#modalConfirm').on('hidden.bs.modal', function() {
    ajaxJson({
        url: '/cabinet-wallet/send2',
        data: $('form').serializeArray(),
        success: function(ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location = '/cabinet-bills/index';
            }).modal();
        },
        errorScript: function(ret) {
            if (ret.id == 102) {
                for (var key in ret.data.errors) {
                    if (ret.data.errors.hasOwnProperty(key)) {
                        var name = key;
                        var value = ret.data.errors[key];
                        var id;
                        for (var key2 in ret.data.fields) {
                            if (ret.data.fields.hasOwnProperty(key2)) {
                                var name2 = key2;
                                var value2 = ret.data.fields[key2];
                                if (name == name2) {
                                    id = 'field-' + value2;
                                }
                            }
                        }
                        var g = $('.' + id);
                        g.addClass('has-error');
                        g.find('p.help-block-error').html(value.join('<br>')).show();
                    }
                }
            }
        }
    });
});

$('.ButtonConfirm').click(function(e) {
    $('#modalConfirm').modal('hide');
});
JS
                )
                ?>
                <button class="btn btn-default buttonSend" style="width: 100%;"><?= \Yii::t('c.EQFlAIQvBL', 'Отправить') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.EQFlAIQvBL', 'Security spoiler') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.EQFlAIQvBL', 'Подтвердить выполнение?') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary ButtonConfirm"><?= \Yii::t('c.EQFlAIQvBL', 'Да') ?></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.EQFlAIQvBL', 'Отмена') ?></button>
            </div>
        </div>
    </div>
</div>