<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = 'Информационный Telegram';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

$this->registerJs(<<<JS
$('.buttonPin').click(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/pin',
        success: function(ret) {
            window.location.reload();
        }
    });
});
$('.buttonUnPin').click(function(e) {
    ajaxJson({
        url: '/cabinet-telegram/un-pin',
        success: function(ret) {
            window.location.reload();
        }
    });
})
JS
);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <div  style="margin-top: 20px;">
            <?php if (!empty($user->telegram_lot_chat_id)): ?>

                <p class="alert alert-info">
                    <?= \Yii::t('c.AmyzFVMQnh', 'Аккаунт') ?>: <?= (empty($user->telegram_lot_username))? $user->telegram_lot_chat_id . '(chat_id)' : $user->telegram_lot_username ?>
                </p>

                <button class="btn btn-primary buttonUnPin"><?= \Yii::t('c.AmyzFVMQnh', 'Отсоединить') ?></button>

            <?php else: ?>

                <p>Краткая инструкция по активации инфобота <a href="tg://resolve?domain=e_lottery_info_bot" target="_blank">@e_lottery_info_bot</a>:</p>
                <ul>
                    <li>Зайдите в Telegram <a href="tg://resolve?domain=e_lottery_info_bot" target="_blank">@e_lottery_info_bot</a></li>
                    <li>Напишите "Привет"</li>
                    <li>Ответьте словом «да» на вопрос, есть ли аккаунт на платформе TopMate.one</li>
                    <li>Напишите ваш e-mail, который указывали при регистрации.</li>
                    <li>Далее есть два пути - «а» или «б». Можно выбрать любой:</li>
                    <li>
                        <ul>
                            <li>а) В своем ЛК вы обновляете страницу (F5) на вкладке «Telegram» , и нажимаете кнопку "Присоединить".</li>
                            <li>б) Зайдите на e-mail, перейдите по ссылке в письме, попав в свой ЛК, нажмите кнопку "Присоединить".</li>
                        </ul>
                    </li>
                </ul>

                <?php if (\common\models\UserTelegramLotTemp::findOne(['email' => Yii::$app->user->identity->email])) { ?>
                    <button class="btn btn-primary buttonPin" style="margin-bottom: 30px;"><?= \Yii::t('c.AmyzFVMQnh', 'Присоединить') ?></button>
                <?php } ?>

            <?php endif; ?>

            <p style="margin-top: 30px;"><a href="<?= Url::to('/page/item?id=9', true)?>"><?= \Yii::t('c.AmyzFVMQnh', 'Подробная инструкция') ?></a></p>
        </div>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>



