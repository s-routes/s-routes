<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\Lot */

$this->title = $item->name;

?>



<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">

        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <div class="row">
            <div class="col-lg-4">
                <p>
                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>">
                </p>
            </div>
            <div class="col-lg-8">
                <p><?= nl2br($item['description']) ?></p>
            </div>
        </div>

        <?= \yii\widgets\DetailView::widget([
            'model' => $item,
            'attributes' => [
                'id',
                [
                    'label' => 'Цена билета',
                    'value' => $item['price'] / 100
                ],
                'cash_back_percent:text:% кеш бека от пакета компенсации',
                'time_start:datetime',
                'time_finish:datetime',
                'time_raffle:datetime',
            ]
        ]) ?>

        <p>
            Докупить "Пакет компенсации" (<?= \common\models\piramida\Currency::getValueFromAtom($item['cash_back_price'], \common\models\piramida\Currency::LOT) ?> LOT)
            <?= \common\widgets\CheckBox2\CheckBox::widget([
                'model' => new \lot\models\forms\LotBuy(),
                'attribute' => 'id',
            ]) ?>
        </p>

        <p style="margin-top: 20px;">
            <?php
            $this->registerJs(<<<JS
$('.buttonBuy').click(function (e) {
    ajaxJson({
        url: '/cabinet-lot/buy-ajax',
        data: {
            id: $(this).data('id'), 
            is_cash_back: $('#lotbuy-id').prop('checked') ? 1 : 0
        },
        success: function (ret) {
            new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Успешно'
            }).show();
        },
        errorScript: function (ret) {
            console.log(ret);
            new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Успешно'
            }).show();
        }
    });
});
JS
            )
            ?>
            <button class="btn btn-success buttonBuy" data-id="<?= $item['id'] ?>">Подтвердить покупку</button>
        </p>
    </div>

</div>


