<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $item \common\models\Lot */
/** @var $room_id int */

// счетчик запущен
// \common\models\Lot::TYPE_MANUAL

\avatar\assets\SocketIO\Asset::register($this);
$serverName = \avatar\assets\SocketIO\Asset::getHost();
if (Yii::$app->user->isGuest) {
    $uid = 0;
} else {
    $uid = Yii::$app->user->id;
}


$this->registerJs(<<<JS

var socket = io.connect('{$serverName}');
var roomName = 'room' + {$room_id};
var myid2 = {$uid};
socket.emit('chat1-new-user', roomName, myid2);


socket.on('chat1-counter-win', function(data) {
    console.log(['chat1-counter-win', data]);
    
    $('.js-counter').html('Победитель: ' + getBilet(data.ticket));
    
    
    if (data.ticket.user.is_bot) {
        var text = [
            ['РОЗЫГРЫШ ПРОВЕДЕН!', 'color: red; font-size: 150%; font-weight: bold;'],
            ['Победитель: ' + getBilet2(data.ticket), 'color: red; font-size: 150%; font-weight: bold;'],
            ['Info from e-LOTTERY Support: этот системный бот – внутренний равноправный участник любого розыгрыша. Запланированный розыгрыш должен состояться обязательно и задача бота довыкупить все билеты до 100% заполнения пула участников. Он имеет равные с главными участниками шансы на выигрыш, но по понятным причинам не может «получать» бонусы, кешбеки и материальные призы.', ''],
        ];
        var text2 = '';
        for (var i9 = 0; i9 < text.length; i9++) {
            text2 = text2 + '<p style="' + text[i9][1] + '"> ' + text[i9][0] + '</p>';
        }
        $('.js-desc')
            .html(text2)
            .addClass('text-center')
            ;
    } else {
        var text = [
            'РОЗЫГРЫШ ПРОВЕДЕН!',
            'Победитель: ' + getBilet2(data.ticket),
            'ОТ ДУШИ ПОЗДРАВЛЯЕМ ПОБЕДИТЕЛЯ!',
            'ЖЕЛАЕМ ВАМ И В ДАЛЬНЕЙШЕМ УЛЫБОК ФОРТУНЫ!',
            '',
            'Мы направили победителю на e-mail письмо с информацией по выигрышу и получению Главного Приза. Если возникнут вопросы, свяжитесь с нами по указанным в письме контактам.',
            '',
            'e-LOTTERY Team'
        ];
        $('.js-desc')
            .html(text.join('<br>'))
            .attr('style', 'color: red; font-size: 150%; font-weight: bold;')
            .addClass('text-center')
            ;
    }
    
    
});



function getBilet(row) 
{
    var u = 'U_00' + row.user.id;
    var num = parseInt(row.num.substring(6));
    var i2 = [
        'LOTTERY ID ' + row.lot_id,
        'USER ID ' + u,
        'Tx Id ' + row.tx_id,
        'LOTTERY ticket Created At ' + row.created_at,
        'Идентификатор ' + row.num,
        'Билет №' + num
    ];
    var i = i2.join('<br>');
    
    var avatar = '' + row.user.avatar;
    if (avatar == '') {
        avatar = '/images/cabinet/index/photo_2020-01-28_15-58-38.jpg';
    }
    
    var name = row.user.name;
    var suffix = '<img src="'+avatar+'" class="img-circle" width="40" style="margin-right: 10px; margin-left: 10px;"> ' + name;
    return i + '<br>' + suffix;
}

function getBilet2(row) 
{
    var u = 'U_00' + row.user.id;
    var num = parseInt(row.num.substring(6));
    var i2 = [
        'USER ID ' + u,
        'Идентификатор ' + row.num,
        'Билет №' + num
    ];
    var i = i2.join(' * ');
    
    var avatar = '' + row.user.avatar;
    if (avatar == '') {
        avatar = '/images/cabinet/index/photo_2020-01-28_15-58-38.jpg';
    }
    
    var name = row.user.name;
    var suffix = '<img src="'+avatar+'" class="img-circle" width="40" style="margin-right: 10px; margin-left: 10px;"> ' + name;
    
    return suffix + ' * ' + i;
}

JS
);

?>

<div class="row">
    <div class="col-lg-4 js-counter" style="font-size: 200%; font-weight: bold; height: 420px;">

    </div>
    <div class="col-lg-8 js-desc">
        <p>Проводится розыгрыш. Ссылки: </p>
        <p>Youtube: <a href="<?= $item->link_youtube ?>" target="_blank"><?= $item->link_youtube ?></a></p>
        <p>Instagram: <a href="<?= $item->link_instagram ?>" target="_blank"><?= $item->link_instagram ?></a></p>
        <p>Telegram: <a href="<?= $item->link_telegram ?>" target="_blank"><?= $item->link_telegram ?></a></p>
    </div>
</div>

