<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $item \common\models\Lot */
/** @var $room_id int */

// счетчик запущен
// \common\models\Lot::TYPE_AUTO

\avatar\assets\SocketIO\Asset::register($this);
$serverName = \avatar\assets\SocketIO\Asset::getHost();

if (!Yii::$app->user->isGuest) {
    $userCurrent = ['avatar' => Yii::$app->user->identity->getAvatar(), 'name' => Yii::$app->user->identity->getName2(), 'id' => Yii::$app->user->id, 'address' => Yii::$app->user->identity->getAddress()];
    $userCurrentJson = \yii\helpers\Json::encode($userCurrent);
} else {
    $userCurrentJson = \yii\helpers\Json::encode([]);
}

$rows = \common\models\Bilet::find()->where(['lot_id' => $item->id])->asArray()->all();
$rows2 = [];
foreach ($rows as $r) {
    $user = \common\models\UserAvatar::findOne($r['user_id']);
    $u = $user->getAddress();

    $num = substr($r['num'], 6);
    $num = intval($num);

    $i2 = [
        'LOTTERY ID ' . $r['lot_id'],
        'USER ID ' . $u,
        'Tx Id ' . $r['tx_id'],
        'LOTTERY ticket Created At ' . $r['created_at'],
        'Идентификатор ' . $r['num'],
        'Билет №' . $num,
    ];

    $i = join('<br>', $i2);

    $suffix = Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 40, 'style' => 'margin-right: 10px; margin-left: 10px;', 'data' => ['toggle' => 'tooltip'], 'title' => $user->getName2()]) . $user->getName2();
    $rows2[] = $i . '<br>' . $suffix;
}
$rows2json = yii\helpers\Json::encode($rows2);

$timer = $item->counter;

if (Yii::$app->user->isGuest) {
    $uid = 0;
} else {
    $uid = Yii::$app->user->id;
}

$this->registerJs(<<<JS

var socket = io.connect('{$serverName}');
var roomName = 'room' + {$room_id};
var myid2 = {$uid};
socket.emit('chat1-new-user', roomName, myid2);

var timer = 0;
var is_timer = false;
var timerId;

function getBilet(row) 
{
    var u = 'U_00' + row.user_id;
    var num = parseInt(row.num.substring(6));
    var i2 = [
        'LOTTERY ID ' + row.lot_id,
        'USER ID ' + u,
        'Tx Id ' + row.tx_id,
        'LOTTERY ticket Created At ' + row.created_at,
        'Идентификатор ' + row.num,
        'Билет №' + num
    ];
    var i = i2.join('<br>');
    
    var avatar = '' + row.avatar;
    if (avatar == '') {
        avatar = 'https://elottery.cloud/images/cabinet/index/photo_2020-01-28_15-58-38.jpg';
    }
    
    var name = row.name_first + ' ' + row.name_last;
    var suffix = '<img src="' + avatar + '" class="img-circle" width="40" style="margin-right: 10px; margin-left: 10px;"> ' + name;
    return i + '<br>' + suffix;
}

function getBilet2(row) 
{
    var u = 'U_00' + row.user_id;
    var num = parseInt(row.num.substring(6));
    var i2 = [
        'USER ID ' + u,
        'Идентификатор ' + row.num,
        'Билет №' + num
    ];
    var i = i2.join(' * ');
    
    var avatar = '' + row.avatar;
    if (avatar == '') {
        avatar = 'https://elottery.cloud/images/cabinet/index/photo_2020-01-28_15-58-38.jpg';
    }
    
    var name = row.name_first + ' ' + row.name_last;
    var suffix = '<img src="' + avatar + '" class="img-circle" width="40" style="margin-right: 10px; margin-left: 10px;"> ' + name;
    
    return suffix + ' * ' + i;
}

function getBiletId(rows, id) 
{
    var row;
    for(var i=0; i < rows.length; i++) {
        if (rows[i].id == id) {
            row = rows[i];          
        }
    }
    
    return getBilet(row);
}

function findBilet(rows, id) 
{
    var row;
    for(var i=0; i < rows.length; i++) {
        if (rows[i].id == id) {
            row = rows[i];          
        }
    }
    
    return row;
}



socket.on('chat1-counter-stop', function(data) {
    console.log(['chat1-counter-stop', data]);
    if (is_timer) {
        clearInterval(timerId);
    }
    
    var timerId0;
    var timerId1;
    var timerId2;
    var timerId3;
    var timerId4;
    var timerId5;
    var timerId6;
    var timerId7;
    var timerId8;
    var timerId9;
    var timerId10;
    var timerId11;
    var timerId12;
    var timerId13;
    var timerId14;
    var timerId15;
    
    $('.buttonStop').hide();
    var user = data.user;
    var str =  [
        '<p>Участник U_00' + user.id + ' <img src="' + user.avatar + '" width=20 style="margin-left:10px; margin-right: 10px;"> ' + user.name + ' инициировал выбор победителя розыгрыша</p>',
        '<p>Замедление "вращения колеса фортуны" от 100Гц до 0Гц происходит со скоростью 6.699 Гц/сек за секунду.</p>',
        '<p>Через ~15сек имя победителя отразится на экране.</p>',
        '<p class="js-counter2"></p>'
    ];
    $('.js-desc').html(str.join('<br>'));
    
    var timer0 = 0;
    // Устанавливаю таймер 
    timerId0 = setInterval(function(e) {
        $('.js-counter2').html(timer0 + ' сек');
        timer0++;
    }, 1000);
    
    
    var biletList = [];
    for (var i = 0; i < data.rows.length; i++) {
        // Добавить код чтобы аватарка была в crop 
        biletList.push({id:data.rows[i].id, text: getBilet(data.rows[i])});
    }
    var c = 0;
    timerId1 = setInterval(function(e) {
        $('.js-counter').html(biletList[c].text);
        c++;
        if (c == biletList.length) c = 0;
    }, 10);
    
    setTimeout(function(e) {
        clearInterval(timerId1);
        timerId2 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 45);
    }, 1000);

    setTimeout(function(e) {
        clearInterval(timerId2);
        timerId3 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 80);
    }, 2000);

    setTimeout(function(e) {
        clearInterval(timerId3);
        timerId4 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 115);
    }, 3000);

    setTimeout(function(e) {
        clearInterval(timerId4);
        timerId5 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 150);
    }, 4000);

    setTimeout(function(e) {
        clearInterval(timerId5);
        timerId6 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 185);
    }, 5000);

    setTimeout(function(e) {
        clearInterval(timerId6);
        timerId7 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 220);
    }, 6000);

    setTimeout(function(e) {
        clearInterval(timerId7);
        timerId8 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 255);
    }, 7000);

    setTimeout(function(e) {
        clearInterval(timerId8);
        timerId9 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 290);
    }, 8000);

    setTimeout(function(e) {
        clearInterval(timerId9);
        timerId10 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 325);
    }, 9000);

    setTimeout(function(e) {
        clearInterval(timerId10);
        timerId11 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 360);
    }, 10000);

    setTimeout(function(e) {
        clearInterval(timerId11);
        timerId12 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 395);
    }, 11000);

    setTimeout(function(e) {
        clearInterval(timerId12);
        timerId13 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 430);
    }, 12000);


    setTimeout(function(e) {
        clearInterval(timerId13);
        timerId14 = setInterval(function(e) {
            $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 465);
    }, 13000);


    setTimeout(function(e) {
        clearInterval(timerId14);
        timerId15 = setInterval(function(e) {
           $('.js-counter').html(biletList[c].text);
            c++;
            if (c == biletList.length) c = 0;
        }, 500);
    }, 14000);
    
    setTimeout(function(e) {
        clearInterval(timerId15);
        
        // считаю до победителя
        $('.js-counter').html(biletList[c].text);
       
        $('.js-counter').html('Победитель: ' + getBilet(findBilet(data.rows, data.winner_id)));
           
        if (data.winner_is_bot) {
            var text = [
                ['РОЗЫГРЫШ ПРОВЕДЕН!', 'color: red; font-size: 150%; font-weight: bold;'],
                ['Победитель: ' + getBilet2(findBilet(data.rows, data.winner_id)), 'color: red; font-size: 150%; font-weight: bold;'],
                ['Info from e-LOTTERY Support: этот системный бот – внутренний равноправный участник любого розыгрыша. Запланированный розыгрыш должен состояться обязательно и задача бота довыкупить все билеты до 100% заполнения пула участников. Он имеет равные с главными участниками шансы на выигрыш, но по понятным причинам не может «получать» бонусы, кешбеки и материальные призы.', ''],
            ];
            var text2 = '';
            for (var i9 = 0; i9 < text.length; i9++) {
                text2 = text2 + '<p style="' + text[i9][1] + '"> ' + text[i9][0] + '</p>';
            }
            $('.js-desc')
                .html(text2)
                .addClass('text-center')
                ;
        } else {
            var text = [
                'РОЗЫГРЫШ ПРОВЕДЕН!',
                'Победитель: ' + getBilet2(findBilet(data.rows, data.winner_id)),
                'ОТ ДУШИ ПОЗДРАВЛЯЕМ ПОБЕДИТЕЛЯ!',
                'ЖЕЛАЕМ ВАМ И В ДАЛЬНЕЙШЕМ УЛЫБОК ФОРТУНЫ!',
                '',
                'Мы направили победителю на e-mail письмо с информацией по выигрышу и получению Главного Приза. Если возникнут вопросы, свяжитесь с нами по указанным в письме контактам.',
                '',
                'e-LOTTERY Team'
            ];
            $('.js-desc')
                .html(text.join('<br>'))
                .attr('style', 'color: red; font-size: 150%; font-weight: bold;')
                .addClass('text-center')
                ;

        }
        clearInterval(timer0);
    }, 16000);
    
});

$('.buttonStop').click(function(ret) {
    socket.emit('chat1-counter-stop', roomName, {$item->id}, {$userCurrentJson});
    $('.buttonStop').hide();
});



JS
);
$this->registerJs(<<<JS

var biletList = {$rows2json};
var c = 0;
is_timer = true;
// запускаю счетчик
timerId = setInterval(function(e) {
    $('.js-counter').html(biletList[c]);
    c++;
    if (c == biletList.length) c = 0;
}, 1);

JS
);


?>

<div class="row">
    <div class="col-lg-4 js-counter" style="font-size: 200%; font-weight: bold; height: 420px;">

    </div>

    <div class="col-lg-8 js-desc">
        Уважаемые участники! Сейчас начался автоматизированный процесс розыгрыша Главного Приза. Каждый из вас имеет равные права, и в любой момент может инициировать постепенную остановку вращения «колеса фортуны», просто нажав зеленую кнопку «ВЫБРАТЬ ПОБЕДИТЕЛЯ», и таким образом определить счастливчика. Сейчас госпожа Фортуна - в ваших руках!<br>
        <br>
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -<br>
        <br>
        Технологически процесс можно описать так: электронный «барабан колеса фортуны» вращается и перебирает участников по порядку, в соответствии с их регистрацией на розыгрыш и очередностью приобретения билетов. Рабочая частота вращения барабана 100,0Гц (оборотов в секунду). При нажатии на кнопку "Выбрать победителя" происходит плавное замедление перебора участников c потерей «угловой скорости вращения» в 6,699Гц (об/сек) за секунду. Таким образом для вас визуально остановка барабана произойдет через ~15 сек, в зависимости от поступления управляющего сигнала на сервер в начале или в конце секунды, и вашей скорости входящего интернет-трафика в моменте. После остановки «колеса фортуны» (уменьшении угловой скорости до 0,0Гц) - вы увидите победителя текущего розыгрыша.
        <br>
        <br>
        <?php if (!Yii::$app->user->isGuest) { ?>
            <?php if (\common\models\Bilet::find()->where(['lot_id' => $item->id, 'user_id' => Yii::$app->user->id])->exists()) { ?>
                <?php if (Yii::$app->requestedRoute == 'cabinet-lot/play') { ?>
                    <button class="btn btn-success buttonStop" style=" font-size: 150%; font-weight: bold;">ВЫБРАТЬ  ПОБЕДИТЕЛЯ</button>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
</div>

