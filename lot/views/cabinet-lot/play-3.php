<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $item \common\models\Lot */
/** @var $room_id int */

// счетчик остановлен, есть победитель


$bilet = \common\models\Bilet::findOne($item->winner_id);

$user = \common\models\UserAvatar::findOne($bilet->user_id);
$u = $user->getAddress();

$num = substr($bilet['num'], 6);
$num = intval($num);

$str = [
    'LOTTERY ID ' . $bilet['lot_id'],
    'USER ID ' . $u,
    'Tx Id ' . $bilet['tx_id'],
    'LOTTERY ticket Created At ' . $bilet['created_at'],
    'Идентификатор ' . $bilet['num'],
    'Билет №' . $num,
];
$i = join(' * ', $str);

$suffix = Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 40, 'style' => 'margin-right: 10px; margin-left: 10px;', 'data' => ['toggle' => 'tooltip'], 'title' => $user->getName2()]) . $user->getName2();
$p = $i . $suffix;

?>
<div class="row">
    <div class="col-lg-4 js-counter" style="font-size: 200%; font-weight: bold;">
        Победитель: <?= $p ?>
    </div>
    <?php if ($bilet->user_id == Yii::$app->params['AUTO_REDEMPTION_USER_ID']) { ?>
        <div class="col-lg-8 text-center js-desc">
            <p style="color: red; font-size: 150%; font-weight: bold;">РОЗЫГРЫШ ПРОВЕДЕН!</p>
            <p style="color: red; font-size: 150%; font-weight: bold;">Победитель: <img src="<?= $user->getAvatar() ?>" width="40" class="img-circle" style="margin-right: 10px; margin-left: 10px;"> <?= $user->getName2() ?> * USER ID <?= $user->getAddress() ?> * Идентификатор <?= $bilet['num'] ?> Билет № <?= $num ?></p>
            <p>Info from e-LOTTERY Support: этот системный бот – внутренний равноправный участник любого розыгрыша. Запланированный розыгрыш должен состояться обязательно и задача бота довыкупить все билеты до 100% заполнения пула участников. Он имеет равные с главными участниками шансы на выигрыш, но по понятным причинам не может «получать» бонусы, кешбеки и материальные призы.</p>
        </div>
    <?php } else { ?>
        <div class="col-lg-8 text-center js-desc" style="color: red; font-size: 150%; font-weight: bold;">
            РОЗЫГРЫШ ПРОВЕДЕН!<br>
            Победитель: <img src="<?= $user->getAvatar() ?>" width="40" class="img-circle" style="margin-right: 10px; margin-left: 10px;"> <?= $user->getName2() ?> * USER ID <?= $user->getAddress() ?> * Идентификатор <?= $bilet['num'] ?> Билет № <?= $num ?>
            ОТ ДУШИ ПОЗДРАВЛЯЕМ ПОБЕДИТЕЛЯ!<br>
            ЖЕЛАЕМ ВАМ И В ДАЛЬНЕЙШЕМ УЛЫБОК ФОРТУНЫ!<br>
            <br>
            Мы направили победителю на e-mail письмо с информацией по выигрышу и получению Главного Приза. Если возникнут вопросы, свяжитесь с нами по указанным в письме контактам.<br>
            <br>
            e-LOTTERY Team
        </div>
    <?php } ?>


</div>



