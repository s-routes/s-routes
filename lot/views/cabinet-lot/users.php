<?php

use common\models\piramida\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\Lot */

$this->title = $item->name;

$c = Currency::findOne(Currency::LOT);

$cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

$type = '';
if ($item->type == \common\models\Lot::TYPE_AUTO) $type = 'Автоматизированный';
if ($item->type == \common\models\Lot::TYPE_MANUAL) $type = 'Online трансляция';
// кол-во купленных билетов
$bilet_pay_count = \common\models\Bilet::find()->where(['lot_id' => $item['id']])->count();

$prize = '';
if ($item->prize_type == \common\models\Lot::PRIZE_TYPE_MONEY) {
    $prize = Currency::getValueFromAtom($item['prize_amount'], Currency::LOT) . $cHtml;
}
if ($item->prize_type == \common\models\Lot::PRIZE_TYPE_THING) {
    $prize = $item->prize_name;
}
?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">

        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <div class="row">
            <div class="col-lg-4">
                <p><img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>"></p>
            </div>
            <div class="col-lg-8">

                <?php
                $attributes = [
                    [
                        'label' => 'Розыгрыш',
                        'captionOptions' => ['style' => 'width: 30%'],
                        'value' => $type,
                    ],
                    [
                        'label' => 'Краткая информация о лоте',
                        'format' => 'html',
                        'value' =>nl2br($item['description']),
                    ],
                    [
                        'label'  => 'Главный Приз',
                        'format' => 'html',
                        'value'  => $prize,
                    ],
                    [
                        'label' => 'Количество участников',
                        'format' => 'html',
                        'value' => $bilet_pay_count,
                    ],
                    [
                        'label' => 'Шансы на выигрыш',
                        'format' => 'html',
                        'value' => Yii::$app->formatter->asDecimal(((1/$item->bilet_count) *100), 2) . ' %',
                    ],
                    [
                        'label' => 'Стоимость билета eLottery',
                        'format' => 'html',
                        'value' => Currency::getValueFromAtom($item['price'], Currency::LOT) . $cHtml,
                    ],

                    'cash_back_percent:text:Cashback от стоимости билета (%)',
                    'time_start:datetime:Время начала продаж билетов',
                    'time_finish:datetime:Время окончания продаж билетов',
                    'time_raffle:datetime:Время розыгрыша Главного Приза',
                ];
                if ($item['type'] == \common\models\Lot::TYPE_MANUAL) {
                    $attributes[] = [
                        'attribute' => 'link_youtube',
                        'format'    => ['url', ['target' => '_blank']],
                        'label'    => 'Ссылка Youtube',
                    ];
                    $attributes[] = [
                        'attribute' => 'link_instagram',
                        'format'    => ['url', ['target' => '_blank']],
                        'label'    => 'Ссылка Инстаграм',
                    ];
                    $attributes[] = [
                        'attribute' => 'link_telegram',
                        'format'    => ['url', ['target' => '_blank']],
                        'label'    => 'Ссылка Telegram',
                    ];
                }
                ?>


                <?= \yii\widgets\DetailView::widget([
                    'model'      => $item,
                    'attributes' => $attributes,
                ]) ?>
            </div>
        </div>

        <?php
        $c = \common\models\Bilet::find()
            ->where(['lot_id' => $item['id']])->count();
        $this->registerJs(<<<JS
var count = {$c};
timerId = setInterval(function(e) {
    
    ajaxJson({
        url: '/cabinet-lot/users-get',
        data: {id: {$item->id}},
        success: function(ret) {
             if (count != ret.count) {
                 window.location.reload();
             }         
        }
    });
    
}, 10000);

JS
        );
        ?>

        <h2>Купленные билеты</h2>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\Bilet::find()
                    ->where(['lot_id' => $item['id']])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
                'sort' => ['defaultOrder' => ['created_at' => SORT_ASC]]
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => 'Билет',
                    'content' => function ($item) {

                        $u = 'U_' . str_repeat('0', 6 - strlen($item['user_id'])) . $item['user_id'];

                        $num = substr($item['num'], 6);
                        $num = intval($num);

                        $str = [
                            'LOTTERY ID ' . $item['lot_id'],
                            'USER ID ' . $u,
                            'Tx Id ' . $item['tx_id'],
                            'LOTTERY ticket Created At ' . $item['created_at'],
                            'Идентификатор ' . $item['num'],
                            'Билет №' . $num,
                        ];

                        return join(' * ', $str);
                    },
                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $lot = \common\models\Lot::findOne($item['lot_id']);
                        $user = \common\models\UserAvatar::findOne($item['user_id']);

                        return Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 40, 'style' => 'margin-right: 10px;', 'data' => ['toggle' => 'tooltip'], 'title' => $user->getName2()]) . $user->getName2();
                    },
                ],
            ],
        ]) ?>

    </div>

</div>


