<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $item \common\models\Lot */
/** @var $room_id int */

// счетчик не запущен


$this->registerJs(<<<JS

// Проверяю раз в 10 сек запуск счетчика
setInterval(function(e) {
    ajaxJson({
        url: '/lot/is-start',
        data: {id: {$item->id}},
        success: function(ret) {
            if (ret.is_counter_start == 1) {
                window.location.reload();
            }
        }
    });
}, 1000 * 10);

JS
);


?>

<div class="row" id="time_div">
    <div class="col-lg-4">
        <p>Текущее время: <span id="js-time-hour"></span>:<span id="js-time-min"></span>:<span id="js-time-sec"></span></p>
        <?php
        $this->registerJs(<<<JS
setInterval(function() {
    var d = new Date();
    var hour = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds();
    if (hour < 10) hour = '0' + hour; 
    if (min < 10) min = '0' + min; 
    if (sec < 10) sec = '0' + sec; 
    
    $('#js-time-hour').html(hour);
    $('#js-time-min').html(min);
    $('#js-time-sec').html(sec);
}, 1000);
JS
        )

        ?>
    </div>
    <div class="col-lg-8">
        <p class="alert alert-info">Внимание! 1-Все розыгрыши проходят по московскому времени (GMT+3). 2-Каждый пользователь видит текущее время и часовой пояс, установленные в его системе. 3-Для корректности отображения начала этапов розыгрыша, мы рекомендуем синхронизировать время вашей системы (мин и сек) с эталонным, взятым например <a href="https://time100.ru" target="_blank">отсюда</a> <a href="https://time100.ru" target="_blank">https://time100.ru</a></p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 js-counter"  style="color: red; font-size: 150%; font-weight: bold;">
        <p class="text-center">Здесь будет размещена информация по розыгрышу</p>
    </div>

</div>

<div class="row">
    <div class="col-lg-2">
    </div>
</div>




