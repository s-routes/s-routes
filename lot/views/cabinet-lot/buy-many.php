<?php

use common\models\piramida\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\Lot */
/* @var $model \lot\models\validate\CabinetLotBuyMany */

$this->title = $item->name;
$c = Currency::findOne(Currency::LOT);

$cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

$data = \common\models\avatar\UserBill::getInternalCurrencyWallet(Currency::LOT);
/** @var \common\models\piramida\Wallet $wallet */
$wallet = $data['wallet'];

$price = Currency::getValueFromAtom($item->price, Currency::LOT);
$cash_back_price = Currency::getValueFromAtom($item['cash_back_price'], Currency::LOT);

$button = Html::a('Приобрести LOTs', '/page/item?id=17', ['class' => 'btn btn-primary', 'style' => 'margin-left: 10px;']);
?>



<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">

        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <div class="row">
            <div class="col-lg-4">
                <p>
                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>">
                </p>
            </div>
            <div class="col-lg-8">
                <p><?= nl2br($item['description']) ?></p>
            </div>
        </div>

        <?= \yii\widgets\DetailView::widget([
            'model' => $item,
            'attributes' => [
                [
                    'label'          => 'Ваш баланс eLottery',
                    'captionOptions' => ['style' => 'width: 30%'],
                    'format'         => 'html',
                    'value'          => Currency::getValueFromAtom($wallet->amount, Currency::LOT) . $cHtml . $button,
                ],
                [
                    'label'  => 'Стоимость билета eLottery',
                    'format' => 'html',
                    'value'  => Currency::getValueFromAtom($item['price'], Currency::LOT) . $cHtml,
                ],

            ]
        ]) ?>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function (ret) {
    $('#ids').html(ret.join(', '));
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-lot/play?id=' + {$item->id};
    }).modal();
}
JS

        ]);
        $this->registerJs(<<<JS
$('#cabinetlotbuymany-count').on('input', function(e) {
    var val = $('#cabinetlotbuymany-count').val();
    var price = {$price};
    var cash_back_price = {$cash_back_price};
    var is_cash_back = $('#cabinetlotbuymany-is_cash_back').prop('checked');
    if (is_cash_back) {
        $('#price').html((val * (price + cash_back_price)) );
    } else {
        $('#price').html(val * price);
    }
});
$('input[name="CabinetLotBuyMany[is_cash_back]"]').on('change', function(e) {
    var val = $('#cabinetlotbuymany-count').val();
    var price = {$price};
    var cash_back_price = {$cash_back_price};
    if ($('#cabinetlotbuymany-is_cash_back').length > 0) {
        var is_cash_back = $('#cabinetlotbuymany-is_cash_back').prop('checked');
        if (is_cash_back) {
            $('#price').html((val * (price + cash_back_price)) );
        } else {
            $('#price').html(val * price);
        }
    } else {
        $('#price').html(val * price);
    }
});
JS
)
        ?>
        <?= $form->field($model, 'count')->label('Вы приобретаете билетов:') ?>

        <?php if ($item->cash_back_price > 0) { ?>
            <?= $form->field($model, 'is_cash_back')
                ->label('Докупить "Пакет компенсации" ('. Currency::getValueFromAtom($item['cash_back_price'], Currency::LOT).' LOT)')
                ->hint('Пакет будет куплен на каждый билет')
            ?>
        <?php } ?>

        <p>Сумма списания с баланса eLottery: <span id="price"></span> <span class="label label-info" style="margin-left:5px;">LOT</span></p>
        <hr>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Подтвердить покупку']); ?>

    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Уважаемый <?= Yii::$app->user->identity->getName2() ?>! Ваше участие в лотерее подтверждено. Всю информацию по розыгрышу Вы будете получать по адресу е-mail и в Telegram. Желаем Вам удачи и профита! Ваш идентификатор при розыгрыше лота: <span id="ids"></span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>