<?php

/** @var $this \yii\web\View  */
/** @var $request \common\models\Bilet */

use common\models\avatar\Currency;
use common\models\CurrencyIO;
use common\widgets\FileUpload7\FileUpload;
use cs\services\DatePeriod;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\shop\Request;


$this->title = 'Заказ #' . $request->id;


?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $request,
        ]) ?>

        <h1 class="page-header">Лот</h1>
        <?= \yii\widgets\DetailView::widget([
            'model'      => \common\models\Lot::findOne($request->lot_id),
        ]) ?>

        <p><a href="<?= Url::to(['cabinet-lot/play', 'id' => $request->lot_id]) ?>" class="btn btn-primary">Участовать в розыгрыше</a></p>

    </div>
</div>


