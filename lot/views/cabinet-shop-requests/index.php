<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'МОИ БИЛЕТЫ';


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-lot/play' + '?' + 'id' + '=' + $(this).data('lot_id');
});
JS
    );
    $sort = new \yii\data\Sort([
        'defaultOrder' => ['created_at' => SORT_DESC],
    ]);
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\Bilet::find()
                ->select([
                    'bilet.*',
                    'lot.winner_id',
                ])
                ->innerJoin('lot', 'lot.id = bilet.lot_id')
                ->where(['bilet.user_id' => Yii::$app->user->id])
                ->asArray()
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => $sort,
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id'], 'lot_id' => $item['lot_id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'lot_id:text:ID розыгрыша',
            [
                'header'  => 'Лот',
                'content' => function ($item) {
                    $lot = \common\models\Lot::findOne($item['lot_id']);

                    return Html::img(\iAvatar777\widgets\FileUpload7\FileUpload::getFile($lot->image, 'crop'), ['class' => 'thumbnail', 'width' => 70, 'style' => 'margin-bottom: 0px;', 'data' => ['toggle' => 'tooltip'], 'title' => $lot->name]);
                },
            ],
            [
                'header'  => 'Главный приз',
                'content' => function ($item) {
                    $lot = \common\models\Lot::findOne($item['lot_id']);
                    $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LOT);
                    $cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

                    $prize = '';
                    if ($lot->prize_type == \common\models\Lot::PRIZE_TYPE_MONEY) {
                        if (!\cs\Application::isEmpty($lot['prize_name'])) {
                            $prize = $lot['prize_name'];
                        } else {
                            $prize = \common\models\piramida\Currency::getValueFromAtom($lot['prize_amount'], \common\models\piramida\Currency::LOT) . $cHtml;
                        }
                    }
                    if ($lot->prize_type == \common\models\Lot::PRIZE_TYPE_THING) {
                        $prize = $lot->prize_name;
                    }

                    return $prize;
                },
            ],
            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                },
            ],
            [
                'header'  => 'Пакет компенсации',
                'content' => function ($item) {
                    if (!is_null($item['is_cash_back'])) {
                        if ($item['is_cash_back'] == 1) return Html::tag('span', 'Есть', ['class' => 'label label-success']);
                    }

                    return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                },
            ],
            [
                'header'  => 'Номер билета',
                'content' => function ($item) {
                    if (is_null($item['num'])) return '';
                    $num = substr($item['num'], 6);

                    return 'Билет №' . intval($num);
                },
            ],
            [
                'header'  => '',
                'content' => function ($item) {
                    if ($item['winner_id'] == $item['id']) {
                        return Html::tag('span', 'winner', ['class' => 'label label-info']);
                    }

                },
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>