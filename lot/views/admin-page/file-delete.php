<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\BlogItem */

$this->title = 'Удалить файл';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS

$('.buttonDelete').click(function (e) {
    if ($('.inputFile').val() == '') {
        alert('Поле не должгл быть пустым');
        return;
    }
    ajaxJson({
        url: '/admin-page/file-delete-ajax',
        data: {file: $('.inputFile').val()},
        success: function (ret) {
            new Noty({
                timeout: 1000,
                theme: 'relax',
                type: 'success',
                layout: 'bottomLeft',
                text: 'Успешно'
            }).show();
            
            $('.inputFile').val('');
        }
    })
});
JS
);
$model = new \avatar\models\validate\AdminPageFileDeleteAjax();
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model' => $model,
                        'success' => <<<JS
function (ret) {
    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Успешно'
    }).show();
    
    $('#adminpagefiledeleteajax-url').val('');
}
JS


                ])?>
                <?= $form->field($model, 'url') ?>

                <?php $form = \iAvatar777\services\FormAjax\ActiveForm::end()?>

            </div>
        </div>
    </div>
</div>
