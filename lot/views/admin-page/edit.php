<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\BlogItem */

$this->title = $model->name;
\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <div class="alert alert-success">
                Успешно обновлено.
            </div>

            <?php $id = Yii::$app->session->getFlash('form'); ?>
            <p>
                <a href="<?= \yii\helpers\Url::to(['admin-page/index']) ?>" class="btn btn-default">Все страницы</a>
            </p>

        <?php  } else { ?>

            <div class="row">
                <div class="col-lg-5">
                    <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                        'id'        => 'upload' . 1,
                        'name'      => 'upload' . 1,
                        'attribute' => 'upload' . 1,
                        'model'     => new \avatar\models\forms\ChatFile(),
                        'update'    => [],
                        'settings'  => [
                            'maxSize'           => 50 * 1000,
                            'controller'        => 'upload4',
                            'button_label'      => Yii::t('c.xgKKA2JpVp', 'Прикрепите файл'),
                            'functionSuccess'   => new \yii\web\JsExpression(<<<JS
function (response) {
    // response.url
    $('.buttonCopy').removeClass('hide');
    $('.buttonCopy').attr('data-clipboard-text', response.url);
    return false;
}
JS
                            ),
                        ],
                    ]); ?>
                    <button class="btn btn-default btn-xs hide buttonCopy" role="">Копировать</button>
                    <hr>
                    <?php $form = ActiveForm::begin([
                        'id'      => 'contact-form',
                        'options' => ['enctype' => 'multipart/form-data']
                    ]); ?>
                    <?= $model->field($form, 'name') ?>

                    <?= $model->field($form, 'content') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Обновить', [
                            'class' => 'btn btn-default',
                            'name'  => 'contact-button',
                            'style' => 'width:100%',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        <?php } ?>
    </div>
</div>
