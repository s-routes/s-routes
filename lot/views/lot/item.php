<?php

use common\models\piramida\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $item \common\models\Lot */

$this->title = $item->name;

$c = Currency::findOne(Currency::LOT);

$cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);

$type = '';
if ($item->type == \common\models\Lot::TYPE_AUTO) $type = 'Автоматизированный';
if ($item->type == \common\models\Lot::TYPE_MANUAL) $type = 'Online трансляция';

// кол-во купленных билетов
$bilet_pay_count = \common\models\Bilet::find()->where(['lot_id' => $item['id']])->count();

$prize = '';
if ($item->prize_type == \common\models\Lot::PRIZE_TYPE_MONEY) {
    if (!\cs\Application::isEmpty($item['prize_name'])) {
        $prize = $item['prize_name'];
    } else {
        $prize = Currency::getValueFromAtom($item['prize_amount'], Currency::LOT) . $cHtml;
    }
}
if ($item->prize_type == \common\models\Lot::PRIZE_TYPE_THING) {
    $prize = $item->prize_name;
}
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">

        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <div class="row">
            <div class="col-lg-4">
                <p><img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>"></p>
            </div>
            <div class="col-lg-8">

                <?php
                $attributes = [
                    [
                        'label'          => 'Розыгрыш',
                        'captionOptions' => ['style' => 'width: 30%'],
                        'value'          => $type,
                    ],
                    [
                        'label'  => 'Краткая информация о лоте',
                        'format' => 'html',
                        'value'  => nl2br($item['description']),
                    ],
                    [
                        'label'  => 'Главный Приз',
                        'format' => 'html',
                        'value'  => $prize,
                    ],
                    [
                        'label'  => 'Количество участников',
                        'format' => 'html',
                        'value'  => $item['bilet_count'],
                    ],
                    [
                        'label'  => 'Шансы на выигрыш',
                        'format' => 'html',
                        'value'  => Yii::$app->formatter->asDecimal(((1 / $item->bilet_count) * 100), 2) . ' %',
                    ],
                    [
                        'label'  => 'Стоимость билета eLottery',
                        'format' => 'html',
                        'value'  => Currency::getValueFromAtom($item['price'], Currency::LOT) . $cHtml,
                    ],

                    'cash_back_percent:text:Cashback от стоимости билета (%)',
                    'time_start:datetime:Время начала продаж билетов',
                    'time_finish:datetime:Время окончания продаж билетов',
                    'time_raffle:datetime:Время розыгрыша Главного Приза',
                ];
                if ($item['type'] == \common\models\Lot::TYPE_MANUAL) {
                    $attributes[] = [
                        'attribute' => 'link_youtube',
                        'format'    => ['url', ['target' => '_blank']],
                        'label'     => 'Ссылка Youtube',
                    ];
                    $attributes[] = [
                        'attribute' => 'link_instagram',
                        'format'    => ['url', ['target' => '_blank']],
                        'label'     => 'Ссылка Инстаграм',
                    ];
                    $attributes[] = [
                        'attribute' => 'link_telegram',
                        'format'    => ['url', ['target' => '_blank']],
                        'label'     => 'Ссылка Telegram',
                    ];
                }
                ?>


                <?= \yii\widgets\DetailView::widget([
                    'model'      => $item,
                    'attributes' => $attributes,
                ]) ?>

                <p>
                    <?php
                    $isShow = false;
                    if (!Yii::$app->user->isGuest) {
                        if (\common\models\Bilet::find()->where(['lot_id' => $item->id, 'user_id' => Yii::$app->user->id])->exists()) {
                            $isShow = true;
                        }
                    }
                    $options = [
                        'class' =>   'btn btn-primary',
                    ];
                    if ($isShow) {
                        $b1 = Html::a('СТРАНИЦА РОЗЫГРЫША<br>(для участников)', ['cabinet-lot/play', 'id' => $item->id], $options);
                    } else {
                        $options['disabled'] = 'disabled';
                        $b1 = Html::button('СТРАНИЦА РОЗЫГРЫША<br>(для участников)', $options);
                    }
                    ?>
                    <?= $b1 ?>

                    <a href="<?= Url::to(['lot/view', 'id' => $item->id]) ?>" class="btn btn-primary">
                        СТРАНИЦА РОЗЫГРЫША<br>
                        (для зрителей)
                    </a>
                </p>
            </div>
        </div>


        <?php if ($item['time_raffle'] < time()) { ?>
            <p class="label label-danger">Разыграна</p>
        <?php } ?>
        <?php

        // кол-во всех билетов
        $bilet_count = $item['bilet_count'];

        $percent = (int)($bilet_pay_count / $bilet_count * 100);
        ?>
        <p>Кол-во купленных билетов: <?= $bilet_pay_count ?>/<?= $bilet_count ?></p>
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="<?= $percent ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= $percent ?>%;">
                <span class="sr-only"><?= $percent ?>% Complete</span>
            </div>
        </div>


        <p>Условия лотереи: <a href="/page/item?id=13">Дисклеймер</a></p>

        <div class="row">
            <div class="col-lg-2">
                <?php
                $this->registerJs(<<<JS
        

$('#lotitem-dis').change(function(e) {
    if ($('#lotitem-dis').prop('checked')) {
        $('.js-button').removeAttr('disabled');
    } else {
        $('.js-button').attr('disabled', 'disabled');
    } 
});
$('.js-button').click(function(e) {
    window.location = $(this).attr('href');
});
JS
                )
                ?>
                <?= \common\widgets\CheckBox2\CheckBox::widget([
                    'model'         => new \lot\models\forms\LotItem,
                    'attribute'     => 'dis'
                ]) ?>
            </div>
            <div class="col-lg-8">
                <?php if ($bilet_pay_count < $bilet_count) { ?>
                    <div>
                        <p>
                            <button href="/cabinet-lot/buy-many?id=<?= $item->id ?>" class="btn btn-success js-button" disabled="disabled">Купить лотерейные билеты</button>
                        </p>
                    </div>
                <?php } else { ?>
                    <p class="alert alert-info">Все билеты распроданы</p>
                <?php } ?>
            </div>
            <div class="col-lg-2">
                <a href="/cabinet-shop-requests/index" class="btn btn-primary">Ваши лотереи</a>
            </div>
        </div>




        <h2>Подробная информация о лоте</h2>
        <div>
            <?= $item->content ?>
        </div>


    </div>

</div>


