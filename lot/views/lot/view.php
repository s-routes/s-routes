<?php
use common\models\piramida\Currency;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $item \common\models\Lot */

$this->title = $item->name;

if (is_null($item->room_id)) {
    $room = \common\models\ChatRoom::add([]);
    $item->room_id = $room->id;
    $room_id = $room->id;
    $item->save();
} else {
    $room_id = $item->room_id;
}

$c = Currency::findOne(Currency::LOT);
$cHtml = Html::tag('span', $c->code, ['class' => 'label label-info', 'style' => 'margin-left:5px;']);


$prize = '';
if ($item->prize_type == \common\models\Lot::PRIZE_TYPE_MONEY) {
    if (!\cs\Application::isEmpty($item['prize_name'])) {
        $prize = $item['prize_name'];
    } else {
        $prize = Currency::getValueFromAtom($item['prize_amount'], Currency::LOT) . $cHtml;
    }
}
if ($item->prize_type == \common\models\Lot::PRIZE_TYPE_THING) {
    $prize = $item->prize_name;
}

if (Yii::$app->user->isGuest) {
    $uid = 0;
} else {
    $uid = Yii::$app->user->id;
}



if ($item->time_finish > time()) {
    $this->registerJs(<<<JS
    
// Проверяю раз в 10 сек запуск счетчика обратного отсчета
var IntervalFinish;

IntervalFinish = setInterval(function(e) {
    ajaxJson({
        url: '/lot/is-finish',
        data: {id: {$item->id}},
        success: function(ret) {
            if (ret.is_time_finish == 1) {
                window.location.reload();
            }
        }
    });
}, 1000 * 10);

JS
    );
}


function sec2time($diff)
{
    $sec = $diff % 60;
    $diffOstatok = $diff - $sec;

    if ($diffOstatok > 0) {
        $min = ($diffOstatok % (60*60)) / 60;
        $diffOstatok = $diffOstatok - $min * 60;
        if ($diffOstatok > 0) {
            $hour = ($diffOstatok % (60*60*24)) / (60*60);
            $diffOstatok = $diffOstatok - $hour * 60*60;
            if ($diffOstatok > 0) {
                $day = $diffOstatok;
            } else {
                $day = 0;
            }
        } else {
            $hour = 0;
            $day = 0;
        }
    } else {
        $min = 0;
        $hour = 0;
        $day = 0;
    }

    if ($day < 10) $day = '0' . $day;
    if ($hour < 10) $hour = '0' . $hour;
    if ($min < 10) $min = '0' . $min;
    if ($sec < 10) $sec = '0' . $sec;

    return $day . ':' . $hour . ':' . $min . ':' . $sec;
}
?>
<style>
    .cntSeparator {
        font-size: 400%;
    }
</style>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">

        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>

        <div class="row">
            <div class="col-lg-4">
                <p>
                    <img src="<?= \iAvatar777\widgets\FileUpload7\FileUpload::getFile($item->image, 'crop') ?>">
                </p>
            </div>
            <div class="col-lg-8">
                <p><?= nl2br($item['description']) ?></p>

            </div>
        </div>

        <?php
        $attributes = [
            'id',
            [
                'label'  => 'Главный Приз',
                'format' => 'html',
                'value'  => $prize,
            ],
            [
                'label' => 'Цена билета',
                'value' => $item['price'] / 100
            ],
            'cash_back_percent:text:% кэшбека пакета компенсации',
            'time_start:datetime:Время начала продаж билетов',
            'time_finish:datetime:Время окончания продаж билетов',
            'time_raffle:datetime:Время розыгрыша Главного Приза',
        ];
        if ($item['type'] == \common\models\Lot::TYPE_MANUAL) {
            $attributes[] = [
                'attribute' => 'link_youtube',
                'format'    => ['url', ['target' => '_blank']],
                'label'    => 'Ссылка Youtube',
            ];
            $attributes[] = [
                'attribute' => 'link_instagram',
                'format'    => ['url', ['target' => '_blank']],
                'label'    => 'Ссылка Инстаграм',
            ];
            $attributes[] = [
                'attribute' => 'link_telegram',
                'format'    => ['url', ['target' => '_blank']],
                'label'    => 'Ссылка Telegram',
            ];
        }
        ?>

        <?= \yii\widgets\DetailView::widget([
            'model'      => $item,
            'attributes' => $attributes,
        ]) ?>
        <hr>

        <?php if ($item->is_counter_stop == 0 && $item->is_counter_start == 0) { ?>
            <?= $this->render('../cabinet-lot/play-0', ['item' => $item, 'room_id' => $room_id]) ?>
        <?php } ?>

        <?php if ($item->is_counter_stop == 0 && $item->is_counter_start == 1) { ?>
            <?php if ($item->type == \common\models\Lot::TYPE_AUTO) { ?>
                <?= $this->render('../cabinet-lot/play-1-1', ['item' => $item, 'room_id' => $room_id]) ?>
            <?php } ?>
            <?php if ($item->type == \common\models\Lot::TYPE_MANUAL) { ?>
                <?= $this->render('../cabinet-lot/play-1-2', ['item' => $item, 'room_id' => $room_id]) ?>
            <?php } ?>
        <?php } ?>

        <?php if ($item->is_counter_stop == 1 && $item->is_counter_start == 1) { ?>
            <?= $this->render('../cabinet-lot/play-2', ['item' => $item, 'room_id' => $room_id]) ?>
        <?php } ?>

        <?php // Если время до начала розыгрыша и после окончания продаж ?>
        <?php if (($item->time_finish < time()) && ($item->time_raffle > time())) { ?>
            <div class="row" id="counter_div">
                <div class="col-lg-12">
                    <p>Время до начала розыгрыша:</p>
                    <?php
                    $asset = \cs\Widget\CountDown\Asset::register($this);
                    // Вычисляю сколько осталось секунд до начала розыгрыша
                    $diff = $item->time_raffle - time();
                    $startTime = sec2time($diff);

                    $this->registerJs(<<<JS
                        
$('#counter').countdown({startTime: "{$startTime}",image: "{$asset->baseUrl}/img/digits.png"});
JS
                    )
                    ?>
                    <div id="counter"></div>
                </div>
            </div>
            <div class="row">
                <p style="
        margin: 0px 0px 0px 25px;
        font-size: 200%;
    ">дней : <span style="margin-left: 62px; ">час</span> : <span style="margin-left: 80px; ">мин</span> : <span style="margin-left: 78px; ">сек</span></p>
            </div>
        <?php } ?>


        <?php if (!Yii::$app->user->isGuest) { ?>
            <hr>

            <h2>Чат</h2>
            <?= $this->render('@shop/views/cabinet-exchange/chat', [
                'room_id'  => $room_id,
                'user_id'  => Yii::$app->user->id,
                'send_url' => '/cabinet-lot/send',
            ]); ?>
        <?php } ?>

    </div>

</div>


