<?php

/** $this \yii\web\View  */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('c.xaREK0rYPp', 'Записная книжка');


?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8">

        <p><a href="<?= Url::to(['cabinet-notepad/add']) ?>" class="btn btn-default"><?= \Yii::t('c.xaREK0rYPp', 'Добавить') ?></a></p>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $str778 = Yii::t('c.xaREK0rYPp', 'Подтвердите удаление');
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();
$('.buttonDelete').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('{$str778}')) {
        var button = $(this);
        var id = $(this).data('id');
        ajaxJson({
            url: '/cabinet-notepad/delete' + '?' + 'id' + '=' + id,
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    button.parent().parent().remove();
                }).modal();
            }
        });
    }
});
$('.rowTable').click(function(e) {
    window.location = '/cabinet-notepad/edit' + '?' + 'id' + '=' + $(this).data('id');
});

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\LotNote::find()
                    ->where(['user_id' => Yii::$app->user->id])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'summary' => '',
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => Yii::t('c.xaREK0rYPp', 'Название'),
                    'content' => function ($item) {
                        return $item['name'];
                    },
                ],
                [
                    'header'  => Yii::t('c.xaREK0rYPp', 'Создано'),
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => Yii::t('c.xaREK0rYPp', 'Удалить'),
                    'content' => function ($item) {
                        return Html::button(Yii::t('c.xaREK0rYPp', 'Удалить'), [
                            'class' => 'btn btn-danger btn-xs buttonDelete',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>


    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>

</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>