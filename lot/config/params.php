<?php

return [
    'adminEmail'         => 'admin@galaxysss.ru',
    'mailList'           => [
        'contact' => 'god@avatar-bank.com',
    ],

    'mailer'                        => [
        // адрес который будет указан как адрес отправителя для писем и рассылок
        'from' => [
            env('MAILER3_FROM_EMAIL') => env('MAILER3_FROM_NAME'),
        ],
    ],

    'rootSite' => 'https://neiro-n.com',
    'google' => [
        'plugins' => [
            'reCaptcha' => [
                'key'       => '6Lf7ZbkZAAAAABcHhTd9eg6P0bWYbdjqbm-O3nYe',
                'secretKey' => '6Lf7ZbkZAAAAAL1uSY3NaaZUz8SP069X1Ow1GS11',
            ],
        ],
    ],
];
