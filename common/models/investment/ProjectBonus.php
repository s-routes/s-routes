<?php

namespace common\models\investment;

use common\models\UserAvatar;
use cs\services\VarDumper;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property int    id            Идентификатор проекта
 * @property int    type          Тип 1 - проценты, 2 - безусловный доход
 * @property float    percent     Проценты
 * @property float    sum         безусловный доход
 *
 */
class ProjectBonus extends ActiveRecord
{
    const TYPE_PERCENT = 1;
    const TYPE_INC = 2;

    public static function tableName()
    {
        return 'projects_bonus';
    }
}