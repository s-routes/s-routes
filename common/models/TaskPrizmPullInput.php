<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer user_id
 * @property integer created_at
 * @property integer txid_sb
 * @property integer txid_sbp
 * @property integer amount
 *
 * @package common\models
 */
class TaskPrizmPullInput extends ActiveRecord
{
    public static function tableName()
    {
        return 'task_prizm_pull_input';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}