<?php

namespace common\models;

use Yii;
use common\extensions\XSSActiveRecord;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * Интерфейс для указания транзакции покупателем
 */
interface PaymentI
{
    /**
     *  устанавливает параметр транзакцию
     * @inheritdoc
     */
    public function setTransaction($txid);

    /**
     * выдает транзакцию
     * @inheritdoc
     */
    public function getTransaction();

    /**
     *  Сохраняет транзакцию
     */
    public function saveTransaction($txid);

    /**
     * @return string
     */
    public function getAddress();
}
