<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 * Class SubscribeMail
 * @package app\models
 *
 * @property int    id
 * @property int    subscribe_id
 * @property string mail
 */
class SubscribeMail extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_subscribe_mails';
    }

    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;

        return $iam;
    }
}