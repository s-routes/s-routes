<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 * Class Subscribe
 * @package app\models
 *
 * @property int    date_insert
 * @property string html
 * @property string subject
 */
class SubscribeOriginal extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_subscribe_original';
    }
    
    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;
        
        return $iam;
    }
}