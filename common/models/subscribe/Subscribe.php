<?php


namespace common\models\subscribe;


use yii\db\ActiveRecord;

/**
 * Class Subscribe
 * @package app\models
 *
 * @property int    id
 * @property int    date_insert
 * @property string html
 * @property string subject
 * @property string from_name
 * @property string from_email
 * @property string mailer
 * @property int    type        \app\services\Subscribe::TYPE_*
 */
class Subscribe extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_subscribe';
    }
    
    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;
        
        return $iam;
    }
}