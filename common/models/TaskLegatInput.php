<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property string  account
 * @property string  txid
 * @property integer user_id
 * @property integer status         0 - задача создана, клиент скинул деньги. 1 - задача подтверждена магазином.
 * @property integer created_at
 * @property integer finished_at
 * @property integer amount
 * @property integer txid_internal
 *
 * @package common\models
 */
class TaskLegatInput extends ActiveRecord
{
    public static function tableName()
    {
        return 'task_legat_input';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}