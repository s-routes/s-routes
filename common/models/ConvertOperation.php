<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer created_at
 * @property integer wallet_from_id
 * @property integer wallet_to_id
 * @property integer tx_from_id
 * @property integer tx_to_id
 * @property integer kurs
 * @property integer amount
 * @property integer amount_to
 * @property integer from_currency_id
 * @property integer to_currency_id
 * @property float   from_price_usd
 * @property float   to_price_usd
 *
 */
class ConvertOperation extends ActiveRecord
{
    public static function tableName()
    {
        return 'convert_operation';
    }

    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}