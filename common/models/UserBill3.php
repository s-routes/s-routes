<?php

namespace common\models;

use common\models\piramida\Wallet;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer wallet_id
 * @property integer password
 * @property integer currency_id        dbWallet.currency.id
 *
 * @package common\models
 */
class UserBill3 extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_bill';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public static function getDb()
    {
        return \Yii::$app->dbAvatar;
    }
}