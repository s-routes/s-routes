<?php

namespace common\models;

use common\models\PaySystem;
use cs\base\DbRecord;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;


/**
 * @property integer id
 * @property integer paysystem_id
 * @property string  model
 * @property string  view
 */
class PaySystemSuccess extends ActiveRecord
{
    /** @var  \common\models\PaySystem */
    private $_paySystem;

    public static function tableName()
    {
        return 'paysystems_success';
    }

    /**
     * @return \common\models\PaySystem
     */
    public function getPaySystem()
    {
        if (is_null($this->_paySystem)) {
            $this->_paySystem = $this->_getPaySystem();
        }

        return $this->_paySystem;
    }

    /**
     * @return \common\models\PaySystem
     */
    public function _getPaySystem()
    {
        return PaySystem::findOne($this->paysystem_id);
    }
}