<?php

namespace common\models;

use common\models\piramida\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    currency_ext_id
 * @property integer    currency_int_id
 * @property integer    main_wallet
 * @property integer    block_wallet
 * @property integer    lost_wallet
 * @property integer    is_exchange
 * @property integer    is_io
 * @property integer    info_page_id
 * @property integer    info_page_shop_id
 * @property integer    is_show_comission
 * @property integer    output_min
 * @property integer    benance_min
 * @property integer    decimals
 * @property string     master_wallet
 * @property integer    master_wallet_is_binance
 * @property integer    is_api                  Флаг, есть ли API для зачисления моенеты, 0 - нет API, 1 - есть API
 * @property string     function_check
 * @property string     validate_account
 * @property string     extended_info
 * @property string     tab_title
 * @property string     field_name_wallet
 * @property string     warning                 Предупреждение для формы ввода
 * @property double     comission_out
 * @property double     benance_fee
 */
class CurrencyIO extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_io';
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }

    public static function findFromExt($currency_ext_id)
    {
        return self::findOne(['currency_ext_id' => $currency_ext_id]);
    }

    public static function findFromInt($currency_int_id)
    {
        return self::findOne(['currency_int_id' => $currency_int_id]);
    }

}
