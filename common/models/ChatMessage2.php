<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id
 * @property string message
 * @property int    room_id
 * @property int    user_id
 * @property int    created_at
 *
 *
 */
class ChatMessage2 extends ActiveRecord
{
    public $user;


    public static function tableName()
    {
        return 'chat_message2';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @return  \common\models\UserAvatar
     */
    public function getUser()
    {
        if (is_null($this->user)) {
            $this->user = UserAvatar::findOne($this->user_id);
        }

        return $this->user;
    }

    public static function add($fields)
    {
        $iam = new self($fields);
        $iam->save();
        $iam->id = \Yii::$app->db->lastInsertID;

        return $iam;
    }
}