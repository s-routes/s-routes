<?php

namespace common\models\shop;

use app\models\Union;
use app\services\Subscribe;
use common\models\shop\DostavkaItem;
use cs\Application;
use cs\services\BitMask;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Class RequestProduct
 *
 * @property int    id
 * @property int    request_id
 * @property int    product_id
 * @property int    count
 *
 * @package common\models\shop
 */
class RequestProduct extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_users_shop_requests_products';
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}