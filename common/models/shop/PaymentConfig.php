<?php

namespace common\models\shop;

use app\models\Shop\Payments;
use app\services\Subscribe;
use common\models\PaySystem;
use cs\base\DbRecord;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Url;


/**
 * Конфиг платежной системы для магазина
 *
 * @property integer id
 * @property integer union_id
 * @property integer paysystem_id
 * @property string config
 */
class PaymentConfig extends ActiveRecord
{
    const CODE_YANDEX   = 'yandex';
    const CODE_BIT_COIN = 'bit-coin';

    /** @var  \common\models\PaySystem */
    private $_paySystem;

    public static function tableName()
    {
        return 'gs_unions_shop_payments';
    }

    public function rules()
    {
        return [
            [['config'], 'required'],
            [['config'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'config' => 'Параметры платежной системы',
        ];
    }

    /**
     * @return \common\models\PaySystem
     */
    public function getPaySystem()
    {
        if (is_null($this->_paySystem)) {
            $this->_paySystem = $this->_getPayment();
        }

        return $this->_paySystem;
    }

    /**
     * @return \common\models\PaySystem
     */
    public function _getPayment()
    {
        return PaySystem::findOne($this->paysystem_id);
    }


    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->getPaySystem()->getClassName();
    }

    /**
     * @return \app\models\Piramida\WalletSourceInterface
     */
    public function getClass()
    {
        $class = $this->getPaySystem()->getClassName();
        return new $class([
            'config'      => $this->config,
            'paymentType' => $this->paysystem_id,
        ]);
    }

}