<?php

namespace common\models\shop;

use common\models\avatar\UserBill;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use cs\Application;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Class Request
 *
 * @property int    id
 * @property int    user_id
 * @property int    school_id
 * @property int    status
 * @property int    created_at
 * @property string comment
 * @property string address
 * @property string phone
 * @property string dop
 * @property string name    Имя клиента
 * @property int    is_answer_from_shop
 * @property int    is_answer_from_client
 * @property int    last_message_time
 * @property int    dostavka_id
 * @property int    price                   Стоимость без доставки
 * @property int    is_canceled
 * @property int    is_paid
 * @property int    is_paid_client
 * @property int    is_hide
 * @property int    parent_id
 * @property int    sum                     Стоимость с доставкой
 * @property int    billing_id
 * @property int    korobka_id              gs_unions_shop_product_korobka.id
 * @property int    is_internal             Флаг 0 - заказ внутренний магазин (по умолчанию), 1 - заказ внешний
 * @property int    currency_id             db.currency.id для поля `price`
 * @property int    chat_id
 * @property int    txid_coin
 * @property int    txid_korobka
 *
 * @package common\models\shop
 */
class Request extends ActiveRecord
{
    const EVENT_BEFORE_SHOP_SUCCESS = 'beforeShopSuccess';
    const EVENT_AFTER_SHOP_SUCCESS = 'afterShopSuccess';

    const STATUS_USER_NOT_CONFIRMED = 1; // Пользователь заказал с регистрацией пользователя, но не подтвердил свою почту еще

    const STATUS_SEND_TO_SHOP = 2;       // заказ отправлен в магазин
    const STATUS_ORDER_DOSTAVKA = 3;     // клиенту выставлен счет с учетом доставки
    const STATUS_PAID_CLIENT = 5;        // заказ оплачен со стороны клиента
    const STATUS_PAID_SHOP = 6;          // оплата подтверждена магазином
    const STATUS_REJECTED = 17;          // Заказ отменен клиентом

    // статусы для доставки
    const STATUS_DONE = 8;               // заказ отгружен


    const DIRECTION_TO_CLIENT = 1;
    const DIRECTION_TO_SHOP = 2;

    public static $statusList = [
        self::STATUS_REJECTED               => [
            'client'   => 'Заказ отменен',
            'shop'     => 'Пользователь отменил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-remove',
                'color' => 'danger',
            ],
        ],
        self::STATUS_USER_NOT_CONFIRMED               => [
            'client'   => 'Пользователь не подтвердил почту',
            'shop'     => 'Пользователь не подтвердил почту',
            'timeLine' => [
                'icon'  => 'glyphicon-minus',
                'color' => 'default',
            ],
        ],
        self::STATUS_SEND_TO_SHOP                     => [
            'client'   => 'Отпрален в магазин',
            'shop'     => 'Пользователь отправил заказ',
            'timeLine' => [
                'icon'  => 'glyphicon-ok',
                'color' => 'default',
            ],
        ],
        self::STATUS_ORDER_DOSTAVKA                   => [
            'client'   => 'Выставлен счет с учетом доставки',
            'shop'     => 'Выставлен счет с учетом доставки',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'default',
            ],
        ],
        self::STATUS_PAID_CLIENT                      => [
            'client'   => 'Оплата сделана',
            'shop'     => 'Оплата сделана',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'warning',
            ],
        ],
        self::STATUS_PAID_SHOP                        => [
            'client'   => 'Оплата подтверждена',
            'shop'     => 'Оплата подтверждена',
            'timeLine' => [
                'icon'  => 'glyphicon-credit-card',
                'color' => 'success',
            ],
        ],

        self::STATUS_DONE           => [
            'client'   => 'Заказ исполнен',
            'shop'     => 'Заказ отгружен',
            'timeLine' => [
                'icon'  => 'glyphicon-thumbs-up',
                'color' => 'success',
            ],
        ],
    ];

    public static function tableName()
    {
        return 'gs_users_shop_requests';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * Клиент подтвердил отправку денег
     */
    public function successClient($message = null)
    {
        if (!is_null($message)) {
            $this->addStatusToShop([
                'status'  => self::STATUS_PAID_CLIENT,
                'message' => $message,
            ]);
        } else {
            $this->addStatusToShop(self::STATUS_PAID_CLIENT);
        }
        $this->is_paid_client = 1;
        $this->save();
    }

    /**
     * @return \yii\db\Query
     */
    public function getMessages()
    {
        return RequestMessage::find()->where(['request_id' => $this->id]);
    }

    /**
     * @return \common\models\school\School
     */
    public function getSchool()
    {
        return School::findOne($this->school_id);
    }

    /**
     * Магазин подтвердил приход денег
     */
    public function successShop()
    {
        $this->trigger(self::EVENT_BEFORE_SHOP_SUCCESS);

        // Ставлю статус оплачено
        $this->is_paid = 1;
        $this->save();

        $this->addStatusToClient(self::STATUS_PAID_SHOP);

        $this->trigger(self::EVENT_AFTER_SHOP_SUCCESS);
    }

    /**
     * Добавить статус
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addStatus($status, $direction)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, $direction);
    }

    /**
     * Добавить статус к клиенту
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addStatusToClient($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить статус в магазин
     *
     * @param int | array $status статус self::STATUS_* или массив со статусом и сообщением
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addStatusToShop($status)
    {
        if (!is_array($status)) {
            $fields = [
                'status' => $status,
            ];
        } else {
            $fields = $status;
        }

        return $this->addMessageItem($fields, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение
     *
     * @param string $message сообщение
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessage($message, $direction)
    {
        return $this->addMessageItem([
            'message' => $message,
        ], $direction);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessageToClient($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_CLIENT);
    }

    /**
     * Добавить сообщение для клиента
     *
     * @param string $message сообщение
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessageToShop($message)
    {
        return $this->addMessage($message, self::DIRECTION_TO_SHOP);
    }

    /**
     * Добавить сообщение или статус
     *
     * @param array $fields поля для сообщения
     * @param int $direction направление сообщения self::DIRECTION_*
     *
     * @return \common\models\shop\RequestMessage
     */
    public function addMessageItem($fields, $direction)
    {
        $fieldsRequest = [
            'is_answer_from_shop'   => ($direction == self::DIRECTION_TO_CLIENT) ? 1 : 0,
            'is_answer_from_client' => ($direction == self::DIRECTION_TO_CLIENT) ? 0 : 1,
            'last_message_time'     => time(),
        ];
        if (isset($fields['status'])) {
            $fieldsRequest['status'] = $fields['status'];
        }
        if ($direction == self::DIRECTION_TO_SHOP) {
            //$fieldsRequest['is_hide'] = null;
        }
        foreach ($fieldsRequest as $k => $v) {
            $this->$k = $v;
        }
        $this->save();

        return RequestMessage::add(ArrayHelper::merge($fields, [
            'request_id' => $this->id,
            'direction'  => $direction,
        ]));
    }

}