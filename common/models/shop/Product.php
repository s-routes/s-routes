<?php

namespace common\models\shop;

use app\models\Union;
use app\services\Subscribe;
use common\models\school\School;
use common\models\shop\DostavkaItem;
use cs\Application;
use cs\services\BitMask;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * @property int    id
 * @property int    tree_node_id
 * @property string name
 * @property int    date_insert
 * @property int    sort_index
 * @property int    school_id
 * @property int    moderation_status
 * @property int    price
 * @property int    is_electron
 * @property int    attached_files
 * @property int    is_piramida
 * @property int    sales_count
 * @property int    oferta_id
 * @property int    is_send_vvb
 * @property int    is_korobka          Флаг, это коробка?
 * @property int    currency_id         db.currency.id
 * @property string electron_text
 * @property string description
 * @property string content
 * @property string image
 *
 */
class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_product';
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}