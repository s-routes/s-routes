<?php

namespace common\models\shop;

use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use common\models\school\ReferalLevel;
use common\models\school\School;
use cs\Application;
use cs\services\BitMask;
use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * @property int        $id
 * @property int        $request_id
 * @property int        $billing_id
 * @property int        $created_at
 * @property int        $amount
 * @property int        $currency_id        db.currency.id
 * @property int        $user_id
 * @property string     $comment
 */
class RequestAdd extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_users_shop_requests_add';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}