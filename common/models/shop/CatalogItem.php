<?php

namespace common\models\shop;

use common\models\shop\Product;
use cs\services\VarDumper;
use yii\base\BaseObject;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

/**
 * @property int id
 * @property int parent_id
 * @property int school_id
 * @property string name
 */
class CatalogItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'gs_unions_shop_tree';
    }


    public function behaviors()
    {
        return [
            [
                'class' => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @param $fields
     *
     * @return CatalogItem
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}