<?php

namespace common\models;

use common\models\piramida\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    currency_id
 * @property integer    user_id
 */
class CurrencyAdmin extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_admin';
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }
}
