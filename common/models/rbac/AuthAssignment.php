<?php

namespace common\models\rbac;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int    id            - идентификатор записи
 * @property int    user_id       - идентификатор кошелька
 * @property string item_name     - данные о транзакции из платежной системы, JSON
 *
 */
class AuthAssignment extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_billing';
    }
}