<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer created_at
 * @property string  name
 * @property string  content
 */
class Note extends ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
