<?php

namespace common\models\statistic;

use app\models\Transaction;
use common\models\User;
use common\extensions\XSSActiveRecord;
use cs\services\VarDumper;
use Yii;
use app\models\CrmLog;
use yii\db\ActiveRecord;

/**
 * @property integer id         идентификатор записи
 * @property integer time       момент времени
 * @property float   usd        сумма портфеля в долларах
 * @property integer user_id    пользователь
 */
class UserBinanceStatisticItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_binance_stat';
    }
    
    public function rules()
    {
        return [
            [['time', 'usd', 'user_id'], 'required'],
            [['time', 'user_id'], 'double'],
            [['usd'], 'double'],
        ];
    }

    public static function getDb()
    {
        return Yii::$app->dbStatistic;
    }

    /**
     * @param array $fields
     * @return \common\models\statistic\UserBinanceStatisticItem
     */
    public static function add($fields)
    {
        if (!isset($fields['time'])) {
            $fields['time'] = time();
        }
        $i = new static($fields);
        $ret = $i->save();
        if (!$ret) throw new \Exception('Не могу сохранить '.\yii\helpers\VarDumper::dumpAsString($i->errors));

        return $i;
    }

}
