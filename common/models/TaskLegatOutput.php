<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

/**
 * @property integer id
 * @property string  account
 * @property string  txid
 * @property integer status         Статус задачи 0 - задача создана. 1 - задача выполнена, транзакция указана
 * @property integer created_at
 * @property integer amount
 * @property integer finished_at
 * @property integer txid_internal
 * @property integer billing_id
 * @property integer currency_id    Идентификатор валюты для вывода, db.currency.id
 *
 * @package common\models
 */
class TaskLegatOutput extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_DONE = 1;
    const STATUS_HIDE = 2;

    public static function tableName()
    {
        return 'task_legat_output';
    }


    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}