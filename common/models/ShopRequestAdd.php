<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 02.09.2016
 * Time: 9:31
 */

namespace common\models;

use cs\services\VarDumper;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property int        $id
 * @property int        $request_id
 * @property int        $billing_id
 * @property int        $created_at
 * @property int        $amount
 * @property int        $currency_id
 * @property int        $user_id
 * @property string     $comment
 */
class ShopRequestAdd extends ActiveRecord
{
    public static function add($fields)
    {
        $new = new self($fields);
        $ret = $new->save();
        if (!$ret) {
            throw new Exception('Не удалось сохранить. '. \yii\helpers\VarDumper::dumpAsString($new->errors));
        }
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }
} 