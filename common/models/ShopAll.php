<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer type
 * @property integer status
 * @property string  link
 * @property string  about
 * @property string  description
 * @property string  account_login
 * @property string  account_password
 * @property string  rekvisit_karti
 *
 * @package common\models
 */
class ShopAll extends ActiveRecord
{
    const TYPE_FEDERAL = 1;
    const TYPE_REGION = 2;

    const STATUS_CREATE = 1;
    const STATUS_ACCEPT = 2;
    const STATUS_REJECT = 3;

    public function attributeLabels()
    {
        return [
            'link'        => 'Ссылка',
            'description' => 'Описание',
            'type'        => 'Тип',
            'about'       => 'Описание полное',
            'status'      => 'Статус',
        ];
    }
}