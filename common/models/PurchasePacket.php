<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer type_id
 * @property integer amount
 * @property integer user_id
 * @property integer packet_id
 * @property integer tx_id
 * @property integer created_at
 */
class PurchasePacket extends ActiveRecord
{
    const TYPE_ID_BUY = 1;
    const TYPE_ID_ADD = 2;

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
