<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use cs\Application;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use Yii;

/**
 *
 * @property int        id
 * @property int        billing_id
 * @property int        created_at
 * @property int        amount
 * @property int        user_id
 * @property int        is_paid
 * @property int        is_paid_client
 * @property int        tx_int_id
 */
class RequestInputRub extends ActiveRecord
{
    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * Уведомляет клиента
     * Начисляет деньги
     *
     * @throws \Exception
     */
    public function successShop()
    {
        if ($this->is_paid != 1) {
            $user_id = $this->user_id;
            $data = UserBill::getInternalCurrencyWallet(Currency::RUB, $user_id);
            /** @var \common\models\piramida\Wallet $wallet */
            $wallet = $data['wallet'];
            $cio = CurrencyIO::findFromInt(Currency::RUB);
            $fromWallet = Wallet::findOne($cio->main_wallet);

            // Получаю комиссию
            $ComissionBank = Config::get('ComissionBank');
            if ($ComissionBank === false) $ComissionBank = 0;

            // вычисляю сколько зачислить
            $amount = (int)((1 - ($ComissionBank / 100)) * $this->amount);
            $t = $fromWallet->move2($wallet, $amount, 'Зачисление на основании заявки #' . $this->id);
            $this->tx_int_id = $t['transaction']['id'];
            $this->is_paid = 1;
            $this->save();

            // отправка уведомления в Телеграм
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegramShop;

            $all = [$this->user_id];

            foreach ($all as $uid) {
                $u = UserAvatar::findOne($uid);
                if (!Application::isEmpty($u->telegram_shop_chat_id)) {
                    /** @var \common\models\UserAvatar $uCurrent */
                    $link1 = Url::to(['buh-billing/index'], true);
                    $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                    $telegram->sendMessage(['chat_id' => $u->telegram_shop_chat_id, 'text' => 'Cчет на пополнение RUB #' . $this->id . ' подтвержден' . "\n" .  "\n" . 'Ссылка на счета: ' . $link1  ]);
                }
            }
        }
    }

    /**
     * Уведомляет бухгалтера
     *
     * @throws \Exception
     */
    public function successClient()
    {
        if ($this->is_paid_client != 1) {
            // отправка уведомления в Телеграм
            /** @var \aki\telegram\Telegram $telegram */
            $telegram = Yii::$app->telegramShop;

            $role_shop_buh = Yii::$app->authManager->getUserIdsByRole('role_shop_buh');
            $all = $role_shop_buh;

            foreach ($all as $uid) {
                $u = UserAvatar::findOne($uid);
                if (!Application::isEmpty($u->telegram_shop_chat_id)) {
                    /** @var \common\models\UserAvatar $uCurrent */
                    $link1 = Url::to(['buh-billing/index'], true);
                    $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                    $telegram->sendMessage(['chat_id' => $u->telegram_shop_chat_id, 'text' => 'Создан счет на пополнение RUB #' . $this->id  . "\n" .  "\n" . 'Ссылка на счета: ' . $link1  ]);
                }
            }
        }
    }


}