<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 *
 * @property integer id
 * @property integer currency_id
 * @property string  txid
 * @property string  data
 * @property string  address
 * @property integer created_at
 *
 * @package common\models
 */
class MasterTransaction extends ActiveRecord
{
    public static function tableName()
    {
        return 'master_transaction';
    }

    public static function add($fields)
    {
        $i = new self($fields);
        $ret = $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }
}