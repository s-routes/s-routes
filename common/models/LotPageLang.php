<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 *
 * @property integer id
 * @property integer parent_id
 * @property string  language
 * @property string  name
 * @property string  content
 *
 * @package common\models
 */
class LotPageLang extends ActiveRecord
{
    public static function tableName()
    {
        return 'lot_page_lang';
    }

    public static function add($fields)
    {
        $i = new self($fields);
        $ret = $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}