<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 25.01.2021
 * Time: 0:05
 */

/**
 * Class BinanceOrder
 * @package common\models
 *
 * @property int    binance_order_id        binance.orderId
 * @property string binance_id              binance.clientOrderId
 * @property string binance_return          то что бинанс выдает первый раз, сохраняется в виде JSON
 * @property int    user_id
 * @property int    billing_id
 * @property int    amount
 * @property int    convert_id
 * @property int    created_at
 * @property int    status                  Статус ордера 0 - только создан, 1 - исполнен
 * @property int    tx_from_id
 * @property int    tx_to_id
 * @property int    currency_to             db.currency.id
 * @property int    amount_to               кол-во атомов которые пришли на счет клиенту в валюте currency_to
 */
class BinanceOrder extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_FILLED = 1;

    public static function tableName()
    {
        return 'binance_order';
    }

    /**
     * @param array $fields
     *
     * @return \common\models\BinanceOrder
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}