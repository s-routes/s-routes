<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 10.04.2016
 * Time: 2:04
 */

namespace common\models\piramida\WalletSource;

use common\models\BillingMain;
use common\models\PaymentEthereum;
use common\models\PaymentPrizm;
use cs\services\VarDumper;

class Prizm extends Base implements \common\models\piramida\WalletSourceInterface
{
    public $transaction;

    public $source_id;

    /** @var  double */
    public $sumAfter;

    /** @var  double */
    public $sumBefore;

    public function getTransactionInfo()
    {
        return $this->transaction;
    }

    public function getPriceWithTax($price)
    {
        return $price;
    }

    public function getTax()
    {
        return 0;
    }

    public function getSumBefore()
    {
        return $this->sumBefore;
    }

    public function getSumAfter()
    {
        return $this->sumAfter;
    }

    /**
     * Формирует форму
     *
     * @param \common\models\BillingMain    $billing         счет, в котором валюта должна быть равна (ETH)
     * @param string                        $description
     * @param string                        $destinationName наименование поучателя
     * @param null | array                  $options
     * - form - array - параметры(аттрибуты) тега формы
     * - values - array - доп параметры для формы как скрытые поля (ключ => значение)
     *
     * @return string
     */
    public function getForm($billing, $description, $destinationName, $destinationAddress, $options = null)
    {
        $config = \yii\helpers\Json::decode($destinationAddress);
        $payment = PaymentPrizm::findOne(['billing_id' => $billing->id]);
        if (is_null($payment)) {
            $payment = new PaymentPrizm([
                'value'      => $billing->sum_after,
                'action'     => $options['action'],
                'billing_id' => $billing->id,
                'address'    => $config['address'],
            ]);
            $ret = $payment->save();
        }

        if (!isset($options['view'])) {
            return $this->form($billing, $options['action'], $config['address']);
        } else {
            return $this->form($billing, $options['action'], $config['address'], $options['view']);
        }
    }

    /**
     * Генерирует форму HTML: картинка и текст с инструкцией
     *
     * @param \common\models\BillingMain    $billing         счет, в котором валюта должна быть равна (ETH)
     * @param string $action
     * @param string $address   адрес выставленного счета
     *
     * @return string HTML картинка и текст с инструкцией
     * @throws \Exception
     */
    private function form($billing, $action, $address, $view = null)
    {
        if (is_null($view)) {
            $view = '@common/models/piramida/WalletSource/Prizm.template.php';
        }

        return \Yii::$app->view->renderFile($view, [
            'amount'     => $billing->sum_after,
            'action'     => $action,
            'address'    => $address,
            'billing'    => $billing,
        ]);
    }


    /**
     * @return bool
     */
    public function success($actions)
    {
        if (\Yii::$app->request->get('type') != 'ethereum') {
            return false;
        }

        return true;
    }

}