<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 27.05.2016
 * Time: 9:03
 */

namespace common\models\piramida;

use Exception;
use yii\db\ActiveRecord;

/**
 * Class Move
 *
 * @package app\models\Piramida
 * 
 * @property int id
 * 
 */
class Move extends ActiveRecord
{
    public static function tableName()
    {
        return 'nw_move';
    }


    public function rules()
    {
        return [
            [[
                'a',
                'b',
                'from',
                'to',
                'from_wallet_a',
                'from_wallet_b',
                'to_wallet',
                'time',
            ], 'required'],
            [[
                'a',
                'b',
                'transaction_a_id',
                'transaction_b_id',
            ], 'integer'],
            [[
                'from',
                'to',
                'from_wallet_a',
                'from_wallet_b',
                'to_wallet',
                'time',
            ], 'double'],
            [[
                'transaction_a_id',
            ], 'oneOfTransaction', 'skipOnEmpty' => false],
        ];
    }
    
    public function oneOfTransaction($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (is_null($this->transaction_a_id) && is_null($this->transaction_b_id)) {
                $this->addError('transaction_a_id', 'Нужно чтобы хотябы одна транзакция присутствовала');
                $this->addError('transaction_b_id', 'Нужно чтобы хотябы одна транзакция присутствовала');
            }
        }
    }
} 