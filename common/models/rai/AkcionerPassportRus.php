<?php

namespace common\models\rai;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int    id              - идентификатор записи
 * @property int    seria           - Серия
 * @property int    number          - Номер паспорта
 * @property string kem             - Кем выдан
 * @property string propiska        - Прописка
 * @property date   issue_date      - Дата выдачи
 * @property date   rojd_date       - Дата рождения
 * @property string rojd_place      - место рождения
 * @property string image1          - Скан1
 * @property string image2          - Скан1
 *
 */
class AkcionerPassportRus extends ActiveRecord
{
    public static function tableName()
    {
        return 'rai_akcioneri_passport_rus';
    }

    /**
     * @param array $fields
     * @return \common\models\rai\AkcionerPassportRus
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }
}