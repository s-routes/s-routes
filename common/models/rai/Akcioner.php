<?php

namespace common\models\rai;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int    id              - идентификатор записи
 * @property string name_first      - Имя
 * @property string name_last       - Фамилия
 * @property string name_middle     - Отчество
 * @property string projivanie      - Место проживания
 * @property string foto            - Фото
 * @property string contact_phone   - Телефон
 * @property string contact_skype   - Скайп
 * @property string contact_email   - Email
 * @property string contact_vk      - VK
 * @property string contact_fb      - FB
 * @property string country         - двухбуквенный код страны пребывания
 * @property string country_passport- двухбуквенный код страны выдавшей паспорт
 * @property int    utc_delta       - Смещение в часах относительно Гринвича для местапребывания
 * @property string time_zone       - Временная Зона места пребывания
 * @property int    is_show_profile - Показывать в списке акционеров
 * @property int    is_bill_exist   - Открыт счет в АО
 * @property int    balance         - Баланс на счету АО, актуален если только is_bill_exist=1
 *
 */
class Akcioner extends ActiveRecord
{
    public static function tableName()
    {
        return 'rai_akcioneri';
    }

    /**
     * @param array $fields
     * @return static
     */
    public static function add($fields)
    {
        if (!isset($fields['balance'])) {
            $fields['balance'] = 0;
        }
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }
}