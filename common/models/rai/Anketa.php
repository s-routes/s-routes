<?php

namespace common\models\rai;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int    id                  - Идентификатор записи
 * @property string email               - Почта
 * @property string name_first          - Имя
 * @property string name_last           - Фамилия
 * @property string name_middle         - Отчество
 * @property string place               - Место проживания
 * @property string rojd_date           - Дата рождения
 * @property string time                - Время которое готовы уделять проекту
 * @property string naviki              - Навыки которыми обладаете
 * @property string contact_phone       - Телефон
 * @property string contact_telegram    - Телеграмм
 * @property string contact_whatsapp    - Вацап
 * @property string contact_skype       - Скайп
 * @property string contact_vk          - VK
 * @property string contact_fb          - FB
 * @property string vopros              - Вопросы
 * @property string foto                - Фото
 */
class Anketa extends ActiveRecord
{
    public static function tableName()
    {
        return 'rai_anketa';
    }

    /**
     * @param array $fields
     * @return static
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }
}