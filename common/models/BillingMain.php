<?php

namespace common\models;

use common\models\PaySystem;
use cs\Application;
use cs\services\BitMask;
use cs\web\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * Содержит информацию об внесении денежных средств из внешней системы
 * Предназначен для всех видов валют
 *
 * @property int    id                 - идентификатор записи
 * @property int    sum_before         - сумма до транзакции, сумма которую пользователь фактически оплатил за транзакцию
 * @property int    sum_after          - сумма которая постпила в систему после транзакции
 * @property double created_at         - время создания счета
 * @property int    is_paid            - флаг подтвержден ли ввод денег из платежной системы 0 - нет 1 - да
 * @property int    source_id          - идентификатор платежной системы
 * @property int    currency_id        - идентификатор валюты в котором был выставлен счет db.currency.id
 * @property int    class_id           - идентификатор класса
 * @property int    config_id          - идентификатор класса
 * @property int    is_paid_client     - флаг подтвержден ли ввод денег от клиента
 * @property string success_url        - ссылка на положительный результат операции
 *
 * Class Billing
 * @package app\models\Piramida
 */
class BillingMain extends ActiveRecord
{
    public static function tableName()
    {
        return 'billing';
    }

    /**
     * @param array $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = microtime(true);
        }
        if (!isset($fields['is_paid'])) {
            $fields['is_paid'] = 0;
        }
        $item = new static($fields);
        $item->save();
        $item->id = self::getDb()->lastInsertID;

        return $item;
    }

    /**
     * @return \common\models\PaySystem
     * @throws \yii\base\Exception
     */
    public function getPaySystem()
    {
        return PaySystem::findOne($this->source_id);
    }


    /**
     * ! Не использовать, для класса Billing это не типичная функция а скорее исключительная, поэтому и неуместна
     *
     * Функция которая возвращает класс совместимый с интерфейсом \common\models\PaymentI для тог чтобы можно было
     * указать транзакцию Это класс для таблицы payment{paymentName}
     *
     * @param int $billingId идентификатор заказа
     *
     * @return \common\models\PaymentI
     *
     * @throws
     */
    function getPayment()
    {
        $paySystem = $this->getPaySystem();
        $className = $paySystem->class_name;
        $fullClassName = '\common\models\Payment' . $className;

        $object = $fullClassName::findOne(['billing_id' => $this->id]);
        if (is_null($object)) throw new Exception('Не найден платеж $billingId=' . $this->id);

        return $object;
    }


    /**
     * Водтверждает платеж
     * Ставит флаг is_paid = 1
     */
    public function success()
    {
        $this->is_paid = 1;
        $this->save();
    }

    public function successClient($message = null)
    {
        $this->is_paid_client = 1;
        $this->save();

        // подтверждение класса
        if ($this->class_id) {
            $class = BillingMainClass::findOne($this->class_id);
            $className = $class->name;
            $classObject = $className::findOne(['billing_id' => $this->id]);
            if (method_exists($classObject, 'successClient')) {
                $classObject->successClient($message);
            }
        }
    }

    public function successShop()
    {
        if ($this->is_paid_client == 0) {
            $this->successClient();
        }

        // Ставлю статус оплачено
        $this->is_paid = 1;
        $this->save();

        // подтверждение класса
        \Yii::info($this->class_id, 'avatar\common\models\BillingMain::successShop');
        if ($this->class_id) {
            $class = BillingMainClass::findOne($this->class_id);
            $className = $class->name;
            $classObject = $className::findOne(['billing_id' => $this->id]);

            \Yii::info($className, 'avatar\common\models\BillingMain::successShop2');
            \Yii::info($classObject, 'avatar\common\models\BillingMain::successShop3');
            \Yii::info($classObject, 'avatar\common\models\BillingMain::successShop4');
            \Yii::info(method_exists($classObject, 'successShop'), 'avatar\common\models\BillingMain::successShop2');
            if (method_exists($classObject, 'successShop')) {
                $classObject->successShop();
            }
        }
    }
}