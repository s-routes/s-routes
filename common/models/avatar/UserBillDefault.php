<?php

namespace common\models\avatar;

use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * @property int    id
 * @property int    user_id
 * @property int    currency_id
 *
 */
class UserBillDefault extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_bill_default';
    }

    /**
     * @return \common\models\avatar\UserBill
     */
    public function getBilling()
    {
        return UserBill::findOne($this->id);
    }
}