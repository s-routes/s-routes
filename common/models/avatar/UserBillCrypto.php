<?php

namespace common\models\avatar;

use avatar\models\Wallet;
use avatar\models\WalletETC;
use avatar\models\WalletETH;
use avatar\models\WalletToken;
use avatar\models\WalletVVB;
use Blocktrail\SDK\BlocktrailSDK;
use common\components\providers\ETH;
use common\components\providers\Token;
use common\models\Card;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security;
use common\services\Security\AES;
use cs\services\BitMask;
use cs\services\VarDumper;
use Yii;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\Application;

/**
 * @property int    id
 * @property string address
 * @property string password
 * @property string name
 * @property int    user_id
 * @property int    mark_deleted
 * @property int    currency
 */
class UserBillCrypto extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_bill_crypto';
    }
}