<?php

namespace common\models\avatar;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id
 *
 */
class UserBillSystem extends ActiveRecord
{
    /**
     * @return UserBill
     */
    public function getBilling()
    {
        return UserBill::findOne($this->id);
    }

    public static function tableName()
    {
        return 'user_bill_system';
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                ],
                'integer'
            ],
        ];
    }

    /**
     * @return \common\models\avatar\UserBillSystem
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найдена ссылка');
        }
        return $item;
    }
}