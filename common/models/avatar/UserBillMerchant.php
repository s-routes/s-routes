<?php

namespace common\models\avatar;

use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use common\payment\BitCoinBlockTrailPayment;
use cs\services\BitMask;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;
use yii\helpers\VarDumper;

/**
 * @property int    id              - счет
 * @property string is_sms_phone
 * @property string is_email_email
 * @property string is_http_address
 * @property string is_http_config
 * @property string code
 * @property string image_top
 * @property string name
 * @property string secret
 * @property int    is_sms
 * @property int    is_email
 * @property int    is_http
 * @property int    confirmations
 * @property int    pay_system_list_id
 * @property int    user_id
 *
 */
class UserBillMerchant extends ActiveRecord
{
    public static function tableName()
    {
        return 'bill_merchant';
    }

    public function rules()
    {
        return [
            [['is_sms_phone'], 'string', 'max' => 20],
            [['is_email_email'], 'string', 'max' => 60],
            [['code'], 'string', 'max' => 60],
            [['secret'], 'string', 'max' => 60],
            [['is_http_address'], 'string', 'max' => 255],
            [['is_http_config'], 'string', 'max' => 255],
            [[
                'is_sms',
                'is_email',
                'is_http',
                'confirmations',
                'pay_system_list_id',
                'user_id',
            ], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'is_sms'             => 'Нужно СМС?',
            'is_email'           => 'Нужно Email?',
            'is_http'            => 'Нужно HTTP?',
            'is_sms_phone'       => 'Телефон',
            'is_email_email'     => 'Email',
            'is_http_address'    => 'Адрес Callback',
            'is_http_config'     => 'Конфиг Callback',
            'secret'             => 'Секретный код',
            'pay_system_list_id' => 'Ссылка на лист настройки платежных систем',
        ];
    }

    /**
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $item = parent::findOne($condition);
        if (is_null($item)) {
            throw new Exception('Не найден мерчант');
        }
        return $item;
    }

    /**
     * @return \common\models\avatar\UserBill
     *
     */
    public function getBilling()
    {
        return UserBill::findOne($this->id);
    }
}