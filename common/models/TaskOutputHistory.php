<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

/**
 * @property integer id
 * @property integer created_at
 * @property integer task_id
 * @property string  comment
 *
 * @package common\models
 */
class TaskOutputHistory extends ActiveRecord
{
    public static function tableName()
    {
        return 'task_output_history';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}