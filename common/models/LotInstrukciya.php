<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer page_id
 * @property integer sort_index
 *
 * @package common\models
 */
class LotInstrukciya extends ActiveRecord
{
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}