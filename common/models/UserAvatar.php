<?php
namespace common\models;

use avatar\models\UserRegistration;
use avatar\modules\UniSender\UniSender;
use common\components\providers\BTC;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBinance;
use cs\Application;
use cs\services\Security;
use cs\services\SitePath;
use cs\services\VarDumper;
use iAvatar777\widgets\FileUpload7\FileUpload;
use Imagine\Image\Box;
use yii\base\Exception;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use cs\services\UploadFolderDispatcher;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use yii\web\IdentityInterface;


/**
 * User model
 *
 * @property integer   id
 * @property string    pin                хеш код пинкода для мобильного приложения
 * @property string    password
 * @property string    email
 * @property string    name_first
 * @property string    name_last
 * @property string    phone
 * @property string    avatar
 * @property integer   phone_is_confirm
 * @property integer   email_is_confirm
 * @property integer   mark_deleted
 * @property integer   registered_ad
 * @property integer   created_at
 * @property integer   parent_id
 * @property integer   is_active
 * @property integer   is_confirm
 * @property integer   currency_view
 * @property integer   auth_key
 * @property integer   subscribe_id       идентификатор по сервису рассылки UniSender
 * @property string    human_design
 *
 * @property string    google_auth_code   код google авторизации
 * @property string    shop_google_auth_code   код google авторизации для магазина
 *
 * @property integer   subscribe_is_news  подписка на новости 0 - не подписан, 1 - подписан
 * @property integer   subscribe_is_blog  подписка на блог    0 - не подписан, 1 - подписан
 * @property integer   is_2step_login     использовать двухступенчатую авторизацию? 0 - нет, 1 - да
 * @property integer   wallets_is_locked  Флаг который показывает есть доступ к кошелькам или нет. 0 - клиент сбросил пароль и доступ к кошелькам не восстановлен. 1 - клиент восстановил доступ к кошелькам.
 * @property integer   password_save_type выбора типа хранения ключей, значение полей полностью соответсвует значениеям <code>user_bill.password_type</code>
 * @property integer   country_id
 * @property integer   last_action
 * @property integer   is_show_phone
 * @property integer   is_show_telegram
 * @property string    telegram_username
 * @property integer   telegram_chat_id
 * @property string    telegram_shop_username
 * @property integer   telegram_shop_chat_id
 * @property integer   last_see_kolokol     Время последнего просмотра новостей
 * @property integer   is_referal_program   Флаг. Участвует этот пользователь в рефералке? 0 - нет, 1 - да участвует
 *
 * @property integer   deals_count_offer    Кол-во сделок для всех предложений автором которых является offer.user_id
 * @property integer   deals_count_all      Общее кол-во сделок в которых учествовал пользователь
 * @property integer   assessment_positive  Общее кол-во положительных оценок пользователя
 * @property integer   assessment_negative  Общее кол-во отрицательных оценок пользователя
 *
 * @property string    address
 * @property string    town
 * @property string    time_zone
 * @property string    public_telegram
 * @property string    language
 *
 * @property string    shop_fio_contact
 * @property string    shop_address
 * @property string    shop_phone
 * @property string    shop_dop
 * @property string    shop_whatsapp
 * @property string    shop_telegram
 *
 * @property string    lot_google_auth_code
 * @property string    telegram_lot_username
 * @property integer   telegram_lot_chat_id
 *
 * @property integer   market_counter           Счетчик На сколько баллов куплено пакетов
 * @property integer   market_till              Момент времени когда заканчивается пакет, если 0 то неограниченно
 * @property integer   market_packet            какой пакет сейчас куплен, если 0 то не куплен
 */
class UserAvatar extends ActiveRecord implements IdentityInterface
{
    const IS_2STEP_LOGIN_OFF = 0;
    const IS_2STEP_LOGIN_DEVICE = 1;
    const IS_2STEP_LOGIN_ON = 2;

    const SUBSCRIBE_NEWS = 1;
    const SUBSCRIBE_BLOG = 2;

    public static function tableName()
    {
        return 'user';
    }

    /**
     *
     *
     * @param mixed $conditions
     * @return static
     * @throws Exception
     */
    public static function findOne($conditions)
    {
        $i = parent::findOne($conditions);
        if (is_null($i)) {
            throw new Exception('Не найден пользователь');
        }
        return $i;
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }


    /**
     * Сохраняет код Google, формирует URL и возвращает
     *
     * @return array
     * Если удачно получилось сохранить то выдает
     * [
     *  'secret' => $secret,
     *  'url'    => $url,
     * ]
     * если после повторного сохранения пройдет меньше времени чем $delta, то будет возвращен пустой массив
     *
     * @see http://new-earth.avatar-bank.com/development-avatar-bank/auth
     */
    public function saveGoogleAuthCode()
    {
        /** @var float $delta время в секундах между двумя вызовами после которого можно записать второй раз */
        $delta = 3;
        $key = '\common\models\UserAvatar::saveGoogleAuthCode()';
        $v = \Yii::$app->session->get($key);
        if ($v !== false) {
            if (microtime(true) - $v <= $delta) return [];
        }

        $file = \Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
        require_once($file);

        $ga = new \GoogleAuthenticator;
        $secret = $ga->generateSecret();
        $url = sprintf("otpauth://totp/%s?secret=%s", $this->email . '@avatar-bank.com', $secret);
        $this->google_auth_code = $secret;
        $this->save();
        \Yii::$app->session->set($key, microtime(true));

        return [
            'secret' => $secret,
            'url'    => $url,
        ];
    }

    /**
     * Сравнивает код $codeExternal с тем что в БД
     *
     * @param string $codeExternal код от пользователя для проверки
     *
     * @return bool
     *
     * @see http://new-earth.avatar-bank.com/development-avatar-bank/auth
     */
    public function validateGoogleAuthCode($codeExternal, $isShop = false)
    {
        $file = \Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
        require_once($file);

        $ga = new \GoogleAuthenticator;
        if ($isShop) {
            $s = $this->shop_google_auth_code;
        } else {
            $s = $this->google_auth_code;
        }
        $codeInternal = $ga->getCode($s);

        return $codeExternal == $codeInternal;
    }

    /**
     * Сравнивает код $codeExternal с тем что в БД
     *
     * @param string $codeExternal код от пользователя для проверки
     *
     * @return bool
     *
     * @see http://new-earth.avatar-bank.com/development-avatar-bank/auth
     */
    public function validateGoogleAuthCodeLot($codeExternal)
    {
        $file = \Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
        require_once($file);

        $ga = new \GoogleAuthenticator;
        $s = $this->lot_google_auth_code;
        $codeInternal = $ga->getCode($s);

        return $codeExternal == $codeInternal;
    }

    /**
     * Заполнены ли данные о Дизайне Человека?
     *
     * @return bool
     */
    public function hasHumanDesign()
    {
        if (is_null($this->human_design)) return false;

        return $this->human_design != '';
    }

    /**
     * Выдает счет по умолчанию
     *
     * @param string | int $currency Валюта
     *
     * @return \common\models\avatar\UserBill
     * @throws Exception
     */
    public function getPiramidaBilling($currency = Currency::BTC)
    {
        if (Application::isInteger($currency)) {
            $currencyId = $currency;
        } else {
            $currencyObject = Currency::findOne(['code' => $currency]);
            if (is_null($currencyObject)) {
                throw new Exception('Не верно указана валюта ' . $currency);
            }
            $currencyId = $currencyObject->id;
        }

        $defaultObject = UserBillDefault::findOne([
            'user_id'     => $this->id,
            'currency_id' => $currencyId,
        ]);
        if (is_null($defaultObject)) {
            throw new Exception('Нет счета по умолчанию у пользователя ' . $this->id);
        }

        return $defaultObject->getBilling();
    }

    /**
     * Выдает счета по умолчанию
     *
     * @return \common\models\avatar\UserBill[]
     * @throws Exception
     */
    public function getDefaultBillingList()
    {
        $linksToBills = UserBillDefault::find()->where(['user_id' => $this->id])->all();
        if (count($linksToBills) == 0) {
            throw new Exception('Нет счета по умолчанию у пользователя '. $this->id);
        }

        return UserBill::find()->where([
            'id' => ArrayHelper::getColumn($linksToBills, 'id'),
        ])->all();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Возвращает дерево
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public function getTree()
    {
        return self::_getTree($this->getId());
    }

    /**
     * Возвращает дерево
     *
     * @param int $parentId
     *
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    private static function _getTree($parentId = null)
    {
        $rows = self::find()
            ->select('id, email, name_first, name_last, avatar')
            ->where(['parent_id' => $parentId])
            ->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[$i];
            $rows2 = self::_getTree($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param  string $username
     *
     * @return UserAvatar
     */
    public static function findByUsername($username)
    {
        return self::findOne(['email' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() == $authKey;
    }

    /**
     * Проверяет пароль на валидность
     *
     * @param  string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public static function hashPassword($password)
    {
        return \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Автивирует пользователя после регистрации
     * и после активации карты
     */
    public function activate()
    {
        // ставлю флаг что почта подтверждена
        $this->email_is_confirm = 1;

        // записываю время активации
        $this->registered_ad = time();

        // сохраняю
        $this->save();
    }

    /**
     * Регистрирует пользователя, высылает письмо проверочное, добавляет запись о регистрации
     *
     * @param $email
     * @param $password
     * @param $params
     *
     * @return array
     * [
     *      'user'             => \common\models\UserAvatar,
     *      'userRegistration' => \avatar\models\UserRegistration,
     * ]
     */
    public static function registration($email, $password, $params = [])
    {
        $user = self::registrationCreateUser($email, $password, $params);
        $userRegistration = UserRegistration::add($user->id);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($userRegistration), 'avatar\\registration\\mail\\result1');

        $ret = \avatar\base\Application::mail($email, 'Подтверждение регистрации', 'registration', [
            'url'      => Url::to([
                'auth/registration-activate',
                'code' => $userRegistration->code
            ], true),
            'user'     => $user,
            'datetime' => $userRegistration->date_finish
        ]);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($ret), 'avatar\\registration\\mail\\result');

        return [
            'user'             => $user,
            'userRegistration' => $userRegistration
        ];
    }

    /**
     * Регистрирует пользователя для мобильника
     *
     * @param string $phone     +7...
     * @param string $password
     * @param array  $fields    дополнительные поля пользователя
     *
     * @return array
     * [
     *      'user'             => \common\models\UserAvatar,
     *      'userRegistration' => \avatar\models\UserRegistration,
     * ]
     */
    public static function registrationPhone($phone, $password, $fields = [])
    {
        $fieldsMain = [
            'phone'             => $phone,
            'password'          => self::hashPassword($password),
            'mark_deleted'      => 0,
            'email_is_confirm'  => 0,
            'phone_is_confirm'  => 1,
            'subscribe_is_news' => 1,
            'subscribe_is_blog' => 1,
            'created_at'        => time(),
            'auth_key'          => \Yii::$app->security->generateRandomString(60),
        ];
        $user = new static(ArrayHelper::merge($fields, $fieldsMain));
        $ret = $user->save();
        $user->id = self::getDb()->lastInsertID;

        $userRegistration = UserRegistration::add($user->id);

        $wallet = \avatar\models\Wallet::create('Мой кошелек', $user->id);
        $d = new UserBillDefault(['id' => $wallet->billing->id]);
        $d->save();

        return [
            'user'             => $user,
            'userRegistration' => $userRegistration
        ];
    }

    /**
     * Регистрирует пользователя
     * Письмо не высыллается
     *
     * @param string $email - email
     * @param string $password - пароль открытый, не хешированный
     * @param array  $params - доролнительные параметры которые нужно установить для пользователя
     *
     * @return \common\models\UserAvatar
     */
    public static function registrationCreateUser($email, $password, $params = [])
    {
        $email = strtolower($email);
        $fields = [
            'email'              => $email,
            'password'           => self::hashPassword($password),
            'mark_deleted'       => 0,
            'email_is_confirm'   => 0,
            'wallets_is_locked'  => 0,
            'subscribe_is_news'  => 1,
            'subscribe_is_blog'  => 1,
            'created_at'         => time(),
            'auth_key'           => \Yii::$app->security->generateRandomString(60),
            'password_save_type' => UserBill::PASSWORD_TYPE_OPEN_CRYPT,
        ];
        if (count($params) > 0) {
            $fields = ArrayHelper::merge($fields, $params);
        }
        $user = new static($fields);
        $ret = $user->save();
        $user->id = self::getDb()->lastInsertID;

        return $user;
    }

    /**
     * Устанавливает пароль
     *
     * @param string $password пароль в открытом виде
     *
     * @return bool
     */
    public function setPassword($password)
    {
        $this->password = self::hashPassword($password);
        return $this->save();
    }

    /**
     * Заменяет пароли c $from на $to в счетах пользователя
     *
     * @param string $from
     * @param string $to
     */
    public function changePasswordInBills($from, $to)
    {
        if ($this->password_save_type == UserBill::PASSWORD_TYPE_OPEN) return;
        \common\models\avatar\UserBill::changePassword($from, $to, $this->id);
    }

    /**
     * Устанавливает новый аватар
     * Картинка должна быть квадратной
     * Размер 300х300
     *
     * @param string $content содержимое файла аватара
     * @param string $extension расширение файла
     *
     * @return \cs\services\SitePath
     */
    public function setAvatarAsContent($content, $extension)
    {
        $path = UploadFolderDispatcher::createFolder('FileUpload3', self::tableName(), $this->getId());
        $path->addAndCreate('small');
        $path->add('avatar.' . $extension);

        file_put_contents($path->getPathFull(), $content);
        $this->avatar = $path->getPath();
        $this->save();

        return $path;
    }

    /**
     * Устанавливает новый аватар из адреса интернет
     *
     * @param string $url полный url на картинку, может быть прямоугольной
     * @param string $extension расширение которое должно быть в результируеющем файле
     *
     * @return \cs\services\SitePath
     */
    public function setAvatarFromUrl($url, $extension = null)
    {
        if (is_null($extension)) {
            $info = parse_url($url);
            $pathinfo = pathinfo($info['path']);
            $extension = $pathinfo['extension'];
        }
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($url), 'gs\\user');
        $image = new Image();
        $imageFileName = \Yii::getAlias('@runtime/temp_images');
        FileHelper::createDirectory($imageFileName);
        $imageFileName .= DIRECTORY_SEPARATOR . time() . '_' . Security::generateRandomString(10) . '.' . $extension;
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($imageFileName), 'gs\\user');

        $image = $image->getImagine()->load(file_get_contents($url));
        $image = $this->expandImage($image, 300, 300, ManipulatorInterface::THUMBNAIL_OUTBOUND);
        $image->thumbnail(new Box(300, 300), ManipulatorInterface::THUMBNAIL_OUTBOUND)->save($imageFileName, ['format' => 'jpg', 'quality' => 100]);

        return $this->setAvatarAsContent(file_get_contents($imageFileName), $extension);
    }

    /**
     * Расширяет маленькую картинку по заданной стратегии
     *
     * @param \Imagine\Image\ImageInterface $image
     * @param int $widthFormat
     * @param int $heightFormat
     * @param int $mode
     *
     * @return \Imagine\Image\ImageInterface
     */
    protected static function expandImage($image, $widthFormat, $heightFormat, $mode)
    {
        $size = $image->getSize();
        $width = $size->getWidth();
        $height = $size->getHeight();
        if ($width < $widthFormat || $height < $heightFormat) {
            // расширяю картинку
            if ($mode == ManipulatorInterface::THUMBNAIL_OUTBOUND) {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->widen($widthFormat);
                    } else {
                        $size = $size->heighten($heightFormat);
                    }
                }
                $image->resize($size);
            } else {
                if ($width < $widthFormat && $height >= $heightFormat) {
                    $size = $size->heighten($heightFormat);
                } else if ($width >= $widthFormat && $height < $heightFormat) {
                    $size = $size->widen($widthFormat);
                } else if ($width < $widthFormat && $height < $heightFormat) {
                    // определяю как расширять по ширине или по высоте
                    if ($width / $widthFormat < $height / $heightFormat) {
                        $size = $size->heighten($heightFormat);
                    } else {
                        $size = $size->widen($widthFormat);
                    }
                }
                $image->resize($size);
            }
        }

        return $image;
    }

    public function getAddress()
    {
        return 'U_' . str_repeat('0', 6 - strlen($this->id)) . $this->id;
    }

    public function getAddressShort()
    {
        $id = (string)$this->id;
        if (strlen($id) > 4) {
            $last4 = substr($id, strlen($id) - 4);
        } else {
            $last4 = str_repeat('0', 4 - strlen($id)) . $id;
        }

        return 'U_...' . $last4;
    }

    /**
     * Возвращает аватар
     * Если не установлен то возвращает заглушку
     *
     * @return string
     */
    public function getAvatar($isFullPath = false)
    {
        $avatar = $this->avatar;
        if ($avatar . '' == '') {
            return '/images/cabinet/index/photo_2020-01-28_15-58-38.jpg';
        }
        if (StringHelper::startsWith($this->avatar, 'http')) {
            $avatar = FileUpload::getFile($this->avatar, 'crop');
        }

        return Url::to($avatar, $isFullPath);
    }

    /**
     * Имеет ли профиль аватар?
     *
     * @return bool
     * true - имеет
     * false - не имеет
     */
    public function hasAvatar()
    {
        return ($this->avatar . '' != '');
    }

    /**
     * Возвращает почту
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }


    /**
     * Возвращает пол
     *
     * 0 - женщина
     * 1 - мужчина
     *
     * @return int|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Возвращает Имя и Фамилию через пробел
     * Если таковых нет то вернет почту, если она подтверждена
     * Если нет то вернет FB:id
     * Если нет то вернет VK:id
     * Если нет то вернет ''
     *
     * @return string
     */
    public function getName2()
    {
        $first = $this->name_first;
        $last = $this->name_last;
        $arr = [];
        if ($first) {
            $arr[] = $first;
        }
        if ($last) {
            $arr[] = $last;
        }
        if (count($arr) == 0) {
            if (is_null($this->email)) return '';
            return $this->email;
        }

        return join(' ', $arr);
    }

    /**
     * Возвращает значение из $this->fields, если значение = null то возвращается пустая строка
     *
     * @param $name
     *
     * @return mixed
     */
    public function getString($name)
    {
        return ArrayHelper::getValue($this->fields, $name, '');
    }

    /**
     * Удаляет аватар
     * и физические файлы
     *
     */
    public function deleteAvatar()
    {
        $avatar = $this->avatar;
        if ($avatar != '') {
            (new SitePath($avatar))->deleteFile();
        }
        $this->avatar = null;
        $this->save();
    }

    /**
     * Удаляет
     *
     * @return bool
     * @throws \Exception
     */
    public function delete()
    {
        parent::delete();
        return true;
    }

    /**
     * Подтверждает email
     *
     */
    public function registrationSuccess()
    {
        $this->email_is_confirm = 1;
        $this->save();
    }

}
