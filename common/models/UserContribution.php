<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer contribution_id идентификатор вклада contribution.id
 * @property integer user_id         идентификатор пользователя
 * @property integer created_at      момент создания вклада
 * @property integer finish_at       момент окончания вклада
 * @property integer wallet_id       кошелек на котором лежат токены пользователя
 * @property integer amount          кол-во вложенных нейронов, атомов
 * @property integer status          status
 */
class UserContribution extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_FINISH = 1;

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {

        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}