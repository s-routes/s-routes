<?php

namespace common\models\form;

use app\common\widgets\FileUpload3\FileUpload;
use app\models\SiteContentInterface;
use app\models\SiteUpdateItem;
use app\models\SubscribeItem;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use cs\services\VarDumper;
use HttpSignatures\Exception;
use yii\db\ActiveRecord;
use yii\db\Query;
use app\services\Subscribe;

/**
 * @property int    id
 * @property string name
 * @property date   start_date
 * @property time   start_time
 * @property date   end_date
 * @property time   end_time
 * @property string content
 * @property string image
 * @property int    user_id
 * @property int    date_insert
 * @property string link
 * @property date   date
 * @property int    is_added_site_update
 * @property int    moderation_status
 * @property int    union_id
 * @property double place_lng
 * @property double place_lat
 * @property double place_address
 *
 * Class Event
 * @package common\models
 */
class Event extends FormActiveRecord
{
    public static function tableName()
    {
        return 'gs_events';
    }

    /**
     * @param $condition
     * @return static
     * @throws Exception
     */
    public static function findOne($condition)
    {
        $i = parent::findOne($condition);
        if (is_null($i)) {
            throw new Exception;
        }
        return $i;
    }

    function beforeInsert()
    {
        return [
            'user_id' => function(\common\models\form\Event $sender) {
                return \Yii::$app->user->id;
            }
        ];
    }

    function formAttributes()
    {
        return [
            [
                'name',
                'Название',
                1,
                'string'
            ],
            [
                'start_date',
                'Старт. Дата',
                1,
                '\app\common\widgets\DatePicker\Validator',
                'widget' => [
                    'app\common\widgets\DatePicker\DatePicker', [
                        'dateFormat' => 'php:d.m.Y',
                    ]
                ],
            ],
            [
                'start_time',
                'Старт. Время',
                0,
                'string',[],
                'формат чч:мм',
            ],
            [
                'end_date',
                'Конец. Дата',
                1,
                '\app\common\widgets\DatePicker\Validator',
                'widget' => [
                    'app\common\widgets\DatePicker\DatePicker', [
                        'dateFormat' => 'php:d.m.Y',
                    ]
                ],
            ],
            [
                'end_time',
                'Конец. Время',
                0,
                'string', [],
                'формат чч:мм',

            ],
            [
                'content',
                'Описание',
                0,
                'string',
                'widget' => [
                    'app\common\widgets\HtmlContent\HtmlContent',
                    [
                    ]
                ]
            ],
            [
                'link',
                'Ссылка',
                0,
                'url',[],
                'Ссылка на мероприятие'
            ],
            [
                'date',
                'Дата',
                1,
                'string',[],
                'Как строка'
            ],
            [
                'image',
                'Картинка',
                0,
                'default',
                'widget' => [
                    FileUpload::className(),
                    [
                        'options' => [
                            'small' => \app\services\GsssHtml::$formatIcon
                        ]
                    ]
                ]
            ],
        ];
    }

}