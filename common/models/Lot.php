<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        price
 * @property int        currency_id
 * @property int        bilet_count         Всего кол-во билетов
 * @property int        bilet_sold          Кол-во проданных билетов
 * @property int        winner_id           Идентификатор пользователя победителя
 * @property int        time_start          Время начала продаж
 * @property int        time_finish         Время окончания продаж
 * @property int        time_raffle         Время розыгрыша
 * @property int        is_hide             Флаг. Скрыт лот? 0 - показывается, 1 - да скрыт, не показывается
 * @property int        status              Статус лота 1 - Создан, 2 - Пул участников сформирован, 3 - Лот разыгран
 * @property int        type                тип лотереи 1 - автоматизированный, 2 - онлайн трансляция
 * @property int        cash_back_percent   процент кешбека
 * @property int        cash_back_price     цена пакета компенсации
 * @property int        room_id
 * @property string     name
 * @property string     image
 * @property string     description
 * @property string     content
 * @property int        is_counter_start    Флаг. Стартовала лотерея?
 * @property int        is_counter_start2   Флаг. Для автоматического. Стартовал барабан?
 * @property int        is_counter_stop     Флаг. Остановлен? Победитель определен?
 * @property int        is_prize_payd       Флаг. Выдан ли приз деньгами?
 * @property int        is_cancel           Флаг. Отменен лот?
 * @property int        counter
 * @property int        is_send_win         Флаг, Отправлено победителю деньги и кеш бек проигравшим
 * @property int        prize_type          тип приза 1 - денежный, 2 - вещевой, название указывается в `prize_name`
 * @property string     prize_name          название приза
 * @property string     prize_amount        Главный Приз денежный, монет LOT
 * @property string     link_youtube        ссылка на youtube для ручной лотереи
 * @property string     link_instagram      ссылка на instagram для ручной лотереи
 * @property string     link_telegram       ссылка на telegram для ручной лотереи
 *
 */
class Lot extends ActiveRecord
{
    const TYPE_AUTO = 1;
    const TYPE_MANUAL = 2;

    const PRIZE_TYPE_MONEY = 1;     // денежный приз
    const PRIZE_TYPE_THING = 2;     // вещественный приз

    const STATUS_CREATED = 1;    // Создан
    const STATUS_POOL_START = 2; // Пул участников сформирован
    const STATUS_FINISHED = 3;   // Лот разыгран

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}

