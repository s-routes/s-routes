<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        lot_id
 * @property int        user_id
 * @property int        created_at
 * @property int        tx_id
 * @property string     num             номер билета #####-####
 * @property int        is_cash_back        флаг. куплен пакет компенсации?
 * @property int        cash_bask_result    флаг. выплачена ли компенсация?
 *
 */
class Bilet extends ActiveRecord
{
    public $user = null;

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        if (!isset($fields['num'])) {
            $num = str_repeat('0', 5 - strlen($fields['lot_id'])) . $fields['lot_id'];
            $c = Bilet::find()->where(['lot_id' => $fields['lot_id']])->count() + 1;
            $num = $num . '-' . str_repeat('0', 4 - strlen($c)) . $c;
            $fields['num'] = $num;
        }

        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public static function generateNum ($lot, $count)
    {
        $num = str_repeat('0', 5 - strlen($lot)) . $lot;
        $num = $num . '-' . str_repeat('0', 4 - strlen($count)) . $count;

        return $num;
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        if (is_null($this->user)) {
            $this->user = UserAvatar::findOne($this->user_id);
        }

        return $this->user;
    }
}

