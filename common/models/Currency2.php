<?php

namespace common\models;

use common\models\piramida\Currency;
use Yii;
use common\extensions\XSSActiveRecord;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property string     id2
 * @property string     symbol
 * @property string     name
 */
class Currency2 extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency2';
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }
}
