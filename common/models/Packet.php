<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer count
 * @property integer price
 */
class Packet extends ActiveRecord
{

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
