<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class CardDesign
 *
 * @property integer id
 * @property string  image
 *
 * @package common\models
 */
class CardDesign extends ActiveRecord
{
    public static function tableName()
    {
        return 'card_design';
    }
}