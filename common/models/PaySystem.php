<?php
namespace common\models;

use common\models\avatar\Currency;
use Yii;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * PaySystem model
 *
 * @property integer    id
 * @property string     code
 * @property string     title
 * @property integer    comission_percent
 * @property string     comission_fix
 * @property string     class_name
 * @property string     image
 * @property string(6)  currency
 */
class PaySystem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'paysystems';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'image'], 'string', 'max' => 255],
            [['class_name', 'code'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 60],
            [['currency'], 'string', 'max' => 6],
            [['id', 'comission_percent'], 'integer'],
        ];
    }

    /**
     * @param mixed $condition
     * @return static
     * @throws \yii\base\Exception
     */
    public static function findOne($condition)
    {
        $i = parent::findOne($condition);
        if (is_null($i)) {
            throw new Exception('Не найдена платежная система');
        }
        return $i;
     }

    /**
     * @return string
     */
    public function getClassName()
    {
        return '\\avatar\\modules\\PaySystems\\items\\' . $this->class_name . '\\in\\Class1';
    }

    /**
     * @param array $fields инициализирующий массив
     *
     * @return \common\models\piramida\WalletSourceInterface
     */
    public function getClass($fields = null)
    {
        $s = $this->getClassName();
        return new $s($fields);
    }

    /**
     * @return \common\models\avatar\Currency
     * @throws Exception
     */
    public function getCurrencyObject()
    {
        $currency = $this->currency;
        $c = Currency::findOne(['code' => $currency]);
        if (is_null($c)) {
            throw new Exception('Не найлена валюта '. $currency);
        }

        return $c;
    }

    /**
     * Функция которая возвращает класс совместимый с интерфейсом \common\models\PaymentI для тог чтобы можно было указать транзакцию
     *
     * @param int $billingId идентификатор заказа
     *
     * @return \common\models\PaymentI
     */
    function getPayment($billingId)
    {
        $classList = [

        ];
        $className = $this->class_name;
        $fullClassName = '\common\models\Payment' . $className;

        $object = $fullClassName::findOne(['billing_id' => $billingId]);
        if (is_null($object)) throw new Exception('Не найден платеж $billingId='.$billingId);

        return $object;
    }

}
