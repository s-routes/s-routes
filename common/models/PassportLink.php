<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class PassportLink
 *
 * @property integer  id
 * @property integer  user_id
 * @property integer  billing_id
 * @property integer  created_at
 *
 * @package common\models
 */
class PassportLink extends ActiveRecord
{
    public static function tableName()
    {
        return 'passport_link';
    }
}