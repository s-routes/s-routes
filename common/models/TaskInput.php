<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property string  account
 * @property string  txid
 * @property integer user_id
 * @property integer type_id
 * @property integer status         0 - задача создана, клиент скинул деньги. 1 - задача подтверждена магазином, 2 - заявка отменена
 * @property integer created_at
 * @property integer finished_at
 * @property string  amount
 * @property integer txid_internal
 * @property integer currency_io_id
 * @property integer task_input
 * @property integer modification_id
 * @property integer amount_user        кол-во атомов которые указал пользователь
 *
 * @package common\models
 */
class TaskInput extends ActiveRecord
{
    const TYPE_ACCOUNT = 1;
    const TYPE_TRANSACTION = 2;

    public static function tableName()
    {
        return 'task_input';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}