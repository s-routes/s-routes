<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        transaction_id
 * @property int        operation_id
 * @property int        type_id
 */
class NeironTransaction extends ActiveRecord
{
    const TYPE_REFERAL = 1;             // Реферальные начисления
    const TYPE_REFERAL_CONVERT = 14;    // Реферальные начисления от конвертации
    const TYPE_REFERAL_CONVERT_BINANCE = 15;    // Реферальные начисления от конвертации на бинансе

    const TYPE_BUY_NOT_TRUSTED = 10;    // Зачисление нейронов при процессе продажи для не доверенного представителя
    const TYPE_BUY_TRUSTED = 2;         // зачисление нейронов при процессе продажи для доверенного представителя
    const TYPE_BUY_OUT_NOT_TRUSTED = 8; // списание нейронов при процессе продажи для не доверенного представителя
    const TYPE_BUY_OUT_TRUSTED = 9;     // списание нейронов при процессе продажи для доверенного представителя
    const TYPE_MOVE = 3;                // перевод другу
    const TYPE_MOVE_MARKET_COMISSION = 16;                // комиссия на MARKET
    const TYPE_CONVERT_GOOD = 4;        // конвертация кошерная
    const TYPE_CONVERT = 13;            // конвертация
    const TYPE_HOLD = 5;                // вклад
    const TYPE_HOLD_RETURN = 11;        // вклад возврат
    const TYPE_HOLD_PERCENT = 12;       // вклад возврат процентов
    const TYPE_OUT = 6;                 // Вывод
    const TYPE_OUT_RETURN = 7;          // Вывод возврат

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public static function getDb()
    {
        return \Yii::$app->dbWallet;
    }
}