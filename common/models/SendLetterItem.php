<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 02.09.2016
 * Time: 9:31
 */

namespace common\models;

use cs\services\VarDumper;
use yii\base\Exception;
use yii\db\ActiveRecord;

/**
 * @property int    $id
 * @property int    $letter_id
 * @property string $text                - текст письма
 * @property int    $created_at          - момент ответа ответа
 */
class SendLetterItem extends ActiveRecord
{
    public static function tableName()
    {
        return 'send_letter_item';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $new = new self($fields);
        $ret = $new->save();
        if (!$ret) {
            throw new Exception('Не удалось сохранить. '. \yii\helpers\VarDumper::dumpAsString($new->errors));
        }
        $new->id = self::getDb()->lastInsertID;

        return $new;
    }
} 