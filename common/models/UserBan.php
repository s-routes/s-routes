<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;


use yii\db\ActiveRecord;

/**
 * @property int    id
 * @property int    created_at
 * @property int    user_id
 * @property int    chat_id
 * @property int    type
 *
 */
class UserBan extends ActiveRecord
{
    const TYPE_BAN = 1;
    const TYPE_DELETE = 2;

    public static function tableName()
    {
        return 'user_ban';
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserBan
     */
    public static function add($fields)
    {
        if (!isset($fields['created_at'])) {
            $fields['created_at'] = time();
        }
        $item = new static($fields);
        $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }
}