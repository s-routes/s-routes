<?php

namespace common\models;

use common\models\piramida\Currency;
use Yii;
use common\extensions\XSSActiveRecord;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property string     code
 * @property string     title
 */
class CurrencyCrypto extends ActiveRecord
{
    const ETH = 1;
    const LEGAT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_crypto';
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }
}
