<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Billing;
use common\models\statistic\StatisticDb;
use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property integer time       время закладки токенов в сек
 * @property integer percent    проценты за месяц * 1000
 * @property integer amount     кол-во атомов заложенных во вклад (для монеты NEIRON) min
 * @property integer amount_max кол-во атомов заложенных во вклад (для монеты NEIRON) max
 * @property string  name       название пакета
 */
class Contribution extends ActiveRecord
{

    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}