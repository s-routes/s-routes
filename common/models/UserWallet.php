<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 06.01.2017
 * Time: 17:14
 */

namespace common\models;


use yii\db\ActiveRecord;
use Yii;

/**
 * @property int    id
 * @property int    user_id
 * @property string pzm
 * @property string eth
 * @property string usdt
 * @property string let
 * @property string egold
 * @property string usdt_erc20
 * @property string usdt_trc20
 */
class UserWallet extends ActiveRecord
{
    public function rules()
    {
        $rows = [];
        $curr = [
            \common\models\avatar\Currency::NEIRO,
            \common\models\avatar\Currency::BTC,
            \common\models\avatar\Currency::ETH,
            \common\models\avatar\Currency::USDT,
            \common\models\avatar\Currency::LTC,
            \common\models\avatar\Currency::PZM,
            \common\models\avatar\Currency::TRX,
            \common\models\avatar\Currency::DASH,
            \common\models\avatar\Currency::BNB,
        ];
        foreach ($curr as $c) {

            $c2 = \common\models\avatar\Currency::findOne($c);
            $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $c]);
            $mod = \common\models\CurrencyIoModification::find()->where(['currency_io_id' => $cio->id])->all();

            if (count($mod) > 0) {
                /** @var \common\models\CurrencyIoModification $m */
                foreach ($mod as $m) {
                    $rows[] = [$m->wallet_field_name, 'string'];
                    $rows[] = [$m->wallet_field_name, 'validateUnique'];
                }
            } else {
                $rows[] = [$cio->field_name_wallet, 'string'];
                $rows[] = [$cio->field_name_wallet, 'validateUnique'];
            }
        }

        return $rows;
    }

    public function validateUnique($a, $p)
    {
        if (!$this->hasErrors()) {
            if ($this->id) {
                $e = UserWallet::find()->where([$a => $this->$a])->andWhere(['not', ['id' => $this->id]])->exists();
            } else {
                $e = UserWallet::find()->where([$a => $this->$a])->exists();
            }
            if ($e) {
                $this->addError($a, Yii::t('c.j8zN5pFpQT', 'Такой кошелек уже установлен'));
                return;
            }
        }
    }

    /**
     *
     * @param array $fields
     *
     * @return \common\models\UserWallet
     */
    public static function add($fields)
    {
        $item = new static($fields);
        $item->save();
        $item->id = static::getDb()->lastInsertID;

        return $item;
    }
}