<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\eth;

use yii\base\Object;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id
 * @property string hash        Хеш транзакции транзакции, начинается с 0x и только малые буквы
 * @property int    time        Момент времени создания транзакции
 * @property int    comission   Комиссия в wei затраченная на выполнение всей транзакции
 * @property int    block       Высота блока в которой запакована транзакция
 *
 */
class Transaction extends ActiveRecord
{
    public static function tableName()
    {
        return 'eth_token_transaction';
    }

}