<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\eth;

use yii\base\Object;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 *
 * @property int    id
 * @property string owner           Идентификатор адреса владельца, начинается с 0x и только малые буквы
 * @property string to              Идентификатор адреса партнера, начинается с 0x и только малые буквы
 * @property string amount          Кол-во копеек токена которые были отправлены
 * @property string contract        Адрес контракта токена, начинается с 0x и только малые буквы
 * @property int    type            Тип операции для поля from. -1 - расход. 1 - приход
 * @property int    transaction_id  Идентификатор транзакции из таблицы `eth_token_transaction`
 *
 */
class Transfer extends ActiveRecord
{
    const TYPE_OUT = -1;
    const TYPE_IN = 1;

    public static function tableName()
    {
        return 'eth_token_transfer';
    }

}