<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

/**
 * @property integer id
 * @property string  account
 * @property string  binance_id     Идентификатор заявки на вывод для бинанса
 * @property string  txid
 * @property integer status         Статус задачи 0 - задача создана. 1 - задача выполнена, транзакция указана, 2 - скрыта, 3 - взята на вывод бухгалтером
 * @property integer created_at
 * @property integer amount
 * @property integer finished_at
 * @property integer txid_internal
 * @property integer billing_id
 * @property integer currency_io_id
 * @property integer currency_id    Идентификатор валюты для вывода, db.currency.id
 * @property integer user_id
 * @property integer buh_user_id
 * @property integer comission
 * @property integer comission_id
 * @property integer comission_user
 * @property integer modification_id
 * @property string  prizm_key
 *
 * @package common\models
 */
class TaskOutput extends ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_DONE = 1;
    const STATUS_HIDE = 2;
    const STATUS_ACCEPTED = 3; // ?
    const STATUS_INIT = 4;

    public static function tableName()
    {
        return 'task_output';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }
}