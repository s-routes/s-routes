<?php

namespace common\models;

use common\models\avatar\UserBill;
use common\models\piramida\Wallet;
use yii\helpers\ArrayHelper;


/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 30.11.2020
 * Time: 21:43
 */

/**
 * Class RequestNeiro
 *
 * @param int $id
 * @param int $user_id
 * @param int $price - кол-во монет для покупки
 * @param int $is_paid
 * @param int $parent_id - кто пригласил
 *
 * @package common\models
 */
class RequestNeiro extends \yii\db\ActiveRecord
{
    /**
     * Магазин подтвердил приход денег
     */
    public function successShop()
    {
        $school_id = 1;
        $referal_wallet_id = 1;

        // Ставлю статус оплачено
        $this->is_paid = 1;
        $this->save();

        // Если пользователь уже встроен в пирамиду, то ссылка не актуальна.
        $partner = \common\models\UserPartner::findOne(['school_id' => $school_id, 'user_id' => $this->user_id]);
        if (is_null($partner)) {
            if ($this->parent_id) {
                $partner = \common\models\UserPartner::add([
                    'school_id' => 1,
                    'user_id'   => $this->user_id,
                    'parent_id' => $this->parent_id,
                ]);
            }
        }

        if ($partner) {
            // распределение реферальных начислений
            $levels = ReferalLevel::find()->where(['school_id' => $school_id])->all();
            if (count($levels) > 0) {
                ArrayHelper::multisort($levels, 'level');
                $referal_wallet = Wallet::findOne($referal_wallet_id);
                $currency_id = $referal_wallet->currency_id;
                $price = $this->price;
                for ($level = 1; $level <= count($levels); $level++) {
                    $parent_id = $partner->parent_id;

                    /** @var \common\models\ReferalLevel $referalLevel */
                    $referalLevel = $levels[$level - 1];

                    // множитель для цены
                    $m = $referalLevel->percent / 10000;

                    // сумма отчислений
                    $amount = (int)($price * $m);
                    $data = UserBill::getInternalCurrencyWallet($currency_id, $parent_id);
                    $wallet = $data['wallet'];
                    $transaction = $referal_wallet->move($wallet, $amount, 'Реферальные начисления за заказ = ' . $this->id . ' от uid=' . $this->user_id . ' к uid=' . $parent_id);

                    // Записываю реферальные начисления
                    $ReferalTransaction = ReferalTransaction::add([
                        'school_id'      => $this->school_id,
                        'request_id'     => $this->id,
                        'level'          => $level,
                        'from_uid'       => $this->user_id,
                        'to_uid'         => $parent_id,
                        'to_wid'         => $wallet->id,
                        'amount'         => $amount,
                        'transaction_id' => $transaction->id,
                        'currency_id'    => $wallet->currency_id,
                    ]);

                    // выбираю следующего из уровня
                    $partner = \common\models\UserPartner::findOne(['school_id' => $school_id, 'user_id' => $partner->parent_id]);

                    // если дерево кончилось то выхожу
                    if (is_null($partner)) break;
                }
            }
        }
    }
}