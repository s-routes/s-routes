<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\exchange;

use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\Contribution;
use common\models\NeironTransaction;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\ReferalLevel;
use common\models\ReferalTransaction;
use common\models\subscribe\Subscribe;
use common\models\UserAvatar;
use common\models\UserContribution;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;

/**
 * @property int id                      Идентификатор заявки
 * @property int user_id                 Идентификатор пользователя кто открыл сделку
 * @property int price                   Цена по которой предлагается купить/продать монеты (currency_io), в атомах
 * @property int currency_id             Валюта для price, указывает на db.currency
 * @property int offer_id                ссылка на предложение
 * @property int created_at              Момент создания предложения
 * @property int volume                  Объем сделки в атомах price и currency_id = (price/100) * volume_vvb
 * @property int volume_vvb              Объем сделки в атомах VVB
 * @property int type_id                 Тип сделки для user_id
 * @property int status                  Статус сделки
 * @property int time_finish             Время окончания сделки
 * @property int time_accepted           Время подтверждения сделки
 * @property int currency_io
 * @property int partner_id              Идентификатор пользователя указанный в реферальной ссылке
 * @property int chat_room_id            Идентификатор чата
 *
 *
 */
class Deal extends ActiveRecord
{
    private $user;

    const TYPE_ID_BUY = 1;
    const TYPE_ID_SELL = 2;

    const STATUS_CREATE = 11;
    const STATUS_OPEN = 1;
    const STATUS_BLOCK = 2;
    const STATUS_MONEY_SENDED = 3;
    const STATUS_MONEY_RECEIVED = 4;
    const STATUS_CLOSE = 5;
    const STATUS_AUDIT_WAIT = 6;
    const STATUS_AUDIT_ACCEPTED = 7;
    const STATUS_AUDIT_FINISH = 8;
    const STATUS_HIDE = 9;             // закрыта но не завершилась, когда закончилось время но не завершилась.
    const STATUS_CANCEL = 10;          // Сделку отменил создатель

    const EVENT_BEFORE_OPEN = 'beforeOpen';
    const EVENT_AFTER_OPEN = 'afterOpen';
    const EVENT_BEFORE_BLOCK = 'beforeBlock';
    const EVENT_AFTER_BLOCK = 'afterBlock';
    const EVENT_BEFORE_MONEY_SENDED = 'beforeMoneySended';
    const EVENT_AFTER_MONEY_SENDED = 'afterMoneySended';
    const EVENT_BEFORE_MONEY_RECEIVED = 'beforeMoneyReceived';
    const EVENT_AFTER_MONEY_RECEIVED = 'afterMoneyReceived';
    const EVENT_BEFORE_CLOSE = 'beforeClose';
    const EVENT_AFTER_CLOSE = 'afterClose';
    const EVENT_BEFORE_AUDIT_WAIT = 'beforeAuditWait';
    const EVENT_AFTER_AUDIT_WAIT = 'afterAuditWait';
    const EVENT_BEFORE_AUDIT_ACCEPTED = 'beforeAuditAccepted';
    const EVENT_AFTER_AUDIT_ACCEPTED = 'afterAuditAccepted';
    const EVENT_BEFORE_AUDIT_FINISH = 'beforeAuditFinish';
    const EVENT_AFTER_AUDIT_FINISH = 'afterAuditFinish';
    const EVENT_BEFORE_HIDE = 'beforeHide';
    const EVENT_AFTER_HIDE = 'afterHide';
    const EVENT_BEFORE_MESSAGE = 'beforeMessage';
    const EVENT_AFTER_MESSAGE = 'afterMessage';

    public static $statusList = [
        self::STATUS_CREATE         => 'Сделка создана',
        self::STATUS_OPEN           => 'Сделка открыта, ожидайте подтверждения',
        self::STATUS_CANCEL         => 'Сделка отменена',
        self::STATUS_BLOCK          => 'Сделка подтверждена и средства заблокированы',
        self::STATUS_MONEY_SENDED   => 'Средства переведены',
        self::STATUS_MONEY_RECEIVED => 'Средства получены',
        self::STATUS_CLOSE          => 'Сделка завершена',
        self::STATUS_AUDIT_WAIT     => 'Сделка ожидает арбитра',
        self::STATUS_AUDIT_ACCEPTED => 'Сделка разбирается арбитром',
        self::STATUS_AUDIT_FINISH   => 'Сделка разобрана арбитром и завершена',
        self::STATUS_HIDE           => 'Сделка закрыта но не завершилась',
    ];

    public static $statusList2 = [

        // Статус показывается у инициатора сделки (Я инициатор сделки)
        Assessment::SCREEN_DEAL => [
            self::STATUS_CREATE         => 'Сделка создана',
            self::STATUS_OPEN           => 'Сделка открыта, ожидайте подтверждения',
            self::STATUS_CANCEL         => 'Сделка отменена',
            self::STATUS_BLOCK          => 'Сделка подтверждена и средства заблокированы',
            self::STATUS_MONEY_SENDED   => 'Средства переведены',
            self::STATUS_MONEY_RECEIVED => 'Средства получены',
            self::STATUS_CLOSE          => 'Сделка завершена',
            self::STATUS_AUDIT_WAIT     => 'Сделка ожидает арбитра',
            self::STATUS_AUDIT_ACCEPTED => 'Сделка разбирается арбитром',
            self::STATUS_AUDIT_FINISH   => 'Сделка разобрана арбитром и завершена',
            self::STATUS_HIDE           => 'Сделка закрыта но не завершилась',
        ],

        // Статус показывается у инициатора предложения (Я не инициатор сделки)
        Assessment::SCREEN_OFFER => [
            self::STATUS_CREATE         => 'Сделка создана',
            self::STATUS_OPEN           => 'Сделка открыта, подтвердите сделку',
            self::STATUS_CANCEL         => 'Сделка отменена',
            self::STATUS_BLOCK          => 'Сделка подтверждена и монеты заблокированы',
            self::STATUS_MONEY_SENDED   => 'Средства переведены',
            self::STATUS_MONEY_RECEIVED => 'Средства получены',
            self::STATUS_CLOSE          => 'Сделка завершена',
            self::STATUS_AUDIT_WAIT     => 'Сделка ожидает арбитра',
            self::STATUS_AUDIT_ACCEPTED => 'Сделка разбирается арбитром',
            self::STATUS_AUDIT_FINISH   => 'Сделка разобрана арбитром и завершена',
            self::STATUS_HIDE           => 'Сделка закрыта но не завершилась',
        ],
    ];

    public function rules()
    {
        return [
            ['user_id', 'integer'],

            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'compare', 'operator' => '!=', 'compareValue' => 0],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'compare', 'operator' => '!=', 'compareValue' => 0],

            ['offer_id', 'required'],
            ['offer_id', 'integer'],
            ['offer_id', 'compare', 'operator' => '!=', 'compareValue' => 0],

            ['created_at', 'integer'],
            ['volume', 'integer'],
            ['volume_vvb', 'integer'],

            ['type_id', 'integer'],

            ['status', 'integer'],

            ['chat_room_id', 'integer'],
        ];
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }


    /**
     * @return UserAvatar
     */
    public function getUser()
    {
        if (is_null($this->user)) {
            $this->user = UserAvatar::findOne($this->user_id);
        }

        return $this->user;
    }
    /**
     * @return \common\models\exchange\Offer
     */
    public function getOffer()
    {
        return Offer::findOne($this->offer_id);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id'     => 'Пользователь',
            'price'       => 'Цена',
            'currency_id' => 'Валюта',
            'offer_id'    => 'Предложение',
            'created_at'  => 'Создано',
            'type_id'     => 'Тип сделки',
            'status'      => 'Статус',
        ];
    }

    /**
     * Вычитает объем из предложения (должна вызываться после завершения сделки)
     */
    public function finish()
    {
        // Вычитаю объем из предложения
        $offer = $this->getOffer();
        $offer->volume_finish = bcsub($offer->volume_finish, $this->volume);
        $offer->volume_io_finish = bcsub($offer->volume_io_finish, $this->volume_vvb);

        // Если выкуплены все монеты
        if (bccomp($offer->volume_io_finish, '0') == 0) {
            // скрываю сделку
            $offer->is_hide = 1;
        } else {
            // Если верхний предел стал ниже минимального
            if (bccomp($offer->volume_io_finish, $offer->volume_io_start) == -1) {
                // скрываю сделку
                $offer->is_hide = 1;

                // Отправляю уведомление пользователю
                $user = $offer->getUser();
                \common\services\Subscribe::sendArray([$user->email], 'В предложении осталось монет меньше минимального порога', 'offer_min', [
                    'offer' => $offer,
                ]);
            }
        }

        // После закрытия сделок пересчитывать значения надо для обоих пользователей, так как оба участвовали в сделке
        $this->finish_updateDealCount(UserAvatar::findOne($this->user_id));
        $this->finish_updateDealCount(UserAvatar::findOne($this->getOffer()->user_id));

        // Сохраняю все параметры
        $offer->save();
    }

    /**
     * Обновляет кол-во сделок для пользователя $user
     *
     * @param \common\models\UserAvatar $user
     */
    private function finish_updateDealCount($user)
    {
        // Пересчитываю кол-во сделок deals_count_offer
        $offerList = Offer::find()->where(['user_id' => $user->id])->select('id')->column();
        $dealsCount = Deal::find()
            ->where(['offer_id' => $offerList])
            ->andWhere([
                'status' => [
                    Deal::STATUS_MONEY_RECEIVED,
                    Deal::STATUS_CLOSE,
                    Deal::STATUS_AUDIT_FINISH,
                ],
            ])
            ->count();

        // Пересчитываю кол-во сделок deals_count_all
        $Deal_count = Deal::find()
            ->where(['user_id' => $user->id])
            ->andWhere([
                'status' => [
                    Deal::STATUS_MONEY_RECEIVED,
                    Deal::STATUS_CLOSE,
                    Deal::STATUS_AUDIT_FINISH,
                ],
            ])
            ->count();
        $deals_count_all = $dealsCount + $Deal_count;

        // Сохраняю данные у пользователя
        $user->deals_count_all = $deals_count_all;
        $user->deals_count_offer = $dealsCount;
        $user->save();
    }

    public function init()
    {
        parent::init(); // TODO: Change the autogenerated stub

        $this->on(self::EVENT_AFTER_MONEY_RECEIVED, function (\yii\base\Event $item) {
            \Yii::info(VarDumper::dumpAsString($item), 'avatar\common\models\exchange\Deal::init');

            /** @var \common\models\exchange\Deal $deal */
            $deal = $item->sender;
            if ($deal->currency_io == 12) { // NEIRO
                // Здесь рефералка

                $cInt = Currency::findOne(Currency::NEIRO);
                // Это продажа от официального представителя?
                $ids = \common\models\Config::get('trusted-users');
                if (\cs\Application::isEmpty($ids)) $ids = [];
                else $ids = \yii\helpers\Json::decode($ids);

                if (in_array($deal->getOffer()->user_id, $ids)) {
                    // Здесь начисляю монеты

                    // Если пользователь уже встроен в пирамиду, то ссылка не актуальна.
                    $partner = \common\models\UserPartner::findOne(['user_id' => $this->user_id]);
                    \Yii::info($partner, 'avatar\common\models\exchange\Deal::init3');
                    if (is_null($partner)) {
                        $partner_id = $this->partner_id;

                        \Yii::info($partner_id, 'avatar\common\models\exchange\Deal::init3');
                        if ($partner_id) {
                            $partner = \common\models\UserPartner::add([
                                'school_id' => 1,
                                'user_id'   => $this->user_id,
                                'parent_id' => $partner_id,
                            ]);
                        }
                    }
                    $referal_wallet_id = \Yii::$app->params['referal-program']['wallet_id'];

                    \Yii::info($referal_wallet_id, 'avatar\common\models\exchange\Deal::init4');
                    \Yii::info($partner, 'avatar\common\models\exchange\Deal::init5');

                    if ($partner) {
                        // распределение реферальных начислений
                        $levels = ReferalLevel::find()->all();
                        if (count($levels) > 0) {
                            ArrayHelper::multisort($levels, 'level');
                            $referal_wallet = Wallet::findOne($referal_wallet_id);
                            $currency_id = $referal_wallet->currency_id;
                            $price = $this->volume_vvb;
                            for ($level = 1; $level <= count($levels); $level++) {
                                $parent_id = $partner->parent_id;

                                /** @var \common\models\ReferalLevel $referalLevel */
                                $referalLevel = $levels[$level - 1];

                                // множитель для цены
                                $m = $referalLevel->percent / 10000;

                                // сумма отчислений
                                $amount = (int)($price * $m);
                                $data = UserBill::getInternalCurrencyWallet($currency_id, $parent_id);
                                $bill = $data['billing'];
                                /** @var \common\models\piramida\Wallet $wallet */
                                $wallet = $data['wallet'];

                                // Если у пользователя более 1000 монет то начисляю рефералку
                                if ($wallet->amount >= (1000 * pow(10, $cInt->decimals))) {
                                    $transaction = $referal_wallet->move2(
                                        $wallet,
                                        $amount,
                                        'Реферальные начисления за заказ = ' . $this->id . ' от uid=' . $this->user_id . ' к uid=' . $parent_id,
                                        NeironTransaction::TYPE_REFERAL
                                    );

                                    // Делаю запись о типе транзакции если это NERON
                                    NeironTransaction::add([
                                        'transaction_id' => $transaction['transaction']['id'],
                                        'operation_id'   => $transaction['operation_add']['id'],
                                        'type_id'        => NeironTransaction::TYPE_REFERAL,
                                    ]);

                                    // Записываю реферальные начисления
                                    $ReferalTransaction = ReferalTransaction::add([
                                        'request_id'     => $this->id,
                                        'level'          => $level,
                                        'from_uid'       => $this->user_id,
                                        'to_uid'         => $parent_id,
                                        'to_wid'         => $wallet->id,
                                        'amount'         => $amount,
                                        'transaction_id' => $transaction['transaction']['id'],
                                        'currency_id'    => $wallet->currency_id,
                                    ]);
                                }

                                // выбираю следующего из уровня
                                $partner = \common\models\UserPartner::findOne(['user_id' => $partner->parent_id]);

                                // если дерево кончилось то выхожу
                                if (is_null($partner)) break;
                            }
                        }
                    }
                }
            }

            \Yii::info(VarDumper::dumpAsString($item), 'avatar\common\models\exchange\Deal::init');
        });
    }

    private function init_findConribution($a)
    {
        /** @var \common\models\Contribution $c */
        foreach (Contribution::find()->all() as $c) {
            if ($a > $c->amount && $a < $c->amount_max) {
                return $c;
            }
        }

        return null;
    }
}