<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\exchange;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int id                      Идентификатор заявки
 * @property int deal_id                 Идентификатор сделки
 * @property int user_id                 Оцениваемый пользователь
 * @property int user_id_author          Идентификатор пользователя который оценивает (автор оценки)
 * @property int value                   Оценка -1, 0, 1
 * @property int created_at              Момент создания предложения
 * @property int offer_id                Идентификатор преложения offer.id
 * @property int offer_type              Тип преложения offer.type_id
 * @property int role                    В какой роли находится оцениваемый (user_id) 1 - продавец, 2 - покупатель
 *
 *
 */
class Assessment extends ActiveRecord
{
    const SCREEN_OFFER = 1;
    const SCREEN_DEAL = 2;

    /**
     * @return \common\models\exchange\Deal
     */
    public function getDeal()
    {
        return Deal::findOne($this->deal_id);
    }

    /**
     * @return \common\models\UserAvatar
     */
    public function getUser()
    {
        return UserAvatar::findOne($this->user_id);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }


    public static function add($fields)
    {
        $item = new self($fields);
        $item->save();
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}