<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\exchange;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int    id           идентификатор
 * @property int    currency_id  валюта db.currency.id
 * @property int    sort_index
 * @property string code
 * @property string title
 */
class OfferPay extends ActiveRecord
{
    public static function tableName()
    {
        return 'offer_pay';
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}