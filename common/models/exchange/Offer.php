<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\exchange;

use common\models\avatar\Currency;
use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int    id                         Идентификатор заявки
 * @property int    user_id                    Идентификатор пользователя кто сделал предложение
 * @property int    price                      Цена по которой предлагается купить продать VVB
 * @property int    price_is_free              Флаг для цены. Свободная цена? 0 - нет точная, 1 - да свободная
 * @property int    currency_id                Валюта цены, указывает на db.currency
 * @property int    type_id                    Вид сделки 1 - купить, 2 - продать
 * @property string condition                  Условия сделки, простой текст
 * @property string name                       Заголовок
 * @property int    created_at                 Момент создания предложения
 * @property int    avg_deal                   Среднее время сделки в сек для создателя предложения user_id
 * @property int    assessment_positive        Кол-во положительных оценок для создателя предложения user_id
 * @property int    assessment_negative        Кол-во положительных оценок для создателя предложения user_id
 * @property int    deals_count                Кол-во сделок для всех предложений автором которых является user_id
 * @property int    currency_io
 * @property string volume_start
 * @property string volume_finish
 * @property string volume_io_start
 * @property string volume_io_finish
 * @property string volume_io_finish_deal
 * @property int    offer_pay_id
 *
 * @property int    volume_io_start_m
 * @property int    volume_io_finish_m
 * @property int    price_m
 * @property int    is_hide                     Флаг. Скрыто предложение? Если максимальный порог опускается ниже минимального то сделка скрывается - 1, 0 - показывается. по умолчанию - 0. Если = 2 то скрывается из общего списка и своего
 * @property int    is_send_time_out_notification  Флаг. Отправлено уведомление об снятии предложения с табло, 0 - не отправлено, 1 - отпрвлено
 */
class Offer extends ActiveRecord
{
    private $user;
    private $currency;

    const TYPE_ID_BUY = 1;  // Я покупаю (Я - тот кто делает предложение)
    const TYPE_ID_SELL = 2; // Я продаю (Я - тот кто делает предложение)

    public function rules()
    {
        return [
            ['type_id', 'required'],
            ['type_id', 'integer'],
            ['type_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['price_m', 'double'],

            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            ['currency_id', 'compare', 'operator' => '!=', 'compareValue' => 0, 'message' => 'Это поле обязательно для заполнения'],

            ['avg_deal', 'integer'],

            ['assessment_positive', 'integer'],
            ['assessment_negative', 'integer'],

            ['deals_count', 'integer'],

            ['offer_pay_id', 'integer'],

            ['volume_io_start_m', 'double'],
            ['volume_io_finish_m', 'double'],

            ['volume_start', 'string'],
            ['volume_finish', 'string'],
            ['volume_io_start', 'string'],
            ['volume_io_finish', 'string'],
            ['volume_io_finish_deal', 'string'],

            ['currency_io', 'integer'],

            ['user_id', 'integer'],

            ['is_hide', 'integer'],

            ['condition', 'string'],

            ['name', 'string', 'max' => 255],
        ];
    }

    public function convertDown()
    {
        $d = $this->getCurrency()->decimals;
        $this->price = bcdiv($this->price, bcpow(10, $d), $d);
        $this->volume_start = bcdiv($this->volume_start, bcpow(10, $d), $d);
        $this->volume_finish = bcdiv($this->volume_finish, bcpow(10, $d), $d);
    }

    public function convertUp()
    {
        $d = $this->getCurrency()->decimals;
        $this->price = bcmul($this->price, bcpow(10, $d));
        $this->volume_start = bcmul($this->volume_start, bcpow(10, $d));
        $this->volume_finish = bcmul($this->volume_finish, bcpow(10, $d));
    }

    /**
     * @return UserAvatar
     */
    public function getUser()
    {
        if (is_null($this->user)) {
            $this->user = UserAvatar::findOne($this->user_id);
        }

        return $this->user;
    }

    public function getCurrency()
    {
        if (is_null($this->currency)) {
            $this->currency = $this->_getCurrency();
        }

        return $this->currency;
    }

    public function _getCurrency()
    {
        return Currency::findOne($this->currency_id);
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type_id'     => 'Тип сделки',
            'price'       => 'Цена',
            'currency_id' => 'Валюта',
            'user_id'     => 'Пользователь',
            'condition'   => 'Условия сделки',
        ];
    }
}