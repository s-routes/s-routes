<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\exchange;

use common\models\BillingMain;
use common\models\UserAvatar;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * @property int        id                      идентификатор заявки
 * @property int        sort_index
 * @property string     name
 *
 *
 */
class PayMethod extends ActiveRecord
{
    public static function tableName()
    {
        return 'pay_method';
    }

    public static function add($fields)
    {
        $item = new self($fields);
        $ret = $item->save();
        if (!$ret) \cs\services\VarDumper::dump($item->errors);
        $item->id = $item::getDb()->lastInsertID;

        return $item;
    }
}