<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer request_id
 * @property integer type_id        Тип заявки 1 - convert_operation, 2 - binance_order, 3 - покупка пакета
 * @property integer currency_id    dbWallet.currency.id
 * @property integer level
 * @property integer from_uid
 * @property integer to_uid
 * @property integer to_wid
 * @property integer amount
 * @property integer transaction_id
 * @property integer created_at
 */
class ShopReferalTransaction extends ActiveRecord
{
    const TYPE_CONVERT = 1;         // convert_operation
    const TYPE_BINANCE = 2;         // binance_order
    const TYPE_PACKET = 3;          // покупка пакета

    public static function tableName()
    {
        return 'shop_referal_transaction';
    }

    public function behaviors()
    {
        return [
            [
                'class'      => '\yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                // если вместо метки времени UNIX используется datetime:
                // 'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}