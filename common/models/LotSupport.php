<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer room_id
 * @property integer last_message
 */
class LotSupport extends ActiveRecord
{
    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    public static function get($user_id)
    {
        $supportShop = self::findOne(['user_id' => $user_id]);
        if (is_null($supportShop)) {
            $room = \common\models\ChatRoom::add(['last_message' => null]);
            $supportShop = self::add([
                'user_id' => $user_id,
                'room_id' => $room->id,
            ]);
        }

        return $supportShop;
    }

}
