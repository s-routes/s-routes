<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer room_id
 * @property integer last_message
 */
class SupportShop extends ActiveRecord
{
    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}
