<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use cs\services\VarDumper;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property integer id
 * @property integer user_id
 * @property integer parent_id
 */
class ShopUserPartner extends ActiveRecord
{
    public static function tableName()
    {
        return 'shop_user_partner';
    }

    public function rules()
    {
        return [
            ['parent_id', 'integer'],
            ['user_id', 'integer'],
        ];
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}