<?php

namespace common\models;

use common\models\piramida\Wallet;
use yii\db\ActiveRecord;

/**
 * @property integer id
 * @property integer user_id
 * @property integer wallet_id
 * @property integer currency_id        dbWallet.currency.id
 *
 * @package common\models
 */
class UserBill2 extends ActiveRecord
{
    public static function tableName()
    {
        return 'user_bill2';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

    /**
     * @return  \common\models\piramida\Wallet
     */
    public function getWallet()
    {
        return Wallet::findOne($this->wallet_id);
    }
}