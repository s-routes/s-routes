<?php

namespace common\models;

use common\models\piramida\Currency;
use Yii;
use yii\db\ActiveRecord;

/**
 * @property integer    id
 * @property integer    currency_io_id
 * @property integer    output_min
 * @property integer    benance_min
 * @property integer    decimals
 * @property double     comission_out
 * @property double     benance_fee
 * @property string     master_wallet
 * @property integer    master_wallet_is_binance
 * @property integer    is_api                  Флаг, есть ли API для зачисления монеты, 0 - нет API, 1 - есть API
 * @property string     name
 * @property string     wallet_field_name
 * @property string     function_check
 * @property string     code                    КОД блокчейна на котором выпущена монета
 * @property string     warning                 Предупреждение для формы ввода
 * @property string     extended_info
 */
class CurrencyIoModification extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_io_modification';
    }

    /**
     * @param array $fields
     *
     * @return self
     * @throws
     */
    public static function add($fields)
    {
        $i = new self($fields);
        $i->save();
        $i->id = $i::getDb()->lastInsertID;

        return $i;
    }
}
