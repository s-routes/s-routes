<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 *
 * @property integer id
 * @property string  title
 * @property integer user_id
 *
 * @package common\models
 */
class Company extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_company';
    }

    public function rules()
    {
        return [[['title', 'user_id', 'id'], 'safe']];
    }

    /**
     * @return \common\models\koop\LinkCompanyKooperative[]
     */
    public function getKooperativeList()
    {
        return LinkCompanyKooperative::find()->where(['company_id' => $this->id])->all();
    }
}