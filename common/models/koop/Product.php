<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 *
 * @property integer        id
 * @property integer        company_id
 * @property string(20)     code
 * @property string(200)    image
 * @property string(255)    title
 * @property double         price
 * @property integer        currency
 * @property integer        count
 * @property integer        created_at
 * @property integer        is_balance_koop     0 - на балансе компании, 1 - на балансе кооператива
 * @property integer        koop_id             идентификатор на балансе какого кооператива числится данный товар.
 * @property integer        is_request          сформирована ли заявка на внесение пая? 0 - нет, 1 - да
 *
 * @package common\models
 */
class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_products';
    }

    public function rules()
    {
        return [
            [array_keys($this->attributes), 'safe'],
        ];
    }

    /**
     * @return \common\models\koop\Company
     */
    public function getCompany()
    {
        return Company::findOne($this->company_id);
    }
}