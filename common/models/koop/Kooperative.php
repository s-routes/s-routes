<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 * Class Kooperative
 *
 * @property integer id
 * @property string  title
 * @property integer user_id
 * @property integer currency_id
 *
 * @package common\models
 */
class Kooperative extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop';
    }

    public function rules()
    {
        return [[['title', 'user_id', 'currency_id', 'id'], 'safe']];
    }
}