<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models\koop;

use yii\db\ActiveRecord;

/**
 * Элемент списка чека
 *
 * @property integer id
 * @property double  count
 * @property integer product_id
 *
 * @package common\models
 */
class ContractRow extends ActiveRecord
{
    public static function tableName()
    {
        return 'koop_buy_list';
    }
}