<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 28.02.2017
 * Time: 23:49
 */

namespace common\models;

use common\models\avatar\Currency;
use yii\db\ActiveRecord;

/**
 * Class Token
 *
 * @property integer id
 * @property integer tx_id
 * @property integer deal_id
 * @property integer volume_io
 *
 * @package common\models
 */
class DealClosed extends ActiveRecord
{

    public static function tableName()
    {
        return 'deal_closed';
    }

    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }

}