<?php


namespace common\services;


use cs\services\VarDumper;

class FrontEnd extends \yii\base\BaseObject

{
    public static function ColumnTxId($header, $attrName)
    {
        return [
            'header'        => $header,
            'attribute'     => 'id',
            'headerOptions' => [
                'style' => \yii\helpers\Html::cssStyleFromArray([
                    'width' => '12%',
                ]),
            ],
            'contentOptions' => [
                'data' => ['attribute-name' => $attrName],
                'style' => \yii\helpers\Html::cssStyleFromArray([
                    'width' => '12%',
                ]),
            ],
            'content'       => function ($model, $key, $index, $column) {
                $v = \yii\helpers\ArrayHelper::getValue($model, $column->contentOptions['data']['attribute-name']);
                if (is_null($v)) return '';
                $transaction = new \common\models\piramida\Transaction(['id' => $v]);
                $address = $transaction->getAddress();
                $addressShort = $transaction->getAddressShort();

                return \yii\helpers\Html::tag('code', $addressShort, [
                    'data'  => [
                        'toggle'         => 'tooltip',
                        'clipboard-text' => $address,
                    ],
                    'class' => 'buttonCopy',
                    'title' => \Yii::t('c.BBXvjwPu8f', 'Транзакция. Нажми чтобы скопировать'),
                ]);
            },
        ];
    }
    public static function ColumnDateTime($header, $attrName)
    {
        return [
            'header'  => $header,
            'contentOptions' => [
                'data' => ['attribute-name' => $attrName],
            ],
            'content' => function ($item, $key, $index, $column) {
                $v = \yii\helpers\ArrayHelper::getValue($item, $column->contentOptions['data']['attribute-name'], 0);
                if ($v == 0) return '';

                return \yii\helpers\Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => \Yii::$app->formatter->asDatetime($v)]);
            },
        ];
    }

    public static function ColumnUserAvatar($header, $attrName)
    {
        return [
            'header'  => $header,
            'contentOptions' => [
                'data' => ['attribute-name' => $attrName],
            ],
            'content' => function ($item, $key, $index, $column) {
                $user = \common\models\UserAvatar::findOne($item[$column->contentOptions['data']['attribute-name']]);
                $i = $user->getAvatar();

                return \yii\helpers\Html::img($i, [
                    'class'  => "img-circle",
                    'data'   => ['toggle' => 'tooltip'],
                    'title'  => $user->getName2(),
                    'width'  => 40,
                    'height' => 40,
                    'style'  => 'margin-bottom: 0px;',
                ]);
            },
        ];
    }
}