<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\UserDevice;
use cs\services\File;
use cs\services\VarDumper;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class BlockCypher extends Component
{
    const COIN_LTC = 'ltc';
    const COIN_BTC = 'btc';
    const COIN_DASH = 'dash';

    public $apiUrl = 'https://api.blockcypher.com/v1';
    public $key;
    public $isTest = false;

    public function _call($coin, $path, $params = [])
    {
        $net = ($this->isTest) ? 'test' : 'main';
        $url = $this->apiUrl . '/' . $coin . '/' . $net . '/' . $path;
        $client = new Client(['baseUrl' => $url]);
        $params['token'] = $this->key;
        $response = $client->get('', $params)->send();

        return $response;
    }

    public function call($coin, $path, $params = [])
    {
        $res = $this->_call($coin, $path, $params);
        $data = Json::decode($res->content);

        return $data;
    }

}