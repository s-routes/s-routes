<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\services;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\Connection;
use yii\di\Instance;
use yii\helpers\VarDumper;
use yii\log\Target;
use yii\web\Request;

/**
 * DbTarget stores log messages in a database table.
 *
 * The database connection is specified by [[db]]. Database schema could be initialized by applying migration:
 *
 * ```
 * yii migrate --migrationPath=@yii/log/migrations/
 * ```
 *
 * If you don't want to use migration and need SQL instead, files for all databases are in migrations directory.
 *
 * You may change the name of the table used to store the data by setting [[logTable]].
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class DbTarget extends Target
{
    /**
     * @var Connection|array|string the DB connection object or the application component ID of the DB connection.
     * After the DbTarget object is created, if you want to change this property, you should only assign it
     * with a DB connection object.
     * Starting from version 2.0.2, this can also be a configuration array for creating the object.
     */
    public $db = 'db';
    /**
     * @var string name of the DB table to store cache content. Defaults to "log".
     */
    public $logTable = '{{%log2}}';


    /**
     * Initializes the DbTarget component.
     * This method will initialize the [[db]] property to make sure it refers to a valid DB connection.
     * @throws InvalidConfigException if [[db]] is invalid.
     */
    public function init()
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::className());
    }

    /**
     * Stores log messages to DB.
     */
    public function export()
    {
        if ($this->db->getTransaction()) {
            // create new database connection, if there is an open transaction
            // to ensure insert statement is not affected by a rollback
            $this->db = clone $this->db;
        }

        $tableName = $this->db->quoteTableName($this->logTable);
        $sql = "INSERT INTO $tableName ([[level]], [[category]], [[log_time]], [[ip]], [[ip_int]], [[session_id]], [[user_id]], [[message]])
                VALUES (:level, :category, :log_time, :ip, :ip_int, :session_id, :user_id, :message)";
        $command = $this->db->createCommand($sql);
        foreach ($this->messages as $message) {
            list($text, $level, $category, $timestamp) = $message;
            if (!is_string($text)) {
                // exceptions may not be serializable if in the call stack somewhere is a Closure
                if ($text instanceof \Throwable || $text instanceof \Exception) {
                    $text = (string) $text;
                } else {
                    $text = VarDumper::export($text);
                }
            }
            $prefix = $this->getMessagePrefix($message);

            $command
                ->bindValues([
                    ':level'      => $level,
                    ':category'   => $category,
                    ':log_time'   => $timestamp,
                    ':ip'         => $prefix['ip'],
                    ':ip_int'     => $this->ipToInt($prefix['ip']),
                    ':session_id' => $prefix['sessionID'],
                    ':user_id'    => $prefix['userID'],
                    ':message'    => $text,
                ])
                ->execute();
        }
    }

    /**
     * Преобразовывает IP в INT
     *
     * @param $ip
     *
     * @return int
     */
    private function ipToInt($ip)
    {
        if ($ip == '') return null;
        if ($ip == '-') return null;
        $list = explode('.', $ip);
        $sum = 256*256*256 * $list[0] + 256*256 * $list[0] + 256 * $list[0] + $list[3];

        return $sum;
    }

    /**
     * Returns a string to be prefixed to the given message.
     * If [[prefix]] is configured it will return the result of the callback.
     * The default implementation will return user IP, user ID and session ID as a prefix.
     * @param array $message the message being exported.
     * The message structure follows that in [[Logger::messages]].
     * @return array
     * [
     *   'ip'           => $ip,
     *   'userID'       => $userID,
     *   'sessionID'    => $sessionID,
     * ]
     */
    public function getMessagePrefix($message)
    {
        $request = Yii::$app->getRequest();
        $ip = $request instanceof Request ? $request->getUserIP() : '-';

        /* @var $user \yii\web\User */
        $user = Yii::$app->has('user', true) ? Yii::$app->get('user') : null;
        if ($user && ($identity = $user->getIdentity(false))) {
            $userID = $identity->getId();
        } else {
            $userID = null;
        }

        /* @var $session \yii\web\Session */
        $session = Yii::$app->has('session', true) ? Yii::$app->get('session') : null;
        if (is_null($session)) {
            $sessionID = '';
        } else {
            $sessionID = $session->getId();
        }

        return [
            'ip' => $ip,
            'userID' => $userID,
            'sessionID' => $sessionID,
        ];
    }

}
