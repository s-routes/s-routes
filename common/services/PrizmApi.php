<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\UserDevice;
use cs\services\File;
use cs\services\VarDumper;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class PrizmApi extends Component
{
    public $url = 'http://5.187.3.161:7742/prizm';

    /** @var  string ключ */
    public $key;

    /**
     * Отправляет команду $path на сервер с параметрами $params
     *
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function _post($path, $params = [])
    {
        $headers = [
            'key' => $this->key,
        ];
        $params = ArrayHelper::merge($params, $headers);

        $provider = new Client(['baseUrl' => $this->url]);
        $request = $provider->post($path, $params);
        $response = $request->send();

        return $response;
    }

    /**
     * Отправляет команду $path на сервер с параметрами $params
     *
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function post($path, $params = [])
    {
        $response = $this->_post($path, $params);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\\PrizmApi\\response');
        if ($response->statusCode != 200) {
            throw new \Exception('PRIZM API server return status code = ' . $response->statusCode);
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('PRIZM API server return not JSON');
        }

        return $data;
    }

}