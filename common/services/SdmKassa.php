<?php


namespace common\services;


use cs\services\VarDumper;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * https://3ds.sdm.ru/cgi-bin/cgi_link – промышленный шлюз
 * https://3dst.sdm.ru/cgi-bin/cgi_link – тестовый шлюз
 *
 * @see https://cloud1.cloud999.ru/upload/cloud/16208/48244_N9rgCVtcjh.docx
 */
class SdmKassa extends Component
{
    public $url = 'https://3dst.sdm.ru/cgi-bin/cgi_link';
//    public $url = 'https://payment-test.sdm.ru';
    public $MERCHANT;
    public $TERMINAL;
    public $secret;

    /**
     * @params array $options
     * [
     *  'successUrl' => string
     * ]
     * @return array
     * {
     * "Success" : true,
     * "ErrorCode" : "0",
     * "TerminalKey" : "TinkoffBankTest",
     * "Status" : "NEW",
     * "PaymentId": "13660",
     * "OrderId" : "21050",
     * "Amount" : 100000,
     * "PaymentURL" : "https://securepay.tinkoff.ru/rest/Authorize/1B63Y1"
     * }
     */
    public function createPayment($billing, $description, $destinationName, $options)
    {
        $client = new \yii\httpclient\Client([
            'baseUrl'        => $this->url,
//            'requestConfig' => [
////                'format' => \yii\httpclient\Client::FORMAT_JSON,
//            ],
//            'responseConfig' => [
////                'format' => \yii\httpclient\Client::FORMAT_JSON,
//            ],
        ]);

        $params['AMOUNT'] = $billing->sum_after / 100;
        $params['CURRENCY'] = 643;
        $params['ORDER'] = $billing->id;
        $params['DESC'] = $description;
        $params['MERCH_NAME'] = $destinationName;
        $params['MERCH_URL'] = 'https://topmate.one';
        $params['MERCHANT'] = $this->MERCHANT;
        $params['TERMINAL'] = $this->TERMINAL;
        $params['TIMESTAMP'] = time();
        $params['MERCH_GMT'] = '-3';
        $params['TRTYPE'] = '1';
        $params['EMAIL'] = '';
        $params['NONCE'] = Security::generateRandomString();
        $params['BACKREF'] = ArrayHelper::getValue($options, 'successUrl', '');

//        $params['P_SIGN'] = $this->getSign($params);

        $payment = $client
            ->post(
                '',
                $params
            )
            ->send();

        if ($payment->headers['http-code'] != 200) {
            VarDumper::dump($payment);
            throw new \Exception('Код от SberAPI'.$payment->headers['http-code']);
        }
        VarDumper::dump($payment);
        return $payment->data;
    }

    /**
     * Сравнивает контрольную сумму $sum с контрольной суммой параметров $params используя симетричное шифрование
     * @param array  $params параметры Ключ значение
     * @param string $sum контрольная сумма от источника получения уведомления. От "Банка"
     * @return bool
     */
    public function isCheckSum($params, $sum)
    {
        ksort($params);
        $rows = [];
        foreach ($params as $k => $v) {
            if ($k != 'type') {
                $rows[] = $k . ';' . $v . ';';
            }
        }
        $data = join('', $rows);

        $key = $this->secret;
        $hmac = hash_hmac ( 'sha256' , $data , $key);

        return strtoupper($hmac) == $sum;
    }

    public function getSign($params)
    {
        $fields = [
            'AMOUNT', 'CURRENCY', 'ORDER', 'DESC', 'MERCH_NAME', 'MERCH_URL', 'MERCHANT', 'TERMINAL', 'EMAIL', 'TRTYPE', 'TIMESTAMP', 'NONCE', 'BACKREF'
        ];
        $String = '';
        foreach ($fields as $k) {
            if (!isset($params[$k])) throw new \Exception('Не найден ключ '. $k);
            $v = $params[$k];
            $String .= strlen($v) . $v;
        }

        $skey = $this->secret;

        return hash_hmac('sha1', $String, hex2bin($skey));
    }

}