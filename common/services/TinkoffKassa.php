<?php


namespace common\services;


use cs\services\VarDumper;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * @see https://www.tinkoff.ru/kassa/develop/api/payments/
 */
class TinkoffKassa extends Component
{
    public $url = 'https://securepay.tinkoff.ru/v2';
    public $login;
    public $password;

    /**
     * @params array $options
     * [
     *  'successUrl' => string
     * ]
     * @return array
     * {
     * "Success" : true,
     * "ErrorCode" : "0",
     * "TerminalKey" : "TinkoffBankTest",
     * "Status" : "NEW",
     * "PaymentId": "13660",
     * "OrderId" : "21050",
     * "Amount" : 100000,
     * "PaymentURL" : "https://securepay.tinkoff.ru/rest/Authorize/1B63Y1"
     * }
     */
    public function createPayment($billing, $description, $destinationName, $options)
    {
        $client = new \yii\httpclient\Client([
            'baseUrl'        => $this->url,
            'requestConfig' => [
                'format' => \yii\httpclient\Client::FORMAT_JSON,
            ],
            'responseConfig' => [
                'format' => \yii\httpclient\Client::FORMAT_JSON,
            ],
        ]);

        $params['Amount'] = (int)($billing->sum_after);
        $params['OrderId'] = $billing->id;
        $params['password'] = $this->password;
        $params['TerminalKey'] = $this->login;
        $params['Description'] = $description;
        $params['PayType'] = 'O';
        $params['NotificationURL'] = 'https://topmate.one/shop-order/success?type=tinkoff';
        $params['SuccessURL'] = ArrayHelper::getValue($options, 'successUrl', '');

        $params['Token'] = $this->getSign($params);

        $payment = $client
            ->post(
                'Init',
                $params
            )
            ->send();

        if ($payment->headers['http-code'] != 200) {
            VarDumper::dump($payment);
            throw new \Exception('Код от SberAPI'.$payment->headers['http-code']);
        }

        return $payment->data;
    }

    /**
     * Сравнивает контрольную сумму $sum с контрольной суммой параметров $params используя симетричное шифрование
     * @param array  $params параметры Ключ значение
     * @param string $sum контрольная сумма от источника получения уведомления. От "Банка"
     * @return bool
     */
    public function isCheckSum($params, $sum)
    {
        ksort($params);
        $rows = [];
        foreach ($params as $k => $v) {
            if ($k != 'type') {
                $rows[] = $k . ';' . $v . ';';
            }
        }
        $data = join('', $rows);

        $key = $this->secret;
        $hmac = hash_hmac ( 'sha256' , $data , $key);

        return strtoupper($hmac) == $sum;
    }

    public function getSign($params)
    {
        $rows = [];
        foreach ($params as $k => $v) {
            if (!in_array($k, ['Receipt', 'DATA'])) {
                $rows[$k] = $v;
            }
        }
        $rows['Password'] = $this->password;
        ksort($rows);
        $s = '';
        foreach ($rows as $k => $v) {
            $s .= $v;
        }
        $hash = hash('sha256', $s);

        return $hash;
    }

}