<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\UserDevice;
use cs\services\VarDumper;
use yii\base\BaseObject;

class Security extends BaseObject
{
    const ALGORITM_ALPHA = 0;
    const ALGORITM_NUMS = 1;
    const ALGORITM_NUMS_ALPHA = 2;

    public static function generateRandomString($length = 32, $algoritm = null)
    {
        if (is_null($algoritm)) {
            $algoritm = self::ALGORITM_NUMS_ALPHA;
        }
        switch ($algoritm) {
            case self::ALGORITM_ALPHA:
                $string = \Yii::$app->security->generateRandomString($length + 15);
                $string = str_replace('_', '', str_replace('-', '', $string));
                $string = str_replace('0', '', $string);
                $string = str_replace('1', '', $string);
                $string = str_replace('2', '', $string);
                $string = str_replace('3', '', $string);
                $string = str_replace('4', '', $string);
                $string = str_replace('5', '', $string);
                $string = str_replace('6', '', $string);
                $string = str_replace('7', '', $string);
                $string = str_replace('8', '', $string);
                $string = str_replace('9', '', $string);
                $string = substr($string, 0, $length);
                break;
            case self::ALGORITM_NUMS_ALPHA:
                $string = \Yii::$app->security->generateRandomString($length + 15);
                $string = str_replace('_', '', str_replace('-', '', $string));
                $string = substr($string, 0, $length);
                break;
            case self::ALGORITM_NUMS:
                $s = '0123456789';
                $string = '';
                for($i = 0; $i < $length; $i++) {
                    $string = $string . $s;
                }
                $string = str_shuffle($string);
                $string = substr($string, 0, $length);
                break;
            default:
                throw new \Exception('Такой опции не предусмотрено');
        }

        return $string;
    }

    /**
     * Вызывается после логина
     *
     * @param \common\models\UserAvatar $user
     *
     * @return bool
     * true  - браузер зарегистрирован, можно заходить
     * false - браузер не зарегистрирован, заходить нельзя
     */
    public static function checkDeviceAfterLogin($user)
    {
        $rows = UserDevice::find()->where(['user_id' => $user->id])->all();
        if (count($rows) == 0) {
            UserDevice::add([
                'user_id'   => $user->id,
                'name'      => \Yii::$app->request->getUserAgent(),
            ]);
            return true;
        }

        return UserDevice::find()->where([
            'user_id' => $user->id,
            'name'    => \Yii::$app->request->getUserAgent(),
        ])->exists();
    }


    public static function getSeeds()
    {
        /** @var int $count кол-во слов в ключе */
        $count = 12;
        $file = \Yii::getAlias('@common/data/ENRUS3.TXT');
        $data = file_get_contents($file);
        $words = explode("\n", $data);
        $return = [];
        for ($i = 0; $i < $count; $i++) {
            $r = rand(0, count($words));
            $return[] = $words[$r];
        }

        return join(' ', $return);
    }

    /**
     * @param array $data
     * @return array
     */
    public static function maskPasswordInArray($data)
    {
        if (isset($data['password'])) {
            $data['password'] = self::maskPassword($data['password']);
        }

        return $data;
    }

    public static function maskPassword($password)
    {
        return str_repeat('*', strlen($password));
    }
}