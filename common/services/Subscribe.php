<?php


namespace common\services;

use common\models\subscribe\SubscribeMail;
use cs\Application;
use cs\services\VarDumper;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;

class Subscribe
{

    /**
     * Генерирует hash для ссылки отписки
     *
     * @param string $email почта клиента
     * @param integer $type тип рассылки \app\services\Subscribe::TYPE_*
     *
     * @return string
     *
     */
    public static function hashGenerate($email, $type)
    {
        return md5($email . '_' . $type);
    }

    /**
     * Проверяет hash для ссылки отписки
     *
     * @param string $email почта клиента
     * @param integer $type имп рассылки \app\services\Subscribe::TYPE_*
     * @param integer $hash
     *
     * @return boolean
     * true - верный
     * false - не верный
     */
    public static function hashValidate($email, $type, $hash)
    {
        return md5($email . '_' . $type) == $hash;
    }


    /**
     * Добавляет в очередь пакет писем
     *
     * @param array | string $emailList пакет писем (массив) или роль (строка)
     * @param string $subject           тема письма
     * @param string $view              представление которое лежит относительно '@avatar/mail/html'
     * @param array $options            настройки для представления
     * @param array $layout             default 'layouts/html'
     *                                  <p><code>$layout</code> если это строка то то путь к обложке (layout) письма от
     *                                  пути '@avatar/mail' в конце по умолчанию добавляется '.php'. В итоге если
     *                                  <code>$layout</code> = <code>layouts/html</code> то это значит файлом обложки
     *                                  является <code>@avatar/mail/layouts/html.php</code>.</p>
     *                                  <p>если это массив то следующие параметры:</p>
     *
     * <p><code>layout</code> то же что и этот параметр в виде строки.</p>
     * <p><code>from_name</code> от кого имя</p>
     * <p><code>from_email</code> от кого почта</p>
     *  - files - массив - не обязательное, файлы
     * [
     * [
     *  'name' => ''
     *  'content' => ''
     * ]
     * ]
     * - namespace - string - заданный namespace в рамках которого будет искаться mailerConfig.
     *
     * @return \common\models\subscribe\Subscribe
     */
    public static function sendArray($emailList, $subject, $view, $options = [], $layout = null)
    {
        // установка значений для функции
        {
            $from = \Yii::$app->params['mailer']['from'];
            $from_name = '';
            $from_email = '';
            foreach ($from as $m => $n) {
                $from_name = $n;
                $from_email = $m;
            }
            if (is_null($layout)) {
                $options2 = [
                    'layout'     => 'layouts/html',
                    'from_name'  => $from_name,
                    'from_email' => $from_email,
                ];
            } else {
                if (!is_array($layout)) {
                    $options2 = [
                        'layout'     => $layout,
                        'from_name'  => $from_name,
                        'from_email' => $from_email,
                    ];
                } else {
                    $options2 = $layout;

                    if (!isset($layout['layout'])) $options2['layout'] = 'layouts/html';
                    if (!isset($layout['from_name']) and !isset($layout['from_email'])) {
                        if (isset($options2['namespace'])) {
                            $prefix = $options2['namespace'];
                            $c = require (\Yii::getAlias('@common/config/params.php'));
                            $v = require (\Yii::getAlias('@'. $prefix . '/config/params.php'));
                            $params = ArrayHelper::merge($c, $v);

                            $from = $params['mailer']['from'];
                            if (is_array($from)) {
                                $from_name = '';
                                $from_email = '';
                                foreach ($from as $m => $n) {
                                    $from_name = $n;
                                    $from_email = $m;
                                }
                            } else {
                                $from_name = '';
                                $from_email = $from;
                            }
                        }
                    }
                    $options2['from_name'] = $from_name;
                    $options2['from_email'] = $from_email;
                }
            }
        }

        if (isset($options2['namespace'])) {
            $prefix = $options2['namespace'];
        } else {
            // Получаю конфиг mailer
            $name = \Yii::$app->controllerNamespace;
            $arr = explode('\\', $name);
            $prefix = 'frontend';
            if (in_array($arr[0], ['avatar', 'frontend'])) {
                $prefix = 'frontend';
            }
            if (in_array($arr[0], ['shop'])) {
                $prefix = 'shop';
            }
            if (in_array($arr[0], ['lot'])) {
                $prefix = 'lot';
            }
        }
        $c = require (\Yii::getAlias('@common/config/main.php'));
        $v = require (\Yii::getAlias('@'. $prefix . '/config/main.php'));
        $s = ArrayHelper::merge($c, $v);
        $mailerConfig = $s['components']['mailer'];

        if (!is_array($emailList)) {
            $role = $emailList;
            $emailList = [];
            $ids = \Yii::$app->authManager->getUserIdsByRole($role);
            foreach ($ids as $id) {
                try {
                    $user = \common\models\UserAvatar::findOne($id);
                    $emailList[] = $user->email;
                } catch (\Exception $e) {
                    \Yii::warning('user id not found ' . $id, 'avatar\common\services\Subscribe::sendArray');
                }
            }
        }

        \Yii::info($prefix, 'avatar\sendArray');

        /** @var \yii\swiftmailer\Mailer $mailer */
        $mailer = \Yii::$app->mailer;
        \Yii::info('@'. $prefix . '/mail/html/' . $view, 'avatar\sendArray2');
        $html = $mailer->render('@'. $prefix . '/mail/html/' . $view, $options, $options2['layout']);

        $subscribe = \common\models\subscribe\Subscribe::add([
            'html'       => $html,
            'subject'    => $subject,
            'from_name'  => $options2['from_name'],
            'from_email' => $options2['from_email'],
            'mailer'     => Json::encode($mailerConfig),
        ]);

        foreach ($emailList as $mail) {
            SubscribeMail::add([
                'subscribe_id' => $subscribe->id,
                'mail'         => $mail,
            ]);
        }

        return $subscribe;
    }

}