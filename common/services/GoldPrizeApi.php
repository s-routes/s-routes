<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 04.02.2017
 * Time: 0:33
 */

namespace common\services;


use common\models\statistic\PingStatus;
use common\models\UserDevice;
use cs\Application;
use cs\services\File;
use cs\services\VarDumper;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

class GoldPrizeApi extends Component
{
    public $url = 'http://goldpricez.com/api';

    /** @var  string ключ */
    public $key;

    public function init()
    {
        if (Application::isEmpty($this->url)) $this->url = 'http://goldpricez.com/api';
    }

    /**
     * Отправляет команду $path на сервер с параметрами $params
     *
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function _post($path, $params = [])
    {
        $provider = new Client(['baseUrl' => $this->url]);
        $request = $provider->post($path, $params);
        $request = $request->addHeaders(['X-API-KEY' => $this->key]);
        $response = $request->send();

        return $response;
    }

    /**
     * Отправляет команду $path на сервер с параметрами $params
     *
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function post($path, $params = [])
    {
        $response = $this->_post($path, $params);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\\GoldPrizeApi\\response');
        if ($response->statusCode != 200) {
            throw new \Exception('server return status code = ' . $response->statusCode);
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('server return not JSON');
        }

        return $data;
    }

    /**
     * Отправляет команду $path на сервер с параметрами $params
     *
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function _get($path, $params = [])
    {
        $headers = [
            'token' => $this->key,
        ];
        $params = ArrayHelper::merge($params, $headers);

        $provider = new Client(['baseUrl' => $this->url]);
        $request = $provider->get($path, $params);
        $request = $request->addHeaders(['X-API-KEY' => $this->key]);
        $response = $request->send();

        return $response;
    }

    /**
     * Отправляет команду $path на сервер с параметрами $params
     *
     * @param $path
     * @param array $params
     *
     * @return \yii\httpclient\Response
     * @throws
     */
    public function get($path, $params = [])
    {
        $response = $this->_get($path, $params);
        \Yii::info(\yii\helpers\VarDumper::dumpAsString($response), 'avatar\\GoldPrizeApi\\response');

        if ($response->statusCode == 200) {
            PingStatus::add(5, 1);
        } else {
            PingStatus::add(5, 0);
        }

        if ($response->statusCode != 200) {
            throw new \Exception('server return status code = ' . $response->statusCode);
        }
        try {
            $data = Json::decode($response->content);
        } catch (\Exception $e) {
            throw new \Exception('server return not JSON');
        }

        return $data;
    }

}