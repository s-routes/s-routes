<?php

namespace common\base;

use yii\base\BaseObject;

class Shop extends BaseObject
{
    /**
     * Выдает ответ "нужно ли прибавлять к заказу 300 руб. для оплаты работы менеджера"
     * @return bool
     */
    public static function isPaidManager()
    {
        $data = \Yii::$app->session->get(\shop\models\forms\Request::$keySession);

        return in_array($data['user']['type_id'], [
            \shop\models\forms\Request2Login::TYPE_ID_CABINET_DATA,
            \shop\models\forms\Request2Login::TYPE_ID_USER_DATA
        ]) || in_array($data['delivery']['type_id'], [
            \shop\models\forms\Request3Login::TYPE_ID_CABINET_DATA,
            \shop\models\forms\Request3Login::TYPE_ID_USER_DATA
        ]);
    }
}