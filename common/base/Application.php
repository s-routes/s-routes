<?php

namespace common\base;

use common\components\providers\ETH;
use common\models\Config;
use common\models\UserAvatar;
use cs\services\SitePath;
use cs\services\VarDumper;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use common\models\avatar\Currency;

class Application extends \yii\web\Application
{
    /**
     * @var array список кошельков которые показываются у пользователя в первом списке NEIRON, тип db.currency.id
     */
    public static $walletListFirst = [
        Currency::PZM,
        Currency::ETH,
        Currency::USDT,

        Currency::BTC,
        Currency::LTC,
        Currency::BNB,
        Currency::DASH,
        Currency::NEIRO,
        Currency::TRX,
        Currency::DAI,
        Currency::DOGE,

    ];

    /**
     * @var array список кошельков которые показываются у пользователя во втором списке NEIRON, тип db.currency.id
     */
    public static $walletListSecond = [
        Currency::MARKET,
        Currency::LOT,
    ];

    /**
     * Список валют которые выводятся автоматом с бинанса db.currency.id
     * @var array
     */
    public static $binanceAutoOutput = [
        Currency::BTC,
        Currency::LTC,
        Currency::DASH,
        Currency::ETH,
        Currency::TRX,
        Currency::USDT,
        Currency::DAI,
        Currency::DOGE,
    ];

    /**
     * @param array $params
     *                     [
     *                     'code'                      => 'DOGE',
     *                     'title'                     => 'DOGE',
     *                     'decimals'                  => 8,
     *                     'decimals_view'             => 8,
     *                     'master_wallet'             => '',
     *                     'master_wadllet_is_binance' => 1,
     *                     'function_check'            => '',
     *                     'validate_account'          => '',
     *                     'field_name_wallet'         => 'doge',
     *                     'warning'                   => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме DOGE - приведет к их потере.',
     *                     'cio'                   => [
     *                          'extended_info' => ''
     * ]
     *                     ];
     *
     * @throws \Exception
     */
    public function addCoin($params)
    {
        $o = $params;

        $cInt = \common\models\piramida\Currency::add([
            'name'               => $o['title'],
            'code'               => $o['code'],
            'decimals'           => $o['decimals'],
            'decimals_view'      => $o['decimals_view'],
        ]);
        $cExt = \common\models\avatar\Currency::findOne(['code' => $o['code']]);
        $wMain = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
        $wBlock = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
        $wLost = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
        $p = \common\models\exchange\Page::add(['name' => $o['code']]);
        $paramsCIO = [
            'currency_ext_id'          => $cExt->id,
            'currency_int_id'          => $cInt->id,
            'main_wallet'              => $wMain->id,
            'block_wallet'             => $wBlock->id,
            'lost_wallet'              => $wLost->id,
            'is_exchange'              => 1,
            'is_io'                    => 1,
            'info_page_id'             => $p->id,
            'info_page_shop_id'        => $p->id,
            'master_wallet_is_binance' => $o['master_wallet_is_binance'],
            'function_check'           => $o['function_check'],
            'validate_account'         => $o['validate_account'],
            'field_name_wallet'        => $o['field_name_wallet'],
        ];
        $paramsCIO = ArrayHelper::merge($paramsCIO, ArrayHelper::getColumn($params, 'cio', []));
        $CIO = \common\models\CurrencyIO::add($paramsCIO);
        $this->addColumn('user_wallet', $o['field_name_wallet'], 'varchar(100)');

// Добавляю кошельки
        /**
         * @var \common\models\UserAvatar $u
         */
        foreach (\common\models\UserAvatar::find()->all() as $u) {
            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cInt->id, $u->id);
            echo 'uid='.$u->id.' bid=' . $data['billing']['id'] . "\n";
        }

// Устанавливаю sort_index
        $c = \common\models\avatar\Currency::findOne($cExt->id);
        $s = \common\models\avatar\Currency::find()->select('max(sort_index)')->scalar();
        $c->sort_index = $s + 1;
        $c->save();
    }

    /** @var  \common\components\sms\IqSms */
    public $sms;


    public function sendTelegramAdminSms($text)
    {
        // Уведомляю админа и бухгалтера по телеграму
        $userListAdmin = Yii::$app->authManager->getUserIdsByRole('role_admin');
        $userList = $userListAdmin;
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!\cs\Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                try {
                    $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
                } catch (\Exception $e) {

                }
            }
        }

    }

}