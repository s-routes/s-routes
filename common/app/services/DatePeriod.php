<?php


namespace cs\services;

use DateTime;
use cs\models\Response;
use yii\helpers\VarDumper;
use Yii;

class DatePeriod
{
    private static $messageBig  = 'более пяти лет назад';
    private static $messageBig2 = 'более пяти лет';

    /**
     * @return array
     */
    protected static function getPeriods()
    {
        return [
            ['min' => 0, 'max' => 10, Yii::t('c.JaENrUcXB6', 'только что'), Yii::t('c.JaENrUcXB6', 'только что'),],
            ['min' => 10, 'max' => 20, '10 '.Yii::t('c.JaENrUcXB6', 'секунд назад'), '10 '.Yii::t('c.JaENrUcXB6', 'сек')],
            ['min' => 20, 'max' => 30, '20 '.Yii::t('c.JaENrUcXB6', 'секунд назад'), '20 '.Yii::t('c.JaENrUcXB6', 'сек')],
            ['min' => 30, 'max' => 40, '30 '.Yii::t('c.JaENrUcXB6', 'секунд назад'), '30 '.Yii::t('c.JaENrUcXB6', 'сек')],
            ['min' => 40, 'max' => 50, '40 '.Yii::t('c.JaENrUcXB6', 'секунд назад'), '40 '.Yii::t('c.JaENrUcXB6', 'сек')],
            ['min' => 50, 'max' => 60, '50 '.Yii::t('c.JaENrUcXB6', 'секунд назад'), '50 '.Yii::t('c.JaENrUcXB6', 'сек')],
            ['min' => 60, 'max' => 60 * 2, '1 '.Yii::t('c.JaENrUcXB6', 'минуту назад'), '1 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 2, 'max' => 60 * 3, '2 '.Yii::t('c.JaENrUcXB6', 'минуты назад'), '2 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 3, 'max' => 60 * 4, '3 '.Yii::t('c.JaENrUcXB6', 'минуты назад'), '3 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 4, 'max' => 60 * 5, '4 '.Yii::t('c.JaENrUcXB6', 'минуты назад'), '4 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 5, 'max' => 60 * 6, '5 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '5 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 6, 'max' => 60 * 7, '6 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '6 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 7, 'max' => 60 * 8, '7 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '7 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 8, 'max' => 60 * 9, '8 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '8 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 9, 'max' => 60 * 10, '9 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '9 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 10, 'max' => 60 * 20, '10 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '10 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 20, 'max' => 60 * 30, '20 '.Yii::t('c.JaENrUcXB6', 'минут назад'), '20 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 30, 'max' => 60 * 60, Yii::t('c.JaENrUcXB6', 'пол часа назад'), '30 '.Yii::t('c.JaENrUcXB6', 'мин')],
            ['min' => 60 * 60, 'max' => 60 * 60 * 2, '1 '.Yii::t('c.JaENrUcXB6', 'час назад'), '1 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 2, 'max' => 60 * 60 * 3, '2 '.Yii::t('c.JaENrUcXB6', 'часа назад'), '2 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 3, 'max' => 60 * 60 * 4, '3 '.Yii::t('c.JaENrUcXB6', 'часа назад'), '3 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 4, 'max' => 60 * 60 * 5, '4 '.Yii::t('c.JaENrUcXB6', 'часа назад'), '4 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 5, 'max' => 60 * 60 * 6, '5 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '5 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 6, 'max' => 60 * 60 * 7, '6 '.Yii::t('c.JaENrUcXB6', 'часа назад'), '6 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 7, 'max' => 60 * 60 * 8, '7 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '7 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 8, 'max' => 60 * 60 * 9, '8 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '8 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 9, 'max' => 60 * 60 * 10, '9 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '9 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 10, 'max' => 60 * 60 * 11, '10 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '10 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 11, 'max' => 60 * 60 * 12, '11 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '11 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 12, 'max' => 60 * 60 * 13, '12 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '12 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 13, 'max' => 60 * 60 * 14, '13 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '13 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 14, 'max' => 60 * 60 * 15, '14 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '14 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 15, 'max' => 60 * 60 * 16, '15 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '15 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 16, 'max' => 60 * 60 * 17, '16 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '16 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 17, 'max' => 60 * 60 * 18, '17 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '17 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 18, 'max' => 60 * 60 * 19, '18 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '18 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 19, 'max' => 60 * 60 * 20, '19 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '19 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 20, 'max' => 60 * 60 * 21, '20 '.Yii::t('c.JaENrUcXB6', 'часов назад'), '20 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 21, 'max' => 60 * 60 * 22, '21 '.Yii::t('c.JaENrUcXB6', 'час назад'), '21 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 22, 'max' => 60 * 60 * 23, '22 '.Yii::t('c.JaENrUcXB6', 'часа назад'), '22 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 23, 'max' => 60 * 60 * 24, '23 '.Yii::t('c.JaENrUcXB6', 'часа назад'), '23 '.Yii::t('c.JaENrUcXB6', 'ч')],
            ['min' => 60 * 60 * 24, 'max' => 60 * 60 * 24 * 2, '1 '.Yii::t('c.JaENrUcXB6', 'день назад'), '1 '.Yii::t('c.JaENrUcXB6', 'д')],
            ['min' => 60 * 60 * 24 * 2, 'max' => 60 * 60 * 24 * 3, '2 '.Yii::t('c.JaENrUcXB6', 'дня назад'), '2 '.Yii::t('c.JaENrUcXB6', 'д')],
            ['min' => 60 * 60 * 24 * 3, 'max' => 60 * 60 * 24 * 4, '3 '.Yii::t('c.JaENrUcXB6', 'дня назад'), '3 '.Yii::t('c.JaENrUcXB6', 'д')],
            ['min' => 60 * 60 * 24 * 4, 'max' => 60 * 60 * 24 * 5, '4 '.Yii::t('c.JaENrUcXB6', 'дня назад'), '4 '.Yii::t('c.JaENrUcXB6', 'д')],
            ['min' => 60 * 60 * 24 * 5, 'max' => 60 * 60 * 24 * 6, '5 '.Yii::t('c.JaENrUcXB6', 'дней назад'), '5 '.Yii::t('c.JaENrUcXB6', 'д')],
            ['min' => 60 * 60 * 24 * 6, 'max' => 60 * 60 * 24 * 7, '6 '.Yii::t('c.JaENrUcXB6', 'дней назад'), '6 '.Yii::t('c.JaENrUcXB6', 'д')],
            ['min' => 60 * 60 * 24 * 7, 'max' => 60 * 60 * 24 * 7 * 2, '1 '.Yii::t('c.JaENrUcXB6', 'неделю назад'), '1 '.Yii::t('c.JaENrUcXB6', 'нед')],
            ['min' => 60 * 60 * 24 * 7 * 2, 'max' => 60 * 60 * 24 * 7 * 3, '2 '.Yii::t('c.JaENrUcXB6', 'недели назад'), '2 '.Yii::t('c.JaENrUcXB6', 'нед')],
            ['min' => 60 * 60 * 24 * 7 * 3, 'max' => 60 * 60 * 24 * 7 * 4, '3 '.Yii::t('c.JaENrUcXB6', 'недели назад'), '3 '.Yii::t('c.JaENrUcXB6', 'нед')],
            ['min' => 60 * 60 * 24 * 7 * 4, 'max' => 60 * 60 * 24 * 7 * 4 * 2, '1 '.Yii::t('c.JaENrUcXB6', 'месяц назад'), '1 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 7 * 4 * 2, 'max' => 60 * 60 * 24 * 30 * 3, '2 '.Yii::t('c.JaENrUcXB6', 'месяца назад'), '2 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 3, 'max' => 60 * 60 * 24 * 30 * 4, '3 '.Yii::t('c.JaENrUcXB6', 'месяца назад'), '3 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 4, 'max' => 60 * 60 * 24 * 30 * 5, '4 '.Yii::t('c.JaENrUcXB6', 'месяца назад'), '4 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 5, 'max' => 60 * 60 * 24 * 30 * 6, '5 '.Yii::t('c.JaENrUcXB6', 'месяцев назад'), '5 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 6, 'max' => 60 * 60 * 24 * 30 * 7, '6 '.Yii::t('c.JaENrUcXB6', 'месяца назад'), '6 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 7, 'max' => 60 * 60 * 24 * 30 * 8, '7 '.Yii::t('c.JaENrUcXB6', 'месяцев назад'), '7 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 8, 'max' => 60 * 60 * 24 * 30 * 9, '8 '.Yii::t('c.JaENrUcXB6', 'месяцев назад'), '8 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 9, 'max' => 60 * 60 * 24 * 30 * 10, '9 '.Yii::t('c.JaENrUcXB6', 'месяцев назад'), '9 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 10, 'max' => 60 * 60 * 24 * 30 * 11, '10 '.Yii::t('c.JaENrUcXB6', 'месяцев назад'), '10 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 11, 'max' => 60 * 60 * 24 * 30 * 12, '11 '.Yii::t('c.JaENrUcXB6', 'месяцев назад'), '11 '.Yii::t('c.JaENrUcXB6', 'мес')],
            ['min' => 60 * 60 * 24 * 30 * 12, 'max' => 60 * 60 * 24 * 30 * 12 * 2, '1 '.Yii::t('c.JaENrUcXB6', 'год назад'), '1 '.Yii::t('c.JaENrUcXB6', 'год')],
            ['min' => 60 * 60 * 24 * 30 * 12 * 2, 'max' => 60 * 60 * 24 * 30 * 12 * 3, '2 '.Yii::t('c.JaENrUcXB6', 'года назад'), '2 '.Yii::t('c.JaENrUcXB6', 'года')],
            ['min' => 60 * 60 * 24 * 30 * 12 * 3, 'max' => 60 * 60 * 24 * 30 * 12 * 4, '3 '.Yii::t('c.JaENrUcXB6', 'года назад'), '3 '.Yii::t('c.JaENrUcXB6', 'года')],
            ['min' => 60 * 60 * 24 * 30 * 12 * 4, 'max' => 60 * 60 * 24 * 30 * 12 * 5, '4 '.Yii::t('c.JaENrUcXB6', 'года назад'), '4 '.Yii::t('c.JaENrUcXB6', 'года')],
        ];
    }

    /**
     * Сколько еще осталось?
     *
     * @return array
     */
    protected static function getPeriods2()
    {
        return [
            ['min' => 0, 'max' => 60, 'меньше минуты'],
            ['min' => 60, 'max' => 60 * 2, '1 минута'],
            ['min' => 60 * 2, 'max' => 60 * 3, '2 минуты'],
            ['min' => 60 * 3, 'max' => 60 * 4, '3 минуты'],
            ['min' => 60 * 4, 'max' => 60 * 5, '4 минуты'],
            ['min' => 60 * 5, 'max' => 60 * 6, '5 минут'],
            ['min' => 60 * 6, 'max' => 60 * 7, '6 минут'],
            ['min' => 60 * 7, 'max' => 60 * 8, '7 минут'],
            ['min' => 60 * 8, 'max' => 60 * 9, '8 минут'],
            ['min' => 60 * 9, 'max' => 60 * 10, '9 минут'],
            ['min' => 60 * 10, 'max' => 60 * 20, '10 минут'],
            ['min' => 60 * 20, 'max' => 60 * 30, '20 минут'],
            ['min' => 60 * 30, 'max' => 60 * 60, '30 минут'],
            ['min' => 60 * 60, 'max' => 60 * 60 * 2, '1 час'],
            ['min' => 60 * 60 * 2, 'max' => 60 * 60 * 3, '2 часа'],
            ['min' => 60 * 60 * 3, 'max' => 60 * 60 * 4, '3 часа'],
            ['min' => 60 * 60 * 4, 'max' => 60 * 60 * 5, '4 часа'],
            ['min' => 60 * 60 * 5, 'max' => 60 * 60 * 6, '5 часов'],
            ['min' => 60 * 60 * 6, 'max' => 60 * 60 * 7, '6 часа'],
            ['min' => 60 * 60 * 7, 'max' => 60 * 60 * 8, '7 часов'],
            ['min' => 60 * 60 * 8, 'max' => 60 * 60 * 9, '8 часов'],
            ['min' => 60 * 60 * 9, 'max' => 60 * 60 * 10, '9 часов'],
            ['min' => 60 * 60 * 10, 'max' => 60 * 60 * 11, '10 часов'],
            ['min' => 60 * 60 * 11, 'max' => 60 * 60 * 12, '11 часов'],
            ['min' => 60 * 60 * 12, 'max' => 60 * 60 * 13, '12 часов'],
            ['min' => 60 * 60 * 13, 'max' => 60 * 60 * 14, '13 часов'],
            ['min' => 60 * 60 * 14, 'max' => 60 * 60 * 15, '14 часов'],
            ['min' => 60 * 60 * 15, 'max' => 60 * 60 * 16, '15 часов'],
            ['min' => 60 * 60 * 16, 'max' => 60 * 60 * 17, '16 часов'],
            ['min' => 60 * 60 * 17, 'max' => 60 * 60 * 18, '17 часов'],
            ['min' => 60 * 60 * 18, 'max' => 60 * 60 * 19, '18 часов'],
            ['min' => 60 * 60 * 19, 'max' => 60 * 60 * 20, '19 часов'],
            ['min' => 60 * 60 * 20, 'max' => 60 * 60 * 21, '20 часов'],
            ['min' => 60 * 60 * 21, 'max' => 60 * 60 * 22, '21 час'],
            ['min' => 60 * 60 * 22, 'max' => 60 * 60 * 23, '22 часа'],
            ['min' => 60 * 60 * 23, 'max' => 60 * 60 * 24, '23 часа'],
            ['min' => 60 * 60 * 24, 'max' => 60 * 60 * 24 * 2, '1 день'],
            ['min' => 60 * 60 * 24 * 2, 'max' => 60 * 60 * 24 * 3, '2 дня'],
            ['min' => 60 * 60 * 24 * 3, 'max' => 60 * 60 * 24 * 4, '3 дня'],
            ['min' => 60 * 60 * 24 * 4, 'max' => 60 * 60 * 24 * 5, '4 дня'],
            ['min' => 60 * 60 * 24 * 5, 'max' => 60 * 60 * 24 * 6, '5 дней'],
            ['min' => 60 * 60 * 24 * 6, 'max' => 60 * 60 * 24 * 7, '6 дней'],
            ['min' => 60 * 60 * 24 * 7, 'max' => 60 * 60 * 24 * 7 * 2, '1 неделю'],
            ['min' => 60 * 60 * 24 * 7 * 2, 'max' => 60 * 60 * 24 * 7 * 3, '2 недели'],
            ['min' => 60 * 60 * 24 * 7 * 3, 'max' => 60 * 60 * 24 * 7 * 4, '3 недели'],
            ['min' => 60 * 60 * 24 * 7 * 4, 'max' => 60 * 60 * 24 * 7 * 4 * 2, '1 месяц'],
            ['min' => 60 * 60 * 24 * 7 * 4 * 2, 'max' => 60 * 60 * 24 * 30 * 3, '2 месяца'],
            ['min' => 60 * 60 * 24 * 30 * 3, 'max' => 60 * 60 * 24 * 30 * 4, '3 месяца'],
            ['min' => 60 * 60 * 24 * 30 * 4, 'max' => 60 * 60 * 24 * 30 * 5, '4 месяца'],
            ['min' => 60 * 60 * 24 * 30 * 5, 'max' => 60 * 60 * 24 * 30 * 6, '5 месяцев'],
            ['min' => 60 * 60 * 24 * 30 * 6, 'max' => 60 * 60 * 24 * 30 * 7, '6 месяца'],
            ['min' => 60 * 60 * 24 * 30 * 7, 'max' => 60 * 60 * 24 * 30 * 8, '7 месяцев'],
            ['min' => 60 * 60 * 24 * 30 * 8, 'max' => 60 * 60 * 24 * 30 * 9, '8 месяцев'],
            ['min' => 60 * 60 * 24 * 30 * 9, 'max' => 60 * 60 * 24 * 30 * 10, '9 месяцев'],
            ['min' => 60 * 60 * 24 * 30 * 10, 'max' => 60 * 60 * 24 * 30 * 11, '10 месяцев'],
            ['min' => 60 * 60 * 24 * 30 * 11, 'max' => 60 * 60 * 24 * 30 * 12, '11 месяцев'],
            ['min' => 60 * 60 * 24 * 30 * 12, 'max' => 60 * 60 * 24 * 30 * 12 * 2, '1 год'],
            ['min' => 60 * 60 * 24 * 30 * 12 * 2, 'max' => 60 * 60 * 24 * 30 * 12 * 3, '2 года'],
            ['min' => 60 * 60 * 24 * 30 * 12 * 3, 'max' => 60 * 60 * 24 * 30 * 12 * 4, '3 года'],
            ['min' => 60 * 60 * 24 * 30 * 12 * 4, 'max' => 60 * 60 * 24 * 30 * 12 * 5, '4 года'],
        ];
    }

    /**
     * Преобразует переданный промежуток в секкунда в строку
     *
     * @param int $diff время в секундах
     * @param array $options
     *
     * @return string
     */
    private static function backConvert($diff, $options)
    {
        $periods = self::getPeriods();
        foreach ($periods as $period) {
            if ($period['min'] <= $diff && $period['max'] > $diff) return ($options['isShort'])? $period[1] : $period[0];
        }

        return self::$messageBig;
    }

    /**
     *
     * Преобразует переданный промежуток в секкунда в строку
     *
     * @param int $diff время в секундах
     *
     * @return string
     */
    private static function forwardConvert($diff, $options)
    {
        $periods = self::getPeriods2();
        foreach ($periods as $period) {
            if ($period['min'] <= $diff && $period['max'] > $diff) return $period[0];
        }

        return self::$messageBig2;
    }

    /**
     * Возвращает строку отвечающий на вопрос: сколько осталось ждать до $date ?
     *
     * @param string|int|DateTime $date               UTC
     * @param mixed               $messageLessThenNow сообщение возвращаемое когда переданное время меньше настоящего
     *                                                момента
     *
     * @return string
     */
    public static function forward($date, $messageLessThenNow = [
        'messageGregerThenNow' => 'просрочено',
        'isShort'              => false,
    ])
    {
        $ret = self::convertDate($date);
        if (!$ret->status) return $ret->error;
        $value = $ret->data;

        return self::forwardInt($value, $messageLessThenNow);
    }

    /**
     * Возвращает строку отвечающий на вопрос: сколько осталось ждать до $date ?
     *
     * @param int   $date               UTC
     * @param mixed $messageLessThenNow сообщение возвращаемое когда переданное время меньше настоящего момента
     *
     * @return string
     */
    public static function forwardInt($date, $messageLessThenNow)
    {
        $end = $date;
        $now = gmdate('U');
//        if ($end < $now) return $messageLessThenNow['messageGregerThenNow'];
        if ($end < $now) return 'просрочено';
        $diff = $end - $now;

        return self::forwardConvert($diff, $messageLessThenNow);
    }

    /**
     * @param string|int|DateTime $date UTC
     *
     * @return \cs\models\Response
     * integer - unix time
     */
    private static function convertDate($date)
    {
        if (filter_var($date, FILTER_VALIDATE_INT)) {
            return Response::success($date);
        } else if (is_double($date)) {
            return Response::success((int)$date);
        } else if ($date instanceof DateTime) {
            return Response::success($date->format('U'));
        } else if (is_string($date)) {
            return Response::success((new \DateTime($date, new \DateTimeZone('UTC')))->format('U'));
        } else {
            return Response::error('Не верный формат данных');
        }
    }

    /**
     * Возвращает строку отвечающий на вопрос: какой промежуток прошел после $date ?
     *
     * @param string|int|DateTime $date                 UTC
     * @param mixed               $options         опции
     * - messageGregerThenNow - сообщение возвращаемое когда переданное время больше настоящего
     * - isShort - bool - короткий формат? true - короткий, false - длинный (по умолчанию)
     *
     * @return string
     */
    public static function back($date, $options = [
                'messageGregerThenNow' => 'больше настоящего',
                'isShort'              => false,
            ])
    {
        $ret = self::convertDate($date);
        if (!$ret->status) return $ret->error;
        $value = $ret->data;

        return self::backInt($value, $options);

    }

    /**
     * Возвращает строку отвечающий на вопрос: какой промежуток прошел после $date ?
     *
     * @param int   $date                 UTC
     * @param mixed $messageGregerThenNow сообщение возвращаемое когда переданное время больше настоящего момента
     *
     * @return string
     */
    public static function backInt($date, $options)
    {
        $start = $date;
        $now = time();

        if ($start > $now) {
            if (!isset($options['messageGregerThenNow'])) {
                return 'Более ...';
            }

            return $options['messageGregerThenNow'];
        }
        $diff = $now - $start;
        return self::backConvert($diff, $options);
    }

    /**
     * Возвращает строку отвечающий на вопрос: какой промежуток прошел после $date ?
     *
     * @param string|int|DateTime $date                 UTC
     * @param mixed               $messageGregerThenNow сообщение возвращаемое когда переданное время больше настоящего
     *                                                  момента
     *
     * @return string
     */
    public static function diff($date)
    {
        if (is_null($date)) return '';
        $ret = self::convertDate($date);
        if (!$ret->status) return $ret->error;
        return self::diffInt($ret->data);
    }

    /**
     * Возвращает строку отвечающий на вопрос: какой промежуток прошел после $date ?
     *
     * @param int   $date                               UTC
     * @param mixed $messageGregerThenNow               сообщение возвращаемое когда переданное время больше настоящего
     *                                                  момента
     *
     * @return string
     */
    public static function diffInt($date)
    {
        $now = gmdate('U');
        if ($now > $date) return self::backInt($date);

        return self::forwardInt($date);
    }
}