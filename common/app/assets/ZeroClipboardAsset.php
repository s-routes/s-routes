<?php

namespace app\assets;

use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;
use yii\web\View;
use yii\helpers\Url;

class ZeroClipboardAsset extends AssetBundle
{
    public function init()
    {
        $this->baseUrl = \Yii::getAlias('@web').'/js/zeroclipboard-2.1.6/dist/';
        $this->js = ArrayHelper::merge($this->js, [YII_DEBUG ? 'ZeroClipboard.js' : 'ZeroClipboard.min.js']);

        parent::init();
    }
}