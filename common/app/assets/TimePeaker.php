<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace cs\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class TimePeaker extends AssetBundle
{
    public $sourcePath = '@vendor/jdewit/bootstrap-timepicker';
    public $css      = [
    ];
    public $js       = [
        'js/bootstrap-timepicker.js',
    ];
    public $depends  = [
        'yii\web\JqueryAsset',
    ];
}
