<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace cs\Widget\Google\reCaptcha;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
    public function init()
    {
        parent::init();
        \Yii::$app->view->registerJsFile('https://www.google.com/recaptcha/api.js?hl=' . \Yii::$app->language, ['position' => \yii\web\View::POS_HEAD]);
    }
}
