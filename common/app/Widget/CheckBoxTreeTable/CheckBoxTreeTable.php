<?php

namespace cs\Widget\CheckBoxTreeTable;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use yii\debug\models\search\Debug;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\JsExpression;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\services\BitMask;
use yii\db\Query;

/**
 * Виджет для вывода списка chackbox'ов по шаблону
 * Данные сохраняет в БД в виде маски
 *
 * - tableName - string - таблица
 * - rootId - int - идентификатор корневой ветки
 *
 * - templateFile - string - шаблон для виджета
 * - templateVariables - array - параметры передаваемые в шаблон
 */
class CheckBoxTreeTable extends InputWidget
{
    public $templateFile = '@csRoot/Widget/CheckBoxTreeTable/template';
    public $templateVariables = [];

    private $attrId;
    private $attrName;

    public $tableName;
    public $rootId;

    public $tableLink;
    public $fieldTree;
    public $fieldProduct;

    /**
     * @var array, string
     *           поля для выборки, нужно вернуть поля id и name
     */
    public $select = 'id, name';

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->attrId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->attrName = $this->model->formName() . '[' . $this->attribute . ']';
    }

    /**
     * Рисует виджет
     */
    public function run()
    {
        $this->registerClientScript();
        $rows = $this->getRows($this->rootId);
        $this->setAttributeSelected($rows);

        return $this->render($this->templateFile, [
            'rows'              => $rows,
            'tableName'         => $this->tableName,
            'formName'          => $this->model->formName(),
            'model'             => $this->model,
            'attrId'            => $this->attrId,
            'attrName'          => $this->attrName,
            'templateVariables' => $this->templateVariables,
        ]);
    }

    /**
     * Выставляет аттрибут selected в массиве $this->rows согласно $this->model->$attr
     */
    public function setAttributeSelected(&$rows)
    {
        $attr = $this->attribute;
        if (is_array($this->model->$attr)) {
            if (count($this->model->$attr) > 0) {
                for ($i = 0; $i < count($rows); $i++) {
                    $item = &$rows[ $i ];
                    $item['selected'] = in_array($item['id'], $this->model->$attr);
                    if (isset($item['nodes'])) {
                        $this->setAttributeSelected($item['nodes']);
                    }
                }
            }
        }
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        \cs\Widget\CheckBoxTreeMask\Asset::register(\Yii::$app->view);
    }

    /**
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

    /**
     * Возвращает элементы списка
     * @return array
     * [[
     *  'id' =>
     *  'name' =>
     *  'nodes' => array
     * ], ... ]
     */
    public function getRows($parentId)
    {
        $rows = (new Query())
            ->select($this->select)
            ->from($this->tableName)
            ->where(['parent_id' => $parentId])
            ->orderBy(['sort_index' => SORT_ASC])
            ->all();
        for ($i = 0; $i < count($rows); $i++) {
            $item = &$rows[ $i ];
            $rows2 = $this->getRows($item['id']);
            if (count($rows2) > 0) {
                $item['nodes'] = $rows2;
            }
        }

        return $rows;
    }

    /**
     * Берет значения из POST и возвраает знаяения для добавления в БД
     *
     * @param array $field
     * @param \yii\base\Model $model
     *
     * @return array
     */
    public static function onUpdate($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];
        $old = (new Query())->select([$field['widget'][1]['fieldTree']])->from($field['widget'][1]['tableLink'])->where([$field['widget'][1]['fieldProduct'] => $model->id])->column();
        $new = $model->$fieldName;
        $same = array_intersect($old, $new);
        if (!((count($same) == count($new)) and (count($new) == count($old)))) {
            $delete = [];
            foreach ($old as $i) {
                if (!in_array($i, $same)) $delete[] = $i;
            }
            if (count($delete) > 0) {
                (new Query())->createCommand()->delete($field['widget'][1]['tableLink'], $field['widget'][1]['fieldProduct'].' = :product_id and '.$field['widget'][1]['fieldTree'].' in (:tree_node_id_string)', [
                    ':product_id'          => $model->id,
                    ':tree_node_id_string' => join(',', $delete),
                ])->execute();
            }
            $insert = [];
            foreach ($new as $i) {
                if (!in_array($i, $same)) $insert[] = [$i, $model->id];
            }
            if (count($insert) > 0) {
                (new Query())->createCommand()->batchInsert($field['widget'][1]['tableLink'], [
                    $field['widget'][1]['fieldTree'],
                    $field['widget'][1]['fieldProduct']
                ], $insert)->execute();
            }
        }

        return [];
    }

    /**
     * Загружает данные из базы данных
     *
     * @param array $field
     * @param self $model
     *
     * @return array поля для обновления в БД
     */
    public static function onLoadDb($field, $model)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];
//        \cs\services\VarDumper::dump($field['widget'][1]);
        $model->$fieldName = (new Query())->select([$field['widget'][1]['fieldTree']])->from($field['widget'][1]['tableLink'])->where([$field['widget'][1]['fieldProduct'] => $model->id])->column();
    }
}
