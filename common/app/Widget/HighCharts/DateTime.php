<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 07.06.2016
 * Time: 14:57
 */

namespace common\widgets\highCharts;


use yii\base\BaseObject;

class DateTime extends BaseObject
{
    /**
     * @var array
     * [
     *      [
     *          'name' => 'Online',
     *          'data' => [[
     *              'time' => int | string ('yyyy-mm-dd hh:mm:ss'),
     *              'value' => '',
     *          ],...],
     *      ]
     * ]
     */
    public $rows;

    public function draw()
    {
        
    }
}