<?php
namespace common\assets;

use yii\web\AssetBundle;

class EthereumAddressIcon extends AssetBundle
{
    public $sourcePath = '@vendor/ethereum/blockies';

    public $js = [
        'blockies.min.js',
    ];

    public $css = [
    ];

    public $depends = [
    ];
}