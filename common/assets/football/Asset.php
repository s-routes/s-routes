<?php


namespace common\assets\football;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/football/src';

    public $css      = [
    ];

    public $js       = [
        'ivank.js',
    ];

    public $depends  = [
        'yii\web\JqueryAsset',
    ];
}
