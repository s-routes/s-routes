<?php
namespace common\assets;

use yii\web\AssetBundle;

class Select extends AssetBundle
{
    public $sourcePath = '@vendor/silviomoreto/bootstrap-select/js';

    public $js = [
        'bootstrap-select.js',
    ];

    public $css = [
    ];

    public $depends = [
    ];
}