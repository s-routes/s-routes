<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\assets\iLightBox;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/iLightBox/ilightbox';
    public $css = [
        'src/css/ilightbox.css'
    ];
    public $js = [
        'src/js/jquery.requestAnimationFrame.js',
        'src/js/jquery.mousewheel.js',
        'src/js/ilightbox.packed.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];

}
