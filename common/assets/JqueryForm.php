<?php
namespace common\assets;

use yii\web\AssetBundle;

class JqueryForm extends AssetBundle
{
    public $sourcePath = '@vendor/jquery-form/form/src';

    public $js = [
        'jquery.form.js',
    ];

    public $css = [
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}