<?php

namespace app\common\widgets\GoogleCaptcha;

use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\web\JsExpression;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\Widget\FileUploadMany\ModelFiles;
use cs\services\UploadFolderDispatcher;
use cs\services\SitePath;
use yii\jui\InputWidget;

/**
 * что сделать
 * - сделать в контроллер загрузки вывод ссылки на оригинал сохраненного изображения.
 * - сделать сохранение в поле
 * -
 *
 * Класс FileUploadMany
 *
 * Виджет который загружает файлы по несколько штук
 * Если загружаются картинки то они показываются в предпросмотре
 * и обрезаются по размеру указанному в параметре small
 *
 * Максимальный размер загружаемого файла по умолчанию устанавливается равный тому который указан в параметре ini.php upload_max_filesize
 *
 *
 *
 * $field->widget('cs\Widget\FileUploadMany2\FileUploadMany', [
 *
 * ]);
 *
 * $options = [
 *      'serverName'
 * ];
 *
 * $model->$fieldName = [
 *      ['file_path', 'file_name'],
 * ];
 */
class GoogleCaptcha extends InputWidget
{
    public $key;
    public $secretKey;

    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();
    }

    /**
     * рисует виджет
     */
    public function run()
    {
        $this->registerClientScript();

        if ($this->hasModel()) {
            $attribute = $this->attribute;

            $files = $this->model->$attribute;
            if (is_null($files)) $files = [];
            $this->clientOptions['files'] = $files;
            $c1 = Html::hiddenInput($this->hiddenName, json_encode($this->clientOptions['files'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), ['id' => $this->hiddenId]);
            $c2 = Html::fileInput($this->fieldName, null, ['id' => $this->fieldId]);

            return Html::tag('div', $c1 . $c2, ['class' => 'multifile_upload']);
        }
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->view);
        $this->clientOptions = ArrayHelper::merge($this->getClientOptions(), $this->clientOptions);
        $options = Json::encode($this->clientOptions);
        $js = <<<JSSSS
FileUploadMany2.init('#{$this->fieldId}', {$options});
JSSSS;
        $this->getView()->registerJs($js);
    }

    /**
     * Возвращает опции для виджета
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [
            'url'         => $this->uploadUrl,
            'maxFileSize' => self::getUploadMaxFileSize(),
            'formData' => [
                'table'       => $this->model->tableName(),
                'field'       => $this->attribute,
            ],
        ];
    }

    /**
     * Возвращает максимально возможно загружаемый файл который установлен в настройках PHP
     *
     * @return int в байтах
     */
    private static function getUploadMaxFileSize()
    {
        $maxFileSize = ini_get('upload_max_filesize');

        return (int)substr($maxFileSize, 0, strlen($maxFileSize) - 1) * 1024 * 1024;
    }

    /**
     * @param array $field
     *
     * @return array поля для обновления в БД
     */
    public function onLoad($field)
    {
        $model = $this->model;
        $fieldName = $this->attribute;
        $post = Yii::$app->request->post();
        $query = $model->formName() . '.' . $fieldName . '-files';
        $filesString = ArrayHelper::getValue($post, $query, '');
        $model->$fieldName = json_decode($filesString);
    }

    /**
     *
     *
     * @param array $field
     *
     * @return array поля для обновления в БД
     */
    public function onLoadDb($field)
    {
        $model = $this->model;
        $fieldName = $this->attribute;
        $file = $model->$fieldName;
        if (is_null($file)) {
            $model->$fieldName = [];
        } else {
            $f = explode('/', $file);

            $model->$fieldName = [[
                $file,
                $f[count($f) - 1],
            ]];
        }
    }

    /**
     * @param array $field
     */
    public function onUpdate($field)
    {
        $model = $this->model;
        $fieldName = $this->attribute;
        $files = $model->$fieldName;
        if (count($files) == 0) {
            $model->$fieldName = null;
            return;
        }

        $model->$fieldName = $files[0][0];
    }

    /**
     * Возвращает ссылку для скачивания файла
     *
     * @return string
     */
    public static function getDownloadLink($id)
    {
        return 'upload2/download/' . $id;
    }

    /**
     * Удаляет
     *
     * @param array $field
     */
    public function onDelete($field)
    {
        return;
    }

    /**
     * Рисует просмотр файла для детального просмотра
     *
     * @param \cs\base\BaseForm | \cs\base\DbRecord $model
     * @param array $field
     *
     * @return string
     */
    public static function onDraw($field, $model)
    {
        $html = [];
        $serverName = ArrayHelper::getValue($field, 'widget.1.options.serverName', '');
        $rows = self::getFiles($model->getTableName(), $field[\cs\base\BaseForm::POS_DB_NAME], $model->getId());
        foreach ($rows as $row) {
            $href = $row['file_path'];
            if ($serverName != '') $href = '//' . $serverName . $href;
            $html[] = Html::a($row['file_name'], $href, ['target' => '_blank']);
            $html[] = Html::tag('br');
        }

        return join("\r", $html);
    }

    /**
     * Выдает список файлов в поле
     *
     * @param string $tableName название таблицы
     * @param string $fieldName название поля
     * @param int $rowId идентификатор строки
     * @param string $serverName имя сервера для ссылок файлов, если задано то оно будет прибавлено к именам в формате "//{$serverName}/ss/..."
     *
     * @return array
     * [[
     *    'id'        => int
     *    'file_path' => str
     *    'file_name' => str
     *    'datetime'  => str
     * ], ...]
     */
    public static function getFiles($tableName, $fieldName, $rowId, $serverName = null)
    {
        $fieldId = self::getFieldId($tableName, $fieldName);
        if ($fieldId === false) return [];

        $select = [
            'id',
            'file_name',
            'UNIX_TIMESTAMP(datetime) as datetime'
        ];
        if (is_null($serverName)) {
            $select[] = 'file_path';
        } else {
            $select[] = "concat('//{$serverName}',`file_path`) as file_path";
        }

        return (new Query())->select($select)->from(self::$tableNameFiles)->where([
            'row_id'   => $rowId,
            'field_id' => $fieldId,
        ])->orderBy('datetime DESC')->all();
    }

    /**
     * Возвращает FieldId
     *
     * @param $tableName
     * @param $fieldName
     *
     * @return false|int
     */
    public static function getFieldId($tableName, $fieldName)
    {
        return (new Query())->select('id')->from(self::$tableNameFields)->where([
            'table_name' => $tableName,
            'field_name' => $fieldName,
        ])->scalar();
    }
}
