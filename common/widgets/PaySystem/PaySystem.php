<?php

namespace app\common\widgets\PaySystem;

use cs\services\Security;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Widget;
use yii\debug\models\search\Debug;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use cs\base\BaseForm;
use cs\services\UploadFolderDispatcher;
use cs\services\SitePath;
use yii\helpers\StringHelper;
use yii\jui\InputWidget;

/**
 * Класс PaySystem
 *
 *
 */
class PaySystem extends \yii\widgets\InputWidget
{
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $unionId;

    public $type = 'union';

    private $fieldId;
    private $fieldName;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->fieldId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->fieldName = $this->model->formName() . '[' . $this->attribute . ']';
    }

    /**
     * рисует виджет
     */
    public function run()
    {
        $this->registerClientScript();
        Yii::$app->session->set($this->className().'_name', $this->fieldName);
        $attribute = $this->attribute;
        $value = $this->model->$attribute;

        return \yii\grid\GridView::widget([
            'showHeader'   => false,
            'summary'      => '',
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\PaySystem::find()
                    ->innerJoin('gs_unions_shop_payments', 'gs_unions_shop_payments.paysystem_id = gs_paysystems.id')
                    ->andWhere(['union_id' => $this->unionId])
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                [
                    'content' => function ($item) {
                        return Html::radio(\Yii::$app->session->get($this->className() . '_name'), false, [
                            'value' => \yii\helpers\ArrayHelper::getValue($item, 'id'),
                            'class' => 'js-radio',
                        ]);
                    }
                ],
                [
                    'attribute' => 'image',
                    'format'    => ['image', ['width' => 100, 'class' => 'thumbnail', 'style' => 'margin-bottom:0px;']]
                ],
                'title',
            ]
        ]);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {

        Asset::register($this->view);
        $this->view->registerJs(<<<JS
$('.rowTable').click(function() {
    $('#paymentType').val($(this).data('id'));
    $(this).parent().find('.js-radio').prop('checked','');
    $(this).find('.js-radio').prop('checked', 'checked');
});
JS
        );
    }


    /**
     * Возвращает опции для виджета
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }

}
