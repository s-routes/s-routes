<?php

/**
 * widget_uploader_many
 * id
 * row_id
 * field_id
 * file_path
 * file_name
 *
 */

namespace common\widgets\FileUploadMany2;

use cs\services\SitePath;
use yii\db\ActiveRecord;

class ModelFiles extends ActiveRecord
{
    public static function tableName()
    {
        return 'widget_uploader_many';
    }

    /**
     * Возвращает полный путь к файлу
     *
     * @return string
     */
    public function getPathFull()
    {
        return (new SitePath($this->file_path))->getPathFull();
    }

    /**
     * Возвращает путь к файлу относительно корня сайта
     *
     * @return string
     */
    public function getPath()
    {
        $val = $this->file_path;
        if (is_null($val)) return '';

        return $val;
    }

    /**
     * Возвращает название файла
     *
     * @return string
     */
    public function getFileName()
    {
        $val = $this->file_name;
        if (is_null($val)) return '';

        return $val;
    }

    /**
     * Возвращает ссылку для скачивания файла
     *
     * @return string
     */
    public function getDownloadLink()
    {
        return FileUploadMany::getDownloadLink($this->id);
    }
}