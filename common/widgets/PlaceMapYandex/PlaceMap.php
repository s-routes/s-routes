<?php

namespace common\widgets\PlaceMapYandex;

use common\services\Security;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\InputWidget;
use yii\web\UploadedFile;
use yii\web\JsExpression;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;

/**
 * class PlaceMap
 *
 * Опции виджета:
 * ```php
 * $options = [
 *     'style' => [
 *                'input' => ''
 *                'divMap' => ''
 *                ]
 * ]);
 * ```
 *
 * Использование:
 *
 * ```php
 * $form->input($model, 'name')->widget(PlaceMap::className(), $options)
 * ```
 */
class PlaceMap extends InputWidget
{
    private $fieldId;
    private $fieldName;

    private $fieldIdMap;

    private $fieldIdLat;
    private $fieldNameLat;

    private $fieldIdLng;
    private $fieldNameLng;

    public $style = [
        'input'  => ['class' => 'form-control'],
        'divMap' => [],
    ];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $formName = $this->model->formName();
        $this->getId();

        $this->fieldId = strtolower($formName . '-' . $this->attribute);
        $this->fieldName = $formName . '[' . $this->attribute . ']';

        $this->fieldIdMap = strtolower($formName . '-' . $this->attribute . '-map');

        $this->fieldIdLng = strtolower($formName . '-' . $this->attribute . '-lng');
        $this->fieldNameLng = $formName . '[' . $this->attribute . '-lng' . ']';

        $this->fieldIdLat = strtolower($formName . '-' . $this->attribute . '-lat');
        $this->fieldNameLat = $formName . '[' . $this->attribute . '-lat' . ']';
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        $this->registerClientScript();
        $html = [];

        if ($this->hasModel()) {
            $lng = null;
            $lat = null;
            $attribute = $this->attribute;
            if ($this->model->$attribute) {
                $lat = ArrayHelper::getValue($this->model->$attribute, 'lat');
                $lng = ArrayHelper::getValue($this->model->$attribute, 'lng');
            } else { // для обеспечения совместимости со старыми версиями
                $fieldNameLng = $this->attribute . '_lng';
                $fieldNameLat = $this->attribute . '_lat';

                if (isset($this->model->$fieldNameLng)) {
                    $lng = $this->model->$fieldNameLng;
                }
                if (isset($this->model->$fieldNameLat)) {
                    $lat = $this->model->$fieldNameLat;
                }
            }

            // hidden
            $html[] = Html::input('hidden', $this->fieldNameLng, $lng, ['id' => $this->fieldIdLng]);
            $html[] = Html::input('hidden', $this->fieldNameLat, $lat, ['id' => $this->fieldIdLat]);

            // input
            $inputAttributes = ArrayHelper::getValue($this->style, 'input', []);
            $inputAttributes = ArrayHelper::merge($inputAttributes, ['id' => $this->fieldId]);
            $html[] = Html::input('text', $this->fieldName, null, $inputAttributes);

            $html[] = Html::tag('div', null, [
                'id'    => 'map',
                'style' => 'width: 100%; height: 300px; border-radius:5px;margin-top: 10px;'
            ]);
        } else {

        }

        return join("\r\n", $html);
    }

    /**
     *
     * @param array $field
     *
     * @return array
     */
    public function onUpdate($field)
    {
        $fieldName = $field[ BaseForm::POS_DB_NAME ];
        $model = $this->model;
        $model->$fieldName = self::getParam($fieldName, $model);
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->getView());

        $id = Html::getInputId($this->model, $this->attribute);
        $hash = 'init'.Security::generateRandomString(10);
        $this->getView()->registerJs(<<<JS
var {$hash} = function() {
    PlaceMapYandex.init('{$id}');    
};
ymaps.ready({$hash});
JS
);
    }

    /**
     * Returns the options for the captcha JS widget.
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [];
    }


    /**
     * Возвращает значение поля формы из поста
     *
     * @param string $fieldName
     * @param \yii\base\Model $model
     *
     * @return string
     */
    public static function getParam($fieldName, $model)
    {
        $formName = $model->formName();
        $query = $formName . '.' . $fieldName;

        return ArrayHelper::getValue(\Yii::$app->request->post(), $query, '');
    }

}
