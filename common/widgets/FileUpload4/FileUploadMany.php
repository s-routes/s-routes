<?php

namespace common\widgets\FileUpload4;

use cs\base\FormActiveRecord;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;
use yii\imagine\Image;
use yii\web\JsExpression;
use Imagine\Image\ManipulatorInterface;
use cs\base\BaseForm;
use cs\Widget\FileUploadMany\ModelFiles;
use cs\services\UploadFolderDispatcher;
use cs\services\SitePath;
use yii\jui\InputWidget;

/**
 * что сделать
 * - сделать в контроллер загрузки вывод ссылки на оригинал сохраненного изображения.
 * - сделать сохранение в поле
 * -
 *
 * Класс FileUploadMany
 *
 * Виджет который загружает файлы по несколько штук
 * Если загружаются картинки то они показываются в предпросмотре
 * и обрезаются по размеру указанному в параметре small
 *
 * Максимальный размер загружаемого файла по умолчанию устанавливается равный тому который указан в параметре ini.php upload_max_filesize
 *
 *
 *
 * $field->widget('cs\Widget\FileUploadMany2\FileUploadMany', [
 *
 * ]);
 *
 * $options = [
 *      'serverName'
 * ];
 *
 * $model->$fieldName = [
 *      ['file_path', 'file_name'],
 * ];
 */
class FileUploadMany extends InputWidget
{
    const MODE_THUMBNAIL_INSET = ManipulatorInterface::THUMBNAIL_INSET;
    const MODE_THUMBNAIL_OUTBOUND = ManipulatorInterface::THUMBNAIL_OUTBOUND;

    /** @var string путь загрузки файлов, может быть только в пределах @webroot и начинается строка с '/' */
    public static $uploadDirectory = 'FileUpload4';
    public static $tableNameFiles = 'widget_uploader_many';
    public static $tableNameFields = 'widget_uploader_many_fields';

    public $uploadUrl = '/upload3/upload';

    /**
     * @var string the template for arranging the CAPTCHA image tag and the text input tag.
     * In this template, the token `{image}` will be replaced with the actual image tag,
     * while `{input}` will be replaced with the text input tag.
     */
    public $template = "<div class='upload'>\n{image}\n{checkbox}\n{input}\n</div>";

    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];

    public $small;

    private $tableName;

    private $fieldId;
    private $fieldName;
    private $hiddenId;
    private $hiddenName;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        $this->fieldId = strtolower($this->model->formName() . '-' . $this->attribute);
        $this->fieldName = $this->model->formName() . '[' . $this->attribute . ']';
        $this->hiddenId = strtolower($this->model->formName() . '-' . $this->attribute . '-files');
        $this->hiddenName = $this->model->formName() . '[' . $this->attribute . '-files' . ']';
    }

    /**
     * рисует виджет
     */
    public function run()
    {
        $this->registerClientScript();

        if ($this->hasModel()) {
            $attribute = $this->attribute;

            $files = $this->model->$attribute;
            if (is_null($files)) $files = [];
            $this->clientOptions['files'] = $files;
            $c1 = Html::hiddenInput($this->hiddenName, json_encode($this->clientOptions['files'], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE), ['id' => $this->hiddenId]);
            $c2 = Html::fileInput($this->fieldName, null, ['id' => $this->fieldId]);

            return Html::tag('div', $c1 . $c2, ['class' => 'multifile_upload']);
        }
    }

    /**
     * Registers the needed JavaScript.
     */
    public function registerClientScript()
    {
        Asset::register($this->view);
        $this->clientOptions = ArrayHelper::merge($this->getClientOptions(), $this->clientOptions);
        $options = Json::encode($this->clientOptions);
        $js = <<<JSSSS
FileUploadMany4_1.init('#{$this->fieldId}', {$options});
JSSSS;
        $this->getView()->registerJs($js);
    }

    /**
     * Возвращает опции для виджета
     *
     * @return array the options
     */
    protected function getClientOptions()
    {
        return [
            'url'         => $this->uploadUrl,
            'maxFileSize' => self::getUploadMaxFileSize(),
            'formData' => [
                'table'       => $this->tableName(),
                'field'       => $this->attribute,
            ],
        ];
    }

    private function tableName()
    {
        if ($this->model instanceof FormActiveRecord) {
            return $this->model->tableName();
        } else {
            return $this->tableName;
        }
    }

    /**
     * Возвращает максимально возможно загружаемый файл который установлен в настройках PHP
     *
     * @return int в байтах
     */
    private static function getUploadMaxFileSize()
    {
        $maxFileSize = ini_get('upload_max_filesize');

        return (int)substr($maxFileSize, 0, strlen($maxFileSize) - 1) * 1024 * 1024;
    }

    /**
     * @param array $field
     *
     * @return array поля для обновления в БД
     */
    public function onLoad($field = null)
    {
        $model = $this->model;
        $fieldName = $this->attribute;
        $post = Yii::$app->request->post();
        $query = $model->formName() . '.' . $fieldName . '-files';
        $filesString = ArrayHelper::getValue($post, $query, '');
        $model->$fieldName = Json::decode($filesString);
    }

    /**
     *
     *
     * @param array $field
     *
     * @return array поля для обновления в БД
     */
    public function onLoadDb($field)
    {
        $model = $this->model;
        $fieldName = $this->attribute;
        $file = $model->$fieldName;
        if (is_null($file)) {
            $model->$fieldName = [];
        } else {
            $f = explode('/', $file);

            $model->$fieldName = [[
                $file,
                $f[count($f) - 1],
            ]];
        }
    }

    /**
     * @param array $field
     */
    public function onUpdate($field)
    {
        $model = $this->model;
        $fieldName = $this->attribute;
        $files = $model->$fieldName;
        if (count($files) == 0) {
            $model->$fieldName = null;
            return;
        }

        $model->$fieldName = $files[0][0];
    }

    /**
     * Удаляет
     *
     * @param array $field
     */
    public function onDelete($field)
    {
        return;
    }

}
