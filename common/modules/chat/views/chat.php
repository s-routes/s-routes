<?php

/** @var $isShowCharMessage bool */
/** @var $room_id int */
/** @var $user_id int */
/** @var $send_url string */

\avatar\assets\SocketIO\Asset::register($this);

$serverName = \avatar\assets\SocketIO\Asset::getHost();

if (!isset($isShowCharMessage)) $isShowCharMessage = true;
?>


<div id="chat">

    <?php

    $myid = Yii::$app->user->id;
    $this->registerJs(<<<JS

let myid =  {$myid};

var socket = io.connect('{$serverName}');

function chatScroll() {
    var chat = $(".panel-body");
    chat.scrollTop(chat.prop("scrollHeight"));
}

chatScroll();

var name;
const buttonSend = document.getElementById('btn-chat');

var roomName = 'room' + {$room_id};

socket.emit('chat1-new-user', roomName, myid);

var message_text;
var file2;

function getFile()
{
    var s = $('.fileUploadedUrl').html();
    if (typeof s == 'undefined') {
        return '';
    } else {
        if (s.length == 0) return '';
        return $('.fileUploadedUrl').html();
    }
}



$('#btn-chat').click(function(e) {
    e.preventDefault();

    message_text = $('#btn-input').val();
    if (message_text.length > 0 || getFile() != '') {
        appendMessage(message_text);
    }    
});

socket.on('chat-message2', (user_sender, data) => {
    console.log(['chat-message2', user_sender, data]);
    appendMessageGet(data);
});

/**
* Выводит сообщение пришедшее от NODEJS
* @param data
*/
function appendMessageGet(data) 
{
    $('.chat').append(data.message.html);

    chatScroll(); 
}

function appendMessage(text)
{
    ajaxJson({
        url: '{$send_url}',
        data: {
            message: text,
            room_id: {$room_id},
            file: getFile()
        },
        success: function(ret) {
            
            // отправляю сообщение на сервер
            socket.emit('chat-message2', roomName, {$user_id}, ret);
            
            $('.chat').append(ret.message.html);
            
            // обнуляю текстовое поле
            $('#btn-input').val('');
            $('.fileUploadedUrl').html('');
            
            chatScroll();
        }
    });
};
JS
    );
    ?>

    <style>
        .chat
        {
            list-style: none;
            margin: 0;
            padding: 0;
        }

        .chat li
        {
            margin-bottom: 10px;
            padding-bottom: 5px;
            border-bottom: 1px dotted #B3A9A9;
        }

        .chat li.left .chat-body
        {
            margin-left: 60px;
        }

        .chat li.right .chat-body
        {
            margin-right: 60px;
        }


        .chat li .chat-body p
        {
            margin: 0;
            color: #777777;
        }

        .panel .slidedown .glyphicon, .chat .glyphicon
        {
            margin-right: 5px;
        }

        .panel-body
        {
            overflow-y: scroll;
            height: 600px;
        }

        ::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar
        {
            width: 30px;
            background-color: #F5F5F5;
        }

        ::-webkit-scrollbar-thumb
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #555;
        }

        .panel-primary > .panel-heading {
            background-image: linear-gradient(to bottom, #750f0b 0%, #750f0b 100%) !important;
        }

        .panel-primary {
            border-color: #750f0b !important;
        }

        .btn-warning {
            background-image: linear-gradient(to bottom, #5cb85c 0%, #5cb85c 100%);
        }

        .btn-warning:hover, .btn-warning:focus {
            background-color: #5cb85c !important;
            background-position: 0 -15px;
        }

        .btn-warning:hover {
            color: #fff;
            background-color: #5cb85c !important;
            border-color: #5cb85c !important;
        }

    </style>


    <div class="panel panel-primary">
        <div class="panel-heading">
            <span class="glyphicon glyphicon-comment"></span> <?= \Yii::t('c.zpiXUyz0nd', 'Чат') ?>

        </div>
        <div class="panel-body">

            <ul class="chat">
                <?php
                $rows = \common\models\ChatMessage2::find()
                    ->where(['room_id' => $room_id])
                    ->all();
                ?>
                <?php /** @var \common\models\ChatMessage2 $message */ ?>
                <?php foreach ($rows as $message) { ?>
                    <?= $this->render('message', ['message' => $message]); ?>
                <?php } ?>
            </ul>
        </div>

        <?php if ($isShowCharMessage) { ?>
            <?= $this->render('chat_footer'); ?>
        <?php } ?>
    </div>
</div>