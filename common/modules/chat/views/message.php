<?php

/** @var $this yii\web\View */
/** @var $message \common\models\ChatMessage2 */

?>
<li class="left clearfix" data-id="<?= $message->id ?>">
    <span class="chat-img pull-left">
        <img width="50" src="<?= $message->getUser()->getAvatar() ?>" alt="<?= $message->getUser()->getName2() ?>" class="img-circle" />
    </span>
    <div class="chat-body clearfix">
        <div class="header2">
            <strong class="primary-font"><?= $message->getUser()->getName2() ?></strong>
            <small class="pull-right text-muted">
                <span class="glyphicon glyphicon-time"></span><?= Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s') ?>
            </small>
        </div>
        <?php $data = \yii\helpers\Json::decode($message->message) ?>
        <?php if (!\cs\Application::isEmpty($data['text'])) { ?>
            <p>
                <?= nl2br($data['text']) ?>
            </p>
        <?php } ?>
        <?php if (!\cs\Application::isEmpty($data['file'])) { ?>
            <p>
                <?= \yii\helpers\Html::a($data['file'], $data['file']) ?>
            </p>
        <?php } ?>

    </div>
</li>
