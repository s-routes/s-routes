<?php
namespace common\components\telegram;


/**
 * Created by PhpStorm.
 * User: Святослав
 * Date: 06.09.2021
 * Time: 20:44
 */
/**
 * Interface TelegramConnectInterface
 *
 * @property int user_id
 * @property string hash
 *
 * @package common\components\telegram
 */
interface TelegramConnectInterface
{

}