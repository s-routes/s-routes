<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 14.04.2016
 * Time: 11:12
 */

namespace common\components;


use common\models\Language;
use Yii;
use yii\base\Component;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * Корневой компонент содержащий в себе сервисы банка
 *
 */
class Bank extends Component
{

}