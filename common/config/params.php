<?php

return [
    'unlockKey'               => '98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c',
    'PROD_DB_TOKEN'           => env('PROD_DB_TOKEN'),
    'AUTO_REDEMPTION_USER_ID' => env('AUTO_REDEMPTION_USER_ID'),
    'landingEmail'            => 'info@neuro-n.com',

    // Почта куда отправляется уведомление что нода рассинхронизировалась
    'prizmAdmin' => [
        'dram1008@yandex.ru',
        'j0565@bk.ru',
        'alex@aronix.ru',
        'desaster@mail.ru',
        'vimvictory@gmail.com',
    ],

    'prizm' => [
        // разница между нашим блокчейном и внешним. кол-во блоков после которого отсылается письмо
        'blockDeltaAlert' => 150,

        // процент на майнинг в день
        'mining-percent' => 0.001
    ],

    'referal-program' => [
        // пользователь который продает нейроны
        'user_id' => [5496, 4379],

        // кошелек откуда берутся реферальные нейроны
        'wallet_id' => 10610
    ],

    'shop-referal-program' => [
        // кошелек откуда берутся реферальные маркеты
        'wallet_id'   => 284,
        'level-list1' => [1, 1, 0.5],
        'level-list2' => [10, 8, 6, 4, 2],
    ],

    // Данные о техничечком перерыве
    'technical-pause' => ['start' => (new DateTime('2021-06-21 23:00:00', new DateTimeZone('Europe/Moscow')))->format('U'), 'finish' => (new DateTime('2021-06-21 23:30:00', new DateTimeZone('Europe/Moscow')))->format('U')],

    'supportEmail'                  => 'support@example.com',

    // время жизни токена для сброса пароля
    'user.passwordResetTokenExpire' => 3600,

    'google-authorisation-code'     => [
        'server' => YII_ENV_DEV ? 's-routes.localhost' : 'neiro-n.com',
    ],
    'mobile' => [
        'phoneTokenSavePeriod' => 60 * 60 * 24 * 100, // срок действия phone-token для быстрого логина, сек
    ],

    'cloudflare' => [
        'dns' => [
            'neironcoin.com' => [
                'id'      => 'a2970a2131c2c55b80ddcde0da457464',
                'records' => [
                    'd2f4e4265e6df4a4c9669ce7607fce9d', // neironcoin.com
                ],
            ],
            'neiro-n.com'    => [
                'id'      => '49c7be171cd66eff417c9ddbfa04cc93',
                'records' => [
                    'f1a45b5c8dfdb1cf4c2193988af7d003', // neiro-n.com
                ],
            ],
            'topmate.one'    => [
                'id'      => 'c2e4029ab4e9a466331b53e7ca2b8050',
                'records' => [
                    '1cb13da3cb5144381d594aeeda30f678', // topmate.one
                ],
            ],
        ],
        'ip' => [
            'prod'   => '217.61.109.77',
            'reserv' => '161.97.172.92',
        ],
    ],
    'google' => [
        'plugins' => [
            'reCaptcha' => [
                'key'       => '6Lf7ZbkZAAAAABcHhTd9eg6P0bWYbdjqbm-O3nYe',
                'secretKey' => '6Lf7ZbkZAAAAAL1uSY3NaaZUz8SP069X1Ow1GS11',
            ],
        ],
    ],

    'telegramLot' => [
        'class'            => '\avatar\controllers\actions\TelegramCallBack',
        'botCodeName'      => '@e_lottery_info_bot',
        'component'        => 'telegramLot',
        'userField'        => 'telegram_lot_chat_id',
        'userFieldAccount' => 'telegram_lot_username',
        'modelTemp'        => '\common\models\UserTelegramLotTemp',
        'modelConnect'     => '\common\models\UserTelegramLotConnect',
        'scheme'           => 'https://elottery.cloud',
        'mailSendOptions'  => [
            'namespace'  => 'lot',
            'from_name'  => 'elottery.cloud',
            'from_email' => 'info@elottery.cloud',
            'layout'     => '@lot/mail/layouts/html',
        ],
        'languageCategory' => 'c.EJ5oxJAeD0',
        'messages' => [
            'textFirst' => 'Какой язык вы предпочитаете русский или английский?
Напишите "RU" или "EN"

Which language do you prefer Russian or English?
Write "RU" or "EN"',
            'commands' => [
                'user' => [
                    '/reset'      => 'Перейти в начало диалога. Сброс.',
                    '/help'       => 'Вывести помощь.',
                    '/disconnect' => 'Отсоединить телеграм бота от профиля',
                    '/me'         => 'Посмотртеть информацию о себе',
                ],
                'guest' => [
                    '/reset' => 'Перейти в начало диалога. Сброс.',
                    '/help'  => 'Вывести помощь.',
                ],
            ],
            'disconnect' => 'Я отсоединяюсь! До скорых встреч!',
            't' => [
                'case2yes'                   => 'Напишите пожалуйста ваш e-mail, который вы указывали при регистрации',
                'case2no'                    => 'Не страшно, давайте я вас зарегистрирую',
                'case2error'                 => 'Можно ответить только словами «да» или «нет»',
                'case3error'                 => 'Вы указываете неверный e-mail, введите пожалуйста корректный адрес почты',
                'case3str1'                  => 'Отлично! Все получается!',
                'case3str2'                  => 'Подтверждение почты от Telegram @e_lottery_info_bot',
                'case3error2'                => 'Сожалею, но такого пользователя на платформе нет. Давайте попробуем еще раз, - напишите ваш e-mail снова',
                'case5str1'                  => 'Подтверждение почты от Telegram @e_lottery_info_bot',
                'case5str2'                  => 'Я выслала вам e-mail. Зайдите в вашу почту, пройдите по ссылке, и подтвердите регистрацию.',
                'case5str3'                  => 'Отлично! Все получается!',
                'case6error'                 => 'Не верный email, введите пожалуйста корректный email',
                'case6error2'                => 'Такой email уже зарегистрирован.',
                'case6error3'                => 'Введите e-mail, который не зарегистрирован, или сбросьте разговор к его началу командой /reset',
                'case6str1'                  => 'Зайдите на почту и подтвердите почту или напишите "выслать" и я вышлю еще раз',
                'case6str2'                  => 'Подтверждение почты от Telegram @e_lottery_info_bot',
                'case8str1'                  => 'Ссылка продублирована. Зайдите на почту и подтвердите e-mail.',
                'case8str2'                  => 'Зайдите на почту и подтвердите почту или напишите "выслать" и я вышлю еще раз',
                'case10str1'                 => 'Доброго времени суток, уважаемый пользователь экосистемы Elottery',
                'case10str2'                 => 'Можно ответить только словами «ru» или «en» / You can only respond with the words «ru» or «en»',
                'actionConfirmTelegram'      => 'Вы молодец! У вас всё получилось! Я буду на связи. До скорых встреч...',
                'actionPin'                  => 'Вы молодец! У вас всё получилось! Я буду на связи. До скорых встреч...',
                'actionRegistrationTelegram' => 'Ура! Все получилось. Вы зарегистрированы, и теперь сможете получать всю информацию оперативно.',
                'actionUnPin'                => 'Я отсоединяюсь! До скорых встреч!',
            ],
        ],
    ],
];
