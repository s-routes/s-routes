<?php

Yii::setAlias('@api', dirname(dirname(__DIR__)) . '/api');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@common', dirname(dirname(__DIR__)) . '/common');
Yii::setAlias('@avatar', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@csRoot', dirname(dirname(__DIR__)) . '/common/app');
Yii::setAlias('@cs', dirname(dirname(__DIR__)) . '/common/app');
Yii::setAlias('@shop', dirname(dirname(__DIR__)) . '/shop');
Yii::setAlias('@coin', dirname(dirname(__DIR__)) . '/coin');
Yii::setAlias('@webroot', dirname(dirname(__DIR__)) . '/public_html');
Yii::setAlias('@lot', dirname(dirname(__DIR__)) . '/lot');
