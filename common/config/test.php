<?php

use common\services\SberKassa;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language'   => 'ru',
    'aliases' => [
        '@bower' => env('BOWER_PATH'),
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'sms'              => [
            'class'    => '\common\components\sms\IqSms',
            'gate'     => env('SMS_GATE'),
            'login'    => env('SMS_LOGIN'),
            'password' => env('SMS_PASSWORD'),
        ],
        'telegram'         => [
            'class'    => '\aki\telegram\Telegram',
            'botToken' => env('TELEGRAM_BOT_ID'),
        ],
        'telegramShop'     => [
            'class'    => '\aki\telegram\Telegram',
            'botToken' => env('TELEGRAM_BOT_SHOP_ID'),
        ],
        'telegramLot'     => [
            'class'    => '\aki\telegram\Telegram',
            'botToken' => env('TELEGRAM_BOT_LOT_ID'),
        ],
        'PrizmApi'         => [
            'class' => '\common\services\PrizmApi',
            'key'   => env('PRIZM_API_KEY'),
            'url'   => env('PRIZM_URL'),
        ],
        'SberKassa' => [
            'class'    => '\common\services\SberKassa',
            'operator' => env('SBER_KASSA_OPERATOR'),
            'api'      => env('SBER_KASSA_API'),
            'password' => env('SBER_KASSA_PASSWORD'),
            'secret'   => env('SBER_KASSA_SECRET'),
        ],
        'TinkoffKassa' => [
            'class'    => '\common\services\TinkoffKassa',
            'login'    => env('TINKOFF_KASSA_LOGIN'),
            'password' => env('TINKOFF_KASSA_PASSWORD'),
        ],
        'SdmKassa' => [
            'class'    => '\common\services\SdmKassa',
            'MERCHANT' => env('SDM_KASSA_MERCHANT_ID'),
            'TERMINAL' => env('SDM_KASSA_TERMINAL_ID'),
            'secret'   => env('SDM_KASSA_PASSWORD'),
        ],
        'Binance'          => [
            'class'     => '\frontend\modules\Binance\Binance',
            'apiUrl'    => env('BINANCE_API_URL', 'https://api.binance.com'),
            'apiKey'    => env('BINANCE_API_KEY'),
            'apiSecret' => env('BINANCE_API_SECRET'),
        ],
        'CloudFlare'       => [
            'class'     => '\common\services\CloudFlare',
            'authKey'   => env('CLOUD_FLARE_AUTH_KEY'),
            'authEmail' => env('CLOUD_FLARE_AUTH_EMAIL'),
        ],
        'CurrencyDataFeed' => [
            'class' => '\common\services\CurrencyDataFeed',
            'key'   => env('CURRENCYDATAFEED_API_KEY'),
            'url'   => env('CURRENCYDATAFEED_API_URL'),
        ],
        'GoldPrizeApi'     => [
            'class' => '\common\services\GoldPrizeApi',
            'key'   => env('GOLD_PRIZE_API_KEY'),
        ],
        'BlockCypher'      => [
            'class' => '\common\services\BlockCypher',
            'key'   => env('BLOCKCYPHER_API_KEY'),
        ],
        'CustomizeService' => [
            'class' => '\avatar\services\CustomizeService',
        ],
        'onlineManager'    => [
            'class'           => 'common\components\OnlineManager',
            'maxTimeToUpdate' => 2,
            'sessionTime'     => 3600,
            'clearTime'       => 10,
        ],
        'monitoring'       => [
            'class' => 'common\components\Monitoring',
        ],
        'monitoringDb'     => [
            'class' => 'common\components\MonitoringDb',
        ],
        'monitoringParams' => [
            'class' => 'common\components\MonitoringParams',
        ],
        'urlManager'       => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
            'enableStrictParsing' => true,
            'hostInfo'            => env('URL_MANAGER_HOST_INFO'),
            'suffix'              => '',
            'rules'               => [
                ['class' => '\avatar\services\CompanyUrlRule'],
                '/'                         => 'site/index',
                'merchant'                  => 'merchant/index',
                'ru'                        => 'ru/index',
                'en'                        => 'en/index',
                'documents'                 => 'documents/index',
                'api/<controller>/<action>' => 'api-<controller>/<action>',

                // карточка пользователей
                'user/<id:\\d+>'            => 'user/index',

                'upload/HtmlContent2_common'       => 'upload/upload',
                'upload3/upload'                   => 'upload3/upload',
                'partner/registration/<code:\\d+>' => 'partner/registration',

                '<controller>/<action>'           => '<controller>/<action>',
                '<controller>/<action>/<id:\\w+>' => '<controller>/<action>',
            ],
        ],
        'db'               => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'dsn'                 => env('MYSQL_DSN_MAIN'),
            'username'            => env('MYSQL_USER'),
            'password'            => env('MYSQL_PASSWORD'),

            // Enable cache schema
            'enableSchemaCache'   => false,

            // Duration of schema cache.
            'schemaCacheDuration' => 3600,

            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],
        'dbAvatar'               => [
            'class'               => 'yii\db\Connection',
            'charset'             => 'utf8',
            'dsn'                 => 'mysql:host=mysql;dbname=av_network_prod',
            'username'            => env('MYSQL_USER'),
            'password'            => env('MYSQL_PASSWORD'),

            // Enable cache schema
            'enableSchemaCache'   => false,

            // Duration of schema cache.
            'schemaCacheDuration' => 3600,

            // Name of the cache component used to store schema information
            'schemaCache'         => 'cache',
        ],
        'dbStatistic'      => [
            'class'             => 'yii\db\Connection',
            'charset'           => 'utf8',
            'dsn'               => env('MYSQL_DSN_STAT'),
            'username'          => env('MYSQL_USER'),
            'password'          => env('MYSQL_PASSWORD'),
            //            'enableSchemaCache' => true,
        ],
        'dbWallet'         => [
            'class'             => 'yii\db\Connection',
            'charset'           => 'utf8',
            'dsn'               => env('MYSQL_DSN_WALLET'),
            'username'          => env('MYSQL_USER'),
            'password'          => env('MYSQL_PASSWORD'),
            //            'enableSchemaCache' => false,
        ],
        'dbInfo'           => [
            'class'             => 'yii\db\Connection',
            'charset'           => 'utf8',
            'dsn'               => env('MYSQL_DSN_INFO'),
            'username'          => env('MYSQL_USER'),
            'password'          => env('MYSQL_PASSWORD'),
            'enableSchemaCache' => false,
        ],
        'authManager'      => [
            'class'          => 'yii\rbac\PhpManager',
            'assignmentFile' => '@frontend/rbac/assignments.php',
            'ruleFile'       => '@frontend/rbac/rules.php',
            'itemFile'       => '@frontend/rbac/items.php',
        ],
        'cache'            =>
            [
                'class'     => 'yii\caching\FileCache',
                'keyPrefix' => 'avatar-bank',
                'cachePath' => '@frontend/runtime/cache',
            ],
        'cacheLanguages'   =>
            [
                'class'     => 'yii\caching\FileCache',
                'keyPrefix' => 'avatar-bank-languages',
                'cachePath' => '@frontend/runtime/cache-languages',
            ],
        // Языки
        'i18n'             => [
            'translations' => [
                'app*' => [
                    'class'                 => '\common\components\DbMessageSourceSky',
                    'cache'                 => 'cacheLanguages',
                    'forceTranslation'      => true,
                    'enableCaching'         => true,
                    'sourceLanguage'        => 'ru',
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
                '*'    => [
                    'class'                 => '\common\components\DbMessageSourceSky',
                    'cache'                 => 'cacheLanguages',
                    'forceTranslation'      => true,
                    'enableCaching'         => true,
                    'sourceLanguage'        => 'ru',
                    'on missingTranslation' => ['\common\components\DbMessageSourceSky', 'handleMissingTranslation'],
                ],
            ],
        ],
        'formatter'        => [
            'dateFormat'        => 'dd.MM.yyyy',
            'timeFormat'        => 'php:H:i:s',
            'datetimeFormat'    => 'php:d.m.Y H:i',
            'decimalSeparator'  => '.',
            'thousandSeparator' => ' ',
            'currencyCode'      => 'RUB',
            'locale'            => 'ru',
            'nullDisplay'       => '',
        ],
        'log'              => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'       => 'yii\log\FileTarget',
                    'levels'      => [
                        'error',
                        'warning',
                    ],
                    'except'      => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],
                    'maxLogFiles' => 1,
                ],
                [
                    'class'      => 'common\services\DbTarget',
                    'db'         => 'dbStatistic',
                    'logTable'   => 'log2',
                    'categories' => ['avatar\\*'],
                    'levels'     => [
                        'error',
                        'warning',
                    ],
                ],
                [
                    'class'    => 'common\services\DbTarget',
                    'db'       => 'dbStatistic',
                    'logTable' => 'log2',
                    'levels'   => [
                        'error',
                        'warning',
                    ],
                    'except'   => [
                        'yii\web\HttpException:404',
                        'yii\web\HttpException:403',
                        'cs\web\Exception',
                    ],
                ],
            ],
        ],
        'mailer'           => [
            'class'            => 'yii\swiftmailer\Mailer',
            'useFileTransport' => YII_ENV_DEV ? true : false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => env('MAILER_TRANSPORT_HOST'),  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username'   => env('MAILER_TRANSPORT_USERNAME'),
                'password'   => env('MAILER_TRANSPORT_PASSWORD'),
                'port'       => env('MAILER_TRANSPORT_PORT'), // Port 25 is a very common port too
                'encryption' => env('MAILER_TRANSPORT_ENCRYPTION'), // It is often used, check your provider or mail server specs
            ],
        ],
        'deviceDetect'     => [
            'class' => '\common\components\DeviceDetect',
        ],
    ],
];
