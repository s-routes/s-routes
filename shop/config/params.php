<?php

return [
    'adminEmail'         => 'admin@galaxysss.ru',
    'mailList'           => [
        'contact' => 'god@avatar-bank.com',
    ],

    'mailer'                        => [
        // адрес который будет указан как адрес отправителя для писем и рассылок
        'from' => [
            env('MAILER2_FROM_EMAIL') => env('MAILER2_FROM_NAME'),
        ],
    ],

    'rootSite' => 'https://neiro-n.com',
];
