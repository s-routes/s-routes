<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$config = [
    'id'                                              => 'sroutes-shop',
    'basePath'                                        => dirname(__DIR__),
    'bootstrap'                                       => ['log'],
    'controllerNamespace'                             => 'shop\controllers',
    'vendorPath'                                      => dirname(dirname(__DIR__)) . '/vendor',
    'language'                                        => 'ru',
    'sourceLanguage'                                  => 'ru',
    'timeZone'                                        => 'Etc/GMT-3',
    'catchAll'                                        => require('catchAll.php'),
    'aliases'                                         => [
        '@csRoot'  => __DIR__ . '/../../common/app',
        '@cs'      => __DIR__ . '/../../common/app',
        '@webroot' => __DIR__ . '/../../public_html',
    ],
    'components'                                      => [
        'session'         => [
            'timeout' => 60*60*24*365,
            'class'   => 'yii\web\CacheSession',
            'cache'   => 'cacheSession',
        ],
        'cacheSession' => [
            'class'     => 'yii\caching\FileCache',
            'keyPrefix' => 'session',
        ],
        'urlManager'         => [
            'rules' => [
                ['class' => '\avatar\services\CompanyUrlRule'],
                '/'              => 'shop/index',
                'user/<id:\\d+>' => 'user/item',
            ],
            'hostInfo'            => env('SHOP_URL_MANAGER_HOST_INFO'),
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager'    => [
            'appendTimestamp' => true,
            'basePath'        => '/application/shop/web/assets',
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],
        'mailer'           => [
            'viewPath' => '@shop/mail',
        ],

    ],
    'params'                                          => $params,
    'controllerMap'                                   => [
        'upload'  => '\common\widgets\HtmlContent\Controller',
        'upload3' => '\common\widgets\FileUploadMany2\UploadController',
        'upload4' => '\iAvatar777\assets\JqueryUpload1\Upload2Controller',
    ],
    'on beforeRequest'                                => function ($event) {
        Yii::$app->onlineManager->onBeforeRequest();
        Yii::$app->monitoring->onBeforeRequest();

        // Сохраняю реферальный параметр в сессию
        $value = \Yii::$app->request->get('partner_id');
        if (!is_null($value)) {
            \Yii::$app->session->set('ReferalProgram', ['partner_id' => $value]);
        }
    },
    'on ' . \yii\web\Application::EVENT_AFTER_REQUEST => function ($event) {
        Yii::$app->monitoring->onAfterRequest();
    }
];


if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
