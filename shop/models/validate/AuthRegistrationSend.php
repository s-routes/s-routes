<?php

namespace shop\models\validate;

use avatar\models\search\UserAvatar;
use common\components\Card;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\SendLetter;
use common\models\User;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class AuthRegistrationSend extends Model
{
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'string'],
        ];
    }


    public function action()
    {
        /**
         * @var $data array
         * [
         *      'user'             => \common\models\UserAvatar,
         *      'userRegistration' => \avatar\models\UserRegistration,
         * ]
         */
        $data = Yii::$app->session->get('registration-code');
        $ret = \avatar\base\Application::mail($data['user']['email'], 'Подтверждение регистрации', 'registration', [
            'url'      => Url::to([
                'auth/registration-activate',
                'code' => $data['userRegistration']['code']
            ], true),
            'user'     => $data['user'],
            'datetime' => $data['userRegistration']['date_finish']
        ]);

        return 1;
    }

}
