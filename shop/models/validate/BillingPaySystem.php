<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\PaySystemConfig;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class BillingPaySystem extends \iAvatar777\services\FormAjax\Model
{
    /** @var int */
    public $ps_id;

    /** @var \common\models\BillingMain */
    public $billing;

    public $bid;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['ps_id', 'required'],
            ['ps_id', 'integer'],
        ];
    }

    public function attributeWidgets()
    {
        return [
            'ps_id' => [
                'class' => '\avatar\widgets\PaySystemList',
                'rows' => \common\models\PaySystemConfig::find()
                    ->innerJoin('paysystems', 'paysystems_config.paysystem_id = paysystems.id')
                    ->andWhere(['paysystems.currency' => 'RUB'])
            ]
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $config = PaySystemConfig::findOne($this->ps_id);
        $this->billing->config_id = $config->id;
        $this->billing->source_id = $config->paysystem_id;
        $this->billing->save();

        return ['url' => '/billing/pay?id='.$this->billing->id];
    }

}
