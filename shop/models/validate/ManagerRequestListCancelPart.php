<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopRequestAdd;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class ManagerRequestListCancelPart extends \iAvatar777\services\FormAjax\Model
{
    public $price;
    public $comment;

    /** @var int db.currency.id */
    public $currency_id;

    /** @var \common\models\ShopRequest */
    public $request;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'validateAmount'], // Проверка средств на кошельке

            ['comment', 'required'],
            ['comment', 'string'],

            ['currency_id', 'required'],
            ['currency_id', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price'       => 'Сумма удержания',
            'comment'     => 'Коментарий',
            'currency_id' => 'Кошелек на который будут возвращены средства',
        ];
    }

    public function validateAmount($a, $p)
    {
        if (!$this->hasErrors()) {
            // считаю сколько было доплат
            $rows = ShopRequestAdd::find()->where(['request_id' => $this->request->id])->all();
            $sum = 0;
            foreach ($rows as $row) {
                $cio = CurrencyIO::findFromExt($row['currency_id']);
                $sum = bcadd($sum, Currency::getValueFromAtom($row['amount'], $cio->currency_int_id));
            }

            // добавляю цену заказа
            $sum = bcadd($sum, Currency::getValueFromAtom($this->request->price, Currency::RUB));

            $CIO = CurrencyIO::findFromExt($this->request->currency_id_paid);
            if (Currency::getAtomFromValue($this->price, Currency::RUB) > Currency::getAtomFromValue($sum, Currency::RUB)) {
                $this->addError($a, 'Заказ стоит меньше чем вычет');
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // считаю сколько было доплат
        $rows = ShopRequestAdd::find()->where(['request_id' => $this->request->id])->all();
        $sum = 0;
        foreach ($rows as $row) {
            $cio = CurrencyIO::findFromExt($row['currency_id']);
            $sum = bcadd($sum, Currency::getValueFromAtom($row['amount'], $cio->currency_int_id));
        }

        // возвращаю деньги
        $CIO = CurrencyIO::findFromExt($this->currency_id);
        $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id, $this->request->user_id);
        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletTo = $data['wallet'];
        $walletFrom = Wallet::findOne($CIO->main_wallet);
        $sum = bcadd($sum, Currency::getValueFromAtom($this->request->price, Currency::RUB));
        $sum = bcsub($sum, $this->price);
        $t = $walletFrom->move2($walletTo,
            Currency::getAtomFromValue(
                $sum,
                $CIO->currency_int_id
            ),
            'Возврат по заказу №' . $this->request->id . ': ' . $this->comment);

        // Отменяю заказ
        $this->request->is_cancel = 1;
        $this->request->type_cancel = ShopRequest::TYPE_CANCEL_MANAGER_PART;
        $this->request->save();

        return [];
    }

}
