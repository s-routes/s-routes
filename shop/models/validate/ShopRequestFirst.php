<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.06.2016
 * Time: 11:48
 */

namespace shop\models\validate;


use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\shop\Basket;
use common\models\shop\Product;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class ShopRequestFirst extends Model
{
    /** @var  string */
    public $id;

    public function rules()
    {
        return [
            ['id', 'string'],
            ['id', 'validateMarket'],
        ];
    }

    public function validateMarket($a, $p)
    {
        if (!$this->hasErrors()) {
            $rows  = \common\models\ShopTempProduct::find()->where(['user_id' => \Yii::$app->user->id])->all();
            $price = 0;
            foreach ($rows as $row) {
                $price += $row['price'] * $row['count'];
            }
            if (\Yii::$app->user->identity->market_counter < \common\models\piramida\Currency::getAtomFromValue($price, \common\models\piramida\Currency::MARKET)) {
                $this->addError($a, 'Не хватает пакета для покупки');
                return;
            }
            if (\Yii::$app->user->identity->market_till < time()) {
                $this->addError($a, 'У вашего пакета вышел срок давности');
                return;
            }
        }
    }

    public function action()
    {
        return 1;
    }
}