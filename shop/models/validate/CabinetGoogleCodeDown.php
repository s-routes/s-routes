<?php

namespace shop\models\validate;

use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\PaySystem;
use common\models\RequestTokenCreate;
use common\models\UserAvatar;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 *
 */
class CabinetGoogleCodeDown extends \iAvatar777\services\FormAjax\Model
{
    /** @var string */
    public $pin;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['pin', 'required'],
            ['pin', 'string', 'length' => 6],
            ['pin', 'validateCode'],
        ];
    }

    /**
     *
     */
    public function validateCode($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $secret = Yii::$app->user->identity->shop_google_auth_code;
            $pinExternal = $this->pin;
            $file = \Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
            require_once($file);
            $ga = new \GoogleAuthenticator;
            $pinInternal = $ga->getCode($secret);
            if ($pinInternal != $pinExternal) {
                $this->addError($attribute, 'Пин код не верный');
                return;
            }
        }
    }

    /**
     *
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        Yii::$app->user->identity->shop_google_auth_code = null;
        Yii::$app->user->identity->save();
        Yii::$app->session->remove('googleKey');
    }
}
