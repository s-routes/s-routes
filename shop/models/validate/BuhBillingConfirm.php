<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class BuhBillingConfirm extends Model
{
    /** @var  int */
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        $b = BillingMain::findOne($this->id);
        $b->successShop();

        return 1;
    }

    /**
     * Уведомляет клиента
     */
    private function notificateTelegram()
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $request = ShopRequest::findOne(['chat_room_id' => $this->room_id]);

        $u = UserAvatar::findOne($request->user_id);
        /** @var \common\models\UserAvatar $uCurrent */
        $uCurrent = Yii::$app->user->identity;

        if (!Application::isEmpty($u->telegram_shop_chat_id)) {
            $link = Url::to(['cabinet-shop-requests/view', 'id' => $request->id], 'https://topmate.one');
            $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
            $telegram->sendMessage(['chat_id' => $u->telegram_shop_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение от TopMate по заказу №' . $request->id, [], $language) . ' ' . $uCurrent->getName2() . ': ' . "\n". $this->message  . "\n" .  "\n" . 'Ссылка на чат: ' . $link]);
        }
    }

}
