<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class CabinetWalletMarketBuy extends Model
{
    public $count;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        $bm = BillingMainClass::findOne(['name' => '\common\models\RequestInputMarket']);
        $b = BillingMain::add([
            'sum_before'  => $this->count * 100,
            'sum_after'   => $this->count * 100,
            'source_id'   => null,
            'currency_id' => \common\models\avatar\Currency::RUB,
            'class_id'    => $bm->id,
            'config_id'   => null,
            'success_url' => 'https://topmate.one/',
        ]);

        $m = \common\models\RequestInputMarket::add([
            'created_at' => time(),
            'billing_id' => $b->id,
            'amount'     => Currency::getAtomFromValue($this->count, Currency::MARKET),
            'user_id'    => Yii::$app->user->id,
        ]);
        $b->success_url = 'https://topmate.one/cabinet-wallet/input-market-success?id=' . $m->id;
        $b->save();

        return ['billing' => ['id' => $b->id], 'url' => '/billing/pay-system?id='.$b->id];
    }
}
