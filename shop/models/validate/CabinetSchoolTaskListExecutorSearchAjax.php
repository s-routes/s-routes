<?php

namespace shop\models\validate;

use avatar\models\forms\school\CommandLink;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillOperation;
use common\models\PassportLink;
use common\models\RequestTokenCreate;
use common\models\school\AdminLink;
use common\models\school\Kurs;
use common\models\school\Potok;
use common\models\school\PotokUser3Link;
use common\models\school\School;
use common\models\school\SubscribeItem;
use common\models\school\UserLink;
use common\models\ShopAll;
use common\models\task\Helper;
use common\models\task\Task;
use common\models\UserAvatar;
use common\models\UserRoot;
use common\models\UserRootExt;
use cs\services\File;
use cs\services\Str;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use Imagine\Image\Box;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\imagine\Image;

/**
 */
class CabinetSchoolTaskListExecutorSearchAjax extends Model
{
    /** @var  string */
    public $term;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['term', 'string'],
        ];
    }

    /**
     * @return array;
     */
    public function action()
    {
        $rows2 = ShopAll::find()
            ->select([
                'id',
                'link as value',
            ])
            ->where(
                ['like', 'link', $this->term]
            )
            ->limit(10)
            ->asArray()
            ->all()
        ;

        return $rows2;
    }
}
