<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ChatList;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class CabinetSupportChatSend extends Model
{
    public $message;

    /** @var  int */
    public $chat_id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['chat_id', 'required'],
            ['chat_id', 'integer'],

            ['message', 'required'],
            ['message', 'string'],
        ];
    }

    /**
     */
    public function action()
    {
        $message = ChatMessage::add([
            'deal_id' => $this->chat_id,
            'user_id' => Yii::$app->user->id,
            'message' => $this->message,
        ]);
        $html = Yii::$app->view->renderFile('@frontend/views/cabinet-exchange/message.php', ['message' => $message]);

        $data = ArrayHelper::toArray($message);
        $data['timeFormatted'] = Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s');

        $list = \common\models\ChatList::findOne(['user_id' => Yii::$app->user->id]);
        if (is_null($list)) {
            $list = \common\models\ChatList::add(['user_id' => Yii::$app->user->id,'last_message' => time()]);
        } else {
            $list->last_message = time();
            $list->save();
        }

        // Уведомляю по телеграму
        try {
            $this->notificateTelegram();
        } catch (\Exception $e) {
            Yii::error($e->getMessage(), 'avatar\models\validate\CabinetSupportChatSend::action');
        }

        return [
            'message' => [
                'object'        => $data,
                'html'          => $html,
                'timeFormatted' => Yii::$app->formatter->asDatetime($message->created_at, 'php:H:i:s'),
            ],
            'user'    => [
                'id'       => Yii::$app->user->id,
                'avatar'   => Yii::$app->user->identity->getAvatar(),
                'name2'    => Yii::$app->user->identity->getName2(),
            ],
        ];
    }

    private function notificateTelegram()
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        // Если написал админ
        if ($this->isAdmin()) {
            // Уведомляю пользователя

            $uid = $this->chat_id - 2000000000;
            /** @var \common\models\UserAvatar $uCurrent */
            $uCurrent = Yii::$app->user->identity;
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                $link = Url::to(['cabinet-support/chat'], true);
                $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение в поддержку от пользователя', [], $language).' ' . $uCurrent->getName2() . ': ' . "\n". $this->message  . "\n" .  "\n" . 'Ссылка на чат: '.$link]);
            }
        } else {

            // Уведомляю всех
            if (Yii::$app->id == 'sroutes-shop') {
                $role_admin = Yii::$app->authManager->getUserIdsByRole('role_admin');
                $role_shop_buh = Yii::$app->authManager->getUserIdsByRole('role_shop_buh');
                $role_shop_manager = Yii::$app->authManager->getUserIdsByRole('role_shop_manager');

                $admins = ArrayHelper::merge($role_admin, $role_shop_buh);
                $all = ArrayHelper::merge($admins, $role_shop_manager);
                $all = array_unique($all);
            } else {
                $adminList = Yii::$app->authManager->getUserIdsByRole('role_admin');
                $buhList = Yii::$app->authManager->getUserIdsByRole('role_buh');
                $all = ArrayHelper::merge($adminList,$buhList);
                $all = array_unique($all);
            }

            foreach ($all as $uid) {
                $u = UserAvatar::findOne($uid);
                if (!Application::isEmpty($u->telegram_chat_id)) {
                    /** @var \common\models\UserAvatar $uCurrent */
                    $uCurrent = Yii::$app->user->identity;
                    $uid = $this->chat_id - 2000000000;
                    $link = Url::to(['admin-support/chat', 'id' => $uid], true);
                    $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                    $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Сообщение в поддержку от пользователя', [], $language).' ' . $uCurrent->getName2() . ': ' . "\n". $this->message  . "\n" .  "\n" . 'Ссылка на чат: ' . $link]);
                }
            }
        }
    }

    private function isAdmin()
    {
        if (Yii::$app->id == 'sroutes-shop') {
            $role_admin = Yii::$app->authManager->getUserIdsByRole('role_admin');
            $role_shop_buh = Yii::$app->authManager->getUserIdsByRole('role_shop_buh');
            $role_shop_manager = Yii::$app->authManager->getUserIdsByRole('role_shop_manager');

            return in_array(Yii::$app->user->id, $role_admin) || in_array(Yii::$app->user->id, $role_shop_buh) || in_array(Yii::$app->user->id, $role_shop_manager);
        } else {
            $admins = Yii::$app->authManager->getUserIdsByRole('role_admin');
            $BuhList = Yii::$app->authManager->getUserIdsByRole('role_buh');

            return in_array(Yii::$app->user->id, $admins) || in_array(Yii::$app->user->id, $BuhList);
        }
    }

    /**
     * Возвращает ответ: Из админов кто-нибудь написал?
     */
    private function hasAdmin()
    {
        $users = ChatMessage::find()->where(['deal_id' => $this->chat_id])->select(['user_id'])->groupBy(['user_id'])->column();
        $admins = Yii::$app->authManager->getUserIdsByRole('role_admin');
        if (count($users) == 1) {
            if (in_array($users[0], $admins)) {
                return true;
            } else {
                return false;
            }
        } else {
            foreach ($users as $uid) {
                if (in_array($uid, $admins)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Возвращает список админов кто уже ранее общался в чате
     */
    private function getAdmins()
    {
        $users = ChatMessage::find()->where(['deal_id' => $this->chat_id])->select(['user_id'])->groupBy(['user_id'])->column();
        $admins = Yii::$app->authManager->getUserIdsByRole('role_admin');
        if (count($users) == 1) {
            if (in_array($users[0], $admins)) {
                return $users;
            } else {
                return [];
            }
        } else {
            $rows = [];
            foreach ($users as $uid) {
                if (in_array($uid, $admins)) {
                    return $rows[] = $uid;
                } else {
                    return false;
                }
            }
            return $rows;
        }
    }
}
