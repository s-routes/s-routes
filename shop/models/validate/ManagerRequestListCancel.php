<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopRequestAdd;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class ManagerRequestListCancel extends Model
{
    public $id;

    /** @var ShopRequest */
    public $request;

    /** @var int */
    public $type_cancel;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateRequest'],

            ['type_cancel', 'required'],
            ['type_cancel', 'integer'],
        ];
    }

    public function validateRequest($a, $p)
    {
        if (!$this->hasErrors()) {
            $r = ShopRequest::findOne($this->id);
            if (is_null($r)) {
                $this->addError($a, 'Заказ не найден');
                return;
            }
            $this->request = $r;
        }
    }

    /**
     *
     */
    public function action()
    {
        if ($this->request->is_paid) {
            $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $walletMAR = \common\models\UserBill2::findOne(['user_id' => $this->request->user_id, 'currency_id' => $cMAR->id])->getWallet();
            $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));

            // Возвращаю все доплаты
            $rows = ShopRequestAdd::find()->where(['request_id' => $this->request->id])->all();
            foreach ($rows as $row) {
                $CIO = CurrencyIO::findFromExt($row['currency_id']);
                $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id, $this->request->user_id);
                /** @var \common\models\piramida\Wallet $walletFrom */
                $walletTo = $data['wallet'];
                $walletFrom = Wallet::findOne($CIO->main_wallet);
                $t = $walletFrom->move2(
                    $walletTo,
                    $row['amount'],
                    Json::encode([
                        'Возврат по заказу №{id} {comment}',
                        [
                            'id'      => $this->request->id,
                            'comment' => $row['comment'],
                        ],
                    ])
                );

                // возвращаю пакет
                $walletMain->move2(
                    $walletMAR,
                    Currency::convertAtom($row['amount'], $CIO->currency_int_id, $cMAR->id),
                    Json::encode([
                        'Возврат по заказу №{id} {comment}',
                        [
                            'id'      => $this->request->id,
                            'comment' => $row['comment'],
                        ],
                    ])
                );
            }

            // возвращаю деньги
            $CIO = CurrencyIO::findFromExt($this->request->currency_id_paid);
            $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id, $this->request->user_id);
            /** @var \common\models\piramida\Wallet $walletFrom */
            $walletTo = $data['wallet'];
            $walletFrom = Wallet::findOne($CIO->main_wallet);
            $t = $walletFrom->move2(
                $walletTo,
                Currency::getAtomFromValue(
                    Currency::getValueFromAtom($this->request->price, Currency::RUB),
                    $CIO->currency_int_id
                ) ,
                Json::encode([
                    'Возврат по заказу №{id}',
                    [
                        'id' => $this->request->id,
                    ],
                ])
            );

            // возвращаю пакет
            $walletMain->move2(
                $walletMAR,
                Currency::getValueFromAtom($this->request->price, $cMAR->id),
                Json::encode([
                    'Возврат по заказу №{id}',
                    [
                        'id' => $this->request->id,
                    ],
                ])
            );
        }

        // Отменяю заказ
        $request = ShopRequest::findOne($this->id);
        $request->is_cancel = 1;
        $request->type_cancel = $this->type_cancel;
        $request->save();
    }
}
