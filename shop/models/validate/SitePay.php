<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use shop\models\forms\ShopAll;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class SitePay extends Model
{
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['id', 'required'],
            ['id', 'integer'],
        ];
    }

    /**
     */
    public function action()
    {
        $b = BillingMain::add([
            'sum_before'  => 100 * 100,
            'sum_after'   => 100 * 100,
            'source_id'   => 11,
            'currency_id' => \common\models\avatar\Currency::RUB,
            'class_id'    => null,
            'config_id'   => null,
            'success_url' => 'https://topmate.one/site/pay-success',
        ]);

        /** @var \common\services\TinkoffKassa $client */
        $client = Yii::$app->TinkoffKassa;

        $Payment = $client->createPayment($b, '1', '1', ['successUrl' => $b->success_url]);

        return ['url' => $Payment['PaymentURL']];
    }

}
