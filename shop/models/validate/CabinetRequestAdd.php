<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopRequestAdd;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class CabinetRequestAdd extends \iAvatar777\services\FormAjax\Model
{
    public $price;
    public $currency_id;
    public $comment;

    /** @var \common\models\ShopRequest */
    public $request;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['price', 'required'],
            ['price', 'integer'],
            ['price', 'validateAmount'], // Проверка средств на кошельке
            ['price', 'validateAmount2'], // Проверка средств на кошельке

            ['currency_id', 'required'],
            ['currency_id', 'integer'],

            ['comment', 'required'],
            ['comment', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price' => 'Сумма',
            'currency_id' => 'Валюта',
            'comment' => 'Коментарий',
        ];
    }

    public function validateAmount($a, $p)
    {
        if (!$this->hasErrors()) {
            $CIO = CurrencyIO::findFromExt($this->currency_id);
            $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id);
            /** @var \common\models\piramida\Wallet $wallet */
            $wallet = $data['wallet'];
            if ($wallet->amount < Currency::getAtomFromValue($this->price, $CIO->currency_int_id)) {
                $this->addError($a, 'Недостаточно средств на кошельке');
                return;
            }
        }
    }

    public function validateAmount2($a, $p)
    {
        if (!$this->hasErrors()) {
            $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();

            if ($walletMAR->amount < \common\models\piramida\Currency::getAtomFromValue($this->price, \common\models\piramida\Currency::MARKET)) {
                $this->addError($a, 'У Вас израсходован лимит пакета маркет-баллов. Купить подписку на новый, или сделать доплату за пакет большего объема маркет-баллов Вы можете здесь => https://topmate.one/cabinet-market/index');
                return;
            }
            if (\Yii::$app->user->identity->market_till > 0) {
                if (\Yii::$app->user->identity->market_till < time()) {
                    $this->addError($a, 'У вашего пакета вышел срок давности');
                    return;
                }
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        // списываю деньги
        $CIO = CurrencyIO::findFromExt($this->currency_id);
        $data = UserBill::getInternalCurrencyWallet($CIO->currency_int_id);
        /** @var \common\models\piramida\Wallet $walletFrom */
        $walletFrom = $data['wallet'];
        $walletTo = Wallet::findOne($CIO->main_wallet);
        $t = $walletFrom->move2($CIO->main_wallet, Currency::getAtomFromValue($this->price, $CIO->currency_int_id), 'Доплата по заказу №' . $this->request->id . ': ' . $this->comment);

        // Добавляю доплату
        $m = \common\models\ShopRequestAdd::add([
            'created_at'  => time(),
            'amount'      => Currency::getAtomFromValue($this->price, $CIO->currency_int_id),
            'currency_id' => $this->currency_id,
            'user_id'     => Yii::$app->user->id,
            'request_id'  => $this->request->id,
            'comment'     => $this->comment,
        ]);

        // Списываю пакет
        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
        $t = $walletMAR->move2(
            $walletMain,
            Currency::getAtomFromValue($this->price, Currency::MARKET),
            Json::encode([
                'Доплата #{id} для заказа #{rid}',
                [
                    'id'    => $m->id,
                    'rid'    => $this->request->id,
                ],
            ])
        );

        return [];
    }

}
