<?php

namespace shop\models\validate;

use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\BillingMain;
use common\models\BillingMainClass;
use common\models\ChatList;
use common\models\ChatMessage2;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\exchange\DealStatus;
use common\models\exchange\Offer;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopRequestAdd;
use common\models\SupportShop;
use common\models\UserAvatar;
use common\models\UserBan;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use cs\web\Exception;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 */
class CabinetReferal extends \iAvatar777\services\FormAjax\Model
{
    public $url;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['url', 'required'],
            ['url', 'string'],
            ['url', 'validateParent'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'price' => 'Сумма',
            'currency_id' => 'Валюта',
            'comment' => 'Коментарий',
        ];
    }

    public function validateParent($a, $p)
    {
        if (!$this->hasErrors()) {
            $urlObject = new \cs\services\Url($this->url);

            $partner_id = $urlObject->params['partner_id'];

            $partner = \common\models\ShopUserPartner::findOne([
                'user_id'   => Yii::$app->user->id,
                'parent_id' => $partner_id,
            ]);

            if (!is_null($partner)) {
                $this->addError($a, 'Вы уже в программе');
                return;
            }
        }
    }


    public function save($runValidation = true, $attributeNames = null)
    {
        // присоединяю
        $urlObject = new \cs\services\Url($this->url);

        $partner_id = $urlObject->params['partner_id'];

        $partner = \common\models\ShopUserPartner::add([
            'user_id'   => Yii::$app->user->id,
            'parent_id' => $partner_id,
        ]);

        return 1;
    }

}
