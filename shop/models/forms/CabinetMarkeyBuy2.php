<?php

namespace shop\models\forms;

use common\models\avatar\UserBill;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\Packet;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\PurchasePacket;
use common\models\UserDocument;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use Dompdf\Dompdf;
use yii\base\Exception;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use Yii;

/**
 */
class CabinetMarkeyBuy2 extends Model
{
    public $count;

    /** @var int цена пакета в монетах */
    public $price;

    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'integer'],
            ['count', 'validateWallet'],
        ];
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {

            // определяю цену пакета
            $price = [0,0,0,0];

            if (Yii::$app->user->identity->market_packet > 0) {
                if (Yii::$app->user->identity->market_packet == 10000000) {
                    $price = [0,6000,12000,27000];
                }
                if (Yii::$app->user->identity->market_packet == 30000000) {
                    $price = [0,0,6000,21000];
                }
                if (Yii::$app->user->identity->market_packet == 50000000) {
                    $price = [0,0,0,15000];
                }
                if (Yii::$app->user->identity->market_packet == 100000000) {
                    $price = [0,0,0,0];
                }
            }
            if ($this->count == 100000) $i = 0;
            if ($this->count == 300000) $i = 1;
            if ($this->count == 500000) $i = 2;
            if ($this->count == 1000000) $i = 3;
            $this->price = $price[$i];

            // делаю проверку
            $data = UserBill::getInternalCurrencyWallet(Currency::MARKET, Yii::$app->user->id);
            /** @var \common\models\piramida\Wallet $w */
            $w  = $data['wallet'];
            if ($w->amount < Currency::getAtomFromValue($this->price, Currency::MARKET)) {
                $this->addError($a, 'У Вас не хватает средств на покупку');
                return;
            }
        }
    }

    public function action()
    {
        $p = Packet::findOne(['count' => $this->count * 100]);
        $amount = Currency::getAtomFromValue($this->price, Currency::MARKET);

        // записываю покупку пакета
        $PurchasePacket = PurchasePacket::add([
            'type_id'   => PurchasePacket::TYPE_ID_BUY,
            'amount'    => $amount,
            'user_id'   => Yii::$app->user->id,
            'packet_id' => $p->id,
        ]);

        // списываю деньги
        $data = UserBill::getInternalCurrencyWallet(Currency::MARKET, Yii::$app->user->id);
        /** @var \common\models\piramida\Wallet $w */
        $w  = $data['wallet'];
        $cio = CurrencyIO::findFromInt(Currency::MARKET);
        $wTo = Wallet::findOne($cio->main_wallet);
        $t = $w->move2(
            $wTo,
            $amount,
            Json::encode(['Докупка #{id} "Пакета возможностей" на шопинг-расходы до {count} МБ', ['count' => $this->count, 'id' => $PurchasePacket->id]])
        );
        $PurchasePacket->tx_id = $t['transaction']['id'];
        $PurchasePacket->save();

        // раскидываю рефералку
        \shop\models\forms\CabinetMarkeyBuy::referal($PurchasePacket, $wTo, Yii::$app->user->id, $amount);

        // перевожу пакет
        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
        if ($walletMAR->amount > 0) {
            $t = $walletMAR->move2(
                $walletMain,
                $walletMAR->amount,
                Json::encode(['Сжигание баланса пакета по условиям исчерпания возможности его использования', []])
            );
        }
        $t = $walletMain->move2(
            $walletMAR,
            Currency::getAtomFromValue($this->count, Currency::MARKET),
            Json::encode(['Докупка #{id} "Пакета возможностей" на шопинг-расходы до {count} МБ', ['count' => $this->count, 'id' => $PurchasePacket->id]])
        );

        // сохраняю параметры
        Yii::$app->user->identity->market_till = time() + (60*60*24*30);
        Yii::$app->user->identity->market_packet = Currency::getAtomFromValue($this->count, Currency::MARKET);
        Yii::$app->user->identity->save();

        return 1;
    }

}