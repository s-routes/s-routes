<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopAdd extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'shop_all';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['type', 'required'],
            ['type', 'integer'],

            ['link', 'required'],
            ['link', 'string'],
            ['link', 'url'],

            ['description', 'required'],
            ['description', 'string'],
        ];

        return $rules;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        parent::save($runValidation, $attributeNames);

        $managerList = Yii::$app->authManager->getUserIdsByRole('role_shop_manager');
        foreach ($managerList as $id) {
            $u = UserAvatar::findOne($id);
            if (!Application::isEmpty($u->telegram_shop_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegramShop;
                $rows = [
                    'Добавлен магазин',
                    'Ссылка:'.$this->link,
                    'Описание:'.$this->description,
                ];
                $telegram->sendMessage(['chat_id' => $u->telegram_shop_chat_id, 'text' =>  join("\n", $rows) ]);
            }
        }

        return 1;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'link'        => 'Ссылка',
            'description' => 'Описание',
            'type'        => 'Тип',
        ];
    }
}
