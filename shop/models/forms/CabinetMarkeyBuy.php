<?php

namespace shop\models\forms;

use common\models\avatar\UserBill;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\Packet;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\PurchasePacket;
use common\models\ShopReferalTransaction;
use common\models\ShopUserPartner;
use common\models\UserAvatar;
use common\models\UserBill2;
use common\models\UserDocument;
use cs\Application;
use cs\base\FormActiveRecord;
use cs\services\BitMask;
use Dompdf\Dompdf;
use yii\base\Exception;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\helpers\Json;
use Yii;

/**
 */
class CabinetMarkeyBuy extends Model
{
    public $count;

    public function rules()
    {
        return [
            ['count', 'required'],
            ['count', 'integer'],
            ['count', 'validateWallet'],
        ];
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            $data = UserBill::getInternalCurrencyWallet(Currency::MARKET, Yii::$app->user->id);
            /** @var \common\models\piramida\Wallet $w */
            $w  = $data['wallet'];
            $price = $this->getPrice();
            if ($w->amount < Currency::getAtomFromValue($price, Currency::MARKET)) {
                $this->addError($a, 'У Вас не хватает средств на покупку');
                return;
            }
        }
    }

    private function getPrice()
    {
        $price = 0;
        if ($this->count == 100000) $price = 3000;
        if ($this->count == 300000) $price = 6000;
        if ($this->count == 500000) $price = 9000;
        if ($this->count == 1000000) $price = 18000;

        return $price;
    }

    public function action()
    {
        $price = $this->getPrice();
        $amount = Currency::getAtomFromValue($price, Currency::MARKET);

        $p = Packet::findOne(['price' => $amount]);

        // записываю покупку пакета
        $PurchasePacket = PurchasePacket::add([
            'type_id'   => PurchasePacket::TYPE_ID_BUY,
            'amount'    => $amount,
            'user_id'   => Yii::$app->user->id,
            'packet_id' => $p->id,
        ]);

        // списываю деньги
        $data = UserBill::getInternalCurrencyWallet(Currency::MARKET, Yii::$app->user->id);
        /** @var \common\models\piramida\Wallet $w */
        $w  = $data['wallet'];
        $cio = CurrencyIO::findFromInt(Currency::MARKET);
        $wTo = Wallet::findOne($cio->main_wallet);
        $t = $w->move2(
            $wTo,
            $amount,
            Json::encode(['Покупка #{id} "Пакета возможностей" на шопинг-расходы до {count} МБ', ['count' => $this->count, 'id' => $PurchasePacket->id]])
        );

        $PurchasePacket->tx_id = $t['transaction']['id'];
        $PurchasePacket->save();

        // рефералка
        self::referal($PurchasePacket, $wTo, Yii::$app->user->id, $amount);

        // перевожу пакет
        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
        if ($walletMAR->amount > 0) {
            $t = $walletMAR->move2(
                $walletMain,
                $walletMAR->amount,
                Json::encode(['Сжигание баланса пакета по условиям исчерпания возможности его использования', []])
            );
        }
        $market_till = time() + (60*60*24*30);
        $time = sprintf('%sг. %s Мск (GMT+3)',
            Yii::$app->formatter->asDatetime($market_till, 'php:d.m.Y'),
            Yii::$app->formatter->asDatetime($market_till, 'php:H:i')
        );
        $t = $walletMain->move2(
            $walletMAR,
            Currency::getAtomFromValue($this->count, Currency::MARKET),
            Json::encode([
                'Покупка #{id} "Пакета возможностей" на шопинг-расходы до {count} МБ. Таймер до {time}',
                [
                    'count' => $this->count,
                    'id'    => $PurchasePacket->id,
                    'time'  => $time,
                ],
            ])
        );

        // сохраняю параметры
        Yii::$app->user->identity->market_till = $market_till;
        Yii::$app->user->identity->market_packet = Currency::getAtomFromValue($this->count, Currency::MARKET);
        Yii::$app->user->identity->save();

        return 1;
    }

    /**
     * @param \common\models\PurchasePacket $PurchasePacket
     * @param \common\models\piramida\Wallet $walletOut кошелкк откуда будут браться средства на рефералку
     * @param int $uid пользователь от которого пошла рефералка
     * @param int $amount стоимость покупки от которой считаются проценты по рефералке
     * @throws Exception
     */
    public static function referal($PurchasePacket, $walletOut, $uid, $amount)
    {
        $cMar = Currency::findOne(['code' => 'MAR']);
        $levels = Yii::$app->params['shop-referal-program']['level-list2'];
        $uCurrent = $uid;
        $u1 = UserAvatar::findOne($uid);
        for ($i = 0; $i < 5; $i++) {
            $s = ShopUserPartner::findOne(['user_id' => $uCurrent]);
            if (is_null($s)) break;
            $parent_id = $s->parent_id;
            $u = \common\models\UserAvatar::findOne($parent_id);

            $data2 = UserBill2::findOne(['user_id' => $u->id, 'currency_id' => $cMar->id]);
            $w1 = Wallet::findOne($data2->wallet_id);
            if ($u['market_till'] > 0) {
                if (($u['market_packet'] > 0) and ($u['market_till'] > time()) and ($w1->amount > 0)) {
                    $dataTo = UserBill::getInternalCurrencyWallet($walletOut->currency_id, $u->id);
                    $w = $dataTo['wallet'];
                    $a = (int) ($amount * ($levels[$i]/100));
                    $t_to = $walletOut->move2($w, $a, Json::encode([
                        'Реферальное начисление от {from} к {to}',
                        [
                            'from' => $uid,
                            'to'   => $u->id,
                        ],
                    ]));

                    $ShopReferalTransaction = ShopReferalTransaction::add([
                        'request_id'     => $PurchasePacket->id,
                        'type_id'        => 3,
                        'currency_id'    => $walletOut->currency_id,
                        'level'          => $i + 1,
                        'from_uid'       => $uid,
                        'to_uid'         => $u->id,
                        'amount'         => $a,
                        'transaction_id' => $t_to['transaction']['id'],
                    ]);

                    // уведомляю по телеграму
                    $userTelegram = $u;
                    $uC = UserAvatar::findOne($u1);
                    if (!Application::isEmpty($userTelegram->telegram_shop_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegramShop;
                        try {
                            $telegram->sendMessage([
                                'chat_id' => $userTelegram->telegram_chat_id,
                                'text'    => sprintf('Вам начислено реферальное вознаграждение %s МБ. Ваш реферал: USER ID-%s, линия %s',
                                    Currency::getValueFromAtom($a, $walletOut->currency_id),
                                    $uC->getAddress(),
                                    $i + 1
                                ),
                            ]);
                        } catch (\Exception $e) {
                            Yii::warning($e->getMessage(), 'avatar\shop\models\forms\CabinetMarkeyBuy::referal()');
                        }
                    }
                }
            }

            // текущим становится $parent_id
            $uCurrent = $parent_id;
        }
    }

}