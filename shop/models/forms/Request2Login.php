<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopTempProduct;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request2Login extends \iAvatar777\services\FormAjax\Model
{
    const TYPE_ID_CABINET_DATA = 1;
    const TYPE_ID_USER_DATA = 2;
    const TYPE_ID_MY_CABINET = 3;

    public $type_id;

    /** @var string */
    public $name;

    /** @var string */
    public $phone;

    /** @var string */
    public $email;

    /** @var string */
    public $whatsapp;

    /** @var string */
    public $telegram;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['type_id', 'required'],
            ['type_id', 'integer'],

            ['name', 'trim'],
            ['name', 'string'],

            ['phone', 'trim'],
            ['phone', 'string'],

            ['telegram', 'trim'],
            ['telegram', 'string'],

            ['whatsapp', 'trim'],
            ['whatsapp', 'string'],

            ['email', 'trim'],
            ['email', 'string'],
            ['email', 'email'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name'     => 'Имя',
            'phone'    => 'Телефон',
            'email'    => 'Email',
            'telegram' => 'Имя пользователя Telegram',
            'whatsapp' => 'WhatsApp телефон',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);

        switch ($this->type_id) {
            case self::TYPE_ID_CABINET_DATA:
                $data['user']['type_id'] = self::TYPE_ID_CABINET_DATA;
                /** @var UserAvatar $user */
                $user = Yii::$app->user->identity;
                $data['user']['name'] = $user->shop_fio_contact;
                $data['user']['phone'] = $user->shop_phone;
                $data['user']['email'] = $user->email;
                $data['user']['telegram'] = $user->shop_telegram;
                $data['user']['whatsapp'] = $user->shop_whatsapp;
                break;
            case self::TYPE_ID_USER_DATA:
                $data['user']['type_id'] = self::TYPE_ID_USER_DATA;
                $data['user']['name'] = $this->name;
                $data['user']['phone'] = $this->phone;
                $data['user']['email'] = $this->email;
                $data['user']['telegram'] = $this->telegram;
                $data['user']['whatsapp'] = $this->whatsapp;
                break;
            case self::TYPE_ID_MY_CABINET:
                $data['user']['type_id'] = self::TYPE_ID_MY_CABINET;
                $data['user']['name'] = '';
                $data['user']['phone'] = '';
                $data['user']['email'] = '';
                $data['user']['telegram'] = '';
                $data['user']['whatsapp'] = '';
                break;
        }

        Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);

        return 1;
    }
}
