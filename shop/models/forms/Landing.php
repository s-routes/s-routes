<?php

namespace shop\models\forms;

use common\models\SendLetter;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;


/**
 * Registration is the model behind the contact form.
 */
class Landing extends \iAvatar777\services\FormAjax\Model
{
    public $name;
    public $email;
    public $text;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['email', 'trim'],

            ['name', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['name', 'trim'],

            ['text', 'required', 'message' => Yii::t('c.0uMdJb0e0n', 'Это поле должно быть заполнено обязательно')],
            ['text', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'text' => 'Ваше сообщение',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $letter = SendLetter::add([
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->text,
        ]);

        Application::mail(Yii::$app->params['landingEmail'], 'Письмо с лендинга магазина', 'landing', [
            'name'  => $this->name,
            'email' => $this->email,
            'text'  => $this->text,
        ]);

        $role_admin = Yii::$app->authManager->getUserIdsByRole('role_admin');
        $role_shop_buh = Yii::$app->authManager->getUserIdsByRole('role_shop_buh');
        $role_shop_manager = Yii::$app->authManager->getUserIdsByRole('role_shop_manager');

        $admins = ArrayHelper::merge($role_admin, $role_shop_buh);
        $admins = ArrayHelper::merge($admins, $role_shop_manager);
        $admins = array_unique($admins);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        foreach ($admins as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!\cs\Application::isEmpty($u->telegram_chat_id)) {
                /** @var \common\models\UserAvatar $uCurrent */
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => 'Сообщение с лендинга магазина от пользователя ' . $letter->email . ': ' . "\n". $this->text]);
            }
        }

        return true;
    }
}
