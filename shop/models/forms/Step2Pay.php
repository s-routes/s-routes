<?php

namespace shop\models\forms;

use common\models\avatar\UserBill;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\SendLetter;
use common\models\shop\RequestAdd;
use common\models\ShopRequest;
use common\models\ShopRequestAdd;
use common\models\ShopRequestProduct;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Step2Pay extends Model
{
    /** @var integer */
    public $id;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['id', 'required'],
            ['id', 'integer'],
            ['id', 'validateWallet'],
            ['id', 'validateMarket'],
        ];

        return $rules;
    }

    public function validateMarket($a, $p)
    {
        if (!$this->hasErrors()) {
            $data = Yii::$app->session->get(\shop\models\forms\PhoneStep1::KEY);
            $price = $data['price'];

            $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();

            if ($walletMAR->amount < \common\models\piramida\Currency::getAtomFromValue($price, \common\models\piramida\Currency::MARKET)) {
                $this->addError($a, 'У Вас израсходован лимит пакета маркет-баллов. Купить подписку на новый, или сделать доплату за пакет большего объема маркет-баллов Вы можете здесь => ' . Html::a('https://topmate.one/cabinet-market/index', 'https://topmate.one/cabinet-market/index'));
                return;
            }
            if (\Yii::$app->user->identity->market_till > 0) {
                if (\Yii::$app->user->identity->market_till < time()) {
                    $this->addError($a, 'У вашего пакета вышел срок давности');
                    return;
                }
            }
        }
    }

    public function validateWallet($a, $p)
    {
        if (!$this->hasErrors()) {
            // Списываю деньги с счета
            $data = UserBill::getInternalCurrencyWallet(Currency::MARKET);

            /** @var \common\models\piramida\Wallet $wallet */
            $wallet = $data['wallet'];

            // вычисляю сумму заказа
            $data = Yii::$app->session->get(\shop\models\forms\PhoneStep1::KEY);
            $sum = $data['price'];

            if ($sum > $wallet->getAmountWithDecimals()) {
                $this->addError($a, 'В кошельке недостаточно средств для оплаты');
                return;
            }
        }
    }

    public function action()
    {
        $data = Yii::$app->session->get(\shop\models\forms\PhoneStep1::KEY);
        $sum = $data['price'];
        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::RUB);
        $sumRub = bcmul($sum, bcpow(10, $c->decimals));

        $request = ShopRequest::add([
            'price'            => $sumRub,
            'currency_id'      => $c->id,
            'currency_id_paid' => \common\models\avatar\Currency::MARKET,
            'type'             => 2,
            'info'             => Json::encode([
                'type' => $data['type'],
            ]),
            'created_at'       => time(),
            'user_id'          => Yii::$app->user->id,
        ]);

        // Удаляю переменную сессии
        Yii::$app->session->remove(\shop\models\forms\PhoneStep1::KEY);

        // Списываю деньги с счета
        $data = UserBill::getInternalCurrencyWallet(Currency::MARKET);
        /** @var \common\models\piramida\Wallet $wallet */
        $wallet = $data['wallet'];
        $CIO = CurrencyIO::findOne(['currency_int_id' => Currency::MARKET]);
        $cIntMarket = Currency::findOne($CIO->currency_int_id);
        $sumMarket = bcmul($sum, bcpow(10, $cIntMarket->decimals));
        $t = $wallet->move2(
            $CIO->main_wallet,
            $sumMarket,
            Json::encode([
                'Оплата заказа #{id}',
                [
                    'id'    => $request->id,
                ],
            ])
        );

        // Списываю пакет
        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
        $t = $walletMAR->move2(
            $walletMain,
            $sumMarket,
            Json::encode([
                'Оплата заказа #{id}',
                [
                    'id'    => $request->id,
                ],
            ])
        );

        // Записываю остаток пакета
        $request->ostatok = $walletMAR->amount;
        $request->save();

        // Отправляю уведомление для бухгалтеров и менеджеров
        $this->sendNotification($request);

        // Ставлю статус заказа "оплачен"
        $request->is_paid = 1;
        $request->save();

        return ['request' => $request];
    }

    /**
     * @param \common\models\ShopRequest $request
     * @throws \yii\base\Exception
     */
    public function sendNotification($request)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $role_shop_buh = Yii::$app->authManager->getUserIdsByRole('role_shop_buh');
        $role_shop_manager = Yii::$app->authManager->getUserIdsByRole('role_shop_manager');

        $all = ArrayHelper::merge($role_shop_buh, $role_shop_manager);
        $all = array_unique($all);

        foreach ($all as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_shop_chat_id)) {
                /** @var \common\models\UserAvatar $uCurrent */
                $link1 = Url::to(['manager-request-list/view', 'id' => $request->id], true);
                $link2 = Url::to(['buh-request-list/view', 'id' => $request->id], true);
                $language = (Application::isEmpty($u->language)) ? 'ru' : $u->language;
                try {
                    $telegram->sendMessage(['chat_id' => $u->telegram_shop_chat_id, 'text' => 'Создан заказ #'. $request->id  . "\n" .  "\n" . 'Ссылка на заказ: ' . $link1  . "\n" . 'Ссылка на заказ: ' . $link2]);
                } catch (\Exception $e) {

                }
            }
        }
    }
}
