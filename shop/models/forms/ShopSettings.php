<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class ShopSettings extends \iAvatar777\services\FormAjax\ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['shop_fio_contact', 'required'],
            ['shop_fio_contact', 'string'],

            ['shop_address', 'required'],
            ['shop_address', 'string'],

            ['shop_phone', 'required'],
            ['shop_phone', 'string'],

            ['shop_dop', 'required'],
            ['shop_dop', 'string'],

            ['shop_whatsapp', 'string'],

            ['shop_telegram', 'string'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'shop_fio_contact' => 'ФИО контактного лица',
            'shop_address'     => 'Адрес доставки',
            'shop_phone'       => 'Контактный телефон',
            'shop_dop'         => 'Дополнительная информация и пожелания по доставке',
            'shop_telegram'    => 'Имя пользователя Telegram',
            'shop_whatsapp'    => 'WhatsApp телефон',
        ];
    }
}
