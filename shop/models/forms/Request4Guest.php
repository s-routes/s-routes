<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\piramida\Currency;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopRequestProduct;
use common\models\ShopTempProduct;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserSeed;
use common\services\Security;
use common\services\Subscribe;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request4Guest extends \iAvatar777\services\FormAjax\Model
{
    const TYPE_ID_AUTO_PASSWORD = 1;
    const TYPE_ID_USER_PASSWORD = 2;

    public $email;

    public $type_id;

    /** @var string */
    public $password1;
    public $password2;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['email', 'required'],
            ['email', 'string'],
            ['email', 'email'],

            ['type_id', 'required'],
            ['type_id', 'integer'],

            ['password1', 'trim'],
            ['password1', 'string'],

            ['password2', 'trim'],
            ['password2', 'string'],
            ['password2', 'compare', 'compareAttribute' => 'password1', 'operator' => '==', 'message' => Yii::t('c.0uMdJb0e0n', 'Пароли должны совпадать')],
        ];

        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'password1' => 'Введите пароль',
            'password2' => 'Введите пароль еще раз для проверки',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $email = $this->email;
        $password = '';
        if ($this->type_id == self::TYPE_ID_AUTO_PASSWORD) {
            $password = Security::generateRandomString(10, Security::ALGORITM_NUMS);
            $user = UserAvatar::registrationCreateUser($email, $password);
            $userRegistration = \avatar\models\UserRegistration::add($user->id);
            $ret = Subscribe::sendArray([$email], 'Подтверждение регистрации', 'registration_order_password', [
                'url'      => Url::to([
                    'auth/registration-order-activate',
                    'code' => $userRegistration->code
                ], true),
                'user'     => $user,
                'datetime' => $userRegistration->date_finish,
                'password' => $password,
            ]);
        } else {
            $password = $this->password1;
            $user = UserAvatar::registrationCreateUser($email, $password);
            $userRegistration = \avatar\models\UserRegistration::add($user->id);
            $ret = Subscribe::sendArray([$email], 'Подтверждение регистрации', 'registration_order', [
                'url'      => Url::to([
                    'auth/registration-order-activate',
                    'code' => $userRegistration->code
                ], true),
                'user'     => $user,
                'datetime' => $userRegistration->date_finish,
            ]);
        }

        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        $data['is_guest'] = true;
        $data['user_id'] = $user->id;
        Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);

        return 1;
    }
}
