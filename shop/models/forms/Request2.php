<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopTempProduct;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request2 extends \iAvatar777\services\FormAjax\Model
{
    const TYPE_ID_CABINET_DATA = 1;
    const TYPE_ID_USER_DATA = 2;

    /** @var string */
    public $name;
    public $phone;
    public $email;

    /** @var string */
    public $whatsapp;

    /** @var string */
    public $telegram;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['name', 'required'],
            ['name', 'trim'],
            ['name', 'string'],

            ['phone', 'required'],
            ['phone', 'trim'],
            ['phone', 'string'],

            ['email', 'required'],
            ['email', 'trim'],
            ['email', 'string'],
            ['email', 'email'],

            ['telegram', 'trim'],
            ['telegram', 'string'],

            ['whatsapp', 'trim'],
            ['whatsapp', 'string'],

        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name'  => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'telegram' => 'Имя пользователя Telegram',
            'whatsapp' => 'WhatsApp телефон',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        $data['user']['type_id'] = self::TYPE_ID_USER_DATA;
        $data['user']['name'] = $this->name;
        $data['user']['phone'] = $this->phone;
        $data['user']['email'] = $this->email;
        $data['user']['whatsapp'] = $this->whatsapp;
        $data['user']['telegram'] = $this->telegram;
        Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);

        return 1;
    }
}
