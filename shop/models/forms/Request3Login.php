<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request3Login extends \iAvatar777\services\FormAjax\Model
{
    const TYPE_ID_CABINET_DATA = 1;
    const TYPE_ID_USER_DATA = 2;
    const TYPE_ID_MY_CABINET = 3;

    public $type_id;

    /** @var string */
    public $address;
    public $dop;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['type_id', 'required'],
            ['type_id', 'integer'],

            ['address', 'trim'],
            ['address', 'string'],

            ['dop', 'trim'],
            ['dop', 'string'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'address' => 'Адрес доставки (указать, что это: пункт выдачи/самовывоза или адрес доставки для курьера)',
            'dop'     => 'Дополнительная информация и пожелания по доставке',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);

        switch ($this->type_id) {
            case self::TYPE_ID_CABINET_DATA:
                $data['delivery']['type_id'] = self::TYPE_ID_CABINET_DATA;
                $data['delivery']['address'] = Yii::$app->user->identity->shop_address;
                $data['delivery']['dop'] = Yii::$app->user->identity->shop_dop;
                break;
            case self::TYPE_ID_USER_DATA:
                $data['delivery']['type_id'] = self::TYPE_ID_USER_DATA;
                $data['delivery']['address'] = $this->address;
                $data['delivery']['dop'] = $this->dop;
                break;
            case self::TYPE_ID_MY_CABINET:
                $data['delivery']['type_id'] = self::TYPE_ID_MY_CABINET;
                $data['delivery']['address'] = '';
                $data['delivery']['dop'] = '';
                break;
        }

        Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);

        return 1;
    }
}
