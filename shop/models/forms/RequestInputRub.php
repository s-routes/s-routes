<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 12.04.2016
 * Time: 16:13
 */

namespace common\models\forms;

use common\models\avatar\UserBill;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 *
 * @property int        id
 * @property int        billing_id
 * @property int        created_at
 * @property int        amount
 * @property int        user_id
 * @property int        tx_int_id
 */
class RequestInputRub extends \iAvatar777\services\FormAjax\ActiveRecord
{
    /**
     * @param $fields
     *
     * @return static
     */
    public static function add($fields)
    {
        $i = new static($fields);
        $i->save();
        $i->id = self::getDb()->lastInsertID;

        return $i;
    }
}