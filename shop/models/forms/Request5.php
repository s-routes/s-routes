<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request5 extends \iAvatar777\services\FormAjax\Model
{
    /** @var string */
    public $link;
    public $price;
    public $currency_id;
    public $delivery;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['link', 'required'],
            ['link', 'trim'],
            ['link', 'url'],

            ['price', 'required'],
            ['price', 'trim'],
            ['price', 'double'],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],

            ['delivery', 'required'],
            ['delivery', 'string'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'link'        => 'Ссылка',
            'price'       => 'Цена',
            'currency_id' => 'Валюта',
            'delivery'    => 'Доставка',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        ShopRequest::add([
            'link'        => $this->link,
            'price'       => (int)($this->price * 100),
            'currency_id' => $this->currency_id,
            'delivery'    => $this->delivery,
            'user_id'     => Yii::$app->user->id,
        ]);

        return 1;
    }
}
