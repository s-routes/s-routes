<?php

namespace shop\models\forms;

use common\models\SendLetter;
use common\models\ShopTempProduct;
use common\models\UserAvatar;
use cs\Application;
use cs\services\Str;
use cs\services\VarDumper;
use Yii;
use yii\base\Model;
use yii\helpers\StringHelper;


/**
 */
class Login extends \iAvatar777\services\FormAjax\Model
{
    public $username;
    public $password;

    private $_user = false;

    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'email'],
            ['username', 'validateUser'],
            ['username', 'validateBlocked'],

            ['password', 'required'],
            ['password', 'string', 'min' => 3],
            ['password', 'validatePassword'],
        ];
    }

    private function validatePhone($amount)
    {
        $symbols = Str::getChars($amount);
        foreach ($symbols as $char) {
            if (!in_array($char, ['0','1','2','3','4','5','6','7','8','9'])) return false;
        }
        return true;
    }

    public function validateUser($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (StringHelper::startsWith($this->username, '+7')) {
                // Это телефон
                // +79252374501
                if (strlen($this->username) != 12) {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'В телефоне должно быть 12 символов и никаких спецсимволов'));
                    return;
                }
                if (!$this->validatePhone(substr($this->username, 1))) {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'В телефоне должны быть только цифры'));
                    return;
                }
                $user = $this->getUserPhone();
            } else {
                if (strpos($this->username, '@')) {
                    if (!filter_var($this->username, FILTER_VALIDATE_EMAIL)) {
                        $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Не верный формат email'));
                        return;
                    }
                } else {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Email должен содержать').' @');
                    return;
                }
                try {
                    $user = $this->getUser();
                } catch (\Exception $e) {
                    $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Пользователя нет'));
                    return;
                }
            }

            if (is_null($user)) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Пользователя нет'));
                return;
            }
            if ($user->password == '') {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Вы  не завели себе пароль'));
                return;
            }
            if ($user->email_is_confirm != 1) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Пользователь не активирован'));
                return;
            }
        }
    }

    public function validateBlocked($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if ($user->mark_deleted == 1) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Ваш аккаунт заблокирован по подозрению в нарушении правил системы. Обратитесь в службу поддержки.'));
                return;
            }
        }
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (is_null($user)) {
                return;
            }

            if ($user->validatePassword($this->password) == false) {
                $this->addError($attribute, Yii::t('c.Q2vFnrwWB1', 'Не верный пароль'));
                return;
            }
        }
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Yii::$app->user->login($this->getUser());

        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        $data['is_guest'] = false;
        Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);

        return 1;
    }


    public function attributeLabels()
    {
        return [
            'username' => Yii::t('c.Q2vFnrwWB1', 'Логин/email'),
            'password' => Yii::t('c.Q2vFnrwWB1', 'Пароль'),
        ];
    }

    /**
     * Finds user by [email]]
     *
     * @return \common\models\UserAvatar | null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = UserAvatar::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Finds user by [[phone]]
     *
     * @return \common\models\UserAvatar | null
     */
    public function getUserPhone()
    {
        if ($this->_user === false) {
            $this->_user = UserAvatar::findOne(['phone' => substr($this->username,1)]);
        }

        return $this->_user;
    }
}
