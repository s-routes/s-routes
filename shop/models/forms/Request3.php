<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request3 extends \iAvatar777\services\FormAjax\Model
{
    const TYPE_ID_CABINET_DATA = 1;
    const TYPE_ID_USER_DATA = 2;

    /** @var string */
    public $address;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['address', 'required'],
            ['address', 'trim'],
            ['address', 'string'],
        ];

        return $rules;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'address'        => 'Адрес доставки',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);

        $data['delivery']['type_id'] = self::TYPE_ID_USER_DATA;
        $data['delivery']['address'] = $this->address;

        Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);

        return 1;
    }
}
