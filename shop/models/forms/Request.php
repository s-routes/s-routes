<?php

namespace shop\models\forms;

use common\models\avatar\Currency;
use common\models\avatar\CurrencyLink;
use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\ShopTempProduct;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class Request extends \iAvatar777\services\FormAjax\Model
{
    public static $keySession = 'shop-request';

    /** @var string */
    public $link;
    public $price;
    public $count;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['link', 'required'],
            ['link', 'trim'],
            ['link', 'url'],

            ['price', 'required'],
            ['price', 'trim'],
            ['price', 'double'],
            ['price', 'validatePrice'], // проверка на 2 десятичных знаков

            ['count', 'required'],
            ['count', 'integer', 'min' => 1],
        ];

        return $rules;
    }

    public function validatePrice($a, $p)
    {
        if (!$this->hasErrors()) {
            $v = $this->price;
            if ((float)$v == 0) {
                $this->addError($a, 'Цена не может быть 0');
                return;
            }
            if ((float)$v < 0) {
                $this->addError($a, 'Цена не может быть отрицательной');
                return;
            }
            if (((float)($this->normalizePrice($v))) != ((float)$v)) {
                $this->addError($a, 'Разрядность монеты 2 знака');
                return;
            }
        }
    }


    /**
     * Обрезает лишние знаки $this->getCurrency()->decimals_view
     * Убирает лишние нули сзади
     */
    public function normalizePrice($v1)
    {
        $c = Currency::findOne(Currency::RUB);
        $link = CurrencyLink::findOne(['currency_ext_id' => $c->id]);
        $c2 = \common\models\piramida\Currency::findOne($link->currency_int_id);
        $a = explode('.', $v1);
        $v = '0';
        if (count($a) == 1) {
            $v =  $this->price;
        }
        if (count($a) == 2) {
            $v = $a[0] . '.' . substr($a[1], 0, $c2->decimals_view_shop);
        }

        return $v;
    }

    /**
     * Убирает лишние нули сзади
     * @param $v
     * @return mixed
     */
    public function normalize($v)
    {
        $a = explode('.', $v);
        if (count($a) == 1) {
            return $v;
        }
        $a = Str::getChars($v);
        $a = array_reverse($a);
        $c1 = 0;
        foreach ($a as $c) {
            if ($c == '0') {
                $c1++;
                continue;
            } else {
                break;
            }
        }
        if ($a[$c1] == '.') $c1++;

        $ret = [];
        for($i = $c1; $i < count($a); $i++) {
            $ret[] = $a[$i];
        }
        $ret = array_reverse($ret);

        return join('', $ret);
    }


    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'link'        => 'Добавьте полную ссылку из поисковой строки браузера на товар или услугу',
            'price'       => 'Стоимость (с учетом доставки)',
            'count'       => 'Желаемое кол-во',
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        if ($data['is_guest'] == false) {
            $user_id = Yii::$app->user->id;
        } else {
            $user_id = $data['user_id'];
        }

        $product = ShopTempProduct::add([
            'link'        => $this->link,
            'price'       => $this->price,
            'count'       => $this->count,
            'user_id'     => $user_id,
        ]);

        // считаю сумму
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        if ($data['is_guest'] == false) {
            $user_id = Yii::$app->user->id;
        } else {
            $user_id = $data['user_id'];
        }
        $sum = 0;
        $rows = ShopTempProduct::find()->where(['user_id' => $user_id])->all();
        foreach ($rows as $row) {
            $sum += $row['price'] * $row['count'];
        }

        return [
            'product'      => $product,
            'sum'          => $sum,
            'sumFormatted' => Yii::$app->formatter->asDecimal($sum, 2),
        ];
    }
}
