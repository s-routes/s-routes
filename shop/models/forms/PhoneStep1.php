<?php

namespace shop\models\forms;

use common\models\CurrencyIO;
use common\models\SendLetter;
use common\models\ShopRequest;
use common\models\UserAvatar;
use common\models\UserSeed;
use common\services\Security;
use cs\Application;
use cs\services\Str;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 */
class PhoneStep1 extends \iAvatar777\services\FormAjax\Model
{
    const KEY = 'request-phone';

    public $price;
    public $type;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        $rules = [
            ['type', 'required'],
            ['type', 'integer'],

            ['price', 'required'],
            ['price', 'string'],
        ];

        return $rules;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        Yii::$app->session->set(self::KEY, [
            'type'  => $this->type,
            'price' => $this->price,
        ]);

        return 1;
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'price'       => 'Сумма',
            'type'        => 'Тип',
        ];
    }
}
