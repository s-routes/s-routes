<?php

/** $this \yii\web\View  */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Приобрести пакет для активации аккаунта, получения возможности делать покупки (в пределах выбираемых вами лимитов)';

$this->registerJs(<<<JS
$('.buttonPay').click(function (e) {
    window.location = '/cabinet-market/buy?count=' + $(this).data('count');
});
JS
);
$time = sprintf('%sг. %s Мск (GMT+3)',
    Yii::$app->formatter->asDatetime(Yii::$app->user->identity->market_till, 'php:d.m.Y'),
    Yii::$app->formatter->asDatetime(Yii::$app->user->identity->market_till, 'php:H:i')
);

$this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>

<!-- Large modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                $id = 91;
                $page = \common\models\exchange\Page::findOne($id);

                $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                if (is_null($pageLang)) {
                    $name = $page->name;
                    $content = $page->content;
                } else {
                    $name = $pageLang->name;
                    $content = $pageLang->content;
                }
                ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $name ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= Html::encode($this->title) ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>

        <?php

        $c22 = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::MARKET);

        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $billMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id]);
        $walletMAR = \common\models\piramida\Wallet::findOne($billMAR->wallet_id);
        $c22value = Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($walletMAR->amount, \common\models\piramida\Currency::MARKET), 2);

        // если пакет еще не куплен, стартовый пакет
        if (Yii::$app->user->identity->market_till == 0) {
            $typeView = 1;
        } else {
            // если купленный пакет просрочен или кончились деньги
            if ((Yii::$app->user->identity->market_till < time()) || ($walletMAR->amount == 0)) {
                $typeView = 2;
            } else {
                // Все работает
                $typeView = 3;
            }
        }
        ?>

        <?php if ($typeView == 1) {  ?>
            <p class="text-center lead">Пакет активирован, таймер истечения: подарочный пакет возможностей</p>
        <?php } ?>
        <?php if ($typeView == 2) {  ?>
            <p class="text-center lead">Срок действия пакета закончился <?= $time ?></p>
        <?php } ?>
        <?php if ($typeView == 3) {  ?>
            <p class="text-center lead">Пакет активирован, таймер истечения <?= $time ?></p>
        <?php }   ?>
    </div>

    <div class="col-lg-8">
        <h2>Купить "Пакет возможностей"</h2>

    </div>


    <div class="col-lg-8 col-lg-offset-2" style="margin-bottom: 50px;">
        <div class="col-lg-3 col-md-6 col-sm-6" >
            <p><button class="btn btn-success buttonPay" data-count="100000" style="width: 100px;" title="Стоимость 3 000 MARKET" data-toggle="tooltip">100 000</button></p>
            <p>Стоимость 3 000 MARKET</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <p><button class="btn btn-success buttonPay" data-count="300000" style="width: 100px;" title="Стоимость 6 000 MARKET" data-toggle="tooltip">300 000</button></p>
            <p>Стоимость 6 000 MARKET</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <p><button class="btn btn-success buttonPay" data-count="500000" style="width: 100px;" title="Стоимость 9 000 MARKET" data-toggle="tooltip">500 000</button></p>
            <p>Стоимость 9 000 MARKET</p>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <p><button class="btn btn-success buttonPay" data-count="1000000" style="width: 100px;" title="Стоимость 18 000 MARKET" data-toggle="tooltip">1 000 000</button></p>
            <p>Стоимость 18 000 MARKET</p>
        </div>
    </div>

    <div class="col-lg-12" style="margin-bottom: 50px;">

        <h2 class="page-header">История</h2>
        <?php \yii\widgets\Pjax::begin(); ?>

        <?php
        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id])->getWallet();

        $this->registerJs(<<<JS

$('[data-toggle="tooltip"]').tooltip();

JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\piramida\Operation::find()
                    ->where(['wallet_id' => $walletMAR->id])
                    ->orderBy(['datetime' => SORT_DESC])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'        => 'OID',
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '12%',
                        ]),
                    ],
                    'content'       => function (\common\models\piramida\Operation $item) {
                        $address = $item->getAddress();
                        $addressShort = $item->getAddressShort();

                        return Html::tag('code', $addressShort, [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Операция. Нажми чтобы скопировать',
                        ]);
                    },
                ],
                [
                    'header'        => 'TID',
                    'attribute'     => 'id',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '12%',
                        ]),
                    ],
                    'content'       => function (\common\models\piramida\Operation $item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'transaction_id');
                        if (is_null($v)) return '';
                        $transaction = new \common\models\piramida\Transaction(['id' => $v]);
                        $address = $transaction->getAddress();
                        $addressShort = $transaction->getAddressShort();

                        return Html::tag('code', $addressShort, [
                            'data'  => [
                                'toggle'         => 'tooltip',
                                'clipboard-text' => $address,
                            ],
                            'class' => 'buttonCopy',
                            'title' => 'Транзакция. Нажми чтобы скопировать',
                        ]);
                    },
                ],
                [
                    'header'        => 'Тип',
                    'attribute'     => 'type',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '5%',
                        ]),
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'type', 0);
                        if ($v == 0) return '';
                        if ($v == 2) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-save', 'style' => 'color: #57b257']);
                        if ($v == 1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-import', 'style' => 'color: #57b257']);
                        if ($v == -1) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-export', 'style' => 'color: #d54d49']);
                        if ($v == -2) return Html::tag('span', null, ['class' => 'glyphicon glyphicon-open', 'style' => 'color: #d54d49']);
                        return '';
                    },
                ],
                [
                    'header'         => 'сумма',
                    'attribute'      => 'amount',
                    'headerOptions'  => [
                        'style' => Html::cssStyleFromArray([
                            'width'      => '10%',
                            'text-align' => 'right',
                        ]),
                    ],
                    'contentOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'text-align' => 'right',
                        ]),
                        'nowrap' => 'nowrap',
                    ],
                    'content'        => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'amount', 0);
                        if ($v == 0) return 0;
                        $v = $v / pow(10, 2);
                        $color = 1;
                        $prefix = '';
                        if (in_array($item['type'], [1, 2])) {
                            $prefix = '+';
                            $color = 'green';
                        }
                        if (in_array($item['type'], [-1, -2])) {
                            $prefix = '-';
                            $color = 'red';
                        }

                        return
                            Html::tag('span', $prefix . Yii::$app->formatter->asDecimal($v, 2), ['style' => 'color: ' . $color]);
                    },
                ],
                [
                    'header'        => Yii::t('c.BBXvjwPu8f', 'Время'),
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '10%',
                        ]),
                    ],
                    'content'       => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'datetime', 0);
                        if ($v == 0) return '';
                        $v = (int)($v / 1000);

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    },
                ],

                [
                    'header'        => Yii::t('c.BBXvjwPu8f', 'Комментарий'),
                    'attribute'     => 'comment',
                    'headerOptions' => [
                        'style' => Html::cssStyleFromArray([
                            'width' => '55%',
                        ]),
                    ],
                    'format' => 'html',
                    'content'       => function ($item) {
                        if (\yii\helpers\StringHelper::startsWith($item['comment'], '[')) {
                            $data = \yii\helpers\Json::decode($item['comment']);
                            return Yii::t('c.GGWWLBoXB4', $data[0], $data[1]);
                        } else {
                            return $item['comment'];
                        }
                    },
                ],
            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>



</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>