<?php

?>

<div class="panel-footer js-chat-panel-footer">
    <div class="input-group">
        <textarea id="btn-input" class="form-control input-sm" placeholder="<?= \Yii::t('c.zpiXUyz0nd', 'Напиши свое сообщение здесь...') ?>" rows="3"></textarea>
        <span class="input-group-btn">
            <button class="btn btn-warning btn-sm" id="btn-chat" style="height: 66px;">
                <?= \Yii::t('c.zpiXUyz0nd', 'Отправить') ?>
            </button>
        </span>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <p><?= \Yii::t('c.zpiXUyz0nd', 'Если необходимо') ?>:</p>

            <div style="margin-top: 10px;">
                <?= \iAvatar777\widgets\FileUpload8\FileUpload::widget([
                    'id'        => 'upload' . 1,
                    'name'      => 'upload' . 1,
                    'attribute' => 'upload' . 1,
                    'model'     => new \avatar\models\forms\ChatFile(),
                    'update'    => [],
                    'settings'  => [
                        'maxSize'           => 5 * 1000,
                        'controller'        => 'upload4',
                        'accept'            => 'image/*',
                        'button_label'      => Yii::t('c.zpiXUyz0nd', 'Прикрепите файл'),
                        'allowedExtensions' => ['jpg', 'jpeg', 'png'],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-lg-6">
            <p class="text-right">Горячая клавиша: Shift+Enter</p>
        </div>
    </div>

</div>
