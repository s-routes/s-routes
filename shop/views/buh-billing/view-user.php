<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $user \common\models\UserAvatar */

$this->title = $user->name_first;

$partner_user = $user;
?>
<div class="container">
    <div class="col-lg-12">
        <h1  class="page-header text-center"><?= Html::encode($this->title) ?></h1>

        <div class="row">
            <div class="col-lg-3">
                <p class="text-right"><img src="<?= $partner_user->getAvatar() ?>" width="80" class="img-circle"></p>
                <p class="text-right label-p"><?= $partner_user->getName2() ?></p>
                <p class="text-right label-p"><?= $partner_user->getAddress() ?></p>
            </div>
            <div class="col-lg-9">
                <?php
                // вывожу рейтинги
                $r_neg = $partner_user->assessment_negative;
                $r_pos = $partner_user->assessment_positive;

                $html_r_neg = Html::tag('span', $r_neg, ['class' => 'label label-danger', 'title' => 'Негативных оценок', 'data' => ['toggle' => 'tooltip']]);
                $html_r_pos = Html::tag('span', $r_pos, ['class' => 'label label-success', 'title' => 'Позитивных оценок', 'data' => ['toggle' => 'tooltip']]);
                ?>
                <p class="label-p"><?= $html_r_neg . $html_r_pos ?></p>

                <p class="label-p">Сделок всего в которых участвовал: <?= $partner_user->deals_count_all ?></p>
                <p class="label-p">Сделок по всем своим предложениям: <?= $partner_user->deals_count_offer ?></p>

                <p class="label-p">Среднее время сделки: <?php
                    $c = \common\models\exchange\Deal::find()
                        ->innerJoin('offer', 'offer.id = deal.offer_id')
                        ->where(['offer.user_id' => $partner_user->id])
                        ->andWhere([
                            'deal.status' => [
                                \common\models\exchange\Deal::STATUS_CLOSE,
                                \common\models\exchange\Deal::STATUS_MONEY_RECEIVED,
                            ],
                        ])
                        ->andWhere(['not', ['deal.time_finish' => null]])
                        ->select('avg(deal.time_finish - deal.time_accepted)')
                        ->scalar();

                    if (is_null($c)) {
                        echo '';
                    } else {
                        $c = (int)$c;
                        $min = (int)($c / 60);
                        $sec = $c - ($min * 60);

                        echo $min . ' мин' . ' ' . $sec . ' сек';
                    }

                    ?></p>
            </div>
        </div>
        <?php
        $v0 = Html::tag('span', 'Нет', ['class' => 'label label-default']);
        $v1 = Html::tag('span', 'Да', ['class' => 'label label-success']);
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model' =>  $user,
            'attributes' => [
                [
                    'label' => 'Ник',
                    'value' => $user->name_first,
                    'captionOptions' => ['style' => 'width: 30%;']
                ],
                'name_last:text:Имя',
                [
                    'label'  => 'USER-ID',
                    'format' => 'html',
                    'value'  => $user->getAddress(),
                ],
                'email:text:е-mail',
                'public_telegram:text:Адрес телеграмм (пользователь)',
                [
                    'label'  => 'Показывать адрес телеграмм в профиле',
                    'format' => 'html',
                    'value'  => ($user->is_show_telegram == 1) ? $v1 : $v0,
                ],
                'phone:text:Телефон',
                [
                    'label'  => 'Показывать Телефон в профиле',
                    'format' => 'html',
                    'value'  => ($user->phone == 1) ? $v1 : $v0,
                ],
                [
                    'label'  => 'Двухфакторная авторизация',
                    'format' => 'html',
                    'value'  => \cs\Application::isEmpty($user->google_auth_code) ? $v0 : $v1,
                ],
                [
                    'label' => 'Telegram Nommy подсоединение',
                    'format' => 'html',
                    'value'  => \cs\Application::isEmpty($user->telegram_username) ? $v0 : $v1,
                ],
                'telegram_username:text:Адрес телеграмм (для Nommy)',
            ],
        ]) ?>
        <p><a href="/admin-support/chat?id=<?= $user->id ?>" class="btn btn-primary">Перейти в чат поддержки</a></p>

        <?php

        $currencyBTC = \common\models\avatar\Currency::BTC;
        $currencyETH = \common\models\avatar\Currency::ETH;
        $currencyETC = \common\models\avatar\Currency::ETC;
        $currencyVVB = \common\models\avatar\Currency::VVB;

        $strCopy = Yii::t('c.TsqnzVaJuC', 'Скопировано');

        \avatar\assets\Clipboard::register($this);

        $this->registerJs(<<<JS
var clipboard = new Clipboard('.js-buttonTransactionInfo');
clipboard.on('success', function(e) {
    $(e.trigger).tooltip({
        title: '{$strCopy}',
        placement: 'bottom'
    });
    $(e.trigger).tooltip('show');
    setTimeout(function(ee) {
        $(e.trigger).tooltip('destroy');
    }, 1000);
});
JS
        );

        $this->registerJs(<<<JS

$('.rowTable').click(function(e) {
    window.location = '/cabinet-wallet/item' + '?' + 'id' + '=' + $(this).data('id');
});
JS
        );


        ?>

        <style>
            .textDecorated {
                text-decoration-line: underline;
                text-decoration-style: dotted;
            }
            .balance1 {
                font-size: 150%;
            }
        </style>
        <h1 class="page-header text-center">Сервисные счета</h1>
        <?php
        $this->registerJs(<<<JS
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
        );
        ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\avatar\UserBill::find()
                    ->innerJoin('currency', 'currency.id = user_bill.currency')
                    ->where([
                        'user_bill.user_id'      => $user->id,
                        'user_bill.mark_deleted' => 0,
                        'user_bill.currency'     => [
                            \common\models\avatar\Currency::LEGAT,
                            \common\models\avatar\Currency::PZM,
                            \common\models\avatar\Currency::ETH,
                            \common\models\avatar\Currency::USDT,
                            \common\models\avatar\Currency::BTC,
                            \common\models\avatar\Currency::LTC,
                            \common\models\avatar\Currency::DASH,
                            \common\models\avatar\Currency::NEIRO,
                            \common\models\avatar\Currency::EGOLD,
                            \common\models\avatar\Currency::TRX,
                            \common\models\avatar\Currency::BNB,
                            \common\models\avatar\Currency::DAI,
                        ],
                    ])
                    ->select(['user_bill.*'])
                    ->orderBy(['currency.sort_index' => SORT_ASC])
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'id'    => 'tableTransaction',
            ],
            'summary'      => '',
            'rowOptions'   => function (\common\models\avatar\UserBill $item) {
                $data = [
                    'data'  => [
                        'id'             => $item['id'],
                        'currency'       => $item['currency'],
                        'currencyObject' => \yii\helpers\ArrayHelper::toArray(\common\models\avatar\Currency::findOne($item['currency'])),
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];

                return $data;
            },
            'columns'      => [
                [
                    'header'        => '',
                    'headerOptions' => [
                        'style' => 'width: 5%;',
                    ],
                    'content'       => function ($item) {
                        return Html::img('/images/controller/cabinet-bills/index/wallet.png', ['width' => 30, 'style' => 'margin-top: 13px;']);
                    },
                ],
                [
                    'header'        => '',
                    'headerOptions' => [
                        'style' => 'text-align: center;width: 10%;',
                    ],
                    'content'       => function ($item) {
                        $c = \common\models\avatar\Currency::findOne($item['currency']);

                        $imgOptions = [
                            'width' => 50,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $c->code,
                            ],
                        ];

                        return Html::img($c->image, $imgOptions);
                    },
                ],

                [
                    'header'        => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                    'headerOptions' => [
                        'style' => 'width: 10%;',
                    ],
                    'content'       => function ($item) {

                        $html = [];

                        /** @var \common\models\avatar\UserBill $u */
                        $u = $item;
                        $w = \common\models\piramida\Wallet::findOne($u->address);
                        if (is_null($w)) {
                            \cs\services\VarDumper::dump($item);
                        }
                        $shortAddress = $w->getAddressShort();
                        $fullAddress = $w->getAddress();
                        $html[] = Html::tag(
                            'span',
                            $shortAddress,
                            [
                                'class' => 'js-buttonTransactionInfo textDecorated',
                                'style' => 'font-family: "Courier New", Courier, monospace;',
                                'role'  => 'button',
                                'data'  => [
                                    'placement'      => 'bottom',
                                    'clipboard-text' => $fullAddress,
                                ],
                            ]
                        );

                        return join('<br>', $html);
                    },
                ],
                [
                    'header'        => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                    'headerOptions' => [
                        'style' => 'width: 15%;',
                    ],
                    'attribute'     => 'name',
                ],
                [
                    'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                    'headerOptions'  => [
                        'style' => 'text-align: right;width: 30%;',
                    ],
                    'contentOptions' => function ($item) {
                        return [
                            'class' => 'rowBill',
                            'id'    => 'bill_confirmed_' . $item['id'],
                            'data'  => ['id' => $item['id']],
                            'style' => 'text-align: right;',
                        ];
                    },
                    'content'        => function (\common\models\avatar\UserBill $item) {
                        $a = $item->address;
                        $w = \common\models\piramida\Wallet::findOne($a);
                        $c = \common\models\piramida\Currency::findOne($w->currency_id);
                        $v = $w->getAmountWithDecimals();

                        return Html::tag('code', Yii::$app->formatter->asDecimal($v, $c->decimals_view), ['class' => 'balance1']) . Html::tag('span', $c->code, ['class' => 'label label-info']);
                    },
                ],


            ],
        ]) ?>


        <h1 class="page-header text-center">Внутренние счета</h1>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => \common\models\avatar\UserBill::find()
                    ->innerJoin('currency', 'currency.id = user_bill.currency')
                    ->where([
                        'user_bill.user_id'      => $user->id,
                        'user_bill.mark_deleted' => 0,
                        'user_bill.currency'     => [
                            \common\models\avatar\Currency::MARKET,
                            \common\models\avatar\Currency::RUB,
                        ],
                    ])
                    ->select(['user_bill.*'])
                    ->orderBy(['currency.sort_index' => SORT_ASC])
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-hover table-striped',
                'id'    => 'tableTransaction',
            ],
            'summary' => '',
            'rowOptions'   => function (\common\models\avatar\UserBill $item) {
                $data = [
                    'data'  => [
                        'id'             => $item['id'],
                        'currency'       => $item['currency'],
                        'currencyObject' => \yii\helpers\ArrayHelper::toArray(\common\models\avatar\Currency::findOne($item['currency'])),
                    ],
                    'role'  => 'button',
                    'class' => 'rowTable',
                ];

                return $data;
            },
            'columns'      => [
                [
                    'header'  => '',
                    'headerOptions'  => [
                        'style' => 'width: 5%;'
                    ],
                    'content' => function ($item) {
                        return Html::img('/images/controller/cabinet-bills/index/wallet.png', ['width' => 30, 'style' => 'margin-top: 13px;']);
                    },
                ],
                [
                    'header'        => '',
                    'headerOptions' => [
                        'style' => 'text-align: center;width: 10%;',
                    ],
                    'content'       => function ($item) {
                        $c = \common\models\avatar\Currency::findOne($item['currency']);

                        $imgOptions = [
                            'width' => 50,
                            'class' => 'img-circle',
                            'data'  => [
                                'toggle' => 'tooltip',
                                'title'  => $c->code,
                            ],
                        ];

                        return Html::img($c->image, $imgOptions);
                    },
                ],
                [
                    'header'  => Yii::t('c.TsqnzVaJuC', 'Адрес'),
                    'headerOptions'  => [
                        'style' => 'width: 10%;',
                    ],
                    'content' => function ($item) {

                        $html = [];

                        /** @var \common\models\avatar\UserBill $u */
                        $u = $item;
                        $w = \common\models\piramida\Wallet::findOne($u->address);
                        if (is_null($w)) {
                            \cs\services\VarDumper::dump($item);
                        }
                        $shortAddress = $w->getAddressShort();
                        $fullAddress = $w->getAddress();
                        $html[] = Html::tag(
                            'span',
                            $shortAddress,
                            [
                                'class' => 'js-buttonTransactionInfo textDecorated',
                                'style' => 'font-family: "Courier New", Courier, monospace;',
                                'role'  => 'button',
                                'data' => [
                                    'placement'      => 'bottom',
                                    'clipboard-text' => $fullAddress,
                                ],
                            ]
                        );

                        return join('<br>', $html);
                    },
                ],
                [
                    'header' => Yii::t('c.TsqnzVaJuC', 'Наименование'),
                    'headerOptions'  => [
                        'style' => 'width: 15%;',
                    ],
                    'attribute' => 'name',
                ],
                [
                    'header'         => Yii::t('c.TsqnzVaJuC', 'Баланс'),
                    'headerOptions'  => [
                        'style' => 'text-align: right;width: 30%;'
                    ],
                    'contentOptions' => function ($item) {
                        return [
                            'class' => 'rowBill',
                            'id'    => 'bill_confirmed_' . $item['id'],
                            'data'  => ['id' => $item['id']],
                            'style' => 'text-align: right;',
                        ];
                    },
                    'content'        => function (\common\models\avatar\UserBill $item) {
                        $a = $item->address;
                        $w = \common\models\piramida\Wallet::findOne($a);
                        $c = \common\models\piramida\Currency::findOne($w->currency_id);
                        $v = \common\models\piramida\Currency::getValueFromAtom($w->amount, $w->currency_id);

                        return Html::tag('code', Yii::$app->formatter->asDecimal($v, 2), ['class' => 'balance1']) . Html::tag('span', $c->code, ['class' => 'label label-info']);
                    },
                ],
            ],
        ]) ?>
    </div>
</div>
