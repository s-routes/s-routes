<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Все счета';

?>

<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php \yii\widgets\Pjax::begin(); ?>
        <?php
        $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    // window.location = '/buh-billing/view' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonConfirm').click(function (e) {
    if (confirm('Подтверждаете?')) {
        ajaxJson({
            url: '/buh-billing/confirm',
            data: {id: $(this).data('id')},
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location = '/buh-billing/index';
                }).modal();
            } 
        });
    }
});
JS
        );
        ?>
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\BillingMain::find()
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];

                return $data;
            },
            'columns'      => [
                'id',
                [
                    'header'  => 'Пополнение',
                    'content' => function ($item) {
                        $class_id = $item['class_id'];
                        $classObject = \common\models\BillingMainClass::findOne($class_id);
                        if (is_null($classObject)) return '';
                        /** @var \yii\db\ActiveRecord $className */
                        $className = $classObject->name;
                        switch ($className) {
                            case '\common\models\RequestInputRub':
                                return 'RUB';
                            case '\common\models\RequestInputMarket':
                                return 'MARKET';
                            default:
                                return '';
                        }
                    },
                ],
                [
                    'header'  => 'Пользователь',
                    'content' => function ($item) {
                        $class_id = $item['class_id'];
                        $classObject = \common\models\BillingMainClass::findOne($class_id);
                        if (is_null($classObject)) return '';
                        /** @var \yii\db\ActiveRecord $className */
                        $className = $classObject->name;
                        $o = $className::findOne(['billing_id' => $item['id']]);
                        $i = $o['user_id'];

                        try {
                            $user = \common\models\UserAvatar::findOne($i);
                        } catch (Exception $e) {
                            return '';
                        }

                        return Html::img($user->getAvatar(), [
                            'class'  => "img-circle",
                            'width'  => 60,
                            'height' => 60,
                            'style'  => 'margin-bottom: 0px;',
                            'title' => $user->getName2(),
                            'data' => [
                                'toggle' => 'tooltip',
                            ],
                        ]);
                    },
                ],
                [
                    'header'  => 'Создано',
                    'content' => function ($item) {
                        $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                        if ($v == 0) return '';

                        return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                    }
                ],
                [
                    'header'  => 'Сумма',
                    'headerOptions' => ['class' => 'text-right'],
                    'contentOptions' => ['class' => 'text-right'],
                    'content' => function ($item) {
                        $ps = $item['sum_after'];

                        return Yii::$app->formatter->asDecimal($ps/100, 2);
                    },
                ],
                [
                    'header'  => 'ПС',
                    'content' => function ($item) {
                        $ps = $item['source_id'];
                        try {
                            $psObject = \common\models\PaySystem::findOne($ps);
                        } catch (Exception $e) {
                            return  '';
                        }

                        return Html::img('https://neiro-n.com'.$psObject->image, ['width' => 50, 'class' => 'thumbnail', 'style' => 'margin-bottom:0px;']);
                    },
                ],
                [
                    'header'  => '№ Заявки',
                    'content' => function ($item) {
                        $class_id = $item['class_id'];
                        $classObject = \common\models\BillingMainClass::findOne($class_id);
                        if (is_null($classObject)) return '';
                        /** @var \yii\db\ActiveRecord $className */
                        $className = $classObject->name;
                        $o = $className::findOne(['billing_id' => $item['id']]);
                        $i = $o['id'];

                        return $i;
                    },
                ],
                [
                    'header'  => 'Оплачен? Клиент',
                    'content' => function ($item) {
                        if (!is_null($item['is_paid_client'])) {
                            if ($item['is_paid_client'] == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        }

                        return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                    },
                ],
                [
                    'header'  => 'Оплачен? Магазин',
                    'content' => function ($item) {
                        if (!is_null($item['is_paid'])) {
                            if ($item['is_paid'] == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                        }

                        return Html::tag('span', 'Нет', ['class' => 'label label-default']) .'<br><br>'. Html::button('Подтвердить', ['class' => 'btn btn-xs btn-default buttonConfirm', 'data' => ['id' => $item['id']]]) ;
                    },
                ],

                [
                    'header'  => 'Ключ',
                    'content' => function ($item) {
                        $ps = $item['source_id'];
                        try {
                            $psObject = \common\models\PaySystem::findOne($ps);
                        } catch (Exception $e) {
                            return  '';
                        }
                        switch ($psObject->id) {
                            case 5: // sber
                                $p = \common\models\PaymentSber::findOne(['billing_id' => $item['id']]);
                                break;
                            case 6: // tinkoff
                                $p = \common\models\PaymentTinkoff::findOne(['billing_id' => $item['id']]);
                                break;
                            case 13: // ozon
                                $p = \common\models\PaymentOzon::findOne(['billing_id' => $item['id']]);
                                break;
                            default:
                                return '';
                        }

                        return $p['key'];
                    },
                ],


                [
                    'header'  => 'Карта',
                    'content' => function ($item) {
                        $ps = $item['source_id'];
                        try {
                            $psObject = \common\models\PaySystem::findOne($ps);
                        } catch (Exception $e) {
                            return  '';
                        }
                        switch ($psObject->id) {
                            case 5: // sber
                                $p = \common\models\PaymentSber::findOne(['billing_id' => $item['id']]);
                                break;
                            case 6: // tinkoff
                                $p = \common\models\PaymentTinkoff::findOne(['billing_id' => $item['id']]);
                                break;
                            case 13: // ozon
                                $p = \common\models\PaymentOzon::findOne(['billing_id' => $item['id']]);
                                break;
                            default:
                                return '';
                        }

                        return $p['card'];
                    },
                ],
                [
                    'header'  => 'Кошельки',
                    'content' => function ($item) {
                        $class_id = $item['class_id'];
                        $classObject = \common\models\BillingMainClass::findOne($class_id);
                        if (is_null($classObject)) return '';
                        /** @var \yii\db\ActiveRecord $className */
                        $className = $classObject->name;
                        $o = $className::findone(['billing_id' => $item['id']]);
                        $uid = $o['user_id'];

                        return Html::a('Кошельки', ['wallet-list', 'id' => $uid], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],
                [
                    'header'  => 'Карточка',
                    'content' => function ($item) {
                        $class_id = $item['class_id'];
                        $classObject = \common\models\BillingMainClass::findOne($class_id);
                        if (is_null($classObject)) return '';
                        /** @var \yii\db\ActiveRecord $className */
                        $className = $classObject->name;
                        $o = $className::findone(['billing_id' => $item['id']]);
                        $uid = $o['user_id'];

                        return Html::a('Карточка', ['view-user', 'id' => $uid], [
                            'class' => 'btn btn-default btn-xs',
                            'data'  => [
                                'id' => $item['id'],
                            ]
                        ]);
                    }
                ],

            ],
        ]) ?>
        <?php \yii\widgets\Pjax::end(); ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>