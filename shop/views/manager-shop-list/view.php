<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $shop \common\models\ShopAll */

$this->title = $shop->id;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?= \yii\widgets\DetailView::widget([
            'model' => $shop,
            'attributes' => [
                [
                    'attribute' => 'id',
                    'captionOptions' => ['style' => 'width: 30%']
                ],
                'link:text:Ссылка',
                'description:text:Описание',
                'account_login:text:Логин',
                'account_password:text:Пароль',
            ]
        ]) ?>
    </div>
</div>
