<?php

use Endroid\QrCode\ErrorCorrectionLevel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \shop\models\forms\GoogleAuth */

$this->title = Yii::t('c.DLo6pHtmzQ', 'Двухфакторная авторизация (2FA)');

/** @var $user \common\models\UserAvatar */
$user = Yii::$app->user->identity;


Yii::setAlias('@Endroid/QrCode', '@vendor/endroid/QrCode/src');
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <?php if ($user->shop_google_auth_code) { ?>
            <p class="alert alert-success"><?= \Yii::t('c.DLo6pHtmzQ', 'Код установлен') ?></p>
            <p><?= \Yii::t('c.DLo6pHtmzQ', 'Для того чтобы сбросить, введите пинкод 2FA') ?></p>

            <?php $model = new \avatar\models\validate\CabinetGoogleCodeDown(); ?>
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'formUrl' => '/cabinet-google-code/reset',
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet-google-code/set?type=reload';
    }).modal();
}
JS
            ,
        ]) ?>

            <?= $form->field($model, 'pin')->label(Yii::t('c.DLo6pHtmzQ', 'Пин код')) ?>
            <hr>
            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => Yii::t('c.DLo6pHtmzQ', 'Сбросить')]) ?>

        <?php } else { ?>
            <p>
                <?php
                $secret = Yii::$app->session->get('googleKey');
                if (is_null($secret)) {
                    $file = Yii::getAlias('@vendor/chregu/GoogleAuthenticator.php/lib/GoogleAuthenticator.php');
                    require_once($file);

                    $ga = new \GoogleAuthenticator;
                    $secret = $ga->generateSecret();
                    Yii::$app->session->set('googleKey', $secret);
                }

                $url = sprintf("otpauth://totp/%s?secret=%s", $user->email . '@' . 'topmate.one', $secret);

                $qr = new \Endroid\QrCode\QrCode();
                $qr->setText($url);
                $qr->setSize(180);
                $qr->setMargin(20);
                $qr->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
                $qr->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
                $qr->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
                $qr->setLabelFontSize(16);
                $content = $qr->writeString();
                $src = 'data:image/png;base64,' . base64_encode($content);

                ?>
            <p class="text-center"><img src="<?= $src ?>"></p>
            <div class="row">
                <div class="col-lg-6">
                    <p class="text-center"><a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" target="_blank"><img src="/images/mobile/android.gif"></a></p>
                </div>
                <div class="col-lg-6">
                    <p class="text-center"><a href="https://itunes.apple.com/ru/app/google-authenticator/id388497605?mt=8" target="_blank"><img src="/images/mobile/ios.gif"></a></p>
                </div>
                <div class="col-lg-12">
                    <p class="text-center"><?= \Yii::t('c.DLo6pHtmzQ', 'Установите на вашем смартфоне приложение Google Authenticator') ?></p>
                </div>
            </div>
            <p class="alert alert-success text-center"><?= \Yii::t('c.DLo6pHtmzQ', 'Сохраните код') ?>:<br><code style="font-size: 200%;"><?= $secret ?></code></p>
            <p class="text-center">
                <a
                        href="<?= Url::to(['cabinet-google-code/download', 'code' => $secret, 'email' => $user->email]) ?>"
                        class="btn btn-success"
                        style="width: 100%; max-width: 200px;text-align: center;"
                ><i class="glyphicon glyphicon-download"></i> <?= \Yii::t('c.DLo6pHtmzQ', 'Скачать PDF') ?></a>
            </p>

            <table align="center" width="200" border="0">
                <tr>
                    <td>
                        <?php $form = ActiveForm::begin([
                            'options' => ['style' => 'width: 100%; max-width: 200px;']
                        ]) ?>
                        <?= $form->field($model, 'pin', ['inputOptions' => ['placeholder' => '000000']])->label(Yii::t('c.DLo6pHtmzQ', 'Код с телефона')) ?>
                        <?= Html::hiddenInput(Html::getInputName($model, 'code'), $secret) ?>
                        <?= Html::submitButton(Yii::t('c.DLo6pHtmzQ', 'Включить'), ['class' => 'btn btn-success', 'style' => 'width:100%']) ?>
                        <?php ActiveForm::end() ?>
                    </td>
                </tr>
            </table>

        <?php } ?>
    </div>

    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?= \Yii::t('c.57GUXH4BTk', 'Информация') ?></h4>
            </div>
            <div class="modal-body">
                <?= \Yii::t('c.57GUXH4BTk', 'Успешно!') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>