<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $request \common\models\ShopRequest */

$this->title = 'Заказ №' . $request->id;

if (is_null($request->chat_room_id)) {
    $chatRoom = \common\models\ChatRoom::add([
        'last_message' => null,
    ]);
    $request->chat_room_id = $chatRoom->id;
    $request->save();
}

$room_id = $request->chat_room_id;
$user_id = Yii::$app->user->id;

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);

$type_cancel = \common\models\ShopRequest::TYPE_CANCEL_MANAGER_ALL;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

        <?php
        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::RUB);
        $user = \common\models\UserAvatar::findOne($request->user_id);

        try {
            $manager = \common\models\UserAvatar::findOne($request->manager_id);
            $manager = Html::img($manager->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $manager->getName2() . ' (' . $manager->getAddress() . ')']) . $manager->getName2() . ' (' . $manager->getAddress() . ')';
        } catch (Exception $e) {
            $manager = '';
        }
        $c2 = \common\models\avatar\Currency::findOne($request->currency_id_paid);
        $cio = \common\models\CurrencyIO::findFromExt($request->currency_id_paid);
        $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cio->currency_int_id, $request->user_id);
        /** @var \common\models\avatar\UserBill $bill */
        $bill = $data['billing'];
        $c2text = (is_null($c2))? '' : $c2->code;
        $c2text = Html::a($c2text, ['cabinet-wallet/item', 'id' => $bill->id], ['title' => 'Перейти в кошелек']);
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model'         => $request,
            'attributes' => [
                [
                    'attribute'      => 'id',
                    'captionOptions' => ['style' => 'width: 30%'],
                ],
                'created_at:datetime:Создан',
                [
                    'label'       => 'Сумма',
                    'attribute'   => 'price',
                    'format'      => ['decimal', $c->decimals_view_shop],
                    'value'       => $request->price / pow(10, $c->decimals),
                ],
                [
                    'label'      => 'Валюта заказа на маркетплейсе указана в',
                    'attribute'   => 'currency_id',
                    'format'      => 'html',
                    'value'       => Html::tag('span', $c->code, ['class' => 'label label-info']),
                ],
                [
                    'label'      => 'Оплата заказа произведена в',
                    'attribute'   => 'currency_id_paid',
                    'format'      => 'html',
                    'value'       => Html::tag('span', $c2text, ['class' => 'label label-info']),
                ],
                [
                    'label'       => 'Пользователь',
                    'attribute'   => 'user_id',
                    'format'      => 'html',
                    'value'       => Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $user->getName2() . ' (' . $user->getAddress() . ')']) . $user->getName2() . ' (' . $user->getAddress() . ')',
                ],
                [
                    'label'       => 'Тип указания данных',
                    'attribute'   => 'type_id',
                    'value'       =>  \cs\Application::isEmpty($request->type_id)? '' : \common\models\ShopRequest::$_type_id[$request->type_id],
                ],
                [
                    'label'       => 'Тип указания адреса',
                    'attribute'   => 'type_id_delivery',
                    'value'       => \cs\Application::isEmpty($request->type_id_delivery)? '' : \common\models\ShopRequest::$_type_id_delivery[$request->type_id_delivery],
                ],

                'delivery:text:Адрес доставки',
                'delivery:text:Адрес доставки',
                'delivery_dop:text:Дополнительная информация и пожелания по доставке',
                'user_name:text:Имя',
                'user_phone:text:Телефон',
                'user_email:text:Email',
                'user_telegram:text:Telegram',
                'user_whatsapp:text:Whatsapp',
                [
                    'label'      => 'Менеджер',
                    'attribute'   => 'manager_id',
                    'format'      => 'html',
                    'value'       => $manager,
                ],
            ]
        ]) ?>

        <?php if (ArrayHelper::getValue($request, 'is_cancel', 0) == 0) { ?>
            <?php if (\cs\Application::isEmpty($request->manager_id)) { ?>
                <p>
                    <button class="btn btn-info buttonGet" data-id="<?= $request->id ?>">Взять заказ</button>
                </p>
                <?php
                $this->registerJs(<<<JS
$('.buttonGet').click(function (e) {
    ajaxJson({
        url: '/buh-request-list/get',
        data: {id: $(this).data('id') },
        success: function (ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location.reload();
            }).modal();
        }
    });
});
JS
                )
                ?>
            <?php } ?>

            <?php if (!\cs\Application::isEmpty($request->manager_id)) { ?>
                <?php if ($request->is_closed == 0) { ?>
                    <p>
                        <button class="btn btn-warning buttonClose" data-id="<?= $request->id ?>">Закрыть заказ</button>
                        <button class="btn btn-default buttonCancel" data-id="<?= $request->id ?>">ОТМЕНА ЗАКАЗА ПОЛНОСТЬЮ</button>
                        <a class="btn btn-default buttonCancelPart" href="/buh-request-list/cancel-part?id=<?= $request->id ?>" data-id="<?= $request->id ?>">ОТМЕНА ЗАКАЗА ЧАСТИЧНО</a>
                    </p>
                    <?php
                    $this->registerJs(<<<JS

$('.buttonClose').click(function (e) {
    ajaxJson({
        url: '/buh-request-list/close',
        data: {id: $(this).data('id') },
        success: function (ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location.reload();
            }).modal();
        }
    });
});

$('.buttonCancel').click(function (e) {
    if (confirm('Вы подтверждаете действие?')) {
        ajaxJson({
            url: '/buh-request-list/cancel',
            data: {
                id: $(this).data('id'),
                type_cancel: {$type_cancel}
            },
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    }
});
JS
                    )
                    ?>
                <?php } ?>
            <?php } ?>
        <?php } else { ?>
            <p class="alert alert-danger" style="margin-bottom: 20px;">Заказ отменен</p>
        <?php } ?>


        <?php
        $dataRub = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::RUB, $request->user_id);
        /** @var \common\models\avatar\UserBill $billRub */
        $billRub = $dataRub['billing'];

        $dataMarket = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::MARKET, $request->user_id);
        /** @var \common\models\avatar\UserBill $billMarket */
        $billMarket = $dataMarket['billing'];
        ?>
        <p>
            <a href="/cabinet-wallet/item?id=<?= $billRub->id ?>">
                <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" alt="" data-toggle="tooltip" data-placement="bottom" title="RUB">
            </a>
            <a href="/cabinet-wallet/item?id=<?= $billMarket->id ?>">
                <img src="/images/controller/cabinet-bills/index/wallet.png" width="30" alt="" data-toggle="tooltip"  data-placement="bottom" title="MARKET">
            </a>

        </p>


        <h2 class="page-header">Список товаров</h2>
        <?php if ($request->type == 1) { ?>
            <?php $c = 0; ?>
            <?php $sum = 0; ?>
            <?php foreach (\common\models\ShopRequestProduct::find()->where(['request_id' => $request->id])->all() as $p) { ?>
                <div class="row rowProduct" data-count="<?= $c ?>" style="margin-bottom: 20px;">
                    <div class="col-lg-8"><abbr class="buttonCopy" data-clipboard-text="<?= $p['link'] ?>" data-toggle="tooltip" title="Нажмите чтобы скопировать"><?= $p['link'] ?></abbr></div>
                    <div class="col-lg-2"><?= $p['price'] ?> RUB</div>
                    <div class="col-lg-2"><?= $p['count'] ?> шт</div>
                </div>
                <?php $sum += $p['price'] * $p['count']; ?>
                <?php $c++; ?>
            <?php } ?>
        <?php } ?>

        <?php if ($request->type == 2) { ?>
            <?php
            $info = \yii\helpers\Json::decode($request->info);
            $t = $info['type'];
            $sum = \common\models\piramida\Currency::getValueFromAtom($request->price, $request->currency_id);
            ?>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-8">Пополнение баланса номера телефона сотовой связи (мобильной SIM-карты) <?= \common\models\ShopRequest::$_phone[$t] ?></div>
                <div class="col-lg-2"><?= \common\models\piramida\Currency::getValueFromAtom($request->price, $request->currency_id) ?> RUB</div>
            </div>
        <?php } ?>
        <?php foreach (\common\models\ShopRequestAdd::find()->where(['request_id' => $request->id])->all() as $p) { ?>

            <div class="row rowProduct2" style="margin-bottom: 20px;">
                <div class="col-lg-8"><?= $p['comment'] ?></div>
                <?php
                $cio = \common\models\CurrencyIO::findFromExt($p['currency_id']);
                $price = \common\models\piramida\Currency::getValueFromAtom($p['amount'], $cio->currency_int_id);
                ?>
                <div class="col-lg-2"><?= Yii::$app->formatter->asDecimal($price, 2) ?> RUB</div>
            </div>
            <?php $sum += $price; ?>
        <?php } ?>
        <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
            <div class="col-lg-8 text-right" style="font-weight: bold">Итого:</div>
            <div class="col-lg-4" style="font-weight: bold"><span class="js-product-list-sum"><?= Yii::$app->formatter->asDecimal($sum, 2) ?></span> RUB</div>
        </div>

        <?= $this->render('../cabinet-exchange/chat', [
            'room_id'  => $room_id,
            'user_id'  => $user_id,
            'send_url' => '/buh-request-list/send',
        ]); ?>
    </div>
</div>
