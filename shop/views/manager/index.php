<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Менеджер магазина';


?>

<div class="container" style="padding-bottom: 70px;">

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="col-lg-8">
        <p><a href="/manager-request-list/index" class="btn btn-default">Все заказы</a></p>
        <p><a href="/manager-shop-list/index" class="btn btn-default">Все магазины</a></p>
        <p><a href="/admin-support/index" class="btn btn-default">Чат поддержки</a></p>
    </div>

</div>