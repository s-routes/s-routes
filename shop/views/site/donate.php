<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Поддержка проекта, донаты, взносы';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= $this->title ?></h1>
    </div>
    <div class="col-lg-8 col-lg-offset-2">

        <p>Если Вам нравится проект - помогите нам в его развитии. Мы будем признательны всем нашим спонсорам за любой объем материальной помощи. Мы гарантируем, а Вы можете быть уверены в том, что все 100% средств финансовой поддержки идут на текущие затраты по проекту, и на его дальнейшее развитие.</p>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\PaySystemConfig::find()->where(['paysystem_id' => [5,6,13]])
                ,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'rowOptions'   => function ($item) {
                $data = [
                    'data'  => ['id' => $item['id']],
                    'role'  => 'button',
                    'class' => 'rowTable'
                ];
                return $data;
            },
            'columns'      => [
                [
                    'header'  => '',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        $ps = $item->getPaySystem();
                        $i = $ps->image;
                        if ($i == '') return '';

                        return Html::img('https://neiro-n.com'.$i, [
                            'class'  => "thumbnail",
                            'width'  => 80,
                            'height' => 80,
                            'style'  => 'margin-bottom: 0px;',
                        ]);
                    }
                ],

                [
                    'header'  => 'Валюта',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        $ps = $item->getPaySystem();
                        $currency = $ps->getCurrencyObject();
                        if (is_null($currency)) return '';

                        return Html::tag('span', $currency->code, ['class' => 'label label-info']);
                    },
                ],
                [
                    'header'  => 'Банк',
                    'content' => function (\common\models\PaySystemConfig $item) {
                        $ps = $item->getPaySystem();

                        return $ps->title;
                    },
                ],
                [
                    'header'  => 'Карта',
                    'content' => function (\common\models\PaySystemConfig $item) {

                        return \yii\helpers\Json::decode($item['config'])['card'];
                    },
                ],
            ],
        ]);
        ?>
    </div>



</div>


