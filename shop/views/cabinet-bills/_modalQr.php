<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 23.01.2017
 * Time: 10:21
 */
/* @var $this yii\web\View */

use common\models\avatar\Currency;

\avatar\assets\QrCode\Asset::register($this);

$this->registerJs(<<<JS
$('.buttonQrCode').click(function(e) {
    e.preventDefault();
    e.stopPropagation();
    var address = $(this).data('address');
    var div = $('<p>', {class: 'text-center'});
    $('#modalQrCode .modal-body').html(div);
    div.qrcode(address);
    $('#modalQrCode .modal-body').append($('<p>', {class: 'text-center'}).html($('<code>').html(address)));
    $('#modalQrCode').modal();
});

JS
);

?>

<div class="modal fade" tabindex="-1" role="dialog" id="modalQrCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?= \Yii::t('c.TsqnzVaJuC', 'Принять на счет') ?></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.TsqnzVaJuC', 'Закрыть') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

