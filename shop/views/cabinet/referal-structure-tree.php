<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 16.04.2016
 * Time: 20:21
 */

/**
 * @var array $rows = [
 *      [
 *          'id' =>
 *          'items' => []
 *      ],
 * ]
 */
/** @var $cInt      \common\models\piramida\Currency */
/** @var $rootUser  \common\models\UserAvatar - корневой пользователь всей отображаемой структуры */

?>


<?php foreach ($rows as $row) { ?>
    <?php $user = $row; ?>
    <?php $nodes = null; ?>
    <?php if (isset($row['items'])) $nodes = $row['items'];  ?>
    <?php unset($row['items']);  ?>
    <?php $userObject = \common\models\UserAvatar::findOne($row['id']) ?>
    <div class="media">
        <div class="media-left">
            <img
                class="media-object"
                src="<?= $userObject->getAvatar() ?>"
                style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
            >
        </div>
        <div class="media-body">
            <?php
            $c = \common\models\ShopUserPartner::find()->where(['parent_id' => $userObject->id])->count();
            ?>
            <h4 class="media-heading"><?= $userObject->getName2() ?> <code class="buttonCopy" title="Нажмите чтобы скопировать" data-toggle="tooltip" data-clipboard-text="<?= $userObject->getAddress() ?>"><?= $userObject->getAddress() ?></code> (<?= $c ?>)</h4>

            <!-- я заработал на нем (от него мне были бонусы)   -->
            <?php
            $sum = \common\models\ShopReferalTransaction::find()->where(['from_uid' => $userObject->id, 'to_uid' => $rootUser->id])->select('sum(amount) as s1')->scalar();
            $sum = $sum ? $sum : 0;
            $sum = bcdiv($sum, bcpow(10, $cInt->decimals), $cInt->decimals);
            ?>
            <span class="label label-success" title="я заработал на нем (от него мне были бонусы)" data-toggle="tooltip">+ <?= $sum ?> MБ</span> |

            <!-- сколько он заработал по партнерской программе   -->
            <?php
            $sum = \common\models\ShopReferalTransaction::find()->where(['to_uid' => $userObject->id])->select('sum(amount) as s1')->scalar();
            $sum = $sum ? $sum : 0;
            $sum = bcdiv($sum, bcpow(10, $cInt->decimals), $cInt->decimals);
            ?>
            <span class="label label-info" title="сколько он заработал по партнерской программе" data-toggle="tooltip">+ <?= $sum ?> MБ</span>

            <?php if (!is_null($nodes)) { ?>
                <?= $this->render('referal-structure-tree', ['rows' => $nodes, 'cInt' => $cInt, 'rootUser' => $rootUser]); ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>
