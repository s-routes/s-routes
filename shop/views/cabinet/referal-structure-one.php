<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/** @var $this yii\web\View */
/** @var \common\models\UserAvatar $userObject */

/**
[
0 => [
'id' => 9
'items' => [
0 => [
'id' => 4200
]
]
]
]
 */
function getTree($school, $parent_id)
{
    $data = ['items' => []];
    $arr = [];
    $partnerList = \common\models\ShopUserPartner::find()->where(['parent_id' => $parent_id])->all();
    /** @var \common\models\ShopUserPartner $item */
    foreach ($partnerList as $item) {
        $item1 = ['id' => $item->user_id];

        $items = getNode($school, $item->user_id);

        if (count($items) > 0) {
            $item1['items'] = $items;
        }

        $arr[] = $item1;
    }

    return ['items' => $arr];
}

function getNode($school, $user_id)
{
    $arr = [];
    $partnerList = \common\models\ShopUserPartner::find()->where([ 'parent_id' => $user_id])->all();
    /** @var \common\models\UserPartner $item */
    foreach ($partnerList as $item) {
        $item1 = ['id' => $item->user_id];

        $items = getNode($school, $item->user_id);
        if (count($items) > 0) {
            $item1['items'] = $items;
        }

        $arr[] = $item1;
    }

    return $arr;
}

$items = getTree(1, $userObject->id);
$cInt = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::MARKET);

?>
<ul class="media-list">
    <li class="media">
        <div class="media-left">
            <a href="#">
                <img class="media-object"
                     src="<?= $userObject->getAvatar() ?>"
                     style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
                >
            </a>
        </div>
        <div class="media-body">
            <?php
            $c = \common\models\ShopUserPartner::find()->where(['parent_id' => $userObject->id])->count();
            ?>

            <h4 class="media-heading"><?= $userObject->getName2() ?> <code class="buttonCopy" title="Нажмите чтобы скопировать" data-toggle="tooltip" data-clipboard-text="<?= $userObject->getAddress() ?>"><?= $userObject->getAddress() ?></code> (<?= $c ?>)</h4>
            <?php
            $sum = \common\models\ShopReferalTransaction::find()->where(['to_uid' => $userObject->id])->select('sum(amount) as s1')->scalar();
            $sum = $sum ? $sum : 0;
            $sum = bcdiv($sum, bcpow(10, $cInt->decimals), $cInt->decimals);
            ?>
            <span
                title="я заработал на всей структуре"
                data-toggle="tooltip"
                class="label label-info"
            ><?= $sum ?> MБ</span>

            <?= $this->render('referal-structure-tree', ['rows' => $items['items'], 'cInt' => $cInt, 'rootUser' => $userObject]) ?>

        </div>
    </li>
</ul>