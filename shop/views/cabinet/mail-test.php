<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \avatar\models\forms\MailTest */

$this->title = 'Временная почта';

$this->registerJs(<<<JS
$('.buttonSet').click(function(e) {
    function setCookie2 (name, value) {
        var date = new Date();
        var now = new Date();
        var time = now.getTime();
        var expireTime = time + (1000 * 60 * 60 * 24 * 365);
        now.setTime(expireTime);
        document.cookie = name + "=" + value +
        "; expires=" + now.toGMTString() +
        "; path=/";
    }
    setCookie2('mailTest', $('#mailtest-mail').val());
})
JS
);

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header"><?= $this->title ?></h1>

        <?php if (Yii::$app->session->hasFlash('form')) { ?>
            <p class="alert alert-success">Успешно</p>
        <?php } else { ?>
            <?php $form = \yii\bootstrap\ActiveForm::begin() ?>
            <?= $form->field($model, 'mail') ?>
            <hr>
            <?= Html::button('Обновить', ['class' => 'btn btn-success buttonSet'])?>
            <?php \yii\bootstrap\ActiveForm::end() ?>
        <?php } ?>

    </div>
</div>