<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/** @var $this yii\web\View */
/** @var $form yii\bootstrap\ActiveForm */
/** @var $model \shop\models\forms\ShopSettings */

$this->title = 'Данные для оформления доставки';


?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <br>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <p class="alert alert-success">
                <?= \Yii::t('c.FMg0mLqFR9', 'Успешно обновлено') ?>.
            </p>

            <p>
                <a href="/cabinet/profile" class="btn btn-primary">
                    Профиль
                </a>
            </p>
        <?php else: ?>

            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model' => $model,
                'success'  => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/cabinet/index';
    }).modal();
}
JS
,
            ]); ?>

            <?= $form->field($model, 'shop_fio_contact') ?>
            <?= $form->field($model, 'shop_address') ?>
            <?= $form->field($model, 'shop_phone') ?>
            <?= $form->field($model, 'shop_whatsapp') ?>
            <?= $form->field($model, 'shop_telegram') ?>
            <?= $form->field($model, 'shop_dop')->textarea(['rows' => 5]) ?>

            <hr class="featurette-divider">

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>

</div>



<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
