<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $model \avatar\models\forms\RepareWallets */

$this->title = 'Восстановление доступа к кошелькам';

/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">

        <?php if (Yii::$app->session->hasFlash('form')) { ?>

            <p class="alert alert-success">Успешно</p>

        <?php } else { ?>
            <?php if ($user->wallets_is_locked) { ?>
                <p class="alert alert-danger">Доступ к кошелькам заблокирован</p>

                <?php $form = ActiveForm::begin() ?>

                <?= $form->field($model, 'seeds')->textarea(['rows' => 3]) ?>
                <?= $form->field($model, 'password')->passwordInput() ?>

                <hr>
                <?= Html::submitButton('Восстановить', [
                    'class' => 'btn btn-success',
                    'name'  => 'contact-button',
                    'style' => 'width:100%',
                ]) ?>

                <?php ActiveForm::end() ?>

            <?php } else { ?>
                <p class="alert alert-success">Доступ к кошелькам актуален через пароль кабинета</p>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



