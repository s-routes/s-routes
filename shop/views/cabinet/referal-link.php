<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model \shop\models\validate\CabinetReferal */

$this->title = 'Реферальная ссылка';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
$url = Url::to(['/', 'partner_id' => Yii::$app->user->id], true);

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <div class="input-group">
            <input type="text" class="form-control" value="<?= $url ?>">
            <span class="input-group-btn">
                <button class="btn btn-default buttonCopy" type="button" data-clipboard-text="<?= $url ?>">Скопировать</button>
            </span>
        </div><!-- /input-group -->


        <hr>
        <?php
        $partner = \common\models\ShopUserPartner::findOne([
            'user_id'   => Yii::$app->user->id,
        ]);
        ?>
        <?php if (is_null($partner))  { ?>
            <P>Я хочу встать в структуру по реферальной ссылке от пригласителя</P>
            <?php
            $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model,
                'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function(e) {
        window.location.reload();
    }).modal();
}
JS
                ,

            ]); ?>
            <?= $form->field($model, 'url') ?>
            <hr>
            <?php \iAvatar777\services\FormAjax\ActiveForm::end(); ?>
            <hr>


            <P>Внимание: ввод реферальной ссылки и автоматическая привязка к структуре пригласителя - ваш осознанный выбор. Возможность встать в структуру однократная. Второго шанса ввести ссылку не будет - поле ввода станет недоступно. Дважды проверьте корректность вводимой ссылки.</P>

        <?php } else { ?>
            <P class="alert-info alert">Вы уже в структуре</P>

        <?php } ?>

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
