<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;


$menu = [
    [
        'route' => 'cabinet/index',
        'label' => Yii::t('c.IaXmubZn21', 'Кабинет'),
        'urlList' => [
            ['route', 'cabinet/index'],
        ],
    ],
    [
        'route' => 'cabinet/profile',
        'label' => Yii::t('c.IaXmubZn21', 'Редактировать профиль'),
        'urlList' => [
            ['route', 'cabinet/profile'],
        ],
    ],
    [
        'route' => 'cabinet-google-code/set',
        'label' => Yii::t('c.IaXmubZn21', 'Двухфакторная авторизация'),
        'urlList' => [
            ['route', 'cabinet-google-code/set'],
        ],

    ],
    [
        'route' => 'cabinet-telegram/index',
        'label' => 'Информационный Telegram-bot',
        'urlList' => [
            ['controller', 'cabinet-telegram'],
        ],
    ],
    [
        'route' => 'cabinet/profile-shop',
        'label' => 'Данные для оформления доставки',
        'urlList' => [
            ['route', 'cabinet/profile-shop'],
        ],

    ],
    [
        'route' => 'cabinet-support/chat',
        'label' => Yii::t('c.IaXmubZn21', 'Чат поддержки'),
        'urlList' => [
            ['controller', 'cabinet-support'],
        ],
    ],
    [
        'route' => 'cabinet-profile-phone/index',
        'label' => Yii::t('c.IaXmubZn21', 'Телефон'),
        'urlList' => [
            ['controller', 'cabinet-profile-phone'],
        ],
    ],
    [
        'route' => 'cabinet-notepad/index',
        'label' => Yii::t('c.IaXmubZn21', 'Записная книжка'),
        'urlList' => [
            ['controller', 'cabinet-notepad'],
        ],
    ],
];

$menu = \yii\helpers\ArrayHelper::merge($menu, [
    [
        'route'   => 'cabinet/referal-link',
        'label'   => 'Реферальная ссылка',
        'urlList' => [
            ['startsWith', '/cabinet/referal-link'],
        ],
    ],
    [
        'route'   => 'cabinet/referal-structure',
        'label'   => 'Реферальная структура',
        'urlList' => [
            ['startsWith', '/cabinet/referal-structure'],
        ],
    ],
    [
        'route'   => 'cabinet/referal-transactions',
        'label'   => 'Реферальные начисления',
        'urlList' => [
            ['startsWith', '/cabinet/referal-transactions'],
        ],
    ],

]);

function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<div class="list-group">
    <?php
    foreach ($menu as $item) {
        $options = ['class' => ['list-group-item']];
        if (hasRoute12($item, Yii::$app->requestedRoute)) {
            $options['class'][] = 'active';
        }
        if (\yii\helpers\ArrayHelper::keyExists('data', $item)) {
            $options['data'] = $item['data'];
        }
        $options['class'] = join(' ', $options['class']);
        echo Html::a($item['label'], [$item['route']], $options);
    }
    ?>
</div>
