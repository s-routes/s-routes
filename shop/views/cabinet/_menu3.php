<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\Url;

/* меню в кабинете, табуляция, недоделана */



/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;


$menu = [
    [
        'route' => 'cabinet-exchange/index',
        'label' => 'Обменник',
        'urlList' => [
            ['route', 'cabinet-exchange/index'],
            ['route', 'cabinet-exchange/deal-action'],
        ],

    ],
    [
        'route' => 'cabinet-exchange/my-deals',
        'label' => 'Мои сделки',
        'urlList' => [
            ['route', 'cabinet-exchange/my-deals'],
            ['route', 'cabinet-exchange/my-deals-edit'],
        ],
    ],
    [
        'route' => 'cabinet-exchange/offer',
        'label' => 'Мои предложения',
        'urlList' => [
            ['route', 'cabinet-exchange/offer'],
            ['route', 'cabinet-exchange/offer-add'],
            ['route', 'cabinet-exchange/offer-edit'],
        ],
    ],
];

function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<ul class="nav nav-tabs">
    <?php
    foreach ($menu as $item) {
        $optionsLI = [];
        $options = ['class' => []];
        if (hasRoute12($item, Yii::$app->requestedRoute)) {
            $optionsLI['class'][] = 'active';
        }

        $optionsClass = (isset($optionsLI['class']))? join(' ', $options['class']) : '';
        if ($optionsClass != '') $options['class'] = $optionsClass;
        $optionsLiClass = (isset($optionsLI['class']))? join(' ', $optionsLI['class']) : '';
        if ($optionsLiClass != '') $optionsLI['class'] = $optionsLiClass;

        echo Html::tag(
            'li',
            Html::a($item['label'], [$item['route']], $options),
            $optionsLI
            );

    }
    ?>
</ul>
