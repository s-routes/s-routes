<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = 'Реферальная структура';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);


?>
<style>
    .media {
        border-left: 1px solid #888;
    }
</style>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-12">

        <?php
        $row = \common\models\ShopUserPartner::find()->where(['user_id' => Yii::$app->user->id])->all();
        ?>
        <?php if (count($row) > 0) { ?>
            <p>Ваш аплайнер</p>
            <?php
            $userObject = \common\models\UserAvatar::findOne($row[0]['parent_id']);
            ?>
            <ul class="media-list">
                <li class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object"
                                 src="<?= $userObject->getAvatar() ?>"
                                 style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
                            >
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><?= $userObject->getName2() ?> <code class="buttonCopy" title="Нажмите чтобы скопировать" data-toggle="tooltip" data-clipboard-text="<?= $userObject->getAddress() ?>"><?= $userObject->getAddress() ?></code></h4>
                    </div>
                </li>
            </ul>
        <?php } ?>

        <p>Реферальная структура</p>

        <?= $this->render('referal-structure-one', ['userObject' => Yii::$app->user->identity]) ?>

    </div>
</div>



