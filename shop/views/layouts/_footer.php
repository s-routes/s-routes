<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 21.01.2017
 * Time: 18:16
 */

/** @var \avatar\services\CustomizeService $CustomizeService */
/** @var array $companySocialNets telegram, facebook, vk, skype, youtube */

/**
 * Last Update.
 * User: slpv
 * Date: 12.12.2019
 * Time: 13:29
 */
$user_id = Yii::$app->user->id;
\avatar\assets\Notify::register($this);
$this->registerJs(<<<JS
$('.buttonPay20210424').click(function (e) {
    ajaxJson({
        url: '/site/pay',
        data: {id:1},
        success: function (ret) {
            window.location = ret.url;
        }
    })
});
JS
);
?>

</div>


<style>
    .white {color: #fff;}
</style>
<footer>
    <div class="sections">
        <div class="section">
            <table style="margin-top: 20px;">
                <tr>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="/page/item?id=42" class="white">Как сделать заказ</a></p>
                        <p><a href="/page/item?id=43" class="white">Способы оплаты</a></p>
                        <p><a href="/page/item?id=44" class="white">Доставка</a></p>
                        <p><a href="/page/item?id=45" class="white">Возврат товара</a></p>
                        <p><a href="/page/item?id=46" class="white">Возврат денежных средств</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="/page/item?id=47" class="white">Правила продажи</a></p>
                        <p><a href="/page/item?id=48" class="white">Правила пользования торговой площадкой</a></p>
                        <p><a href="/page/item?id=84" class="white">Инструкция по оплате картой банка</a></p>
                        <p><a href="/site/donate" class="white">Поддержка проекта, донаты, взносы</a></p>
                        <p><a href="/page/item?id=93" class="white">Ссылки на медиаресурсы и соцсети</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="/page/item?id=39" class="white">О нас</a></p>
                        <p><a href="/page/item?id=49" class="white">Вопросы-ответы</a></p>
                        <p><a href="/page/item?id=53" class="white">Контакты</a></p>
                        <p><img src="/images/layouts/footer/Image_25.png" width="150"></p>
                    </td>

                </tr>
            </table>

            <div class="title">© Все права защищены. topmate.one 2020-2021</div>

        </div>
    </div>
</footer>