<?php
use yii\helpers\Url;
?>

<li><a href="/cabinet/index">ПРОФИЛЬ</a></li>
<li><a href="/cabinet-bills/index">МОИ БАЛАНСЫ</a></li>
<li><a href="/cabinet-market/index">КУПИТЬ "ПАКЕТ ВОЗМОЖНОСТЕЙ"</a></li>
<li><a href="/shop/request1">КОРЗИНА ЗАКАЗОВ</a></li>
<li><a href="/cabinet-shop-requests/index">ВСЕ МОИ ЗАКАЗЫ (архив)</a></li>
<li><a href="/page/item?id=86">КАК ВЫГОДНЕЕ ОПЛАТИТЬ ЗАКАЗ</a></li>
<?php if (Yii::$app->user->can('permission_shop_buh')) { ?>
    <li><a href="/buh/index">Бухгалтер</a></li>
<?php } ?>
<?php if (Yii::$app->user->can('permission_shop_manager')) { ?>
    <li><a href="/manager/index">Менеджер</a></li>
<?php } ?>
<li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post">ВЫЙТИ</a></li>
