<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;



?>
<?php  if (!Yii::$app->user->isGuest)  { ?>
    <?php

    $c22 = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::MARKET);

    $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
    $billMAR = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cMAR->id]);
    $walletMAR = \common\models\piramida\Wallet::findOne($billMAR->wallet_id);
    $c22value = Yii::$app->formatter->asDecimal(\common\models\piramida\Currency::getValueFromAtom($walletMAR->amount, \common\models\piramida\Currency::MARKET), 2);

    // если пакет еще не куплен, стартовый пакет
    if (Yii::$app->user->identity->market_till == 0) {
        $typeView = 1;
    } else {
        // если купленный пакет просрочен или кончились деньги
        if ((Yii::$app->user->identity->market_till < time()) || ($walletMAR->amount == 0)) {
            $typeView = 2;
        } else {
            // Все работает
            $typeView = 3;
        }
    }

    if ($typeView == 1) {
        $c22title = 'бессрочно';
    }
    if ($typeView == 2) {
        $c22title = 'Срок действия пакета закончился '. Yii::$app->formatter->asDatetime(Yii::$app->user->identity->market_till);
    }
    if ($typeView == 3) {
        $c22title = 'до '. Yii::$app->formatter->asDatetime(Yii::$app->user->identity->market_till);
    }

    ?>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brands" href="#">
                    <a class="navbar-brands" href="/">
                        <img style="margin-top: 11px; height: 70px;" src="/images/i1.jpg" alt="logo" class="logo_img" height="70">
                    </a>
                </a>

                <li class="hidden-sm hidden-md hidden-lg"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><button type="button" class="btns btn-danger" data-toggle="button"><img src="<?= \Yii::$app->params['rootSite'] ?><?= Yii::$app->user->identity->getAvatar() ?>" class="img-profile" width="30">МОЙ КАБИНЕТ</button></a>
                    <ul class="dropdown-menu">
                        <li class="hidden-xs" style="margin-top: 5px;">
                            <a href="/cabinet-market/index" title="<?= $c22title ?>" data-toggle="tooltip" data-placement="bottom">
                                <?= $c22value ?> <img src="<?= $c22->getImage2() ?>" width="20" class="img-circle">
                            </a>
                        </li>
                        <li>
                            <a href="/shop/request1">
                                <i class="fa fa-shopping-basket rounded-circle bg-white mr-1" style="padding: 7px 6px;">
                                </i>
                                ЗАКАЗ
                            </a>
                        </li>
                        <?= $this->render('_menu') ?>
                    </ul>
                </li>

            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-rss mr-1" style="padding: 7px 9px;"></i>ИНФОРМАЦИЯ</a>
                        <ul class="dropdown-menu">
                            <li><a href="/page/about">О КОМПАНИИ</a></li>
                            <li><a href="/cabinet-support/chat">СЛУЖБА ПОДДЕРЖКИ</a></li>
                            <li><a href="/page/contact">КОНТАКТЫ</a></li>
                        </ul>
                        <li><a href="/"><i class="fa fa-shopping-bag" style="padding: 7px 9px;"></i>МАГАЗИНЫ</a></li>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-xs" style="margin-top: 5px;">
                        <a href="/cabinet-market/index" title="<?= $c22title ?>" data-toggle="tooltip" data-placement="bottom">
                            <?= $c22value ?> <img src="<?= $c22->getImage2() ?>" width="20" class="img-circle">
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a href="/shop/request1">
                            <i class="fa fa-shopping-basket rounded-circle bg-white mr-1" style="padding: 7px 6px;">
                            </i>
                            ЗАКАЗ
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a href="/cabinet-bills/index">
                            <img src="/images/controller/cabinet-bills/index/wallet2.png" width="40" alt=""
                                 data-toggle="tooltip"
                                 title="<?= \Yii::t('c.cKsbnadvgD', 'Мои балансы') ?>"
                                 data-placement="bottom"
                            >
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?= Yii::$app->user->identity->getAvatar() ?>" class="img-profile" width="35">МОЙ КАБИНЕТ</a>
                        <ul class="dropdown-menu">
                            <?= $this->render('_menu') ?>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

<?php } else { ?>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brands" href="#">
                    <a class="navbar-brands" href="/"><img style="margin-top: 11px; height: 70px;" src="/images/i1.jpg" alt="logo"></a>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-sitemap mr-1" style="padding: 7px 9px;"></i>О ПРОЕКТЕ</a>
                        <ul class="dropdown-menu">
                            <li><a href="/page/news">НОВОСТИ</a></li>
                            <li><a href="/page/contact">КОНТАКТЫ</a></li>
                        </ul>
                    </li>
                    <li><a href="/"><i class="fa fa-shopping-bag" style="padding: 7px 9px;"></i>МАГАЗИНЫ</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/auth/login"><i class="fa fa-user rounded-circle bg-white mr-1" style="padding: 7px 9px;"></i>ВОЙТИ</a></li>
                    <li><a href="/auth/registration"><i class="fa fa-user-plus rounded-circle bg-white mr-1" style="padding: 7px 6px;"></i>РЕГИСТРАЦИЯ</a></li>
                </ul>
            </div>
        </div>
    </nav>
<?php } ?>
<div style="flex-grow: 1;">
    <?php
    $tstart30 = Yii::$app->params['technical-pause']['start'] - 30 * 60;
    $tstart = Yii::$app->params['technical-pause']['start'];
    $tfinish = Yii::$app->params['technical-pause']['finish'];
    ?>

    <?php
    $t = time();
    $isShowTechnicalInfo = ($t > $tstart30 && $t < $tfinish);
    ?>

    <div class="row js-technical-pause" style="display: <?= ($isShowTechnicalInfo)? 'block' : 'none' ?>">
        <div class="col-lg-10 col-md-8">
            <p class="alert alert-danger
         " style="
    margin-left: 20px;
    margin-right: 20px;
">Уважаемые пользователи! Мы планируем обновить программное обеспечение с <b><?= Yii::$app->formatter->asDatetime(Yii::$app->params['technical-pause']['start']) ?></b> по <b><?= Yii::$app->formatter->asDatetime(Yii::$app->params['technical-pause']['finish']) ?></b>. Перед началом технических работ запущен таймер. Просьба корректно выйти из системы, т.к. сервисы все равно временно будут недоступны. После окончания техработ, весь функционал будет доступен вам в полном объеме.

            </p>
        </div>
        <div class="col-lg-2 col-md-4">



            <?php
            if ($isShowTechnicalInfo) {
                \avatar\assets\CountDown\Asset::register($this);
                $this->registerJs(<<<JS
var d1 = new Date();
d1.setTime({$tstart}000);
var y = d1.getFullYear();
var m = d1.getMonth() + 1;
if (m < 10) m = '0' + m;
var d = d1.getDate();
var h = d1.getHours();
if (h < 10) h = '0' + h;
var i = d1.getMinutes();
if (i < 10) i = '0' + i;
var s = d1.getSeconds();
if (s < 10) s = '0' + s;
$('.countdown2').downCount({
    date: m + '/' + d + '/' + y + ' ' + h + ':' + i + ':' + s,
    offset: -(d1.getTimezoneOffset() / 60)
}, function () {
    $('.countdown2').html($('<p>').html('Ведутся технические работы'));
});
JS
                );
            }
            ?>

            <ul class="countdown2" style="text-align: left;">
                <li>
                    <span class="minutes">00</span>
                    <p class="minutes_ref">минут</p>
                </li>
                <li class="seperator">:</li>
                <li>
                    <span class="seconds">00</span>
                    <p class="seconds_ref">секунд</p>
                </li>
            </ul>
        </div>
    </div>


    <?php // admin-notify  ?>
    <?php
    $adminNotify = null;
    $adminNotifyData = \common\models\Config::get('admin-notify');
    $isData = false;
    if ($adminNotifyData !== false) {
        if (!is_null($adminNotifyData)) {
            if ($adminNotifyData != '') {
                $isData = true;
            }
        }
    }
    ?>

    <?php if ($isData) { ?>
        <?php
        $adminNotify = \yii\helpers\Json::decode($adminNotifyData);
        $t = time();
        $start_date = $adminNotify['start_date'];
        $start_time = $adminNotify['start_time'];
        $start = DateTime::createFromFormat('d.m.Y H:i', $start_date . ' ' . $start_time);
        $finish_date = $adminNotify['finish_date'];
        $finish_time = $adminNotify['finish_time'];
        $finish = DateTime::createFromFormat('d.m.Y H:i', $finish_date . ' ' . $finish_time);
        $adminNotifyText = $adminNotify['text'];

        $isShowAdminNotify = ($t > $start->format('U') && $t < $finish->format('U'));
        ?>
        <?php if ($isShowAdminNotify) { ?>

            <div class="row js-technical-pause">
                <div class="col-lg-10 col-md-8 col-lg-offset-1 col-md-offset-2">
                    <p class="alert alert-danger" style="
margin-left: 20px;
margin-right: 20px;
"><?= $adminNotifyText ?></p>
                </div>
            </div>

        <?php } ?>
    <?php } ?>


    <?php  if (!Yii::$app->user->isGuest)  { ?>
        <?php if ($typeView == 1) { ?>
            <div class="container">
                <div class="col-lg-12">
                    <p class="alert alert-info">
                        Сейчас вы можете только пополнять счет МБ. Активируйте аккаунт покупкой любого "Пакета возможностей"
                        здесь <a
                                href="https://topmate.one/cabinet-market/index">https://topmate.one/cabinet-market/index</a>
                        и
                        вам станут доступны все опции: online-шопинг, внутренние переводы, зачисление на счет реферальных.
                    </p>
                </div>
            </div>
        <?php } ?>
        <?php if ($typeView == 2) { ?>
            <div class="container">
                <div class="col-lg-12">
                    <p class="alert alert-info">
                        Срок действия пакета
                        закончился <?= Yii::$app->formatter->asDatetime(Yii::$app->user->identity->market_till) ?> Мск
                        (GMT+3).<br><br>

                        Сейчас вы можете только пополнять счет МБ. Активируйте аккаунт покупкой любого "Пакета возможностей"
                        здесь <a
                                href="https://topmate.one/cabinet-market/index">https://topmate.one/cabinet-market/index</a>
                        и
                        вам станут доступны все опции: online-шопинг, внутренние переводы, зачисление на счет реферальных.
                    </p>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
