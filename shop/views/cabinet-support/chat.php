<?php
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */


$this->title = Yii::t('c.zpiXUyz0nd', 'Чат поддержки');

$user_id = Yii::$app->user->id;
$supportChat = \common\models\SupportShop::findOne(['user_id' => $user_id]);
if (is_null($supportChat)) {
    $chatRoom = \common\models\ChatRoom::add(['last_message' => null]);
    $supportChat = \common\models\SupportShop::add([
        'room_id' => $chatRoom->id,
        'user_id' => $user_id,
    ]);
}
$room_id = $supportChat->room_id;

\avatar\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);

$serverName = \avatar\assets\SocketIO\Asset::getHost();

$isShowCharMessage = true;

?>

<!-- Large modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                $id = 81;
                $page = \common\models\exchange\Page::findOne($id);

                $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                if (is_null($pageLang)) {
                    $name = $page->name;
                    $content = $page->content;
                } else {
                    $name = $pageLang->name;
                    $content = $pageLang->content;
                }
                ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $name ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>



<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
    </div>



    <div class="col-lg-8">
        <?= $this->render('@shop/views/cabinet-exchange/chat', [
            'room_id'  => $room_id,
            'user_id'  => $user_id,
            'send_url' => '/cabinet-support/send',
        ]); ?>
    </div>

    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>
