<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\exchange\Assessment;
use common\models\exchange\Deal;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $id int */

$this->title = 'Чат поддержки';

$user_id = $id;
$supportChat = \common\models\SupportShop::findOne(['user_id' => $user_id]);
$room_id = $supportChat->room_id;

\avatar\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);

$serverName = \avatar\assets\SocketIO\Asset::getHost();

$isShowCharMessage = true;

?>


<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>



    <div class="row">

        <div class="col-lg-6" style="margin-top: 50px;">

            <?= $this->render('@shop/views/cabinet-exchange/chat', [
                'room_id'  => $room_id,
                'user_id'  => Yii::$app->user->id,
                'send_url' => '/admin-support/send',
            ]); ?>
    </div>
</div>
