<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $school \common\models\school\School */

$page = \common\models\exchange\Page::findOne(5);

$this->title = $page->name;


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
        <ul class="nav nav-tabs">
            <li><a href="/shop/index">Магазин</a></li>
            <li><a href="/page/dogovor-oferty">Договор Оферты</a></li>
            <li><a href="/page/opisanie-tovarov-uslug">Описание товаров и услуг</a></li>
            <li class="active"><a href="/page/yuridicheskaya-informatsiya">Контакты и реквизиты</a></li>
        </ul>
    </div>

    <div class="col-lg-1">

    </div>
    <div class="col-sm-10">
        <?= $page->content ?>
    </div>
</div>


