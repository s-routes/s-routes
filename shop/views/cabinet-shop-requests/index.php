<?php

/** $this \yii\web\View  */

use yii\helpers\Url;
use app\services\GsssHtml;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\services\GetArticle\Collection;

$this->title = 'Мои заказы на TopMate';

$type_cancel = \common\models\ShopRequest::TYPE_CANCEL_CLIENT;
?>

<div class="container">
    <div class="col-lg-10">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-2">
        <a href="/shop/request1" class="btn btn-success" style="margin-top: 40px;">СОЗДАТЬ НОВЫЙ ЗАКАЗ</a>
    </div>
    <div class="col-lg-12">



    <?php \yii\widgets\Pjax::begin(); ?>
    <?php
    $this->registerJS(<<<JS
$('[data-toggle="tooltip"]').tooltip();

$('.rowTable').click(function() {
    window.location = '/cabinet-shop-requests/view' + '?' + 'id' + '=' + $(this).data('id');
});
$('.buttonCancel').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    if (confirm('Вы подтверждаете действие?')) {
        ajaxJson({
            url: '/cabinet-shop-requests/cancel',
            data: {
                id: $(this).data('id'),
                type_cancel: {$type_cancel}
            },
            success: function (ret) {
                $('#modalInfo').on('hidden.bs.modal', function() {
                    window.location.reload();
                }).modal();
            }
        });
    } 
});
JS
    );
    $sort = new \yii\data\Sort([
        'defaultOrder' => ['id' => SORT_DESC],
    ]);
    ?>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query'      => \common\models\ShopRequest::find()
                ->where(['user_id' => Yii::$app->user->id])
            ,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => $sort,
        ]),
        'tableOptions' => [
            'class' => 'table table-striped table-hover',
        ],
        'rowOptions'   => function ($item) {
            $data = [
                'data'  => ['id' => $item['id']],
                'role'  => 'button',
                'class' => 'rowTable',
            ];
            return $data;
        },
        'columns'      => [
            'id',
            'delivery:text:Адрес доставки',
            [
                'header'         => 'Стоимость заказа',
                'contentOptions' => ['class' => 'text-right'],
                'headerOptions'  => ['class' => 'text-right'],
                'content'        => function ($item) {
                    $currency_id = $item['currency_id'];
                    $cInt = \common\models\piramida\Currency::findOne($currency_id);
                    if (is_null($cInt)) return '';
                    $v = bcdiv($item['price'], bcpow(10, $cInt->decimals), $cInt->decimals_view_shop);

                    return $v;
                },
            ],

            [
                'header'  => '',
                'content' => function ($item) {
                    $currency_id = $item['currency_id'];
                    $c = \common\models\piramida\Currency::findOne($currency_id);
                    if (is_null($c)) return '';

                    return Html::tag('span', $c->code, ['class' => 'label label-info']);
                },
            ],
            [
                'header'  => 'Оплачен?',
                'content' => function ($item) {
                    if (!is_null($item['is_paid'])) {
                        if ($item['is_paid'] == 1) return Html::tag('span', 'Да', ['class' => 'label label-success']);
                    }

                    return Html::tag('span', 'Нет', ['class' => 'label label-default']);
                },
            ],

            [
                'header'  => 'Создано',
                'content' => function ($item) {
                    $v = \yii\helpers\ArrayHelper::getValue($item, 'created_at', 0);
                    if ($v == 0) return '';

                    return Html::tag('abbr', \cs\services\DatePeriod::back($v, ['isShort' => true]), ['data' => ['toggle' => 'tooltip'], 'title' => Yii::$app->formatter->asDatetime($v)]);
                }
            ],
            [
                'header'  => 'Отменить',
                'content' => function ($item) {
                    if (ArrayHelper::getValue($item, 'is_cancel', 0) == 1) {
                        return Html::tag('span', 'Заказ отменен', ['class' => 'label label-warning']);
                    }
                    if (!\cs\Application::isEmpty($item['manager_id'])) {
                        return Html::tag('span', 'Заказ в обработке', ['class' => 'label label-default']);
                    }
                    return Html::button('Отменить', [
                        'class' => 'btn btn-danger btn-xs buttonCancel',
                        'data'  => [
                            'id' => $item['id'],
                        ]
                    ]);
                }
            ],
        ],
    ]) ?>
    <?php \yii\widgets\Pjax::end(); ?>

    </div>
</div>

<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>