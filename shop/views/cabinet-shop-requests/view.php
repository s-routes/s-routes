<?php

/** @var $this \yii\web\View  */
/** @var $request \common\models\ShopRequest */

use cs\services\DatePeriod;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\shop\Request;


$this->title = 'Заказ #' . $request->id;

if (is_null($request->chat_room_id)) {
    $chatRoom = \common\models\ChatRoom::add([
        'last_message' => null,
    ]);
    $request->chat_room_id = $chatRoom->id;
    $request->save();
}

$room_id = $request->chat_room_id;

$user_id = Yii::$app->user->id;

\avatar\assets\Notify::register($this);

$isShowCharMessage = true;
?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


        <?php
        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::RUB);
        $user = \common\models\UserAvatar::findOne($request->user_id);
        $c2 = \common\models\avatar\Currency::findOne($request->currency_id_paid);
        $c2text = (is_null($c2))? '' : $c2->code;
        ?>
        <?= \yii\widgets\DetailView::widget([
            'model' => $request,
            'attributes' => [
                [
                    'attribute'      => 'id',
                    'captionOptions' => ['style' => 'width: 30%'],
                ],
                'created_at:datetime:Создан',
                [
                    'label'      => 'Сумма',
                    'attribute'   => 'price',
                    'format'      => ['decimal', $c->decimals_view_shop],
                    'value'       => $request->price / pow(10, $c->decimals),
                ],
                [
                    'label'      => 'Валюта заказа на маркетплейсе указана в',
                    'attribute'   => 'currency_id',
                    'format'      => 'html',
                    'value'       => Html::tag('span', $c->code, ['class' => 'label label-info']),
                ],
                [
                    'label'      => 'Оплата заказа произведена в',
                    'attribute'   => 'currency_id_paid',
                    'format'      => 'html',
                    'value'       => Html::tag('span', $c2text, ['class' => 'label label-info']),
                ],
                [
                    'label'      => 'Пользователь',
                    'attribute'   => 'user_id',
                    'format'      => 'html',
                    'value'       => Html::img($user->getAvatar(), ['class' => 'img-circle', 'width' => 30, 'title' => $user->getName2() . ' (' . $user->getAddress() . ')']) . $user->getName2() . ' (' . $user->getAddress() . ')',
                ],
                'delivery:text:Адрес доставки',
                'delivery_dop:text:Дополнительная информация и пожелания по доставке',
                'user_name:text:Имя',
                'user_phone:text:Телефон',
                'user_email:text:Email',
                'user_telegram:text:Telegram',
                'user_whatsapp:text:Whatsapp',
            ]
        ]) ?>

        <h2 class="page-header">Список товаров</h2>


        <?php if ($request->type == 1) { ?>
            <?php $c = 0; ?>
            <?php $sum = 0; ?>
            <?php foreach (\common\models\ShopRequestProduct::find()->where(['request_id' => $request->id])->all() as $p) { ?>
                <div class="row rowProduct" data-count="<?= $c ?>" style="margin-bottom: 20px;">
                    <div class="col-lg-8"><?= $p['link'] ?></div>
                    <div class="col-lg-2"><?= $p['price'] ?> RUB</div>
                    <div class="col-lg-2"><?= $p['count'] ?> шт</div>
                </div>
                <?php $sum += $p['price'] * $p['count']; ?>
                <?php $c++; ?>
            <?php } ?>
        <?php } ?>
        <?php if ($request->type == 2) { ?>
            <?php
            $info = \yii\helpers\Json::decode($request->info);
            $t = $info['type'];
            $sum = \common\models\piramida\Currency::getValueFromAtom($request->price, $request->currency_id);
            ?>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-lg-8"><a href="<?= \common\models\ShopRequest::$_phoneLink[$t] ?>" target="_blank">Пополнение баланса номера телефона сотовой связи (мобильной SIM-карты) <?= \common\models\ShopRequest::$_phone[$t] ?></a></div>
                <div class="col-lg-2"><?= \common\models\piramida\Currency::getValueFromAtom($request->price, $request->currency_id) ?> RUB</div>
            </div>
        <?php } ?>


        <?php foreach (\common\models\ShopRequestAdd::find()->where(['request_id' => $request->id])->all() as $p) { ?>

            <div class="row rowProduct2" style="margin-bottom: 20px;">
                <div class="col-lg-8"><?= $p['comment'] ?></div>
                <?php
                $cio = \common\models\CurrencyIO::findFromExt($p['currency_id']);
                $price = \common\models\piramida\Currency::getValueFromAtom($p['amount'], $cio->currency_int_id);
                ?>
                <div class="col-lg-2"><?= Yii::$app->formatter->asDecimal($price, 2) ?> RUB</div>
            </div>
            <?php $sum += $price; ?>
        <?php } ?>


        <?php if ($request->type == 1) { ?>
            <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
                <div class="col-lg-8 text-right" style="font-weight: bold">Итого:</div>
                <div class="col-lg-4" style="font-weight: bold"><span class="js-product-list-sum"><?= Yii::$app->formatter->asDecimal($sum, 2) ?></span> RUB</div>
            </div>
        <?php } ?>
        <?php if ($request->type == 2) { ?>
            <?php
            $info = \yii\helpers\Json::decode($request->info);
            $t = $info['type'];
            ?>
            <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
                <div class="col-lg-6" >Для оплаты перейдите на сайт мобильного оператора:
                    <a href="<?= \common\models\ShopRequest::$_phoneLink[$t] ?>" target="_blank">
                        <img src="<?= \common\models\ShopRequest::$_phoneImage[$t] ?>" height="30">
                    </a>
                </div>
                <div class="col-lg-2 text-right" style="font-weight: bold">Итого:</div>
                <div class="col-lg-4" style="font-weight: bold"><span class="js-product-list-sum"><?= Yii::$app->formatter->asDecimal($sum, 2) ?></span> RUB</div>
            </div>
        <?php } ?>

        <?php if (ArrayHelper::getValue($request, 'is_cancel', 0) == 0) { ?>
            <div class="row" style="margin-top: 20px; margin-bottom: 50px;">
                <div class="col-lg-8 text-right" style="font-weight: bold; margin-top: 8px;">
                    <p>При необходимости доплаты по заказу, например за курьерскую доставку, нажмите на кнопку</p>
                </div>
                <div class="col-lg-4" style="font-weight: bold"><a class="btn btn-success" href="add?id=<?= $request->id ?>">ДОПЛАТА</a></div>
            </div>
        <?php } ?>



        <?= $this->render('../cabinet-exchange/chat', [
            'room_id'  => $room_id,
            'user_id'  => $user_id,
            'send_url' => '/cabinet-shop-requests/send',
        ]); ?>

    </div>
</div>
