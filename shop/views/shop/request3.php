<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \shop\models\forms\Request3 */

$this->title = 'Шаг 3: УКАЖИТЕ АДРЕС И ДОП.ИНФО ДЛЯ ДОСТАВКИ';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">


        <?php if (Yii::$app->user->isGuest) { ?>

            <?php $model = new \shop\models\forms\Request3(); ?>
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model,
                'formUrl' => '/shop/request3-guest',
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/shop/request4';
    }).modal();
}
JS
            ]); ?>

            <?= $form->field($model, 'address') ?>

            <hr>

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Далее']); ?>


        <?php } else { ?>
            <?php $model3 = new \shop\models\forms\Request3Login() ?>
            <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                'model'   => $model3,
                'formUrl' => '/shop/request3-login',
                'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/shop/request4';
    }).modal();
}
JS
                ,
            ]); ?>

            <?= $form->field($model3, 'type_id')->radioList([
                \shop\models\forms\Request3Login::TYPE_ID_CABINET_DATA => 'Вариант 1: Оформление через менеджера. Использовать свои данные из личного кабинета TopMate.',
                \shop\models\forms\Request3Login::TYPE_ID_USER_DATA    => 'Вариант 2: Оформление через менеджера. Указать новые контактные данные для оформления доставки.',
                \shop\models\forms\Request3Login::TYPE_ID_MY_CABINET   => 'Вариант 3: Данные не указываю. Оформлю самостоятельно через ЛК интернет-магазина.',
            ])->label('Выберите вариант') ?>
            <p>Просьба учесть: если Вы не хотите тратить время на самостоятельное оформление заказа через Ваш личный кабинет в интернет-магазине, а планируете поручить эту работу менеджеру TopMate, стоимость такой услуги составит 300 маркет-баллов (МБ), за каждый заказ. Если вы выберете варианты оформления 1-2 в ШАГе.1/ШАГе.2 - cервис автоматически добавит эту сумму к оплате.
                Вариант 3 - бесплатный - так как заказ Вы оформляете самостоятельно.</p>
            <?php
            $this->registerJs(<<<JS

$('input[name="Request3Login[type_id]"]').on('change', function(e) {
    if ($(this).val() == 1) {
        $('#formRequest3Login').collapse('hide');
    }
    if ($(this).val() == 2) {
        $('#formRequest3Login').collapse('show');
    }
    if ($(this).val() == 3) {
        $('#formRequest3Login').collapse('hide');
    }
});

$('input[name="Request3Login[type_id]"]').each(function(i,e) {
    if ($(this).val() == 1) {
        $(e).prop('checked', 'checked');
    }
})
JS
            )
            ?>
            <div class="collapse" id="formRequest3Login">
                <?= $form->field($model3, 'address')->textarea(['rows' => 3]) ?>
                <?= $form->field($model3, 'dop')->textarea(['rows' => 3]) ?>
            </div>

            <hr>

            <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Далее']); ?>
        <?php } ?>




    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
