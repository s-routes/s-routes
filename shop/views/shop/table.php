<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */


$this->title = 'Магазины';



?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>

        <?= $this->render('_menu3') ?>


    </div>

    
    
    <div class="row retailers retailers_params mt-3">
        <div class="col-md-8 retailers__title">
            <h2 class="retailers__title-header">Магазины партнеры</h2>
        </div>
        <div class="col-md-4 retailers__search">

        </div>
    </div>

    <div class="row retailers retailers_params mt-3">

        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\ShopAll::find()
                ->where(['type' => \common\models\ShopAll::TYPE_FEDERAL])
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'columns' => [
                'id',
                [
                    'attribute' => 'link',
                    'content'   => function ($i) {
                        if (\yii\helpers\StringHelper::startsWith($i['link'], 'http')) {
                            $link = $i['link'];
                        } else {
                            $link = 'http://' . $i['link'];
                        }
                        return Html::a($link, $link, ['target' => '_blank']);
                    },
                ],
                'description:text:Кратко',
                [
                    'header'    => 'Описание',
                    'attribute' => 'about',
                    'content'   => function ($i) {
                        return nl2br($i['about']);
                    },
                ],
            ],
        ]) ?>

    </div>


</div>


