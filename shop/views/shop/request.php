<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \shop\models\forms\Request */

$this->title = 'Шаг 1: СОЗДАЙТЕ НОВЫЙ ЗАКАЗ';

\avatar\assets\Notify::register($this);

$this->registerJs(<<<JS
var functionDelete = function(e) {
    var c = $(this).parent().parent().data('id');
    ajaxJson({
        url: '/shop/cart-delete2',
        data: {id: c},
        success: function(ret) {
            $('.rowProduct[data-id='+c+']').remove();
            $('.js-product-list-sum').html(ret.sumFormatted);
            
            // выставляю порядковый номер
            var c1 = 1;
            $('.js-product-list-item').each(function(i,e) {
                $(e).html(c1);
                c1++;
            });
        }
    });
};
$('.buttonDelete').click(functionDelete);
JS
);

$data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
if ($data['is_guest'] == false) {
    $user_id = Yii::$app->user->id;
} else {
    $user_id = $data['user_id'];
}
?>
<!-- Large modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                $id = 85;
                $page = \common\models\exchange\Page::findOne($id);

                $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                if (is_null($pageLang)) {
                    $name = $page->name;
                    $content = $page->content;
                } else {
                    $name = $pageLang->name;
                    $content = $pageLang->content;
                }
                ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $name ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>

<?php
$this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-8">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>
    <div class="col-lg-2">
        <h1 class="page-header text-center">
            <a href="/page/item?id=86" class="btn btn-success">КАК ВЫГОДНЕЕ ОПЛАЧИВАТЬ</a>

        </h1>
    </div>
    <div class="col-lg-2">
        <h1 class="text-center">
            <a href="#" id="modalHelp"><i title="<?= \Yii::t('c.57GUXH4BTk', 'Помощь') ?>" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 25px;"></i></a>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">
        <p class="alert alert-danger">Внимание! Просьба номенклатуру заказа составлять из ассортимента одного интернет-магазина. Не включайте в перечень ссылки на товары из разных магазинов. Это очень затрудняет работу менеджеров, и может привести к отмене вашего заказа. Соблюдайте правило: "Для каждого магазина - отдельный заказ"</p>
        <div class="js-product-list">
            <?php $rows = \common\models\ShopTempProduct::find()->where(['user_id' => $user_id])->all(); ?>
            <?php $c = 1; ?>
            <?php $sum = 0; ?>
            <?php foreach ($rows as $p) { ?>
                <div class="row rowProduct" data-id="<?= $p['id'] ?>">
                    <div class="col-lg-1 js-product-list-item"><?= $c ?></div>
                    <div class="col-lg-7"><?= $p['link'] ?></div>
                    <div class="col-lg-1"><?= $p['price'] ?> RUB</div>
                    <div class="col-lg-1"><?= $p['count'] ?> шт</div>
                    <div class="col-lg-2"><button class="btn btn-default btn-xs buttonDelete">Удалить</button></div>
                </div>
                <?php $sum += $p['price'] * $p['count']; ?>
                <?php $c++; ?>
            <?php } ?>
        </div>
        <div class="row" style="margin-top: 20px;">
            <div class="col-lg-8 text-right" style="font-weight: bold">Итого:</div>
            <div class="col-lg-4" style="font-weight: bold"><span class="js-product-list-sum"><?= Yii::$app->formatter->asDecimal($sum, 2) ?></span> RUB</div>
        </div>
        <hr>
        <?php $model = new \shop\models\forms\Request(); ?>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'formUrl' => 'request-add',
            'success' => <<<JS
function (ret) {
    $('.js-product-list').append(
        $('<div>', {class:'row rowProduct', "data-id": ret.product.id})
        .append($('<div>', {class:'col-lg-1 js-product-list-item'}).html('1'))
        .append($('<div>', {class:'col-lg-7'}).html(ret.product.link))
        .append($('<div>', {class:'col-lg-1'}).html(ret.product.price + ' RUB'))
        .append($('<div>', {class:'col-lg-1'}).html(ret.product.count + ' шт'))
        .append($('<div>', {class:'col-lg-2'}).html($('<button>', {class:'btn btn-default btn-xs buttonDelete'}).html('Удалить').on('click', functionDelete)))
    );
    $('#request-link').val('');
    $('#request-price').val('');
    $('#request-count').val('');
    $('.buttonOrder').show();
    
    $('.js-product-list-sum').html(ret.sumFormatted);
    
    // выставляю порядковый номер
    var c2 = 1;
    $('.js-product-list-item').each(function(i,e) {
        $(e).html(c2);
        c2++;
    });
}
JS
,
        ]); ?>

        <div class="js-original-product">
            <?= $form->field($model, 'link') ?>
            <div class="row">
                <div class="col-lg-4">
                    <?= $form->field($model, 'price') ?>
                </div>
                <div class="col-lg-4">
                    <p>Стоимость в RUB или в МАРКЕТ-БАЛЛАХ (1МАРКЕТ-БАЛЛ = 1RUB)</p>
                </div>
                <div class="col-lg-4">
                    <?= $form->field($model, 'count') ?>
                </div>
            </div>
        </div>


<?php
$this->registerJs(<<<JS
$('.buttonOrder').click(function(e) {
    if ($('.js-product-list .rowProduct').length > 0) {
        window.location = '/shop/request2';
    } else {
        $('#modalInfo').modal();
    }
});
JS
)
?>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Добавить в список заказов корзины']); ?>
        <hr>
        <?php
        $isShow = false;
        if (count($rows) >= 0) {
            $isShow = true;
        }
        ?>
        <button class="btn btn-default buttonOrder" data-action="add" style="width: 100%; display: <?= $isShow ? 'block' : 'none' ?>;">Приступить к оформлению покупок</button>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Ошибка</h4>
            </div>
            <div class="modal-body">
                Вам нужно добавить хотя бы один товар (указать ссылку, цену и кол-во и нажать кнопку "Добавить в список заказов корзины")
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

