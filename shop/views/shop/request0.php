<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $model \shop\models\forms\Request2 */

$this->title = 'Вход в систему';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">


        <div>
            <p><span style="color: red">Важно</span>! Убедитесь, что вы находитесь на сайте <a href="https://topmate.one">https://topmate.one</a></p>

            <p>Пожалуйста заполните следующие поля:</p>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Войти</a></li>
                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Зарегистрироваться</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home" style="padding-top: 50px;">
                    <?php $model2 = new \shop\models\forms\Login() ?>
                    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model'   => $model2,
                        'formUrl' => '/shop/login',
                        'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/shop/request';
    }).modal();
}
JS
                        ,
                    ]); ?>

                    <?= $form->field($model2, 'username') ?>
                    <?= $form->field($model2, 'password')->passwordInput() ?>

                    <hr>

                    <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Войти']); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile" style="padding-top: 50px;">
                    <?php $model3 = new \shop\models\forms\Request4Guest() ?>

                    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
                        'model'   => $model3,
                        'formUrl' => '/shop/request4-guest',
                        'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/shop/request';
    }).modal();
}
JS
                        ,
                    ]); ?>

                    <?= $form->field($model3, 'email') ?>
                    <?= $form->field($model3, 'type_id')->radioList([
                        \shop\models\forms\Request4Guest::TYPE_ID_AUTO_PASSWORD => 'Сгенерировать пароль и выслать на почту',
                        \shop\models\forms\Request4Guest::TYPE_ID_USER_PASSWORD => 'Указать свой пароль',
                    ])->label('Какой пароль вы хотите для своего кабинета?') ?>

                    <?php
                    $this->registerJs(<<<JS

$('input[name="Request4Guest[type_id]"]').on('change', function(e) {
    if ($(this).val() == 1) {
        $('#formRequest4Guest').collapse('hide');
    }
    if ($(this).val() == 2) {
        $('#formRequest4Guest').collapse('show');
    }
});

$('input[name="Request4Guest[type_id]"]').each(function(i,e) {
    if ($(this).val() == 1) {
        $(e).prop('checked', 'checked');
    }
})
JS
                    )
                    ?>
                    <div class="collapse" id="formRequest4Guest">
                        <?= $form->field($model3, 'password1')->passwordInput() ?>
                        <?= $form->field($model3, 'password2')->passwordInput() ?>
                    </div>

                    <hr>

                    <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Далее']); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
