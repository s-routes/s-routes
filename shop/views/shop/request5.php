<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this \yii\web\View */

$this->title = 'Шаг 4: ВЫБЕРИТЕ ВАРИАНТ ОПЛАТЫ И ОПЛАТИТЕ ЗАКАЗ';

\avatar\assets\Notify::register($this);

// считаю сумму
$data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
if ($data['is_guest'] == false) {
    $user_id = Yii::$app->user->id;
} else {
    $user_id = $data['user_id'];
}
$sum = 0;
$rows = \common\models\ShopTempProduct::find()->where(['user_id' => $user_id])->all();
foreach ($rows as $row) {
    $sum += $row['price'] * $row['count'];
}
// если платный заказ
if (\common\base\Shop::isPaidManager()) {
    $sum += 300;
}

?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">

        <?php if (Yii::$app->user->isGuest) { ?>

            <p><a href="#">Оплата через платежный шлюз</a></p>

        <?php } else { ?>
            <p>Заказы в корзине на сумму: <?= Yii::$app->formatter->asDecimal($sum, 2) ?> RUB</p>
            <p>Выберите как вы хотите оплатить:</p>
            <div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Маркет баллы</a></li>
                    <li role="presentation"><a href="#home-rub" aria-controls="home-rub" role="tab" data-toggle="tab">Рубли</a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home" style="margin: 20px;">
                        <?php
                        $data = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::MARKET);
                        /** @var \common\models\piramida\Wallet $wallet */
                        $wallet = $data['wallet'];
                        $this->registerJs(<<<JS
$('.buttonPay').on('click', function(e) {
    ajaxJson({
        url: '/shop/request5-pay',
        data: {id: 1},
        success: function(ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location = '/cabinet-shop-requests/view?id=' + ret.request.id;
            }).modal();
        }, 
        errorScript: function(ret) {
            if (ret.id == 102) {
                var value2;
                for (var key2 in ret.data) {
                    if (ret.data.hasOwnProperty(key2)) {
                        var name2 = key2;
                        value2 = ret.data[key2];
                    }
                }
                
                $('#modalError .modal-body').html($('<p>').html(value2.join('')));
                $('#modalError').modal();
            }
        }
    });
});
JS
)
                        ?>
                        <p>На вашем счете находится <?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), 2) ?> <span class="label label-info">MARKET</span></p>

                        <p>Будет списано <?= Yii::$app->formatter->asDecimal($sum, 2) ?> MARKET</p>

                        <p><button class="btn btn-success buttonPay" type="button">Оплатить</button></p>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="home-rub" style="margin: 20px;">
                        <?php
                        $data = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::RUB);
                        /** @var \common\models\piramida\Wallet $wallet */
                        $wallet = $data['wallet'];
                        $this->registerJs(<<<JS
$('.buttonPayRub').on('click', function(e) {
    ajaxJson({
        url: '/shop/request5-pay-rub',
        data: {id: 1},
        success: function(ret) {
            $('#modalInfo').on('hidden.bs.modal', function() {
                window.location = '/cabinet-shop-requests/view?id=' + ret.request.id;
            }).modal();
        }, 
        errorScript: function(ret) {
            if (ret.id == 102) {
                var value2;
                for (var key2 in ret.data) {
                    if (ret.data.hasOwnProperty(key2)) {
                        var name2 = key2;
                        value2 = ret.data[key2];
                    }
                }
                
                $('#modalError .modal-body').html($('<p>').html(value2.join('')));
                $('#modalError').modal();
            }
        }
    });
});
JS
)
                        ?>
                        <p>На вашем счете находится <?= Yii::$app->formatter->asDecimal($wallet->getAmountWithDecimals(), 2) ?> <span class="label label-info">RUB</span></p>

                        <p>Будет списано <?= Yii::$app->formatter->asDecimal($sum, 2) ?> RUB</p>

                        <p><button class="btn btn-success buttonPayRub" type="button">Оплатить</button></p>
                    </div>


                </div>
            </div>
        <?php } ?>

    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
