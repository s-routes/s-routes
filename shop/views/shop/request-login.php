<?php
/**
 * Created by PhpStorm.
 * User: ramha
 * Date: 17.11.2020
 * Time: 8:44
 */

?>
<p>Если вы имеете аккаунт вы можете авторизоваться</p>
<p><button class="btn btn-default buttonLogin1">Войти</button></p>
<?php
$this->registerJs(<<<JS
$('.buttonLogin1').click(function(e) {
    $('.js-login').collapse('toggle');
});
JS
)
?>
<div class="collapse js-login">
    <?php $model2 = new \shop\models\forms\Login() ?>
    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
        'model'                 => $model2,
        'formUrl' => '/shop/login',
        'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location.reload();
    }).modal();
}
JS
        ,
    ]); ?>

    <?= $form->field($model2, 'username') ?>
    <?= $form->field($model2, 'password')->passwordInput() ?>

    <hr>

    <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Войти']); ?>
</div>

<hr>

