<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = 'Регистрация';


?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2">

        <?php $model3 = new \shop\models\forms\Request4Guest() ?>

        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model3,
            'formUrl' => '/shop/request4-guest',
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/shop/request5';
    }).modal();
}
JS
            ,
        ]); ?>

        <?= $form->field($model3, 'type_id')->radioList([
            \shop\models\forms\Request4Guest::TYPE_ID_AUTO_PASSWORD => 'Сгенерировать пароль и выслать на почту',
            \shop\models\forms\Request4Guest::TYPE_ID_USER_PASSWORD => 'Указать свой пароль',
        ])->label('Какой пароль вы хотите для своего кабинета?') ?>

        <?php
        $this->registerJs(<<<JS

$('input[name="Request4Guest[type_id]"]').on('change', function(e) {
    if ($(this).val() == 1) {
        $('#formRequest4Guest').collapse('hide');
    }
    if ($(this).val() == 2) {
        $('#formRequest4Guest').collapse('show');
    }
});

$('input[name="Request4Guest[type_id]"]').each(function(i,e) {
    if ($(this).val() == 1) {
        $(e).prop('checked', 'checked');
    }
})
JS
        )
        ?>
        <div class="collapse" id="formRequest4Guest">
            <?= $form->field($model3, 'password1')->passwordInput() ?>
            <?= $form->field($model3, 'password2')->passwordInput() ?>
        </div>

        <hr>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Далее']); ?>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
