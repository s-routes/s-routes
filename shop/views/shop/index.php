<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */


$this->title = '#TOPMATE - ПЛАТФОРМА ОРГАНИЗАЦИИ ONLINE-ПЛАТЕЖЕЙ';

$ritailers = [
    [
        'nohover' => '1-1.png',
        'hover'   => '1-2.png',
        'name'    => 'Wildberries',
        'url'     => 'https://wildberries.ru',
    ],
    [
        'nohover' => '2-1.png',
        'hover'   => '2-2.png',
        'name'    => 'citilink.ru',
        'url'     => 'https://citilink.ru',
    ],
    [
        'nohover' => '3-1.png',
        'hover'   => '3-2.png',
        'name'    => 'mvideo.ru',
        'url'     => 'https://mvideo.ru',
    ],
    [
        'nohover' => '4-1.png',
        'hover'   => '4-2.png',
        'name'    => 'ozon.ru',
        'url'     => 'https://ozon.ru',
    ],
    [
        'nohover' => '5-1.png',
        'hover'   => '5-2.png',
        'name'    => 'dns-shop.ru',
        'url'     => 'https://dns-shop.ru',
    ],
    [
        'nohover' => '6-1.png',
        'hover'   => '6-2.png',
        'name'    => 'lamoda.ru',
        'url'     => 'https://lamoda.ru',
    ],
    [
        'nohover' => '7-1.png',
        'hover'   => '7-2.png',
        'name'    => 'eldorado.ru',
        'url'     => 'https://eldorado.ru',
    ],
    [
        'nohover' => '8-1.png',
        'hover'   => '8-2.png',
        'name'    => 'svyaznoy.ru',
        'url'     => 'https://svyaznoy.ru',
    ],
    [
        'nohover' => '9-1.png',
        'hover'   => '9-2.png',
        'name'    => 'technopoint.ru',
        'url'     => 'https://technopoint.ru',
    ],
    [
        'nohover' => '10-1.png',
        'hover'   => '10-2.png',
        'name'    => 'petrovich.ru',
        'url'     => 'https://petrovich.ru',
    ],
    [
        'nohover' => '11-1.png',
        'hover'   => '11-2.png',
        'name'    => 'vseinstrumenti.ru',
        'url'     => 'https://vseinstrumenti.ru',
    ],
    [
        'nohover' => '12-1.png',
        'hover'   => '12-2.png',
        'name'    => 'onlinetrade.ru',
        'url'     => 'https://onlinetrade.ru',
    ],
    [
        'nohover' => '13-1.png',
        'hover'   => '13-2.png',
        'name'    => 'apteka.ru',
        'url'     => 'https://apteka.ru',
    ],
    [
        'nohover' => '14-1.png',
        'hover'   => '14-2.png',
        'name'    => 'bonprix.ru',
        'url'     => 'https://bonprix.ru',
    ],
    [
        'nohover' => '15-1.png',
        'hover'   => '15-2.png',
        'name'    => 'utkonos.ru',
        'url'     => 'https://utkonos.ru',
    ],
    [
        'nohover' => '16-1.png',
        'hover'   => '16-2.png',
        'name'    => 'komus.ru',
        'url'     => 'https://komus.ru',
    ],
    [
        'nohover' => '17-1.png',
        'hover'   => '17-2.png',
        'name'    => 'ulmart.ru',
        'url'     => 'https://ulmart.ru',
    ],
    [
        'nohover' => '18-1.png',
        'hover'   => '18-2.png',
        'name'    => 'detmir.ru',
        'url'     => 'https://detmir.ru',
    ],
    [
        'nohover' => '19-1.png',
        'hover'   => '19-2.png',
        'name'    => 'sima-land.ru',
        'url'     => 'https://sima-land.ru',
    ],
    [
        'nohover' => '20-1.png',
        'hover'   => '20-2.png',
        'name'    => 'ikea.com',
        'url'     => 'https://ikea.com',
    ],
    [
        'nohover' => '21-1.png',
        'hover'   => '21-2.png',
        'name'    => 'witt-magazine.ru',
        'url'     => 'https://witt-magazine.ru',
    ],
    [
        'nohover' => '22-1.png',
        'hover'   => '22-2.png',
        'name'    => 'labirint.ru',
        'url'     => 'https://labirint.ru',
    ],
    [
        'nohover' => '23-1.png',
        'hover'   => '23-2.png',
        'name'    => 'shop.mts.ru',
        'url'     => 'https://shop.mts.ru',
    ],
    [
        'nohover' => '24-1.png',
        'hover'   => '24-2.png',
        'name'    => 'holodilnik.ru',
        'url'     => 'https://holodilnik.ru',
    ],
    [
        'nohover' => '25-1.png',
        'hover'   => '25-2.png',
        'name'    => 'kupivip.ru',
        'url'     => 'https://kupivip.ru',
    ],
    [
        'nohover' => '26-1.png',
        'hover'   => '26-2.png',
        'name'    => 'petshop.ru',
        'url'     => 'https://petshop.ru',
    ],
    [
        'nohover' => '27-1.png',
        'hover'   => '27-2.png',
        'name'    => 'kolesa-darom.ru',
        'url'     => 'https://kolesa-darom.ru',
    ],
    [
        'nohover' => '28-1.png',
        'hover'   => '28-2.png',
        'name'    => 'hoff.ru',
        'url'     => 'https://hoff.ru',
    ],
    [
        'nohover' => '29-1.png',
        'hover'   => '29-2.png',
        'name'    => 'eurosed.ru',
        'url'     => 'https://eurosed.ru/',
    ],
    [
        'nohover' => '30-1.png',
        'hover'   => '30-2.png',
        'name'    => '220-volt.ru',
        'url'     => 'https://220-volt.ru',
    ],
];

?>
<div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                $id = 64;
                $page = \common\models\exchange\Page::findOne($id);

                $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                if (is_null($pageLang)) {
                    $name = $page->name;
                    $content = $page->content;
                } else {
                    $name = $pageLang->name;
                    $content = $pageLang->content;
                }
                ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $name ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(<<<JS
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12" style="margin-bottom: 30px;">
        <h1 class="page-header text-center" style="font-size: 250%;">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
        <p class="text-center lead">ЛЮБЫЕ МАРКЕТПЛЕЙСЫ ДЛЯ ВАШЕГО ИНТЕРНЕТ-ШОПИНГА</p>



        <?= $this->render('_menu3') ?>

    </div>


    <div class="row retailers retailers_params mt-3">
        <div class="col-md-8">
            <p class="text-center"><a href="/shop/table" class="btn btn-primary" style="width: 100%;">Список наиболее крупных федеральных и сетевых магазинов</a></p>
        </div>
        <div class="col-md-4">
            <p><a href="/shop/request0" class="btn btn-success" style="width: 100%;">Создать заказ</a></p>
        </div>
    </div>

    <div class="row retailers retailers_params mt-3">

        <?php
        $this->registerJs('var v1;');
        $count = 0;
        function echoCount($count){echo $count;}
        function echoHover($hover){
            echo $hover;
        }
        function echoNoHover($noHover){
            echo $noHover;
        }
        function echoName($name){
            echo $name;
        }
        function echoUrl($url){
            echo $url;
        }
        ?>
        <?php foreach ($ritailers as $ritailersItem) { ?>
            <?php
            $hover = $ritailersItem['hover'];
            $noHover = $ritailersItem['nohover'];
            $name = $ritailersItem['name'];
            $url = $ritailersItem['url'];
            ?>

            <div class="col-xs-6 col-sm-4 col-md-2 retailers__item retailers__item_params">
                <a
                    href="<?php echoUrl($url);?>"
                    id="retailers_item_<?php echoCount($count);?>"
                    target="_blank"
                    class="retailers__item-icon retailers__item-icon_hover"
                    onMouseOver="this.style.backgroundImage = 'url(../images/retailers/<?php echoHover($hover);?>)'"
                    onMouseOut="this.style.backgroundImage = 'url(../images/retailers/<?php echoNoHover($noHover);?>)'"
                    style="background-image: url('../images/retailers/<?php echoNoHover($noHover);?>')"
                >
                </a>

            </div>

            <?php $count++; ?>
        <?php } ?>


    </div>


    <?php
    $model = new \shop\models\forms\Landing();
    ?>
    <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
        'model' => $model,
        'formUrl' => '/shop/mail',
        'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.reload();
    }).modal();
}

JS
        ,

    ]) ?>
    <hr>
    <div class="row">

        <div class="col-lg-6">
            <?= $form->field($model, 'email') ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'name') ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'text')->textarea(['rows' => 5]) ?>
        </div>

    </div>
    <hr>
    <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Отправить'
    ]) ?>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
