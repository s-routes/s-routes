<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */


$this->title = 'Добавьте ваш магазин';



?>
<?php
$this->registerJs(<<<JS

//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});

JS
);
?>
<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12" style="margin-bottom: 30px;">
        <h1 class="page-header text-center">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>

        <div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <?php
                        $id = 67;
                        $page = \common\models\exchange\Page::findOne($id);

                        $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                        if (is_null($pageLang)) {
                            $name = $page->name;
                            $content = $page->content;
                        } else {
                            $name = $pageLang->name;
                            $content = $pageLang->content;
                        }
                        ?>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><?= $name ?></h4>
                    </div>
                    <div class="modal-body">
                        <?= $content ?>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.XwLuZKjn6M', 'Закрыть') ?></button>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->render('_menu3') ?>

    </div>
    <div class="col-lg-1">

    </div>
    <div class="col-sm-10">

        <?= $content ?>

    </div>



</div>


