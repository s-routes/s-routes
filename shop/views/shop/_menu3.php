<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\db\Query;
use yii\helpers\Url;

/* меню в кабинете, табуляция, недоделана */



/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;


$menu = [
    [
        'url'     => '/page/item?id=54',
        'label'   => 'Как все работает',
        'urlList' => [
            ['startsWith', '/page/item?id=54'],
        ],
    ],
    [
        'url'     => ['shop/index'],
        'label'   => 'Федеральные и сетевые',
        'urlList' => [
            ['route', 'shop/index'],
        ],
    ],
    [
        'url'     => ['shop/region'],
        'label'   => 'Региональные и местные',
        'urlList' => [
            ['route', 'shop/region'],
        ],
    ],
    [
        'url'     => ['shop/add'],
        'label'   => 'Добавьте магазин',
        'urlList' => [
            ['route', 'shop/add'],
        ],
    ],
    [
        'url'     => '/page/item?id=41',
        'label'   => 'Оплата баллами',
        'urlList' => [
            ['startsWith', '/page/item?id=41'],
        ],
    ],
    [
        'url'     => '/page/item?id=92',
        'label'   => 'Дисклеймер',
        'urlList' => [
            ['startsWith', '/page/item?id=92'],
        ],
    ],
];

function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if (isset($i['route'])) {
                if ($i['route'] == $route) {
                    return true;
                }
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<ul class="nav nav-tabs">
    <?php
    foreach ($menu as $item) {
        $optionsLI = [];
        $options = ['class' => []];
        if (hasRoute12($item, Yii::$app->requestedRoute)) {
            $optionsLI['class'][] = 'active';
        }

        $optionsClass = (isset($optionsLI['class']))? join(' ', $options['class']) : '';
        if ($optionsClass != '') $options['class'] = $optionsClass;
        $optionsLiClass = (isset($optionsLI['class']))? join(' ', $optionsLI['class']) : '';
        if ($optionsLiClass != '') $optionsLI['class'] = $optionsLiClass;

        echo Html::tag(
            'li',
            Html::a($item['label'], $item['url'], $options),
            $optionsLI
        );
    }
    ?>
</ul>

