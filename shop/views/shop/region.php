<?php

use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $model \shop\models\forms\ShopAdd */


$this->title = '#TOPMATE - ПЛАТФОРМА ОРГАНИЗАЦИИ ONLINE-ПЛАТЕЖЕЙ';



?>

<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12" style="margin-bottom: 30px;">
        <h1 class="page-header text-center" style="font-size: 250%;">
            <?= $this->title ?>
            <a href="#" id="modalHelp"><i title="Помощь" class="fas fa-question-circle c-link" style="font-size: 30px;float: right;padding-top: 5px;"></i></a>
        </h1>
        <p class="text-center lead">ЛЮБЫЕ МАРКЕТПЛЕЙСЫ ДЛЯ ВАШЕГО ИНТЕРНЕТ-ШОПИНГА</p>

        <?= $this->render('_menu3') ?>

    </div>


    <div class="row retailers retailers_params mt-3">
        <div class="col-md-8">
            <p><a href="#addShop" class="btn btn-default" style="width: 100%;">Добавьте в список ваш любимый интернет-магазин</a></p>
        </div>
        <div class="col-md-4">
            <p><a href="/shop/request" class="btn btn-success" style="width: 100%;">Оформить заказ</a></p>
        </div>
    </div>

    <div class="row retailers retailers_params mt-3">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query'      => \common\models\ShopAll::find()
                    ->where([
                        'type' => \common\models\ShopAll::TYPE_REGION,
                        'status' => \common\models\ShopAll::STATUS_ACCEPT,
                    ])
                ,
                'pagination' => [
                    'pageSize' => 100,
                ],
            ]),
            'tableOptions' => [
                'class' => 'table table-striped table-hover',
            ],
            'summary' => '',
            'columns' => [
                'id',
                [
                    'attribute' => 'link',
                    'content'   => function ($i) {
                        return Html::a($i['link'], $i['link'], ['target' => '_blank']);
                        if (\yii\helpers\StringHelper::startsWith('http', strtolower($i['link']))) {
                            return Html::a($i['link'], $i['link'], ['target' => '_blank']);
                        } else {
                            return Html::a($i['link'], 'http://' . $i['link'], ['target' => '_blank']);
                        }
                    },
                ],
                'description:text:Кратко',
                [
                    'header'    => 'Описание',
                    'attribute' => 'about',
                    'content'   => function ($i) {
                        return nl2br($i['about']);
                    },
                ],
            ]
        ]) ?>


    </div>

    <div class="row services-shop services-shop_params">
        <p><a name="addShop"></a>Добавить магазин</p>
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS

function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location.reload();
    }).modal();
}

JS
            ,

        ]) ?>
        <hr>
        <?= $form->field($model, 'link') ?>
        <?= $form->field($model, 'description') ?>
        <hr>
        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Добавить']) ?>

    </div>

</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>


<?php
$this->registerJs(<<<JS
//при нажатию на любую кнопку, имеющую класс .btn
$("#modalHelp").click(function() {
    //открыть модальное окно с id="myModal"
    $("#myModal").modal('show');
});
JS
);
?>
<!-- Large modal -->
<div id="myModal" class="modal fade" tabindex="-1" role="document" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <?php
                $id = 82;
                $page = \common\models\exchange\Page::findOne($id);

                $pageLang = \common\models\PageLang::findOne(['parent_id' => $id, 'language' => Yii::$app->language]);
                if (is_null($pageLang)) {
                    $name = $page->name;
                    $content = $page->content;
                } else {
                    $name = $pageLang->name;
                    $content = $pageLang->content;
                }
                ?>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?= $name ?></h4>
            </div>
            <div class="modal-body">
                <?= $content ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= \Yii::t('c.57GUXH4BTk', 'Закрыть') ?></button>
            </div>
        </div>
    </div>
</div>