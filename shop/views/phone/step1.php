<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this \yii\web\View */
/** @var $model \shop\models\forms\Request */

$this->title = 'ШАГ 1: ВВЕДИТЕ СУММУ ПОПОЛНЕНИЯ И ВЫБЕРИТЕ ОПЕРАТОРА';

\avatar\assets\Notify::register($this);

$this->registerJs(<<<JS
var functionDelete = function(e) {
    var c = $(this).parent().parent().data('id');
    ajaxJson({
        url: '/shop/cart-delete2',
        data: {id: c},
        success: function(ret) {
            $('.rowProduct[data-id='+c+']').remove();
            $('.js-product-list-sum').html(ret.sumFormatted);
            
            // выставляю порядковый номер
            var c1 = 1;
            $('.js-product-list-item').each(function(i,e) {
                $(e).html(c1);
                c1++;
            });
        }
    });
};
$('.buttonDelete').click(functionDelete);
JS
);


?>




<div class="container" style="padding-bottom: 70px;">
    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>
    </div>


    <div class="col-lg-8 col-lg-offset-2">
        <?php $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    $('#modalInfo').on('hidden.bs.modal', function() {
        window.location = '/phone/step2';
    }).modal();
}
JS
        ]); ?>

        <?= $form->field($model, 'price') ?>
        <?= $form->field($model, 'type')->radioList(\common\models\ShopRequest::$_phone) ?>

        <hr>

        <?php \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Далее']); ?>
    </div>
</div>


<div class="modal fade" id="modalInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Информация</h4>
            </div>
            <div class="modal-body">
                Успешно!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>