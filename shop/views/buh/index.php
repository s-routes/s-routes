<?php
use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

/** @var \yii\web\View $this */
$this->title = 'Бухгалтер';


?>




<div class="container" style="padding-bottom: 70px;">

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            Бухгалтер
        </h1>
    </div>
    <div class="col-lg-8">
        <p><a href="/buh-shop-list/index" class="btn btn-default">Все магазины</a></p>
        <p><a href="/buh-request-list/index" class="btn btn-default">Все заказы</a></p>
        <p><a href="/admin-support/index" class="btn btn-default">Чат поддержки</a></p>
        <p><a href="/buh-billing/index" class="btn btn-default">Счета</a></p>
    </div>

</div>


