<?php

/** @var $this \yii\web\View */
/** @var $bill \common\models\avatar\UserBill */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Приобрести пакет МАРКЕТ-БАЛЛОВ';

$this->registerJs(<<<JS
$('.buttonPay').click(function (e) {
    ajaxJson({
        url: '/cabinet-wallet/market-buy',
        data: {
            count: $(this).data('count')
        },
        success: function (ret) {
            window.location = ret.url;
        }
    });
});
JS
);
?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-8 col-lg-offset-2" style="margin-bottom: 50px;">
        <div class="col-lg-3" >
            <button class="btn btn-success buttonPay" data-count="100" style="width: 100px;">100</button>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="500" style="width: 100px;">500</button>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="1000" style="width: 100px;">1 000</button>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="2000" style="width: 100px;">2 000</button>
        </div>
    </div>
    <div class="col-lg-8 col-lg-offset-2">


        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="5000" style="width: 100px;">5 000</button>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="10000" style="width: 100px;">10 000</button>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="50000" style="width: 100px;">50 000</button>
        </div>
        <div class="col-lg-3">
            <button class="btn btn-success buttonPay" data-count="100000" style="width: 100px;">100 000</button>
        </div>


    </div>
</div>
