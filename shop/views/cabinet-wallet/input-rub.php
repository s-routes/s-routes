<?php

/** @var $this \yii\web\View */
/** @var $model \shop\models\validate\CabinetWalletInputRub */

use common\models\avatar\UserBill;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Пополнить счет';


?>


<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="col-lg-12">
        <?php
        $form = \iAvatar777\services\FormAjax\ActiveForm::begin([
            'model'   => $model,
            'success' => <<<JS
function (ret) {
    window.location = ret.url;
}
JS
            ,
        ]); ?>

        <?= $form->field($model, 'price') ?>
        <hr>

        <?php  \iAvatar777\services\FormAjax\ActiveForm::end(['label' => 'Внести']); ?>

    </div>
</div>
