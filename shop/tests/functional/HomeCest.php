<?php

namespace shop\tests\functional;

use cs\services\VarDumper;
use frontend\tests\FunctionalTester;

class HomeCest
{
    public function checkOpen(FunctionalTester $I)
    {
        $I->amOnPage('/');
        $I->see('ПЛАТФОРМА ОРГАНИЗАЦИИ ONLINE-ПЛАТЕЖЕЙ');
    }

}