<?php

namespace shop\controllers;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\CurrencyIO;
use common\models\PaymentBitCoin;
use common\models\shop\Basket;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\ShopRequest;
use common\models\ShopRequestProduct;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class PhoneController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'step2-pay' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\shop\models\forms\Step2Pay',
            ],
            'step2-pay-rub' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\shop\models\forms\Step2PayRub',
            ],
        ];
    }

    /**
     */
    public function actionStep1()
    {
        $model = new \shop\models\forms\PhoneStep1();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionStep2()
    {
        $model = new \avatar\models\forms\AdminComission();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();
                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

}
