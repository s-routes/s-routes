<?php

namespace shop\controllers;

use avatar\controllers\actions\DefaultAjax;
use avatar\controllers\AdminBaseController;
use avatar\models\forms\BlogItem;
use avatar\models\forms\UserAvatar;
use cs\models\Tables\Client;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\UserException;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client as ClientHttp;
use yii\web\Controller;
use yii\filters\VerbFilter;
use cs\base\BaseController;
use yii\web\Response;
use yii\widgets\ActiveForm;

class CabinetMarketController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'buy-ajax' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\forms\CabinetMarkeyBuy',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionBuy($count)
    {

        return $this->render([
            'count' => $count,
        ]);
    }

    public function actionBuy2($count)
    {
        return $this->render([
            'count' => $count,
        ]);
    }


}
