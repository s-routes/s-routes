<?php

namespace shop\controllers;

use aki\telegram\Telegram;
use avatar\models\forms\BlogItem;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\NewsItem;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\models\UserTelegramConnect;
use common\models\UserTelegramShopConnect;
use common\models\UserTelegramShopTemp;
use common\models\UserTelegramTemp;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TelegramController extends \avatar\base\BaseController
{
    public $enableCsrfValidation = false;

    public static $languageStrategy = [
        'ru' => ['ru'],
        'en' => ['en', 'hi', 'id', 'zh'],
    ];

    /**
     *
     */
    public function actionIndex()
    {
        return $this->actionCallBack();
    }

    /**
     * Выдает оптимальный язык для бота ru/en в зависимости от установок пользователя
     *
     * @param \common\models\UserAvatar $user
     *
     * @return string язык
     */
    public static function getOptimalLanguage($user)
    {
        $language = 'ru';
        $languageStrategy = \avatar\controllers\TelegramController::$languageStrategy;
        // Если язык установлен у пользователя?
        if (!Application::isEmpty($user->language)) {
            foreach ($languageStrategy as $e => $l) {
                if (in_array($user->language, $l)) {
                    $language = $e;
                }
            }
        }

        return $language;
    }

    /**
     */
    public function actionTest2()
    {
        /** @var Telegram $t */
        $t = Yii::$app->telegramShop;
        $t->sendVideo([
            'chat_id' => 122605414,
            'video'   => Yii::getAlias('@webroot/images/telegram/tumblr_o2j1islebK1tjki5do1_540.gif'),
            'caption' => 'a',
        ]);
    }

    /**
     */
    public function actionCallBack()
    {
        $message = '';
        Yii::info(\yii\helpers\VarDumper::dumpAsString(Json::decode(Yii::$app->request->rawBody)), 'avatar\controllers\TelegramController::actionCallBackShop');

        $data = Json::decode(Yii::$app->request->rawBody);

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        if (isset($data['channel_post'])) {
            return 'ok';
        }
        if (!isset($data['message'])) {
            return 'ok';
        }
        $chat_id = $data['message']['chat']['id'];

        $language = 'ru';
        $languageStrategy = self::$languageStrategy;

        // определить а кто вообще пишет? Свой или гость?
        {
            $username = null;
            if (isset($data['message']['chat']['username'])) {
                $username = $data['message']['chat']['username'];
            }
            try {
                $user = UserAvatar::findOne(['telegram_shop_chat_id' => $chat_id]);

                // Пишет свой $user
                // Если язык установлен у пользователя?
                if (!Application::isEmpty($user->language)) {
                    foreach ($languageStrategy as $e => $l) {
                        if (in_array($user->language, $l)) {
                            $language = $e;
                        }
                    }
                }

            } catch (\Exception $e) {
                // Пишет гость
                $user = null;
            }
        }

        $textFirst = 'Какой язык вы предпочитаете русский или английский?
Напишите "RU" или "EN"

Which language do you prefer Russian or English?
Write "RU" or "EN"';

        if (isset($data['message']['text'])) {

            $text = trim($data['message']['text']);

            if ($text == '/help') {
                if ($user) {
                    // Пишет свой $user
                    $rows = [
                        '/reset'      => 'Перейти в начало диалога. Сброс.',
                        '/help'       => 'Вывести помощь.',
                        '/disconnect' => 'Отсоединить телеграм бота от профиля магазина',
                        '/me'         => 'Посмотртеть информацию о себе',
                    ];
                } else {
                    // Пишет гость
                    $rows = [
                        '/reset' => 'Перейти в начало диалога. Сброс.',
                        '/help'  => 'Вывести помощь.',
                    ];
                }
                $lines = [];
                foreach ($rows as $command => $description) {
                    $lines[] = $command . ' - ' . $description;
                }
                $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => join("\n", $lines)]);
            } else {
                if ($user) {

                    // Пишет свой $user
                    if ($text == '/disconnect') {
                        // удаляю прошлый статус разговора
                        $connect = UserTelegramShopTemp::findOne(['chat_id' => $user->telegram_shop_chat_id]);
                        if (!is_null($connect)) $connect->delete();

                        // сообщаю
                        $telegram->sendMessage(['chat_id' => $user->telegram_shop_chat_id, 'text' => 'Я отсоединяюсь! До скорых встреч!']);

                        // сбрасываю привязку у пользователя
                        $user->telegram_shop_chat_id = null;
                        $user->telegram_shop_username = null;
                        $user->save();

                    } elseif ($text == '/me') {
                        $lines = [];
                        $lines[] = 'email: ' . $user->email;
                        $lines[] = 'id: ' . $user->id;
                        $lines[] = 'Имя: ' . $user->name_first;
                        $lines[] = 'Фамилия: ' . $user->name_last;
                        $telegram->sendMessage(['chat_id' => $user->telegram_shop_chat_id, 'text' => join("\n", $lines)]);
                    } else {
                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => 'Привет ' . $user->getName2()]);
                    }
                } else {
                    // Пишет гость
                    $userTelegram = UserTelegramShopTemp::findOne(['chat_id' => $chat_id]);

                    if ($userTelegram) {
                        if (!Application::isEmpty($userTelegram->language)) {
                            $language = $userTelegram->language;
                        }
                        // уже общались
                        if ($text == '/reset') {
                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $textFirst]);
                            $userTelegram->status = 10;
                            $userTelegram->save();
                        } else {
                            // Это может быть и не сообщение, а я обрабатываю пока только сообщения
                            if (isset($data['message']['text'])) {
                                switch ($userTelegram->status) {
                                    case 2:
                                        if ($language == 'ru') {
                                            $yes = ['да', 'Да', 'ДА'];
                                            $no = ['нет', 'Нет'];
                                        } else {
                                            $yes = ['yes', 'Yes', 'YES'];
                                            $no = ['no', 'No', 'NO'];
                                        }

                                        if (in_array($text, $yes)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Напишите пожалуйста ваш e-mail', [], $language)]);
                                            $userTelegram->status = 3;
                                            $userTelegram->save();
                                            break;
                                        }
                                        if (in_array($text, $no)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Не страшно, давайте я вас зарегистрирую', [], $language)]);
                                            $userTelegram->status = 6;
                                            $userTelegram->save();
                                            break;
                                        }
                                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Можно ответить только словами «да» или «нет»', [], $language)]);
                                        break;

                                    case 3:
                                        if (!filter_var($text, FILTER_VALIDATE_EMAIL)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Вы указываете неверный e-mail, введите пожалуйста корректный адрес почты', [], $language)]);
                                            break;
                                        }
                                        try {
                                            $email = strtolower($text);
                                            $user = UserAvatar::findOne(['email' => $email]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Отлично! Все получается!', [], $language)]);
                                            $userTelegram->email = strtolower($text);
                                            $userTelegram->status = 5;
                                            $userTelegram->save();

                                            $hash = Security::generateRandomString(32);
                                            UserTelegramShopConnect::add([
                                                'username' => $username,
                                                'hash'     => $hash,
                                                'user_id'  => $user->id,
                                                'chat_id'  => $data['message']['chat']['id'],
                                            ]);

                                            // отправить почту
                                            \common\services\Subscribe::sendArray([$email], Yii::t('c.idjnVpnIC5', 'Подтверждение почты от Telegram @TopMate_one_info_bot', [], $language), 'telegram_email', [
                                                'hash'     => $hash,
                                                'username' => $username,
                                                'email'    => $email,
                                                'url'      => Url::to(['auth/confirm-telegram', 'hash' => $hash], true),
                                                'language' => $language,
                                            ]);
                                        } catch (\Exception $e) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Сожалею, но такого пользователя на платформе нет.', [], $language)]);
                                        }
                                        break;

                                    case 5:
                                        if ($language == 'ru') {
                                            $send = ['выслать', 'Выслать'];
                                        } else {
                                            $send = ['send', 'Send'];
                                        }
                                        if (in_array($text, $send)) {
                                            // отправить почту
                                            $UserTelegramShopConnect = UserTelegramShopConnect::findOne(['chat_id' => $chat_id]);
                                            $user = UserAvatar::findOne($UserTelegramShopConnect->user_id);

                                            \common\services\Subscribe::sendArray([$user->email], Yii::t('c.idjnVpnIC5', 'Подтверждение почты от Telegram @TopMate_one_info_bot', [], $language), 'telegram_email', [
                                                'hash'     => $UserTelegramShopConnect->hash,
                                                'username' => $username,
                                                'email'    => $user->email,
                                                'url'      => Url::to(['auth/confirm-telegram', 'hash' => $UserTelegramShopConnect->hash], true),
                                                'language' => $language,
                                            ]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Я выслал вам e-mail.', [], $language)]);
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Отлично! Все получается!', [], $language)]);
                                        }
                                        break;

                                        break;

                                    case 6:
                                        if (!filter_var($data['message']['text'], FILTER_VALIDATE_EMAIL)) {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Не верный email, введите пожалуйста корректный email', [], $language)]);
                                            break;
                                        }
                                        try {
                                            $user = UserAvatar::findOne(['email' => $data['message']['text']]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Такой e-mail уже зарегистрирован.', [], $language)]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Введите e-mail, который не зарегистрирован, или сбросьте разговор к его началу командой /reset', [], $language)]);
                                        } catch (\Exception $e) {
                                            // регистрация
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Зайдите на почту и подтвердите её, или напишите "выслать" и я вышлю ссылку еще раз.', [], $language)]);

                                            $email = strtolower($text);

                                            $hash = Security::generateRandomString(32);
                                            UserTelegramConnect::add([
                                                'username' => $username,
                                                'hash'     => $hash,
                                                'chat_id'  => $data['message']['chat']['id'],
                                                'email'    => $email,
                                                'language' => $userTelegram->language,
                                            ]);

                                            // отправить почту
                                            \common\services\Subscribe::sendArray([$email], Yii::t('c.idjnVpnIC5', 'Подтверждение почты от Telegram @TopMate_one_info_bot', [], $language), 'telegram_email', [
                                                'hash'     => $hash,
                                                'username' => $username,
                                                'email'    => $email,
                                                'url'      => Url::to(['auth/registration-telegram', 'hash' => $hash], true),
                                                'language' => $language,
                                            ]);
                                            $userTelegram->status = 8;
                                            $userTelegram->save();
                                        }
                                        break;
                                    case 8:
                                        if (in_array($text, ['выслать', 'Выслать'])) {
                                            // отправить почту
                                            $UserTelegramConnect = UserTelegramShopConnect::findOne(['chat_id' => $chat_id]);

                                            \common\services\Subscribe::sendArray([$UserTelegramConnect->email], Yii::t('c.idjnVpnIC5', 'Подтверждение почты от Telegram @TopMate_one_info_bot', [], $language), 'telegram_email', [
                                                'hash'     => $UserTelegramConnect->hash,
                                                'username' => $username,
                                                'email'    => $UserTelegramConnect->email,
                                                'url'      => Url::to(['auth/registration-telegram', 'hash' => $UserTelegramConnect->hash], true),
                                                'language' => $language,
                                            ]);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Выслал. Зайдите на почту и подтвердите e-mail.', [], $language)]);
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => Yii::t('c.idjnVpnIC5', 'Зайдите на почту и подтвердите её', [], $language)]);
                                        }
                                        break;

                                    case 10:
                                        if (in_array(strtolower($text), ['ru', 'en'])) {
                                            // установить язык
                                            if (strtolower($text) == 'ru') {
                                                $userTelegram->language = 'ru';
                                            }
                                            if (strtolower($text) == 'en') {
                                                $userTelegram->language = 'en';
                                            }
                                            $language = $userTelegram->language;
                                            $text10 = Yii::t('c.idjnVpnIC5', 'Доброго времени суток, уважаемый пользователь TopMate.one', [], $language);
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $text10]);

                                            $userTelegram->status = 2;
                                            $userTelegram->save();
                                        } else {
                                            $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => 'Можно ответить только словами «ru» или «en» / You can only respond with the words «ru» or «en»']);
                                        }
                                        break;
                                }

                            }

                        }
                    } else {
                        // первый раз пишет
                        $telegram->sendMessage(['chat_id' => $data['message']['chat']['id'], 'text' => $textFirst]);
                        UserTelegramShopTemp::add([
                            'username' => $username,
                            'chat_id'  => $data['message']['chat']['id'],
                            'status'   => 10,
                        ]);
                    }
                }
            }
        }

        return 'ok';
    }

    /**
     */
    public function actionGroup()
    {
        return $this->render();
    }

    /**
     */
    public function actionChannel()
    {
        return $this->render();
    }

    /**
     */
    public function actionSetCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['url' => 'https://topmate.one/telegram/call-back'])
            ->addFile('certificate', '/home/god/i-am-avatar/www/ssl/letsencrypt/fullchain.pem')
            ->send();

        VarDumper::dump($response);
    }

    /**
     */
    public function actionTest()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $url = "https://www.i-am-avatar.com/telegram";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        VarDumper::dump($response);
    }

    /**
     */
    public function actionGetWebhookInfo()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/getWebhookInfo";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        VarDumper::dump($response);
    }

    /**
     */
    public function actionRemoveCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        VarDumper::dump($response);
    }
}
