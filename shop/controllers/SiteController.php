<?php

namespace shop\controllers;

use avatar\controllers\actions\DefaultAjax;
use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class SiteController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'error'   => [
                'class' => 'yii\web\ErrorAction',
                'view'  => '@avatar/views/site/error.php',
            ],
            'captcha' => [
                'class'           => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => !YII_ENV_PROD ? '123' : null,
            ],
            'pay'     => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\SitePay',
            ],
        ];
    }

    /**
     */
    public function actionMerchant()
    {
        return $this->render();
    }

    /**
     */
    public function actionDonate()
    {
        return $this->render();
    }

    /**
     */
    public function actionToken()
    {
        return $this->render();
    }

    /**
     */
    public function actionContracts()
    {
        return $this->render();
    }

    /**
     */
    public function actionPaySuccess()
    {
        return $this->render();
    }

    /**
     */
    public function actionCloses()
    {
        return $this->render();
    }

    /**
     */
    public function actionAvatarPay()
    {
        return $this->render('avatar-pay');
    }

    /**
     */
    public function actionFreePrivacyPolicy()
    {
        return $this->render('free-privacy-policy');
    }

    /**
     */
    public function actionMap()
    {
        return $this->render();
    }

    /**
     */
    public function actionConvert()
    {
        $table = Currency::getRateMatrix();

        return $this->render([
            'table' => $table,
        ]);
    }

    /**
     * Конвертирует значения валют
     *
     * REQUEST:
     * + value - float - значение для конвертации
     * + from - string - код валюты
     * + to - string - код валюты
     */
    public function actionConvertAjax()
    {
        $model = new \avatar\models\validate\SiteConvertAjax();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(101, 'не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(102, $model->errors);
        }
        $value = $model->action();
        $currency = Currency::findOne(['code' => $model->to]);

        return self::jsonSuccess([
            'unformated' => $model->action(),
            'formated'   => Yii::$app->formatter->asDecimal($value, $currency->decimals),
            'currencyTo' => [
                'code'     => $currency->code,
                'decimals' => $currency->decimals,
                'title'    => $currency->title,
            ],
        ]);
    }

    /**
     */
    public function actionService()
    {
        return $this->render();
    }

    /**
     */
    public function actionIndex()
    {
        $layout = 'landing';
        $view = '/landing/index';

        $this->layout = $layout;

        return $this->render($view);
    }

    /**
     */
    public function actionMain()
    {
        return $this->render();
    }

    /**
     */
    public function actionAvatar()
    {
        return $this->render();
    }

    /**
     */
    public function actionHelp()
    {
        return $this->render();
    }

    /**
     */
    public function actionAbout()
    {
        return $this->render();
    }

    /**
     */
    public function actionProtection()
    {
        return $this->render();
    }

    /**
     */
    public function actionContact()
    {
        $model = new Contact();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            /**
             * @var $data array
             * [
             *     'cabinet.new-earth.space' => [
             *          'id' => 1,
             *          'domain-name' => 'cabinet.new-earth.space'
             *          //...
             *      ],
             * ]
             */
            $data = \common\models\CompanyCustomizeItem::getAll();
            $mail = $data['avatarnetwork.io']['contact']['mail'];

            $serverName = $_SERVER['HTTP_HOST'];
            if (isset($data[$serverName])) {
                if (isset($data[$serverName]['contact']['mail'])) {
                    $mail = $data[$serverName]['contact']['mail'];
                }
            }
            $model->contact($mail);
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }
}
