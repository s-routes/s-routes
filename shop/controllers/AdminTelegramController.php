<?php

namespace shop\controllers;

use common\services\Security;
use common\services\Subscribe;
use cs\services\VarDumper;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class AdminTelegramController extends \avatar\controllers\AdminBaseController
{
    public function actionIndex()
    {
        return $this->render([]);
    }


    /**
     */
    public function actionSetCallBack()
    {
        $path = self::getParam('path');

        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $i = pathinfo($path);
        $file = Yii::getAlias('@runtime/'.Security::generateRandomString().'.'.$i['extension']);
        $arrContextOptions = [
            "ssl" => [
                "verify_peer"      => false,
                "verify_peer_name" => false,
            ],
        ];
        file_put_contents($file, file_get_contents($path, false, stream_context_create($arrContextOptions)));

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->addData(['url' => 'https://topmate.one/telegram/call-back'])
            ->addFile('certificate', $file)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionGetWebhookInfo()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/getWebhookInfo";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }

    /**
     */
    public function actionRemoveCallBack()
    {
        $client = new Client();
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegramShop;

        $url = "https://api.telegram.org/bot" . $telegram->botToken . "/setWebhook";

        $response = $client->createRequest()
            ->setMethod('POST')
            ->setUrl($url)
            ->send();

        return self::jsonSuccess(Json::decode($response->content));
    }
}
