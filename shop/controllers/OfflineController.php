<?php

namespace shop\controllers;


use cs\services\VarDumper;

class OfflineController extends \avatar\base\BaseController
{
    public $layout = 'blank';

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }
}
