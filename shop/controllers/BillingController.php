<?php

namespace shop\controllers;

use avatar\controllers\actions\DefaultAjax;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use common\models\RequestInputRub;
use school\modules\sroutes\models\Arbitrator;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class BillingController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [];
    }

    /**
     * Оплата заявки
     *
     * @param int $id идентификатор заявки billing.id
     *
     * @return string Response
     * @throws
     */
    public function actionPay($id)
    {
        $BillingMain = BillingMain::findOne($id);
        if (is_null($BillingMain)) {
            throw new \Exception('Не найден счет');
        }

        return $this->render([
            'BillingMain' => $BillingMain,
        ]);
    }

    /**
     * @param int $id billing.id
     * @return string
     * @throws Exception
     */
    public function actionPaySystem($id)
    {
        try {
            $bill = BillingMain::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        $model = new \shop\models\validate\BillingPaySystem(['billing' => $bill]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {

                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
            'bill' => $bill,
        ]);
    }

}