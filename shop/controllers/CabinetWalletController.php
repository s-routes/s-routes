<?php

namespace shop\controllers;

use avatar\controllers\actions\DefaultAjax;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillMerchant;
use common\models\BillingMain;
use common\models\RequestInputRub;
use school\modules\sroutes\models\Arbitrator;
use school\modules\sroutes\models\Deal;
use school\modules\sroutes\models\DealStatus;
use common\models\investment\IcoRequest;
use common\models\investment\IcoRequestSuccess;
use common\models\PaySystemConfig;
use common\models\piramida\Billing;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\services\Security;
use cs\Application;
use cs\base\BaseController;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ErrorAction;
use yii\web\Response;

class CabinetWalletController extends \avatar\controllers\CabinetBaseController
{
    public function actions()
    {
        return [
            'send2' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\avatar\models\forms\SendAtom2',
                'formName' => 'SendAtom2',
            ],
            'market-buy' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\shop\models\validate\CabinetWalletMarketBuy',
            ]
        ];
    }

    public function actionIndex()
    {
        return $this->render();
    }

    public function actionItem($id)
    {
        try {
            $bill = UserBill::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        // Если не (бухгалтер)
        if (!(Yii::$app->authManager->checkAccess($user->id, 'permission_shop_buh'))) {
            // Если не (менеджер)
            if (!(Yii::$app->authManager->checkAccess($user->id, 'permission_shop_manager'))) {
                // Проверяю доступ
                if ($bill->user_id != Yii::$app->user->id) {
                    return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
                }
            }
        }

        return $this->render(['bill' => $bill]);
    }

    public function actionInputRub($id)
    {
        $model = new \shop\models\validate\CabinetWalletInputRub();

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {

                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    public function actionInputMarketSuccess($id)
    {
        return $this->render([
        ]);
    }

    public function actionInputRubSuccess($id)
    {
        return $this->render([
        ]);
    }

    public function actionInputMarket($id)
    {
        return $this->render();
    }

    public function actionSend($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        // проверка на возможность отправлять средства
        $isShowSend = true;
        if ($bill->currency == \common\models\avatar\Currency::MARKET) {
            $cInt = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $UserBill2 = \common\models\UserBill2::findOne(['user_id' => Yii::$app->user->id, 'currency_id' => $cInt->id]);
            $w = \common\models\piramida\Wallet::findOne($UserBill2->wallet_id);
            if ((Yii::$app->user->identity->market_till < time()) || (Yii::$app->user->identity->market_till == 0) || ($w->amount == 0)) {
                $isShowSend = false;
            }
        }
        if (!$isShowSend) {
            return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Переводы и зачисление средств на баланс недоступны без активации аккаунта. Активируйте его покупкой любого "Пакета возможностей" ' . \yii\helpers\Html::a('здесь', 'https://topmate.one/cabinet-market/index')]);
        }

        $model = new \avatar\models\forms\SendAtom2();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $t = $model->action();
            Yii::$app->session->setFlash('form', $t->getAddress());
        }

        return $this->render([
            'bill'   => $bill,
            'model'  => $model,
        ]);
    }

    public function actionConvert($id)
    {
        try {
            $bill = UserBill::findOne($id);
            if ($bill->user_id != Yii::$app->user->id) {
                return $this->render('@avatar/views/site/error', ['name' => 'Ошибка', 'message' => 'Это не ваш счет']);
            }
        } catch (\Exception $e) {
            throw new Exception('Счет не найден');
        }

        $model = new \avatar\models\forms\ConvertSbp();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $ret = $model->action();
            Yii::$app->session->setFlash('form', $ret);
        }

        return $this->render([
            'bill'   => $bill,
            'model'  => $model,
        ]);
    }


    public function actionDelete()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDelete();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $result = $model->action();
        if (!$result) return self::jsonErrorId(2, 'Кошелек не пустой');

        return self::jsonSuccess();
    }

    public function actionDeleteAgree()
    {
        $model = new \avatar\models\validate\CabinetWalletControllerDeleteAgree();

        if (!$model->load(Yii::$app->request->post(), '')) {
            return self::jsonErrorId(400, 'Не загружены данные');
        }
        if (!$model->validate()) {
            return self::jsonErrorId(103, $model->errors);
        }
        $model->action();

        return self::jsonSuccess();
    }
}