<?php

namespace shop\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\School;
use common\models\ShopRequest;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class ManagerRequestListController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_shop_manager'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'send' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\BuhRequestListSend',
            ],
            'get' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\ManagerRequestListGet',
            ],
            'close' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\ManagerRequestListClose',
            ],
            'cancel' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\ManagerRequestListCancel',
            ],
        ];
    }
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionView($id)
    {
        $request = ShopRequest::findOne($id);

        return $this->render(['request' => $request]);
    }

    public function actionCancelPart($id)
    {
        $request = ShopRequest::findOne($id);
        $model = new \shop\models\validate\ManagerRequestListCancelPart(['request' => $request]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {

                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render([
            'model'   => $model,
            'request' => $request,
        ]);
    }

}
