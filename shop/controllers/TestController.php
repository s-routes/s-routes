<?php

namespace shop\controllers;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\PaymentBitCoin;
use common\models\piramida\Wallet;
use common\models\school\School;
use common\models\shop\Basket;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Subscribe;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TestController extends \avatar\base\BaseController
{

    /**
     */
    public function actionIndex()
    {
        $t = Wallet::findOne(26166)->move2(26167,10000000, 'test');
VarDumper::dump($t);
    }

    /**
     */
    public function actionTest1()
    {
        VarDumper::dump(Subscribe::sendArray(['solncelub@gmail.com'], '1adsf123','test', ['text' => '1sdffd']));
    }

}
