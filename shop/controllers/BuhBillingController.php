<?php

namespace shop\controllers;

use app\models\Article;
use app\models\SiteUpdate;
use app\services\Subscribe;
use avatar\base\Application;
use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\HD;
use common\models\HDtown;
use common\models\school\School;
use common\models\ShopRequest;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class BuhBillingController extends \avatar\controllers\CabinetBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['role_shop_buh'],
                    ],
                ],
            ],
        ];
    }

    /**
     */
    public function actionViewUser($id)
    {
        $user = \common\models\UserAvatar::findOne($id);

        return $this->render(['user' => $user]);
    }

    /**
     * @param int $id идентификатор пользователя
     * @return string
     * @throws
     */
    public function actionWalletList($id)
    {
        if (!Application::isInteger($id)) {
            throw new Exception('id должен быть INT');
        }
        try {
            $user = \common\models\UserAvatar::findOne($id);
        } catch (\Exception $e) {
            throw new Exception('Не найден пользователь');
        }

        return $this->render(['user' => $user]);
    }

    public function actions()
    {
        return [
            'confirm' => [
                'class' => '\avatar\controllers\actions\DefaultAjax',
                'model' => '\shop\models\validate\BuhBillingConfirm',
            ],
        ];
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionView($id)
    {
        $request = ShopRequest::findOne($id);

        return $this->render(['request' => $request]);
    }

}
