<?php

namespace shop\controllers;

use avatar\models\forms\Contact;
use avatar\services\LogReader;
use common\components\providers\ETH;
use common\components\sms\IqSms;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\CompanyCustomizeItem;
use common\models\CurrencyIO;
use common\models\PaymentBitCoin;
use common\models\shop\Basket;
use common\models\shop\CatalogItem;
use common\models\shop\Product;
use common\models\shop\Request;
use common\models\ShopRequest;
use common\models\ShopRequestProduct;
use common\models\UserAvatar;
use common\models\UserRegistration;
use common\payment\BitCoinBlockTrailPayment;
use cs\Application;
use cs\base\BaseController;
use cs\services\Security;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use iAvatar777\services\FormAjax\DefaultFormAjax;
use Yii;
use yii\base\Event;
use yii\base\UserException;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ShopController extends \avatar\base\BaseController
{
    public function actions()
    {
        return [
            'cart-update' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ShopCartUpdate',
            ],
            'validate-code' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\ValidateCode',
            ],
            'cart-add' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\CartAdd',
            ],
            'cart-delete' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\CartDelete',
            ],
            'cart-delete2' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\forms\shop\CartDelete2',
            ],
            'first' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\avatar\models\validate\ShopFirst',
            ],
            'request-start' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\shop\models\validate\ShopRequestFirst',
            ],
            'mail' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Landing',
            ],
            'login' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Login',
            ],
            'request-add' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Request',
                'formName'    => 'Request',
            ],
            'request2-guest' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Request2',
            ],
            'request2-login' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Request2Login',
                'formName'    => 'Request2Login',
            ],
            'request3-guest' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Request3',
            ],
            'request3-login' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Request3Login',
                'formName' => 'Request3Login',
            ],
            'request4-guest' => [
                'class'    => '\iAvatar777\services\FormAjax\DefaultFormAjax',
                'model'    => '\shop\models\forms\Request4Guest',
            ],
            'request5-pay' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\shop\models\forms\Request5Pay',
            ],
            'request5-pay-rub' => [
                'class'    => '\avatar\controllers\actions\DefaultAjax',
                'model'    => '\shop\models\forms\Request5PayRub',
            ],
        ];
    }

    /**
     */
    public function actionTable()
    {
        return $this->render();
    }

    /**
     */
    public function actionAdd()
    {
        return $this->render();
    }

    /**
     */
    public function actionRegion()
    {
        $model = new \shop\models\forms\ShopAdd(['type' => \common\models\ShopAll::TYPE_REGION]);

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $s = $model->save();

                return self::jsonSuccess($s);
            } else {
                return self::jsonErrorId(102, $model->getErrors102());
            }
        }

        return $this->render(['model' => $model]);
    }

    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionCatalog($id)
    {
        $catalog = CatalogItem::findOne($id);

        return $this->render([
            'catalog' => $catalog,
        ]);
    }


    /**
     */
    public function actionItem($id)
    {
        $Product = Product::findOne($id);
        if (is_null($Product)) {
            throw new Exception('Не найден продукт');
        }

        return $this->render([
            'item' => $Product,
        ]);
    }

    /**
     * Показыват форму авторизации/регистрации
     */
    public function actionRequest0()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['auth/login']);
        } else {
            return $this->redirect(['request1']);
        }
    }

    /**
     */
    public function actionRequest1()
    {
        return $this->render([]);
    }

    /**
     */
    public function actionRequest()
    {
        $data = Yii::$app->session->get(\shop\models\forms\Request::$keySession);
        if (!isset($data['is_guest'])) {
            if (!Yii::$app->user->isGuest) {
                $data['is_guest'] = false;
                Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);
            }
        }

        return $this->render([]);
    }

    /**
     *
     */
    public function actionRequest2()
    {
        if (!isset($data['is_guest'])) {
            if (!Yii::$app->user->isGuest) {
                $data['is_guest'] = false;
                Yii::$app->session->set(\shop\models\forms\Request::$keySession, $data);
            }
        }

        return $this->render([]);
    }

    /**
     *
     */
    public function actionRequest3()
    {
        return $this->render([]);
    }

    /**
     *
     */
    public function actionRequest4()
    {
        return $this->redirect(['request5']);
    }

    /**
     */
    public function actionRequest5()
    {
        return $this->render();
    }

    /**
     * Отправляет код
     */
    public function actionSendCode()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $code = Yii::$app->session->get('dogovor_code');
        if (is_null($code)) {
            $code = Security::generateRandomString();
            Yii::$app->session->set('dogovor_code', $code);
        }
        /** @var UserAvatar $user */
        $user = Yii::$app->user->identity;
        $walletEscrow = \Yii::$app->params['walletEscrow'];

        \common\services\Subscribe::sendArray(
            [$user->email],
            'Код для подписания договора оферты',
            'oferta',
            [
                'code' => $code,
            ]
        );

        return self::jsonSuccess();
    }


    /**
     * Показывает корзину
     */
    public function actionCart()
    {
        return $this->render();
    }

}
