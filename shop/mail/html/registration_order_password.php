<?php
/**
 * @var $url string
 * @var $password string
 * @var $datetime int
 * @var $user \common\models\UserAvatar
 */
?>

<p>Вы зарегистрировались на сайте «TOPMATE.ONE» и оформили заказ.</p>

<p>Ваш пароль: <?= $password ?></p>

<p>Вам нужно подтвердить свою почту, пройдя по ссылке на кнопке:</p>

<p><a href="<?= $url ?>" style="
            text-decoration: none;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
             display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
"><?= \Yii::t('c.rwdT81IXyt', 'Ссылка') ?></a></p>

<p>Вы так-же можете скопировать ссылку, и вставить её в поле поиска вашего браузера.</p>

<p><?= \Yii::t('c.rwdT81IXyt', 'Данная ссылка будет действительна 7 дней') ?></p>
