<?php
/** @var  \common\models\TaskInput $task */
/** @var  string $amount_formatted */
/** @var  string $code */

?>

<p>Заявка #<?= $task->id ?> на пополнение выполнена</p>
<p>Зачислено <?= $amount_formatted ?> <?= $code ?></p>

