<?php
/**
 * Created by PhpStorm.
 * User: god
 * Date: 20.02.2017
 * Time: 0:36
 */

/** @var $merchant  \common\models\avatar\UserBillMerchant */
/** @var $billingTo \common\models\avatar\UserBill */
/** @var $request   \common\models\MerchantRequest */

$bitCoinPayment = $request->getPaymentBitCoin();
?>

<p>Вам был совершен перевод</p>
<p>Назначение: <?= $request->title ?></p>
<p>Идентификатор инвойса по мерчанту: <?= $request->id ?></p>
<p>Сумма: <?= Yii::$app->formatter->asDecimal($request->price, 8) ?> BTC</p>
<p>Время формирования заявки: <?= Yii::$app->formatter->asDatetime($request->created_at, 'php:c') ?></p>
<p>Время подтверждения заявки: <?= Yii::$app->formatter->asDatetime($bitCoinPayment->time_confirm, 'php:c') ?></p>