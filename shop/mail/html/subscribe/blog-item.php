<?php
/**
 * Created by PhpStorm.
 * User: s.arhangelskiy
 * Date: 13.04.2017
 * Time: 11:34
 */

/** @var $newsItem  \common\models\BlogItem*/
?>

<h1 style="text-align: center;font-weight: normal;"><?= $newsItem->name ?></h1>
<p style="text-align: center;">
    <a href="<?= $newsItem->getLink(true) ?>">
        <img src="<?= $newsItem->getImage(true) ?>" width="300" style="border-radius: 20px;"/>
    </a>
</p>
<p style="text-align: center;"><?= \avatar\services\Html::getMiniText($newsItem->content) ?></p>
<p style="text-align: center;"><a href="<?= $newsItem->getLink(true) ?>" style="
text-decoration: none;
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
             display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
">Подробнее</a></p>

<p style="margin-top: 40px;">Присоединяйтесь к нашим каналам:</p>
<p>
    <a target="_blank" href="https://www.youtube.com/channel/UC5_n6x3PFK_pA0w2So6lciQ"><img src="https://avatarnetwork.io/images/controller/landing/index/media.png" alt="YouTube" width="43" data-toggle="tooltip" title="" data-placement="top" data-original-title="YouTube"></a>
    <a target="_blank" href="https://www.facebook.com/AvatarNetwork.io/"><img src="https://avatarnetwork.io/images/controller/landing/index/fb.png" alt="FaceBook" data-toggle="tooltip" title="" data-placement="top" data-original-title="FaceBook"></a>
    <a target="_blank" href="https://t.me/AvatarNetwork"><img src="https://avatarnetwork.io/network/assets/images/Telegram.png" alt="Telegram" data-toggle="tooltip" title="" data-placement="top" data-original-title="Telegram"></a>
</p>
