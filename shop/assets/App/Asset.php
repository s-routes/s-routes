<?php

namespace shop\assets\App;

use yii\web\AssetBundle;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@shop/assets/App/source';
    public $css      = [
        'sort-table.css',
        'second.css',
        'third.css',
        'footer.css',
    ];
    public $js       = [
        'gsss.js',
        'script.js',
        'second.js',
        'third.js',
    ];
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\web\JQueryAsset',
        'yii\bootstrap\BootstrapThemeAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
