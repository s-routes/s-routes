#!/bin/bash

# param 1 PROD_DB_TOKEN
# param 2 сайт откуда брать БД
# param 3 DB PASSWORD

# Скачиваю пустую БД
sudo curl -k -o s_routes_prod_main.sql.gz    -d "file=s_routes_prod_main&key=OFdtTeWuTlT3ZW76Xbfi" -X POST https://neiro-n.com/db/get
sudo curl -k -o s_routes_prod_stat.sql.gz    -d "file=s_routes_prod_stat&key=OFdtTeWuTlT3ZW76Xbfi" -X POST https://neiro-n.com/db/get
sudo curl -k -o s_routes_prod_wallet.sql.gz  -d "file=s_routes_prod_wallet&key=OFdtTeWuTlT3ZW76Xbfi" -X POST https://neiro-n.com/db/get

# Распаковываю БД
gunzip -d s_routes_prod_main.sql.gz
gunzip -d s_routes_prod_stat.sql.gz
gunzip -d s_routes_prod_wallet.sql.gz

# Залить базу данных
docker exec s-routes-mysql mysql -u root -p$3 < init.sql
docker exec s-routes-mysql ./db_load.sh $3
docker exec s-routes-mysql3 ./db_load3.sh $3