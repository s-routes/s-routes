#!/bin/bash

#Копируем скрипт в

destination="/root/backups_db_prod/dumps"
fdate=`date +%F` #Префикс по дате для структурирования резервных копий


find $destination -type d -ctime +30 -exec rm -R {} \; 2>&1

mkdir $destination/$fdate 2>&1

cp /root/backups_db_prod/dumps/today/10m $destination/$fdate