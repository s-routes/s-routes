<?php

namespace coin\controllers;

use avatar\models\WalletVVB;
use avatar\modules\UniSender\UniSender;
use common\components\providers\ETH;
use common\models\avatar\Currency;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillDefault;
use common\models\UserAvatar;
use common\models\UserDocument;
use common\services\Security\AES;
use cs\base\BaseController;
use cs\services\SitePath;
use cs\services\UploadFolderDispatcher;
use cs\services\VarDumper;
use cs\web\Exception;
use Yii;
use yii\base\UserException;
use yii\bootstrap\ActiveForm;
use yii\db\Connection;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\web\Response;

class CabinetController extends \avatar\controllers\CabinetBaseController
{
    /**
     */
    public function actionIndex()
    {
        return $this->render();
    }

    /**
     */
    public function actionReferalLink()
    {
        return $this->render();
    }

    /**
     */
    public function actionReferalTransactions()
    {
        return $this->render();
    }

    /**
     */
    public function actionReferalStructure()
    {
        return $this->render();
    }


    /**
     */
    public function actionShopRequests()
    {
        return $this->render();
    }

    /**
     */
    public function actionShopRequestsItem($id)
    {
        $request = \common\models\shop\Request::findOne($id);
        if ($request->user_id != Yii::$app->user->id) {
            throw new \Exception('Это не ваш заказ');
        }

        return $this->render([
            'request' => $request,
        ]);
    }

    /**
     */
    public function actionChain()
    {
        return $this->render();
    }

    /**
     */
    public function actionActive()
    {
        return $this->render();
    }

    /**
     */
    public function actionToken()
    {
        return $this->render();
    }

    /**
     */
    public function actionPacket()
    {
        return $this->render();
    }


    public function actionProfile()
    {
        $model = \avatar\models\forms\Profile::findOne(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->update();
            Yii::$app->session->setFlash('contactFormSubmitted');
        }

        return $this->render([
            'model' => $model,
        ]);
    }

    /**
     * Добавляет документ
     *
     * @return string|Response
     */
    public function actionDocumentsAdd()
    {
        $model = new \avatar\models\forms\UserDocument();

        if ($model->load(Yii::$app->request->post()) && $model->insert()) {
            Yii::$app->session->setFlash('contactFormSubmitted', $model->id);

            $document = UserDocument::findOne($model->id);
            $document->created_at = time();
            $document->hash = hash('sha256', file_get_contents(Yii::getAlias('@webroot' . $document->file)) . $document->data);
            $document->user_id = Yii::$app->user->id;
            $document->save();

            return $this->refresh();
        } else {
            return $this->render('documents-add', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Заканчивает процедуру регистрации
     *
     * @return string
     */
    public function actionRegistrationActivate()
    {
        $model = new \avatar\models\forms\RegistrationActivate();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $seeds = $model->action();
            Yii::$app->session->set('seeds', $seeds);
            Yii::$app->session->setFlash('form', 1);

            return $this->refresh();
        } else {
            return $this->render('registration-activate', [
                'model' => $model,
            ]);
        }
    }

    public function actionProfileSettings()
    {
        /** @var \avatar\models\forms\ProfileSettings $model */
        $model = \avatar\models\forms\ProfileSettings::findOne(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            Yii::$app->session->setFlash('form');

            return $this->refresh();
        } else {
            return $this->render('profile-settings', [
                'model' => $model,
            ]);
        }
    }

    public function actionProfileSecurity()
    {
        return $this->render('profile-security');
    }


}
