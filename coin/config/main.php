<?php

$params = \yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$config = [
    'id'                                              => 'sroutes-coin',
    'basePath'                                        => dirname(__DIR__),
    'bootstrap'                                       => ['log'],
    'controllerNamespace'                             => 'coin\controllers',
    'vendorPath'                                      => dirname(dirname(__DIR__)) . '/vendor',
    'language'                                        => 'ru',
    'sourceLanguage'                                  => 'ru',
    'timeZone'                                        => 'Etc/GMT-3',
    'aliases'                                         => [
        '@csRoot'  => __DIR__ . '/../../common/app',
        '@cs'      => __DIR__ . '/../../common/app',
        '@webroot' => __DIR__ . '/../../public_html',
    ],
    'components'                                      => [
        'session'         => [
            'timeout' => 600,
            'class'   => 'yii\web\CacheSession',
            'cache'   => 'cacheSession',
        ],
        'cacheSession' => [
            'class'     => 'yii\caching\FileCache',
            'keyPrefix' => 'session',
        ],
        'urlManager'         => [
            'rules' => [
                '/'              => 'site/index',
            ],
            'hostInfo'            => env('SHOP_URL_MANAGER_HOST_INFO'),
        ],
        'languageManager' => [
            'class'    => 'common\components\LanguageManager',
            'default'  => 'ru',
            'strategy' => [
                'ru' => ['ru', 'be', 'uk'],
                'en' => ['en', 'fr', 'de'],
            ],
        ],
        'authManager'     => [
            'class' => 'yii\rbac\PhpManager',
        ],
        'assetManager'    => [
            'appendTimestamp' => true,
        ],
        'piramida'        => [
            'class' => 'app\common\components\Piramida',
        ],
        'request'         => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey'    => '',
            'enableCookieValidation' => false,
        ],
        'deviceDetect'    => [
            'class'     => 'avatar\services\DeviceDetect',
            'setParams' => 'false',
        ],
        'user'            => [
            'identityClass'   => 'common\models\UserAvatar',
            'enableAutoLogin' => true,
            'loginUrl'        => ['auth/login'],
        ],
        'errorHandler'    => [
            'errorAction' => 'site/error',
            'errorView'   => '@avatar/views/site/error.php',
        ],
        'mailer'           => [
            'viewPath' => '@avatar/mail',

        ],

    ],
    'params'                                          => $params,
    'controllerMap'                                   => [
        'upload'  => '\common\widgets\HtmlContent\Controller',
        'upload3' => '\common\widgets\FileUploadMany2\UploadController',
        'upload4' => '\iAvatar777\assets\JqueryUpload1\Upload2Controller',
    ],
    'on beforeRequest'                                => function ($event) {
        Yii::$app->languageManager->onBeforeRequest();
        Yii::$app->onlineManager->onBeforeRequest();
        Yii::$app->monitoring->onBeforeRequest();
    },
    'on ' . \yii\web\Application::EVENT_AFTER_REQUEST => function ($event) {
        Yii::$app->monitoring->onAfterRequest();
    }
];

if (env('MEMCACHE_ENABLED', false)) {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\MemCache',
        'useMemcached' => true,
        'servers'      => [
            [
                'host' => env('MEMCACHE_HOST', 'localhost'),
                'port' => env('MEMCACHE_PORT', 11211),
            ],
        ],
    ];
} else {
    $config['components']['cacheSession'] = [
        'keyPrefix'    => 'session',
        'class'        => 'yii\caching\FileCache',
    ];
}

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
