<?php

return [
    'adminEmail'         => 'admin@galaxysss.ru',
    'mailList'           => [
        'contact' => 'god@avatar-bank.com',
    ],
    'mailer'                        => [
        // адрес который будет указан как адрес отправителя для писем и рассылок
        'from' => [
            env('MAILER_FROM_EMAIL') => env('MAILER_FROM_NAME'),
        ],
    ],
    'google'             => [
        'plugins' => [
            'reCaptcha' => [
            ],
        ],
    ],
    'rootSite' => 'https://s-routes.com'
];
