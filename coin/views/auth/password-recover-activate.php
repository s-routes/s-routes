<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\PasswordNewRecover */
/* @var $user \common\models\UserAvatar */

$this->title = 'Восстановление пароля';
?>
<div class="container">

    <div class="site-contact">
        <h1 class="page-header text-center"><?= Html::encode($this->title) ?></h1>


        <div class="row">
            <div class="col-lg-4 col-lg-offset-4">
                <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
                    <div class="alert alert-success">
                        Поздравляем вы успешно установили новый пароль.
                    </div>
                    <hr>
                    <p><a href="/cabinet/index" class="btn btn-success" style="width:100%;">Перейти в кабинет</a>
                <?php else: ?>
                    <?php $form = ActiveForm::begin([
                        'id' => 'contact-form',
                    ]); ?>
                    <?= $form->field($model, 'password1')->passwordInput()->label('Пароль') ?>
                    <?= $form->field($model, 'password2')->passwordInput()->label('Повторите пароль') ?>

                    <hr>
                    <div class="form-group">
                        <?= Html::submitButton('Подтвердить', [
                            'class' => 'btn btn-primary',
                            'name'  => 'contact-button',
                            'style' => 'width:100%;',
                        ]) ?>
                    </div>
                    <?php ActiveForm::end(); ?>


                <?php endif; ?>
            </div>
        </div>

    </div>
</div>