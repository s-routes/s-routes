<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 21.01.2017
 * Time: 18:16
 */

/** @var \avatar\services\CustomizeService $CustomizeService */
/** @var array $companySocialNets telegram, facebook, vk, skype, youtube */

/**
 * Last Update.
 * User: slpv
 * Date: 12.12.2019
 * Time: 13:29
 */
$user_id = Yii::$app->user->id;
\avatar\assets\SocketIO\Asset::register($this);
\avatar\assets\Notify::register($this);

?>

</div>

<footer>
    <div class="sections">
        <div class="section">

            <div class="messegers">
                <ul>
                    <li><a href="#"><i class="fab fa-telegram-plane"></i></a></li>
                    <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                </ul>
            </div>

            <div class="title">© Все права защищены. neironcoin.com 2019-2021</div>

        </div>
    </div>
</footer>

<?php
$serverName = 'http://chat.s-routes.com';
if (YII_ENV_TEST) $serverName = 'http://chat-stage.s-routes.com';
if (YII_ENV_DEV) $serverName = 'http://chat-test.s-routes.com';

/**
 * УВЕДОМЛЕНИЯ типа ПУШ (начало)
 */
if (!Yii::$app->user->isGuest) {
    $this->registerJs(<<<JS

if (typeof socket == 'undefined') {
    var socket = io.connect('{$serverName}');
}

let myid_f = {$user_id};

function nott(deal, name, mes) {
    var noty = new Noty({
        timeout: 10000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: '<a href="/cabinet-exchange/deal-action?id='+deal+'"><b>Сделка №'+deal+'</b></a>' +
              '<br>Новое сообщение от ' + name + '' +
               '<br>' + mes + '<br>',
        buttons: [
            Noty.button(
                'Перейти в сделку', 'btn btn-success', 
                function () {
                    location.href = "/cabinet-exchange/deal-action?id="+deal
                },
                {
                id: 'button1',
                'data-status': 'ok'
                }),
                
            Noty.button('Скрыть', 'btn btn-success', 
                function () {
                    noty.close();
                })
        ]
    });

    noty.show();
}
function PushDeal(deal, status) {
    
    let statuses = [" ","Сделка открыта","Сделка подтверждена и монеты заблокированы", "Деньги отправлены", "Деньги получены", "Партнер поставил оценку и сделка завершена", "", "Арбитр принял сделку", "", "", "Сделка отменена"];
    
    var noty = new Noty({
        timeout: 5000,
        theme: 'nest',
        type: 'info',
        layout: 'bottomLeft',
        text: '<b>Статус сделка #' + deal + '</b>' +
               '<br>' + statuses[status] + '<br>',
    });
    noty.show();
}

socket.emit('new-room', 0);
socket.emit('new-user', 0, myid_f);

socket.on('deal-opened', function(deal_id, user_id, partner_user_id) {
    console.log(["deal-opened", deal_id, user_id, partner_user_id]);
    if (partner_user_id == myid_f) {
        STATUS = 1;
        var noty = new Noty({
        timeout: 10000,
        theme: 'relax',
        type: 'warning',
        layout: 'bottomLeft',
        text: '<a href="/cabinet-exchange/deal-action?id='+deal_id+'"><b>Сделка №'+deal_id+'</b></a>' +
              '<br>Открыта сделка',
        buttons: [
            Noty.button(
                'Перейти в сделку', 'btn btn-success', 
                function () {
                    location.href = "/cabinet-exchange/deal-action?id="+deal_id
                },
                {
                id: 'button1',
                'data-status': 'ok'
                }),
                
            Noty.button('Скрыть', 'btn btn-success', 
                function () {
                    noty.close();
                })
        ]
    });

    noty.show();
    }     
});

socket.on('reload-status', data => {
    console.log(["reload-status", 'footer', data]);
    if (data.user_id == myid_f) {
        STATUS = data.status;
        PushDeal(data.deal, data.status);
    }     
});

socket.on('accept-arbitr', (deal_id, user_id, to) => {
    console.log(["accept-arbitr", deal_id, user_id, to]);
    if ($.inArray(myid_f, to) >= 0) {
        PushDeal(deal_id, 7);
    }     
});

/**
* 
* @param deal_id идентификатор сделки
* @param user_id идентификатор пользователя от кого пришло уведомление
* @param to массив идентификаторов пользователей кому нужно показать уведомление
* @param to_user объект пользователя в чью пользу принято решение по сделке {id,name, avatar}
*/
socket.on('arbitr-result-deal', (deal_id, user_id, to, to_user) => {
    console.log(["arbitr-result-deal", deal_id, user_id, to, to_user]);
    if ($.inArray(myid_f, to) >= 0) {
        var noty = new Noty({
            timeout: 5000,
            theme: 'nest',
            type: 'info',
            layout: 'bottomLeft',
            text: '<b>Статус сделки #' + deal_id + '</b>' +
                   '<br>' + 'Сделка разобрана арбитром и завершена' + '<br>' + 'Решение было принято в пользу ' + to_user.name,
        });
        noty.show();
    }     
});
/**
* 
* @param deal_id идентификатор сделки
* @param user_id идентификатор пользователя от кого пришло уведомление
* @param to массив идентификаторов пользователей кому нужно показать уведомление
* @param to_user объект пользователя в чью пользу принято решение по сделке {id,name, avatar}
*/
socket.on('arbitr-result-offer', (deal_id, user_id, to, to_user) => {
    console.log(["arbitr-result-offer", deal_id, user_id, to, to_user]);
    if ($.inArray(myid_f, to) >= 0) {
        var noty = new Noty({
            timeout: 5000,
            theme: 'nest',
            type: 'info',
            layout: 'bottomLeft',
            text: '<b>Статус сделки #' + deal_id + '</b>' +
                   '<br>' + 'Сделка разобрана арбитром и завершена' + '<br>' + 'Решение было принято в пользу ' + to_user.name,
        });
        noty.show();
    }     
});
            
socket.on('push', datap => {
    data = datap;
    if ((new URL(window.location.href)).searchParams.get("id") != data.deal) {
        if ($.inArray(myid_f, data.to_user_id) >= 0) {
            nott(data.deal, data.name, data.message);
        } 
    }
});


/**
* Уведомление от партнера что он вызвал арбитра
* @param int deal_id
* @param int user_id
* @param int to_user
*/
socket.on('need-arbitr2', function(deal_id, user_id, to_user) {
    console.log('need-arbitr2', deal_id, user_id, to_user);
    
    if (myid_f == to_user) {
        var noty = new Noty({
            timeout: 10000,
            theme: 'relax',
            type: 'warning',
            layout: 'bottomLeft',
            text: '<b>Статус сделки #' + deal_id + '</b>' +
                   '<br>' + 'Партнер вызвал арбитра'
        });
    
        noty.show();
    }
});

JS
    );
    $isProd = (YII_ENV_PROD)? 1 : 2;
if (Yii::$app->user->can('permission_arbitrator')) {
    $this->registerJs(<<<JS

socket.on('new-arbitr', function(user_id) {
    console.log('new-arbitr', user_id);
});

/**
* Уведомление для арбитра
* @param int deal_id
* @param int user_id
* @param int partner_user_id
*/
socket.on('need-arbitr', function(deal_id, user_id, partner_user_id) {
    console.log('need-arbitr', deal_id, user_id, partner_user_id);
    
    var noty = new Noty({
        timeout: 10000,
        theme: 'relax',
        type: 'warning',
        layout: 'bottomLeft',
        text: '<a href="/cabinet-arbitrator/item?id=' + deal_id + '"><b>Сделка №' + deal_id + '</b></a>' +
              '<br>Сделкка требует арбитра',
        buttons: [
            Noty.button(
                'Принять и перейти в сделку', 'btn btn-success', 
                function () {
                    ajaxJson({
                        url: '/cabinet-arbitrator/accept' + '?' + 'id' + '=' + deal_id,
                        success: function (ret) {
                            socket.emit('accept-arbitr', deal_id, {$user_id}, [user_id, partner_user_id]);
                            window.location = '/cabinet-arbitrator/item?id=' + deal_id;
                        }
                    });
                },
                {
                    "id": 'button1',
                    'data-status': 'ok'
                }),
                
            Noty.button('Скрыть', 'btn btn-success', 
            function () {
                noty.close();
            })
        ]
    });

    noty.show();
});

socket.emit('new-arbitr', myid_f);

JS
);
}

}
?>
