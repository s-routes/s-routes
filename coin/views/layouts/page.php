<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 01.11.2016
 * Time: 9:42
 */

/** @var $this \yii\web\View */
/** @var $content */

\coin\assets\App\Asset::register($this);
\avatar\assets\LayoutMenu\Asset::register($this);
\avatar\assets\FontAwesome::register($this);
\avatar\assets\Prizmology\Asset::register($this);
\avatar\assets\BootstrapSroutes::register($this);

use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs(<<<JS
$('[data-toggle="tooltip"]').tooltip();
JS
);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <script src="https://kit.fontawesome.com/b99e675b6e.js"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $this->title ?></title>
    <link rel="shortcut icon" href="/images/layouts/main/N3-w-128.png">
    <?= Html::csrfMetaTags() ?>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody(); ?>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <a class="navbar-brands">
                <a class="navbar-brands" href="/">
                    <img src="/images/controller/site/index/coin.png" alt="logo" class="logo_img">
                </a>
            </a>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <ul class="dropdown-menu">
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            </ul>
        </div>
    </div>
</nav>

<div style="flex-grow: 1;">

    <?= $content ?>

</div>

<footer>
    <div class="sections">
        <div class="section">

            <div class="messegers">
                <ul>
                </ul>
            </div>

        </div>
    </div>
</footer>

<?php if (Yii::$app->controller->layout != 'cabinet') { ?>
    <?= $this->render('_counters') ?>
<?php } ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>