<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 11.11.2016
 * Time: 1:44
 */

/** @var $this \yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

if (!Yii::$app->user->isGuest)  {
    $c = \common\models\shop\Basket::getCount(); ?>
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brands" href="#">
                    <a class="navbar-brands" href="/">
                        <img src="/images/controller/site/index/coin.png" alt="logo" class="logo_img">
                    </a>
                </a>

                <li class="hidden-sm hidden-md hidden-lg"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><button type="button" class="btns btn-danger" data-toggle="button"><img src="<?= \Yii::$app->params['rootSite'] ?><?= Yii::$app->user->identity->getAvatar() ?>" class="img-profile" width="30">МОЙ КАБИНЕТ</button></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/shop/cart">
                                </i>
                                КОРЗИНА
                                <span class="label label-success basketCounter" style="margin-top: 0px;margin-bottom: 0px;">
                                <?= $c ?>
                            </span>
                            </a>
                        </li>
                        <li><a href="/cabinet/index">ПРОФИЛЬ</a></li>
                        <li><a href="/cabinet-bills/index">МОИ БАЛАНСЫ</a></li>
                        <li><a href="/cabinet-shop-requests/index">ЗАКАЗЫ (магазин)</a></li>
                        <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post">ВЫЙТИ</a></li>
                    </ul>
                </li>

            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-rss mr-1" style="padding: 7px 9px;"></i>ИНФОРМАЦИЯ</a>
                        <ul class="dropdown-menu">
                            <li><a href="/page/about">О КОМПАНИИ</a></li>
                            <li><a href="/page/blog">БЛОГ</a></li>
                            <li><a href="/page/contact">КОНТАКТЫ</a></li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-xs">
                        <a href="/shop/cart">
                            <i class="fa fa-shopping-basket rounded-circle bg-white mr-1" style="padding: 7px 6px;">
                            </i>
                            КОРЗИНА
                            <span class="label label-success basketCounter" style="margin-top: 10px;margin-bottom: 40px;">
                                <?= $c ?>
                            </span>
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><img src="<?= \Yii::$app->params['rootSite'] ?><?= Yii::$app->user->identity->getAvatar() ?>" class="img-profile" width="35">МОЙ КАБИНЕТ</a>
                        <ul class="dropdown-menu">
                            <li><a href="/cabinet/index">ПРОФИЛЬ</a></li>
                            <li><a href="/cabinet-bills/index">МОИ БАЛАНСЫ</a></li>
                            <li><a href="/cabinet-shop-requests/index">ЗАКАЗЫ (магазин)</a></li>
                            <li><a href="<?= Url::to(['auth/logout']) ?>" data-method="post">ВЫЙТИ</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

<?php } else { ?>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brands" href="#">
                    <a class="navbar-brands" href="/"><img src="<?= \Yii::$app->params['rootSite'] ?>/images/vvf-logo1.png" alt="logo"></a>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-sitemap mr-1" style="padding: 7px 9px;"></i>О ПРОЕКТЕ</a>
                        <ul class="dropdown-menu">
                            <li><a href="/page/news">НОВОСТИ</a></li>
                            <li><a href="/page/contact">КОНТАКТЫ</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/auth/login"><i class="fa fa-user rounded-circle bg-white mr-1" style="padding: 7px 9px;"></i>ВОЙТИ</a></li>
                    <li><a href="/auth/registration"><i class="fa fa-user-plus rounded-circle bg-white mr-1" style="padding: 7px 6px;"></i>РЕГИСТРАЦИЯ</a></li>
                </ul>
            </div>
        </div>
    </nav>
<?php } ?>
<div style="flex-grow: 1;">
