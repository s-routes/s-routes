<?php


/* @var $this yii\web\View */

$this->title = \Yii::t('c.qb8Cvmts0H', 'Монета NEIRON');

$this->registerJs('$(".lang-switcher").css("width", "160px")');
?>



<section class="top-layer">
    <header>
        <div class="container">
            <div class="row align-items-center">

                <div class="col header-contain">
                    <div class="lang-switcher-container">
                        <?php $l = \common\models\Language::findOne(['code' => Yii::$app->language]) ?>
                        <div class="avatar-btn--header-white lang-switcher" style="background-color: white; color: black"><?= strtoupper(Yii::$app->language) ?></div>
                        <div class="lang-choose-layer">
                            <ul>
                                <li><a href="javascript:void(0);" class="buttonLanguageSet" data-code="ru">ru</a></li>
                                <li><a href="javascript:void(0);" class="buttonLanguageSet" data-code="en">en</a></li>
                            </ul>
                        </div>
                    </div>

                    <?php if (!Yii::$app->deviceDetect->isMobile()) { ?>
                        <p style="font-size: 100%; color: #888;"><?= \Yii::t('c.qb8Cvmts0H', 'Монета NEIRON') ?></p>
                    <?php } ?>

                    <p><a href="https://neiro-n.com/auth/login" class="avatar-btn--header-login" target="_blank" style="background-color: white; color: black"><?= \Yii::t('c.YqmF2Dqma4', 'Войти') ?> →</a></p>

                </div>
            </div>
        </div>
    </header>
    <div class="container full-height">
        <div class="row top-layer__row full-height justify-content-center align-items-center">
            <div class="col text-center top-layer-col">
                <?php if (Yii::$app->deviceDetect->isMobile()) { ?>
                    <div class="top-layer__logo reveal">
                        <img src="/images/controller/site/index/coin.png" alt="logo2" style="margin-bottom: 0px;" width="200">
                    </div>

                    <p style="color: black; font-size: 100%;
    text-align: center;
    display: inline-block;
    padding: 18px 50px;
    border-radius: 30px;
" class="avatar-btn--white" >
                        <b><?= \Yii::t('c.qb8Cvmts0H', 'ЦИФРОВОЙ ТОКЕН NEIRON (NEIRO ERC-20)') ?></b><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'СМАРТ-КОНТРАКТ НА БЛОКЧЕЙНЕ ETHEREUM') ?><br>

                        ****<br>

                        <b><?= \Yii::t('c.qb8Cvmts0H', 'МЕДАЛЬОН ИЗ ЧИСТОГО СЕРЕБРА Ag 999+ (Fine Silver Coin)') ?></b><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'БАЗОВЫЙ МАТЕРИАЛЬНЫЙ ЭКВИВАЛЕНТ (PROOF)') ?><br>

                        ****<br>

                        <b><?= \Yii::t('c.qb8Cvmts0H', 'PROOF of VALUE of METALL (PoVM)') ?></b><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'АЛГОРИТМ ОЦЕНКИ БАЛАНСА СООТНОШЕНИЯ СТОИМОСТИ') ?><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'ЦИФРОВЫХ И МАТЕРИАЛЬНЫХ АКТИВОВ') ?><br>
                    </p>
                    <p><a href="<?= \Yii::t('c.qb8Cvmts0H', 'WP1') ?>" class="avatar-btn avatar-btn--white reveal"><?= \Yii::t('c.qb8Cvmts0H', 'Белая книга') ?> PDF</a></p>

                <?php } else { ?>
                    <div class="top-layer__logo reveal">
                        <img src="/images/controller/site/index/coin.png" alt="logo1" style="margin-bottom: 0px;" width="300">
                    </div>

                    <p style="color: black; font-size: 100%;
    text-align: center;
    display: inline-block;
    padding: 18px 50px;
    border-radius: 30px;
" class="avatar-btn--white" >
                        <b style="font-weight: 900;"><?= \Yii::t('c.qb8Cvmts0H', 'ЦИФРОВОЙ ТОКЕН NEIRON (NEIRO ERC-20)') ?></b><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'СМАРТ-КОНТРАКТ НА БЛОКЧЕЙНЕ ETHEREUM') ?><br>

                        ****<br>

                        <b style="font-weight: 900;"><?= \Yii::t('c.qb8Cvmts0H', 'МЕДАЛЬОН ИЗ ЧИСТОГО СЕРЕБРА Ag 999+ (Fine Silver Coin)') ?></b><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'БАЗОВЫЙ МАТЕРИАЛЬНЫЙ ЭКВИВАЛЕНТ (PROOF)') ?><br>

                        ****<br>

                        <b style="font-weight: 900;"><?= \Yii::t('c.qb8Cvmts0H', 'PROOF of VALUE of METALL (PoVM)') ?></b><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'АЛГОРИТМ ОЦЕНКИ БАЛАНСА СООТНОШЕНИЯ СТОИМОСТИ') ?><br>

                        <?= \Yii::t('c.qb8Cvmts0H', 'ЦИФРОВЫХ И МАТЕРИАЛЬНЫХ АКТИВОВ') ?><br>
                    </p>
                    <p><a href="<?= \Yii::t('c.qb8Cvmts0H', 'WP1') ?>" class="avatar-btn avatar-btn--white reveal"><?= \Yii::t('c.qb8Cvmts0H', 'Белая книга') ?> PDF</a></p>

                <?php } ?>


            </div>
        </div>
    </div>
</section>







<style>
    .container p {
        text-align: center;
        font-size: 150%;
    }
    .container li {
        text-align: center;
        font-size: 150%;
    }
</style>
<section class="main-gradient contacts section-padding-sm color-white" style="background: url(/images/controller/site/index/01.jpg) repeat-y 50%;">
    <div class="container reveal" style="color: #000;">
        <p style="font-weight: bold;"><?= \Yii::t('c.qb8Cvmts0H', 'КОНЦЕПЦИЯ ТЕХНОЛОГИЧЕСКОЙ УСТОЙЧИВОСТИ СТОИМОСТИ ЦИФРОВОГО АКТИВА') ?><br>
            <br>
            <?= \Yii::t('c.qb8Cvmts0H', 'Продвигаемое видение ценности криптоактива') ?></p>

        <p><a href="/files/how_to_buy_neiron.pdf" class="avatar-btn avatar-btn--white reveal" target="_blank">Приобрести токены NEIRO (вариант 1) - P2P-площадка</a></p>
        <p><a href="/files/how_to_buy_neiron_convert.pdf" class="avatar-btn avatar-btn--white reveal" target="_blank">Приобрести токены NEIRO (вариант 2) - Конвертация</a></p>

        <p style="font-weight: bold;margin-top: 100px; "><?= \Yii::t('c.qb8Cvmts0H', 'ПОЧЕМУ ВЫБРАНО ИМЕННО СЕРЕБРО?') ?></p>


        <ul>
            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро на протяжении всей истории человечества считалось') ?></li>


            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро, как и другие драгметаллы') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро обеспечивает стабильность и отлично балансирует биржевые кризисы.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро обладает уникальными свойствами денег.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро повышает статусные рейтинги, так как ассоциируется с богатством и успехом.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро играет одну из ведущих ролей в международной торговле.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро – проверенное веками долгосрочное средство сбережения богатства.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро – самый популярный металл при производстве ювелирных изделий.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро является металлом, широко используемым во всех отраслях промышленности.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро используется в большинстве религий, культов, оно считается церковным металлом.') ?></li>

            <li>~ <?= \Yii::t('c.qb8Cvmts0H', 'Серебро наделено облагораживающими и исцеляющими') ?>
            </li>
        </ul>

        <p style="margin-top: 100px; font-weight: bold;"><?= \Yii::t('c.qb8Cvmts0H', 'АЛГОРИТМ ДОКАЗАТЕЛЬСТВА ВЛАДЕНИЕМ ЦЕННЫМ МЕТАЛЛОМ') ?></p>

        <p><?= \Yii::t('c.qb8Cvmts0H', 'Объём эмиссии цифровой и материальной составляющих рассчитывается') ?> -<br>
            <span style="font-weight: bold;">Proof of Value of Metall</span> (PoVM).</p>

        <p><?= \Yii::t('c.qb8Cvmts0H', 'Алгоритм') ?> <span style="font-weight: bold;">Proof of Value of Metall (PoVM)</span> - <?= \Yii::t('c.qb8Cvmts0H', 'это симбиоз производственно-аппаратно-программного комплексов') ?>.</p>

        <p><?= \Yii::t('c.qb8Cvmts0H', 'Благодаря электронной системе учёта, функциям личного кабинета') ?></p>

        <p><?= \Yii::t('c.qb8Cvmts0H', 'При желании, в любой момент можно выйти из электронного оборота') ?> <span style="font-weight: bold;">Ag 999+ (Fine Silver)</span>,<br>
            <?= \Yii::t('c.qb8Cvmts0H', 'в виде инвестиционных медальонов или других изделий из серебра.') ?>.</p>

        <p><?= \Yii::t('c.qb8Cvmts0H', 'В данной системе учета серебро является') ?></p>

        <p style="font-weight: bold;"><?= \Yii::t('c.qb8Cvmts0H', 'Девиз проекта: «Звонкая монета лучше любых звонких обещаний!»') ?></p>

        <p><?= \Yii::t('c.qb8Cvmts0H', 'Диверсифицируйте свои риски и будьте богаты...') ?></p>
    </div>
</section>



<section id="contacts" class="main-gradient contacts section-padding-sm color-white">
    <div class="container reveal">
        <div class="row">
            <div class="col-12">
                <a name="contacts2"></a><h1 class="text-center"><?= \Yii::t('c.qb8Cvmts0H', 'Обратная связь') ?></h1>
            </div>
        </div>
        <form class="contacts__form" action="">
            <?= \yii\helpers\Html::hiddenInput('_csrf', Yii::$app->request->csrfToken) ?>
            <div class="row">
                <div class="col-12 col-md-5 offset-md-1">
                    <input type="text" placeholder="<?= \Yii::t('c.YqmF2Dqma4', 'Ваше имя') ?>" name="name">
                </div>
                <div class="col-12 col-md-5">
                    <input type="text" placeholder="<?= \Yii::t('c.YqmF2Dqma4', 'Email') ?>" name="email">
                </div>
                <div class="col-12 col-md-10 offset-md-1">
                    <textarea name="text" placeholder="<?= \Yii::t('c.YqmF2Dqma4', 'Сообщение') ?>" cols="30" rows="5"></textarea>

                    <div class="text-right">
                        <input type="button" value="<?= \Yii::t('c.qb8Cvmts0H', 'Отослать сообщение') ?>" class="avatar-btn avatar-btn--white popup-open-mail" data-form="success" style="border: 0">
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<style>
    footer p {
        color: #000;
    }
</style>
<footer>
    <div class="sections">
        <div class="section">

            <table style="margin-bottom: 30px; margin-top: 50px;">
                <tr>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="/page/item?id=70" class="white">Вопросы-ответы</a></p>
                        <p><a href="/page/item?id=71" class="white">Новости</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="/page/item?id=72" class="white">Кратко о нас</a></p>
                        <p><a href="/page/item?id=73" class="white">База знаний</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="/page/item?id=74" class="white">Как купить</a></p>
                        <p><a href="/page/item?id=75" class="white">Где потратить</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="http://t.me/NeironCoinCommunity" class="white" target="_blank">Telegram</a></p>
                        <p><a href="https://www.youtube.com/channel/UCH7MEtOt3niudlhQjPJ3_0A" class="white" target="_blank">YouTube</a></p>
                    </td>
                    <td align="left" valign="top" style="padding-right: 30px;">
                        <p><a href="https://github.com/NeironCoin/main" class="white" target="_blank">GitHub</a></p>
                    </td>
                </tr>
            </table>

            <div class="title" style="margin-bottom: 20px;"><?= \Yii::t('c.qb8Cvmts0H', '© Все права защищены. neironcoin.com 2019-2020') ?></div>

        </div>
    </div>
</footer>

<div class="avatar__overlay"></div>
<div class="currency__popup">
    <div class="currency__popup-inner" style="width: 500px;">

    </div>
</div>

<script>
    var path = document.getElementsByClassName('desicion-svg-path');

    for (i=0;i < path.length;i++){
        path[i].style.strokeDasharray = path[i].getTotalLength();
        path[i].style.strokeDashoffset = path[i].getTotalLength();
    }
</script>
<script src="/network/assets/js/jquery-3.2.1.min.js"></script>
<script src="/network/assets/js/countdown.js"></script>
<script src="/network/assets/js/scrollreveal.min.js"></script>
<script src="/network/assets/js/wow.min.js"></script>
<script src="/network/assets/js/slick/slick.min.js"></script>
<script src="/network/assets/js/main.js?ver=1.0.3311"></script>

<?= $this->render('../blocks/shareMeta', [
    'image'       => \yii\helpers\Url::to('/images/vvf-logo1.png', true),
    'title'       => $this->title,
    'url'         => \yii\helpers\Url::current([], true),
    'description' => Yii::t('c.YqmF2Dqma4', 'УНИВЕРСАЛЬНЫЕ СЕРВИСЫ НЕЙРО: Р2Р обмен активами без комиссий, конфиденциальные чаты пользователей, рост ваших активов'),
]) ?>