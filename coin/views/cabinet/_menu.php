<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;
use yii\db\Query;
use yii\helpers\Url;

/* @var $this yii\web\View */
$requestedRoute = Yii::$app->requestedRoute;


$menu = [
    [
        'route' => 'cabinet/profile',
        'label' => Yii::t('c.kTtfiHPqec', 'Профиль'),
    ],


];


$menu[] =     [
    'route'   => 'cabinet/referal-link',
    'label'   => 'Реферальная ссылка',
    'urlList' => [
        ['startsWith', '/cabinet/referal-link'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet/referal-transactions',
    'label'   => 'Реферальные начисления',
    'urlList' => [
        ['startsWith', '/cabinet/referal-transactions'],
    ],
];
$menu[] =     [
    'route'   => 'cabinet/referal-structure',
    'label'   => 'Реферальная структура',
    'urlList' => [
        ['startsWith', '/cabinet/referal-structure'],
    ],
];


function hasRoute12($item, $route)
{
    if (isset($item['items'])) {
        foreach ($item['items'] as $i) {
            if ($i['route'] == $route) {
                return true;
            }
        }
    }
    if (isset($item['route'])) {
        if ($item['route'] == $route) return true;
    }
    if (\yii\helpers\ArrayHelper::keyExists('urlList', $item)) {
        foreach ($item['urlList'] as $i) {
            switch ($i[0]) {
                case 'route':
                    if ($route == $i[1]) return true;
                    break;
                case 'startsWith':
                    if (\yii\helpers\StringHelper::startsWith($route, $i[1])) return true;
                    break;
                case 'controller':
                    $arr = explode('/', $route);
                    if ($arr[0] == $i[1]) return true;
                    break;
            }
        }
    }

    return false;
}

?>

<div class="list-group">
    <?php
    foreach ($menu as $item) {
        $options = ['class' => ['list-group-item']];
        if (hasRoute12($item, Yii::$app->requestedRoute)) {
            $options['class'][] = 'active';
        }
        if (\yii\helpers\ArrayHelper::keyExists('data', $item)) {
            $options['data'] = $item['data'];
        }
        $options['class'] = join(' ', $options['class']);
        echo Html::a($item['label'], [$item['route']], $options);
    }
    ?>
</div>
