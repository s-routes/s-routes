<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \avatar\models\forms\Subscribe */

$this->title = 'Подписки';
?>
<div class="container">
    <div class="col-lg-12">

        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>


    </div>
    <div class="col-lg-8">
        <p>В этом разделе вы можете управлять своими подписками на рассылки.</p>
        <p>Если вы включите опции то вы будете получать рассылку на почту которую указали при регистрации. Если
            выключите опцию то не будете получать. По умолчанию эти опции включены после регистрации.</p>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <div class="alert alert-success">
                Поздравляем вы успешно установили новый пароль.
            </div>

        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'layout'      => 'horizontal',
                'fieldConfig' => [
                    'template'             => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label'   => 'col-sm-4',
                        'error'   => '',
                        'hint'    => '',
                    ],
                ],
            ]); ?>
            <?= $model->field($form, 'subscribe_is_news') ?>
            <?= $model->field($form, 'subscribe_is_blog') ?>
            <hr>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', [
                    'class' => 'btn btn-primary',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>


</div>