<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */

$this->title = 'Реферальная структура';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);

$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'sunset',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
/**
[
    0 => [
        'id' => 9
        'items' => [
            0 => [
                'id' => 4200
            ]
        ]
    ]
]
*/
function getTree($school)
{
    $data = ['items' => []];
    $arr = [];
    $partnerList = \common\models\UserPartner::find()->where(['school_id' => $school, 'parent_id' => Yii::$app->user->id])->all();
    /** @var \common\models\UserPartner $item */
    foreach ($partnerList as $item) {
        $item1 = ['id' => $item->user_id];

        $items = getNode($school, $item->user_id);

        if (count($items) > 0) {
            $item1['items'] = $items;
        }

        $arr[] = $item1;
    }

    return ['items' => $arr];
}

function getNode($school, $user_id)
{
    $arr = [];
    $partnerList = \common\models\UserPartner::find()->where(['school_id' => $school, 'parent_id' => $user_id])->all();
    /** @var \common\models\UserPartner $item */
    foreach ($partnerList as $item) {
        $item1 = ['id' => $item->user_id];

        $items = getNode($school, $item->user_id);
        if (count($items) > 0) {
            $item1['items'] = $items;
        }

        $arr[] = $item1;
    }

    return $arr;
}

?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>
    </div>
    <div class="col-lg-8">

        <p>Реферальная структура</p>
        <?php
        // собираю структуру
        $items = getTree(1);
        ?>

        <ul class="media-list">
            <li class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object"
                             src="<?= Yii::$app->user->identity->getAvatar() ?>"
                             style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
                        >
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?= Yii::$app->user->identity->getName2() ?></h4>
                    <span
                            class="label label-success"><?= Yii::$app->formatter->asCurrency(0) ?></span>
                    | <span
                            class="label label-info"><?= Yii::$app->formatter->asCurrency(0) ?></span>

                    <?= $this->render('referal-structure-tree', ['rows' => $items['items']]) ?>
                </div>
            </li>
        </ul>

    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



