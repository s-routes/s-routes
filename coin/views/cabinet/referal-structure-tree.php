<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 16.04.2016
 * Time: 20:21
 */
/** @var array $rows = [
 *      [
 *          'id' =>
 *          'items' => []
 *      ],
 * ] */

use app\models\Piramida\Transaction;
use app\models\User;
use app\models\Piramida\Wallet;
use app\models\Piramida\ReferalBonus;

?>


<?php foreach ($rows as $row) { ?>
    <?php $user = $row; ?>
    <?php $nodes = null; ?>
    <?php if (isset($row['items'])) $nodes = $row['items'];  ?>
    <?php unset($row['items']);  ?>
    <?php $userObject = \common\models\UserAvatar::findOne($row['id']) ?>
    <div class="media">
        <div class="media-left">
            <a href="/user/<?= $user['id'] ?>">
                <img
                    class="media-object"
                    src="<?= $userObject->getAvatar() ?>"
                    style="width: 64px; height: 64px; border-radius: 10px; border: 1px solid #ccc"
                    data-toggle="tooltip"
                    title="<?= $userObject->email ?>"
                    >
            </a>
        </div>
        <div class="media-body">
            <h4 class="media-heading"><?= $userObject->getName2() ?></h4>

            <!-- я заработал на нем (от него мне были бонусы)   -->
            <?php
//            $iAmId = Yii::$app->user->id;
//            $sum = ReferalBonus::find()
//            ->where([
//                'from' => $user['id'],
//                'to'   => $iAmId,
//            ])
//            ->select([
//                'sum(sum)'
//            ])
//            ->scalar();
//            $sum = $sum ? $sum : 0;
            $sum = 0;
            ?>
            <span class="label label-success" title="я заработал на нем (от него мне были бонусы)" data-toggle="tooltip">+ <?= $sum ?> р.</span> |

            <!-- сколько он заработал по партнерской программе   -->
            <?php
            // сколько ему перечислено денег на B по бонусам
//            $walletB = Wallet::find()->where([
//                'user_id' => $user['id'],
//                'type'    => 'B',
//            ])->select('id')->scalar();
//            if ($walletB) {
//                $sum = Transaction::find()
//                    ->select([
//                        'sum(summa)'
//                    ])
//                    ->andWhere([
//                        'operation' => Transaction::OPERATION_BONUS,
//                        'to'        => $walletB,
//                    ])
//                    ->scalar()
//                ;
//                $sum = $sum ? $sum : 0;
//            } else {
//                $sum = 0;
//            }
            $sum = 0;
            ?>
            <span class="label label-info" title="сколько он заработал по партнерской программе" data-toggle="tooltip">+ <?= $sum ?> р.</span>

            <?php if (!is_null($nodes)) { ?>
                <?= $this->render('referal-structure-tree', ['rows' => $nodes]); ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>
