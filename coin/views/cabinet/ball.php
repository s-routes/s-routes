<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $data array */

$this->title = 'Мои баллы';


$currencyVVB = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::VVB);
?>
<div class="container">
    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <ul class="nav nav-tabs">
        <li><a href="/cabinet-exchange/index">Обменник</a></li>
        <li class="active"><a href="/cabinet/ball">Мои Баллы</a></li>
        <li><a href="/cabinet-exchange/my-deals">Мои Сделки</a></li>
        <li><a href="/cabinet-exchange/offer">Мои Предложения</a></li>
    </ul>
    <br>
    <div class="col-lg-8">
        <?php if ($data['status'] == 1) { ?>
            <?= \yii\widgets\DetailView::widget([
                'model' => $data['wallets']['prizm'],
                'attributes' => [
                    'inSystemBalance' => [
                        'label' => 'Баланс PZM	',
                        'value' => Yii::$app->formatter->asDecimal($data['wallets']['prizm']['inSystemBalance'], 2),
                    ],
                    'wallet' => [
                        'label' => 'Кошелек PRIZM',
                        'value' => $data['wallets']['prizm']['wallet'],
                    ],
                    'publickey' => [
                        'label' => 'Публичный ключ',
                        'format' => 'html',
                        'value' => Html::tag('code', $data['wallets']['prizm']['publickey']),
                    ],
                    'inPull' => [
                        'label' => 'Баланс в общем пуле',
                        'value' => Yii::$app->formatter->asDecimal($data['wallets']['prizm']['inPull'] / 100, 2)
                    ],
                    'balance' => [
                        'label' => 'Баланс баллов',
                        'value' => Yii::$app->formatter->asDecimal($data['balance'], $currencyVVB->decimals),
                    ],
                ]
            ]) ?>
        <?php } ?>
        <?php if ($data['status'] == 3) { ?>
            <p class="alert alert-danger"><?= $data['error'] ?></p>
        <?php } ?>
    </div>
    <div class="col-lg-4">
        <?= $this->render('_menu') ?>
    </div>
</div>



