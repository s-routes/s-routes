<?php
use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

/** @var \yii\web\View $this */
$this->title = $user->getName2();

$cardList = \common\models\avatar\UserBill::find()
    ->where([
        'user_id' => Yii::$app->user->id,
        'mark_deleted' => 0,
    ])
    ->groupBy(['card_id'])
    ->select(['card_id'])
    ->column();

$carListObjects = \common\models\Card::find()->where(['id' => $cardList])->all();

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container" style="padding-bottom: 70px;">

    <div class="col-lg-8">
        <h1 class="page-header text-center">
            <?= $user->getName2() ?>
        </h1>

        <p class="text-center">
            <img src="https://s-routes.com<?= $user->getAvatar() ?>" width="300" class="img-circle">
        </p>
        <p class="text-center">
            <code title="Нажми чтобы скопировать" data-toggle="tooltip" data-clipboard-text="<?= $user->getAddress() ?>" class="buttonCopy"><?= $user->getAddress() ?></code>
        </p>

        <p class="text-center"><a href="/cabinet-buy/index" class="btn btn-success">Купить монеты</a></p>

    </div>
    <div class="col-lg-4">
        <?= $this->render('../cabinet/_menu') ?>
    </div>
</div>


