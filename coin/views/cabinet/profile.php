<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UnionCategory;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model cs\base\BaseForm */

$this->title = Yii::t('c.FMg0mLqFR9', 'Профиль');


?>
<div class="container">

    <div class="col-lg-12">
        <h1 class="page-header"><?= Html::encode($this->title) ?></h1>

    </div>
    <div class="col-lg-8">
        <ul class="nav nav-tabs">
            <li><a href="/cabinet/index">Кабинет</a></li>
            <li class="active"><a href="/cabinet/profile">Редактировать профиль</a></li>
            <li><a href="/cabinet-google-code/set">Двухфакторная авторизация</a></li>
        </ul>
        <br>
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

            <p class="alert alert-success">
                <?= \Yii::t('c.FMg0mLqFR9', 'Успешно обновлено') ?>.
            </p>

            <p>
                <a href="/cabinet/profile" class="btn btn-primary">
                    Профиль
                </a>
            </p>
        <?php else: ?>

            <?php $form = ActiveForm::begin([
                'id'      => 'contact-form',
                'options' => ['enctype' => 'multipart/form-data'],
                'layout'  => 'horizontal',
            ]); ?>
            <?= $model->field($form, 'name_first') ?>
            <?= $model->field($form, 'name_last') ?>
            <?= $model->field($form, 'avatar') ?>
            <?= $model->field($form, 'country_id')->dropDownList(
                    \yii\helpers\ArrayHelper::map(
                            \common\models\Country::find()->all(),
                            'id',
                            'name'
                    ), ['prompt' => '- Ничего не выбрано -']
            ) ?>
            <?= $model->field($form, 'town') ?>
            <?= $model->field($form, 'address') ?>


            <hr class="featurette-divider">
            <div class="form-group">
                <?= Html::submitButton(Yii::t('c.FMg0mLqFR9', 'Обновить'), [
                    'class' => 'btn btn-success',
                    'name'  => 'contact-button',
                    'style' => 'width: 100%;',
                ]) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php endif; ?>
    </div>

</div>



