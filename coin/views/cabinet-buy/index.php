<?php
use yii\helpers\Html;
use yii\helpers\Url;


/** @var \common\models\UserAvatar $user */
$user = Yii::$app->user->identity;

/** @var \yii\web\View $this */
$this->title = 'Покупка монет NEIRO';

\avatar\assets\Notify::register($this);
\avatar\assets\Clipboard::register($this);


?>
<?php
$this->registerJs(<<<JS

var clipboard = new Clipboard('.buttonCopy');
clipboard.on('success', function(e) {

    new Noty({
        timeout: 1000,
        theme: 'relax',
        type: 'success',
        layout: 'bottomLeft',
        text: 'Скопировано'
    }).show();

});

JS
);
?>

<div class="container" style="padding-bottom: 70px;">

    <div class="col-lg-12">
        <h1 class="page-header text-center">
            <?= $this->title ?>
        </h1>



    </div>
</div>


