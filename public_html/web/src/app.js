/* global $, Wallet*/
var async = require('async');
var Web3 = require('web3');
var ethTx = require('ethereumjs-tx');
var SolidityFunction = require('web3/lib/web3/function');
var Wallet = require('./wallet');

var ABI = [{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"memberData","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"memberId","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"kill","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"members","outputs":[{"name":"member","type":"address"},{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"approved","type":"bool"},{"name":"memberSince","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getMemberCount","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"changeOwner","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getMember","outputs":[{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"member","type":"address"},{"name":"name","type":"string"},{"name":"surname","type":"string"},{"name":"patronymic","type":"string"},{"name":"birthDate","type":"uint256"},{"name":"birthPlace","type":"string"},{"name":"avatarHash","type":"string"},{"name":"avatarID","type":"uint256"},{"name":"data","type":"string"}],"name":"addMember","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"pks","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"id","type":"uint256"}],"name":"getPK","outputs":[{"name":"","type":"string"}],"payable":false,"type":"function"},{"inputs":[],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberAdded","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"member","type":"address"},{"indexed":false,"name":"id","type":"uint256"}],"name":"MemberChanged","type":"event"}];

var url = 'http://213.248.20.80:7777/';
var web3 = new Web3(new Web3.providers.HttpProvider(url));

var contractAddress = '0xff6c78bfedf1bdb2d85071a9040798f8057a0a36';
web3.eth.defaultAccount = '0x809bd2e5a62ec903242efb75b7af840c0158fc39';
var contract = web3.eth.contract(ABI).at(contractAddress);

contract.MemberAdded(function(error, result) {
    if (!error)
        console.log('MemberAdded\n' + JSON.stringify(result));
});

contract.MemberChanged(function(error, result) {
    if (!error)
        console.log('MemberChanged\n' + JSON.stringify(result));
});

$(function() {
  $('#form-registration').submit(function(e) {
    e.preventDefault();
    var surname = $('#form-surname').val();
    var name = $('#form-name').val();
    var patronymic = $('#form-patronymic').val();
    var birthDate = $('#form-birth-date').val();
    var birthPlace = $('#form-birth-place').val();
    var avatarId = $('#form-avatar-id').val();
    var avatarHash = $('#form-avatar-hash').val();
    var data = $('#form-data').val();

    var wallet = Wallet.generate(false);
    var address = wallet.getChecksumAddressString();
    var privateKey = wallet.getPrivateKeyString();

    contract.addMember(address,
    name,
    surname,
    patronymic,
    birthDate,
    birthPlace,
    avatarHash,
    avatarId, 
	data, {from: web3.eth.accounts[0], gas: 4700000},function(err, result) {
        if (err)
          console.log('Error: ' + err);
        console.log('Transaction ID: ' + result);
      }); 
  });
  
   $('#form-registration-count').click(function(e) {
        e.preventDefault();
        var count = contract.getMemberCount();
        console.log('Members: ' + count);
        alert('Members count: ' + count);
   });
   
   $('#form-registration-get-user').click(function(e) {
        e.preventDefault();
        var id = $('#form-user-id').val();
        var member = contract.getMember(id);
        console.log('Member: ' + JSON.stringify(member));
        alert('Member: ' + JSON.stringify(member));
   });
});
