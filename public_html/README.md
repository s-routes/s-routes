# Gods

Для Studio нужно установить sandboxId в web/src/app.js.

Сборка:
```
$ npm install gulp-cli -g
$ npm install
$ gulp
```

Тестирование:
```
$ gulp test
```

Запуск:
```
$ cd web
$ npm install http-server -g
$ http-server
```
