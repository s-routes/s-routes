<p class="currency__header">
    <?= \Yii::t('c.zlOfQkItiW', 'Инструкция по покупке токенов') ?>
</p>


<p><?= \Yii::t('c.zlOfQkItiW', 'Чтобы купить токены AVR') ?> <code style="color: #c7254e; font-weight: bold;">0xcD389F4873e8fBCE7925b1D57804842043a3bF36</code>.</p>

<p><?= \Yii::t('c.zlOfQkItiW', 'Это адрес нашего контракта.') ?></p>

<p><?= \Yii::t('c.zlOfQkItiW', 'Цена токена = 0,1 Доллар США.') ?></p>

<p><?= \Yii::t('c.zlOfQkItiW', 'Цена действует на ограниченный этап продаж на который выделено') ?> 2,250,000 AVR.</p>

<p><?= \Yii::t('c.zlOfQkItiW', 'При переводе') ?></p>

<p><?= \Yii::t('c.zlOfQkItiW', 'Внимание!') ?></p>

<p><?= \Yii::t('c.zlOfQkItiW', 'Не переводите ETC на этот адрес, так как это контракт для ETH.') ?></p>
