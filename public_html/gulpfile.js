var browserify = require('browserify'),
  watchify = require('watchify'),
  concat = require('gulp-concat'),
  gulp = require('gulp'),
  mocha = require('gulp-mocha'),
  source = require('vinyl-source-stream');
  
var sourceFiles = ['./web/src/wallet.js', './web/src/app.js'],
  destFolder = './web/dist/',
  destFile = 'app.js';
 
gulp.task('browserify', function() {
  return browserify(sourceFiles).bundle()
    .pipe(source(destFile))
    .pipe(gulp.dest(destFolder));
});

gulp.task('watch', function() {
  var bundler = browserify({
    cache: {},
    packageCache: {},
    plugin: [watchify]
  }).add(sourceFiles);
  
  bundler.on('update', rebundle);
  rebundle();

  function rebundle() {
    return bundler.bundle()
      .pipe(source(destFile))
      .pipe(gulp.dest(destFolder));
  }
});

gulp.task('test', function() {
  return gulp
    .src('test/*.js')
    .pipe(mocha());
});

gulp.task('default', ['browserify']);