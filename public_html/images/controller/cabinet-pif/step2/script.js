var n = function(){
	var data = $(".news_one").attr("data-accordion");

	$(".btn_n").bind("click", function(){
		if(data == "close"){
			$(".news_text").slideUp();
			if($(this).hasClass("active")){
				$(this).toggleClass("active");
			}
			else{
				(".news_title").removeClass("active");
			}
		}
		else{
			$(this).toggleClass("active");
		}
		$(this).next(".news_text").not(":animated").slideToggle();
	})
}

n();




var speedCanvas = document.getElementById("speedChart");
Chart.defaults.global.defaultFontSize = 15;

var dataFirst = {
    label: "Команда А",
    data: [0, 70, 10, 75, 20, 65, 40],
    lineTension: 0.3,
    fill: true,
    borderColor: '#00effc',
    pointRadius: 0,
    backgroundColor: 'rgba(0, 239, 252, 0.3)',
    borderWidth: 0.5
  };

var dataSecond = {
    label: "Команда Б",
    data: [20, 15, 60, 60, 65, 30, 70],
    lineTension: 0.3,
    fill: true,
  	borderColor: '0, 252, 160',
  	pointRadius: 0,
  	pointRotation: 7,
  	backgroundColor: 'rgba(0, 252, 160, 0.3)',
  	borderWidth: 0.5
  };

var speedData = {
  labels: ["10 дней", "20 дней", "месяц", "0.5 года", "1 год", "2 года", "3 года"],
  datasets: [dataFirst, dataSecond]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 30,
      fontColor: '#1973fb',
      fontFamily: "'Exo 2', sans-serif"
    },
  },scales: {
		    yAxes: [{
		        ticks: {
		            display: true,
		            fontColor: '#1973fb',
		            fontSize: 12,
		            fontFamily: "'Exo 2', sans-serif"
		        },
		        gridLines: {
		            display: true,
		            drawBorder: true,
					color:"rgba(0, 239, 252, 0.2)",
		        	zeroLineColor: "rgba(0, 239, 252, 0)",
		        	zeroLineWidth: 1
		        },
		    }],
		    xAxes: [{
		        ticks: {
		            fontSize: 14,
		            fontColor: '#1973fb',
		            fontFamily: "'Exo 2', sans-serif",
		        },
		        gridLines: {
		            display: true,
		            drawBorder: true,
		            color:"rgba(0, 239, 252, 0.2)",
		        	zeroLineColor: "rgba(0, 239, 252, 0)",
		        	zeroLineWidth: 1
		        }
		    }],
		    onclick: function(){}
		}
};

var lineChart = new Chart(speedCanvas, {
  type: 'line',
  data: speedData,
  options: chartOptions
});








var oilCanvas = document.getElementById("oilChart");
Chart.defaults.global.defaultFontFamily = "'Exo 2', sans-serif";
Chart.defaults.global.defaultFontSize = 14;

var oilData = {
    labels: [
        "активы",
        "накопления",
        "страховой",
        "прибыль",
        "к выплате"
    ],
    datasets: [
        {
            data: [30, 10, 20, 15, 30],
            backgroundColor: [
                "#7cf77c",
                "#e9f77c",
                "#f7ce7c",
                "#42f5e6",
                "#dad1ff"
            ],
            borderColor: "white",
            borderWidth: 5,
            hoverBorderColor: "white"
        }],

};

var optionsPie = {
	animation: {
		duration: 500,
		animateScale: true,
		animateRotate: true
	},
	legend: {
	    display: true,
	    position: 'left',
	    labels: {
	      boxWidth: 15,
	      fontColor: '#1973fb',
	      fontFamily: "'Exo 2', sans-serif"
	    }
	}
}

var pieChart = new Chart(oilCanvas, {
  type: 'doughnut',
  data: oilData,
  options: optionsPie
});

// Ваш портфель управление


var youCase = {
	setActiveVal: function(val){
		$('.case__title-active-val').text(val);
	},
	setSummVal: function(val){
		$('.case__title-summ-val').text(val);
	},
	setProfitVal: function(val){
		$('.case__title-profitableness-val').text(val);
	},
	setPeriodVal: function(val){
		$('.case__title-period-val').text(val);
	}
};


function templateHoverHandler(plan){
	$('#' + plan.id).hover(
		function(){
			$(this).addClass(plan.hover);
			$("#" + plan.id + " .plans__item-hr").addClass(plan.hr_hover);
			$("#" + plan.id + " .plans__item-h1").addClass(plan.h1_hover);
		},
		function(){
			$(this).removeClass(plan.hover);
			$("#" + plan.id + " .plans__item-hr").removeClass(plan.hr_hover);
			$("#" + plan.id + " .plans__item-h1").removeClass(plan.h1_hover);
		}
	)
}
function templateClickHandler(plan, classes){
	$('#' + plan.id).click(
		function(){
			youCase.setProfitVal(plan.percent);
			youCase.setPeriodVal(plan.period);
			$(this).addClass(plan.click);
			$("#" + plan.id + " .plans__item-hr").addClass(plan.hr_click);
			$("#" + plan.id + " .plans__item-h1").addClass(plan.h1_click);
			$(".calculator_range_percent").val(plan.calculator_range_percent);
			$(".calculator_range_time_scale").val(plan.calculator_range_time_scale);
			console.log(plan.id);
			$.each(classes,
				//Удалить все классы кроме того что на выделенном текущем элементе
				function (key, value) {
					if(plan.id + '_click' !== value){ //Добавляем _click
						$('#' + value.slice(0, -6)).removeClass(value); //Отрезаем слово _click
						$('#' + value.slice(0, -6) + " .plans__item-hr").removeClass(plan.hr_click);
						console.log('#' + value.slice(0, -6));
						console.log(plan.hr_click);
						$('#' + value.slice(0, -6)  + " .plans__item-h1").removeClass(plan.h1_click);
						console.log(plan.h1_click);
					}
				}
			)
		}
	)
}

function setStatePlan(plan, classes) {
	$("#" + plan.id).addClass(plan.click);
	$("#" + plan.id + " .plans__item-hr").addClass(plan.hr_click);
	$("#" + plan.id + " .plans__item-h1").addClass(plan.h1_click);
	$.each(classes,
		//Удалить все классы кроме того что на выделенном текущем элементе
		function (key, value) {
			if(plan.id + '_click' !== value){ //Добавляем _click
				$('#' + value.slice(0, -6)).removeClass(value); //Отрезаем слово _click
				$('#' + value.slice(0, -6) + " .plans__item-hr").removeClass(plan.hr_click);
				console.log('#' + value.slice(0, -6));
				console.log(plan.hr_click);
				$('#' + value.slice(0, -6)  + " .plans__item-h1").removeClass(plan.h1_click);
				console.log(plan.h1_click);
			}
		}
	)
}
function resetStatePlan(plan, classes) {
	$.each(classes,
		//Удалить все классы кроме того что на выделенном текущем элементе
		function (key, value) {
				$('#' + value.slice(0, -6)).removeClass(value); //Отрезаем слово _click
				$('#' + value.slice(0, -6) + " .plans__item-hr").removeClass(plan.hr_click);
				console.log('#' + value.slice(0, -6));
				console.log(plan.hr_click);
				$('#' + value.slice(0, -6)  + " .plans__item-h1").removeClass(plan.h1_click);
				console.log(plan.h1_click);
		}
	)
}




var classes = ["plans__item_3year_click",
				"plans__item_2year_click",
				"plans__item_1year_click",
				"plans__item_6months_click",
				"plans__item_3months_click",
				"plans__item_1months_click",
				"plans__item_20days_click",
				"plans__item_10days_click",
			];

var plans = [
	{id:'plans__item_3year',
	period: '3 года',
	percent: '20%',
	hover: 'plans__item_3year_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_3year_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 8,
	calculator_range_time_scale: 8,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_2year',
	period: '2 года',
	percent: '18%',
	hover: 'plans__item_2year_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_2year_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 7,
	calculator_range_time_scale: 7,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_1year',
	period: '1 год',
	percent: '16%',
	hover: 'plans__item_1year_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_1year_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 6,
	calculator_range_time_scale: 6,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_6months',
	period: '6 месяцев',
	percent: '15%',
	hover: 'plans__item_6months_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_6months_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 5,
	calculator_range_time_scale: 5,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_3months',
	period: '3 месяца',
	percent: '13%',
	hover: 'plans__item_3months_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_3months_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 4,
	calculator_range_time_scale: 4,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_1months',
	period: '1 месяц',
	percent: '12%',
	hover: 'plans__item_1months_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_1months_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 3,
	calculator_range_time_scale: 3,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_20days',
	period: '20 дней',
	percent: '11%',
	hover: 'plans__item_20days_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_20days_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 2,
	calculator_range_time_scale: 2,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
	{id:'plans__item_10days',
	period: '10 дней',
	percent: '10%',
	hover: 'plans__item_10days_hover',
	hr_hover: 'plans__item-hr_hover',
	h1_hover: 'plans__item_h1_hover',
	click: 'plans__item_10days_click',
	hr_click: 'plans__item-hr_click',
	h1_click: 'plans__item_h1_click',
	calculator_range_percent: 1,
	calculator_range_time_scale: 1,
	addHoverHandler: function(){
		templateHoverHandler(this);
	},
	addClickHandler: function(classes){
		templateClickHandler(this, classes);
	},
	execute: function () {
		this.addHoverHandler();
		this.addClickHandler(classes);
	}
	},
];

function animatePlans(plans, classes){
	$.each(plans,
		//Удалить все классы кроме того что на выделенном текущем элементе
		function (key, value) {
			value.execute(classes);
		}
	)
}

animatePlans(plans, classes);



$('.calculator-btn-item').click(function() {
	$('.calculator-btn-item').removeClass('calculator-btn-item_clicked');
	$(this).addClass('calculator-btn-item_clicked')
	youCase.setActiveVal($(this).text());
});



$('.calculator_range_percent').change(
	function () {
		var value = Number($(this).val());
		$('.calculator_range_time_scale').val(value);
		var idPlan = value - 1;

		console.log('val ' + value);
		console.log('idPlan ' + idPlan);

		switch (value) {
			case 8:
				setStatePlan(plans[0], classes);
				youCase.setPeriodVal(plans[0].period);
				youCase.setProfitVal(plans[0].percent);
				break;
			case 7:
				setStatePlan(plans[1], classes);
				youCase.setPeriodVal(plans[1].period);
				youCase.setProfitVal(plans[1].percent);
				break;
			case 6:
				setStatePlan(plans[2], classes);
				youCase.setPeriodVal(plans[2].period);
				youCase.setProfitVal(plans[2].percent);
				break;
			case 5:
				setStatePlan(plans[3], classes);
				youCase.setPeriodVal(plans[3].period);
				youCase.setProfitVal(plans[3].percent);
				break;
			case 4:
				setStatePlan(plans[4], classes);
				youCase.setPeriodVal(plans[4].period);
				youCase.setProfitVal(plans[4].percent);
				break;
			case 3:
				setStatePlan(plans[5], classes);
				youCase.setPeriodVal(plans[5].period);
				youCase.setProfitVal(plans[5].percent);
				break;
			case 2:
				setStatePlan(plans[6], classes);
				youCase.setPeriodVal(plans[6].period);
				youCase.setProfitVal(plans[6].percent);
				break;
			case 1:
				setStatePlan(plans[7], classes);
				youCase.setPeriodVal(plans[7].period);
				youCase.setProfitVal(plans[7].percent);
				break;
			default:
				resetStatePlan(plans[0], classes);
		}
	}
);




$('.calculator_range_time_scale').change(
	function(){
		var value = Number($(this).val());
		$('.calculator_range_percent').val(value);

		switch (value) {
			case 8:
				setStatePlan(plans[0], classes);
				youCase.setPeriodVal(plans[0].period);
				youCase.setProfitVal(plans[0].percent);
				break;
			case 7:
				setStatePlan(plans[1], classes);
				youCase.setPeriodVal(plans[1].period);
				youCase.setProfitVal(plans[1].percent);
				break;
			case 6:
				setStatePlan(plans[2], classes);
				youCase.setPeriodVal(plans[2].period);
				youCase.setProfitVal(plans[2].percent);
				break;
			case 5:
				setStatePlan(plans[3], classes);
				youCase.setPeriodVal(plans[3].period);
				youCase.setProfitVal(plans[3].percent);
				break;
			case 4:
				setStatePlan(plans[4], classes);
				youCase.setPeriodVal(plans[4].period);
				youCase.setProfitVal(plans[4].percent);
				break;
			case 3:
				setStatePlan(plans[5], classes);
				youCase.setPeriodVal(plans[5].period);
				youCase.setProfitVal(plans[5].percent);
				break;
			case 2:
				setStatePlan(plans[6], classes);
				youCase.setPeriodVal(plans[6].period);
				youCase.setProfitVal(plans[6].percent);
				break;
			case 1:
				setStatePlan(plans[7], classes);
				youCase.setPeriodVal(plans[7].period);
				youCase.setProfitVal(plans[7].percent);
				break;
			default:
				resetStatePlan(plans[0], classes);
		}
	}
);

$('.calculator-range_summ').change(
	function(){
		var summ = $(this).val();
		youCase.setSummVal(summ);
	}
);
