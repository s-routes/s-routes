const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const mysql = require("mysql");
const fs = require('fs');

require('dotenv').config();

console.log(process.env);

// экземляр объекта socket
var s1;


app.get('/', function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/.well-known/pki-validation/54621F64006617A99563751B71BF3297.txt', function(req, res) {
    res.sendFile(__dirname + '/54621F64006617A99563751B71BF3297.txt');
});

app.post('/set-win', function(req, res) {

    var from = req.query.from || req.body.from;
    var to = req.query.from || req.body.to;
    var password = req.query.password || req.body.password;
    var amount = req.query.amount || req.body.amount;
    var tokenID = req.query.tokenID || req.body.tokenID;

    room, user_sender, data
    socket.to(room).broadcast.emit('chat1-message', data);

    res.sendFile(__dirname + '/54621F64006617A99563751B71BF3297.txt');
});

server.listen(3000, function() {
    console.log('listening on *:3000');
});


io.on('connection', function(socket) {

    s1 = socket;

    /**
     * Создаём нового юзера после его коннекта и засовываем в комнату.
     */
    socket.on('new-user', (room, user_id) => {
        console.log(['new-user', room, user_id]);

        socket.join(room);
        socket.to(room).broadcast.emit('user-connected', user_id);
    });

    /**
     * Подключает арбитра в комнату room_arbitr
     */
    socket.on('new-arbitr', (user_id) => {
        console.log(['new-arbitr', user_id]);
        socket.join('room_arbitr');
        socket.to('room_arbitr').broadcast.emit('new-arbitr', user_id);
    });

    /**
     * Подключает арбитра в комнату room_arbitr
     * @param int deal_id идентификатор сделки
     * @param int user_id идентификатор пользователя
     */
    socket.on('need-arbitr', (deal_id, user_id, partner_user_id) => {
        console.log(['need-arbitr', deal_id, user_id, partner_user_id]);
        socket.to(0).broadcast.emit('need-arbitr2', deal_id, user_id, partner_user_id);
        socket.to('room_arbitr').broadcast.emit('need-arbitr', deal_id, user_id, partner_user_id);
    });

    /**
     * Арбитр принимает сделку
     *
     * deal_id  - int   - идентификатор сделки
     * user_id  - int   - идентификатор пользователя который принял разбирать сделку
     * to       - array - массив кому отправить уведомление
     */
    socket.on('accept-arbitr', (deal_id, user_id, to) => {
        console.log(['accept-arbitr', deal_id, user_id]);
        socket.to(0).broadcast.emit('accept-arbitr', deal_id, user_id, to);
    });

    /**
     * Арбитр принимает решение в сделке в пользу инициатора сделки (deal.user_id)
     *
     * deal_id  - int   - идентификатор сделки
     * user_id  - int   - идентификатор пользователя который принял разбирать сделку
     * to       - array - массив кому отправить уведомление
     * to_user  - object - в чью пользу разрешена сделка {id,avatar,name}
     */
    socket.on('arbitr-result-deal', (deal_id, user_id, to, to_user) => {
        console.log(['arbitr-result-deal', deal_id, user_id, to, to_user]);
        socket.to(0).broadcast.emit('arbitr-result-deal', deal_id, user_id, to, to_user);
    });

    /**
     * Арбитр принимает решение в сделке в пользу инициатора предложения (offer.user_id)
     *
     * deal_id  - int   - идентификатор сделки
     * user_id  - int   - идентификатор пользователя который принял разбирать сделку
     * to       - array - массив кому отправить уведомление
     * to_user  - object - в чью пользу разрешена сделка {id,avatar,name}
     */
    socket.on('arbitr-result-offer', (deal_id, user_id, to, to_user) => {
        console.log(['arbitr-result-offer', deal_id, user_id, to, to_user]);
        socket.to(0).broadcast.emit('arbitr-result-offer', deal_id, user_id, to, to_user);
    });

    /**
     * Отправляем сообщение в массив
     * Пополняем массив (для теста)
     * Лог
     * Номер сообщения ++
     * отправляем в 'chat-message' сообщение и имя юзера
     * @param user_sender кто отправляет (объект)
     * @param user_to кому отправляем (массив ID)
     */
    socket.on('send-chat-message', (room, user_sender, user_to, message) => {
        console.log(['send-chat-message', room, user_sender, user_to, message]);
        var user_image = user_sender["image"];
        var user_name = user_sender["name"];

        socket.to(0).broadcast.emit('push', {
            to_user_id: user_to,
            name: user_name,
            message: message,
            deal: room,
        });

        socket.to(room).broadcast.emit('chat-message', {name: user_name, image: user_image, message: message});
    });

    /**
     * Отправляет сообщение в чат
     *
     * @param int       room        идентификатор комнаты
     * @param object    user_sender кто отправляет (объект)
     * @param object    message     Сообщение
     */
    socket.on('chat-message2', (room, user_sender, message) => {
        console.log(['chat-message2', user_sender, message]);
        socket.to(room).broadcast.emit('chat-message2', user_sender, message);
    });

    socket.on('disconnect', () => {

    });

    /**
     * @param int deal идентификатор сделки
     * @param int user_id идентификатор пользователя кто отправляет уведомление
     * @param int partner_user_id идентификатор пользователя кому предназначено уведомление
     */
    socket.on('deal-opened', (deal, user_id, partner_user_id) => {
        console.log(['deal-opened', deal, user_id, partner_user_id]);
        socket.to(0).emit('deal-opened', deal, user_id, partner_user_id);
    });

    /**
     * @param deal int
     * @param user_id int
     * @param status int
     */
    socket.on('buy-cancel', (deal, user_id, status) => {
        console.log(['buy-cancel', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
        socket.to(0).emit('buy-cancel', {deal: deal, user_id: user_id, status: status});
    });

    /**
     * @param deal int
     * @param user_id int пользователь который вызвал событие
     */
    socket.on('deal-cancel', (deal, user_id) => {
        console.log(['deal-cancel', deal, user_id]);
        socket.to(deal).emit('deal-cancel', {deal: deal, user_id: user_id});
    });

    socket.on('sell-open-accept', (deal, user_id, status) => {
        console.log(['sell-open-accept', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('buy-open-accept', (deal, user_id, status) => {
        console.log(['buy-open-accept', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('sell-money-sended', (deal, user_id, status) => {
        console.log(['sell-money-sended', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('buy-money-sended', (deal, user_id, status) => {
        console.log(['buy-money-sended', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('sell-money-received', (deal, user_id, status) => {
        console.log(['sell-money-received', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('buy-money-received', (deal, user_id, status) => {
        console.log(['buy-money-received', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('assessment-down', (deal, user_id, status) => {
        console.log(['assessment-down', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });

    socket.on('assessment-up', (deal, user_id, status) => {
        console.log(['assessment-up', deal, user_id, status]);
        socket.to(0).emit('reload-status', {deal: deal, user_id: user_id, status: status});
    });









    /**
     * Отправляет сообщение
     * @param user_sender кто отправляет (объект)
     * @param data
     * {
     *     message: {}
     *     user: {id,avatar,name2}
     * }
     */
    socket.on('chat1-message', (room, user_sender, data) => {
        console.log(['chat1-message', room, user_sender, data]);
        socket.to(room).broadcast.emit('chat1-message', data);
    });


    /**
     * Создаём нового юзера после его коннекта и засовываем в комнату.
     */
    socket.on('chat1-new-user', (room, user_id) => {
        console.log(['chat1-new-user', room, user_id]);

        socket.join(room);
        socket.to(room).broadcast.emit('chat1-user-connected', user_id);
    });

    /**
     * Сообщение на удаление сообщения
     */
    socket.on('chat1-delete', (room, message_id) => {
        console.log(['chat1-delete', room, message_id]);
        socket.to(room).broadcast.emit('chat1-delete', message_id);
    });

    /**
     * Забанить пользователя
     */
    socket.on('chat1-ban', (room, user_id) => {
        console.log(['chat1-ban', room, user_id]);
        socket.to(room).broadcast.emit('chat1-ban', user_id);
    });

    /**
     * Удалить пользователя
     */
    socket.on('chat1-delete-user', (room, user_id) => {
        console.log(['chat1-delete-user', room, user_id]);
        socket.to(room).broadcast.emit('chat1-delete-user', user_id);
    });


    /**
     * Уведомляет о технических работах
     */
    socket.on('technical-pause-alert', (start) => {
        console.log(['technical-pause-alert', start]);
        socket.to(0).emit('technical-pause-alert', start);
    });

    /**
     * Запускает счетчик
     */
    socket.on('chat1-counter-start2', function(room, lot_id, user) {
        console.log(['chat1-counter-start2', room, lot_id, user]);
        const connection = mysql.createConnection({
            host: "mysql",
            user: "root",
            database: "s_routes_prod_main",
            password: process.env.MYSQL_PASSWORD
        });

        // Записываю в БД
        var sql = 'UPDATE lot SET is_counter_start2=1 WHERE id=?';
        var data = [lot_id];
        connection.query(sql, data, function(err, results) {
            if(err)  {
                console.log(err);
            } else {
                sql = 'SELECT bilet.*, user.avatar, user.name_first, user.name_last FROM `bilet` INNER JOIN `user` on `bilet`.user_id = `user`.id where bilet.lot_id=?';
                data = [lot_id];
                connection.query(sql, data, function(err, results3) {
                    if(err)  {
                        console.log(err);
                    } else {
                        var results4 = JSON.parse(JSON.stringify(results3));

                        // выдаю вознаграждения
                        socket.to(room).emit(
                            'chat1-counter-start2',
                            {
                                room: room,
                                lot_id: lot_id,
                                rows: results4,
                                user: user
                            }
                        );

                        connection.end();
                    }
                });
            }
        });
    });

    /**
     * Останавливает счетчик
     */
    socket.on('chat1-counter-stop', function(room, lot_id, user) {
        console.log(['chat1-counter-stop', room, lot_id, user]);

        const connection = mysql.createConnection({
            host: "mysql",
            user: "root",
            database: "s_routes_prod_main",
            password: process.env.MYSQL_PASSWORD
        });

        //
        // const connectionWallet = mysql.createConnection({
        //     host: "mysql",
        //     user: "root",
        //     database: "s_routes_prod_wallet",
        //     password: process.env.MYSQL_PASSWORD
        // });

        // получаю время начала счетчика
        var sql = 'SELECT * FROM lot where id=?';
        var data = [lot_id];
        connection.query(sql, data, function(err, results) {
            if(err) {
                console.log(err);
            } else {
                results1=JSON.parse(JSON.stringify(results));

                if (results1[0].winner_id === null) {
                    var n = new Date().getTime();
                    var delta = n - results1[0].counter;
                    // сумма награды
                    var summa_win = results1[0].price * results1[0].bilet_count;

                    // Вычисляю список участников
                    sql = 'SELECT id,user_id FROM bilet where lot_id=?';
                    data = [lot_id];
                    connection.query(sql, data, function(err, results) {
                        if(err)  {
                            console.log(err);
                        } else {
                            results1=JSON.parse(JSON.stringify(results));

                            // Кол-во участников в розыгрыше
                            var uchKol = results1.length;

                            // порядковый номер победителя от 0 до uchKol-1
                            var winner_count = delta % uchKol;
                            var uchList = [];
                            for(var i = 0; i < results1.length; i++) {
                                uchList.push(results1[i]);
                            }

                            // Записываю в БД
                            sql = 'UPDATE lot SET winner_id=?,is_counter_stop=1 WHERE id=?';
                            data = [uchList[winner_count].id, lot_id];
                            connection.query(sql, data, function(err, results) {
                                if(err)  {
                                    console.log(err);
                                } else {
                                    sql = 'SELECT bilet.*, user.avatar, user.name_first, user.name_last FROM `bilet` INNER JOIN `user` on `bilet`.user_id = `user`.id where bilet.lot_id=?';
                                    data = [lot_id];
                                    connection.query(sql, data, function(err, results3) {
                                        if(err)  {
                                            console.log(err);
                                        } else {
                                            var results4 = JSON.parse(JSON.stringify(results3));

                                            // выдаю вознаграждения
                                            socket.to(room).emit(
                                                'chat1-counter-stop',
                                                {
                                                    room: room,
                                                    lot_id: lot_id,
                                                    winner_id: uchList[winner_count].id,
                                                    winner_is_bot: process.env['AUTO_REDEMPTION_USER_ID'] == uchList[winner_count].user_id ? 1 : 0,
                                                    winner_user_id: uchList[winner_count].user_id,
                                                    now: n,
                                                    rows: results4,
                                                    user: user
                                                }
                                            );

                                            connection.end();
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    });

    /**
     * Указывает победителя для онлайн трансляции
     */
    socket.on('chat1-counter-win', function(room, lot_id, ticket) {
        console.log(['chat1-counter-win', room, lot_id, ticket]);

        // выдаю вознаграждения
        socket.to(room).emit(
            'chat1-counter-win',
            {
                room: room,
                lot_id: lot_id,
                ticket: ticket
            }
        );
    });

});




