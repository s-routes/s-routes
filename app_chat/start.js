const express = require('express');
const app = express();
const io = require('/socket.io/socket.io.js');
const mysql = require("mysql");

require('dotenv').config();

var socket = io.connect('http://node-chat:3000');

var t = new Date().getTime();

const connection = mysql.createConnection({
    host: "mysql",
    user: "root",
    database: "s_routes_prod_main",
    password: process.env.MYSQL_PASSWORD
});

// const sql = 'SELECT * FROM lot WHERE time_start BETWEEN ? AND ?';
// const data = [t, t + (100 * 1000)];
const sql = 'SELECT * FROM lot WHERE id = ?';
const data = [3];
connection.query(sql, data, function(err, results) {
    if(err) console.log(err);

    results1=JSON.parse(JSON.stringify(results));
    console.log(results1);

    for (var i=0; i < results1.length; i++) {
        var item = results1[i];
        var room = 'room' + item.room_id;
        var lot_id = item.id;
        console.log([room, lot_id, t]);
        socket.emit('chat1-counter-start', [room, lot_id, t]);
    }
});



connection.end();
