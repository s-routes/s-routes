# Документация на IO

Метод `/set-win`
 
POST

Вызывается когда админ указывает победителя

Что делает
Отсылает событие `chat1-message` в комнату `room_id`

Параметры
- `room_id` - идентификатор комнаты в которую нужно отправить сообщение
- `user_sender` - идентификатор посылающего сообщение
- `data` - JSON сообщение выдаваемое после `/cabinet-support/send`

Сначала надо отправить сообщение в БД 

пример кода:
```php


```