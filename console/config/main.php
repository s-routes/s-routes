<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components'          => [
        'mailer' => [
            'viewPath' => '@avatar/mail',
        ],
        'urlManager' => [
            'baseUrl' => env('URL_MANAGER_HOST_INFO'),
        ],
    ],
    'params'              => $params,
];
