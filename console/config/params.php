<?php
return [
    'adminEmail' => 'admin@example.com',

    'mailer'                        => [
        // адрес который будет указан как адрес отправителя для писем и рассылок
        'from' => [
            env('MAILER_FROM_EMAIL') => env('MAILER_FROM_NAME'),
        ],
    ],
];
