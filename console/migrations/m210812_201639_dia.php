<?php

use yii\db\Migration;

/**
 * Class m210812_201639_dia
 */
class m210812_201639_dia extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findOne(17);
        \common\models\CurrencyIoModification::add([
            'currency_io_id'           => $cio->id,
            'master_wallet'            => 'DCSL3WY4aJj6tQ8qVBggCuDnp5wU4hoXZX',
            'master_wallet_is_binance' => 1,
            'name'                     => 'DOGE',
            'wallet_field_name'        => 'doge',
            'function_check'           => '\avatar\services\currency\Doge',
            'code'                     => 'DOGE',
            'warning'                  => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме DOGE - приведет к их потере.',
        ]);
        \common\models\CurrencyIoModification::add([
            'currency_io_id'           => $cio->id,
            'master_wallet'            => '0x3a736b425ed07f2c7f033a15c51d81e6c424c7e0',
            'master_wallet_is_binance' => 1,
            'name'                     => 'DOGE BEP20',
            'wallet_field_name'        => 'doge_bep20',
            'function_check'           => '\avatar\services\currency\Doge_bep20',
            'code'                     => 'BSC',
            'warning'                  => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме DOGE - приведет к их потере.',
        ]);
        $this->addColumn('user_wallet', 'doge_bep20', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210812_201639_dia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210812_201639_dia cannot be reverted.\n";

        return false;
    }
    */
}
