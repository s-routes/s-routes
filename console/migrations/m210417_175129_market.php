<?php

use yii\db\Migration;

/**
 * Class m210417_175129_market
 */
class m210417_175129_market extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('request_input_market', 'user_id', 'int default null');
        $this->addColumn('request_input_market', 'tx_int_id', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210417_175129_market cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210417_175129_market cannot be reverted.\n";

        return false;
    }
    */
}
