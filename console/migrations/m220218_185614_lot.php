<?php

use yii\db\Migration;

/**
 * Class m220218_185614_lot
 */
class m220218_185614_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'is_counter_start2', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220218_185614_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220218_185614_lot cannot be reverted.\n";

        return false;
    }
    */
}
