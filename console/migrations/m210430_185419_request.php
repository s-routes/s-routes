<?php

use yii\db\Migration;

/**
 * Class m210430_185419_request
 */
class m210430_185419_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'is_cancel', 'tinyint default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210430_185419_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210430_185419_request cannot be reverted.\n";

        return false;
    }
    */
}
