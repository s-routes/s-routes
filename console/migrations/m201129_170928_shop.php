<?php

use yii\db\Migration;

/**
 * Class m201129_170928_shop
 */
class m201129_170928_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('create table shop_request_product
(
    id int(11) NOT NULL AUTO_INCREMENT,

    link    varchar(200) null,
    request_id   int null,
    count   int null,
    price   double null,
    
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->createIndex('shop_request_product_request_id', 'shop_request_product', 'request_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201129_170928_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201129_170928_shop cannot be reverted.\n";

        return false;
    }
    */
}
