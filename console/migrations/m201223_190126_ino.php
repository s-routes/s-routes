<?php

use yii\db\Migration;

/**
 * Class m201223_190126_ino
 */
class m201223_190126_ino extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $curr = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LTC);
        $curr->name = 'LiteCoin';
        $curr->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_190126_ino cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_190126_ino cannot be reverted.\n";

        return false;
    }
    */
}
