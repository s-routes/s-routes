<?php

use yii\db\Migration;

/**
 * Class m210518_181357_random
 */
class m210518_181357_random extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments_ozon', 'key', 'varchar(7) default null');
        $this->addColumn('payments_sber', 'key', 'varchar(7) default null');
        $this->addColumn('payments_tinkoff', 'key', 'varchar(7) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210518_181357_random cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210518_181357_random cannot be reverted.\n";

        return false;
    }
    */
}
