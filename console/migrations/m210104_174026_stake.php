<?php

use yii\db\Migration;

/**
 * Class m210104_174026_stake
 */
class m210104_174026_stake extends Migration
{
    public function init()
    {
        $this->db = 'dbWallet';
        parent::init();

    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `neiron_transaction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `transaction_id` int DEFAULT NULL,
  `operation_id` int DEFAULT NULL,
  `type_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('neiron_transaction_operation_id', 'neiron_transaction', 'operation_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210104_174026_stake cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210104_174026_stake cannot be reverted.\n";

        return false;
    }
    */
}
