<?php

use yii\db\Migration;

/**
 * Class m210706_192653_purchase_packet
 */
class m210706_192653_purchase_packet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `purchase_packet` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    type_id       tinyint             null,
    amount        int             null,
    user_id       int             null,
    packet_id     int             null,
    tx_id         int             null,
    created_at    int             null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `packet` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    count         int             null,
    price         int             null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->dropTable('piramida_in_requests');
        $this->dropTable('piramida_in_requests_messages');
        $this->dropTable('company_customize');
        $this->dropTable('projects');
        $this->dropTable('projects_bonus');
        $this->dropTable('projects_gifts');
        $this->dropTable('projects_ico');
        $this->dropTable('requests_token_create');
        $this->dropTable('token');

        \common\models\Packet::add([
            'count' => 10000000,
            'price' => 300000,
        ]);
        \common\models\Packet::add([
            'count' => 30000000,
            'price' => 900000,
        ]);
        \common\models\Packet::add([
            'count' => 50000000,
            'price' => 1500000,
        ]);
        \common\models\Packet::add([
            'count' => 100000000,
            'price' => 3000000,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210706_192653_purchase_packet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210706_192653_purchase_packet cannot be reverted.\n";

        return false;
    }
    */
}
