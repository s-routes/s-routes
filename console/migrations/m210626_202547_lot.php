<?php

use yii\db\Migration;

/**
 * Class m210626_202547_lot
 */
class m210626_202547_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'winner_id', 'int default null');
        $this->addColumn('lot', 'bilet_sold', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210626_202547_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210626_202547_lot cannot be reverted.\n";

        return false;
    }
    */
}
