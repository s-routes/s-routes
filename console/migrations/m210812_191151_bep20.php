<?php

use yii\db\Migration;

/**
 * Class m210812_191151_bep20
 */
class m210812_191151_bep20 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_wallet', 'bnb_bep20', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210812_191151_bep20 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210812_191151_bep20 cannot be reverted.\n";

        return false;
    }
    */
}
