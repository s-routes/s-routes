<?php

use yii\db\Migration;

/**
 * Class m211026_175551_lot
 */
class m211026_175551_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'is_auto', 'tinyint null');

        $this->createIndex('lot_is_auto', 'lot', 'is_auto');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211026_175551_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211026_175551_lot cannot be reverted.\n";

        return false;
    }
    */
}
