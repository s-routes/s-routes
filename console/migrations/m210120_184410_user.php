<?php

use yii\db\Migration;

/**
 * Class m210120_184410_user
 */
class m210120_184410_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_referal_program', 'tinyint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210120_184410_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210120_184410_user cannot be reverted.\n";

        return false;
    }
    */
}
