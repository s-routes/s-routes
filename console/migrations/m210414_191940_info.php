<?php

use yii\db\Migration;

/**
 * Class m210414_191940_info
 */
class m210414_191940_info extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'info_page_shop_id', 'int default null');

        $this->createIndex('currency_io_currency_ext_id', 'currency_io', 'currency_ext_id');
        $this->createIndex('currency_io_currency_int_id', 'currency_io', 'currency_int_id');
        $this->createIndex('currency_io_is_io', 'currency_io', 'is_io');

        $CIO = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::MARKET);
        $CIO->info_page_shop_id = 83;
        $CIO->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210414_191940_info cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210414_191940_info cannot be reverted.\n";

        return false;
    }
    */
}
