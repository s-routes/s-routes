<?php

use yii\db\Migration;

/**
 * Class m210401_192031_input
 */
class m210401_192031_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'time_input_min', 'varchar(20)');
        $this->addColumn('currency_io', 'coin_name', 'varchar(20)');
        $this->addColumn('currency_io_modification', 'time_input_min', 'varchar(20)');
        $this->addColumn('currency_io_modification', 'coin_name', 'varchar(20)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210401_192031_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210401_192031_input cannot be reverted.\n";

        return false;
    }
    */
}
