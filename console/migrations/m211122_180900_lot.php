<?php

use yii\db\Migration;

/**
 * Class m211122_180900_lot
 */
class m211122_180900_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'link_telegram', 'varchar(255) null');

        $this->createIndex('deal_chat_room_id', 'deal', 'chat_room_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211122_180900_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211122_180900_lot cannot be reverted.\n";

        return false;
    }
    */
}
