<?php

use yii\db\Migration;

/**
 * Class m201205_193344_tele
 */
class m201205_193344_tele extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `user` 
CHANGE COLUMN `telegram_shop_username` `telegram_shop_username` VARCHAR(200) NULL DEFAULT NULL ;
');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201205_193344_tele cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201205_193344_tele cannot be reverted.\n";

        return false;
    }
    */
}
