<?php

use yii\db\Migration;

/**
 * Class m201206_190741_shop
 */
class m201206_190741_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `shop_temp_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `count` int DEFAULT NULL,
  `price` DOUBLE DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_temp_product_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201206_190741_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201206_190741_shop cannot be reverted.\n";

        return false;
    }
    */
}
