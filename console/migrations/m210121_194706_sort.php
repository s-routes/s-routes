<?php

use yii\db\Migration;

/**
 * Class m210121_194706_sort
 */
class m210121_194706_sort extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('offer_pay', 'sort_index', 'int null');
        \common\models\exchange\OfferPay::findOne(5)->delete();
        \common\models\exchange\OfferPay::findOne(7)->delete();
        $USTTRC20 = \common\models\exchange\OfferPay::add([
            'code'        => 'USDT',
            'title'       => 'USDT (TRC-20)',
            'currency_id' => \common\models\avatar\Currency::USDT,
        ]);
        $NEIRO = \common\models\exchange\OfferPay::add([
            'code'        => 'NEIRO',
            'title'       => 'NEIRO',
            'currency_id' => \common\models\avatar\Currency::NEIRO,
        ]);
        $ids = [
            1,//1-RUB
            2,//  2-USD
            $NEIRO->id,//   3-NEIRON
            6,//  4-MARKET-БАЛЛЫ
            //  5-BITCOIN
            //  6-ETHEREUM
            3, //  7-USDT_(ERC20)
            $USTTRC20->id,//  8-USDT_(TRC20)
            //  9-LITECOIN
            //  10-PRIZM
            //  11-TRON
            //  12-DASH
            //  13-eGOLD
            //  14-BNB
            9, //  15-UAH
            10, //  16-BYN
            11, //  17-EURO
            12, //  18-INR
            13, //  19-IDR
            14, //  20-CNY
        ];
        $sort_index = 0;
        foreach ($ids as $id) {
            $OfferPay =  \common\models\exchange\OfferPay::findOne($id);
            $OfferPay->sort_index = $sort_index;
            $sort_index++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210121_194706_sort cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210121_194706_sort cannot be reverted.\n";

        return false;
    }
    */
}
