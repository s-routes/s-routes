<?php

use yii\db\Migration;

/**
 * Class m210709_191748_coin
 */
class m210709_191748_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210709_191748_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210709_191748_coin cannot be reverted.\n";

        return false;
    }
    */
}
