<?php

use yii\db\Migration;

/**
 * Class m210810_190706_lot
 */
class m210810_190706_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_telegram_lot_temp` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    username          varchar(100) null,
    chat_id           int          null,
    status            int          null,
    last_message_time int          null,
    email             varchar(100) null,
    language          varchar(5)   null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `user_telegram_lot_connect` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    hash       varchar(32)  null,
    username   varchar(64)  null,
    chat_id    int          null,
    user_id    int          null,
    created_at int          null,
    email      varchar(100) null,
    language   varchar(5)   null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('user_telegram_lot_temp_chat_id', 'user_telegram_lot_temp', 'chat_id');
        $this->createIndex('user_telegram_lot_connect_chat_id', 'user_telegram_lot_connect', 'chat_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210810_190706_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210810_190706_lot cannot be reverted.\n";

        return false;
    }
    */
}
