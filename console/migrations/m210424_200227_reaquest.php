<?php

use yii\db\Migration;

/**
 * Class m210424_200227_reaquest
 */
class m210424_200227_reaquest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'type_id_delivery', 'int default null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210424_200227_reaquest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210424_200227_reaquest cannot be reverted.\n";

        return false;
    }
    */
}
