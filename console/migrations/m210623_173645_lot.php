<?php

use yii\db\Migration;

/**
 * Class m210623_173645_lot
 */
class m210623_173645_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'time_raffle', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210623_173645_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210623_173645_lot cannot be reverted.\n";

        return false;
    }
    */
}
