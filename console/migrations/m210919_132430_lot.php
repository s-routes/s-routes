<?php

use yii\db\Migration;

/**
 * Class m210919_132430_lot
 */
class m210919_132430_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'content', 'text null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210919_132430_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210919_132430_lot cannot be reverted.\n";

        return false;
    }
    */
}
