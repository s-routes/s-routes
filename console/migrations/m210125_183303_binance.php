<?php

use yii\db\Migration;

/**
 * Class m210125_183303_binance
 */
class m210125_183303_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('binance_order', 'convert_id', 'int null');
        $currency = [
            'BTC',
            'ETH',
            'LTC',
            'TRX',
            'DASH',
            'BNB',
        ];
        foreach ($currency as $c) {
            $c2 = \common\models\avatar\Currency::findOne(['code' => $c]);
            $c2->id_coingecko2 = null;
            $c2->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210125_183303_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210125_183303_binance cannot be reverted.\n";

        return false;
    }
    */
}
