<?php

use yii\db\Migration;

/**
 * Class m211127_185749_offer
 */
class m211127_185749_offer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('offer', 'is_send_time_out_notification', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211127_185749_offer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211127_185749_offer cannot be reverted.\n";

        return false;
    }
    */
}
