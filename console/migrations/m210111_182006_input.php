<?php

use yii\db\Migration;

/**
 * Class m210111_182006_input
 */
class m210111_182006_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('task_input', 'type_id');
        $c = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::USDT]);
        $c->tab_title = 'USDT';
        $c->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210111_182006_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210111_182006_input cannot be reverted.\n";

        return false;
    }
    */
}
