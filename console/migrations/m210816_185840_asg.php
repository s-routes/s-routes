<?php

use yii\db\Migration;

/**
 * Class m210816_185840_asg
 */
class m210816_185840_asg extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        $app = new \common\base\Application();
//        $app->addCoin([
//            'code'                      => 'ASG',
//            'title'                     => 'ASG',
//            'decimals'                  => 8,
//            'decimals_view'             => 8,
//            'master_wallet'             => '',
//            'master_wallet_is_binance'  => 0,
//            'function_check'            => '',
//            'validate_account'          => '',
//            'field_name_wallet'         => 'asg',
//            'warning'                   => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме ASG - приведет к их потере.',
//        ]);

        $this->execute('CREATE TABLE `currency_admin` (
    `id`          int          NOT NULL AUTO_INCREMENT,
    currency_id   int          null,
    user_id       int          null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('currency_admin_currency_id', 'currency_admin', 'currency_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210816_185840_asg cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210816_185840_asg cannot be reverted.\n";

        return false;
    }
    */
}
