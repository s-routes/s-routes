<?php

use yii\db\Migration;

/**
 * Class m210622_184815_lot
 */
class m210622_184815_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cInt = \common\models\piramida\Currency::add([
            'code' => 'LOT',
            'name' => 'LOTBERRY',
            'decimals' => 2,
            'decimals_view' => 2,
            'decimals_view_shop' => 2,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210622_184815_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210622_184815_lot cannot be reverted.\n";

        return false;
    }
    */
}
