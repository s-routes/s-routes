<?php

use yii\db\Migration;

/**
 * Class m211127_171017_lot
 */
class m211127_171017_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'time_type', 'tinyint default 0');
        $this->addColumn('lot', 'time_finish_add', 'int null');
        $this->addColumn('lot', 'time_raffle_add', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211127_171017_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211127_171017_lot cannot be reverted.\n";

        return false;
    }
    */
}
