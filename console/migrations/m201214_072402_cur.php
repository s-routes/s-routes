<?php

use yii\db\Migration;

/**
 * Class m201214_072402_cur
 */
class m201214_072402_cur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $o = \common\models\CurrencyIO::findOne(['currency_int_id' => 30]);
        $o->tab_title = 'TRON';
        $o->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201214_072402_cur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201214_072402_cur cannot be reverted.\n";

        return false;
    }
    */
}
