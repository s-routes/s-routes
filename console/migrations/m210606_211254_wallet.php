<?php

use yii\db\Migration;

/**
 * Class m210606_211254_wallet
 */
class m210606_211254_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /**
         * @var \common\models\UserAvatar $u
         */
        foreach (\common\models\UserAvatar::find()->all() as $u) {
            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::DAI, $u->id);
            echo 'uid='.$u->id.' bid=' . $data['billing']['id'] . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210606_211254_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210606_211254_wallet cannot be reverted.\n";

        return false;
    }
    */
}
