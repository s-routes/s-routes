<?php

use yii\db\Migration;

/**
 * Class m220129_143406_lot
 */
class m220129_143406_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $d = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::LOT);
        $d->is_exchange = 1;
        $d->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220129_143406_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220129_143406_lot cannot be reverted.\n";

        return false;
    }
    */
}
