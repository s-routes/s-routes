<?php

use yii\db\Migration;

/**
 * Class m210411_175459_neiro
 */
class m210411_175459_neiro extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::NEIRO);

        \common\models\CurrencyIoModification::add([
            'currency_io_id'    => $cio->id,
            'output_min'        => $cio->output_min,
            'benance_min'       => $cio->benance_min,
            'comission_out'     => $cio->comission_out,
            'benance_fee'       => $cio->benance_fee,
            'master_wallet'     => $cio->master_wallet,
            'name'              => 'ERC-20',
            'wallet_field_name' => $cio->field_name_wallet,
            'function_check'    => $cio->function_check,
            'code'              => 'ETH',
            'warning'           => '',
        ]);

        $this->addColumn('user_wallet', 'neiro_trc20', 'varchar(100) default null');

        \common\models\CurrencyIoModification::add([
            'currency_io_id'    => $cio->id,
            'output_min'        => null,
            'benance_min'       => null,
            'comission_out'     => null,
            'benance_fee'       => null,
            'master_wallet'     => 'TQBhWj95R8unqXxd47AFyLMkhQu8jDxM8G',
            'name'              => 'TRC-20',
            'wallet_field_name' => 'neiro_trc20',
            'function_check'    => '\avatar\services\currency\NeironTrc20',
            'code'              => 'TRX',
            'warning'           => '',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210411_175459_neiro cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210411_175459_neiro cannot be reverted.\n";

        return false;
    }
    */
}
