<?php

use yii\db\Migration;

/**
 * Class m201204_174229_tron
 */
class m201204_174229_tron extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = [
            'TRX' => [
                'title'              => 'Tron',
                'decimals'           => 6,
                'decimals_view'      => 6,
                'decimals_view_shop' => 6,
                'db_field'           => 'trx',
            ],
        ];


        foreach ($rows as $c => $data) {
            $cExt = \common\models\avatar\Currency::findOne(['code' => $c]);

            // Создаю у нас валюту
            $cInt = \common\models\piramida\Currency::add([
                'code'               => $c,
                'name'               => $data['title'],
                'decimals'           => $data['decimals'],
                'decimals_view'      => $data['decimals_view'],
                'decimals_view_shop' => $data['decimals_view_shop'],
            ]);

            // Создаю кошельки
            $walletMain = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
            $walletMain->in(bcmul(1000000, bcpow(10, $data['decimals'])),'Первая эмиссия');
            $walletBlock = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
            $walletLost = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);

            // Создаю инфо страницы
            $p = \common\models\exchange\Page::add(['name' => $cInt->code]);
            $pl = \common\models\PageLang::add(['name' => $cInt->code, 'language' => 'ru', 'parent_id' => $p->id]);
            $p2 = \common\models\PageLang::add(['name' => $cInt->code, 'language' => 'en', 'parent_id' => $p->id]);

            $cio = \common\models\CurrencyIO::add([
                'currency_int_id'   => $cInt->id,
                'currency_ext_id'   => $cExt->id,
                'is_exchange'       => 1,
                'is_io'             => 1,
                'field_name_wallet' => 'trx',
                'main_wallet'       => $walletMain->id,
                'block_wallet'      => $walletBlock->id,
                'lost_wallet'       => $walletLost->id,
                'info_page_id'      => $p->id,
                'function_check'    => '\avatar\services\currency\\' . strtoupper(substr($c,0, 1)) . strtolower(substr($c,1)),
            ]);

            // Создаю поле для сохранения кошелька
            $this->addColumn('user_wallet', $data['db_field'], 'varchar(100) null');


        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201204_174229_tron cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201204_174229_tron cannot be reverted.\n";

        return false;
    }
    */
}
