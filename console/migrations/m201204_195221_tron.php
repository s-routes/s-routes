<?php

use yii\db\Migration;

/**
 * Class m201204_195221_tron
 */
class m201204_195221_tron extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cExt = \common\models\avatar\Currency::findOne(['code' => 'TRX']);
        $cInt = \common\models\piramida\Currency::findOne(['code' => 'TRX']);

        // Создаю Link
        \common\models\avatar\CurrencyLink::add([
            'currency_int_id' => $cInt->id,
            'currency_ext_id' => $cExt->id,
        ]);

        // Создаю кошельки у пользователей
        /** @var \common\models\UserAvatar $u */
        foreach (\common\models\UserAvatar::find()->all() as $u) {
            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cInt->id, $u->id);
            echo 'create id=' . $data['billing']['id'] . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201204_195221_tron cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201204_195221_tron cannot be reverted.\n";

        return false;
    }
    */
}
