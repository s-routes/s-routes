<?php

use yii\db\Migration;

/**
 * Class m211114_201914_bilet
 */
class m211114_201914_bilet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bilet', 'num', 'varchar(10) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211114_201914_bilet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211114_201914_bilet cannot be reverted.\n";

        return false;
    }
    */
}
