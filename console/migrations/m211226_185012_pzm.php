<?php

use yii\db\Migration;

/**
 * Class m211226_185012_pzm
 */
class m211226_185012_pzm extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $w = \common\models\piramida\Wallet::findOne(6183);

        $cio = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::PZM);

        $t = $w->move2(
            $cio->main_wallet,
            \common\models\piramida\Currency::getAtomFromValue(20000, \common\models\piramida\Currency::PZM),
            '["Средства списаны со счета по заявке на вывод {request}",{"request":"<a href=\"/cabinet-active/out-index\" data-pjax=\"0\">#865</a>"}]',
            6
        );

        echo 'tid=' . $t['transaction']['id'] . "\n";
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211226_185012_pzm cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211226_185012_pzm cannot be reverted.\n";

        return false;
    }
    */
}
