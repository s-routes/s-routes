<?php

use yii\db\Migration;

/**
 * Class m210112_135503_usdt
 */
class m210112_135503_usdt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = \common\models\UserWallet::find()
            ->where(['not', ['usdt' => null]])
            ->andWhere(['not', ['usdt' => '']])
            ->all();
        /** @var \common\models\UserWallet $r */

        foreach ($rows as $r) {
            $uid = $r['user_id'];
            $r->usdt_erc20 = $r->usdt;
            $r->save();
            echo 'update user_id=' . $uid . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210112_135503_usdt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210112_135503_usdt cannot be reverted.\n";

        return false;
    }
    */
}
