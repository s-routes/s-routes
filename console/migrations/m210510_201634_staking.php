<?php

use common\models\NeironTransaction;
use yii\db\Migration;

/**
 * Class m210510_201634_staking
 */
class m210510_201634_staking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::NEIRO);
        $data = [
            ["Проценты со вклада #1",4919, 72.5],
            ["Проценты со вклада #2",5663, 72.5],
            ["Проценты со вклада #3",5708, 108.75],
            ["Проценты со вклада #6",5750, 14],
        ];
        $walletFrom = \common\models\piramida\Wallet::findOne($cio->main_wallet);
        foreach ($data as $row) {
            $d = \common\models\avatar\UserBill::getInternalCurrencyWallet($cio->currency_int_id, $row[1]);
            /** @var \common\models\piramida\Wallet $wTo */
            $wTo = $d['wallet'];
            $t = $walletFrom->move2(
                $wTo,
                \common\models\piramida\Currency::getAtomFromValue($row[2], $cio->currency_int_id),
                $row[0],
                NeironTransaction::TYPE_HOLD_PERCENT
            );

            NeironTransaction::add([
                'transaction_id' => $t['transaction']['id'],
                'operation_id'   => $t['operation_add']['id'],
                'type_id'        => NeironTransaction::TYPE_HOLD_PERCENT,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210510_201634_staking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210510_201634_staking cannot be reverted.\n";

        return false;
    }
    */
}
