<?php

use yii\db\Migration;

/**
 * Class m210713_175010_set
 */
class m210713_175010_set extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\Config::set('pzm_exchange_limit', \common\models\piramida\Currency::getAtomFromValue(100000, \common\models\piramida\Currency::PZM));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210713_175010_set cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210713_175010_set cannot be reverted.\n";

        return false;
    }
    */
}
