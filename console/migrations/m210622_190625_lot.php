<?php

use yii\db\Migration;

/**
 * Class m210622_190625_lot
 */
class m210622_190625_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cInt = \common\models\piramida\Currency::findOne([
            'code' => 'LOT',
        ]);
        $cExt = new \common\models\avatar\Currency([
            'code' => 'LOT',
            'title' => 'LOTBERRY',
            'decimals' => 2,
        ]);
        $cExt->save();
        $cExt->id = $cExt::getDb()->lastInsertID;

        $main = \common\models\piramida\Wallet::addNew(['currency_id' =>  $cInt->id]);
        $cio = \common\models\CurrencyIO::add([
            'currency_ext_id' => $cExt->id,
            'currency_int_id' => $cInt->id,
            'main_wallet'     => $main->id,
        ]);

        // Добавляю кошельки
        /**
         * @var \common\models\UserAvatar $u
         */
        foreach (\common\models\UserAvatar::find()->all() as $u) {
            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cInt->id, $u->id);
            echo 'uid='.$u->id.' bid=' . $data['billing']['id'] . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210622_190625_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210622_190625_lot cannot be reverted.\n";

        return false;
    }
    */
}
