<?php

use yii\db\Migration;

/**
 * Class m211122_182929_lot
 */
class m211122_182929_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'prize_amount', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211122_182929_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211122_182929_lot cannot be reverted.\n";

        return false;
    }
    */
}
