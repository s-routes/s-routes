<?php

use yii\db\Migration;

/**
 * Class m201225_190514_curr
 */
class m201225_190514_curr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $curr = [
            \common\models\piramida\Currency::USDT => [
                'old' => 4,
                'new' => 8,
            ],
        ];
        /** @var \common\models\exchange\Offer $o */
        foreach (\common\models\exchange\Offer::find()->all() as $o) {
            foreach ($curr as $c => $v) {
                $CIO = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $c]);
                if ($o->currency_id == $CIO->currency_ext_id) {
                    $price = $o->price;
                    if ($v['old'] < $v['new']) {
                        $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                    } else {
                        $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                    }
                    $o->price = $price;
                    $o->save();
                    echo 'order price id='.$o->id.' updated' . "\n";
                }
            }
        }

        /** @var \common\models\exchange\Deal $o */
        foreach (\common\models\exchange\Deal::find()->all() as $o) {
            foreach ($curr as $c => $v) {
                $CIO = \common\models\avatar\CurrencyLink::findOne(['currency_int_id' => $c]);
                if ($o->currency_id == $CIO->currency_ext_id) {
                    $price = $o->price;
                    if ($v['old'] < $v['new']) {
                        $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                    } else {
                        $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                    }
                    $o->price = $price;
                    $o->save();
                    echo 'deal price id='.$o->id.' updated' . "\n";
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201225_190514_curr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201225_190514_curr cannot be reverted.\n";

        return false;
    }
    */
}
