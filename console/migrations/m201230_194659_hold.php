<?php

use yii\db\Migration;

/**
 * Class m201230_194659_hold
 */
class m201230_194659_hold extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `contribution` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `time` int DEFAULT NULL,
  `percent` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `user_contribution` (
  `id` int NOT NULL AUTO_INCREMENT,
  `contribution_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `finish_at` int DEFAULT NULL,
  `wallet_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('user_contribution_contribution_id', 'user_contribution', 'contribution_id');
        $this->createIndex('user_contribution_user_id', 'user_contribution', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201230_194659_hold cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201230_194659_hold cannot be reverted.\n";

        return false;
    }
    */
}
