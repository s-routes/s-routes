<?php

use yii\db\Migration;

/**
 * Class m211117_182815_lot
 */
class m211117_182815_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'link_youtube', 'varchar(255) null');
        $this->addColumn('lot', 'link_instagram', 'varchar(255) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211117_182815_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211117_182815_lot cannot be reverted.\n";

        return false;
    }
    */
}
