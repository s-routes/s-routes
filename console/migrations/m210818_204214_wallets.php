<?php

use yii\db\Migration;

/**
 * Class m210818_204214_wallets
 */
class m210818_204214_wallets extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = \common\models\piramida\Operation::find()->where([
            'and',
            ['wallet_id' => 10610],
            ['>=', 'id', 38618],
        ])->all();
        $before = $rows[0]['before'];
        $after = 0;
        $c = 1;
        foreach ($rows  as $o) {
            if ($c == 1) {
                $amount = $o['amount'];
                $after = $before + ($o['type'] * $amount);
                $o->after = $after;
                $o->save();
                $before = $after;
            } else {
                $amount = $o['amount'];
                $after = $before + ($o['type'] * $amount);
                $o->after = $after;
                $o->before = $before;
                $o->save();
                $before = $after;
            }
            $c++;
            echo 'oid='.$o->id . "\n";
        }
        $w  = \common\models\piramida\Wallet::findOne(10610);
        $w->amount = $after;
        $w->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210818_204214_wallets cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210818_204214_wallets cannot be reverted.\n";

        return false;
    }
    */
}
