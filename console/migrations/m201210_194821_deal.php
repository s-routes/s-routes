<?php

use yii\db\Migration;

/**
 * Class m201210_194821_deal
 */
class m201210_194821_deal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('deal','partner_id', 'int null');

        \common\models\ReferalLevel::add([
            'school_id' => 1,
            'level'     => 1,
            'percent'   => 500,
        ]);
        \common\models\ReferalLevel::add([
            'school_id' => 1,
            'level'     => 2,
            'percent'   => 300,
        ]);
        \common\models\ReferalLevel::add([
            'school_id' => 1,
            'level'     => 3,
            'percent'   => 100,
        ]);
        \common\models\ReferalLevel::add([
            'school_id' => 1,
            'level'     => 4,
            'percent'   => 300,
        ]);
        \common\models\ReferalLevel::add([
            'school_id' => 1,
            'level'     => 5,
            'percent'   => 500,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201210_194821_deal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201210_194821_deal cannot be reverted.\n";

        return false;
    }
    */
}
