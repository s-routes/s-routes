<?php

use yii\db\Migration;

/**
 * Class m210425_191647_request_add
 */
class m210425_191647_request_add extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `shop_request_add` (
  `id` int NOT NULL AUTO_INCREMENT,
  `request_id` int DEFAULT NULL,
  `billing_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `currency_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('shop_request_add_request_id', 'shop_request_add', 'request_id');
        $this->createIndex('shop_request_add_billing_id', 'shop_request_add', 'billing_id');

        \common\models\BillingMainClass::add(['name' => '\common\models\ShopRequestAdd']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210425_191647_request_add cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210425_191647_request_add cannot be reverted.\n";

        return false;
    }
    */
}
