<?php

use yii\db\Migration;

/**
 * Class m211030_172047_lot
 */
class m211030_172047_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'is_send_win', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211030_172047_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211030_172047_lot cannot be reverted.\n";

        return false;
    }
    */
}
