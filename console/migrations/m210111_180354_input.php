<?php

use yii\db\Migration;

/**
 * Class m210111_180354_input
 */
class m210111_180354_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cExt = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::USDT);
        $cExt->title = 'USDT';
        $cExt->save();
        $cInt = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::USDT);
        $cInt->name = 'USDT';
        $cInt->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210111_180354_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210111_180354_input cannot be reverted.\n";

        return false;
    }
    */
}
