<?php

use yii\db\Migration;

/**
 * Class m210829_140051_lot
 */
class m210829_140051_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `lot_page` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    created_at int          null,
    name       varchar(255) null,
    content    longtext     null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `lot_page_lang` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    parent_id int          null,
    language  varchar(5)   null,
    name      varchar(255) null,
    content   longtext     null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('lot_page_lang_parent_id', 'lot_page_lang', 'parent_id');
        $this->createIndex('lot_page_lang_language', 'lot_page_lang', 'language');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210829_140051_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210829_140051_lot cannot be reverted.\n";

        return false;
    }
    */
}
