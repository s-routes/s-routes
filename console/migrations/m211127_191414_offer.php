<?php

use yii\db\Migration;

/**
 * Class m211127_191414_offer
 */
class m211127_191414_offer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('offer_is_send_time_out_notification', 'offer', 'is_send_time_out_notification');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211127_191414_offer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211127_191414_offer cannot be reverted.\n";

        return false;
    }
    */
}
