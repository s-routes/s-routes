<?php

use yii\db\Migration;

/**
 * Class m210130_181028_binance
 */
class m210130_181028_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('binance_order', 'binance_order_id', 'int null');
        $this->addColumn('binance_order', 'tx_from_id', 'int null');
        $this->addColumn('binance_order', 'tx_to_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210130_181028_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210130_181028_binance cannot be reverted.\n";

        return false;
    }
    */
}
