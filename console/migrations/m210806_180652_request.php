<?php

use yii\db\Migration;

/**
 * Class m210806_180652_request
 */
class m210806_180652_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'type', 'int default 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210806_180652_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210806_180652_request cannot be reverted.\n";

        return false;
    }
    */
}
