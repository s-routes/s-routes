<?php

use yii\db\Migration;

/**
 * Class m201216_181544_output
 */
class m201216_181544_output extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io','output_min', 'bigint null');
        $this->addColumn('currency_io','is_show_comission', 'tinyint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201216_181544_output cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201216_181544_output cannot be reverted.\n";

        return false;
    }
    */
}
