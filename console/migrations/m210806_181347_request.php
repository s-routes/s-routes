<?php

use yii\db\Migration;

/**
 * Class m210806_181347_request
 */
class m210806_181347_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'info', 'text null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210806_181347_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210806_181347_request cannot be reverted.\n";

        return false;
    }
    */
}
