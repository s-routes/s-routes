<?php

use yii\db\Migration;

/**
 * Class m201204_200856_tron
 */
class m201204_200856_tron extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c= \common\models\avatar\Currency::findOne(['code' => 'TRX']);
        $c->sort_index = 100;
        $c->save();


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201204_200856_tron cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201204_200856_tron cannot be reverted.\n";

        return false;
    }
    */
}
