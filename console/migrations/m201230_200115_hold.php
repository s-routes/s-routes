<?php

use yii\db\Migration;

/**
 * Class m201230_200115_hold
 */
class m201230_200115_hold extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 10 * 1000,
            'time'    => 1 * 60 * 60 * 24,
            'percent' => 1 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 2',
            'amount'  => 50 * 1000,
            'time'    => 3 * 60 * 60 * 24,
            'percent' => 2 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 100 * 1000,
            'time'    => 7 * 60 * 60 * 24,
            'percent' => 3 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 200 * 1000,
            'time'    => 10 * 60 * 60 * 24,
            'percent' => 5 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 500 * 1000,
            'time'    => 30 * 60 * 60 * 24,
            'percent' => 7 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 1000 * 1000,
            'time'    => 60 * 60 * 60 * 24,
            'percent' => 10 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 2000 * 1000,
            'time'    => 90 * 60 * 60 * 24,
            'percent' => 12 * 10000,
        ]);
        \common\models\Contribution::add([
            'name'    => 'Вклад 1',
            'amount'  => 5000 * 1000,
            'time'    => 180 * 60 * 60 * 24,
            'percent' => 15 * 10000,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201230_200115_hold cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201230_200115_hold cannot be reverted.\n";

        return false;
    }
    */
}
