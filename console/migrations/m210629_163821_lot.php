<?php

use yii\db\Migration;

/**
 * Class m210629_163821_lot
 */
class m210629_163821_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'lot_google_auth_code', 'varchar(16) default null');
        $this->addColumn('user', 'telegram_lot_username', 'varchar(200) default null');
        $this->addColumn('user', 'telegram_lot_chat_id', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210629_163821_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210629_163821_lot cannot be reverted.\n";

        return false;
    }
    */
}
