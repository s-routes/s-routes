<?php

use yii\db\Migration;

/**
 * Class m210610_174722_bnb
 */
class m210610_174722_bnb extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io_modification', 'extended_info', 'text default null');

        $CIO = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::BNB);

        \common\models\CurrencyIoModification::add([
            'currency_io_id'           => $CIO->id,
            'master_wallet'            => 'bnb136ns6lfw4zs5hg4n85vdthaad7hq5m4gtkgf23',
            'master_wallet_is_binance' => 1,
            'name'                     => 'BNB BEP2',
            'extended_info'            => \yii\helpers\Json::encode(['memo' => '100232884']),
            'wallet_field_name'        => 'bnb_bep2',
            'function_check'           => '\avatar\services\currency\Bnb_bep2',
            'code'                     => 'BNB',
            'warning'                  => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме BNB - приведет к их потере.',
        ]);
        $this->addColumn('user_wallet', 'bnb_bep2', 'varchar(100) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210610_174722_bnb cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210610_174722_bnb cannot be reverted.\n";

        return false;
    }
    */
}
