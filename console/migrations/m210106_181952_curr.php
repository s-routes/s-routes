<?php

use yii\db\Migration;

/**
 * Class m210106_181952_curr
 */
class m210106_181952_curr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
//        // Создание новой монеты
//        $rows = [
//            [
//                'code'           => 'USDTTRC20',
//                'name'           => 'USDT TRC-20',
//                'emission'       => pow(10, 7),
//                'decimals'       => 6,
//                'function_check' => '\avatar\services\currency\UsdtTrc20',
//            ],
//        ];
//        foreach ($rows as $c) {
//            // Создаю внутренний токен
//            $cInt = \common\models\piramida\Currency::add([
//                'code'               => $c['code'],
//                'name'               => $c['name'],
//                'decimals'           => $c['decimals'],
//                'decimals_view'      => $c['decimals'],
//                'decimals_view_shop' => $c['decimals'],
//            ]);
//            $cExt = \common\models\piramida\Currency::add([
//                'code'               => $c['code'],
//                'name'               => $c['name'],
//                'decimals'           => $c['decimals'],
//                'decimals_view'      => $c['decimals'],
//                'decimals_view_shop' => $c['decimals'],
//            ]);
//
//
//            // Созданию информационную страницу
//            $p = \common\models\exchange\Page::add(['name' => $c['code']]);
//            $pl = \common\models\PageLang::add(['name' => $c['code'], 'language' => 'ru', 'parent_id' => $p->id]);
//            $p2 = \common\models\PageLang::add(['name' => $c['code'], 'language' => 'en', 'parent_id' => $p->id]);
//
//            // Делаю эмиссию
//            $Wallet = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
//            $Wallet->in(bcmul($c['emission'], pow(10, $c['decimals'])), 'Первая эмиссия');
//
//            $CIO = \common\models\CurrencyIO::findOne([
//                'currency_int_id' => $cInt->id,
//                'currency_ext_id' => $cExt->id,
//                'tab_title'       => $c['name'],
//                'main_wallet'     => $Wallet->id,
//                'info_page_id'    => $p->id,
//            ]);
//
//            // Созданию счета у пользователей
//            /** @var \common\models\UserAvatar $user */
//            foreach (\common\models\UserAvatar::find()->all() as $user ){
//                $data = \common\models\avatar\UserBill::getInternalCurrencyWallet($cInt->id, $user->id);
//                /** @var \common\models\avatar\UserBill $billing */
//                $billing = $data['billing'];
//                echo 'bill id='.$billing->id.' updated' . "\n";
//            }
//        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210106_181952_curr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210106_181952_curr cannot be reverted.\n";

        return false;
    }
    */
}
