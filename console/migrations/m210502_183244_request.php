<?php

use yii\db\Migration;

/**
 * Class m210502_183244_request
 */
class m210502_183244_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'type_cancel', 'tinyint default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210502_183244_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210502_183244_request cannot be reverted.\n";

        return false;
    }
    */
}
