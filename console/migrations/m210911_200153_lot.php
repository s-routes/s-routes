<?php

use yii\db\Migration;

/**
 * Class m210911_200153_lot
 */
class m210911_200153_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'status', 'tinyint default 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210911_200153_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210911_200153_lot cannot be reverted.\n";

        return false;
    }
    */
}
