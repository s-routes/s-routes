<?php

use yii\db\Migration;

/**
 * Class m210712_205714_ref
 */
class m210712_205714_ref extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::LOT);
        $c->name = 'e-LOTTERY';
        $c->save();

        $c = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::LOT);
        $c->title = 'e-LOTTERY';
        $c->save();

        foreach (\common\models\UserAvatar::find()->all() as $u) {

            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, $u->id);
            /** @var \common\models\avatar\UserBill $bill */
            $bill = $data['billing'];
            $bill->name = 'e-LOTTERY';
            $bill->save();
            echo 'bill_id=' . $bill->id . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210712_205714_ref cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210712_205714_ref cannot be reverted.\n";

        return false;
    }
    */
}
