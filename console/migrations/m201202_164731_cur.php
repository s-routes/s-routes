<?php

use yii\db\Migration;

/**
 * Class m201202_164731_cur
 */
class m201202_164731_cur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::RUB]);
        $cio->info_page_id = 61;
        $cio->save();

        foreach (\common\models\UserAvatar::find()->all() as $u) {
            $rows = \common\models\avatar\UserBill::find()->where([
                'user_id' => $u->id,
                'currency' => \common\models\avatar\Currency::RUB,
            ])->all();

            if (count($rows) > 1) {
                try {
                    if ($rows[0]['is_default'] == 1 and $rows[1]['is_default'] == 1) {
                        // удаляю второй
                        $this->delete1($rows[1]);
                    } else if ($rows[0]['is_default'] == 0 and $rows[1]['is_default'] == 1) {
                        // удаляю первый
                        $this->delete1($rows[0]);
                    } else {
                        // удаляю второй
                        $this->delete1($rows[1]);
                    }

                } catch (Exception $e) {
                    echo ($e->getMessage()) . "\n";
                }
            }
        }
    }

    /**
     * @param \common\models\avatar\UserBill $bill
     */
    public function delete1($bill)
    {
        $wallet = \common\models\piramida\Wallet::findOne($bill->address);
        if ($wallet->amount > 0) {
            throw new Exception('на счете '.$bill->id.' положительный баланс');
        }
        $wallet->delete();
        echo 'счет id='. $bill['id'] . ' удален' . "\n";
        $bill->delete();
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201202_164731_cur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201202_164731_cur cannot be reverted.\n";

        return false;
    }
    */
}
