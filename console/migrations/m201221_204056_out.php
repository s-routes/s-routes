<?php

use yii\db\Migration;

/**
 * Class m201221_204056_out
 */
class m201221_204056_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $c = [
            \common\models\piramida\Currency::NEIRO => [
                'comission_out' => 15,
                'output_min'    => 30,
            ],
            \common\models\piramida\Currency::TRX   => [
                'comission_out' => 3.35,
                'output_min'    => 200,
            ],
            \common\models\piramida\Currency::USDT  => [
                'comission_out' => 3.35,
                'output_min'    => 7,
            ],
            \common\models\piramida\Currency::DASH  => [
                'comission_out' => 0.005,
                'output_min'    => 0.07,
            ],
            \common\models\piramida\Currency::ETH   => [
                'comission_out' => 0.006,
                'output_min'    => 0.011,
            ],
            \common\models\piramida\Currency::LTC   => [
                'comission_out' => 0.003,
                'output_min'    => 0.07,
            ],
            \common\models\piramida\Currency::BTC   => [
                'comission_out' => 0.0006,
                'output_min'    => 0.0015,
            ],
            \common\models\piramida\Currency::BNB   => [
                'comission_out' => 0.005,
                'output_min'    => 0.2,
            ],
            \common\models\piramida\Currency::EGOLD => [
                'comission_out' => 3,
                'output_min'    => 3,
            ],
        ];
        foreach ($c as $cid => $data) {
            $d = \common\models\CurrencyIO::findOne(['currency_int_id' => $cid]);
            $d->comission_out = $data['comission_out'];
            $d->output_min = $data['output_min'];
            $d->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201221_204056_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201221_204056_out cannot be reverted.\n";

        return false;
    }
    */
}
