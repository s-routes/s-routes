<?php

use yii\db\Migration;

/**
 * Class m220127_191913_lot
 */
class m220127_191913_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'is_prize_payd', 'tinyint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220127_191913_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220127_191913_lot cannot be reverted.\n";

        return false;
    }
    */
}
