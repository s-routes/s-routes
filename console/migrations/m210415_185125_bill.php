<?php

use yii\db\Migration;

/**
 * Class m210415_185125_bill
 */
class m210415_185125_bill extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\BillingMainClass::add(['name' => '\common\models\RequestInputMarket']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210415_185125_bill cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210415_185125_bill cannot be reverted.\n";

        return false;
    }
    */
}
