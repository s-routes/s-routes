<?php

use yii\db\Migration;

/**
 * Class m210131_105321_binance
 */
class m210131_105321_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('binance_order', 'binance_return', 'text null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210131_105321_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210131_105321_binance cannot be reverted.\n";

        return false;
    }
    */
}
