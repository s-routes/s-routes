<?php

use yii\db\Migration;

/**
 * Class m210512_184602_offer
 */
class m210512_184602_offer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('offer', 'price_is_free', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210512_184602_offer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210512_184602_offer cannot be reverted.\n";

        return false;
    }
    */
}
