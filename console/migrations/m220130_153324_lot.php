<?php

use yii\db\Migration;

/**
 * Class m220130_153324_lot
 */
class m220130_153324_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::LOT);
        $w = \common\models\piramida\Wallet::addNew(['currency_id' => \common\models\piramida\Currency::LOT]);
        $cio->block_wallet = $w->id;
        $cio->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220130_153324_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220130_153324_lot cannot be reverted.\n";

        return false;
    }
    */
}
