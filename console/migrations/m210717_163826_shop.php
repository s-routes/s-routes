<?php

use common\models\Config;
use common\models\piramida\Wallet;
use yii\db\Migration;

/**
 * Class m210717_163826_shop
 */
class m210717_163826_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
        foreach (\common\models\UserAvatar::find()->all() as $u) {

            $data = \common\models\UserBill2::findOne(['user_id' => $u->id, 'currency_id' => $c->id]);
            if (!is_null($data)) {
                $w = \common\models\piramida\Wallet::findOne($data->wallet_id);
                if ($w->amount == 10000000) {
                    $t = $w->move2($walletMain, 9000000, 'Возврат');
                    echo 't='. $t['transaction']->getAddress() . "\n";
                    continue;
                }
                if ($w->amount == 3000000) {
                    $t = $w->move2($walletMain, 2000000, 'Возврат');
                    echo 't='. $t['transaction']->getAddress() . "\n";
                    continue;
                }
                if ($w->amount == 0) {
                    continue;
                }
                if ($w->id == $walletMain->id) {
                    continue;
                }
                if ($w->amount > 0) {
                    if ($w->amount > 1000000) {
                        $t = $w->move2($walletMain, $w->amount - 1000000, 'Возврат');
                        echo 't='. $t['transaction']->getAddress() . "\n";
                    }
                    continue;
                }
            }

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210717_163826_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210717_163826_shop cannot be reverted.\n";

        return false;
    }
    */
}
