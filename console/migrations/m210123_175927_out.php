<?php

use yii\db\Migration;

/**
 * Class m210123_175927_out
 */
class m210123_175927_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::NEIRO]);
        $cio->comission_out = null;
        $cio->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210123_175927_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210123_175927_out cannot be reverted.\n";

        return false;
    }
    */
}
