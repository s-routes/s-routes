<?php

use yii\db\Migration;

/**
 * Class m210607_184114_dai
 */
class m210607_184114_dai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::DAI);
        $c->sort_index = 20;
        $c->save();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210607_184114_dai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210607_184114_dai cannot be reverted.\n";

        return false;
    }
    */
}
