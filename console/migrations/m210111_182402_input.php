<?php

use yii\db\Migration;

/**
 * Class m210111_182402_input
 */
class m210111_182402_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io_modification', 'function_check', 'varchar(100) null');
        $m1 = \common\models\CurrencyIoModification::findOne(1);
        $m1->function_check = '\avatar\services\currency\Usdt';
        $m1->save();
        $m1 = \common\models\CurrencyIoModification::findOne(2);
        $m1->function_check = '\avatar\services\currency\UsdtTrc20';
        $m1->comission_out = 3.35;
        $m1->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210111_182402_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210111_182402_input cannot be reverted.\n";

        return false;
    }
    */
}
