<?php

use yii\db\Migration;

/**
 * Class m210120_200551_staking
 */
class m210120_200551_staking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_contribution', 'status', 'int default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210120_200551_staking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210120_200551_staking cannot be reverted.\n";

        return false;
    }
    */
}
