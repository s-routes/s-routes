<?php

use yii\db\Migration;

/**
 * Class m210401_193849_input
 */
class m210401_193849_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'warning', 'varchar(2000)');
        $this->addColumn('currency_io_modification', 'warning', 'varchar(2000)');
        $this->dropColumn('currency_io', 'time_input_min');
        $this->dropColumn('currency_io', 'coin_name');
        $this->dropColumn('currency_io_modification', 'time_input_min');
        $this->dropColumn('currency_io_modification', 'coin_name');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210401_193849_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210401_193849_input cannot be reverted.\n";

        return false;
    }
    */
}
