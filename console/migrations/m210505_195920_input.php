<?php

use yii\db\Migration;

/**
 * Class m210505_195920_input
 */
class m210505_195920_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_input','type_id', 'tinyint default 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210505_195920_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210505_195920_input cannot be reverted.\n";

        return false;
    }
    */
}
