<?php

use yii\db\Migration;

/**
 * Class m201203_180817_user
 */
class m201203_180817_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'telegram_shop_username', 'int null');
        $this->addColumn('user', 'telegram_shop_chat_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201203_180817_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201203_180817_user cannot be reverted.\n";

        return false;
    }
    */
}
