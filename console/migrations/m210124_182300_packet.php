<?php

use yii\db\Migration;

/**
 * Class m210124_182300_packet
 */
class m210124_182300_packet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('contribution', ['amount' => 0]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210124_182300_packet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210124_182300_packet cannot be reverted.\n";

        return false;
    }
    */
}
