<?php

use yii\db\Migration;

/**
 * Class m210630_191424_referal
 */
class m210630_191424_referal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `shop_user_partner` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    user_id       int             null,
    parent_id     int             null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `shop_referal_transaction` (
    `id`           int             NOT NULL AUTO_INCREMENT,
    request_id     int             null,
    type_id        int             null,
    level          int             null,
    from_uid       int             null,
    to_uid         int             null,
    amount         int             null,
    transaction_id int             null,
    created_at     int             null,
    currency_id    int             null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210630_191424_referal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210630_191424_referal cannot be reverted.\n";

        return false;
    }
    */
}
