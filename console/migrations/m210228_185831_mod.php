<?php

use yii\db\Migration;

/**
 * Class m210228_185831_mod
 */
class m210228_185831_mod extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io_modification', 'code', 'varchar(10) null');
        $m = \common\models\CurrencyIoModification::findOne(1);
        $m->code = 'ETH';
        $m->save();
        $m = \common\models\CurrencyIoModification::findOne(2);
        $m->code = 'TRX';
        $m->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210228_185831_mod cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210228_185831_mod cannot be reverted.\n";

        return false;
    }
    */
}
