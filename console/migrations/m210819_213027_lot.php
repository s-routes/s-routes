<?php

use yii\db\Migration;

/**
 * Class m210819_213027_lot
 */
class m210819_213027_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'type', 'tinyint default 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210819_213027_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210819_213027_lot cannot be reverted.\n";

        return false;
    }
    */
}
