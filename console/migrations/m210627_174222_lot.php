<?php

use yii\db\Migration;

/**
 * Class m210627_174222_lot
 */
class m210627_174222_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'cash_back_percent', 'int default null');
        $this->addColumn('lot', 'cash_back_price', 'int default null');
        $this->addColumn('bilet', 'is_cash_back', 'tinyint default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210627_174222_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210627_174222_lot cannot be reverted.\n";

        return false;
    }
    */
}
