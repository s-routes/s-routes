<?php

use yii\db\Migration;

/**
 * Class m210217_082003_convert
 */
class m210217_082003_convert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('convert_operation', 'user_id', 'int null');
        $this->createIndex('convert_operation_user_id', 'convert_operation', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210217_082003_convert cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210217_082003_convert cannot be reverted.\n";

        return false;
    }
    */
}
