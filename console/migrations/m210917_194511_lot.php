<?php

use yii\db\Migration;

/**
 * Class m210917_194511_lot
 */
class m210917_194511_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'is_hide', 'tinyint default 0');

        $this->createIndex('lot_is_hide', 'lot', 'is_hide');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210917_194511_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210917_194511_lot cannot be reverted.\n";

        return false;
    }
    */
}
