<?php

use yii\db\Migration;

/**
 * Class m210120_190029_user
 */
class m210120_190029_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\UserAvatar::updateAll(['is_referal_program' => 0]);
        $this->execute('ALTER TABLE `user` 
CHANGE COLUMN `is_referal_program` `is_referal_program` TINYINT NULL DEFAULT 0 ;
');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210120_190029_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210120_190029_user cannot be reverted.\n";

        return false;
    }
    */
}
