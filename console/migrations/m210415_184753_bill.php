<?php

use yii\db\Migration;

/**
 * Class m210415_184753_bill
 */
class m210415_184753_bill extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('billing', 'success_url', 'varchar(255) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210415_184753_bill cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210415_184753_bill cannot be reverted.\n";

        return false;
    }
    */
}
