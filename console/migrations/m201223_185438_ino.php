<?php

use yii\db\Migration;

/**
 * Class m201223_185438_ino
 */
class m201223_185438_ino extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $curr = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::LTC]);
        $curr->tab_title = 'LiteCoin';
        $curr->save();

        $curr = \common\models\CurrencyIO::findOne(['currency_ext_id' => \common\models\avatar\Currency::LEGAT]);
        $curr->is_io = 0;
        $curr->save();
        $d = [
            \common\models\piramida\Currency::EGOLD,
            \common\models\piramida\Currency::ETH,
            \common\models\piramida\Currency::DASH,
            \common\models\piramida\Currency::LTC,
            \common\models\piramida\Currency::BTC,
            \common\models\piramida\Currency::BNB,
            \common\models\piramida\Currency::USDT,
            \common\models\piramida\Currency::TRX,
        ];
        foreach ($d as $d1) {
            $curr = \common\models\CurrencyIO::findOne(['currency_int_id' => $d1]);
            $curr->is_show_comission = 1;
            $curr->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201223_185438_ino cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201223_185438_ino cannot be reverted.\n";

        return false;
    }
    */
}
