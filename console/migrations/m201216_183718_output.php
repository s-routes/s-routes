<?php

use yii\db\Migration;

/**
 * Class m201216_183718_output
 */
class m201216_183718_output extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\CurrencyIO::findOne(15);
        $c->is_show_comission = 1;
        $c->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201216_183718_output cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201216_183718_output cannot be reverted.\n";

        return false;
    }
    */
}
