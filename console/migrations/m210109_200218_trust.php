<?php

use yii\db\Migration;

/**
 * Class m210109_200218_trust
 */
class m210109_200218_trust extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $new = Yii::$app->params['referal-program']['user_id'];
        \common\models\Config::set('trusted-users', \yii\helpers\Json::encode($new));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210109_200218_trust cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210109_200218_trust cannot be reverted.\n";

        return false;
    }
    */
}
