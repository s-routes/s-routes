<?php

use yii\db\Migration;

/**
 * Class m211001_200139_lot
 */
class m211001_200139_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'counter', 'bigint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211001_200139_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211001_200139_lot cannot be reverted.\n";

        return false;
    }
    */
}
