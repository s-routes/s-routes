<?php

use yii\db\Migration;

/**
 * Class m210103_181845_staking
 */
class m210103_181845_staking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('contribution', 'amount_max', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210103_181845_staking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210103_181845_staking cannot be reverted.\n";

        return false;
    }
    */
}
