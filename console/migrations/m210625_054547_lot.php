<?php

use yii\db\Migration;

/**
 * Class m210625_054547_lot
 */
class m210625_054547_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('bilet_lot_id', 'bilet', 'lot_id');
        $this->createIndex('bilet_user_id', 'bilet', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210625_054547_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210625_054547_lot cannot be reverted.\n";

        return false;
    }
    */
}
