<?php

use yii\db\Migration;

/**
 * Class m210103_184440_staking
 */
class m210103_184440_staking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user_contribution', 'amount', 'bigint null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210103_184440_staking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210103_184440_staking cannot be reverted.\n";

        return false;
    }
    */
}
