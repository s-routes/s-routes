<?php

use yii\db\Migration;

/**
 * Class m210103_185303_staking
 */
class m210103_185303_staking extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `s_routes_prod_main`.`contribution` 
ADD COLUMN `contributioncol` VARCHAR(45) NULL AFTER `amount_max`,
CHANGE COLUMN `amount` `amount` BIGINT NULL DEFAULT NULL ,
CHANGE COLUMN `amount_max` `amount_max` BIGINT NULL DEFAULT NULL ;');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210103_185303_staking cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210103_185303_staking cannot be reverted.\n";

        return false;
    }
    */
}
