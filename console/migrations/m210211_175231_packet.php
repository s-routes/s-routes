<?php

use yii\db\Migration;

/**
 * Class m210211_175231_packet
 */
class m210211_175231_packet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\Contribution::add([
            'time'       => 45 * 60 * 60 * 24,
            'percent'    => 2 * 1000,
            'amount'     => \common\models\piramida\Currency::getAtomFromValue(500, \common\models\piramida\Currency::NEIRO),
            'amount_max' => \common\models\piramida\Currency::getAtomFromValue(1000, \common\models\piramida\Currency::NEIRO),
            'name'       => 'Тестовый пакет',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210211_175231_packet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210211_175231_packet cannot be reverted.\n";

        return false;
    }
    */
}
