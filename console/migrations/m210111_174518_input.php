<?php

use yii\db\Migration;

/**
 * Class m210111_174518_input
 */
class m210111_174518_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_input', 'modification_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210111_174518_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210111_174518_input cannot be reverted.\n";

        return false;
    }
    */
}
