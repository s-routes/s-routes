<?php

use yii\db\Migration;

/**
 * Class m210225_174241_out
 */
class m210225_174241_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_output', 'binance_id', 'varchar(40)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210225_174241_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210225_174241_out cannot be reverted.\n";

        return false;
    }
    */
}
