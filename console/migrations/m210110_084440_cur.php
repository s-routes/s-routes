<?php

use yii\db\Migration;

/**
 * Class m210110_084440_cur
 */
class m210110_084440_cur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $curr = [
            \common\models\avatar\Currency::NEIRO,
            \common\models\avatar\Currency::BTC,
            \common\models\avatar\Currency::ETH,
            \common\models\avatar\Currency::USDT,
            \common\models\avatar\Currency::LTC,
            \common\models\avatar\Currency::PZM,
            \common\models\avatar\Currency::TRX,
            \common\models\avatar\Currency::DASH,
            \common\models\avatar\Currency::EGOLD,
            \common\models\avatar\Currency::BNB,
        ];
        $i = 0;
        /** @var int $c */
        foreach ($curr as $c) {
            $c2 = \common\models\avatar\Currency::findOne($c);
            $c2->sort_index = $i;
            $i ++;
            $c2->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210110_084440_cur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210110_084440_cur cannot be reverted.\n";

        return false;
    }
    */
}
