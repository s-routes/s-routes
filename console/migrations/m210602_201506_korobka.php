<?php

use yii\db\Migration;

/**
 * Class m210602_201506_korobka
 */
class m210602_201506_korobka extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gs_users_shop_requests', 'korobka_id', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210602_201506_korobka cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210602_201506_korobka cannot be reverted.\n";

        return false;
    }
    */
}
