<?php

use yii\db\Migration;

/**
 * Class m210425_203106_request
 */
class m210425_203106_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'currency_id_paid', 'int default null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210425_203106_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210425_203106_request cannot be reverted.\n";

        return false;
    }
    */
}
