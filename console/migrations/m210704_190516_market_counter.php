<?php

use yii\db\Migration;

/**
 * Class m210704_190516_market_counter
 */
class m210704_190516_market_counter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'market_counter', 'int default 10000000');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210704_190516_market_counter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210704_190516_market_counter cannot be reverted.\n";

        return false;
    }
    */
}
