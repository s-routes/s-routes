<?php

use common\models\exchange\ChatMessage;
use common\models\NeironSupport;
use yii\db\Migration;

/**
 * Class m211119_200249_neiro
 */
class m211119_200249_neiro extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        NeironSupport::deleteAll();
        $rows = \common\models\ChatList::find()->all();

        /** @var \common\models\ChatList $row */
        foreach ($rows as $row) {
            $chatRoom = \common\models\ChatRoom::add(['last_message' => $row['last_message']]);
            $support = NeironSupport::add([
                'user_id'      => $row['user_id'],
                'room_id'      => $chatRoom['id'],
                'last_message' => $row['last_message'],
            ]);
            $chat_id = 2000000000 + $row['user_id'];
            echo 'user_id=' . $row['user_id'] . ' started' . "\n";
            $messageList = ChatMessage::find()->where(['deal_id' => $chat_id])->all();
            $ids = [];
            /** @var ChatMessage $m */
            foreach ($messageList as $m) {
                \Yii::$app->db->createCommand()->insert('chat_message2',[
                    'room_id' => $chatRoom['id'],
                    'user_id' => $row['user_id'],
                    'message' => \yii\helpers\Json::encode([
                        'text'       => $m['message'],
                        'file'       => '',
                    ]),
                    'created_at' => $m['created_at'],
                ])->execute();
                $ids[] = $m['id'];
            }
            ChatMessage::deleteAll(['in', 'id', $ids]);
            echo 'user_id=' . $row['user_id'] . ' finished' . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211119_200249_neiro cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211119_200249_neiro cannot be reverted.\n";

        return false;
    }
    */
}
