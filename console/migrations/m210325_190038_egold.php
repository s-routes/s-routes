<?php

use yii\db\Migration;

/**
 * Class m210325_190038_egold
 */
class m210325_190038_egold extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::EGOLD);
        $cio->is_io = 0;
        $cio->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210325_190038_egold cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210325_190038_egold cannot be reverted.\n";

        return false;
    }
    */
}
