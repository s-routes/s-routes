<?php

use yii\db\Migration;

/**
 * Class m211018_193838_lot
 */
class m211018_193838_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('lot_time_finish', 'lot', 'time_finish');
        $this->createIndex('lot_time_raffle', 'lot', 'time_raffle');
        $this->createIndex('lot_is_counter_start', 'lot', 'is_counter_start');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211018_193838_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211018_193838_lot cannot be reverted.\n";

        return false;
    }
    */
}
