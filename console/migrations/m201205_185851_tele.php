<?php

use yii\db\Migration;

/**
 * Class m201205_185851_tele
 */
class m201205_185851_tele extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `user_telegram_shop_connect` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) DEFAULT NULL,
  `username` varchar(64) DEFAULT NULL,
  `chat_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_telegram_shop_connect_chat_id_index` (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `user_telegram_shop_temp` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `chat_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `last_message_time` int DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_telegram_shop_temp_chat_id` (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201205_185851_tele cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201205_185851_tele cannot be reverted.\n";

        return false;
    }
    */
}
