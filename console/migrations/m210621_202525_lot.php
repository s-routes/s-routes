<?php

use yii\db\Migration;

/**
 * Class m210621_202525_lot
 */
class m210621_202525_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `lot` (
    `id`            int             NOT NULL AUTO_INCREMENT,
    name            varchar(255)    null,
    image           varchar(255)    null,
    price           int             null,
    currency_id     int             null,
    bilet_count     int             null,
    time_start      int             null,
    time_finish     int             null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->execute('CREATE TABLE `bilet` (
    `id`            int             NOT NULL AUTO_INCREMENT,
    lot_id          int             null,
    user_id         int             null,
    created_at      int             null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210621_202525_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210621_202525_lot cannot be reverted.\n";

        return false;
    }
    */
}
