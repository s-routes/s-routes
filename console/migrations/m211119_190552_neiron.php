<?php

use yii\db\Migration;

/**
 * Class m211119_190552_neiron
 */
class m211119_190552_neiron extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `neiron_support` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    room_id       int     null,
	user_id       int     null,
	last_message  int     null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('neiron_support_room_id', 'neiron_support', 'room_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211119_190552_neiron cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211119_190552_neiron cannot be reverted.\n";

        return false;
    }
    */
}
