<?php

use yii\db\Migration;

/**
 * Class m210705_182321_user
 */
class m210705_182321_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'market_till', 'int default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210705_182321_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210705_182321_user cannot be reverted.\n";

        return false;
    }
    */
}
