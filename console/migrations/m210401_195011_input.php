<?php

use yii\db\Migration;

/**
 * Class m210401_195011_input
 */
class m210401_195011_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = [
            9 => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме BTC - приведет к их потере.',
            2 => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме ETH - приведет к их потере.',
            15 => 'Внимание! Сумма ввода должна быть больше 0.00002TRX. Среднее время зачисления 1 мин. Отправка любых монет/токенов кроме TRON - приведет к их потере.',
            10 => 'Внимание! Среднее время зачисления от 23 мин. Отправка любых монет/токенов кроме DASH - приведет к их потере.',
            11 => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме BNB ERC20 - приведет к их потере.',
            8 => 'Внимание! Сумма ввода должна быть больше 0.002 LTC. Cреднее время зачисления 7 мин. Отправка любых монет/токенов кроме LTC - приведет к их потере.',
        ];
        $mod = [
            1 => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме USDT ERC20 - приведет к их потере.',
            2 => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме USDT TRC20 - приведет к их потере.',
            3 => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме BNB ERC20 - приведет к их потере.',
        ];
        foreach ($cio as $cid => $text) {
            $c = \common\models\CurrencyIO::findOne($cid);
            $c->warning = $text;
            $c->save();
        }
        foreach ($mod as $cid => $text) {
            $c = \common\models\CurrencyIoModification::findOne($cid);
            $c->warning = $text;
            $c->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210401_195011_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210401_195011_input cannot be reverted.\n";

        return false;
    }
    */
}
