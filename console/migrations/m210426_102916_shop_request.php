<?php

use yii\db\Migration;

/**
 * Class m210426_102916_shop_request
 */
class m210426_102916_shop_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request_add', 'comment', 'varchar(200) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210426_102916_shop_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210426_102916_shop_request cannot be reverted.\n";

        return false;
    }
    */
}
