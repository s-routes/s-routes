<?php

use yii\db\Migration;

/**
 * Class m210516_195709_pay
 */
class m210516_195709_pay extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $p = new \common\models\PaySystem([
            'code'       => 'rub-ozon',
            'title'      => 'ozon',
            'class_name' => 'RubOzon',
            'currency'   => 'RUB',
        ]);
        $p->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210516_195709_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210516_195709_pay cannot be reverted.\n";

        return false;
    }
    */
}
