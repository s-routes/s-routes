<?php

use yii\db\Migration;

/**
 * Class m210528_180412_request
 */
class m210528_180412_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\BillingMainClass::add(['name' => '\common\models\shop\Request']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210528_180412_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210528_180412_request cannot be reverted.\n";

        return false;
    }
    */
}
