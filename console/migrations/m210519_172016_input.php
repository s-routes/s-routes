<?php

use yii\db\Migration;

/**
 * Class m210519_172016_input
 */
class m210519_172016_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('request_input_rub', 'is_paid_client', 'tinyint default 0');
        $this->addColumn('request_input_market', 'is_paid_client', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210519_172016_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210519_172016_input cannot be reverted.\n";

        return false;
    }
    */
}
