<?php

use yii\db\Migration;

/**
 * Class m210606_210242_wallet
 */
class m210606_210242_wallet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromInt(\common\models\piramida\Currency::DAI);
        $cio->currency_ext_id = \common\models\avatar\Currency::DAI;
        $cio->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210606_210242_wallet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210606_210242_wallet cannot be reverted.\n";

        return false;
    }
    */
}
