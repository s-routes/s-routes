<?php

use yii\db\Migration;

/**
 * Class m210429_202542_tinkoff
 */
class m210429_202542_tinkoff extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $p = new \common\models\PaySystem([
            'code'       => 'rub-tinkoff-api',
            'title'      => 'tinkoff-api',
            'class_name' => 'RubTinkoffApi',
            'currency'   => 'RUB',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210429_202542_tinkoff cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210429_202542_tinkoff cannot be reverted.\n";

        return false;
    }
    */
}
