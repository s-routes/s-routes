<?php

use yii\db\Migration;

/**
 * Class m210619_180754_instr
 */
class m210619_180754_instr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `instrukciya` (
    `id` int NOT NULL AUTO_INCREMENT,
    page_id    int            null,
    sort_index int            null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('instrukciya_page_id', 'instrukciya', 'page_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210619_180754_instr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210619_180754_instr cannot be reverted.\n";

        return false;
    }
    */
}
