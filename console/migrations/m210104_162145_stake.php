<?php

use yii\db\Migration;

/**
 * Class m210104_162145_stake
 */
class m210104_162145_stake extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = [
            ["month" => 1, "percent" => 2, "amount" => 0, "amount_max" => 1],
            ["month" => 3, "percent" => 12, "amount" => 1, "amount_max" => 5],
            ["month" => 5, "percent" => 25, "amount" => 5, "amount_max" => 10],
            ["month" => 6, "percent" => 42, "amount" => 10, "amount_max" => 25],
            ["month" => 8, "percent" => 64, "amount" => 25, "amount_max" => 50],
            ["month" => 10, "percent" => 90, "amount" => 50, "amount_max" => 100],
            ["month" => 12, "percent" => 120, "amount" => 100, "amount_max" => 250],
            ["month" => 15, "percent" => 180, "amount" => 250, "amount_max" => 500],
            ["month" => 18, "percent" => 252, "amount" => 500, "amount_max" => 750],
            ["month" => 21, "percent" => 336, "amount" => 750, "amount_max" => 1000],
            ["month" => 24, "percent" => 480, "amount" => 1000, "amount_max" => 5000],
        ];
        \common\models\Contribution::deleteAll();
        $с = 1;
        foreach ($data as $d) {
            \common\models\Contribution::add([
                'time'       => $d['month'] * 60 * 60 * 24 * 30,
                'percent'    => $d['percent'] * 1000,
                'amount'     => $d['amount'] * 1000 * 10000,
                'amount_max' => $d['amount_max'] * 1000 * 10000,
                'name'       => 'Вклад ' . $с,
            ]);
            $с++;

        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210104_162145_stake cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210104_162145_stake cannot be reverted.\n";

        return false;
    }
    */
}
