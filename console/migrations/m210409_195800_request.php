<?php

use yii\db\Migration;

/**
 * Class m210409_195800_request
 */
class m210409_195800_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'is_closed', 'int default 0');
        $this->addColumn('shop_request', 'manager_id', 'int default NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210409_195800_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210409_195800_request cannot be reverted.\n";

        return false;
    }
    */
}
