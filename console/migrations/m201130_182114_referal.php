<?php

use yii\db\Migration;

/**
 * Class m201130_182114_referal
 */
class m201130_182114_referal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `school_user_partner` (
  `id` int NOT NULL AUTO_INCREMENT,
  `school_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `school_user_partner_school_id_index` (`school_id`),
  KEY `school_user_partner_user_id_index` (`user_id`),
  KEY `school_user_partner_parent_id_index` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;');

        $this->execute('CREATE TABLE `school_referal_transaction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `school_id` int DEFAULT NULL,
  `request_id` int DEFAULT NULL,
  `level` int DEFAULT NULL,
  `from_uid` int DEFAULT NULL,
  `to_uid` int DEFAULT NULL,
  `to_wid` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `transaction_id` bigint DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `currency_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `school_referal_transaction_school_id_index` (`school_id`),
  KEY `school_referal_transaction_request_id_index` (`request_id`),
  KEY `school_referal_transaction_to_uid` (`to_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `school_referal_level` (
  `id` int NOT NULL AUTO_INCREMENT,
  `school_id` int DEFAULT NULL,
  `level` int DEFAULT NULL,
  `percent` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `school_referal_level_school_id_index` (`school_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201130_182114_referal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201130_182114_referal cannot be reverted.\n";

        return false;
    }
    */
}
