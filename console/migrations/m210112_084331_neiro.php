<?php

use yii\db\Migration;

/**
 * Class m210112_084331_neiro
 */
class m210112_084331_neiro extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = \common\models\piramida\Wallet::find()->where([
            'currency_id' => 25,
        ])->andWhere(['>', 'amount', 0])->all();
        /** @var \common\models\piramida\Wallet $w */
        foreach ($rows as $w) {
            $w->out($w->amount, 'Сжигание перед официальным стартом');
        }
        $w = \common\models\piramida\Wallet::findOne(10610);
        $w->in(280000 * 10000, 'Первая официальная эмиссия для кошелька 0xf65150ceb3e4757725a81edb803994c5cf67a299');

        // Удаляю все транзакции
        \common\models\NeironTransaction::deleteAll();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210112_084331_neiro cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210112_084331_neiro cannot be reverted.\n";

        return false;
    }
    */
}
