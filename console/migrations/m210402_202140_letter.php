<?php

use yii\db\Migration;

/**
 * Class m210402_202140_letter
 */
class m210402_202140_letter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromInt(\common\models\piramida\Currency::EGOLD);
        $cio->is_io= 1;
        $cio->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210402_202140_letter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210402_202140_letter cannot be reverted.\n";

        return false;
    }
    */
}
