<?php

use yii\db\Migration;

/**
 * Class m201225_172052_curr
 */
class m201225_172052_curr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $curr = [
            \common\models\piramida\Currency::USDT => [
                'old' => 4,
                'new' => 8,
            ],
        ];

        /** @var \common\models\piramida\Wallet $o */
        foreach (\common\models\piramida\Wallet::find()->all() as $o) {
            foreach ($curr as $c => $v) {
                $CIO = \common\models\CurrencyIO::findOne(['currency_int_id' => $c]);
                if ($o->currency_id == $CIO->currency_int_id) {
                    $price = $o->amount;
                    if ($price > 0) {
                        if ($v['old'] < $v['new']) {
                            $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                        } else {
                            $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                        }
                        $o->amount = $price;
                        $o->save();
                        echo 'Wallet id='.$o->id.' updated' . "\n";
                    }
                }
            }
        }

        /** @var \common\models\piramida\Transaction $o */
        foreach (\common\models\piramida\Transaction::find()->all() as $o) {
            foreach ($curr as $c => $v) {
                $CIO = \common\models\CurrencyIO::findOne(['currency_int_id' => $c]);
                $w = \common\models\piramida\Wallet::findOne($o->from);
                if ($w->currency_id == $CIO->currency_int_id) {
                    $price = $o->amount;
                    if ($v['old'] < $v['new']) {
                        $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                    } else {
                        $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                    }
                    $o->amount = $price;
                    $o->hash = $o->hash();
                    $o->save();
                    echo 'Transaction id='.$o->id.' updated' . "\n";
                }
            }
        }

        /** @var \common\models\piramida\Operation $o */
        foreach (\common\models\piramida\Operation::find()->all() as $o) {
            foreach ($curr as $c => $v) {
                $CIO = \common\models\CurrencyIO::findOne(['currency_int_id' => $c]);
                $w = \common\models\piramida\Wallet::findOne($o->wallet_id);
                if ($w->currency_id == $CIO->currency_int_id) {
                    $price = $o->amount;
                    if ($v['old'] < $v['new']) {
                        $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                    } else {
                        $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                    }
                    $o->amount = $price;

                    $price = $o->before;
                    if ($v['old'] < $v['new']) {
                        $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                    } else {
                        $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                    }
                    $o->before = $price;

                    $price = $o->after;
                    if ($v['old'] < $v['new']) {
                        $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
                    } else {
                        $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
                    }
                    $o->after = $price;

                    $o->hash = $o->hash();
                    $o->save();
                    echo 'Operation id='.$o->id.' updated' . "\n";
                }
            }
        }

        foreach ($curr as $c => $v) {
            $CO = \common\models\piramida\Currency::findOne($c);
            $CO->decimals = $v['new'];
            $CO->decimals_view = $v['new'];

            $price = $CO->amount;
            if ($v['old'] < $v['new']) {
                $price = bcmul($price, bcpow(10, $v['new'] - $v['old']));
            } else {
                $price = bcdiv($price, bcpow(10, $v['old'] - $v['new']));
            }
            $CO->amount = $price;

            $CO->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201225_172052_curr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201225_172052_curr cannot be reverted.\n";

        return false;
    }
    */
}
