<?php

use yii\db\Migration;

/**
 * Class m210509_200332_input
 */
class m210509_200332_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_input', 'amount_user', 'bigint default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210509_200332_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210509_200332_input cannot be reverted.\n";

        return false;
    }
    */
}
