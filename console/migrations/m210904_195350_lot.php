<?php

use yii\db\Migration;

/**
 * Class m210904_195350_lot
 */
class m210904_195350_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var \common\models\Lot $lot */
        foreach (\common\models\Lot::find()->all() as $lot) {
            $lot->bilet_sold = \common\models\Bilet::find()->where(['lot_id' => $lot->id])->count();
            $lot->save();
            echo 'lot id=' . $lot->id . ' count=' . $lot->bilet_sold . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210904_195350_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210904_195350_lot cannot be reverted.\n";

        return false;
    }
    */
}
