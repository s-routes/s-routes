<?php

use yii\db\Migration;

/**
 * Class m210131_112515_binance
 */
class m210131_112515_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('binance_order', 'currency_to', 'int null');
        \common\models\BinanceOrder::updateAll(['currency_to' => \common\models\avatar\Currency::NEIRO]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210131_112515_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210131_112515_binance cannot be reverted.\n";

        return false;
    }
    */
}
