<?php

use yii\db\Migration;

/**
 * Class m210416_182723_sber
 */
class m210416_182723_sber extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $p = new \common\models\PaySystem([
            'code'       => 'rub-sber-api',
            'title'      => 'SberAPI',
            'class_name' => 'RubSberApi',
            'currency'   => 'RUB',
        ]);
        $p->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210416_182723_sber cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210416_182723_sber cannot be reverted.\n";

        return false;
    }
    */
}
