<?php

use yii\db\Migration;

/**
 * Class m210501_190915_input
 */
class m210501_190915_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('request_input_market', 'is_paid', 'tinyint default null');
        $this->addColumn('request_input_rub', 'is_paid', 'tinyint default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210501_190915_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210501_190915_input cannot be reverted.\n";

        return false;
    }
    */
}
