<?php

use yii\db\Migration;

/**
 * Class m210914_062440_support
 */
class m210914_062440_support extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `lot_support` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    room_id       int     null,
	user_id       int     null,
	last_message  int     null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('lot_support_room_id', 'lot_support', 'room_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210914_062440_support cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210914_062440_support cannot be reverted.\n";

        return false;
    }
    */
}
