<?php

use yii\db\Migration;

/**
 * Class m210109_064418_packet
 */
class m210109_064418_packet extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $data = [
            ["month" => 30, "percent" => 2, "amount" => 0, "amount_max" => 1],
            ["month" => 90, "percent" => 2.5, "amount" => 1, "amount_max" => 5],
            ["month" => 150, "percent" => 3, "amount" => 5, "amount_max" => 10],
            ["month" => 210, "percent" => 3.5, "amount" => 10, "amount_max" => 25],
            ["month" => 270, "percent" => 4, "amount" => 25, "amount_max" => 50],
            ["month" => 315, "percent" => 4.5, "amount" => 50, "amount_max" => 100],
            ["month" => 365, "percent" => 5, "amount" => 100, "amount_max" => 250],
            ["month" => 450, "percent" => 6, "amount" => 250, "amount_max" => 500],
            ["month" => 540, "percent" => 7, "amount" => 500, "amount_max" => 750],
            ["month" => 630, "percent" => 8, "amount" => 750, "amount_max" => 1000],
            ["month" => 720, "percent" => 10, "amount" => 1000, "amount_max" => 5000],
        ];
        \common\models\Contribution::deleteAll();
        $с = 0;
        foreach ($data as $d) {
            \common\models\Contribution::add([
                'time'       => $d['month'] * 60 * 60 * 24,
                'percent'    => $d['percent'] * 1000,
                'amount'     => $d['amount'] * 1000 * 10000,
                'amount_max' => $d['amount_max'] * 1000 * 10000,
                'name'       => ($с == 0) ? 'Тестовый пакет' : 'Пакет ' . $с,
            ]);
            $с++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210109_064418_packet cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210109_064418_packet cannot be reverted.\n";

        return false;
    }
    */
}
