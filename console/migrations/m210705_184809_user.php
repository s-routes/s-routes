<?php

use yii\db\Migration;

/**
 * Class m210705_184809_user
 */
class m210705_184809_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'market_packet', 'int default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210705_184809_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210705_184809_user cannot be reverted.\n";

        return false;
    }
    */
}
