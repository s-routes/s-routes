<?php

use yii\db\Migration;

/**
 * Class m210228_191123_mod
 */
class m210228_191123_mod extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::BNB);
        $cio = \common\models\CurrencyIO::findOne(['currency_ext_id' => $c->id]);
        \common\models\CurrencyIoModification::add([
            'currency_io_id'    => $cio->id,
            'output_min'        => $cio->output_min,
            'comission_out'     => $cio->comission_out,
            'benance_fee'       => $cio->benance_fee,
            'master_wallet'     => $cio->master_wallet,
            'name'              => 'bnb_erc20',
            'wallet_field_name' => $cio->field_name_wallet,
            'function_check'    => $cio->function_check,
            'code'              => 'ETH',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210228_191123_mod cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210228_191123_mod cannot be reverted.\n";

        return false;
    }
    */
}
