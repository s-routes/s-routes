<?php

use yii\db\Migration;

/**
 * Class m201225_155436_input
 */
class m201225_155436_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_input','type_id', 'tinyint default 1');
        $this->createIndex('school_referal_transaction_from_uid', 'school_referal_transaction', 'from_uid');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201225_155436_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201225_155436_input cannot be reverted.\n";

        return false;
    }
    */
}
