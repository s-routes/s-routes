<?php

use yii\db\Migration;

/**
 * Class m210608_184450_cur
 */
class m210608_184450_cur extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'is_api', 'tinyint default 0');
        $this->addColumn('currency_io_modification', 'is_api', 'tinyint default 0');
        $c = [
            'DAI/ETH',
            'NEIRO/ETH',
            'NEIRO/TRX',
            'BTC/BTC',
            'ETH/ETH',
            'USDT/ETH',
            'USDT/TRX',
            'LTC/LTC',
            'PZM/PZM',
            'TRX/TRX',
            'DASH/DASH',
            'EGOLD/EGOLD',
            'BNB/ETH',
        ];
        foreach ($c as $c1) {
            $arr = explode('/', $c1);
            if ($arr[0] == $arr[1]) {
                $c2 = \common\models\avatar\Currency::findOne(['code' => $arr[0]]);
                $CIO = \common\models\CurrencyIO::findFromExt($c2->id);
                $CIO->is_api = 1;
                $CIO->save();
            } else {
                $c2 = \common\models\avatar\Currency::findOne(['code' => $arr[0]]);
                $CIO = \common\models\CurrencyIO::findFromExt($c2->id);
                $mod = \common\models\CurrencyIoModification::findOne(['currency_io_id' => $CIO->id, 'code' => $arr[1]]);
                $mod->is_api = 1;
                $mod->save();
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210608_184450_cur cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210608_184450_cur cannot be reverted.\n";

        return false;
    }
    */
}
