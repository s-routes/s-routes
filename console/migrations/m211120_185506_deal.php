<?php

use yii\db\Migration;

/**
 * Class m211120_185506_deal
 */
class m211120_185506_deal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('deal', 'chat_room_id', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211120_185506_deal cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211120_185506_deal cannot be reverted.\n";

        return false;
    }
    */
}
