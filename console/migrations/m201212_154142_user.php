<?php

use yii\db\Migration;

/**
 * Class m201212_154142_user
 */
class m201212_154142_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'shop_whatsapp', 'varchar(200) null');
        $this->addColumn('user', 'shop_telegram', 'varchar(200) null');

        $this->addColumn('shop_request', 'user_telegram', 'varchar(200) null');
        $this->addColumn('shop_request', 'user_whatsapp', 'varchar(200) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201212_154142_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201212_154142_user cannot be reverted.\n";

        return false;
    }
    */
}
