<?php

use yii\db\Migration;

/**
 * Class m211202_173452_lot
 */
class m211202_173452_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `lot_instrukciya` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    page_id       int     null,
	sort_index    int     null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('lot_instrukciya_page_id', 'lot_instrukciya', 'page_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211202_173452_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211202_173452_lot cannot be reverted.\n";

        return false;
    }
    */
}
