<?php

use yii\db\Migration;

/**
 * Class m210606_202948_dai
 */
class m210606_202948_dai extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cInt = \common\models\piramida\Currency::add([
            'name'               => 'DAI',
            'code'               => 'DAI',
            'decimals'           => '4',
            'decimals_view'      => '4',
            'decimals_view_shop' => '4',
        ]);
        $cExt = \common\models\piramida\Currency::findOne(['code' => 'DAI']);
        $wMain = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
        $wBlock = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
        $wLost = \common\models\piramida\Wallet::addNew(['currency_id' => $cInt->id]);
        $CIO = \common\models\CurrencyIO::add([
            'currency_ext_id'          => $cExt->id,
            'currency_int_id'          => $cInt->id,
            'main_wallet'              => $wMain->id,
            'block_wallet'             => $wBlock->id,
            'lost_wallet'              => $wLost->id,
            'is_exchange'              => 1,
            'is_io'                    => 1,
            'info_page_id'             => 88,
            'info_page_shop_id'        => 88,
            'master_wallet_is_binance' => 1,
            'function_check'           => '\avatar\services\currency\Dai',
            'validate_account'         => 'validateAccountEth',
            'field_name_wallet'        => 'dai',
        ]);
        $this->addColumn('user_wallet', 'dai', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210606_202948_dai cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210606_202948_dai cannot be reverted.\n";

        return false;
    }
    */
}
