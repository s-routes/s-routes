<?php

use yii\db\Migration;

/**
 * Class m210403_214024_letter
 */
class m210403_214024_letter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `send_letter_item` (
  `id` int NOT NULL AUTO_INCREMENT,
  `letter_id` int DEFAULT NULL,
  `text` text DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('send_letter_item_letter_id', 'send_letter_item', 'letter_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210403_214024_letter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210403_214024_letter cannot be reverted.\n";

        return false;
    }
    */
}
