<?php

use yii\db\Migration;

/**
 * Class m210410_194457_2fa
 */
class m210410_194457_2fa extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'shop_google_auth_code', 'varchar(16) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210410_194457_2fa cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210410_194457_2fa cannot be reverted.\n";

        return false;
    }
    */
}
