<?php

use yii\db\Migration;

/**
 * Class m210228_200101_mod
 */
class m210228_200101_mod extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE `currency_io_modification` 
CHANGE COLUMN `output_min` `output_min` BIGINT NULL DEFAULT NULL ;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210228_200101_mod cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210228_200101_mod cannot be reverted.\n";

        return false;
    }
    */
}
