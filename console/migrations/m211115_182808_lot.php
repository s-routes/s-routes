<?php

use yii\db\Migration;

/**
 * Class m211115_182808_lot
 */
class m211115_182808_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'prize_name', 'varchar(200) null');
        $this->addColumn('lot', 'prize_type', 'tinyint default 1');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211115_182808_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211115_182808_lot cannot be reverted.\n";

        return false;
    }
    */
}
