<?php

use yii\db\Migration;

/**
 * Class m210107_182810_io
 */
class m210107_182810_io extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `currency_io_modification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `master_wallet` varchar(100) DEFAULT NULL,
  `wallet_field_name` varchar(100) DEFAULT NULL,
  `currency_io_id` int DEFAULT NULL,
  `output_min` int DEFAULT NULL,
  `comission_out` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('currency_io_modification_currency_io_id', 'currency_io_modification', 'currency_io_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210107_182810_io cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210107_182810_io cannot be reverted.\n";

        return false;
    }
    */
}
