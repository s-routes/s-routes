<?php

use yii\db\Migration;

/**
 * Class m210812_193748_dia
 */
class m210812_193748_dia extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromExt(\common\models\avatar\Currency::DAI);
        \common\models\CurrencyIoModification::add([
            'currency_io_id'           => $cio->id,
            'master_wallet'            => '0x3a736b425ed07f2c7f033a15c51d81e6c424c7e0',
            'master_wallet_is_binance' => 1,
            'name'                     => 'DAI BEP20',
            'wallet_field_name'        => 'dai_bep20',
            'function_check'           => '\avatar\services\currency\Dai_bep20',
            'code'                     => 'BSC',
            'warning'                  => 'Внимание! Среднее время зачисления от 1 мин. Отправка любых монет/токенов кроме DAI - приведет к их потере.',
        ]);
        $this->addColumn('user_wallet', 'dai_bep20', 'varchar(100)');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210812_193748_dia cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210812_193748_dia cannot be reverted.\n";

        return false;
    }
    */
}
