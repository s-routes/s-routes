<?php

use yii\db\Migration;

/**
 * Class m210415_192123_bill
 */
class m210415_192123_bill extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `request_input_market` (
  `id` int NOT NULL AUTO_INCREMENT,
  `billing_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('request_input_market_billing_id', 'request_input_market', 'billing_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210415_192123_bill cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210415_192123_bill cannot be reverted.\n";

        return false;
    }
    */
}
