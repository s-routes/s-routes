<?php

use yii\db\Migration;

/**
 * Class m210601_184858_request
 */
class m210601_184858_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gs_users_shop_requests', 'txid_coin', 'int default null');
        $this->addColumn('gs_users_shop_requests', 'txid_korobka', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210601_184858_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210601_184858_request cannot be reverted.\n";

        return false;
    }
    */
}
