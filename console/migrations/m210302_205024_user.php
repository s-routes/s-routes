<?php

use yii\db\Migration;

/**
 * Class m210302_205024_user
 */
class m210302_205024_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('user', 'last_see_kolokol', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210302_205024_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210302_205024_user cannot be reverted.\n";

        return false;
    }
    */
}
