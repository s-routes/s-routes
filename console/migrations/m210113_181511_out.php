<?php

use yii\db\Migration;

/**
 * Class m210113_181511_out
 */
class m210113_181511_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_output', 'modification_id', 'int null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210113_181511_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210113_181511_out cannot be reverted.\n";

        return false;
    }
    */
}
