<?php

use yii\db\Migration;

/**
 * Class m210531_182952_korobka
 */
class m210531_182952_korobka extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `gs_unions_shop_product_korobka` (
    `id` int NOT NULL AUTO_INCREMENT,
    product_id   int   null,
    description  text  null,
    price        int   null,
    currency_id  int   null,
    image        varchar(200) null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('alter table gs_unions_shop_product_video modify url varchar(200) null;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210531_182952_korobka cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210531_182952_korobka cannot be reverted.\n";

        return false;
    }
    */
}
