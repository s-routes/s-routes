<?php

use yii\db\Migration;

/**
 * Class m210623_195026_lot
 */
class m210623_195026_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bilet', 'tx_id', 'int default null');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210623_195026_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210623_195026_lot cannot be reverted.\n";

        return false;
    }
    */
}
