<?php

use yii\db\Migration;

/**
 * Class m210424_195940_reaquest
 */
class m210424_195940_reaquest extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'type_id', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210424_195940_reaquest cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210424_195940_reaquest cannot be reverted.\n";

        return false;
    }
    */
}
