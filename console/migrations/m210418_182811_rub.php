<?php

use yii\db\Migration;

/**
 * Class m210418_182811_rub
 */
class m210418_182811_rub extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `request_input_rub` (
  `id` int NOT NULL AUTO_INCREMENT,
  `billing_id` int DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  `amount` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `tx_int_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('request_input_rub_billing_id', 'request_input_rub', 'billing_id');

        \common\models\BillingMainClass::add(['name' => '\common\models\RequestInputRub']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210418_182811_rub cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210418_182811_rub cannot be reverted.\n";

        return false;
    }
    */
}
