<?php

use yii\db\Migration;

/**
 * Class m210514_203226_input
 */
class m210514_203226_input extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'master_wallet_is_binance', 'tinyint default 0');
        $this->addColumn('currency_io_modification', 'master_wallet_is_binance', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210514_203226_input cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210514_203226_input cannot be reverted.\n";

        return false;
    }
    */
}
