<?php

use yii\db\Migration;

/**
 * Class m201215_200233_request
 */
class m201215_200233_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'delivery_dop', 'varchar(2000) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201215_200233_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201215_200233_request cannot be reverted.\n";

        return false;
    }
    */
}
