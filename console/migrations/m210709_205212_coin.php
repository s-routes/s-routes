<?php

use common\models\Config;
use common\models\piramida\Wallet;
use yii\db\Migration;

/**
 * Class m210709_205212_coin
 */
class m210709_205212_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));

        // Добавляю кошельки
        /**
         * @var \common\models\UserAvatar $u
         */
        foreach (\common\models\UserAvatar::find()->all() as $u) {
            $wallet = \common\models\piramida\Wallet::addNew(['currency_id' => $cMAR->id]);
            $data = \common\models\UserBill2::add([
                'user_id'     => $u->id,
                'wallet_id'   => $wallet->id,
                'currency_id' => $cMAR->id,
            ]);
            $t = $walletMain->move2($wallet, 10000000, 'Первый тестовый пакет');
            echo 'uid='.$u->id.' bid=' . $data->id . ' tid=' . $t['transaction']['id'] . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210709_205212_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210709_205212_coin cannot be reverted.\n";

        return false;
    }
    */
}
