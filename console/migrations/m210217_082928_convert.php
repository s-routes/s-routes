<?php

use yii\db\Migration;

/**
 * Class m210217_082928_convert
 */
class m210217_082928_convert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var \common\models\ConvertOperation $c */
        foreach (\common\models\ConvertOperation::find()->all() as $c) {
            try {
                $userBill = \common\models\avatar\UserBill::findOne(['address' => $c->wallet_from_id]);
                $c->user_id = $userBill->user_id;
                $c->save();
                echo 'update id='.$c->id .' userId='.$userBill->user_id. "\n";
            } catch (Exception $e) {
                echo 'error id='.$c->id. "\n";
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210217_082928_convert cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210217_082928_convert cannot be reverted.\n";

        return false;
    }
    */
}
