<?php

use yii\db\Migration;

/**
 * Class m210206_201915_convert
 */
class m210206_201915_convert extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('school_referal_transaction', 'type_id', 'tinyint default 1');
        $this->createIndex('school_referal_transaction_type_id', 'school_referal_transaction', 'type_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210206_201915_convert cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210206_201915_convert cannot be reverted.\n";

        return false;
    }
    */
}
