<?php

use yii\db\Migration;

/**
 * Class m210127_202211_binance
 */
class m210127_202211_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('binance_order', 'status', 'tinyint default 0');

        $this->createIndex('binance_order_status', 'binance_order', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210127_202211_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210127_202211_binance cannot be reverted.\n";

        return false;
    }
    */
}
