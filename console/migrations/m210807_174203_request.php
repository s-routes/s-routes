<?php

use yii\db\Migration;

/**
 * Class m210807_174203_request
 */
class m210807_174203_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_request', 'ostatok', 'int null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210807_174203_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210807_174203_request cannot be reverted.\n";

        return false;
    }
    */
}
