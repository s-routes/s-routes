<?php

use yii\db\Migration;

/**
 * Class m210131_114315_binance
 */
class m210131_114315_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('binance_order', 'amount_to', 'bigint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210131_114315_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210131_114315_binance cannot be reverted.\n";

        return false;
    }
    */
}
