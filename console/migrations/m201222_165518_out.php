<?php

use yii\db\Migration;

/**
 * Class m201222_165518_out
 */
class m201222_165518_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $c = [
            \common\models\piramida\Currency::NEIRO => [
                'comission_out' => 15,
                'output_min'    => 30,
            ],
            \common\models\piramida\Currency::TRX   => [
                'comission_out' => 3.35,
                'output_min'    => 200,
            ],
            \common\models\piramida\Currency::USDT  => [
                'comission_out' => 3.35,
                'output_min'    => 7,
            ],
            \common\models\piramida\Currency::DASH  => [
                'comission_out' => 0.005,
                'output_min'    => 0.07,
            ],
            \common\models\piramida\Currency::ETH   => [
                'comission_out' => 0.006,
                'output_min'    => 0.011,
            ],
            \common\models\piramida\Currency::LTC   => [
                'comission_out' => 0.003,
                'output_min'    => 0.07,
            ],
            \common\models\piramida\Currency::BTC   => [
                'comission_out' => 0.0006,
                'output_min'    => 0.0015,
            ],
            \common\models\piramida\Currency::BNB   => [
                'comission_out' => 0.005,
                'output_min'    => 0.2,
            ],
            \common\models\piramida\Currency::EGOLD => [
                'comission_out' => 3,
                'output_min'    => 3,
            ],
        ];
        foreach ($c as $cid => $data) {
            $d = \common\models\CurrencyIO::findOne(['currency_int_id' => $cid]);
            $cInt = \common\models\piramida\Currency::findOne($cid);
            $d->output_min = bcmul($data['output_min'], bcpow(10, $cInt->decimals));
            $d->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201222_165518_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201222_165518_out cannot be reverted.\n";

        return false;
    }
    */
}
