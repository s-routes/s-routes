<?php

use yii\db\Migration;

/**
 * Class m210928_200424_lot
 */
class m210928_200424_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('lot', 'room_id', 'int null');
        $this->addColumn('lot', 'is_counter_start', 'tinyint default 0');
        $this->addColumn('lot', 'is_counter_stop', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210928_200424_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210928_200424_lot cannot be reverted.\n";

        return false;
    }
    */
}
