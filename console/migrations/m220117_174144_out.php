<?php

use yii\db\Migration;

/**
 * Class m220117_174144_out
 */
class m220117_174144_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io_modification', 'decimals', 'tinyint null');
        $this->addColumn('currency_io', 'decimals', 'tinyint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m220117_174144_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220117_174144_out cannot be reverted.\n";

        return false;
    }
    */
}
