<?php

use yii\db\Migration;

/**
 * Class m210128_192850_usdt
 */
class m210128_192850_usdt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $rows = \common\models\CurrencyIoModification::find()->where(['currency_io_id' => 3])->all();
        foreach ($rows as $r) {
            $r->output_min = 700000000;
            $r->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210128_192850_usdt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210128_192850_usdt cannot be reverted.\n";

        return false;
    }
    */
}
