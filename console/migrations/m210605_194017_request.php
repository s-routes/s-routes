<?php

use yii\db\Migration;

/**
 * Class m210605_194017_request
 */
class m210605_194017_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gs_users_shop_requests', 'name', 'varchar(20) default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210605_194017_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210605_194017_request cannot be reverted.\n";

        return false;
    }
    */
}
