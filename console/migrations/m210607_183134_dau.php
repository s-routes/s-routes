<?php

use yii\db\Migration;

/**
 * Class m210607_183134_dau
 */
class m210607_183134_dau extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $cio = \common\models\CurrencyIO::findFromInt(\common\models\piramida\Currency::DAI);
        \common\models\CurrencyIoModification::add([
            'currency_io_id'           => $cio->id,
            'master_wallet'            => '0x3a736b425ed07f2c7f033a15c51d81e6c424c7e0',
            'master_wallet_is_binance' => 1,
            'name'                     => 'DAI ERC-20',
            'wallet_field_name'        => 'dai',
            'function_check'           => '\avatar\services\currency\Dai',
            'code'                     => 'ETH',
            'warning'                  => 'Внимание! Среднее время зачисления от 3 мин. Отправка любых монет/токенов кроме DAI ERC20 - приведет к их потере.',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210607_183134_dau cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210607_183134_dau cannot be reverted.\n";

        return false;
    }
    */
}
