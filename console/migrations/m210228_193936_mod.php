<?php

use yii\db\Migration;

/**
 * Class m210228_193936_mod
 */
class m210228_193936_mod extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'benance_min', 'bigint null');
        $this->addColumn('currency_io_modification', 'benance_min', 'bigint null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210228_193936_mod cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210228_193936_mod cannot be reverted.\n";

        return false;
    }
    */
}
