<?php

use yii\db\Migration;

/**
 * Class m210122_181444_coin
 */
class m210122_181444_coin extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $btc = \common\models\exchange\OfferPay::add([
            'code'        => 'BTC',
            'title'       => 'BITCOIN',
            'currency_id' => \common\models\avatar\Currency::BTC,
        ]);
        $LTC = \common\models\exchange\OfferPay::add([
            'code'        => 'LTC',
            'title'       => 'LITECOIN',
            'currency_id' => \common\models\avatar\Currency::LTC,
        ]);
        $ETH = \common\models\exchange\OfferPay::add([
            'code'        => 'ETH',
            'title'       => 'ETHEREUM',
            'currency_id' => \common\models\avatar\Currency::ETH,
        ]);
        $PZM = \common\models\exchange\OfferPay::add([
            'code'        => 'PZM',
            'title'       => 'PRIZM',
            'currency_id' => \common\models\avatar\Currency::PZM,
        ]);
        $TRX = \common\models\exchange\OfferPay::add([
            'code'        => 'TRX',
            'title'       => 'TRON',
            'currency_id' => \common\models\avatar\Currency::TRX,
        ]);
        $DASH = \common\models\exchange\OfferPay::add([
            'code'        => 'DASH',
            'title'       => 'DASH',
            'currency_id' => \common\models\avatar\Currency::DASH,
        ]);
        $EGOLD = \common\models\exchange\OfferPay::add([
            'code'        => 'EGOLD',
            'title'       => 'eGOLD',
            'currency_id' => \common\models\avatar\Currency::EGOLD,
        ]);
        $BNB = \common\models\exchange\OfferPay::add([
            'code'        => 'BNB',
            'title'       => 'BNB',
            'currency_id' => \common\models\avatar\Currency::BNB,
        ]);
        $ids = [
            1,//1-RUB
            2,//  2-USD
            16,//   3-NEIRON
            6,//  4-MARKET-БАЛЛЫ
            $btc->id, //  5-BITCOIN
            4,//  6-ETHEREUM
            3, //  7-USDT_(ERC20)
            15,//  8-USDT_(TRC20)
            $LTC->id, //  9-LITECOIN
            8, //  10-PRIZM
            $TRX->id, //  11-TRON
            $DASH->id, //  12-DASH
            $EGOLD->id, //  13-eGOLD
            $BNB->id, //  14-BNB
            9, //  15-UAH
            10, //  16-BYN
            11, //  17-EURO
            12, //  18-INR
            13, //  19-IDR
            14, //  20-CNY
        ];
        $sort_index = 0;
        foreach ($ids as $id) {
            $OfferPay = \common\models\exchange\OfferPay::findOne($id);
            $OfferPay->sort_index = $sort_index;
            $OfferPay->save();
            $sort_index++;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210122_181444_coin cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210122_181444_coin cannot be reverted.\n";

        return false;
    }
    */
}
