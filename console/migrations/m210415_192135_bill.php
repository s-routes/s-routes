<?php

use yii\db\Migration;

/**
 * Class m210415_192135_bill
 */
class m210415_192135_bill extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210415_192135_bill cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210415_192135_bill cannot be reverted.\n";

        return false;
    }
    */
}
