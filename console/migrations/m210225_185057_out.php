<?php

use yii\db\Migration;

/**
 * Class m210225_185057_out
 */
class m210225_185057_out extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('currency_io', 'benance_fee', 'double null');
        $this->addColumn('currency_io_modification', 'benance_fee', 'double null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210225_185057_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210225_185057_out cannot be reverted.\n";

        return false;
    }
    */
}
