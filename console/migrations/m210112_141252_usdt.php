<?php

use yii\db\Migration;

/**
 * Class m210112_141252_usdt
 */
class m210112_141252_usdt extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $c = \common\models\avatar\Currency::findOne(['code' => "USDT"]);

        foreach (\common\models\avatar\UserBill::find()->where(['currency' => $c->id])->all() as $bill) {
            $bill->name = 'USDT';
            $bill->save();
            echo 'update bill_id = '.$bill->id . "\n";
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210112_141252_usdt cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210112_141252_usdt cannot be reverted.\n";

        return false;
    }
    */
}
