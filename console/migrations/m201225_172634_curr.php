<?php

use yii\db\Migration;

/**
 * Class m201225_172634_curr
 */
class m201225_172634_curr extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201225_172634_curr cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201225_172634_curr cannot be reverted.\n";

        return false;
    }
    */
}
