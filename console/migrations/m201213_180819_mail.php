<?php

use yii\db\Migration;

/**
 * Class m201213_180819_mail
 */
class m201213_180819_mail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gs_subscribe', 'from_name', 'varchar(200) null');
        $this->addColumn('gs_subscribe', 'from_email', 'varchar(200) null');
        $this->addColumn('gs_subscribe', 'mailer', 'varchar(2000) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201213_180819_mail cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201213_180819_mail cannot be reverted.\n";

        return false;
    }
    */
}
