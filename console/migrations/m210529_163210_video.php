<?php

use yii\db\Migration;

/**
 * Class m210529_163210_video
 */
class m210529_163210_video extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `gs_unions_shop_product_video` (
  `id` int NOT NULL AUTO_INCREMENT,
    product_id  int          null,
    url         varchar(20)  null,
    image       varchar(200) null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');
        $this->createIndex('gs_unions_shop_product_video_product_id', 'gs_unions_shop_product_video', 'product_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210529_163210_video cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210529_163210_video cannot be reverted.\n";

        return false;
    }
    */
}
