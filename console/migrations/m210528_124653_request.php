<?php

use yii\db\Migration;

/**
 * Class m210528_124653_request
 */
class m210528_124653_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('gs_users_shop_requests', 'chat_id', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210528_124653_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210528_124653_request cannot be reverted.\n";

        return false;
    }
    */
}
