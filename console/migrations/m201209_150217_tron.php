<?php

use yii\db\Migration;

/**
 * Class m201209_150216_tron
 */
class m201209_150217_tron extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** @var \common\models\UserAvatar $user */
        foreach (\common\models\UserAvatar::find()->all() as $user ){
            $data = \common\models\avatar\UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::TRX, $user->id);
            /** @var \common\models\avatar\UserBill $billing */
            $billing = $data['billing'];
            $billing->name = 'Tron';
            $billing->save();
            echo 'bill id='.$billing->id.' updated' . "\n";
        }

        $c = \common\models\piramida\Currency::findOne(\common\models\piramida\Currency::TRX);
        $c->name = 'Tron';
        $c->save();

        $c = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::TRX);
        $c->title = 'Tron';
        $c->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201209_150216_tron cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201209_150216_tron cannot be reverted.\n";

        return false;
    }
    */
}
