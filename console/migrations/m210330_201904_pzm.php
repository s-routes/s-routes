<?php

use yii\db\Migration;

/**
 * Class m210330_201904_pzm
 */
class m210330_201904_pzm extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $w13452 = \common\models\piramida\Wallet::findOne(13452);
        $w12162 = \common\models\piramida\Wallet::findOne(12162);
        $w804 = \common\models\piramida\Wallet::findOne(804);
        $w801 = \common\models\piramida\Wallet::findOne(801);
        $w13452->move2($w804, 25750000, 'Списание средств по сделке от 09 марта в 18:28');
        $w804->move2($w12162, 25750000, 'Зачисление средств по сделке от 09 марта в 18:28');
        $w13452->move2($w801, 2056 , 'Списание за неактуальное начисление процентов');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210330_201904_pzm cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210330_201904_pzm cannot be reverted.\n";

        return false;
    }
    */
}
