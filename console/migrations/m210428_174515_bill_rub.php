<?php

use yii\db\Migration;

/**
 * Class m210428_174515_bill_rub
 */
class m210428_174515_bill_rub extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $users = [4376,
                  4384,
                  4386,
                  4390,
                  4888,
                  5360];
        foreach ($users as $uid) {
            $rows = \common\models\avatar\UserBill::find()
                ->where(['user_id' => $uid])
                ->all()
            ;

            if (count($rows) == 2) {
                /** @var \common\models\avatar\UserBill $b1 */
                $b1 = $rows[0];
                /** @var \common\models\avatar\UserBill $b2 */
                $b2 = $rows[1];
                $w1 = \common\models\piramida\Wallet::findOne($rows[0]['address']);
                $w2 = \common\models\piramida\Wallet::findOne($rows[1]['address']);
                if ($w1->amount > 0 and $w2->amount == 0) {
                    echo '$b2->id = '. $b2->id . ' deleted' . "\n";
                    $b2->delete();
                    if ($b1->is_default == 0) {
                        $b1->is_default = 1;
                        $b1->save();
                    }
                }
                if ($w2->amount > 0 and $w1->amount == 0) {
                    echo '$b1->id = '. $b2->id . ' deleted' . "\n";
                    $b1->delete();
                    if ($b2->is_default == 0) {
                        $b2->is_default = 1;
                        $b2->save();
                    }
                }
                if ($w2->amount == 0 and $w1->amount == 0) {
                    echo '$b1->id = '. $b2->id . ' deleted' . "\n";
                    $b1->delete();
                    if ($b2->is_default == 0) {
                        $b2->is_default = 1;
                        $b2->save();
                    }
                }
            }

         }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210428_174515_bill_rub cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210428_174515_bill_rub cannot be reverted.\n";

        return false;
    }
    */
}
