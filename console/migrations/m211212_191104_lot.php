<?php

use yii\db\Migration;

/**
 * Class m211212_191104_lot
 */
class m211212_191104_lot extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('bilet', 'cash_bask_result', 'tinyint default 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m211212_191104_lot cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m211212_191104_lot cannot be reverted.\n";

        return false;
    }
    */
}
