<?php

use yii\db\Migration;

/**
 * Class m210124_205759_binance
 */
class m210124_205759_binance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('CREATE TABLE `binance_order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `binance_id` varchar(100) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `billing_id` int DEFAULT NULL,
  `amount` bigint DEFAULT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('binance_order_binance_id', 'binance_order', 'binance_id');
        $this->createIndex('binance_order_user_id', 'binance_order', 'user_id');
        $this->createIndex('binance_order_created_at', 'binance_order', 'created_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210124_205759_binance cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210124_205759_binance cannot be reverted.\n";

        return false;
    }
    */
}
