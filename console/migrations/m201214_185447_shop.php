<?php

use yii\db\Migration;

/**
 * Class m201214_185447_shop
 */
class m201214_185447_shop extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('shop_all', 'status', 'tinyint default 1');
        \common\models\ShopAll::updateAll(['status' => \common\models\ShopAll::STATUS_ACCEPT]);
        $this->createIndex('shop_all_status', 'shop_all', 'status');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201214_185447_shop cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201214_185447_shop cannot be reverted.\n";

        return false;
    }
    */
}
