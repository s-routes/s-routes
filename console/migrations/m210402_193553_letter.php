<?php

use yii\db\Migration;

/**
 * Class m210402_193553_letter
 */
class m210402_193553_letter extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('send_letter', 'answer_text', 'text default null');
        $this->addColumn('send_letter', 'answer_at', 'int default null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210402_193553_letter cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210402_193553_letter cannot be reverted.\n";

        return false;
    }
    */
}
