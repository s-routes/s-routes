<?php

use common\models\Config;
use common\models\piramida\Wallet;
use yii\db\Migration;

/**
 * Class m210711_125234_ss
 */
class m210711_125234_ss extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $s = [5974,5975,5976];
        foreach ($s as $u) {
            // выдаю первый тестовый пакет
            $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
            $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
            $wallet = \common\models\piramida\Wallet::addNew(['currency_id' => $cMAR->id]);
            $data = \common\models\UserBill2::add([
                'user_id'     => $u,
                'wallet_id'   => $wallet->id,
                'currency_id' => $cMAR->id,
            ]);
            $t = $walletMain->move2($wallet, 10000000, 'Первый тестовый пакет');
            echo 't=' . $t['transaction']['id'] . '\n';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210711_125234_ss cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210711_125234_ss cannot be reverted.\n";

        return false;
    }
    */
}
