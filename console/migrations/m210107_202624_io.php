<?php

use yii\db\Migration;

/**
 * Class m210107_202624_io
 */
class m210107_202624_io extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        \common\models\CurrencyIoModification::add([
            'name'              => 'ERC-20',
            'currency_io_id'    => 3,
            'output_min'        => 70000,
            'comission_out'     => 3.35,
            'master_wallet'     => '0x16537eCC59d3cf3448e621afC0F7469eD43A9c70',
            'wallet_field_name' => 'usdt_erc20',
        ]);

        \common\models\CurrencyIoModification::add([
            'name'              => 'TRC-20',
            'currency_io_id'    => 3,
            'output_min'        => 70000,
            'comission_out'     => 0,
            'master_wallet'     => 'TBXaGoDyBuhBerdc2Vakcf9wuMFzVwXhM5',
            'wallet_field_name' => 'usdt_trc20',
        ]);

        $this->addColumn('user_wallet', 'usdt_erc20', 'VARCHAR(100) null');
        $this->addColumn('user_wallet', 'usdt_trc20', 'VARCHAR(100) null');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210107_202624_io cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210107_202624_io cannot be reverted.\n";

        return false;
    }
    */
}
