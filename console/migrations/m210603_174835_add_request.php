<?php

use yii\db\Migration;

/**
 * Class m210603_174835_add_request
 */
class m210603_174835_add_request extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->execute('CREATE TABLE `gs_users_shop_requests_add` (
    `id` int NOT NULL AUTO_INCREMENT,
    request_id   int            null,
    billing_id   int            null,
    created_at   int            null,
    amount       int            null,
    currency_id  int            null,
    user_id      int            null,
    comment      varchar(200)   null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('gs_users_shop_requests_add_request_id', 'gs_users_shop_requests_add', 'request_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210603_174835_add_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210603_174835_add_request cannot be reverted.\n";

        return false;
    }
    */
}
