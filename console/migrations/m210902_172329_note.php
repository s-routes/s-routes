<?php

use yii\db\Migration;

/**
 * Class m210902_172329_note
 */
class m210902_172329_note extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('ALTER TABLE s_routes_prod_main.user_telegram_lot_connect RENAME TO s_routes_prod_main.lot_user_telegram_connect;');
        $this->execute('ALTER TABLE s_routes_prod_main.user_telegram_lot_temp RENAME TO s_routes_prod_main.lot_user_telegram_temp;;');

        $this->execute('CREATE TABLE `lot_note` (
    `id`          int             NOT NULL AUTO_INCREMENT,
    user_id int null,
	created_at int null,
	name varchar(100) null,
	content text null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('lot_note_user_id', 'lot_note', 'user_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210902_172329_note cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210902_172329_note cannot be reverted.\n";

        return false;
    }
    */
}
