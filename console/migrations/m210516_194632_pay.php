<?php

use yii\db\Migration;

/**
 * Class m210516_194632_pay
 */
class m210516_194632_pay extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {


        $this->execute('CREATE TABLE `payments_tinkoff` (
  `id` int NOT NULL AUTO_INCREMENT,
    billing_id  int          null,
    value       double       null,
    action      varchar(20)  null,
    card        varchar(30)  null,
    transaction varchar(200) null,
    `from`      varchar(200) null,
    num         varchar(100) null,
    date        varchar(100) null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->execute('CREATE TABLE `payments_ozon` (
  `id` int NOT NULL AUTO_INCREMENT,
    billing_id  int          null,
    value       double       null,
    action      varchar(20)  null,
    card        varchar(30)  null,
    transaction varchar(200) null,
    `from`      varchar(200) null,
    num         varchar(100) null,
    date        varchar(100) null,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('payments_tinkoff_billing_id', 'payments_tinkoff', 'billing_id');
        $this->createIndex('payments_ozon_billing_id', 'payments_ozon', 'billing_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210516_194632_pay cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210516_194632_pay cannot be reverted.\n";

        return false;
    }
    */
}
