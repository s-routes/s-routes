<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\ChartPoint;
use common\models\CurrencyIO;
use common\models\CurrencyIoModification;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\TaskInput;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security\AES;
use common\services\Subscribe;
use cs\Application;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use Yii;

class InputController extends \console\base\Controller
{
    /** @var CurrencyIO */
    public $c;

    /**
     * Смотрит все открытые заявки и закрывает их
     */
    public function actionCheck()
    {
        /** @var \common\models\TaskInput $task */
        foreach (\common\models\TaskInput::find()->where(['status' => 0])->orderBy(['created_at' => SORT_ASC])->all() as $task) {

            if (time() - $task->created_at > 60*60*24) {

                $task->status = 2;
                $task->save();

                echo 'task #' . $task->id . ' closed' . "\n";

            } else {

                $c = CurrencyIO::findOne($task->currency_io_id);
                $mod = null;
                if (!Application::isEmpty($task->modification_id)) {
                    $mod = CurrencyIoModification::findOne($task->modification_id);
                    $function_class = $mod->function_check;
                    $master_wallet_is_binance = $mod->master_wallet_is_binance;
                } else {
                    $function_class = $c->function_check;
                    $master_wallet_is_binance = $c->master_wallet_is_binance;
                }
                $this->c = $c;

                // вычисляю как искать транзакцию
                if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
                    $function = 'blockchain';
                } else {
                    if ($master_wallet_is_binance) {
                        $function = 'binance';
                    } else {
                        $function = 'blockchain';
                    }
                }

                $cInt = Currency::findOne($c->currency_int_id);

                if ($function == 'binance') {
                    $ret = $this->findInBinance($task);
                } else {
                    /** @var \avatar\services\currency\CurrencyInterface $function_check */
                    $function_check = new $function_class();

                    $ret = $function_check->check($task, $c, $mod);
                }

                if ($ret['status'] == 1) {
                    $fields = [];
                    if ($task->type_id == TaskInput::TYPE_ACCOUNT) {
                        $fields = ['txid' => $ret['transactionHash']];
                    }

                    try {
                        $this->action2($task, $ret['value'], $fields);
                    } catch (\Exception $e) {

                        // Уведомляю бухгалтера по телеграму
                        $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
                        foreach ($userList as $uid) {
                            $user = UserAvatar::findOne($uid);
                            if (!Application::isEmpty($user->telegram_chat_id)) {
                                /** @var \aki\telegram\Telegram $telegram */
                                $telegram = Yii::$app->telegram;
                                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                                $sum = Yii::$app->formatter->asDecimal(
                                        bcdiv($task->amount, bcpow(10, $cInt->decimals), $cInt->decimals),
                                        $cInt->decimals
                                    ) . ' ' . $cInt->code ;
                                $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => Yii::t('c.LSZBX1ce2W', 'Заявка #{id} на ввод {sum} ОШИБКА!! . ' . $e->getMessage() . "\n" . $e->getTraceAsString(), ['id' => $task->id, 'sum' => $sum], $language)]);
                            }
                        }

                        echo 'task #' . $task->id . ' error' . "\n";
                        continue;
                    }

                    $sum = Yii::$app->formatter->asDecimal(
                            bcdiv($task->amount, bcpow(10, $cInt->decimals), $cInt->decimals),
                            $cInt->decimals
                        ) . ' ' . $cInt->code ;

                    // Уведомляю пользователя по телеграму
                    $user = UserAvatar::findOne($task->user_id);
                    if (!Application::isEmpty($user->telegram_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegram;
                        $telegram->sendMessage([
                            'chat_id' => $user->telegram_chat_id,
                            'text'    => Yii::t('c.LSZBX1ce2W', 'Заявка #{id} на ввод {sum} выполнена', ['id' => $task->id, 'sum' => $sum], 'ru')
                        ]);
                    }

                    // Уведомляю бухгалтера по телеграму
                    $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
                    foreach ($userList as $uid) {
                        $user = UserAvatar::findOne($uid);
                        if (!Application::isEmpty($user->telegram_chat_id)) {
                            /** @var \aki\telegram\Telegram $telegram */
                            $telegram = Yii::$app->telegram;
                            $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;

                            $telegram->sendMessage([
                                'chat_id' => $user->telegram_chat_id,
                                'text' => Yii::t('c.LSZBX1ce2W', 'Заявка #{id} на ввод {sum} выполнена', ['id' => $task->id, 'sum' => $sum], $language)
                            ]);
                        }
                    }

                    echo 'task #' . $task->id . ' completed' . "\n";

                } else if ($ret['status'] == 2) {
                    echo 'error ' . $ret['message'] . "\n";
                } else {
                    echo 'task #' . $task->id . ' wait status' . $ret['status'] . "\n";
                }
            }
        }
    }

    /**
     * Зачисляет монеты
     *
     * @param  \common\models\TaskInput $task кол-во копеек сколько надо зачислить
     * @param  int                      $amountNQT   кол-во копеек сколько надо зачислить
     * @param  array                    $fields      поля для задачи
     * @return array
     * @throws \Exception
     */
    private function action2($task, $amountNQT, $fields)
    {
        $task->amount = $amountNQT;

        // Ищу кошелек пользователя
        $d = UserBill::getInternalCurrencyWallet($this->c->currency_int_id, $task->user_id);
        /** @var \common\models\piramida\Wallet $walletIn */
        $walletIn = $d['wallet'];

        // Ищу кошелек откуда зачислять
        $walletOut = Wallet::findOne($this->c->main_wallet);
        $txid_int = $walletOut->move($walletIn, $task->amount,
            Json::encode([
                'Зачисление на основании задачи id={task}',
                [
                    'task' => $task->id
                ]
            ])

        );

        foreach ($fields as $k => $v) {
            $task->$k = $v;
        }
        // Сохраняю данные в задачу о положительном завершении
        $task->txid_internal = $txid_int->id;
        $task->status = 1;
        $task->finished_at = time();
        $task->save();

        $cur = \common\models\piramida\Currency::findOne($this->c->currency_int_id);

        // отправляю уведомление на почту
        $user = UserAvatar::findOne($task->user_id);
        Subscribe::sendArray([$user->email], 'Заявка на пополнение выполнена', 'task_input_done', [
            'task'             => $task,
            'amount_formatted' => Yii::$app->formatter->asDecimal(bcdiv($task->amount, bcpow(10, $cur->decimals), $cur->decimals), $cur->decimals),
            'code'             => $cur->code,
        ]);

        return [
            'status'                => 1,
            'amount'                => $task->amount,
            'amount_formatted'      => Yii::$app->formatter->asDecimal(bcdiv($task->amount, bcpow(10, $cur->decimals), $cur->decimals), $cur->decimals),
            'code'                  => $cur->code,
        ];
    }

    /**
     * Ищет поступление для заявки
     *
     * @see https://binance-docs.github.io/apidocs/spot/en/#deposit-history-supporting-network-user_data
     * @param \common\models\TaskInput $task
     *
     * 'txId' => 'Internal transfer 60276181415'
     *
     * @return array
     *
     * // не найдено зачисление
     * [
     *  'status' => 0
     * ]
     *
     * // найдено зачисление
     * [
     *  'status' => 1
     *  'value' => 1        //кол-во копеек сколько надо зачислить
     *  'transactionHash' => 'sdsdadasd'
     * ]
     *
     * //критическая ошибка
     * [
     *  'status' => 2
     *  'message' => string
     * ]
     */
    private function findInBinance($task)
    {
        $cio = CurrencyIO::findOne($task['currency_io_id']);
        $cInt = Currency::findOne($cio->currency_int_id);
        $mod = null;
        $network = $cInt->code;
        if (!Application::isEmpty($task->modification_id)) {
            $mod = CurrencyIoModification::findOne($task->modification_id);
            $network = $mod->code;
        }

        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        /**
         * @var array
         *
         * [
         * 0 => [
         * 'amount' => '15'
         * 'coin' => 'USDT'
         * 'network' => 'TRX'
         * 'status' => 1
         * 'address' => 'TUtQbDUjgTrgofhna4tyFFHMxA2J3RJWV1'
         * 'addressTag' => ''
         * 'txId' => 'Internal transfer 60276181415'
         * 'insertTime' => 1620846069000
         * 'transferType' => 1
         * 'confirmTimes' => '1/1'
         * ]
         */
        $data = $provider->_callApiKeyAndSigned('/sapi/v1/capital/deposit/hisrec', [
            'coin' => $cInt->code,
        ]);

        foreach ($data as $row) {
            if ($row['network'] == $network) {
                if (strtolower($row['txId']) == strtolower($task->txid)) {
                    if ($row['status'] == 1) {
                        $amount_binance = Currency::getAtomFromValue($row['amount'], $this->c->currency_int_id);
                        if ($task->amount_user == $amount_binance) {
                            return [
                                'status'          => 1,
                                'transactionHash' => $row['txId'],
                                'value'           => $task->amount_user,
                            ];
                        }
                    }
                }
            }
        }

        return [
            'status' => 0,
        ];
    }

}