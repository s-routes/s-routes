<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\ChartPoint;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security\AES;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use Yii;

class BillingController extends \console\base\Controller
{
    /**
     *
     *
     * Очищает все WebHook которые старше $time (в сек)
     */
    public function actionBuild()
    {
        $data = [
            0 => 'bitcoin:3Hq2rDs3vPegA3hXGs33xu9XB9RJNDYyhz?i-am-avatar=true',
            1 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            2 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            3 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            4 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            5 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            6 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            7 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            8 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
            9 => 'bitcoin:3Cjr4ouDqgQxnnDnLv9hHmqqop4xsZrerM?i-am-avatar=true',
        ];
        $back = \Yii::getAlias('@console/base/back.png');

        $image = $this->watermark(
            $back,
            [
                [
                    'file'  => [
                        'format'  => 'png',
                        'content' => (new \Endroid\QrCode\QrCode())
                            ->setText($data[0])
                            ->setSize(212 - 20)
                            ->setPadding(10)
                            ->setErrorCorrection('medium')
                            ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0])
                            ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0])
                            ->setLabelFontSize(16)
                            ->setImageType(\Endroid\QrCode\QrCode::IMAGE_TYPE_PNG)
                            ->get('png'),
                    ],
                    'start' => [106, 366],
                ],
            ]
        );
        $image = $this->text(
            $image['img'],
            [
                [
                    'text'        => '000',
                    'start'       => [188, 366],
                    'fontOptions' => [
                        'size'  => 10,
                        'color' => '000',
                    ],
                ],
            ]
        );

        self::logSuccess('Done');
    }


    /**
     * @param string $file полный путь к файлу
     * @param array $watermark
     * [[
     *      'file' => string - полный путь | array [
     *          'format'  => string - расширение возможно jpg png
     *          'content' => string - содержимое
     *      ]
     *      'start' => array [x, y] - точка от верхнего левого угла оригинальной картинки
     * ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function watermark($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = \Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (is_array($watermarkConfig['file'])) {
                $temp = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . \Yii::$app->security->generateRandomString(10) . '.' . $watermarkConfig['file']['format'];
                file_put_contents($temp, $watermarkConfig['file']['content']);
            } else {
                $temp = $watermarkConfig['file'];
            }
            $image = Image::watermark($background, $temp, $watermarkConfig['start']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }

    /**
     * @param string $file полный путь к файлу
     * @param array $watermark
     * [[
     *      'text'     => string
     *      'start'    => array [x, y] - точка от верхнего левого угла оригинальной картинки
     *      'fontOptions' => string
     *      'fontFile' => string
     * ],...]
     *
     * @return array
     * [
     *      'img'  => \Imagine\Image\ImageInterface
     *      'path' => string - полный путь к файлу png
     * ]
     *
     * @throws \yii\base\Exception
     */
    private function text($file, $watermark)
    {
        if (count($watermark) == 0) {
            throw new Exception('Не задан массив $watermark');
        }
        $tempFilePath = Yii::getAlias('@runtime/watermark');
        FileHelper::createDirectory($tempFilePath);
        $background = $file;
        $image = null;
        foreach ($watermark as $watermarkConfig) {
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['fontOptions'] = [];
            }
            if (!isset($watermarkConfig['fontOptions'])) {
                $watermarkConfig['start'] = [0, 0];
            }

            $image = Image::text($background, $watermarkConfig['text'], $watermarkConfig['fontFile'], $watermarkConfig['start'], $watermarkConfig['fontOptions']);
            $background = $tempFilePath . DIRECTORY_SEPARATOR . time() . '_' . Yii::$app->security->generateRandomString(10) . '.' . 'png';
            file_put_contents($background, $image->get('png'));
        }

        return [
            'img'  => $image,
            'path' => $background,
        ];
    }



    /**
     * Очищает все WebHook которые старше $time (в сек)
     */
    public function actionClear()
    {
        /** @var integer $time время в секундах после создания заявки, после которого удаляется слушатель */
        $time = 60 * 60 * 24 * 7;
        $rows = PaymentBitCoin::find()
            ->where(['<', 'time_add', time() - $time])
            ->andWhere(['is_web_hook_deleted' => 0])
            ->all();
        self::log('all = ' . count($rows));
        $provider = new BitCoinBlockTrailPayment();
        $client = $provider->getClient();
        /** @var \common\models\PaymentBitCoin $payment */
        foreach ($rows as $payment) {
            try {
                $ret = $client->deleteWebhook($payment->web_hook);

                self::log('deleted web_hook=' . $payment->web_hook . ' billing_id=' . $payment->billing_id);
                $payment->is_web_hook_deleted = 1;
                $ret = $payment->save();
            } catch (\Exception $e) {
                self::logDanger('Fail deleted web_hook ' . $payment->web_hook . ' billing_id=' . $payment->billing_id);
            }
        }

        self::logSuccess('Done');
    }

    /**
     * Удаляет все кошелькт которые не используются
     */
    public function actionClearNotUsed()
    {
        // найти нет ли ссылок на user_bill_operation
        $rows = UserBill::find()->where(['mark_deleted' => 1])->all();
        /** @var \common\models\avatar\UserBill $billing */
        foreach ($rows as $billing) {
            if (UserBillOperation::find()->where(['bill_id' => $billing->id])->exists()) {
                self::logDanger('Billing not deleted. id=' . $billing->id);
            } else {
                $billing->delete();
                self::log('Billing deleted id=' . $billing->id);
            }
        }
        self::logSuccess('Done');
    }

    /**
     * Удаляет все кошелькт которые не используются
     */
    public function actionConvertConfig()
    {
        // найти нет ли ссылок на user_bill_operation
        $rows = UserBill::find()->where(['not', ['config' => null]])->all();

        /** @var \common\models\avatar\UserBill $billing */
        foreach ($rows as $billing) {
            if (!UserBillConfig::find()->where(['id' => $billing->id])->exists()) {
                $config = new UserBillConfig([
                    'id'     => $billing->id,
                    'config' => $billing->config,
                ]);
                $config->save();
                self::log('id='.$billing->id);
            }
        }
        (new Query())->createCommand()->update('user_bill', ['config' => null])->execute();
        self::logSuccess('Done');
    }

    /**
     *
     */
    public function actionRecoverAddresses()
    {
        $rows = UserBill::find()
            ->leftJoin('user_bill_address', 'user_bill.id = user_bill_address.bill_id')
            ->where(['user_bill_address.address' => null])
            ->select('user_bill.*')
            ->all();

        /** @var \common\models\avatar\UserBill $billing */
        foreach ($rows as $billing) {
            $address = new UserBillAddress([
                'bill_id'    => $billing->id,
                'address'    => $billing->address,
                'created_at' => time(),
            ]);
            $address->save();
            self::log(VarDumper::dumpAsString([
                'address' => $billing->address,
                'bill_id' => $billing->id,
            ]));
        }
    }

    /**
     * Создает счета системные Новая Земля и Банк Аватар
     */
    public function actionCreate()
    {
        $names = [
            'Новая Земля',
            'Аватар Банк',
        ];
        foreach($names as $name) {
            $wallet = \avatar\models\Wallet::create($name);
            $wallet->billing->id;
            $bill = new UserBillSystem([
                'id'       => $wallet->billing->id,
            ]);
            $bill->save();
            self::log('id='.$wallet->billing->id);
        }
        self::logSuccess('Done');
    }

    public function actionNormalize()
    {
        ChartPoint::deleteAll(['<', 'time', time() - 60*60*24*7]);
    }

    /**
     */
    public function actionNormalize1()
    {
        if (count(\Yii::$app->requestedParams) != 2) {
            self::logDanger('params needed `php yii billing/set-password [userId] [password]`');
            exit();
        }
        $userId = \Yii::$app->requestedParams[0];
        $password = \Yii::$app->requestedParams[1];
        $billingList = UserBill::find()->where(['user_id' => $userId, 'password_type' => UserBill::PASSWORD_TYPE_HIDE_CABINET])->all();
        self::log('count='.count($billingList));

        /** @var \common\models\avatar\UserBill $billing */
        foreach ($billingList as $billing) {
            $hash = $billing->password;
            $key32 = UserBill::passwordToKey32($password);
            $passwordWallet = AES::decrypt256CBC($hash, $key32);
            if ($passwordWallet == '') {
                self::logDanger('Не верный пароль');
                exit;
            }
            $billing->password = $passwordWallet;
            $billing->password_type = UserBill::PASSWORD_TYPE_OPEN;
            $billing->save();
            self::log('$billing->id='.$billing->id. ' success');
        }

    }
}