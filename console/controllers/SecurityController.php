<?php

namespace console\controllers;

use common\models\piramida\Operation;
use common\models\statistic\PingStatus;
use common\models\subscribe\Subscribe;
use common\models\subscribe\SubscribeMail;
use common\models\subscribe\SubscribeStat;
use common\models\UserAvatar;
use cs\Application;
use kartik\base\Config;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

/**
 */
class SecurityController extends \console\base\Controller
{
    public function actions()
    {
        return [
            'ping'                     => '\console\controllers\actions\actionPing',
            'test-wallets'             => '\console\controllers\actions\actionTestWallets',
            'test-wallets-no-telegram' => [
                'class'          => '\console\controllers\actions\actionTestWallets',
                'notifyTelegram' => false,
            ],
        ];
    }

    public function actionCheckView()
    {
        $text = 'Необходимо провести профилактический осмотр сервера RESERV';

        $userListAdmin = \Yii::$app->authManager->getUserIdsByRole('role_admin');

        $userList = $userListAdmin;
        foreach ($userList as $uid) {
            $user = UserAvatar::findOne($uid);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = \Yii::$app->telegram;
                $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
            }
        }
    }

}