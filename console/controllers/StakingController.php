<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\Card;
use common\models\Config;
use common\models\Contribution;
use common\models\CurrencyIO;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\NeironTransaction;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\shop\Request;
use common\models\UserAvatar;
use common\models\UserContribution;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Subscribe;
use cs\Application;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\imagine\Image;
use Yii;

class StakingController extends \console\base\Controller
{

    /**
     */
    public function actionClose()
    {
        $rows = UserContribution::find()
            ->where(['status' => UserContribution::STATUS_CREATED])
            ->andWhere(['<', 'finish_at', time()])
            ->all();

        /** @var \common\models\UserContribution $c */
        foreach ($rows as $c) {
            try {
                // делаю возврат
                $wallet = Wallet::findOne($c->wallet_id);
                $data = UserBill::getInternalCurrencyWallet(Currency::NEIRO, $c->user_id);
                /** @var \common\models\piramida\Wallet $walletTo */
                $walletTo = $data['wallet'];
                $t = $wallet->move2(
                    $walletTo,
                    $c->amount,
                    Json::encode([
                        'Стейкинг: возврат на баланс вклада #{id}',
                        [
                            'id' => $c->id
                        ]
                    ]),
                    NeironTransaction::TYPE_HOLD_RETURN
                );

                NeironTransaction::add([
                    'transaction_id' => $t['transaction']['id'],
                    'operation_id'   => $t['operation_add']['id'],
                    'type_id'        => NeironTransaction::TYPE_HOLD_RETURN,
                ]);

                // начисляю проценты
                $cio = CurrencyIO::findOne(['currency_int_id' => Currency::NEIRO]);
                $wallet = Wallet::findOne($cio->main_wallet);
                /** @var \common\models\Contribution $c1 */
                $c1 = Contribution::findOne($c->contribution_id);
                $a = (int) $c->amount * (($c1['time'] / (60 * 60 * 24)) * ($c1['percent'] / (1000 * 30)) / 100);
                $t = $wallet->move2(
                    $walletTo,
                    $a,
                    Json::encode([
                        'Проценты за стейкинг по вкладу #{id}',
                        [
                            'id' => $c->id
                        ]
                    ]),
                    NeironTransaction::TYPE_HOLD_PERCENT
                );

                NeironTransaction::add([
                    'transaction_id' => $t['transaction']['id'],
                    'operation_id'   => $t['operation_add']['id'],
                    'type_id'        => NeironTransaction::TYPE_HOLD_PERCENT,
                ]);

                // закрываю вклад
                $c->status = UserContribution::STATUS_FINISH;
                $c->save();
                echo 'UserContribution id='.$c->id.' closed'."\n";

            } catch (\Exception $e) {
                echo 'error: ' . $e->getMessage()."\n";
            }
        }
    }

    /**
     */
    public function actionTelegramPing()
    {
        $rows = UserContribution::find()
            ->where(['status' => UserContribution::STATUS_CREATED])
            ->andWhere(['>', 'finish_at', time()])
            ->all();
        $cNeironInt = Currency::findOne(Currency::NEIRO);
        $cUsd = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::USDT);
        $cNeiro = \common\models\avatar\Currency::findOne(\common\models\avatar\Currency::NEIRO);

        /** @var \common\models\UserContribution $c */
        foreach ($rows as $c) {
            /** @var \common\models\Contribution $c1 */
            $c1 = Contribution::findOne($c->contribution_id);

            // процентов в день / 100
            $perByDay = ($c1['percent'] / (1000 * 30)) / 100;

            // Сколько в день начислено
            $dayNeironAtom = (int) $c->amount * $perByDay;
            $dayNeiron = Currency::getValueFromAtom($dayNeironAtom, Currency::NEIRO);
            $dayUSD = $dayNeiron * $cNeiro->kurs / $cUsd->kurs;

            // Сколько накопилось на данныый момент вместе с депозитом
            $allNeironAtom = (int) $c->amount * (((time() - $c['created_at']) / (60 * 60 * 24)) * ($c1['percent'] / (1000 * 30)) / 100) + $c->amount;
            $allNeiron = Currency::getValueFromAtom($allNeironAtom, Currency::NEIRO);
            $allUSD = $allNeiron * $cNeiro->kurs / $cUsd->kurs;

            $text = sprintf(
                'NEIRON Token Staking Reward Message: Вам начислен суточный доход на депозит по стейкинг-пакету ID %d в размере %s NEIRO (~ %s USDT). Общий баланс пакета № %d равен %s NEIRO (~ %s USDT)',
                $c->id,
                $dayNeiron,
                Yii::$app->formatter->asDecimal($dayUSD, 2),
                $c->id,
                $allNeiron,
                Yii::$app->formatter->asDecimal($allUSD, 2)
            );

            $this->notificateTelegram([$c->user_id], $text);

            echo 'uid=' . $c->user_id . ' ' . $text . "\n";
        }
    }


    /**
     * Уведомляет Админа и бух
     */
    private function notificateTelegram($all, $text)
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        foreach ($all as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {

                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => $text]);
            }
        }
    }

}