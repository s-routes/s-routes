<?php

namespace console\controllers;

use common\models\avatar\Currency;
use common\models\Config;
use common\models\CryptoCap;
use common\models\Currency2;
use common\models\statistic\PingStatus;
use cs\services\VarDumper;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\httpclient\Client;

/**
 * Test controller
 */
class MoneyRateController extends \console\base\Controller
{
    const RATE_BTC_USD = 'rate.BTC/USD';
    const RATE_BTC_RUB = 'rate.BTC/RUB';

    /**
     * Обновляет графики курс биткойна, AllMarket
     */
    public function actionUpdate()
    {
        // Сохраняю курс AllMarket
        $this->saveCryptoCap();
    }

    public function actionUpdate3()
    {
        $this->CoinMarketCapUpdateFirstPage();
    }

    public function actionUpdateEgold()
    {
        /** @var \common\services\CurrencyDataFeed $CurrencyDataFeed */
        $CurrencyDataFeed = \Yii::$app->CurrencyDataFeed;
        $data = $CurrencyDataFeed->get('data.php', [
            'currency' => 'XAU/RUB',
        ]);
        if (!$data['status']) {
            echo 'status=false' . "\n";
            return;
        } else {
            $v = $data['currency'][0]['value'] / 100000;
            $c = Currency::findOne(['code' => "EGOLD"]);
            $cUSD = Currency::findOne(['code' => "USDT"]);
            $c->kurs = $v;
            $c->price_rub = $v;
            $c->price_usd = $v / $cUSD->kurs;
            $c->save();
            echo 'kurs='.$v . "\n";
        }
    }

    public function actionFlush()
    {
        \Yii::$app->cache->flush();
    }

    /**
     * Обновляет все основыне валюты
     */
    public function actionUpdate2()
    {
        // Обновляю курсы для USD EUR
        $url = 'https://min-api.cryptocompare.com';

        $client = new \yii\httpclient\Client(['baseUrl' => $url]);
        try {
            $response = $client->get('data/price', [
                'fsym'  => 'USD',
                'tsyms' => 'RUB,EUR,UAH,BYN',
            ])->send();

            if ($response->statusCode == 200) {
                PingStatus::add(3, 1);
            } else {
                PingStatus::add(3, 0);
            }
        } catch (\Exception $e) {
            PingStatus::add(3, 0);
            return;
        }


        /** @var array $data
         * {
         * "RUB": 58.69,
         * "EUR": 0.8333
         * }
         */
        $data = \yii\helpers\Json::decode($response->content);

        $b = Currency::findOne(Currency::USDT);
        $b->kurs = $data['RUB'];
        $b->price_rub = $b->kurs;
        $b->price_usd = 1;
        $b->save();
        $kurs_usd = $data['RUB'];

        $b = Currency::findOne(['code' => 'MARKET']);
        $b->kurs = 1;
        $b->price_rub = 1;
        $b->price_usd = 1 / $kurs_usd;
        $b->save();

        $b = Currency::findOne(['code' => 'LOT']);
        $b->kurs = 1;
        $b->price_rub = 1;
        $b->price_usd = 1 / $kurs_usd;
        $b->save();

        $b = Currency::findOne(['code' => 'EUR']);
        $b->kurs = $data['RUB'] / $data['EUR'];
        $b->save();

        $b = Currency::findOne(['code' => 'RUB']);
        $b->kurs = 1;
        $b->price_rub = 1;
        $b->price_usd = 1 / $kurs_usd;
        $b->save();

        $b = Currency::findOne(['code' => 'UAH']);
        $b->kurs = $data['RUB'] / $data['UAH'];
        $b->save();

        $b = Currency::findOne(['code' => 'BYN']);
        $b->kurs = $data['RUB'] / $data['BYN'];
        $b->save();

        // вычисляю цену NEIRO
        $b = Currency::findOne(['code' => 'NEIRO']);
        /** @var \common\services\GoldPrizeApi $p */
        $p = \Yii::$app->GoldPrizeApi;

        try {
            $res = $p->get('rates/currency/usd/measure/gram/metal/all');
        } catch (\Exception $e) {
            PingStatus::add(5, 0);
            return;
        }

        $data = Json::decode($res);
        $kurs = $data['silver_gram_in_usd'] * 31.104;
        $price_usd = $kurs / 100;
        $b->price_usd = $price_usd;
        $price_rub = $kurs_usd * $price_usd;
        $b->price_rub = $price_rub;
        $b->kurs = $price_rub;
        $b->save();

        // Получаю цену с Бинанса
        $this->getBinance($kurs_usd);

        // Получаю цену
        $rows = Currency::find()
            ->where(['not', ['id_coingecko2' => null]])
            ->select(['id_coingecko2'])
            ->column();

        $client = new Client(['baseUrl' => 'https://api.coingecko.com/api/v3']);

        try {
            $response = $client->get('/simple/price', [
                'ids'                 => join(',', $rows),
                'vs_currencies'       => 'usd',
                'include_market_cap'  => 'true',
                'include_24hr_vol'    => 'true',
                'include_24hr_change' => 'true',
            ])->send();
            if ($response->statusCode == 200) {
                PingStatus::add(4, 1);
            } else {
                PingStatus::add(4, 0);
            }
        } catch (\Exception $e) {
            PingStatus::add(4, 0);
            return;
        }

        $content = Json::decode($response->content);

        /**
         * btc_market_cap
         * btc_24h_vol
         * btc_24h_change
         */
        foreach ($content as $id2 => $v) {
            if (isset($v['usd'])) {
                $cur = Currency::findOne(['id_coingecko2' => $id2]);
                if (!is_null($cur)) {
                    $cur->kurs = $v['usd'] * $kurs_usd;
                    $cur->market_cap_usd = $v['usd_market_cap'];
                    $cur->price_usd = $v['usd'];
                    $cur->day_volume_usd = $v['usd_24h_vol'];
                    $cur->chanche_1d = $v['usd_24h_change'];
                    $cur->save();

                    echo $cur->code . ' ' . $cur->kurs . "\n";
                }
            }
        }

        // очищаю кеш
        Currency::clearCache();

        Config::set('kurs-update-time', time());
    }

    private function getBinance($usd_price)
    {
        $currency = [
            'BTC',
            'ETH',
            'LTC',
            'TRX',
            'DASH',
            'BNB',
        ];

        $v = '/api/v3/ticker/price';
        /** @var \frontend\modules\Binance\Binance $b */
        $b = \Yii::$app->Binance;
        $data = $b->_callApiKey($v, []);

        $data2 = ArrayHelper::map($data, 'symbol', 'price');

        $rows = [];
        foreach ($currency as $c) {
            $rows[$c] = $data2[$c . 'USDT'];
            $price_usd = $data2[$c . 'USDT'];
            $c2 = Currency::findOne(['code' => $c]);
            $c2->price_usd = $price_usd;
            $c2->kurs = $price_usd * $usd_price;
            $c2->price_rub = $price_usd * $usd_price;
            $c2->save();
            echo 'binance: ' . $c2->code . ' ' . $c2->kurs . "\n";
        }
    }


    /**
     * Выдает капитализацию криптовалют в млрд
     * @return float
     */
    private function getCryptoCap()
    {
        $url = "http://api.coincap.io/v2/assets";
        $client = new Client();
        $response = $client->get($url, null)->send();
        $sum = 0;
        $data = Json::decode($response->content);

        foreach ($data['data'] as $currency) {
            $s  = ArrayHelper::getValue($currency, 'marketCapUsd');
            if ($s != 'NaN') $sum = bcadd($sum, $s);
        }

        return bcdiv($sum, (1000 * 1000 * 1000), 3);
    }

    /**
     * Обновляет курсы всех валют кроме 'RUB', 'USD', 'EUR'
     * @return bool
     */
    private function CoinMarketCapUpdate()
    {
        // Кол-во перебираемых страниц
        $max = 15;

        for ($page = 1; $page <= $max; $page++) {
            self::log('coinmarketcap page=' . $page);
            $this->CoinMarketCapUpdatePage($page);
        }

        return true;
    }

    /**
     * Обновляет курсы всех валют кроме 'RUB', 'USD', 'EUR'
     * @return bool
     */
    private function CoinMarketCapUpdatePage($page)
    {


        if (in_array($response->headers->get('http-code'), [429])) {
            throw new \Exception('429');
        }
        if (!in_array($response->headers->get('http-code'), [200, 301])) {
            return false;
        }
        $data = Json::decode($response->content);

        $currencyList = Currency::find()
            ->where(['not in', 'code', ['RUB', 'USD', 'EUR']])
            ->andWhere(['not', ['code_coinmarketcap' => '']])
            ->all();

        /** @var \common\models\avatar\Currency $currency */
        foreach ($currencyList as $currency) {
            foreach ($data as $CoinCapCurrency) {
                if ($currency->id_coinmarketcap) {
                    if ($CoinCapCurrency['id'] == $currency->id_coinmarketcap) {
                        echo $CoinCapCurrency['id'] . "\n";
                        $this->save($currency, $CoinCapCurrency);
                        continue;
                    }
                } else {
                    if (isset($CoinCapCurrency['symbol'])) {
                        if ($currency->code_coinmarketcap == $CoinCapCurrency['symbol']) {
                            $this->save($currency, $CoinCapCurrency);
                            continue;
                        }
                    } else {
                        \Yii::warning('No item SYMBOL '. \yii\helpers\VarDumper::dumpAsString([$data,$response]), 'avatar\console\controllers\MoneyRateController::CoinMarketCapUpdatePage');
                        self::log(\yii\helpers\VarDumper::dumpAsString([$data,$response]));
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param \common\models\avatar\Currency    $currencyAv
     * @param array                             $currencyCmc
     */
    private function save($currencyAv, $currencyCmc)
    {
        $currency = $currencyAv;
        $CoinCapCurrency = $currencyCmc;
        $currency->kurs = $CoinCapCurrency['price_rub'];
        $currency->chanche_1h = $CoinCapCurrency['percent_change_1h'];
        $currency->chanche_1d = $CoinCapCurrency['percent_change_24h'];
        $currency->chanche_1w = $CoinCapCurrency['percent_change_7d'];
        $currency->day_volume_usd = $CoinCapCurrency['24h_volume_usd'];
        $currency->market_cap_usd = $CoinCapCurrency['market_cap_usd'];
        $currency->available_supply = $CoinCapCurrency['available_supply'];
        $currency->total_supply = $CoinCapCurrency['total_supply'];
        $currency->max_supply = $CoinCapCurrency['max_supply'];
        $currency->rank = $CoinCapCurrency['rank'];
        $currency->price_rub = $CoinCapCurrency['price_rub'];
        $currency->price_usd = $CoinCapCurrency['price_usd'];
        $currency->price_btc = $CoinCapCurrency['price_btc'];
        $currency->id_coinmarketcap = $CoinCapCurrency['id'];
        $ret = $currency->save();
        if (!$ret) \Yii::warning('not save c='.$currency->code , 'avatar\console\controllers\MoneyRateController::CoinMarketCapUpdatePage');

        // Сохраняю график
        $ret = \common\models\ChartPoint::add([
            'currency_id'   => $currency->id,
            'rub'           => $CoinCapCurrency['price_rub'],
            'usd'           => $CoinCapCurrency['price_usd'],
            'btc'           => $CoinCapCurrency['price_btc'],
        ]);
    }

    /**
     * Обновляет курсы всех валют кроме 'RUB', 'USD', 'EUR'
     * @return bool
     */
    private function CoinMarketCapUpdateFirstPage()
    {
        $client = new Client();
        $response = $client->get('https://api.coinmarketcap.com/v1/ticker', ['convert' => 'RUB'])->send();
        if (!in_array($response->headers->get('http-code'), [200, 301])) {
            return false;
        }
        $data = Json::decode($response->content);

        $currencyList = Currency::find()
            ->where(['is_crypto' => 1])
            ->andWhere(['not', ['code_coinmarketcap' => '']])
            ->all();

        foreach ($data as $CoinCapCurrency) {
            $isFind = false;
            /** @var \common\models\avatar\Currency $currency */
            foreach ($currencyList as $currency) {
                if (isset($CoinCapCurrency['symbol'])) {
                    if ($currency->code_coinmarketcap == $CoinCapCurrency['symbol']) {
                        $isFind = true;
                        break;
                    }
                }
            }

            if (!$isFind) {
                $currency = new Currency([
                    'code'               => $CoinCapCurrency['symbol'],
                    'title'              => $CoinCapCurrency['name'],
                    'kurs'               => $CoinCapCurrency['price_rub'],
                    'code_coinmarketcap' => $CoinCapCurrency['symbol'],
                    'is_crypto'          => 1,
                    'chanche_1h'         => $CoinCapCurrency['percent_change_1h'],
                    'chanche_1d'         => $CoinCapCurrency['percent_change_24h'],
                    'chanche_1w'         => $CoinCapCurrency['percent_change_7d'],
                    'day_volume_usd'     => $CoinCapCurrency['24h_volume_usd'],
                    'market_cap_usd'     => $CoinCapCurrency['market_cap_usd'],
                    'available_supply'   => $CoinCapCurrency['available_supply'],
                    'total_supply'       => $CoinCapCurrency['total_supply'],
                    'max_supply'         => $CoinCapCurrency['max_supply'],
                    'rank'               => $CoinCapCurrency['rank'],
                    'price_rub'          => $CoinCapCurrency['price_rub'],
                    'price_usd'          => $CoinCapCurrency['price_usd'],
                    'price_btc'          => $CoinCapCurrency['price_btc'],
                ]);
                $ret = $currency->save();

                if (!$ret) \Yii::warning('not save c='.$currency->code , 'avatar\console\controllers\MoneyRateController::CoinMarketCapUpdateFirstPage');

                // Сохраняю график
                $ret = \common\models\ChartPoint::add([
                    'currency_id' => $currency::getDb()->lastInsertID,
                    'rub'         => $CoinCapCurrency['price_rub'],
                    'usd'         => $CoinCapCurrency['price_usd'],
                    'btc'         => $CoinCapCurrency['price_btc'],
                ]);
                self::log('added ' . $CoinCapCurrency['symbol']);
            }

        }



        return true;
    }

    /**
     * Сохраняет значение криптомаркета в таблицу
     */
    private function saveCryptoCap()
    {
        $o = new CryptoCap([
            'value' => $this->getCryptoCap(),
            'time'  => time(),
        ]);
        $o->save();
    }

    private function saveCharts()
    {
    }


}