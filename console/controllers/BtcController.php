<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\Card;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Subscribe;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\imagine\Image;
use Yii;

class BtcController extends \console\base\Controller
{

    /**
     */
    public function actionEstimateFee()
    {
        $coinList = ['btc', 'ltc', 'dash'];
        foreach ($coinList as $coin) {
            $client1 = new Client(['baseUrl' => 'https://api.blockcypher.com/v1']);
            $result = $client1->get($coin . '/main')->send();

            if ($result->statusCode != 200) {
                self::log('$result->statusCode != 200');
            } else {
                $data = Json::decode($result->content);
                $SafeGasPrice     = (int)($data['low_fee_per_kb'] / 2);
                $ProposeGasPrice  = (int)($data['medium_fee_per_kb'] / 2);
                $FastGasPrice     = (int)($data['high_fee_per_kb'] / 2);

                $camel = strtoupper(substr($coin,0, 1)) . substr($coin, 1);
                Config::set($camel.'SafeGasPrice', $SafeGasPrice);
                Config::set($camel.'ProposeGasPrice', $ProposeGasPrice);
                Config::set($camel.'FastGasPrice', $FastGasPrice);

                self::log('success '.$camel.' avg price = '.$ProposeGasPrice);
            }
        }
    }

}