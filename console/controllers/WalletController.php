<?php

namespace console\controllers;

use app\models\ActionsRange;
use app\models\Certificates;
use app\models\CertificatesDocuments;
use backend\components\Controller;
use backend\models\PaymentSystemsForm;
use common\extensions\KendoAdapter\KendoDataProvider;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\Currency;
use common\models\CurrencyLog;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\Orders;
use common\models\PaymentBitCoin;
use common\models\PaymentSystem;
use common\models\piramida\Operation;
use common\models\piramida\Transaction;
use common\models\piramida\Wallet;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\services\Debugger;
use frontend\base\JsonController;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use common\models\User;
use common\extensions\KendoAdapter\KendoFiltersCollection;
use app\models\CrmLog;
use common\models\Billing;
use common\models\Banners;
use app\models\UserPaymentAccountsTypes;
use common\models\SmartresponderHistory;
use yii\web\Request;


/**
 */
class WalletController extends \console\base\Controller
{
    /**
     * Меняет
     */
    public function actionPasswordOne()
    {
        $billingList = \common\models\avatar\UserBill::find()
            ->where([
                'mark_deleted'  => 0,
                'password_type' => UserBill::PASSWORD_TYPE_OPEN,
            ])
            ->all();

        $userIdArray = [];
        /** @var \common\models\avatar\UserBill $billing */
        foreach ($billingList as $billing) {
            $unlockKey = Yii::$app->params['unlockKey'];
            $key = UserBill::passwordToKey32($unlockKey);
            $billing->password = \common\services\Security\AES::encrypt256CBC($billing->password, $key);
            $billing->password_type = UserBill::PASSWORD_TYPE_OPEN_CRYPT;
            $billing->save();
            if (!is_null($billing->user_id)) {
                $userIdArray[] = $billing->user_id;
            }
            self::log('billing id = ' . $billing->id);
        }

        $userIdArray = array_unique($userIdArray);
        $userList = \common\models\UserAvatar::find()
            ->andWhere([
                'mark_deleted'       => 0,
                'password_save_type' => UserBill::PASSWORD_TYPE_OPEN,
                ])
            ->all();

        /** @var \common\models\UserAvatar $user */
        foreach ($userList as $user) {
            $user->password_save_type = UserBill::PASSWORD_TYPE_OPEN_CRYPT;
            $user->save();
            self::log('user id = ' . $user->id);
        }
    }

    /**
     * Проверяет реальный баланс всех кошельков с записанным в БД
     */
    public function actionTest()
    {
        /** @var \common\models\piramida\Wallet $w */
        foreach (Wallet::find()->all() as $w) {

            $amount = $w->amount;
            $operationList = Operation::find()->where(['wallet_id' => $w->id])->all();
            $sum = 0;
            /** @var \common\models\piramida\Operation $o */
            foreach ($operationList as $o) {
                switch ($o->type) {
                    case Operation::TYPE_IN:
                    case Operation::TYPE_EMISSION:
                        $sum += $o->amount;
                        break;
                    case Operation::TYPE_OUT:
                    case Operation::TYPE_BURN:
                        $sum -= $o->amount;
                        break;
                }
            }
            if ($amount == $sum) {
                //echo 'wallet id' . $w->id. ' success' . "\n";
            } else {
                echo 'wallet id=' . $w->id. ' error $amount=' . $amount . ' $sum=' . $sum . ' diff=' . ($sum - $amount) . ' cid='. $w->currency_id . "\n";
            }
        }
    }

    /**
     * Проверяет Хеши операций и транзакций
     */
    public function actionTest2()
    {
        /** @var \common\models\piramida\Operation $w */
        foreach (Operation::find()->all() as $w) {
            if ($w->hash() != $w->hash) {
                echo 'wallet oid=' . $w->id. ' error hash' . "\n";
            }
        }
        /** @var \common\models\piramida\Transaction $w */
        foreach (Transaction::find()->all() as $w) {
            if ($w->hash() != $w->hash) {
                echo 'wallet tid=' . $w->id. ' error hash' . "\n";
            }
        }
    }

    /**
     * Проверяет баланс в монетах и в операциях
     */
    public function actionTest3()
    {
        /** @var \common\models\piramida\Operation $w */
        foreach (Operation::find()->all() as $w) {
            if ($w->type < 0) {
                if ($w->before - $w->after != $w->amount) {
                    echo 'oid=' . $w->id. ' error balance' . "\n";
                }
            } else {
                if ($w->after - $w->before != $w->amount) {
                    echo 'oid=' . $w->id. ' error balance' . "\n";
                }
            }
        }

        /** @var \common\models\piramida\Currency $w */
        foreach (\common\models\piramida\Currency::find()->all() as $w) {
            $sum = Wallet::find()->where(['currency_id' => $w->id])->select('sum(amount)')->scalar();
            if ($sum === false) $sum = 0;
            if (is_null($sum)) $sum = 0;
            if ($w->amount != $sum) {
                echo 'currency cid=' . $w->id. ' error amount='. $w->amount . ' sum=' . $sum . ' diff=' . $w->amount - $sum  . "\n";
            }
        }
    }

}
