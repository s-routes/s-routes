<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use common\models\ChatMessage2;
use common\models\exchange\ChatMessage;
use common\models\exchange\Deal;
use common\models\NeironSupport;
use cs\services\VarDumper;
use cs\Widget\BreadCrumbs\BreadCrumbs;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class DbController extends \console\base\Controller
{

    public function actionDeal()
    {
        $rows = Deal::find()->all();

        /** @var \common\models\exchange\Deal $row */
        foreach ($rows as $row) {
            $chatRoom = \common\models\ChatRoom::add(['last_message' => null]);

            echo 'deal_id=' . $row['id'] . ' started';
            $messageList = ChatMessage::find()->where(['deal_id' => $row['id']])->all();
            $ids = [];
            /** @var \common\models\exchange\ChatMessage $m */
            foreach ($messageList as $m) {
                \Yii::$app->db->createCommand()->insert('chat_message2',[
                    'room_id' => $chatRoom['id'],
                    'user_id' => $m['user_id'],
                    'message' => Json::encode([
                        'text'       => $m['message'],
                        'file'       => '',
                    ]),
                    'created_at' => $m['created_at'],
                ])->execute();
                $ids[] = $m['id'];
            }
            $row->chat_room_id = $chatRoom->id;
            $row->save();
            //ChatMessage::deleteAll(['in', 'id', $ids]);
            echo ' finished' . "\n";
        }
    }

    public function actionSupport()
    {
        NeironSupport::deleteAll();
        $rows = \common\models\ChatList::find()->all();

        /** @var \common\models\ChatList $row */
        foreach ($rows as $row) {
            $chatRoom = \common\models\ChatRoom::add(['last_message' => $row['last_message']]);
            $support = NeironSupport::add([
                'user_id'      => $row['user_id'],
                'room_id'      => $chatRoom['id'],
                'last_message' => $row['last_message'],
            ]);
            $chat_id = 2000000000 + $row['user_id'];
            echo 'user_id=' . $row['user_id'] . ' started' . "\n";
            $messageList = ChatMessage::find()->where(['deal_id' => $chat_id])->all();
            $ids = [];
            /** @var \common\models\exchange\ChatMessage $m */
            foreach ($messageList as $m) {
                \Yii::$app->db->createCommand()->insert('chat_message2',[
                    'room_id' => $chatRoom['id'],
                    'user_id' => $m['user_id'],
                    'message' => Json::encode([
                        'text'       => $m['message'],
                        'file'       => '',
                    ]),
                    'created_at' => $m['created_at'],
                ])->execute();
                $ids[] = $m['id'];
            }
            //ChatMessage::deleteAll(['in', 'id', $ids]);
            echo 'user_id=' . $row['user_id'] . ' finished' . "\n";
        }
    }

    /**
     */
    public function actionClear()
    {
        $dbList = [
            'db',
            'dbWallet',
            'dbStatistic',
        ];

        // то чего оставить
        $data = [
            'db' => [
                'config',
                'country',
                'currency',
                'currency_crypto',
                'currency_io',
                'currency_link_avatar_processing',
                'languages',
                'languages_access',
                'languages_category_tree',
                'message',
                'migration',
                'offer_pay',
                'page',
                'pay_method',
                'source_message',
                'user',
                'user_bill',
                'user_wallet',
            ],
            'dbWallet' => [
                ['currency', 'update currency set amount=0 where 1'],
                'login',
                ['wallet', 'update wallet set amount=0 where 1'],
            ],
        ];

        foreach ($dbList as $dbAlias) {
            $dsn = \Yii::$app->$dbAlias->dsn;
            if (!isset($data[$dbAlias])) {
                $data[$dbAlias] = [];
                $list = [];
            } else {
                $list = $this->getNameList($data[$dbAlias]);
            }


            $pos = strpos($dsn, ':');
            $str1 = substr($dsn,$pos + 1);

            $arr = explode(';', $str1);
            $name = '';
            foreach ($arr as $i) {
                $a = explode('=', $i);
                if ($a[0] == 'dbname') {
                    $name = $a[1];
                }
            }

            // получаю список таблиц
            $rows = \Yii::$app->dbInfo->createCommand()->setSql("select * from TABLES where TABLE_SCHEMA='{$name}'")->queryAll();
            $tableList = ArrayHelper::getColumn($rows, 'TABLE_NAME');

            // удаляю данные из таблиц исключая $data
            foreach ($tableList as $name) {
                $t1 = microtime(true);
                if (!in_array($name, $list)) {
                    // то я ее очищаю
                    \Yii::$app->$dbAlias->createCommand()->setSql("delete from {$name} where 1;")->execute();
                    echo 'delete from ' . $name . ' t='.\Yii::$app->formatter->asDecimal(microtime(true) - $t1, 2). "\n";
                } else {
                    $i = $this->getName($data[$dbAlias], $name);
                    if (is_array($i)) {

                        \Yii::$app->$dbAlias->createCommand()->setSql($i[1])->execute();
                        echo $i[1] . ' t='.\Yii::$app->formatter->asDecimal(microtime(true) - $t1, 2). "\n";
                    }
                }
            }
        }
    }
    /**
     */
    public function actionCutForDev()
    {
        $dbList = [
            'db',
            'dbWallet',
            'dbStatistic',
        ];

        // то чего оставить
        $data = [
            'db' => [
                'config',
                'country',
                'currency',
                'currency_crypto',
                'currency_io',
                'currency_link_avatar_processing',
                'languages',
                'languages_access',
                'languages_category_tree',
                'message',
                'migration',
                'offer_pay',
                'page',
                'news',
                'pay_method',
                'source_message',
                ['user', function($dbAlias, $u) {
                    if (!is_null($u)) {
                        \Yii::$app->$dbAlias->createCommand()->setSql('delete from user where id != ' . $u)->execute();
                    }
                }],
                ['user_bill', function($dbAlias, $u) {
                    if (!is_null($u)) {
                        \Yii::$app->$dbAlias->createCommand()->setSql('delete from user_bill where user_id != ' . $u)->execute();
                    }
                }],
                ['user_wallet', function($dbAlias, $u) {
                    if (!is_null($u)) {
                        \Yii::$app->$dbAlias->createCommand()->setSql('delete from user_wallet where user_id != ' . $u)->execute();
                    }
                }],
            ],
            'dbWallet' => [
                ['currency', 'update currency set amount=0 where 1'],
                'login',
                ['wallet', 'update wallet set amount=0 where 1'],
            ],
        ];

        foreach ($dbList as $dbAlias) {
            $dsn = \Yii::$app->$dbAlias->dsn;
            if (!isset($data[$dbAlias])) {
                $data[$dbAlias] = [];
                $list = [];
            } else {
                $list = $this->getNameList($data[$dbAlias]);
            }

            $pos = strpos($dsn, ':');
            $str1 = substr($dsn,$pos + 1);

            $arr = explode(';', $str1);
            $name = '';
            foreach ($arr as $i) {
                $a = explode('=', $i);
                if ($a[0] == 'dbname') {
                    $name = $a[1];
                }
            }

            // получаю список таблиц
            $rows = \Yii::$app->dbInfo->createCommand()->setSql("select * from TABLES where TABLE_SCHEMA='{$name}'")->queryAll();
            $tableList = ArrayHelper::getColumn($rows, 'TABLE_NAME');

            // удаляю данные из таблиц исключая $data
            foreach ($tableList as $name) {
                $t1 = microtime(true);
                if (!in_array($name, $list)) {
                    // то я ее очищаю
                    \Yii::$app->$dbAlias->createCommand()->setSql("delete from {$name} where 1;")->execute();
                    echo 'delete from ' . $name . ' t='.\Yii::$app->formatter->asDecimal(microtime(true) - $t1, 2). "\n";
                } else {
                    $i = $this->getName($data[$dbAlias], $name);
                    if (is_array($i)) {
                        if ($i[1] instanceof \Closure) {
                            $params = \Yii::$app->request->params;
                            $u = null;
                            foreach ($params as $p) {
                                if (substr($p, 0, 1) == 'u') {
                                    $u = substr($p, 1);
                                    break;
                                }
                            }
                            $function = $i[1];
                            $function($dbAlias, $u);
                            echo 'Closure ' . $i[0] . ' t='.\Yii::$app->formatter->asDecimal(microtime(true) - $t1, 2). "\n";
                        } else {
                            \Yii::$app->$dbAlias->createCommand()->setSql($i[1])->execute();
                            echo $i[1] . ' t='.\Yii::$app->formatter->asDecimal(microtime(true) - $t1, 2). "\n";
                        }
                    }

                }
            }
        }
    }

    public function getNameList($list)
    {
        $ret = [];
        foreach ($list as $i)
        {
            if (is_array($i)) {
                $ret[] = $i[0];
            } else {
                $ret[] = $i;
            }
        }

        return $ret;
    }

    public function getName($list, $name)
    {
        $ret = [];
        foreach ($list as $i)
        {
            if (is_array($i)) {
                if ($i[0] == $name) return $i;
            } else {
                if ($i == $name) return $i;
            }
        }

        return $ret;
    }

}