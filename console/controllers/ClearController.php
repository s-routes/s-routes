<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillOperation;
use yii\console\Controller;

class ClearController extends Controller
{
    /**
     * Очищает все счета
     */
    public function actionRun()
    {
        UserBill::deleteAll();
        UserBillAddress::deleteAll();
        UserBillOperation::deleteAll();
        echo('Done.');
    }

} 