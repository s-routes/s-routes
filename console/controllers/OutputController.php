<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use avatar\models\forms\RegistrationActivate;
use common\models\avatar\CurrencyLink;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\ChartPoint;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\CurrencyIoModification;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\TaskInput;
use common\models\TaskOutput;
use common\models\TaskOutputHistory;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Security\AES;
use common\services\Subscribe;
use cs\Application;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\imagine\Image;
use Yii;

class OutputController extends \console\base\Controller
{


    /**
     * Смотрит все открытые заявки и закрывает их
     */
    public function actionBinanceFinish()
    {
        $rows = TaskOutput::find()->where(['status' => TaskOutput::STATUS_INIT])->all();
        if (count($rows) > 0) {
            /** @var \frontend\modules\Binance\Binance $provider */
            $provider = Yii::$app->Binance;

            $data = $provider->_callApiKeyAndSigned('sapi/v1/capital/withdraw/history', []);
            $rows2 = $data;

            /** @var \common\models\TaskOutput $task */
            foreach ($rows as $task) {
                $binanceTask = $this->find2($rows2, $task->binance_id);
                if (is_null($binanceTask)) {
                    Yii::error('заявка ' . $task->binance_id . ' не найдена', 'avatar\console\controllers\OutputController::actionBinanceFinish');
                    continue;
                }

                // Если заявка выполнена?
                if ($binanceTask['status'] == 6) {
                    $task->txid = $binanceTask['txId'];
                    $task->finished_at = time();
                    $task->status = \common\models\TaskOutput::STATUS_DONE;
                    $task->save();

                    $cio = CurrencyIO::findOne($task->currency_io_id);
                    $cExt = \common\models\avatar\Currency::findOne($task->currency_id);
                    $value = \common\models\piramida\Currency::getValueFromAtom($task->amount - $task->comission_user, $cio->currency_int_id);
                    $text = 'Вывод ' . $value . ' ' . $cExt->code . ' по заявке #' . $task->id . ' завершен. TXID: ' . $task->txid;

                    // Отправляю письмо уведомление клиенту
                    /** @var UserAvatar $user */
                    $user = UserAvatar::findOne($task->user_id);
                    \common\services\Subscribe::sendArray([$user->email], $text,'task_out_done', [
                        'task' => $task,
                    ]);

                    // Добавляю историю заявки
                    $i = TaskOutputHistory::add([
                        'task_id' => $task->id,
                        'comment' => $text,
                    ]);

                    // Уведомляю пользователю по телеграму
                    $user = UserAvatar::findOne($task->user_id);
                    if (!Application::isEmpty($user->telegram_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegram;
                        $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
                    }

                    // Уведомляю админа и бухгалтера по телеграму
                    $userListBuh = Yii::$app->authManager->getUserIdsByRole('role_buh');
                    $userListAdmin = Yii::$app->authManager->getUserIdsByRole('role_admin');
                    $userList = ArrayHelper::merge($userListBuh, $userListAdmin);
                    $userList = array_unique($userList);
                    foreach ($userList as $uid) {
                        $user = UserAvatar::findOne($uid);
                        if (!Application::isEmpty($user->telegram_chat_id)) {
                            /** @var \aki\telegram\Telegram $telegram */
                            $telegram = Yii::$app->telegram;
                            $telegram->sendMessage(['chat_id' => $user->telegram_chat_id, 'text' => $text]);
                        }
                    }

                    echo 'task ' . $task->id . ' done' . "\n";
                }
            }
        }
    }

    /**
     * Ищет заявку $id в заявках $rows
     * @param $rows
     * @param $id
     * @return null | array
     */
    private function find2($rows, $id)
    {
        foreach ($rows as $binanceTask) {
            if ($binanceTask['id'] == $id) {
                return $binanceTask;
            }
        }

        return null;
    }

    /**
     * Обновляет значения комиссии на вывод
     */
    public function actionUpdateBinance()
    {
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;
        $data = $provider->_callApiKeyAndSigned('sapi/v1/capital/config/getall', []);

        // получаю наши проценты
        $neiroComission = Config::get('WithdrawFeePersent');
        if ($neiroComission === false) $neiroComission = '[]';
        $neiroComission = Json::decode($neiroComission);

        $c = [
            'BTC/BTC',
            'ETH/ETH',
            'USDT/ETH',
            'USDT/TRX',
            'LTC/LTC',
            'TRX/TRX',
            'DASH/DASH',
            'BNB/ETH',
            'DAI/ETH',
            'DOGE/DOGE',
            'DOGE/BSC',
            'DAI/BSC',
            'BNB/BSC',
        ];
        foreach ($c as $v) {
            $arr = explode('/', $v);
            $coin = $arr[0];
            $net = $arr[1];
            $binance = $this->find($data, $coin, $net);
            $percent = $this->getPercent($neiroComission, $coin, $net);
            $min = $this->getMin($neiroComission, $coin, $net);
            $c = \common\models\piramida\Currency::findOne(['code' => $arr[0]]);
            $cio = \common\models\CurrencyIO::findOne(['currency_int_id' => $c->id]);

            if ($arr[0] == $arr[1]) {

                $cio->comission_out = $binance['withdrawFee'] * (1 + ($percent / 100));
                $cio->benance_fee = $binance['withdrawFee'];
                $cio->output_min = Currency::getAtomFromValue(((float)($binance['withdrawMin'])) * (1 + ($min / 100)), $c);
                $cio->benance_min = Currency::getAtomFromValue(((float)($binance['withdrawMin'])), $c);
                $cio->save();

                $mod = \common\models\CurrencyIoModification::findOne(['code' => $coin, 'currency_io_id' => $cio->id]);
                if (!is_null($mod)) {
                    $mod->comission_out = $binance['withdrawFee'] * (1 + ($percent / 100));
                    $mod->benance_fee = $binance['withdrawFee'];
                    $mod->output_min = Currency::getAtomFromValue($binance['withdrawMin'] * (1 + ($min / 100)), $c);
                    $mod->benance_min = Currency::getAtomFromValue($binance['withdrawMin'], $c);
                    $mod->save();
                }
                echo $v . ' withdrawFee=' . $cio->comission_out . ' withdrawMin=' . $cio->output_min .  "\n";

            } else {
                $mod = \common\models\CurrencyIoModification::findOne(['code' => $arr[1], 'currency_io_id' => $cio->id]);

                $mod->comission_out = $binance['withdrawFee'] * (1 + ($percent / 100));
                $mod->benance_fee = $binance['withdrawFee'];
                $mod->output_min = Currency::getAtomFromValue($binance['withdrawMin'] * (1 + ($min / 100)), $c);
                $mod->benance_min = Currency::getAtomFromValue($binance['withdrawMin'], $c);
                $mod->save();

                echo $v . ' withdrawFee=' . $mod->comission_out . ' withdrawMin=' . $mod->output_min .  "\n";
            }
        }
    }

    private function find($data, $coin, $network)
    {
        foreach ($data as $c) {
            if ($c['coin'] == $coin) {
                foreach ($c['networkList'] as $net) {
                    if ($net['network'] == $network) return $net;
                }
            }
        }

        return null;
    }

    private function getPercent($data, $coin, $network)
    {
        foreach ($data as $c) {
            if ($c['id'] == $coin . '_' . $network) {
                return $c['out'];
            }
        }

        return 0;
    }

    private function getMin($data, $coin, $network)
    {
        foreach ($data as $c) {
            if ($c['id'] == $coin . '_' . $network) {
                if (!isset($c['min'])) {
                    return 0;
                }
                return $c['min'];
            }
        }

        return 0;
    }

}