<?php

namespace console\controllers;

use app\models\ActionsRange;
use app\models\Certificates;
use app\models\CertificatesDocuments;
use backend\components\Controller;
use backend\models\PaymentSystemsForm;
use common\extensions\KendoAdapter\KendoDataProvider;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\Currency;
use common\models\CurrencyLog;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\Orders;
use common\models\PaymentBitCoin;
use common\models\PaymentSystem;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\models\UserAvatar;
use common\services\Debugger;
use frontend\base\JsonController;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use common\models\User;
use common\extensions\KendoAdapter\KendoFiltersCollection;
use app\models\CrmLog;
use common\models\Billing;
use common\models\Banners;
use app\models\UserPaymentAccountsTypes;
use common\models\SmartresponderHistory;
use yii\web\Request;


/**
 * Выполняет воссоздание базы данных с оригинала
 *
 * Class SyncController
 * @package console\controllers
 */
class TestController extends \console\base\Controller
{
    public function actionIndex()
    {
        /** @var \common\models\avatar\UserBillMerchant $merchant */
        foreach (\common\models\avatar\UserBillMerchant::find()->all() as $merchant) {
            try {
                $billing = \common\models\avatar\UserBill::findOne($merchant->id);
                $merchant->user_id = $billing->user_id;
                $merchant->save();
                echo 'merchant ' . $merchant->id . ' updated';
                echo "\n";
            } catch (Exception $e) {
                echo 'not found '. $merchant->id;
                echo "\n";
            }
        }

    }
    public function actionIndex3()
    {
        $unlockKey = '98beb27135aa0a25650557005ad962919d6a278c4b3dde7f4f6a3a1e65aa746c';
        $key = UserBill::passwordToKey32($unlockKey);
        $p = new \avatar\modules\ETH\ServiceEtherScan();

        $rows = \common\models\UserBill3::find()->where(['not', ['currency' => 3]])->all();
        /** @var \common\models\UserBill3 $b */
        foreach ($rows as $b) {
            if ($b->password_type == 4) {
                $passwordHash = $b->password;
                $passwordWallet = \common\services\Security\AES::decrypt256CBC($passwordHash, $key);
                $b->pass = $passwordWallet;
                $b1 = $p->getBalance($b->address);
                $b->balance = $b1[0]['balance'];
                $b->save();
            }
            if ($b->password_type == 1) {
                $b->pass = $b->password;
                $b1 = $p->getBalance($b->address);
                $b->balance = $b1[0]['balance'];
                $b->save();
            }
            echo 'address = ' . $b->address . ' b=' . $b->balance . ' b2=' . bcdiv($b->balance, bcpow(10, 18), 18) . "\n";
        }
    }


    public function actionSync()
    {
        $s_routes_users = UserAvatar::find()->select('email')->column();
        $sale_users = User::find()->select('email')->column();


        // Ищу пользователей которых не хватает в $sale_users
        $sale_users_add = [];
        foreach ($sale_users as $email) {
            if (!in_array($email, $s_routes_users)) {
                $sale_users_add[] = $email;
            }
        }

        // Ищу пользователей которых не хватает в $s_routes_users
        $s_routes_users_add = [];
        foreach ($s_routes_users as $email) {
            if (!in_array($email, $sale_users)) {
                $s_routes_users_add[] = $email;
            }
        }

        $this->log(count($s_routes_users) - count($s_routes_users_add));
        $this->log(count($sale_users) - count($sale_users_add));
//        $this->log(VarDumper::dumpAsString($s_routes_users_add));
//        $this->log(VarDumper::dumpAsString($sale_users_add));

        $this->log('adding $sale_users_add');
        // Добавляю пользователей в s-routes которых нехватает
        foreach ($sale_users_add as $email) {
            $user = User::findOne(['email' => $email]);

            $email = strtolower($email);
            $fields = [
                'email'              => $email,
                'password'           => $user->password,
                'mark_deleted'       => 0,
                'email_is_confirm'   => 1,
                'wallets_is_locked'  => 0,
                'subscribe_is_news'  => 1,
                'subscribe_is_blog'  => 1,
                'created_at'         => time(),
                'auth_key'           => \Yii::$app->security->generateRandomString(60),
                'password_save_type' => UserBill::PASSWORD_TYPE_OPEN_CRYPT,
            ];

            $user = new UserAvatar($fields);
            $ret = $user->save();
            $user->id = UserAvatar::getDb()->lastInsertID;
            $this->log('add '.$email);
        }

        $this->log('adding $s_routes_users_add');
        // Добавляю пользователей в s-routes которых нехватает
        foreach ($s_routes_users_add as $email) {
            $user = UserAvatar::findOne(['email' => $email]);

            $email = strtolower($email);
            $fields = [
                'email'         => $email,
                'password'      => $user->password,
                'registerDate'  => time(),
                'confirmed'     => 1,
                'time_bonus'    => 5,
                'lastLogin'     => 0,
                'ip'            => 1,
                'rememberToken' => '1',
                'role'          => 0,
            ];
            $u = new User($fields);
            $u->save();
            $u->id = $u::getDb()->lastInsertID;
            $this->log('add '.$email);
        }

    }



    /**
     * Меняет
     */
    public function actionPass()
    {
        $billingList = \common\models\avatar\UserBill::find()->where(['mark_deleted' => 0])->all();

        /** @var \common\models\avatar\UserBill $billing */
        foreach ($billingList as $billing) {
            $unlockKey = Yii::$app->params['unlockKey'];
            $key = UserBill::passwordToKey32($unlockKey);
            $billing->password = \common\services\Security\AES::encrypt256CBC($billing->password, $key);
            $billing->save();
            self::log('billing id = ' . $billing->id);
        }
    }

}
