<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\CurrencyLink;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\BinanceOrder;
use common\models\Card;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\NeironTransaction;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\ReferalTransaction;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Subscribe;
use cs\Application;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\imagine\Image;
use Yii;

class ConvertController extends \console\base\Controller
{
    public function getId($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'  => $id,
            'in'  => 0,
            'out' => 0,
        ];
    }

    /**
     * @param \common\models\BinanceOrder $convert
     */
    function referalProgram($convert)
    {
        $user_id = $convert->user_id;

        $cio = CurrencyIO::findOne(['currency_int_id' => \common\models\piramida\Currency::NEIRO]);
        $walletOut = Wallet::findOne($cio->main_wallet);

        /** @var \common\models\UserPartner $partner */
        $partner = \common\models\UserPartner::find()->where(['school_id' => 1, 'user_id' => $user_id])->one();
        $c = 0;
        $level = [
            0 => 5,
            1 => 3,
            2 => 1,
            3 => 3,
            4 => 5,
        ];

        while ($c < 5) {
            if (is_null($partner)) return;
            $uid = $partner->parent_id;
            $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::NEIRO, $uid);

            // Если у пользователя больше 1000 монет на счету то делаю рефералку
            if ($data['wallet']['amount'] > \common\models\piramida\Currency::getAtomFromValue(1000, \common\models\piramida\Currency::NEIRO)) {
                $amountRef = (int) ($convert->amount_to * ($level[$c] / 100));

                // Делаю транзакцию пользователю
                $t = $walletOut->move2(
                    $data['wallet'],
                    $amountRef,
                    Json::encode([
                        'Реферальные начисления за конвертацию bid={cid} от uid={from} к uid={to}',
                        [
                            'cid'  => $convert->id,
                            'from' => $user_id,
                            'to'   => $uid,
                        ]
                    ]),
                    NeironTransaction::TYPE_REFERAL_CONVERT_BINANCE
                );

                // Делаю запись о типе транзакции если это NERON
                NeironTransaction::add([
                    'transaction_id' => $t['transaction']['id'],
                    'operation_id'   => $t['operation_add']['id'],
                    'type_id'        => NeironTransaction::TYPE_REFERAL_CONVERT_BINANCE,
                ]);

                // Сделать запись о реферальном начислении
                ReferalTransaction::add([
                    'request_id'     => $convert->id,
                    'type_id'        => ReferalTransaction::TYPE_CONVERT_BINANCE,
                    'currency_id'    => \common\models\piramida\Currency::NEIRO,
                    'level'          => $c + 1,
                    'from_uid'       => $user_id,
                    'to_uid'         => $uid,
                    'to_wid'         => $data['wallet']['id'],
                    'amount'         => $amountRef,
                    'transaction_id' => $t['transaction']['id'],
                ]);
            }

            $partner = \common\models\UserPartner::find()->where(['school_id' => 1, 'user_id' => $partner['parent_id']])->one();

            $c++;
        }
    }

    /**
     */
    public function actionCheck()
    {
        $this->checkOpenedBinance();
        $this->checkNotOpenedBinance();
    }

    /**
     * Проверяет сделки которые открыты на бинансе но не заполнены (FILLED) до конца
     * Ситуация 1)
     */
    private function checkOpenedBinance()
    {
        $rows = BinanceOrder::find()
            ->where(['status' => BinanceOrder::STATUS_CREATED])
            ->andWhere(['not', ['binance_order_id' => null]])
            ->all();

        /** @var \common\models\BinanceOrder $Order */
        foreach ($rows as $Order) {
            $data1 = Json::decode($Order['binance_return']);

            /** @var \frontend\modules\Binance\Binance $provider */
            $provider = Yii::$app->Binance;
            $data = $provider->_callApiKeyAndSigned('/api/v3/order', [
                'symbol'  =>    $data1['symbol'],
                'orderId' =>    $Order['binance_order_id'],
            ]);

            /** Если заявка выполнена? */
            if ($data['status'] == 'FILLED') {

                // возвращаю деньги
                $amount1 = $data['cummulativeQuoteQty'];

                $bill = UserBill::findOne($Order->billing_id);
                $CIOfrom = CurrencyIO::findOne(['currency_ext_id' => $bill->currency]);

                // Отдаю монеты
                $toCurrencyExt = \common\models\avatar\Currency::findOne($Order->currency_to);
                $CIO_to = CurrencyIO::findOne(['currency_ext_id' => $toCurrencyExt->id]);
                $cInt_to = \common\models\piramida\Currency::findOne($CIO_to->currency_int_id);
                $walletOut = Wallet::findOne($CIO_to->main_wallet);
                $dataTo = UserBill::getInternalCurrencyWallet($cInt_to->id, $Order->user_id);

                // получаю комиссию системы и тип транзакции
                $dataJson = Config::get('NeironConvertParams');
                $type = \common\models\NeironTransaction::TYPE_CONVERT;
                if ($Order->currency_to == \common\models\avatar\Currency::NEIRO) {
                    $dataJson = Config::get('NeironConvertParams');
                    if (in_array($CIOfrom->currency_ext_id, [
                        \common\models\avatar\Currency::BTC,
                        \common\models\avatar\Currency::LTC,
                        \common\models\avatar\Currency::DASH,
                        \common\models\avatar\Currency::ETH,
                        \common\models\avatar\Currency::TRX,
                        \common\models\avatar\Currency::BNB,
                    ])) {
                        $type = \common\models\NeironTransaction::TYPE_CONVERT_GOOD;
                    }
                }
                if ($Order->currency_to == \common\models\avatar\Currency::MARKET) {
                    $dataJson = Config::get('MarketConvertParams');
                }

                $data = Json::decode($dataJson);
                $o = $this->getIdNeiron($data, $CIOfrom->currency_ext_id);
                $amount_m_to = $amount1 * (1 - ($o['in'] / 100)) / $toCurrencyExt->price_usd;
                $amountTo = bcmul($amount_m_to, bcpow(10, $cInt_to->decimals));

                $t_to = $walletOut->move2(
                    $dataTo['wallet'],
                    $amountTo,
                    Json::encode([
                        'Конвертация в ' . $cInt_to->code . ' bid={id}',
                        ['id' => $Order->id],
                    ]),
                    $type
                );

                if ($Order->currency_to == \common\models\avatar\Currency::MARKET) {
                    $amount100 = $amount1 / $toCurrencyExt->price_usd;
                    // раскидываю рефералку
                    \avatar\models\forms\ConvertMarketBinance::referal($Order->id, $walletOut, $Order->user_id, Currency::getAtomFromValue($amount100, $cInt_to->id));
                }

                // Устанавливаю статус
                $Order->tx_to_id = $t_to['transaction']['id'];
                $Order->amount_to = $amountTo;
                $Order->status = BinanceOrder::STATUS_FILLED;
                $Order->save();

                // Делаю реферальные начисления
                if ($type == \common\models\NeironTransaction::TYPE_CONVERT_GOOD) {
                    $this->referalProgram($Order);
                }

                // Уведомляю пользователя по телеграму
                $user = UserAvatar::findOne($Order->user_id);
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                    $telegram->sendMessage([
                        'chat_id' => $user->telegram_chat_id,
                        'text'    => Yii::t(
                            'c.LSZBX1ce2W',
                            'Конвертация #{id} выполнена. Получено {sum} {code}',
                            [
                                'id'   => $Order->id,
                                'sum'  => $amount_m_to,
                                'code' => $cInt_to->code,
                            ],
                            $language
                        ),
                    ]);
                }

                // Уведомляю бухгалтера по телеграму
                $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
                foreach ($userList as $uid) {
                    $user = UserAvatar::findOne($uid);
                    if (!Application::isEmpty($user->telegram_chat_id)) {
                        /** @var \aki\telegram\Telegram $telegram */
                        $telegram = Yii::$app->telegram;
                        $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                        $telegram->sendMessage([
                            'chat_id' => $user->telegram_chat_id,
                            'text'    => Yii::t(
                                'c.LSZBX1ce2W',
                                'Конвертация #{id} выполнена. Получено {sum} {code}',
                                [
                                    'id'   => $Order->id,
                                    'sum'  => $amount_m_to,
                                    'code' => $cInt_to->code,
                                ],
                                $language
                            ),
                        ]);
                    }
                }

                echo 'order id='. $Order->id . ' finish' . "\n";
            } else {
                echo 'order id='. $Order->id . ' waiting' . "\n";
            }
        }
    }

    /**
     * Проверяет сделки при открытии которых не было денег на бинансе
     * Ситуация 2)
     */
    private function checkNotOpenedBinance()
    {
        $rows = BinanceOrder::find()
            ->where(['status' => BinanceOrder::STATUS_CREATED])
            ->andWhere(['binance_order_id' => null])
            ->all();

        if (count ($rows) > 0) {
            /** @var \frontend\modules\Binance\Binance $provider */
            $provider = Yii::$app->Binance;
            // Проверяю баланс на бинансе
            $data = $provider->_callApiKeyAndSigned('/api/v3/account', []);
            $data['balances'];
            /**
             * @var array $balanceList
             * [
             *  'BTC' => '0.154',
             *  'LTC' => '0.0154',
             *  ...
             * ]
             */
            $balanceList = ArrayHelper::map($data['balances'], 'asset', 'free');

            /** @var \common\models\BinanceOrder $Order */
            foreach ($rows as $Order) {

                $UserBill = UserBill::findOne($Order->billing_id);
                $currencyInt = Currency::initFromCurrencyExt($UserBill->currency);

                $a = Currency::getValueFromAtom($Order->amount, $currencyInt->id);
                // Если баланс на бирже меньше чем указал пользователь?
                if ($balanceList[$currencyInt->code] < $a) {
                    echo 'BinanceOrder id=' . $Order->id . ' not enought money' . "\n";
                } else {
                    $this->binanceYes($Order);
                }

            }
        }
    }

    public function getIdNeiron($data, $id)
    {
        foreach ($data as $o) {
            if ($o['id'] == $id) {
                return $o;
            }
        }

        return [
            'id'  => $id,
            'in'  => 0,
            'out' => 0,
        ];
    }

    /**
     * @param \common\models\BinanceOrder $Order
     * @return mixed
     */
    private function binanceYes($Order)
    {
        $BinanceOrder = $Order;

        // Создаю ордер на бинансе
        /** @var \frontend\modules\Binance\Binance $provider */
        $provider = Yii::$app->Binance;

        /**
        'symbol' => 'BTCUSDT'
        'orderId' => 78364
        'orderListId' => -1
        'clientOrderId' => 'ikAmswABSyH35iAmNG3ama'
        'transactTime' => 1611765998657
        'price' => '0.00000000'
        'origQty' => '0.01000000'
        'executedQty' => '0.00841200'
        'cummulativeQuoteQty' => '192.10515000'
        'status' => 'EXPIRED'
        'timeInForce' => 'GTC'
        'type' => 'MARKET'
        'side' => 'SELL'
        'fills' => [
        0 => [
        'price' => '32250.00000000'
        'qty' => '0.00012900'
        'commission' => '0.00000000'
        'commissionAsset' => 'USDT'
        'tradeId' => 4980
        ]
        ]
        ]
         */
        $userBill = UserBill::findOne($BinanceOrder->billing_id);
        $Currency = \common\models\avatar\Currency::findOne($userBill->currency);
        $CurrencyInt = Currency::initFromCurrencyExt($userBill->currency);

        $data = $provider->_callApiKeyPostAndSigned('/api/v3/order', [
            'symbol'           => $Currency->code . 'USDT',
            'side'             => 'sell', // продаю
            'type'             => 'MARKET',
            'quantity'         => Currency::getValueFromAtom($BinanceOrder->amount, $CurrencyInt->id),
            'newOrderRespType' => 'FULL',
        ]);

        $BinanceOrder->binance_id = $data['clientOrderId'];
        $BinanceOrder->binance_order_id = $data['orderId'];
        $BinanceOrder->binance_return = Json::encode($data);
        $BinanceOrder->save();

        /** Если заявка выполнена? */
        if ($data['status'] == 'FILLED') {

            // возвращаю деньги
            $amount1 = $data['cummulativeQuoteQty'];

            // Отдаю монеты
            $cInt_to = \common\models\piramida\Currency::findOne(Currency::initFromCurrencyExt($BinanceOrder->currency_to)->id);
            $CIO_to = CurrencyIO::findOne(['currency_int_id' => $cInt_to->id]);
            $toCurrencyExt = \common\models\avatar\Currency::findOne($CIO_to->currency_ext_id);
            $walletOut = Wallet::findOne($CIO_to->main_wallet);
            $dataTo = UserBill::getInternalCurrencyWallet($cInt_to->id, $Order->user_id);

            // получаю комиссию системы
            $dataJson = '{}';
            if ($BinanceOrder->currency_to == \common\models\avatar\Currency::NEIRO) {
                $dataJson = Config::get('NeironConvertParams');
            }
            if ($BinanceOrder->currency_to == \common\models\avatar\Currency::MARKET) {
                $dataJson = Config::get('MarketConvertParams');
            }
            if ($BinanceOrder->currency_to == \common\models\avatar\Currency::LOT) {
                $dataJson = Config::get('LotConvertParams');
            }
            $data = Json::decode($dataJson);

            $userBill = UserBill::findOne($BinanceOrder->billing_id);
            $o = $this->getId($data, $userBill->currency);
            $amount_m_to = $amount1 * (1-($o['in'] / 100)) / $toCurrencyExt->price_usd;
            $amountTo = bcmul($amount_m_to, bcpow(10, $cInt_to->decimals));

            // Вычисляю тип транзакции
            $type = \common\models\NeironTransaction::TYPE_CONVERT;
            if ($Order->currency_to == \common\models\avatar\Currency::NEIRO) {
                $dataJson = Config::get('NeironConvertParams');
                if (in_array($userBill->currency, [
                    \common\models\avatar\Currency::BTC,
                    \common\models\avatar\Currency::LTC,
                    \common\models\avatar\Currency::DASH,
                    \common\models\avatar\Currency::ETH,
                    \common\models\avatar\Currency::TRX,
                    \common\models\avatar\Currency::BNB,
                ])) {
                    $type = \common\models\NeironTransaction::TYPE_CONVERT_GOOD;
                }
            }

            $t_to = $walletOut->move2(
                $dataTo['wallet'],
                $amountTo,
                Json::encode([
                    'Конвертация в NEIRO bid={id}',
                    ['id' => $BinanceOrder->id],
                ]),
                $type
            );

            // Устанавливаю статус
            $BinanceOrder->tx_to_id = $t_to['transaction']['id'];
            $BinanceOrder->amount_to = $amountTo;
            $BinanceOrder->status = $BinanceOrder::STATUS_FILLED;
            $BinanceOrder->save();

            // Делаю запись о типе транзакции если это NERON
            NeironTransaction::add([
                'transaction_id' => $t_to['transaction']['id'],
                'operation_id'   => $t_to['operation_add']['id'],
                'type_id'        => $type,
            ]);

            // Делаю реферальные начисления
            if ($type == \common\models\NeironTransaction::TYPE_CONVERT_GOOD) {
                $this->referalProgram($BinanceOrder);
            }

            // Уведомляю пользователя по телеграму
            $user = UserAvatar::findOne($BinanceOrder->user_id);
            if (!Application::isEmpty($user->telegram_chat_id)) {
                /** @var \aki\telegram\Telegram $telegram */
                $telegram = Yii::$app->telegram;
                $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                $telegram->sendMessage([
                    'chat_id' => $user->telegram_chat_id,
                    'text'    => Yii::t(
                        'c.LSZBX1ce2W',
                        'Конвертация #{id} выполнена. Получено {sum} {code}',
                        [
                            'id'   => $BinanceOrder->id,
                            'sum'  => $amount_m_to,
                            'code' => $cInt_to->code,
                        ],
                        $language
                    ),
                ]);
            }

            // Уведомляю бухгалтера по телеграму
            $userList = Yii::$app->authManager->getUserIdsByRole('role_buh');
            foreach ($userList as $uid) {
                $user = UserAvatar::findOne($uid);
                if (!Application::isEmpty($user->telegram_chat_id)) {
                    /** @var \aki\telegram\Telegram $telegram */
                    $telegram = Yii::$app->telegram;
                    $language = (Application::isEmpty($user->language)) ? 'ru' : $user->language;
                    $telegram->sendMessage([
                        'chat_id' => $user->telegram_chat_id,
                        'text'    => Yii::t(
                            'c.LSZBX1ce2W',
                            'Конвертация #{id} выполнена. Получено {sum} {code}',
                            [
                                'id'   => $BinanceOrder->id,
                                'sum'  => $amount_m_to,
                                'code' => $cInt_to->code,
                            ],
                            $language
                        ),
                    ]);
                }
            }
        }

        return $BinanceOrder->id;
    }


}