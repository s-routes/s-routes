<?php

namespace console\controllers\actions;

use common\models\Config;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\piramida\Operation;
use common\models\statistic\PingStatus;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Action;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 * проверяет балансы процессинга
 */
class actionTestWallets extends \console\base\Action
{
    public $validateHash = false;
    public $notifyTelegram = true;

    /**
     * пингует главный сайт
     */
    public function run()
    {
        // Проверяю балансы операций
        // Проверяю Контрольную сумму в операциях
        foreach (\common\models\piramida\Operation::find()->all() as $item) {
            $v = \yii\helpers\ArrayHelper::getValue($item, 'type');
            if ($v < 0) $i = -1;
            else $i = 1;
            $sum = $item['after'] - $item['before'] - ($i * $item['amount']);
            if ($sum != 0) {
                $this->sendToTelegram('balance op_id='.$item->id . ' not summed');
            }
            if ($this->validateHash) {
                if ($item->hash() != $item->hash) {
                    self::logDanger('HASH op_id='.$item->id . ' not summed');
                }
            }
        }
        self::logSuccess( 'Operations succesed.');

        if ($this->validateHash) {
            // Проверяю Контрольную сумму в транзакциях
            foreach (\common\models\piramida\Transaction::find()->all() as $item) {
                if ($item->hash() != $item->hash) {
                    self::logDanger( 'HASH tx_id=' . $item->id . ' not summed');
                }
            }
            self::logSuccess( 'Transactions succesed.' );
        }

        // Проверяю балансы в dbWallet.currency
        $data = (new \yii\db\Query())->createCommand(\Yii::$app->dbWallet)->setSql('select t1.am1,t1.currency_id,currency.amount, currency.amount - t1.am1 as balance FROM (
select sum(wallet.amount) as am1,currency_id from wallet GROUP BY currency_id
) as t1
INNER JOIN currency on (currency.id = t1.currency_id)')->queryAll();
        foreach ($data as $item) {
            if ($item['balance'] != 0) {
                $str = [
                    'cid=' . $item['currency_id'] . ' not summed',
                    'sum wallets=' . $item['am1'],
                    'currency.amount=' . $item['amount'],
                    'balance=' . $item['balance'],
                ];
                self::logDanger( join("\n", $str));
                $this->sendToTelegram(join("\n", $str));
            }
        }

        // Проверяю кошельки
        $rows = \common\models\piramida\Wallet::find()->all();
        /** @var \common\models\piramida\Wallet $item */
        foreach ($rows as $item) {
            $operationList = Operation::find()->where(['wallet_id' => $item['id']])->orderBy(['datetime' => SORT_ASC])->all();
            $sum = 0;
            /** @var \common\models\piramida\Operation $o */
            foreach ($operationList as $o) {
                if (in_array($o->type, [Operation::TYPE_IN, Operation::TYPE_EMISSION])) {
                    $sum += $o->amount;
                }
                if (in_array($o->type, [Operation::TYPE_OUT, Operation::TYPE_BURN])) {
                    $sum -= $o->amount;
                }
            }
            if ($item['amount'] != $sum) {

                self::logDanger( 'balance wid=' . $item['id'] . ' not summed' . ' amount= ' . $item['amount'] . ' sum=' . $sum);
                $this->sendToTelegram('balance wid=' . $item['id'] . ' not summed' . ' amount= ' . $item['amount'] . ' sum=' . $sum);
            }
        }
    }

    /**
     */
    private function sendToTelegram($text)
    {
        if ($this->notifyTelegram) {
            $all = \Yii::$app->authManager->getUserIdsByRole('role_admin');

            /** @var \aki\telegram\Telegram $telegram */
            $telegram = \Yii::$app->telegram;

            foreach ($all as $uid) {
                $u = UserAvatar::findOne($uid);
                if (!Application::isEmpty($u->telegram_chat_id)) {
                    /** @var \common\models\UserAvatar $uCurrent */
                    $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => 'Ошибка процессинга:' . "\n" . $text]);
                }
            }
        }
    }
}