<?php

namespace console\controllers\actions\EthController;

use common\models\Config;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use cs\services\VarDumper;
use yii\base\Action;
use yii\helpers\Json;
use yii\helpers\StringHelper;

/**
 */
class actionSyncChain extends \console\base\Action
{
    public $key = '\console\controllers\EthController::lastBlock';
    public $lastAvatarNetwork;

    public function init()
    {
        if (YII_ENV_PROD) {
            $this->lastAvatarNetwork = 5000000;
        } else {
            $this->lastAvatarNetwork = 2700000;
        }
    }

    /**
     * Делает загрузку в БД последних десяти блоков
     */
    public function run()
    {
        /** @var int $count кол-во блоков обрабатываемое за один вызов функции */
        $count = 1000;

        /** @var int $lastAvatarNetwork высота блока загруженная в AvatarNetwork */
        $lastAvatarNetwork = $this->getHighestBlock();

        // очищаю старые остатки которые могут остаться от сброса или ошибки
        $txid = Transaction::find()->where(['block' => $lastAvatarNetwork])->select(['min(id) as minid'])->scalar();
        Transfer::deleteAll(['>', 'transaction_id', $txid]);
        Transaction::deleteAll(['block' => $lastAvatarNetwork]);

        $provider = new \avatar\modules\ETH\ServiceInfura();

        /** @var int $lastETH высота блока в цепи ETH */
        $lastETH = $provider->blockNumber();

        // вычисляю максимальную высоту блока которую буду обрабатывать за цикл вызова этой функции
        if ($lastAvatarNetwork + $count < $lastETH) {
            $max = $lastAvatarNetwork + $count;
        } else {
            $max = $lastETH;
        }

        // цикл для новых блоков
        for ($i = $lastAvatarNetwork; $i <= $max; $i++) {
            // начинаю обработку одного блока
            $this->executeBlock($i, $provider);

            // сохраняю в памяти что этот блок соханен и если вызывающий прервет обработку скрипта то далее он уже не будет обрабатываться
            Config::set($this->key, $i + 1);
        }
    }


    /**
     * Получает список транзакций и обрабатывает их (добавляет в БД)
     *
     * @param int                               $height высота блока
     * @param \avatar\modules\ETH\ServiceInfura $provider
     */
    private function executeBlock($height, $provider)
    {
        $responseData = $provider->getBlockByNumber($height);

        $i = 1;
        $c = count($responseData['transactions']);

        self::log('block = ' . $height . ' count = ' . $c);

        foreach ($responseData['transactions'] as $transaction) {
            self::log('tx ' . $i . '/' . $c . ' ' . $transaction['hash']);

            // Начинаю разбор логов транзакции
            $dataTransaction = $provider->_method('eth_getTransactionReceipt', [$transaction['hash']]);
            if ($dataTransaction['status'] != '0x1') {
                // Если ошибка в транзакции то не буду добавлять ее
                $i++;
                self::logDanger('skip');
                continue;
            }

            $this->executeTransaction($dataTransaction, $transaction, $responseData, $height);

            $i++;
        }
    }

    /**
     * Обрабатывает транзакцию
     *
     * @param array $dataTransaction    данные от `eth_getTransactionReceipt`
     * @param array $transaction        данные от $responseData['transactions'] $provider->getBlockByNumber($height);
     * @param array $responseData
     * @param int   $height             высота блока которому принадлежит транзакция
     */
    private function executeTransaction($dataTransaction, $transaction, $responseData, $height)
    {
        $txid = null;
        if (isset($dataTransaction['logs'])) {
            if (count($dataTransaction['logs']) > 0) {
                foreach ($dataTransaction['logs'] as $logItem) {
                    if (isset($logItem['topics'])) {
                        if (count($logItem['topics']) >= 3) {
                            if (strtolower($logItem['topics'][0]) == '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef') {
                                if (is_null($txid)) {

                                    // добавляю транзакцию если в ней присутствует событие перевода токенов
                                    $item = new Transaction([
                                        'hash'      => $transaction['hash'],
                                        'time'      => hexdec($responseData['timestamp']),
                                        'comission' => (hexdec($transaction['gas'])  * hexdec($transaction['gasPrice'])),
                                        'block'     => $height,
                                    ]);
                                    $item->save();
                                    self::log('Add transaction hash = ' . $transaction['hash']);
                                    $txid = $item::getDb()->lastInsertID;
                                }

                                $from = $this->clearAddress(strtolower($logItem['topics'][1]));
                                $to = $this->clearAddress(strtolower($logItem['topics'][2]));
                                $contract = $logItem['address'];
                                $amount = $this->bchexdec($logItem['data']);

                                // добавляю прямой перевод
                                $transfer = new Transfer([
                                    'owner'          => $from,
                                    'partner'        => $to,
                                    'amount'         => $amount,
                                    'contract'       => $contract,
                                    'type'           => Transfer::TYPE_OUT,
                                    'transaction_id' => $txid,
                                ]);
                                $transfer->save();

                                // добавляю обратный перевод
                                $transfer = new Transfer([
                                    'owner'          => $to,
                                    'partner'        => $from,
                                    'amount'         => $amount,
                                    'contract'       => $contract,
                                    'type'           => Transfer::TYPE_IN,
                                    'transaction_id' => $txid,
                                ]);
                                $transfer->save();

                                self::log('transfer from = ' . $from . ' to = ' . $to);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param string $address 0x000000000000000000000000b4bedf7676fd8b97f0e598098f1afc6dd59c4d8f
     *
     * @return string 0xb4bedf7676fd8b97f0e598098f1afc6dd59c4d8f
     */
    private function clearAddress($address)
    {
        $address = substr($address, 2);
        $example = '01c3BC06CD0f2B5645271e0c5d44D1B2B795F4BA';

        return '0x' . substr($address, strlen($address) - strlen($example));
    }

    private function bchexdec($hex)
    {
        if (StringHelper::startsWith($hex, '0x')) $hex = substr($hex, 2);
        $hex = strtoupper($hex);

        $dec = 0;
        $len = strlen($hex);
        for ($i = 1; $i <= $len; $i++) {
            $dec = bcadd($dec, bcmul(strval(hexdec($hex[$i - 1])), bcpow('16', strval($len - $i))));
        }
        return $dec;
    }

    /**
     * Возвращает высоту блока которая уже обработана в AvatarNetwork
     *
     * @return int
     */
    private function getHighestBlock()
    {
        $value = Config::get($this->key);
        if ($value === false) {
            Config::set($this->key, $this->lastAvatarNetwork);

            return $this->lastAvatarNetwork;
        }

        return $value;
    }
}