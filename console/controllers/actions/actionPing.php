<?php

namespace console\controllers\actions;

use common\models\Config;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\statistic\PingStatus;
use common\models\UserAvatar;
use cs\Application;
use cs\services\VarDumper;
use yii\base\Action;
use yii\helpers\Json;
use yii\helpers\StringHelper;
use yii\httpclient\Client;

/**
 */
class actionPing extends \console\base\Action
{
    const SERVER_ID  = 13;

    /**
     * пингует главный сайт
     */
    public function run()
    {
        $url = 'https://neiro-n.com';
        $client = new Client(['baseUrl' => $url]);
        $status = true;
        try {
            $response = $client->get('/')->send();
            if ($response->statusCode != 200) {
                $status = false;
            }
        } catch (\Exception $e) {
            $status = false;
        }

        if ($status) {
            PingStatus::add(self::SERVER_ID, 1);
            self::logSuccess('success');
        } else {
            PingStatus::add(self::SERVER_ID, 0);

            $this->sendToTelegram();
            self::logDanger('false');
        }
    }

    /**
     */
    private function sendToTelegram()
    {
        $all = \Yii::$app->authManager->getUserIdsByRole('role_admin');

        /** @var \aki\telegram\Telegram $telegram */
        $telegram = \Yii::$app->telegram;

        foreach ($all as $uid) {
            $u = UserAvatar::findOne($uid);
            if (!Application::isEmpty($u->telegram_chat_id)) {
                /** @var \common\models\UserAvatar $uCurrent */
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => 'PROD не доступен']);
            }
        }
    }
}