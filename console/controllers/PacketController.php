<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\Card;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\models\UserAvatar;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Subscribe;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\imagine\Image;
use Yii;

class PacketController extends \console\base\Controller
{

    /**
     */
    public function actionFinish()
    {
        $walletMain = Wallet::findOne(Config::get('shop_packet_coin_main_wallet'));
        /** @var \common\models\UserAvatar $u */
        foreach (UserAvatar::find()->all() as $u) {
            if ($u->market_till > 0) {
                if ($u->market_till < time()) {
                    $cMAR = \common\models\piramida\Currency::findOne(['code' => 'MAR']);
                    $billMAR = \common\models\UserBill2::findOne(['user_id' => $u->id, 'currency_id' => $cMAR->id]);
                    $walletMAR = \common\models\piramida\Wallet::findOne($billMAR->wallet_id);
                    if ($walletMAR->amount > 0) {
                        $t = $walletMAR->move2(
                            $walletMain,
                            $walletMAR->amount,
                            'Списание в связи с окончанием срока действия пакета'
                        );
                        /** @var \common\models\piramida\Transaction $t1 */
                        $t1 = $t['transaction'];
                        self::log('t=' . $t1->getAddress());
                    }
                }
            }
        }
    }

}