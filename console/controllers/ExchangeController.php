<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use common\models\CurrencyIO;
use common\models\DealClosed;
use common\models\exchange\Deal;
use common\models\exchange\Offer;
use common\models\piramida\Wallet;
use cs\services\VarDumper;
use Yii;
use yii\helpers\Json;

class ExchangeController extends \console\base\Controller
{

    /**
     * Выставляет у всех пользователей верное значение deals_count_all и deals_count_offer
     */
    public function actionSetDealsCount()
    {
        /** @var \common\models\UserAvatar $u */
        foreach (\common\models\UserAvatar::find()->all() as $u) {
            // Пересчитываю кол-во сделок deals_count_offer
            $offerList = Offer::find()->where(['user_id' => $u->id])->select('id')->column();
            $dealsCount = Deal::find()
                ->where(['offer_id' => $offerList])
                ->andWhere([
                    'status' => [
                        Deal::STATUS_MONEY_RECEIVED,
                        Deal::STATUS_CLOSE,
                        Deal::STATUS_AUDIT_FINISH,
                    ],
                ])
                ->count();

            // Пересчитываю кол-во сделок deals_count_all
            $Deal_count = Deal::find()
                ->where(['user_id' => $u->id])
                ->andWhere([
                    'status' => [
                        Deal::STATUS_MONEY_RECEIVED,
                        Deal::STATUS_CLOSE,
                        Deal::STATUS_AUDIT_FINISH,
                    ],
                ])
                ->count();
            $deals_count_all = $dealsCount + $Deal_count;

            $u->deals_count_all = $deals_count_all;
            $u->deals_count_offer = $dealsCount;
            $u->save();

            echo 'uid=' . $u->id . ' deals_count_all=' . $deals_count_all . ' deals_count_offer=' . $dealsCount . "\n";
        }
    }

    public function setOfferAssessment()
    {
        $users = \common\models\UserAvatar::find()->all();
        foreach ($users as $u) {
            $r_minus = \common\models\exchange\Assessment::find()->where(['user_id' => $u->id, 'value' => -1])->count();
            $r_plus = \common\models\exchange\Assessment::find()->where(['user_id' => $u->id, 'value' => 1])->count();
            $c1 = \common\models\exchange\Offer::updateAll(['assessment_positive' => $r_plus], ['user_id' => $u->id]);
            $c2 = \common\models\exchange\Offer::updateAll(['assessment_negative' => $r_minus], ['user_id' => $u->id]);
            echo 'user_id='. $u->id . ' plus = ' . $c1 . "\n";
            echo 'user_id='. $u->id . ' minus = ' . $c2 . "\n";
        }
    }

    public function actionCloseDeals()
    {
        // Если сделка открыта и прошло более 72 ч с момента открытия
        $dealList = Deal::find()->where(['<', 'created_at', time() - 60 * 60 * 72])
            ->andWhere([
                'in', 'status', [
                    Deal::STATUS_OPEN,
                    Deal::STATUS_BLOCK,
                    Deal::STATUS_MONEY_SENDED,
                    Deal::STATUS_MONEY_RECEIVED,
                ],
            ])
            ->all();

        /** @var Deal $deal */
        foreach ($dealList as $deal) {
            $deal->trigger(Deal::EVENT_BEFORE_HIDE);

            // Если деньги заблочены то перевести их в кошелек walletLost
            if (in_array($deal->status, [
                Deal::STATUS_BLOCK,
                Deal::STATUS_MONEY_SENDED,
            ])) {
                $cio = CurrencyIO::findOne($deal->currency_io);
                $walletEscrow = Wallet::findOne($cio->block_wallet);
                $walletLost = Wallet::findOne($cio->lost_wallet);
                $t = $walletEscrow->move($walletLost, $deal->volume_vvb, 'Сделка ' . $deal->id . ' так и не завершилась. Монеты перешли из блока в резерв');

                Yii::info(Json::encode([
                    'deal_id'    => $deal->id,
                    'tx_id'      => $t->id,
                    'volume_vvb' => $deal->volume_vvb,
                ]), 'avatar\console\controllers\ExchangeController::actionCloseDeals');

                DealClosed::add([
                    'deal_id'    => $deal->id,
                    'tx_id'      => $t->id,
                    'volume_io' => $deal->volume_vvb,
                ]);

                echo('Сделка ' . $deal->id . ' так и не завершилась. Монеты перешли из блока в резерв. $t=' . $t->getAddress());
                echo("\n");
            }

            $deal->status = Deal::STATUS_HIDE;
            $deal->save();

            echo('Сделка ' . $deal->id . ' закрыта');
            echo("\n");

            $deal->trigger(Deal::EVENT_AFTER_HIDE);
        }
    }

}