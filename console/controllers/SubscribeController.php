<?php

namespace console\controllers;

use common\models\subscribe\Subscribe;
use common\models\subscribe\SubscribeMail;
use common\models\subscribe\SubscribeStat;
use kartik\base\Config;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * Занимается рассылкой писем
 */
class SubscribeController extends Controller
{
    public function dump($v)
    {
        VarDumper::dump($v,3);
        echo("\n");
        exit;
    }

    /**
     * Делает рассылку писем из списка рассылки
     */
    public function actionSend()
    {
        $limit = 8; // писем в минуту
        $limitPerDay = 1900; // писем в день

        $counterDay = \common\models\Config::get('mailCounterPerDay');
        if ($counterDay + $limit > $limitPerDay) {
            echo 'limit end $counterDay=' . $counterDay;
            echo "\n";
            return;
        }


        $time = microtime(true);
        $idsSuccess = [];
        $rows = SubscribeMail::find()
            ->limit($limit)
            ->select(['id', 'subscribe_id', 'mail'])
            ->all();
        if (count($rows) > 0) {
            $subscribeIds = ArrayHelper::map($rows, 'id', 'mail', 'subscribe_id');
            foreach ($subscribeIds as $subscribeId => $mailList) {
                /** @var Subscribe $s */
                $s = Subscribe::findOne($subscribeId);
                if ($s) {
                    foreach ($mailList as $id => $mail) {

                        try {
                            $mailerConfig = Json::decode($s->mailer);
                            $mailer = \Yii::createObject($mailerConfig);
                        } catch (\Exception $e) {
                            echo $e->getMessage();
                            continue;
                        }

                        try {

                            if (!empty($s->from_email) && !empty($s->from_name)) {
                                $from = [$s->from_email => $s->from_name];
                            } else {
                                $from = \Yii::$app->params['mailer']['from'];
                            }

                            $mailO = $mailer
                                ->compose()
                                ->setFrom($from)
                                ->setTo($mail)
                                ->setSubject($s->subject)
                                ->setHtmlBody($s->html);

                        } catch (\Exception $e) {
                            echo $e->getMessage();
                            \Yii::info($e->getMessage(), 'avatar\\app\\commands\\SubscribeController::actionSend2');
                            continue;
                        }

                        echo $mail;
                        echo ' ';
                        try {
                            $result = $mailO->send();
                            if ($result == false) {
                                \Yii::info('Не удалось доствить: ' . $mail, 'avatar\\app\\commands\\SubscribeController::actionSend');
                                echo 'Не удалось доствить: ' . $mail;
                            }
                        } catch (\Exception $e) {
                            \Yii::info('Не удалось доствить: ' . $mail . ' ' . $e->getMessage(), 'avatar\\app\\commands\\SubscribeController::actionSend');
                            echo $e->getMessage();
                        }
                        echo "\n";
                        $idsSuccess[] = $id;
                    }
                } else {
                    SubscribeMail::deleteAll(['subscribe_id' => $subscribeId]);
                }
            }
            (new SubscribeStat([
                'time'    => time(),
                'counter' => count($idsSuccess),
                'delay'   => microtime(true) - $time,
            ]))->save();

            // Удаляю все старые письма
            SubscribeMail::deleteAll(['in', 'id', $idsSuccess]);

            // Устанавливаю счетчик отправленных писем
            \common\models\Config::set('mailCounterPerDay', $counterDay + count($idsSuccess));

        } else {
            Subscribe::deleteAll();
        }
    }

    public function actionGet()
    {
        echo \common\models\Config::get('mailCounterPerDay');
        echo "\n";
    }

    public function actionSetMax()
    {
        \common\models\Config::set('mailCounterPerDay', 1900);
        echo "success\n";
    }

    public function actionResetCounter()
    {
        \common\models\Config::set('mailCounterPerDay', 0);
        echo "success\n";
    }
}