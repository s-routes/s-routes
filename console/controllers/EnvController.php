<?php

namespace console\controllers;

use backend\models\PaymentSystemsForm;
use common\models\avatar\UserBill;
use common\models\Config;
use common\models\Currency;
use common\models\CurrencyLog;
use common\models\Language;
use common\models\language\Category;
use common\models\language\Message;
use common\models\language\SourceMessage;
use common\models\Orders;
use common\models\PaymentBitCoin;
use common\models\PaymentSystem;
use common\models\rbac\AuthAssignment;
use common\models\rbac\AuthItemChild;
use common\models\UserAvatar;
use common\services\Debugger;
use GuzzleHttp\Query;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\web\HttpException;
use common\models\User;
use common\models\Billing;
use common\models\Banners;
use common\models\SmartresponderHistory;
use yii\web\Request;


/**
 * @package console\controllers
 */
class EnvController extends \console\base\Controller
{
    public function actionKey($name1)
    {
        $file = Yii::getAlias('@frontend/../.env');
        $content = file_get_contents($file);
        $rows = explode("\n", $content);
        $keys = [];
        foreach ($rows as $row) {
            $r = trim($row);
            if (strlen($r) > 0) {
                $pos = strpos($r, '=');
                if ($pos !== false) {
                    $name = trim(substr($r,0, $pos));
                    $value = substr($r, $pos + 1);

                    $keys[$name] = $value;
                }
            }
        }
        if (isset($keys[$name1])) {
            echo $keys[$name1] . "\n";
        }
    }

}
