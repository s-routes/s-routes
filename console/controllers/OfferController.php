<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use common\models\avatar\UserBill;
use common\models\CurrencyIO;
use common\models\exchange\Assessment;
use common\models\exchange\Offer;
use cs\Application;
use cs\services\VarDumper;
use Yii;
use yii\helpers\Json;

class OfferController extends \console\base\Controller
{
    public function actionTest()
    {
        $all = Offer::find()->groupBy('user_id')->select(['user_id', 'min(assessment_positive) as minp1', 'min(assessment_negative) as minn1', 'max(assessment_positive) as maxp1', 'max(assessment_negative) as maxn1'])->asArray()->all();
        foreach ($all as $i) {
            if ($i['minn1'] < 0) {
                $i['minn1'] = -$i['minn1'];
            }
            if ($i['maxn1'] < 0) {
                $i['maxn1'] = -$i['maxn1'];
            }

            $v = Assessment::find()->where(['user_id' => $i['user_id']])->andWhere(['>', 'value', 0])->select('sum(value)')->scalar();
            $v2 = Assessment::find()->where(['user_id' => $i['user_id']])->andWhere(['<', 'value', 0])->select('sum(value)')->scalar();
            if (is_null($v)) $v=0;
            if (is_null($v2)) $v2=0;
            if ($v2 < 0) $v2 = -$v2;
            $p1real = $v;
            $n1real = $v2;

            if ($i['minp1'] != $i['maxp1']) {
                echo 'minp1 != maxp1: user_id=' . $i['user_id'] . ' minp1=' . $i['minp1'] . ' maxp1=' . $i['maxp1'] . ' $p1real=' . $p1real . "\n";
            } else if ($p1real != $i['minp1']) {
                echo 'p1real != minp1: user_id=' . $i['user_id'] . ' minp1=' . $i['minp1'] . ' $p1real=' . $p1real . "\n";
            }

            if ($i['minn1'] != $i['maxn1']) {
                echo 'minn1 != maxn1: user_id=' . $i['user_id'] . ' minn1=' . $i['minn1'] . ' maxn1=' . $i['maxn1'] . ' $p1real=' . $p1real . "\n";
            } else if ($n1real != $i['minn1']) {
                echo 'n1real != minn1: user_id=' . $i['user_id'] . ' minn1=' . $i['minn1'] . ' $n1real=' . $n1real . "\n";
            }
        }
    }

    /**
     * Синхронизирует рейтинги
     */
    public function actionSync()
    {
        $all = Offer::find()->groupBy('user_id')->select(['user_id', 'min(assessment_positive) as minp1', 'min(assessment_negative) as minn1', 'max(assessment_positive) as maxp1', 'max(assessment_negative) as maxn1'])->asArray()->all();
        foreach ($all as $i) {
            if ($i['minn1'] < 0) {
                $i['minn1'] = -$i['minn1'];
            }
            if ($i['maxn1'] < 0) {
                $i['maxn1'] = -$i['maxn1'];
            }

            $v = Assessment::find()->where(['user_id' => $i['user_id']])->andWhere(['>', 'value', 0])->select('sum(value)')->scalar();
            $v2 = Assessment::find()->where(['user_id' => $i['user_id']])->andWhere(['<', 'value', 0])->select('sum(value)')->scalar();
            if (is_null($v)) $v=0;
            if (is_null($v2)) $v2=0;
            if ($v2 < 0) $v2 = -$v2;
            $p1real = $v;
            $n1real = $v2;

            // assessment_positive
            if ($i['minp1'] != $i['maxp1']) {
                echo 'minp1 != maxp1: update user_id=' . $i['user_id'] . ' minp1=' . $i['minp1'] . ' maxp1=' . $i['maxp1'] . ' $p1real=' . $p1real . "\n";
                Offer::updateAll(['assessment_positive' => $p1real], ['user_id' => $i['user_id']]);
            } else if ($p1real != $i['minp1']) {
                Offer::updateAll(['assessment_positive' => $p1real], ['user_id' => $i['user_id']]);
                echo 'p1real != minp1: update user_id='.$i['user_id'].' minp1='.$i['minp1'].' $p1real='.$p1real."\n";
            }

            // assessment_negative
            if ($i['minn1'] != $i['maxn1']) {
                echo 'minn1 != maxn1: update user_id=' . $i['user_id'] . ' minn1=' . $i['minn1'] . ' maxn1=' . $i['maxn1'] . ' $p1real=' . $p1real . "\n";
                Offer::updateAll(['assessment_negative' => $n1real], ['user_id' => $i['user_id']]);
            } else if ($n1real != $i['minn1']) {
                Offer::updateAll(['assessment_negative' => $n1real], ['user_id' => $i['user_id']]);
                echo 'n1real != minn1: update user_id='.$i['user_id'].' minn1='.$i['minn1'].' $n1real='.$n1real."\n";
            }
        }
    }

    /**
     * Ситуация 75280808
     * Убирать из списка предложений в обменнике те предложения по продаже монет, кошельки с которыми имеют баланс не менее, чем максимальная сумма монет в предложении.
     */
    public function actionTestEmptyAndHide()
    {
        $all = Offer::find()->where(['is_hide' => 0, 'type_id' => Offer::TYPE_ID_SELL])->all();
        /** @var \common\models\exchange\Offer $i */
        foreach ($all as $i) {
            $cid = CurrencyIO::findOne($i->currency_io);
            $data = UserBill::getInternalCurrencyWallet($cid->currency_int_id, $i->user_id);

            /** @var $wallet \common\models\piramida\Wallet */
            $wallet = $data['wallet'];

            // Если у пользователя который предлагает меньше монет чем он максимально предлагает то скрываю предложение
            if ($wallet->amount < $i->volume_io_finish) {
                $i->is_hide = 1;
                $i->save();
                echo 'oid=' . $i->id . ' c=' . $cid->tab_title.' volume_io_finish=' . $i->volume_io_finish . ' user_amount=' . $wallet->amount . ' hided' . "\n";
                Yii::info(Json::encode(['oid' => $i->id]), 'avatar\console\offer\test-empty-and-hide');
            }
        }
    }

    /**
     * Ситуация 75280808
     * Убирать из списка предложений в обменнике те предложения по продаже монет, кошельки с которыми имеют баланс не менее, чем максимальная сумма монет в предложении.
     */
    public function actionNotifyTimeOut()
    {
        /** @var \aki\telegram\Telegram $telegram */
        $telegram = Yii::$app->telegram;

        $all = Offer::find()
            ->where(['is_hide' => 0])
            ->andWhere(['is_send_time_out_notification' => 0])
            ->andWhere(['<', 'created_at', time() - 60 * 60 * 24 * 7])
            ->all();

        /** @var \common\models\exchange\Offer $i */
        foreach ($all as $i) {
            $u = $i->getUser();
            if (!Application::isEmpty($u->telegram_chat_id)) {
                $text = 'Ваше объявление #' . $i->id . ' на площадке Р2Р-обмена портала Neiro-N.com - закрылось по тайм-ауту (7 дней). При необходимости, Вы можете перевыставить его. Ссылка https://neiro-n.com/cabinet-exchange/index';
                $telegram->sendMessage(['chat_id' => $u->telegram_chat_id, 'text' => $text]);
                echo 'send offer='.$i->id . "\n";
            }
            $i->is_send_time_out_notification = 1;
            $i->save();
        }
    }

    /**
     * Ситуация 75280808
     * Убирать из списка предложений в обменнике те предложения по продаже монет, кошельки с которыми имеют баланс не менее, чем максимальная сумма монет в предложении.
     */
    public function actionTestEmpty()
    {
        $all = Offer::find()->where(['is_hide' => 0, 'type_id' => Offer::TYPE_ID_SELL])->all();

        /** @var \common\models\exchange\Offer $i */
        foreach ($all as $i) {
            $cid = CurrencyIO::findOne($i->currency_io);
            $data = UserBill::getInternalCurrencyWallet($cid->currency_int_id, $i->user_id);

            /** @var $wallet \common\models\piramida\Wallet */
            $wallet = $data['wallet'];

            // Если у пользователя который предлагает меньше монет чем он максимально предлагает то скрываю предложение
            if ($wallet->amount < $i->volume_io_finish) {
                echo 'oid=' . $i->id . ' c='.$cid->tab_title . ' volume_io_finish=' . $i->volume_io_finish . ' user_amount=' . $wallet->amount . "\n";
            }
        }
    }

}