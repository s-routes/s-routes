<?php

namespace console\controllers;

use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\Bilet;
use common\models\BinanceOrder;
use common\models\Card;
use common\models\Config;
use common\models\ConvertOperation;
use common\models\CurrencyIO;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\Lot;
use common\models\NeironTransaction;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Operation;
use common\models\piramida\Wallet;
use common\models\ReferalTransaction;
use common\models\UserAvatar;
use common\services\Subscribe;
use cs\Application;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\imagine\Image;
use Yii;

class LotController extends \console\base\Controller
{
    /**
     */
    public function actionNormalizeBiletSold()
    {
        $rows =  Lot::find()->all();
        /** @var \common\models\Lot $lot */
        foreach ($rows as $lot) {
            $bc = Bilet::find()->where(['lot_id' => $lot->id])->count();
            $lot->bilet_sold = $bc;
            $lot->save();
            echo 'lot_id='. $lot->id . ' BiletSold=' . $bc . "\n";
        }
    }

    /**
     */
    public function actionStart()
    {
        $now = time();
        $rows = Lot::find()->where(['<', 'time_raffle', $now])->andWhere(['is_counter_start' => 0])->all();

        /** @var \common\models\Lot $lot */
        foreach ($rows as $lot) {
            $this->v1($lot);
        }
    }

    /**
     */
    public function actionAutoLot()
    {
        $lot = Config::get('lot-auto');
        if (is_null($lot) || $lot === false) {
            echo 'lot-auto not set' . "\n";
            return;
        } else {
            $lot = Json::decode($lot);
        }
        if (ArrayHelper::getValue($lot, 'is_work', 0) == 1) {
            $one = Lot::find()->where(['is_auto' => 1])->orderBy(['time_start' => SORT_DESC])->one();
            if (is_null($one)) {
                // Если еще ничего нет
                $time_start = time() + $lot['delta'] * 60;
                $time_finish = $time_start + $lot['add_time_finish'] * 60;
                $time_raffle = $time_start + $lot['add_time_raffle'] * 60;
                do {
                    $new = new Lot([
                        'name'              => $lot['name'],
                        'image'             => $lot['image'],
                        'price'             => $lot['price'],
                        'bilet_count'       => $lot['bilet_count'],
                        'time_start'        => $time_start,
                        'time_finish'       => $time_finish,
                        'time_raffle'       => $time_raffle,
                        'description'       => $lot['description'],
                        'cash_back_percent' => $lot['cash_back_percent'],
                        'cash_back_price'   => $lot['cash_back_price'],
                        'type'              => Lot::TYPE_AUTO,
                        'content'           => $lot['content'],
                        'is_auto'           => 1,
                    ]);
                    $new->save();
                    $new->id = $new::getDb()->lastInsertID;
                    echo 'lot id=' . $new->id . ' time_start=' . date('Y-m-d H:i', $time_start) . "\n";

                    $time_start += $lot['delta'] * 60;
                    $time_finish = $time_start + $lot['add_time_finish'] * 60;
                    $time_raffle = $time_start + $lot['add_time_raffle'] * 60;

                } while ($time_start > time() + 24 * 60 * 60);

            } else {
                // Смотрю относительно последнего лота
                if ($one['time_start'] + $lot['delta'] * 60 < time() + 24 * 60 * 60) {
                    $new = new Lot([
                        'name'              => $lot['name'],
                        'image'             => $lot['image'],
                        'price'             => $lot['price'],
                        'bilet_count'       => $lot['bilet_count'],
                        'time_start'        => $one['time_start'] + $lot['delta'] * 60,
                        'time_finish'       => $one['time_start'] + $lot['delta'] * 60 + $lot['add_time_finish'] * 60,
                        'time_raffle'       => $one['time_start'] + $lot['delta'] * 60 + $lot['add_time_raffle'] * 60,
                        'description'       => $lot['description'],
                        'cash_back_percent' => $lot['cash_back_percent'],
                        'cash_back_price'   => $lot['cash_back_price'],
                        'type'              => Lot::TYPE_AUTO,
                        'content'           => $lot['content'],
                        'is_auto'           => 1,
                    ]);
                    $new->save();
                    $new->id = $new::getDb()->lastInsertID;
                    echo 'lot id=' . $new->id . ' time_start=' . date('Y-m-d H:i', $one['time_start'] + $lot['delta'] * 60) . "\n";
                }
            }
        }
    }

    /**
     * выдает победителю и кеш бек
     */
    public function actionWin()
    {
        $rows = Lot::find()->where(['is_send_win' => 0, 'is_counter_stop' => 1])->all();
        /** @var \common\models\Lot $lot */
        foreach ($rows as $lot) {
            $biletWiiner = Bilet::findOne($lot->winner_id);
            $winner = UserAvatar::findOne($biletWiiner->user_id);

            $cio = CurrencyIO::findOne(['currency_int_id' => Currency::LOT]);
            $mainWalletObject = Wallet::findOne($cio->main_wallet);

            if ($lot->prize_type == Lot::PRIZE_TYPE_MONEY) {
                if (!Application::isEmpty($lot['prize_amount'])) {
                    if ($lot['prize_amount'] > 0) {
                        // Отправляю главный приз
                        $data = UserBill::getInternalCurrencyWallet(Currency::LOT, $biletWiiner->user_id);
                        $walletWin = $data['wallet'];
                        try {
                            $t = $mainWalletObject->move2($walletWin, $lot['prize_amount'], 'E-LOTTERY: Главный Приз розыгрыша #'.$lot['id']);
                        } catch (\Exception $e) {
                            self::log($e->getMessage());
                            \cs\services\VarDumper::dump(ArrayHelper::toArray($lot));
                        }
                        echo 'lot=' . $lot->id . ' user_id=' . $biletWiiner->user_id . ' win=' . Currency::getValueFromAtom($lot['price'] * $lot->bilet_count, Currency::LOT) . ' LOT' . "\n";
                    }
                }
            }

            // Отправляю кешбек
            $cashBackRows = Bilet::find()
                ->where(['lot_id' => $lot->id, 'is_cash_back' => 1])
                ->andWhere(['not', ['user_id' => $biletWiiner->user_id]])
                ->all();
            /** @var \common\models\Bilet $bilet */
            foreach ($cashBackRows as $bilet) {
                $walletTo = UserBill::getInternalCurrencyWallet(Currency::LOT, $bilet->user_id);
                $cashBack = (int)($lot['price'] * $lot->cash_back_percent / 100);
                $t = $mainWalletObject->move2($walletTo['wallet'], $cashBack, 'Кешбек за лотерею #' . $lot['id']);
                $bilet->cash_bask_result = 1;
                $bilet->save();
                echo 'lot=' . $lot->id . ' user_id=' . $bilet->user_id . ' cashback=' . Currency::getValueFromAtom($cashBack, Currency::LOT) . ' LOT' . "\n";
            }

            // Отправляю письмо
            Subscribe::sendArray([$winner->email], 'Вы выиграли Главный Приз розыгрыша #' . $lot->id, 'win', ['lot' => $lot], ['layout' => '@lot/mail/layouts/html.php', 'namespace' => 'lot']);

            // ставлю флаг
            $lot->is_send_win = 1;
            $lot->save();
        }

    }

    /**
     * Фунция проверки на старт
     * @param \common\models\Lot $lot
     */
    private function v1($lot)
    {
        $uid = \Yii::$app->params['AUTO_REDEMPTION_USER_ID'];

        // Списываю деньги
        $data = UserBill::getInternalCurrencyWallet(\common\models\piramida\Currency::LOT, $uid);
        /** @var \common\models\piramida\Wallet $w */
        $w = $data['wallet'];
        $cio = CurrencyIO::findFromExt(\common\models\avatar\Currency::LOT);
        $mainWallet = Wallet::findOne($cio->main_wallet);

        // Кол-во билетов которое уже продано
        $countSold = Bilet::find()->where(['lot_id' => $lot->id])->count();
        // Кол-во билетов которое надо выкупить
        $count = $lot->bilet_count - $countSold;

        // Если никто ничего не купил, то Стартую лотерею и останавливаю сразу
        if ($countSold == 0) {
            $lot->is_counter_start = 1;
            $lot->is_counter_stop = 1;
            $lot->counter = time() * 1000;



        } else {
            $lot->counter = time() * 1000;
            $lot->is_counter_start = 1;

            if ($count > 0) {
                // Получение монет для выкупа билетов для лот №{id}
                try {
                    $t = $mainWallet->move2(
                        $w,
                        $lot->price * $count,
                        Json::encode(['Получение монет для выкупа билетов для лот №{id}', ['id' => $lot->id]])
                    );

                } catch (\Exception $e) {

                    \cs\services\VarDumper::dump([
                        $lot->price * $count,
                        $lot->price * $count,
                        $e->getMessage(),
                        ArrayHelper::toArray($lot),
                        ArrayHelper::toArray($w)
                    ]);
                }

                echo 'lot_id='. $lot->id . ' updated t=' . $lot->counter . "\n";
            }
        }

        $rows = [];
        for ($i = 1; $i <= $count; $i++) {
            try {
                $t = $w->move2(
                    $cio->main_wallet,
                    $lot->price,
                    Json::encode(['Покупка билета для лота №{id}', ['id' => $lot->id]])
                );
            } catch (\Exception $e) {
                \cs\services\VarDumper::dump([
                    $e->getMessage(),
                    ArrayHelper::toArray($lot),
                    ArrayHelper::toArray($w)
                ]);
            }

            // Оформление билета
            $i1 = Bilet::add([
                'lot_id'       => $lot->id,
                'user_id'      => $uid,
                'tx_id'        => $t['transaction']['id'],
            ]);
            $rows[] = $i1->id;

            if ($countSold == 0) {
                if ($i == 1) {
                    // Если никто ничего не купил то записываю победителем первый билет
                    $lot->winner_id = $i1->id;
                }
            }

            echo 'lot_id='. $lot->id . ' ticket=' . $i1->id . ' txid=' . $t['transaction']['id'] . " \n";
        }
        // Прибавляю кол-во купленных билетов
        $lot->bilet_sold += $count;
        $lot->save();
    }
}