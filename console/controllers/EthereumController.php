<?php
/**
 * Created by PhpStorm.
 * User: Бог-Творец
 * Date: 08.12.2016
 * Time: 9:33
 */

namespace console\controllers;

use avatar\controllers\actions\CabinetBills\TransactionsList;
use common\models\avatar\QrCode;
use common\models\avatar\UserBill;
use common\models\avatar\UserBillAddress;
use common\models\avatar\UserBillConfig;
use common\models\avatar\UserBillDefault;
use common\models\avatar\UserBillOperation;
use common\models\avatar\UserBillSystem;
use common\models\Card;
use common\models\Config;
use common\models\CurrencyIO;
use common\models\eth\Transaction;
use common\models\eth\Transfer;
use common\models\PaymentBitCoin;
use common\models\piramida\Currency;
use common\models\piramida\Wallet;
use common\payment\BitCoinBlockTrailPayment;
use common\services\Subscribe;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\httpclient\Client;
use yii\imagine\Image;
use Yii;

class EthereumController extends \console\base\Controller
{

    /**
     */
    public function actionGasPrice()
    {
        $client1 = new Client(['baseUrl' => 'https://api.etherscan.io/api']);
        $result = $client1->get('', [
            'module' => 'gastracker',
            'action' => 'gasoracle',
        ])->send();

        if ($result->statusCode != 200) {
            self::log('$result->statusCode != 200');
        } else {
            try {
                $data = Json::decode($result->content);
            } catch (\Exception $e) {
                self::log(VarDumper::dumpAsString($result->content));
                return;
            }
            try {
                $SafeGasPrice       = $data['result']['SafeGasPrice'];
                $ProposeGasPrice    = $data['result']['ProposeGasPrice'];
                $FastGasPrice       = $data['result']['FastGasPrice'];
            } catch (\Exception $e) {
                self::log(VarDumper::dumpAsString($data));
                return;
            }

            Config::set('EthereumSafeGasPrice', $SafeGasPrice);
            Config::set('EthereumProposeGasPrice', $ProposeGasPrice);
            Config::set('EthereumFastGasPrice', $FastGasPrice);

            self::log('success avg price = '.$ProposeGasPrice);
        }
    }

    public function actionMining()
    {
        $cIO = CurrencyIO::findOne(1);
        $mainWallet = Wallet::findOne($cIO->main_wallet);
        $percentByDay = Yii::$app->params['prizm']['mining-percent'];

        // собираю все кошельки у которых баланс больше 0 и кроме главного кошелька
        $walletList = Wallet::find()
            ->where([
                'is_deleted'  => 0,
                'currency_id' => Currency::PZM,
            ])
            ->andWhere(['>', 'amount', 0])
            ->andWhere(['not', ['id' => $mainWallet->id]])
            ->all();


        /** @var  \common\models\piramida\Wallet $wallet */
        foreach ($walletList as $wallet) {
            // Получаю последнюю транзакцию
            $t = \common\models\piramida\Transaction::find()
                ->where([
                    'or',
                    ['from' => $wallet->id],
                    ['to' => $wallet->id],
                ])
                ->orderBy(['datetime' => SORT_DESC])
                ->one();

            // если транзакция была ранее чем сегодня, то начисляю. Отвожу 120 секунд на прошлые доначисления.
            if (time() - ((60 * 60 * 24) - 120) > ($t->datetime/1000)) {
                $percent = (int)($wallet->amount * $percentByDay * 0.01);
                if ($percent > 0) {
                    $tp = $mainWallet->move($wallet, $percent, Json::encode(['Начисление процентов за {date}', ['date' => date('d.m.Y')]]));
                    self::log($tp->getAddress());
                }
            } else {
                self::log('time delta='. Yii::$app->formatter->asTime(time() - $t->datetime) . ' $t->datetime=' . $t->datetime);
            }
        }
    }
}