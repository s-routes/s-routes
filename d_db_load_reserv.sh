#!/bin/bash

cp /home/ubuntu/s-routes/db/dumps/day/0000/* /home/ubuntu/s-routes/
gunzip -d /home/ubuntu/s-routes/s_routes_prod_main.sql.gz
gunzip -d /home/ubuntu/s-routes/s_routes_prod_stat.sql.gz
gunzip -d /home/ubuntu/s-routes/s_routes_prod_wallet.sql.gz

docker exec -it s-routes-mysql ./db_load_reserv.sh $1


